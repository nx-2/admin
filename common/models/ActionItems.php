<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use  yii\db\Query;
use common\models\CommonModel;

/**
 * This is the model class for table "Action_items".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $mag_id
 * @property integer $months
 * @property integer $type
 * @property integer $discount
 *
 * @property Action $action
 */
class ActionItems extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Action_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'mag_id', 'months', 'type', 'discount','type'], 'required'],
            [['action_id', 'mag_id', 'months', 'discount'], 'integer'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'mag_id' => 'Издание',
            'months' => 'Срок',
            'type' => 'Тип подписки',
            'discount' => 'Скидка %',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(\common\models\Action::className(), ['id' => 'action_id']);
    }


    /**
     * @return array
     */
    public static function  getAllJournals(){

        $journal=[];

        if(!$data=\common\models\PublisherEditions::find()->where(['publisher_id'=>Yii::$app->user->identity->publisher_id, 'owner'=>1])->all())
            return $journal;

        foreach ($data as $j){
            $journal[$j->magazine->Message_ID]=$j->magazine->HumanizedName;
        }

        return $journal;
    }


    /**
     * @param $id
     * @return array|bool
     */
    public static function getOneJournals($id){
        return Magazines::find()->where(['Message_ID' => $id])->one();
    }

    /**
     * @param ActionItems $model
     * @return bool
     */
    public function setItems(\common\models\ActionItems $model){
        foreach ($model->type as $v) {
            $new = new self();
            $new->load(Yii::$app->request->post());
            $new->type = $v;
            $new->save();
        }

        return true;
    }
}
