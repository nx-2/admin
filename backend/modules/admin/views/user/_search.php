<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['user/index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'User_ID') ?>

    <?= $form->field($model, 'Password') ?>

    <?= $form->field($model, 'Checked') ?>

    <?= $form->field($model, 'Language') ?>

    <?= $form->field($model, 'Created') ?>

    <?php // echo $form->field($model, 'LastUpdated') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'Login') ?>

    <?php // echo $form->field($model, 'FullName') ?>

    <?php // echo $form->field($model, 'LastName') ?>

    <?php // echo $form->field($model, 'FirstName') ?>

    <?php // echo $form->field($model, 'MidName') ?>

    <?php // echo $form->field($model, 'Confirmed') ?>

    <?php // echo $form->field($model, 'RegistrationCode') ?>

    <?php // echo $form->field($model, 'Auth_Hash') ?>

    <?php // echo $form->field($model, 'company') ?>

    <?php // echo $form->field($model, 'job') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'house') ?>

    <?php // echo $form->field($model, 'building') ?>

    <?php // echo $form->field($model, 'flat') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'company_profile') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'birthday') ?>

    <?php // echo $form->field($model, 'phone_prefix') ?>

    <?php // echo $form->field($model, 'fax_prefix') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'district') ?>

    <?php // echo $form->field($model, 'index_num') ?>

    <?php // echo $form->field($model, 'open_email') ?>

    <?php // echo $form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'UserType') ?>

    <?php // echo $form->field($model, 'Text') ?>

    <?php // echo $form->field($model, 'LastLoginDate') ?>

    <?php // echo $form->field($model, 'publisher_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
