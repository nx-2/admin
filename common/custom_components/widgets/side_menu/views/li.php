<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\helpers\Url;
$href = (isset($data['link']) && isset($data['link']['attributes']) && isset($data['link']['attributes']['href'])) ? $data['link']['attributes']['href'] : '';
$href = trim(preg_replace('/\?.+/', '', $href));
$isActive = $currentUrl == $href;
$activeClass = $isActive ? ' active selected ' : '';

if (!isset($data['attributes']))
	$data['attributes'] = [];

if (isset($data['attributes']['class']))
	$data['attributes']['class'] .= $activeClass;
else
	$data['attributes']['class'] = $activeClass;

//$roles = isset($data['allowedRoles']) ? $data['allowedRoles'] : [];
//$thisUserRolesList = isset($data['thisUserRolesList']) ? $data['thisUserRolesList'] : [];
//
//$allowed = count(array_intersect($roles, $thisUserRolesList)) > 0;
$allowed = true;

$helper = Yii::$app->customHelpers;

?>
<?php if($allowed): ?>
	<?php $attributes = $helper::arrayToAttrs($data['attributes']);?>
	<li <?=$attributes;?>>
		<?php if(isset($data['text']) && !empty($data['text'])): ?>
			<?=$data['text'];?>
		<?php endif; ?>
		<?php if(isset($data['link'])): ?>
			<?php $linkAttributes = isset($data['link']['attributes']) ? $helper::arrayToAttrs($data['link']['attributes']) : ''; ?>
			<a <?=$linkAttributes ?>>
				<?php if(isset($data['link']['icon'])): ?>
					<?php $iconAttributes = isset($data['link']['icon']['attributes']) ? $helper::arrayToAttrs($data['link']['icon']['attributes']) : ''; ?>
					<i <?=$iconAttributes;?>>
						<?php if(isset($data['link']['icon']['text']) && !empty($data['link']['icon']['text'])):  ?>
							<?=$data['link']['icon']['text'];?>
						<?php endif; ?>
					</i> 
				<?php endif; ?>
				
				<?php if(isset($data['link']['text'])): ?>
					<?=$data['link']['text'];?>
				<?php endif; ?>

				<?php if(isset($data['link']['hasChildrenMark']) && $data['link']['hasChildrenMark']): ?>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				<?php endif; ?>
			</a>
		<?php endif; ?>

		<?php 
		if(isset($childUl)){
			echo($childUl);
		} 
		?>
	</li>
<?php endif; ?>