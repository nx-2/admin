<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Вернуть js файл
     * @return string
     */
    public function actionGetSubscribeJs($pId = false, $eId = false)
    {
//        $headers = Yii::$app->response->headers;
//        $headers->set('Content-Type', 'application/javascript');

        $apiUrl = yii::$app->params['api']['host'];

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->getHeaders()->set('Content-Type', 'application/javascript');        
        
        if (!$pId || !$eId)
            return 'console.log("I do not understand what do you mean...");';
        
        $publisher = \common\models\Publisher::findOne(intval($pId));
        $edition = \common\models\Edition::findOne($eId);
        
        if ($publisher == null || $edition == null)
            return 'console.log("I do not understand what do you mean...");';
        
        $eIds = $publisher->getAllMyActiveEditionsIds();
        
        if (!in_array($edition->Message_ID, $eIds))
            return 'console.log("Incorrect pair publisher<->edition...");';
        
        return $this->renderPartial('subscribe_script', ['apiUrl' => $apiUrl, 'publisherId' => $pId, 'editionId' => $eId]);
    }
    public function actionMultyMagSubscribeFormJs($pId = false, $width = 'auto', $showCovers = 1) {
        
        $editions = Yii::$app->request->getQueryParam('eIds', 'ALL');

//        if ($editions && is_array($editions) && count($editions) > 0 ) {
//            $editions = explode(',', $editions[0]);
//        }

        $publisherId = intval($pId);
//        $width = ($width != 'auto') ? intval($auto) . 'px' : '100%';
        $showCovers = (Boolean) $showCovers;

        $apiUrl = yii::$app->params['api']['host'];

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->getHeaders()->set('Content-Type', 'application/javascript');        

        return $this->renderPartial('multy_mag_script', 
                [
                    'publisherId' => $publisherId, 
                    'editions' => $editions, 
                    'width' => $width, 
                    'showCovers' => $showCovers,
                    'apiUrl' => $apiUrl,
                ]
        );
    }
    



    /**
     * Форма подписки
     * @return string
     */
    public function actionSubscribe()
    {
        $id = Yii::$app->request->getQueryParam('id', 1);
        $publisher_id= Yii::$app->request->getQueryParam('publisher_id', 35);
        $data = \common\models\Subscribe::sendRequestApi([], \yii\helpers\Url::to('@domain/subscribe/getForm/'.$id.'/'.$publisher_id.'?_format=json'));
        return $this->renderPartial('//site/subscribe', [
            'data' => $data
        ]);

    }



    /**
     * Форма подписки pdf
     * @return string
     */
    public function actionBuypdf()
    {

        return $this->renderPartial('//site/subscribeOne', [

        ]);

    }


    /**
     * Форма оплаты
     * @return string
     */
    public function actionPay(){
        return $this->render('//site/pay');

    }



    /*
     * Отправить данные для получения варианта оплаты
     */
    public function actionSetpayall()
    {
        $post = Yii::$app->request->post()['NxOrder'];
        $pay_method = \common\models\Subscribe::sendRequestApi($post, \yii\helpers\Url::to('@domain/subscribe/set-pay-all?_format=json'), 'POST');
        if (!$pay_method)
            throw  new \yii\web\HttpException(200, 'No pay method');


        return $this->render('//site/allpay', [
            'data' => $pay_method
        ]);

    }
    public function actionTest_all_paying_methods_form()
    {

        $orderHash = '0cc4812c8cf918ff2af725eccd9efc83';
        $transactionId = '132105';
        $publisherId = 36;
        
        $orderHash = 'bee1e1f9227666131ae3614ee4bafcb0';
        $transactionId = '132182';
        $publisherId = 35;
        
        $order = \common\models\NxOrder::findOne(['hash' => $orderHash]);
//        $transaction = \common\models\Transactions::findOne($transactionId);
        $paySystems = \common\models\RelationSystems::find()->where(['publisher_id' => $publisherId, 'enabled' => 1])->all();
        
        return $this->render('//subscribe/pay/allmethod', [
            'model' => $order,
            'paySystems' => $paySystems,
            'publisherId' => $publisherId,
        ]);
        
    }


    /**
     * Форма оплаты документ
     * @return string
     */
    public function actionDoc(){
        return $this->render('//site/paydoc');

    }

    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            echo json_encode(\yii\widgets\ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }








}
