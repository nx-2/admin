<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\behaviors;

use Yii;
use Closure;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\base\InvalidParamException;


class AttrChangeWatchBehavior extends Behavior
{
    public $attributesList = []; //список атрибутов, находящихся под наблюдением
    public $eventName = null; // имя события, генерируемое поведением.
    public $number = 1; //количество атрибутов из списка attributesList, которое должно измениться, чтобы было сгенерировано событие
    
    // Если eventName == null, то поведение генерирует столько событий, сколько атрибутов фактически изменилось. Имена событий в этом случае
    // строятся по шаблону attrName . 'Changed'. На каждый атрибут отдельное событие \common\models\events\ARsingleAttrChangedEvent();  $number игнорируется вообще.
    // Если же имя события указано (!=null) то генерится событие с указанным именем при соблюдении остальных условий, а именно:
    // Количество фактически изменившихся атрибутов не менее чем $number
    // тип генерируемого события в этом случае \common\models\events\ARmultyAttrChangedEvent() 

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }


    public function afterSave($event)
    {
        $changedAttrs = array_keys($event->changedAttributes);

        if ($this->eventName != null) {
            // одно событие
            $attrsRealyChanged = array_intersect($this->attributesList, $changedAttrs);
            if (intval($this->number) <= 0)
                $this->number = 1;

            if (count($attrsRealyChanged) >= $this->number) {

                $e = new \common\models\events\ARmultyAttrChangedEvent();
                $e->attrsList = $this->attributesList;
                $e->changedAttrsList = $attrsRealyChanged;
                $oldValues = [];
//                foreach( $attrsRealyChanged as $changedAttr ) {
//                    $oldValues[$changedAttr] = isset($event->changedAttributes[$changedAttr]) ?  $event->changedAttributes[$changedAttr] : $this->owner->{$changedAttr};
//                }
                foreach ($this->attributesList as $observedAttrName) {
                    // Если есть в списке изменившихся, то берем его, а если нет то берем текущее значение из родительской модели
                    $oldValues [$observedAttrName] = isset($event->changedAttributes[$observedAttrName]) ?  $event->changedAttributes[$observedAttrName] : $this->owner->{$observedAttrName};
                    $e->attrArray[] = [
                        'attrName' => $observedAttrName,
                        'oldValue' => isset($event->changedAttributes[$observedAttrName]) ? $event->changedAttributes[$observedAttrName] : $this->owner->{$observedAttrName},
                        'newValue' => $this->owner->{$observedAttrName},
                        'is_new_value' => isset($event->changedAttributes[$observedAttrName]) ? 1 : 0,
                    ];
                }
                $e->oldValues = $oldValues;
                $this->owner->trigger($this->eventName, $e);
            }
            return;
        } else {
            // на каждый измененный атрибут отдельное событие
            foreach ($this->attributesList as $watchableAttr) {
                if (in_array($watchableAttr, $changedAttrs)) {
                    //attribute has changed. Trigger event
                    $e = new \common\models\events\ARsingleAttrChangedEvent();
                    $e->attrName = $watchableAttr;
                    $e->oldValue = $event->changedAttributes[$watchableAttr];
                    $e->newValue = $this->owner->{$watchableAttr};

                    $this->owner->trigger($watchableAttr . 'Changed', $e);

                }
            }
            return;
        }


    }

    public function attach($owner)
    {
        if ( !($owner instanceof yii\db\ActiveRecord) )
            throw new InvalidParamException('attrChangeWatchBehavior can be attached to ActiveRecord class only!');

        foreach($this->attributesList as $watchableAttr) {
            if (!in_array($watchableAttr, $owner->attributes()))
                throw new InvalidParamException( $watchableAttr . ' does not exists in ' . $owner->className());
        }

        parent::attach($owner);

    }

}