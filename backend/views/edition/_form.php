<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<br>
<div class="col-md-4 col-sm-4 col-xs-12">
    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'HumanizedName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'start_subscription')->textInput(); ?>

    <?= $form->field($model, 'mask_for_reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_mask')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'ncDescription')->textarea(['rows' => 6]) ?>

    <?php

    echo $form->field($model, 'subscribe')->dropDownList([
        '1' => 'Да',
        '0' => 'Нет'
    ]);
    ?>


    <?php

    echo $form->field($model, 'esubscribe')->dropDownList([
        '1' => 'Да',
        '0' => 'Нет'
    ]);
    ?>

    <?php

    echo $form->field($model, 'Checked')->dropDownList([
        '1' => 'Активен',
        '0' => 'Неактивен'
    ]);
    ?>

</div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'fullNameEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'EnglishName')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'ncDescriptionEn')->textarea(['rows' => 6]) ?>

    </div>





    <div class="col-md-12 col-sm-12 col-xs-12">

<div class="form-group">
    <?= Html::submitButton('Сохранить' , ['class' =>  'btn btn-primary']) ?></div>

<?php ActiveForm::end(); ?>
    </div>
