<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\models;
use Yii;
/**
  *
 * @property integer $action_id
 * @property integer $country_id
 * @property integer $delivery_type_id
 * @property integer $delivery_zone_id
 * @property integer $subscriber_type_id
 * @property integer $publisher_id
 * @property boolean $price_id
 * @property boolean $promo_code
 * @property boolean $user_agreement
 * 
 * 
 */

class OrderData extends \yii\base\Model
{
    public $action_id;
    public $country_id;
    public $delivery_type_id;
    public $delivery_zone_id;
    public $subscriber_type_id;
    public $publisher_id;
    public $price_id;
    public $promo_code;
    public $user_agreement;
    
    public function rules() {
        return [
            [['publisher_id', 'price_id', 'delivery_type_id','delivery_zone_id', 'user_agreement'], 'required'],
            [['publisher_id', 'action_id','country_id','delivery_type_id','delivery_zone_id','price_id'],'integer'],
            [['user_agreement'],'in', 'range'=>[1], 'message' => 'Необходимо согласие на обработку персональных данных'],
            [['subscriber_type_id'],'in', 'range'=>[1,2], 'message' => 'Неизвестный тип подписчика'],
            
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id'], 'message' => 'Указанный издатель не найден'],
            [['action_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => \common\models\Action::className(), 'targetAttribute' => ['action_id' => 'id'], 'message' => 'Указанная акция не найдена'],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\DeliveryType::className(), 'targetAttribute' => ['delivery_type_id' => 'id'], 'message' => 'Указанный спосооб доставки не найден'],
            [['delivery_zone_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\DeliveryZone::className(), 'targetAttribute' => ['delivery_zone_id' => 'id'], 'message' => 'Указанная территория доставки не найдена'],
//            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\NxPrice::className(), 'targetAttribute' => ['price_id' => 'id'], 'message' => 'Указанный прайс не найден'],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\NxPrice::className(), 'targetAttribute' => ['price_id' => 'id', 'publisher_id' => 'publisher_id'],'message' => 'Указанный прайс не найден,либо принадлежит другому издателю'],
            
            [['promo_code'], 'string', 'max' => 45],
        ];
    }
    public function attributeLabels() {
        return [
            'action_id' => 'Акция',
            'country_id' => 'Страна доставки',
            'delivery_type_id' => 'Способ доставки',
            'delivery_zone_id'  => 'Территория доставки',
            'subscriber_type_id'  => 'Тип подписчика',
            'publisher_id'  => 'Издатель',
            'price_id'  => 'Прайс',
            'promo_code' => 'Промокод',
            'user_agreement' => 'Согласие на обработку персон. данных', 
        ];
    }
    
    
}
