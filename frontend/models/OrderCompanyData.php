<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\models;
use Yii;
/**
 *
 * @property string $company_address
 * @property string $company_name
 * @property integer $inn
 * @property integer $kpp
 * 
 * 
 */

class OrderCompanyData extends \yii\base\Model
{
    public $company_address;
    public $company_name;
    public $inn;
    public $kpp;

    public function rules() {
        return [
            [['company_address', 'company_name', 'inn','kpp'], 'required'],
            [['inn', 'kpp'],'integer'],
            [['company_address', 'company_name'],'string', 'max' => 50],
        ];
    }
    public function attributeLabels() {
        return [
            'company_address' => 'Адрес компании',
            'company_name' => 'Название компании',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
        ];
    }
    
    
}
