/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

var severUrl='//new-nx-api.dev';
var mag_id=1;
var year=2014;
var month=7;
var isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

var jqueryExists = (typeof(jQuery) == 'function') ? true : false;

if (!jqueryExists) {
    var script = document.createElement('script');

    script.type = 'text/javascript';
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

    document.getElementsByTagName('head')[0].appendChild(script);

    script.addEventListener('load', runAfterJqueryLoad, false);
} else {
    //runAfterJqueryLoad();
}

function runAfterJqueryLoad() {
    $(document).ready(function () {


        function start() {

            $.ajax({
                type: "GET",
                url:  severUrl + '/subscribe/getFormOne/' + mag_id + '/' + year + '/' + month + '?_format=json',
                dataType:"json",
                success: function(jsondata){

                    if (!jsondata)
                        alert("Ошибка при получении данных. Обратитесь в поддержку");

                    $('#form_sub_insert').html(jsondata);
                    getPrice();

                },
                error: function(jsondata){
                    alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });

        }


        function getPrice() {

            $.ajax({
                type: "POST",
                url: severUrl + "/subscribe/get-price",
                dataType: "json",
                data: {
                    subscriber_type_id: 1,
                    magazineID: mag_id,
                    delivery_type_id: 5,
                    _format: 'json'
                },
                success: function (jsondata) {

                    if (!isNumber(jsondata))
                        alert("Ошибка при расчетах. Обратитесь в поддержку");

                    $('#price').html(jsondata);
                },
                error: function (jsondata) {
                    alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });
        }


        $(document.body).on('submit', "#subscribe-form", function (e) {
            e.preventDefault();

            var $form = $("#subscribe-form");

            jQuery.ajax({
                url: severUrl + '/subscribe/create-pdf',
                type: "POST",
                dataType: "html",
                data: jQuery("#subscribe-form").serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);

                    $('#form_sub_insert').html(json);
                },
                error: function (response) {
                    alert('Что то пошло не так. Обратитесь в поддержку');
                }
            });

            return false;
        });


        start();

    });
}



