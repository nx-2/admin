<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\custom_components\pay;

use Yii;
use \common\models\PaySystemStatus;
use \common\models\Payment;
use \common\models\PaymentType;
use \common\models\NxOrder;
use \common\models\NxPerson;
use \common\models\RelationSystems;
use \common\models\NxOrderState;

/**
 * Description of YandexPay
 *
 * @author andrey
 */
class YandexPay implements interfaces\paySystem {
    // TODO: Много повторяющегося кода при обновлении статусов странзакции и заказа. Вынести обновление статусов в отдельный метод!!
    CONST PAY_SYSTEM_ID = 8;
    CONST CODE_SUCCESS = 0;
    CONST CODE_AUTH_ERR = 1;
    CONST CODE_CANCEL_ORDER = 100;
    CONST CODE_PARSE_REQUEST_ERR = 200;
    CONST LOG_CATEGORY = 'yandexKassaInteraction';
    
    private $order = false;
    private $incomingData = [];
    
    private $answer =  [];
    
    
    public function __construct($incomingData = []) {
        $this->incomingData = $incomingData;
    }
    
    private function initAnswer()
    {
        if (empty($this->incomingData))
            return;

        //Структура ответа для всех трех запросов (checkOrder, cancelOrder, paymentAviso) почти одинаковая
        //Отличия: при paymentAviso нет кода ответа 100, а только 0,1 и 200, 
        // performedDatetime для paymentAviso - время на нашей стороне а для checkOrder - на стороне яндекса
        
        //performedDatetime dateTime
        //code int
        //shopId long
        //invoiceId long
        //orderSumAmount CurrencyAmount
        //message string, до 255 символов
        //techMessage string, до 64 символов

        $data = $this->incomingData;
        $this->answer = [
            'performedDatetime' => isset($data['orderCreatedDatetime']) ? $data['orderCreatedDatetime'] : date('Y-m-d H:i:s') , //Момент обработки запроса по часам в Яндекс.Кассе.
            'code'              => self::CODE_SUCCESS,
            'shopId'            => isset($data['shopId']) ? $data['shopId'] : 0,//Идентификатор магазина. Соответствует значению параметра shopId в запросе.
            'invoiceId'         => isset($data['invoiceId']) ? $data['invoiceId'] : 0, //Идентификатор транзакции в Яндекс.Кассе. Соответствует значению параметра invoiceId в запросе.
            'orderSumAmount'    => isset($data['orderSumAmount']) ? $data['orderSumAmount'] : 0, //Сумма заказа в валюте, определенной параметром запроса orderSumCurrencyPaycash.
        ];
        return;
    }
    private function checkIncomingData($actionName = 'checkOrder')
    {
        // все ли данные пришли в запросе
        // Обязаны быть: action, orderSumAmount, orderSumCurrencyPaycash, orderSumBankPaycash, shopId, invoiceId, customerNumber, md5
        // + дополнительно указанные в платежной форме publisherId

        $data = $this->incomingData;
        $conditions = [
            'action' => isset($data['action']) && ($data['action'] == $actionName),
            'md5' => isset($data['md5']) && strlen($data['md5']) == 32 ,
            'shopId' => isset($data['shopId']),
            'invoiceId' => isset($data['invoiceId']),
//            'orderCreatedDatetime' => isset($data['orderCreatedDatetime']),
            'orderSumAmount' => isset($data['orderSumAmount']),
            'orderSumCurrencyPaycash' => isset($data['orderSumCurrencyPaycash']),
            'orderSumBankPaycash' => isset($data['orderSumBankPaycash']),
            'orderNumber' => isset($data['orderNumber']),
            'customerNumber' => isset($data['customerNumber']),
            'publisherId' => isset($data['publisherId']),
        ];
        $failedFieldNames = [];
        $failedFieldNames = array_keys($conditions, false); //вернет ключи, которым соответствует значение false
        return $failedFieldNames;
    }
    private function buildAnswer($code, $message = null, $techMessage = null)
    {
        $this->answer['code'] = $code;
        
        if ($message != null)
            $this->answer['message'] = $message;
        
        if ($techMessage != null) 
            $this->answer['techMessage'] = $techMessage;
        

        if ($code != self::CODE_SUCCESS && $this->order != false)
            $this->order->transaction->updateAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => -3]); //ошибки в заказе
        
        $this->log($this->answer);
        
        return $this->answer;
    }
    public function getAnswer()
    {
        return $this->answer;
    }
    public function setAnswer(Array $answer = [])
    {
        $this->answer = $answer;
    }

    public function cancelOrder() {
        // документация здесь
        // https://tech.yandex.ru/money/doc/payment-solution/payment-notifications/payment-notifications-cancel-docpage/ 
        // Сюда попадаем когда Яндекс прислал сообщение об отказе от платежа. Это может быть при оплате в кредит, например
        // Здесь мы предполагаем, что по этому заказу яндекс уже присылал раньше запрос на подтверждение и мы уже все проверили и установили в транзакции статус =  "в процессе платежа"
        // 
        // Здесь нужно:
        // проверить md5. Для этого придется найти заказ и настройки Яндекс кассы для указанного издателя
        // изменить в транзакции заказа sys_status на -1 ('отказ платежной системы')
        //
        $this->initAnswer();
        
        $orderId = isset($this->incomingData['orderNumber']) ?  intval($this->incomingData['orderNumber']) : false;
        $publisherId = isset($this->incomingData['publisherId']) ? intval($this->incomingData['publisherId']) : false;
        
        $order = !$orderId ? $orderId : NxOrder::findOne($orderId);
        if (null == $order)
            $order = false;
        
        $paySystemOptions = RelationSystems::findOne(['publisher_id' => $publisherId, 'system_id' => self::PAY_SYSTEM_ID]);
        
        if (null == $paySystemOptions)
            $paySystemOptions = false;
        
        $secretWord = '';
        if ($order != false && $paySystemOptions != false) {
            $this->order = $order;
            $shopId = $paySystemOptions->param_1;
            $secretWord = $paySystemOptions->param_3;
        } 
        
        if (!$this->checkMd5($secretWord)) {
            return $this->buildAnswer(self::CODE_AUTH_ERR, 'md5 incorrect');
        } else {
            // md5 совпало. Это значит, что и заказ и данные яндексКассы издателя найдены и не пустые
            // установить соответствующие статусы заказу и транзакции
            //
            $order->payment_sum = 0;
            $order->order_state_id = NxOrderState::ORDER_STATE_READY;
            $order->epay_result = 'fail';
            $order->update(false);

            $transaction = $this->order->transaction;
            if (!empty($transaction)) {
                $transaction->sys_status = PaySystemStatus::STATUS_CANCELED_BY_PAYMENT_SYSTEM;
                $transaction->update(false);
            }
        }
        return $this->buildAnswer(self::CODE_SUCCESS);
    }

    public function checkOrder() {
        // ожидаемые данные согласно документации https://tech.yandex.ru/money/doc/payment-solution/payment-notifications/payment-notifications-check-docpage/        

        // requestDatetime dateTime Момент формирования запроса в Яндекс.Кассе.
        // action normalizedString, до 16 символов Тип запроса. Значение: checkOrder
        // md5 normalizedString, ровно 32 шестнадцатеричных символа, в верхнем регистре MD5-хэш параметров платежной формы (правила формирования).
        // Составные части строки для получения md5 = action;orderSumAmount;orderSumCurrencyPaycash;orderSumBankPaycash;shopId;invoiceId;customerNumber;shopPassword

        // shopId long Идентификатор магазина, выдается при подключении к Яндекс.Кассе.
        // shopArticleId long Идентификатор товара, выдается в Яндекс.Кассе.
        // invoiceId long Уникальный номер транзакции в Яндекс.Кассе.
        // orderNumber normalizedString, до 64 символов Номер заказа в системе магазина. Передается, только если был указан в платежной форме.
        // customerNumber normalizedString, до 64 символов Идентификатор плательщика на стороне магазина. Присылается в платежной форме.
        // orderCreatedDatetime dateTime Момент регистрации заказа в Яндекс.Кассе.
        // orderSumAmount CurrencyAmount Сумма заказа, присылается в платежной форме в параметре sum.
        // orderSumCurrencyPaycash CurrencyCode Код валюты для суммы заказа.
        // orderSumBankPaycash CurrencyBank Код процессингового центра в Яндекс.Кассе для суммы заказа.
        // shopSumAmount CurrencyAmount Сумма к выплате на расчетный счет магазина (сумма заказа минус комиссия Яндекс.Кассы).
        // shopSumCurrencyPaycash CurrencyCode Код валюты для shopSumAmount.
        // shopSumBankPaycash CurrencyBank Код процессингового центра Яндекс.Кассы для shopSumAmount.
        // paymentPayerCode YMAccount Номер кошелька в Яндекс.Деньгах, с которого производится оплата.
        // paymentType normalizedString Способ оплаты заказа. Коды способов оплаты (https://tech.yandex.ru/money/doc/payment-solution/reference/payment-type-codes-docpage/)

        // Необходимо 
        // 1. Проверить полученные данные на предмет полноты их наличия
        // 2. проверить полученный md5
        // 3. Найти указанный заказ, проверить, что он принадлежит указанному издателю, 
        // 4. проверить что сумма заказа, соответствует значению orderSumAmount
        // 5. в транзакции к этому заказу обновить платежную систему и статус платежной системы
        //    если все хорошо, то статус будет 1 (в процессе оплаты), если нехорошо, то статус -3 (ошибка в заказе)
        // 6. проверить данные shopId у указанного издателя
        // 7. по customerNumber найти физ лицо 
        $this->initAnswer();
        
        $failedFieldNames = $this->checkIncomingData('checkOrder');
        if (count($failedFieldNames) > 0) {
            $failedNameList = implode(',', $failedFieldNames);
            $message = 'incoming data could not been parsed';
            $techMessage = 'Following fields are incorrect or does not exist: ' . $failedNameList;
            return $this->buildAnswer(self::CODE_PARSE_REQUEST_ERR, $message, $techMessage);
        }

        // существует ли запрошенный заказ
        $oId = intval($this->incomingData['orderNumber']);
        $order = NxOrder::findOne($oId);
        if ($order == null) {
            $message = 'order does not exist';
            $techMessage = 'requested order ' . $oId . ' not found';
            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }
        $this->order = $order;
        $orderUpdateData = [
            'epay_result'       => 'fail',
            'order_state_id'    => 4,
            'payment_date'      => date('Y-m-d'),
            'payment_type_id'   => 2,
            'payment_agent_id'  => 0,
//            'payment_sum'       => 0,
            'payment_sum'       => floatval($this->incomingData['orderSumAmount']),
        ];
        //Принадлежит ли этот заказ этому издателю
        $pId = intval($this->incomingData['publisherId']);
        if ($this->order->publisher->id != $pId) {
            $message = 'discrepancy order to publisher';
            $techMessage = 'requested order ' . $oId . ' does not belong to publisher ' . $pId;

            $this->order->setAttributes($orderUpdateData, false);
            $this->order->update(false);
            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED],false);
            $this->order->transaction->update(false);
            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }
        
        // Существует ли указанное физ лицо 
        $personId = intval($this->incomingData['customerNumber']);
        $person = NxPerson::findOne($personId);
        if (null == $person) {
            $message = 'requested customer not found';
            $techMessage = 'customer  ' . $personId . ' does not exist.';

            $this->order->setAttributes($orderUpdateData, false);
            $this->order->update(false);

            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED], false);
            $this->order->transaction->update(false);

            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }
        
        // соответствует ли физ лицо в заказе тому, что прислал яндекс
        if ($order->person_id != $personId) {
            $message = 'discrepancy order to customer';
            $techMessage = 'order  ' . $oId . ' was issued to another customer.';

            $this->order->setAttributes($orderUpdateData, false);
            $this->order->update(false);

            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED], fasle);
            $this->order->transaction->update(false);

            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }
        
        // совпадают ли суммы заказа
        $sum = floatval($this->incomingData['orderSumAmount']);
        $orderSum = floatval($order->price);
        if ($orderSum != $sum) {
            $message = 'order sum incorrect';
            $techMessage = 'total sum for order  ' . $oId . ' is not ' . $sum;
            $orderUpdateData['epay_result'] = 'sum_error';

            $this->order->setAttributes($orderUpdateData,false);
            $this->order->update(false);

            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED], false);
            $this->order->transaction->update(false);

            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }  
        
        $paySystemData = RelationSystems::findOne(['publisher_id' => $pId, 'system_id' => self::PAY_SYSTEM_ID]);
        
        if ($paySystemData == null) {
            $message = 'discrepancy publisher to paying system';
            $techMessage = 'publisher ' . $pId . ' dos not support yandex kassa';

            $this->order->setAttributes($orderUpdateData,false);
            $this->order->update(false);

            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED], false);
            $this->order->transaction->update(false);

            return $this->buildAnswer(self::CODE_CANCEL_ORDER, $message, $techMessage);
        }

        $shopId = $paySystemData->param_1;
        $secretWord = $paySystemData->param_3;
        
        if (!$this->checkMd5($secretWord)) {
            $this->order->setAttributes($orderUpdateData,false);
            $this->order->update(false);

            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED], false);
            $this->order->transaction->update(false);

            return $this->buildAnswer(self::CODE_AUTH_ERR, 'md5 hash incorrect');
        }
        
        // Все ОК 
//        $transaction = $this->order->transaction;
//        $orderUpdateData['order_state_id'] = 2; // готов
        unset($orderUpdateData['order_state_id']);
        $orderUpdateData['epay_result'] = 'wait';

        $this->order->setAttributes($orderUpdateData,false);
        $this->order->update(false);

        $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_PAYMENT_IN_PROGRESS], false);
        $this->order->transaction->update(false);

        return $this->buildAnswer(self::CODE_SUCCESS);
    }

    public function paymentNotify() {
        // Данные, ожидаемые в запросе согласно документации https://tech.yandex.ru/money/doc/payment-solution/payment-notifications/payment-notifications-aviso-docpage/
        //requestDatetime dateTime Момент формирования запроса в Яндекс.Кассе.
        //paymentDatetime dateTime Момент регистрации оплаты заказа в Яндекс.Кассе.
        //action normalizedString, до 16 символов Тип запроса. Значение: paymentAviso
        //
        //md5 normalizedString, ровно 32 шестнадцатеричных символа, в верхнем регистре MD5-хэш параметров платежной формы (правила формирования)
        //shopId long Идентификатор магазина, выдается Яндекс.Кассой.
        //shopArticleId long Идентификатор товара, выдается Яндекс.Кассой.
        //invoiceId long Уникальный номер транзакции в Яндекс.Кассе.
        //orderNumber normalizedString, до 64 символов Номер заказа в системе магазина. Передается, только если был указан в платежной форме.
        //customerNumber normalizedString, до 64 символов Идентификатор плательщика (присланный в платежной форме) на стороне магазина. Например: номер договора, мобильного телефона и т. п.
        //orderCreatedDatetime dateTime Момент регистрации заказа в Яндекс.Кассе.
        //orderSumAmount CurrencyAmount Сумма заказа, присылается в платежной форме в параметре sum.
        //orderSumCurrencyPaycash CurrencyCode Код валюты для суммы заказа.
        //orderSumBankPaycash CurrencyBank Код процессингового центра Яндекс.Кассы для суммы заказа.
        //shopSumAmount CurrencyAmount Сумма к выплате на счет магазина (сумма заказа минус комиссия Яндекс.Кассы).
        //shopSumCurrencyPaycash CurrencyCode Код валюты для shopSumAmount.
        //shopSumBankPaycash CurrencyBank Код процессингового центра Яндекс.Кассы для shopSumAmount.
        //paymentPayerCode YMAccount Номер кошелька в Яндекс.Деньгах, с которого производится оплата.
        //paymentType normalizedString Способ оплаты заказа. Коды способов оплаты
        //cps_user_country_code string, 2 символа Двухбуквенный код страны плательщика в соответствии с ISO 3166-1 alpha-2. Любые названия, отличные от перечисленных выше
        //string Параметры, добавленные магазином в платежную форму.        

        //параметры ответа
        //performedDatetime dateTime Момент обработки запроса по часам в системе магазина.
        //code int Код результата обработки. Список допустимых значений 0 - Успех, 1 - ошибка авторизации, 200 - ошибка разбора запроса .
        //shopId long Идентификатор магазина. Должен дублировать поле shopId запроса.
        //invoiceId long Идентификатор транзакции в Яндекс.Кассе. Должен дублировать поле invoiceId запроса.
        //orderSumAmount CurrencyAmount Сумма заказа в валюте, определенной параметром запроса orderSumCurrencyPaycash.
        //message string, до 255 символов Текстовое пояснение в случае отказа принять платеж.
        //techMessage string, до 64 символов Дополнительное пояснение к ответу магазина. Как правило, используется как дополнительная информация об ошибках. Необязательное поле.        
        //
        // Здесь нужно проверить md5. Для этого придется найти заказ (тарнзакцию к нему само собой) и данные Яндекс кассы издателя
        // Если все хорошо, то поставить статус заказа = "Оплачен", поставить статус тразакции "Успешно оплачен сразу" и создать новую оплату в модели Payments 
        //
        $this->initAnswer();
        $this->answer['performedDatetime'] = date('Y-m-d\TH:i:s.000T:00');
        
        $failedFieldNames = $this->checkIncomingData('paymentAviso');
        if (count($failedFieldNames) > 0) {
            $failedNameList = implode(',', $failedFieldNames);
            $message = 'incoming data could not been parsed';
            $techMessage = 'Following fields are incorrect or does not exist: ' . $failedNameList;
            return $this->buildAnswer(self::CODE_PARSE_REQUEST_ERR, $message, $techMessage);
        }
        
        $orderId = intval($this->incomingData['orderNumber']);
        $publisherId = intval($this->incomingData['publisherId']);
        
        $order = NxOrder::findOne($orderId);
        $paySystemOptions = RelationSystems::findOne(['publisher_id' => $publisherId, 'system_id' => self::PAY_SYSTEM_ID]);
        $secretWord = '';
        
        if (null != $order && null != $paySystemOptions) {
            $secretWord = $paySystemOptions->param_3;
            $this->order = $order;
        }
        
        if (!$this->checkMd5($secretWord)) {
            $message = 'incorrect md5';
            return $this->buildAnswer(self::CODE_AUTH_ERR, $message);
        } 
        // сюда доберемся если md5 проверен и совпал и закзаз найден
        // Обновить статус заказа, обновить статус транзакции, создать оплату
        // TODO !! Весь нижеследующий код обновляющий (добавляющий) что-то в БД 
        // переписать с использованием транзакций
        
        $transaction = Yii::$app->db->beginTransaction();
        $this->order->setAttributes(
            [
                'order_state_id' => NxOrderState::ORDER_STATE_PAID, // оплачен
                'payment_date' => date('Y-m-d'),
                'payment_type_id' =>  PaymentType::PAYMENT_TYPE_ONLINE,
                'payment_agent_id' => 0,
                'payment_sum' => $this->incomingData['orderSumAmount'],
                'epay_result' => 'approved',
            ],false
        );

        $oUpdated =  $this->order->update(true);
         //оплачено, сегодня, электронным платежем, агент не указан, сумма
        // $oUpdated хранит affectedRowsNumber (т.е. 0 или 1)

        
//        $tUpdated = $this->order->transaction->updateAttributes(['sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY]); //оплачено
        $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY],false);
        $tUpdated = $this->order->transaction->update(true);
        // $tUpdated хранит affectedRowsNumber (0 или 1)

        $paymentData = [
            'order_id' => $order->id,
            'transaction_id' => $order->transaction->id,
            'sum' => $this->incomingData['orderSumAmount'],
            'date' => $this->answer['performedDatetime'],
            'year' => date('Y'),
            'payment_agent_id' => 47, // TEMP(!!!) Это исключительно для ООО "ОС"
            //TODO: сделать интерфейс и необходимые таблицы, где будут хранится идентификаторы платежных агентов 1С
//            'purpose' => '',
//            'comment' => '',
            'status' => Payment::STATUS_NOT_SENT_TO_1C_YET, // еще не отправлен в 1С
            'result' => serialize($this->answer),
//            'result' => '',
            'request_data' => serialize($this->incomingData),
//            'request_data' => '',
            'label' => 'transaction_payment',
        ];

//        $publisherId = $this->order->price_ != null  && !empty($this->order->price_->publisher_id) ?  $this->order->price_->publisher_id : false;
        $publisherId = false;
        try {
            $publisherId = $this->order->publisher->id;
        } catch (\Exception $e) {
            $publisherId = false;
        }

        if ( $publisherId )
            $paymentData['publisher_id'] = $publisherId;

        if ($this->order->payment != null) {
//            $pUpdated = $this->order->payment->updateAttributes($paymentData);
            $this->order->payment->setAttributes($paymentData, false);
            $pUpdated = $this->order->payment->update(true);
            // $pUpdated хранит affectedRowsNumber т.е. 0 или 1
        } else {
            $payment = new Payment();
            $payment->setAttributes($paymentData);
            $pUpdated = $payment->insert(true); // insert возвращает boolean 
        }
        
        if (boolval($oUpdated) && boolval($tUpdated) && boolval($pUpdated)) {
            $transaction->commit();
        } else {
            $oErr = print_r($this->order->getErrors(),true);
            $tErr = print_r($this->order->transaction->getErrors(), true);
            $pErr = isset($payment) ? print_r($payment->getErrors(), true) : print_r($this->order->payment->getErrors(), true);
            $logMessage = 'Yandex касса сообщила про успешную оплату заказа №' . $this->order->id
                . ', но при обновлении заказа, транзакции, платежа что-то пошло не так. Статусы НЕ БЫЛИ обновлены ' . $oErr . ', ' . $tErr . ', ' . $pErr;
            Yii::info($logMessage, self::LOG_CATEGORY);
            $transaction->rollBack();
        }

        return $this->buildAnswer(self::CODE_SUCCESS);
    }
    public function log($answer = [])
    {
        $logObj = [
            ['comment' => 'Запрос от Yandex кассы','incomingData' => $this->incomingData,],
            ['comment' => 'Ответ на запрос Yandex кассы','outcomingData' => $answer,],
            'post' => $_POST,
            'server' => $_SERVER,
        ];
//        Yii::info($logObj, 'yandexInteraction');
        $logger = Yii::getLogger();
        $logger->log($logObj,\yii\log\Logger::LEVEL_INFO, self::LOG_CATEGORY);
        return;
    }
    
    private function checkMd5($secretWord = '', $incameMd5 = false )
    {
        // Yandex формировал md по строке составленной из: action;orderSumAmount;orderSumCurrencyPaycash;orderSumBankPaycash;shopId;invoiceId;customerNumber;shopPassword
        // результат переведен в верхний регистр 
        // сформируем таким же образом свою строку md5 на основе техже данных но  секретное слово возьмем из своих источников 
        $data = $this->incomingData;
        if (!$incameMd5 && isset($this->incomingData['md5']))
            $incameMd5 = $this->incomingData['md5'];
            
        $mdStr = $data['action'] . ';' . $data['orderSumAmount'] . ';' . $data['orderSumCurrencyPaycash'] . ';' . $data['orderSumBankPaycash'] . ';' 
                . $data['shopId'] . ';' . $data['invoiceId'] .';' . $data['customerNumber'] .';' . $secretWord;
        $mdStr = strtoupper(md5($mdStr));
        return $mdStr == $incameMd5;
    }
}
