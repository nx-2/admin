<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "Edition_period".
 *
 * @property integer $id
 * @property integer $edition_id
 * @property integer $period
 * @property integer $default
 *
 * @property Edition $edition
 */
class EditionPeriod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Edition_period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['edition_id', 'period'], 'required'],
            [['edition_id', 'period', 'default'], 'integer'],
            [['edition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edition::className(), 'targetAttribute' => ['edition_id' => 'Message_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'edition_id' => 'Издание',
            'period' => 'Период (мес)',
            'default' => 'Значение по умолчанию',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this::updateAll(['default'=>0], ['edition_id'=>$this->edition_id]);
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdition()
    {
        return $this->hasOne(Edition::className(), ['Message_ID' => 'edition_id']);
    }
}
