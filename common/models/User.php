<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use common\custom_components\event_handlers\UserEventHandler;
use common\services\RbacService;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\CommonModel;
use common\models\behaviors\ImageUploadBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\linkmany\LinkManyBehavior;

/**
 * User model
 *
 * @property integer $User_ID
 * @property string $Password
 * @property integer $Checked
 * @property integer $PermissionGroup_ID
 * @property string $Language
 * @property string $Created
 * @property string $LastUpdated
 * @property string $Email
 * @property string $Login
 * @property string $FullName
 * @property string $LastName
 * @property string $FirstName
 * @property string $MidName
 * @property integer $Confirmed
 * @property string $RegistrationCode
 * @property string $Keyword
 * @property integer $isPartner
 * @property string $Auth_Hash
 * @property integer $SubscribeStatus
 * @property integer $UserStatus
 * @property string $company
 * @property string $job
 * @property string $phone
 * @property integer $country
 * @property string $city
 * @property string $address
 * @property string $street
 * @property string $house
 * @property string $building
 * @property string $flat
 * @property string $url
 * @property integer $company_profile
 * @property integer $sex
 * @property string $birthday
 * @property string $phone_prefix
 * @property string $fax_prefix
 * @property string $fax
 * @property integer $region
 * @property string $district
 * @property string $index_num
 * @property integer $open_email
 * @property string $city_other
 * @property integer $old_id
 * @property string $avatar
 * @property string $UserType
 * @property string $ForumSignature
 * @property string $ForumAvatar
 * @property string $Text
 * @property string $LastLoginDate
 * @property integer $publisher_id
 * @property string $ResetPassword
 * @property string $Password_repeat
 *
 * @property Country $country0
 * @property Publisher $publisher
 * @property UserGroup[] $userGroups
 * @property NxUserRole[] $nxUserRoles
 * @property NxRole[] $roles
 */


class User extends CommonModel implements IdentityInterface
{
    const STATUS_DELETED    = 0;
    const STATUS_ACTIVE     = 1;
    const CHECKED           = 1;
    const ADMIN_ROLE        = 1;
    const PUBLISHER         = 6;
    const ACCOUNTANT_ROLE   = 2;
    const SUBSCRIBE_MANAGER_ROLE = 3;
    const SHIP_MANAGER_ROLE = 5;
    const PASSWORD_CHANGED = 'passChanged';
    const PASSWORD_IS_LOST_EVENT_NAME = 'userPasswordIsLost';

    public static $permissions = array();
    public $avatarFile = null;
    public $Password_repeat;
    public $ResetPassword = null;

    public function init() {
        parent::init();
//        $this->on('FirstNameChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userFirstNameChanged']);
//        $this->on('LastNameChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userLastNameChanged']);
//        $this->on('MidNameChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userMidNameChanged']);
//        $this->on('EmailChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userEmailChanged']);
        $this->on('roles_Changed', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userRolesChanged']);
        $this->on('userDataChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userDataChanged']);
        $this->on('newUserCreated', ['common\custom_components\event_handlers\ARattrChangedHandler', 'newUserCreated']);
        $this->on('passwordChanged', ['common\custom_components\event_handlers\ARattrChangedHandler', 'userPasswordChanged']);
        $this->on(self::PASSWORD_IS_LOST_EVENT_NAME, [UserEventHandler::class, 'PasswordIsLostHandler']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%User}}';
    }

    public function behaviors()
    {
        $myBehaviors =  [
            [
                //multy attrs and single event. Common event for all attrs
                'class' => \common\models\behaviors\AttrChangeWatchBehavior::className(),
                'attributesList' => ['FirstName','LastName','MidName', 'Email', 'Login', 'FullName', 'company', 'job', 'city', 'address', 'street', 'house', 'building', 'flat', 'url', 'birthday', 'avatar'],
                'eventName' => 'userDataChanged',
                'number' => 2
            ],

            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user_id',
                'updatedByAttribute' => 'last_user_id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'Created',
                // 'updatedAtAttribute' => false, //это поле будет обновляться средствами БД (CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)
                'updatedAtAttribute' => 'LastUpdated',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'Created',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'LastUpdated',
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
//                'value' => gmdate("Y-m-d\TH:i:s\Z"),// current UTC timestamp
                'value' => gmdate("Y-m-d H:i:s"),// current UTC timestamp
            ],
            'uploadAvatar' => [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'avatar',
                'fileField' => 'avatarFile',
            ],
            'linkGroupBehavior' => [
                'class' =>  \common\models\behaviors\LinkManyBehavior::className(),
                'triggerEvent' => true,
                'relation' => 'roles_', // relation, which will be handled
                'relationReferenceAttribute' => 'rolesIds', // virtual attribute, which is used for related records specification
            ],

        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // ['status', 'default', 'value' => self::STATUS_ACTIVE],
            // ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['Checked', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['Checked', 'default', 'value' => self::STATUS_ACTIVE],
            [['Checked', 'Confirmed', 'country', 'company_profile', 'sex', 'region', 'open_email', 'publisher_id'], 'integer'],
            [['Created', 'LastUpdated', 'last_user_id', 'create_user_id', 'Password_repeat', 'ResetPassword', 'rolesIds','Checked'], 'safe'],
//            ['birthday','date', 'format' => 'php:d.m.Y', 'timestampAttribute' => 'birthday', 'timestampAttributeFormat' => 'php:Y-m-d 00:00:00'],
            ['birthday','date', 'format' => 'php:d.m.Y', 'timestampAttribute' => 'birthday', 'timestampAttributeFormat' => 'php:Y-m-d'],
            ['sex', 'in', 'range' => [0, 1]],
            [['birthday','sex'], 'default', 'value' => null],
            [['Auth_Hash', 'address', 'UserType', 'Text'], 'string'],
            [['Password', 'Password_repeat'], 'string', 'max' => 45],
            [['Password', 'Password_repeat'], 'required', 'on' => 'create'],
            ['Password','compare', 'compareAttribute' => 'Password_repeat', 'on' => 'create'],
//            ['Password_repeat','compare', 'compareAttribute' => 'Password', 'on' => 'update'],
//            ['ResetPassword','compare', 'compareAttribute' => 'Password_repeat', 'on' => 'update'],
            ['Password_repeat','compare', 'compareAttribute' => 'ResetPassword', 'on' => 'update'],
            ['Email', 'email'],
            [['Email','Login','publisher_id', 'LastName','FirstName','rolesIds'],'required'],
            ['Login','unique', 'on' => 'create', 'message' => 'Пользователь с таким логином уже существует.'],
            ['Email','unique', 'on' => 'create', 'message' => 'Пользователь с таким Email уже существует'],
            [['Language', 'Email', 'Login', 'FullName', 'LastName', 'FirstName', 'RegistrationCode', 'company', 'job', 'phone', 'city', 'url', 'phone_prefix', 'fax_prefix', 'fax', 'index_num', 'avatar'], 'string', 'max' => 255],
            [['MidName'], 'string', 'max' => 50],
            ['url', 'url', 'defaultScheme' => 'https'],
            [['street', 'house', 'building', 'flat', 'district'], 'string', 'max' => 64],
            [['country'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country' => 'Country_ID']],
            [['publisher_id'], 'exist', 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id'], 'message' => 'Указанный издатель отсутствует'],
            ['avatarFile', 'image', 'extensions' => 'png, jpg, gif,',
                'skipOnEmpty' => true,
                 'minWidth' => 50, 'maxWidth' => 500,
                 // 'minHeight' => 100, 'maxHeight' => 1000,
                 'message' => 'Принимаются изображения с расширением png, jpg, gif и шириной от 50 до 500 пикселов',
            ],
            [['region'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::className(), 'targetAttribute' => ['region' => 'RegionUser_ID'],'message' => ' Указанный регион отсутствует в справочнике'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_ID'               => 'ИД',
            'Password'              => 'Пароль',
            'Checked'               => 'Активен',
            'Language'              => 'Язык',
            'Created'               => 'Создан',
            'created_user_id'       => 'Создан пользователем',
            'LastUpdated'           => 'Обновлен',
            'last_user_id'          => 'Обновлен пользователем',
            'Email'                 => 'Email',
            'Login'                 => 'Логин',
            'LastName'              => 'Фамилия',
            'FirstName'             => 'Имя',
            'MidName'               => 'Отчество',
            'FullName'              => 'Полное имя',
            'Confirmed'             => 'Подтвержден',
            'RegistrationCode'      => 'Рег. код',
            'Auth_Hash'             => 'Auth  Hash',
            'company'               => 'Название компании',
            'company_profile'       => 'Вид  деятельности компании',
            'job'                   => 'Должность',
            'phone'                 => 'Телефон',
            'country'               => 'Страна',
            'city'                  => 'Город',
            'address'               => 'Адрес',
            'street'                => 'Улица',
            'house'                 => 'Дом',
            'building'              => 'Корпус',
            'flat'                  => 'Квартира',
            'url'                   => 'Сайт',
            'sex'                   => 'Пол',
            'birthday'              => 'Дата рождения',
            'phone_prefix'          => 'Тел. код',
            'fax_prefix'            => 'Код. факса',
            'fax'                   => 'Факс',
            'region'                => 'Область',
            'district'              => 'Район',
            'index_num'             => 'Почтовый индекс',
            'open_email'            => 'email для переписки',
            'avatar'                => 'Аватар',
            'avatarFile'            => 'Аватар',
            'Text'                  => 'Описание',
            'LastLoginDate'         => 'Дата посл. авторизац.',
            'publisher_id'          => 'Издатель',
            'rolesIds'              => 'Список ролей',
            'UserType'              => 'Тип',
            'UserStatus'            => 'User Status',
            'SubscribeStatus'       => 'Subscribe Status',
            'isPartner'             => 'Партнер',
            'Keyword'               => 'Keyword',
            'roles'                 => 'Роли',
            'publisher'             => 'Издатель',
            'ResetPassword'         => 'Новый пароль',
            'Password_repeat'       => 'Новый пароль повторно',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {

        $u = Yii::$app->cache->get('user_' . $id);
        if (!$u) {
            $u = static::findOne(['User_ID' => $id, 'Checked' => self::CHECKED]);
            Yii::$app->cache->set('user_' . $id,$u,180);
        }
        return $u;
//        return static::findOne(['User_ID' => $id, 'Checked' => self::CHECKED]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        // return static::findOne(['Login' => $username, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['Login' => $username, 'Checked' => self::CHECKED]);

    }

    public static function findByEmail($email)
    {
        // return static::findOne(['Login' => $username, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['Email' => $email, 'Checked' => self::CHECKED]);
    }

    public static function isExistWithAttr(string $attrName, $attrValue): bool
    {
        $result = static::find()->select(['User_ID'])->where(['Checked' => 1, $attrName => $attrValue])->asArray(true)->one();
        return $result !== NULL;
    }

    /**
     * Finds user by login and password
     *
     * @param string $login
     * @param string $password
     * @return static|null
     */
    public static function findByCredentials($login = false, $password = false)
    {
        if (!$login || !$password)
            return false;

        $password = "*" . strtoupper(sha1(sha1($password,true)));

        return static::findOne(['Login' => $login, 'Checked' => self::CHECKED, 'password' => $password]);

    }

    public static function findByPasswordResetHash($hash)
    {
         return static::findOne([
             'Auth_Hash' => $hash,
             'Checked' => self::CHECKED,
         ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        // if (!static::isPasswordResetTokenValid($token)) {
        //     return null;
        // }

        // return static::findOne([
        //     'Auth_Hash' => $token,
        //     'checked' => self::CHECKED,
        // ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');

        // if (empty($token)) {
        //     return false;
        // }

        // $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        // $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        // return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->Auth_Hash;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generatePasswordRecoveryLink()
    {
        return Yii::$app->params['homeHost'] . '/site/reset-password?hash=' . $this->getAuthKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password == "*" . strtoupper(sha1(sha1($password,true)));

        // return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        // $this->password = Yii::$app->security->generatePasswordHash($password);
        $this->Password = "*" . strtoupper(sha1(sha1($password,true)));
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {

        $this->Auth_Hash = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        // $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function isAdmin(): bool
    {
        return $this->isRole(self::ADMIN_ROLE);

        // На данный момент не ясно будут ли использоваться права пользователей,
        //описанные в таблицах User_group + PermissionGroup
        // или в таблицах nx_user_role + nx_role. Код ниже для user_group + permissionGroup если все же решим использовать их
        $result = false;
        $groups = $this->permissionGroups;
        if ($groups !== null) {
            foreach($groups as $group) {
                if ($group->PermissionGroup_ID == $adminGroupId) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }
    /**
        let us know has user at least one of requested roles
        @param array $roleId
        @return bool
    **/
    public function isRole($roleId = false)
    {
        if (!$roleId || $this === null || empty($roleId))
            return true; //не запрошена никакая роль

        $roleId = !is_array($roleId) ? [$roleId] : $roleId;

        // '@' is any authorized user
        if (in_array('@', $roleId))
            return true;

        $uId = $this->User_ID;
        $rolesString = is_array($roleId) ? implode('_',$roleId) : '' . $roleId;
        $result = Yii::$app->cache->get('is_user_' . $uId . '_has_role_' . $rolesString);
        if ($result) {
            return $result == 'YES' ? true : false;
        }

        $result = false;

        if (is_numeric($roleId[0])) {
            $fieldName = 'id';
        } elseif(is_string($roleId[0])) {
            $fieldName = 'code';
        }

        foreach($this->roles as $role) {
            if (in_array($role[$fieldName], $roleId)) {
                $result = true;
                break;
            }
        }
        $valueToCache = $result == true ? 'YES' : 'NO';
        Yii::$app->cache->set('is_user_' . $uId . '_has_role_' . $rolesString, $valueToCache, 60);
        return $result;
    }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $event = new \common\models\events\ARcreatedEvent(['model' => $this]);
            $this->trigger('newUserCreated', $event);
        }
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert){
            $this->setPassword($this->Password);
        }
        if (!empty($this->ResetPassword)) {
            $pwdResetEvent = new \common\models\events\ARsingleAttrChangedEvent(['attrName' => 'Password', 'oldValue' => $this->Password, 'newValue' => $this->ResetPassword]);
            $this->setPassword($this->ResetPassword);
            $this->trigger('passwordChanged', $pwdResetEvent);

            Yii::$app->session->addFlash('warning', 'Изменен пароль доступа. На ' . $this->Email . ' будет отправлено уведомление.');
        }
        return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $rbacService = new RbacService();
        $rbacService->removeUserAssignments(intval($this->User_ID));
    }


    /*    private function notify($type = false)
        {
            if (!$type)
                return;

            switch ($type) {
                case self::PASSWORD_CHANGED:
                    Yii::$app->mailer->compose(
                            ['html' => 'user/passChanged'],
                            ['user' => $this]
                        )
                        ->setFrom('from@domain.com')
                        ->setTo('to@domain.com')
                        ->setSubject(' Изменился пароль')
                        ->send();
                    break;

                default:
                    return;
                    break;
            }
        }*/

    // Связи с другими таблицами
    public function getPermissionGroups()
    {
        return $this
            ->hasMany(PermissionGroup::className(), ['PermissionGroup_ID' => 'PermissionGroup_ID'])
            ->viaTable('User_Group', ['User_ID' => 'User_ID']);
    }

    public function getRoles()
    {
        return $this
            ->hasMany(NxRole::className(), ['id' => 'role_id'])
            ->viaTable('nx_user_role', ['user_ID' => 'User_ID'])
            ->asArray(true);
    }

    public function getRolesPublisher()
    {
        return $this
            ->hasMany(NxRole::className(), ['id' => 'role_id'])
            ->viaTable('nx_user_role', ['user_ID' => 'User_ID'])
            ->where(['id' => 6]);
           // ->asArray(true);
    }

    public function getRoles_()
    {
        return $this
            ->hasMany(NxRole::className(), ['id' => 'role_id'])
            ->viaTable('nx_user_role', ['user_ID' => 'User_ID']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsercountry()
    {
        return $this->hasOne(Country::className(), ['Country_ID' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseregion()
    {
        return $this->hasOne(Regions::className(), ['RegionUser_ID' => 'region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        return $this->hasMany(UserGroup::className(), ['User_ID' => 'User_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxUserRoles()
    {
        return $this->hasMany(NxUserRole::className(), ['user_id' => 'User_ID']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);
    }

}
