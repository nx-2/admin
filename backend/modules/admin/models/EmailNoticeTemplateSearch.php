<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EmailNoticeTemplate;

/**
 * EmailNoticeTemplateSearch represents the model behind the search form about `common\models\EmailNoticeTemplate`.
 */
class EmailNoticeTemplateSearch extends EmailNoticeTemplate
{
    public $notice;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'notice_id', 'is_default'], 'integer'],
            [['template_file', 'data_fields','name','notice'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmailNoticeTemplate::find();
        $query->alias('ent');
        // add conditions that should always apply here
        $query->joinWith(['notice']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'template_file',
                    'name',
                    'is_default',
                    'notice_id',
                    'notice' => [
                        'asc' => ['notice.name' => SORT_ASC],
                        'desc' => ['notice.name' => SORT_DESC],
                    ]
                ],
                'route' => \yii\helpers\Url::to(['email-notice-template/index']),
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'notice_id' => $this->notice_id,
            'is_default' => $this->is_default,
        ]);

        $query->andFilterWhere(['like', 'template_file', $this->template_file])
            ->andFilterWhere(['like', 'data_fields', $this->data_fields])
            ->andFilterWhere(['like', 'ent.name', $this->name]);

        return $dataProvider;
    }
}
