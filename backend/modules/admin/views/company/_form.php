<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use backend\views\widgets\create_update_fields\CreateUpdateFields;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\NxPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id='company-form-wrapper'>
    <div class='box box-primary nx-company-form'>
        <?php 
            $action = [];
            if ($model->isNewRecord) {
                $action[] = Url::to(['company/create']);
            } else {
                $action[] = 'company/update';
                $action['id'] = $model->id;
            }
            $formOptions = [
                    'enableAjaxValidation' => true,
                    'validationUrl' => Url::to(['company/validate']),
                    // 'options' => ['data-pjax' => true],
                    'id' => 'company-form',
                    'action' => $action,
            ];
            if (isset($submitAjax))
                $formOptions['options'] = ['data-submitAjax'=> intval($submitAjax)];

        ?>
        <div class='box-header with-border'>

        </div>
        <div class='box-body'>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#company-edit-general" data-toggle="tab" aria-expanded="false">Общее</a></li>
                    <li><a href="#company-edit-address" data-toggle="tab" aria-expanded="true">Адреса</a></li>
                    <!-- <li><a href="#company-edit-person" data-toggle="tab" aria-expanded="true">Контактн. лицо</a></li> -->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="company-edit-general">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <?php $form = ActiveForm::begin($formOptions); ?>
                                    <div class="row" style='padding-top:10px;'>
                                        <?php 
                                            $fieldOptions = ['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']];
                                         ?>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'name', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'shortname', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                            
                                            <?= $form->field($model, 'othernames', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                            
                                            <?= $form->field($model, 'org_form', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'email', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'url', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                            
                                            <?= $form->field($model, 'inn', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm','data-inputmask'=>'"mask": "9","repeat" : 10', 'data-mask' => '']) ?>

                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            

                                            <?= $form->field($model, 'okonh', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm','data-inputmask'=>'"mask": "9","repeat" : 5', 'data-mask' => '']) ?>
                                            
                                            <?= $form->field($model, 'okpo', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm','data-inputmask'=>'"mask": "9","repeat" : 8', 'data-mask' => '']) ?>
                                            
                                            <?= $form->field($model, 'kpp', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm','data-inputmask'=>'"mask": "9","repeat" : 9', 'data-mask' => '']) ?>

                                            <?= $form->field($model, 'contact_person_id', [
                                                            'form' => 'company-form', 
                                                            'options' => ['class'=>'form-group field-company-contact_person_id'],
                                                            'labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px'],
                                                            ])->widget(\yii\jui\AutoComplete::classname(),
                                                                            [
                                                                                'options' => [
                                                                                    'placeholder' => 'Начните набирать...',
                                                                                    'class' => 'form-control input-sm',
                                                                                    'name' => '',
                                                                                    'id' => 'company-person-name',
                                                                                    // 'style' => 'display:inline-block;margin-right:5px;',
                                                                                    'value' => $model->isNewRecord  || $model->contactPerson === null ? '' : $model->contactPerson->name,
                                                                                ],
                                                                                'clientOptions' => [
                                                                                    'source' => Url::to(['person/autocomplete']),
                                                                                    'autoFill'=>false,
                                                                                    'minLength'=>'3',
                                                                                    'appendTo' => 'company-person-name',
                                                                                    'delay' => '300',
                                                                                    'select' => new yii\web\JsExpression('function(event, ui){$("#company-contact-person-id").val(ui.item.id);}'),
                                                                                    'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#company-contact-person-id").val("");}}'),
                                                                                    // 'disabled' => true,

                                                                                ],
                                                                            ]
                                                                        ); 
                                            ?>
                                            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'contact_person_id', $options = ['id' => 'company-contact-person-id', 'form' => 'company-form']));?>
                                            
                                            <?= $form->field($model, 'bank', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'bik', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                            <?= $form->field($model, 'pay_account', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>



                                            <?= $form->field($model, 'corr_account', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= CreateUpdateFields::widget(['model' => $model]); ?>

                                            <?= $form->field($model, 'label', $fieldOptions)->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?php if (!$model->hasErrors() && $model->isNewRecord) {$model->enabled = 1;} ?>
                                            <?= $form->field($model, 'enabled', ['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->checkbox() ?>
                                        </div>
                                        <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'description', $fieldOptions)->textarea(['rows' => 3,'class' => 'form-control input-sm']) ?>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="company-edit-address">
                        <div class="box">
                            <div class='box-body no-padding'>
                            <div style='margin:10px;min-width:300px;'>
                                <?= $form->field($model, 'address_id', ['form' => 'company-form', 'options' => ['style' => 'width:90%;display:inline-block']])->widget(AutoComplete::classname(), [
                                        'options' => [
                                            'placeholder' => 'Начните набирать...',
                                            'class' => 'form-control',
                                            'name' => '',
                                            'id' => 'company-address-name',
                                            'style' => 'display:inline-block;margin-right:5px;',
                                            'value' => $model->isNewRecord || $model->address == null ? '' : $model->address->address,
                                        ],
                                        'clientOptions' => [
                                            'source' => Url::to(['address/autocomplete']),
                                            'autoFill'=>false,
                                            'minLength'=>'3',
                                            'appendTo' => 'company-address-name',
                                            'delay' => '300',
                                            'select' => new yii\web\JsExpression('function(event, ui){$("#company-address-id").val(ui.item.id);}'),
                                            'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#company-address-id").val("");}}'),
                                            // 'disabled' => true,

                                        ],
                                ]) ?>
                                <a href='' rel='nofollow' onclick='toggleElementDisplay("#company-address-form-wrapper", "slide", 600, event)' title='не нашли в списке подходящего адреса? Нажмите и дабавьте новый'>
                                    <i class='fa fa-fw fa-envelope'></i>
                                </a>
                                <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'address_id', $options = ['id' => 'company-address-id', 'form' => 'company-form'])); ?>
                            </div>
                            <div style='margin:10px;min-width:300px;'>
                                <?= $form->field($model, 'legal_address_id', ['form' => 'company-form', 'options' => ['style' => 'width:90%;display:inline-block']])->widget(AutoComplete::classname(), [
                                        'options' => [
                                            'placeholder' => 'Начните набирать...',
                                            'class' => 'form-control',
                                            'name' => '',
                                            'id' => 'company-legal-address-name',
                                            'style' => 'display:inline-block;margin-right:5px;',
                                            'value' => $model->isNewRecord || $model->legalAddress == null ? '' : $model->legalAddress->address,
                                        ],
                                        'clientOptions' => [
                                            'source' =>  Url::to(['address/autocomplete']),
                                            'autoFill'=>false,
                                            'minLength'=>'3',
                                            'appendTo' => 'company-legal-address-name',
                                            'delay' => '300',
                                            'select' => new yii\web\JsExpression('function(event, ui){$("#company-legal-address-id").val(ui.item.id);}'),
                                            'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#company-legal-address-id").val("");}}'),
                                            // 'disabled' => true,

                                        ],
                                ]) ?>
                                <a href='' rel='nofollow' onclick='toggleElementDisplay("#company-address-form-wrapper", "slide", 600, event)' title='не нашли в списке подходящего адреса? Нажмите и дабавьте новый'>
                                    <i class='fa fa-fw fa-envelope'></i>
                                </a>
                                <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'legal_address_id', $options = ['id' => 'company-legal-address-id', 'form' => 'company-form'])); ?>
                            </div>

                            <div id='company-address-form-wrapper' style='display:none;'>
                                <?= $this->render('/address/_form', ['model' => $addressModel, 'submitAjax' => true, 'formName' => 'company-address-form']); ?>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class='tab-pane' id='company-edit-person'>
                        <div class="box">
                            <div class='box-body no-padding'>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class='box-footer'>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                <?= Html::submitButton('Сохранить комапнию', ['class' => 'btn btn-primary pull-left', 'form' => 'company-form']); ?>
                <?php if(isset($showMessage)): ?>
                    <?php 
                        switch ($showMessage['type']) {
                            case 'success':
                                $alertBg = 'bg-green';
                                $alerticon = 'fa-check';
                                break;
                            case 'error':
                                $alertBg = 'bg-orange';
                                $alerticon = 'fa-warning';
                                break;
                            case 'info':
                                $alertBg = 'bg-aqua';
                                $alerticon = 'fa-info';
                                break;
                            case 'danger':
                                $alertBg = 'dg-red';
                                $alerticon = 'fa-ban';
                                break;
                            default:
                                $alertBg = 'dg-aqua';
                                $alerticon = 'fa-info';
                                break;
                        }
                    ?>
                <div class='alert alert-dismissible pull-right <?=$alertBg; ?>' style='position:relative'>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style='position:absolute;top:2px;right:7px'>×</button>
                    <div>
                        <i class="icon fa <?=$alerticon;?>"></i>
                        <?=$showMessage['text']; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


