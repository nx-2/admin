<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\services;
use common\models\Issue;
use common\models\MagazineSubscribes;
use common\models\MagazineSubscribesQueue;
use common\models\NxOrder;
use common\models\NxOrderState;
use common\models\PaymentType;
use common\models\User;
use Yii;
use yii\rbac\ManagerInterface;

/**
 *
 * @author andrey
 * @property Issue | NULL $issue
 * @property NxOrder | NULL $order
 */
class PdfLogService {

    private $issue = NULL;
    private $order = NULL;

    public function __construct()
    {
    }

    public function updateLogsForIssue(int $issueId)
    {
        $this->issue = Issue::findOne(['Message_ID' => $issueId]);
        if (empty($this->issue) || $this->issue->status_id != Issue::STATUS_ISSUED) {
            return;
        }
        $issueLogItem = $this->getOrCreateIssueLogItem($issueId);

        $issueOrders = $this->issue->activeOrders;
        $issueOrdersIds = array_map(function ($order) {
            /** @var NxOrder $order */
            return  intval($order->id);
        }, $issueOrders);

        $orderLogItems = $issueLogItem->magazineSubscribesQueues;
        $loggedOrdersIds = array_map(function ($item) {
            /** @var MagazineSubscribesQueue $item */
            return intval($item->subscriptionID);
        }, $orderLogItems);

        $ordersIdsToAdd = array_diff($issueOrdersIds, $loggedOrdersIds);
        $ordersToAdd = array_filter($issueOrders, function ($order) use ($ordersIdsToAdd) {
            return in_array($order->id, $ordersIdsToAdd);
        });

        $dataToInsert = [];
        $columnsToInsert = [
            'subscribeID',
            'email',
            'send',
            'user_id',
            'subscriptionID',
        ];
        foreach ($ordersToAdd as $orderToAdd) {
            $dataToInsert[] = [
                intval($issueLogItem->id), // 'subscribeID'
                $orderToAdd->email, // email
                0, // send
                $orderToAdd->user_id, // user_id
                intval($orderToAdd->id), // subscriptionID
            ];
        }
        if (!empty($dataToInsert)) {
            try {
                $inserted = Yii::$app->getDb()->createCommand()->batchInsert('MagazineSubscribesQueue',$columnsToInsert, $dataToInsert)->execute();
            } catch (\Exception $e) {
                $a = 12;
                // do nothing
            }
        }
        return;
    }

    public function updateLogsForOrder(int $orderId): void
    {
        $this->order = NxOrder::findOne(['id' => $orderId]);

        if (empty($this->order) || !$this->order->isActive()) {
            return;
        }
        $orderItems = $this->order->items;
        foreach ($orderItems as $orderItem) {
            $orderItemIssues = $orderItem->itemIssues;
            foreach ($orderItemIssues as $i) {
                $this->updateLogsForIssue(intval($i->release_id));
            }
        }

    }
    /**
     * @throws \Exception
     */
    private function getOrCreateIssueLogItem(int $issueId): MagazineSubscribes
    {
        $issueLogItem = MagazineSubscribes::findOne(['issueID' => $issueId]);
        if (empty($issueLogItem)) {
            if (empty($this->issue)) {
                throw new \Exception('instanc of ' . Issue::class . ' required. NULL provided');
            }
            $issueLogItem = new MagazineSubscribes();
            $issueLogItem->issueID = intval($this->issue->Message_ID);
            $issueLogItem->magazineID = intval($this->issue->magazine->Message_ID);
            $issueLogItem->templateID = 4;
            $issueLogItem->type = 'pdf';
            if (!$issueLogItem->save()) {
                throw new \Exception(print_r($issueLogItem->getErrors(), true));
            }
        }
        return $issueLogItem;
    }

}
