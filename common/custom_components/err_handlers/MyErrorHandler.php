<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\err_handlers;

use yii\web\ErrorHandler;

class MyErrorHandler extends ErrorHandler
{

    public function handleFatalError()
    {
        $error = error_get_last();
        $error = is_array($error) && isset($error['message']) ? $error['message'] : $error;
        if (strpos($error, 'SOAP-ERROR') === false)
            parent::handleFatalError();
    }

}
