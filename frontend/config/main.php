<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    // require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
// require(__DIR__ . '/params-local.php')
);

if (file_exists( __DIR__ . '/../../common/config/params-local.php' )) {
    $params = array_merge(
        $params,
        require(__DIR__ . '/../../common/config/params-local.php')
    );
}

if (file_exists( __DIR__ . '/params-local.php' )) {
    $params = array_merge(
        $params,
        require(__DIR__ . '/params-local.php')
    );
}

return [
    'id' => 'app-frontend',
    'name' => 'API',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log','debug'],
    'language' => 'ru',
    'defaultRoute' => 'site/index',
    'aliases' => [
//            '@domain' => 'https://nx2-api.dimeo.ru',
            '@domain' => getenv('THE_SCHEME') . getenv('THE_API_HOST'),
          ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['172.18.0.29', '172.18.0.1']
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
        'migration_utility' => [
            'class' => 'c006\utility\migration\Module',
        ],
        'guarded' => [
            'class' => \frontend\modules\guarded\Module::class
        ],
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'csrfParam' => '_csrf-api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'cookieValidationKey' => 'ZrPI5vFj4mMT2GsdPSRd--u6lksE3BcR',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'enableSession' => false,
            // 'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'publisher_api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
//                 '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'company',
                    // 'pluralize' => false,
                    'patterns' => [
                        // 'PUT,PATCH {id}' => 'update',
                        // 'DELETE {id}' => 'delete',
                        'GET,HEAD {id}' => 'one',
                        // 'POST' => 'create',
                        'GET,HEAD' => 'index',
                        '{id}' => 'options',
                        '' => 'options'
                    ],
                    'extraPatterns' => [
                        'GET orders/{id}' =>  'orders',
                        // 'GET for_mag/{mag}' =>  'for-mag',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\d[\\d,]*>',
                        // '{mag}' => '<mag_id:\\d[\\d,]*>',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'issue',
                    'pluralize' => false,
                    'patterns' => [
                        // 'PUT,PATCH {id}' => 'update',
                        // 'DELETE {id}' => 'delete',
//                        'GET,HEAD {id}' => 'one',
                        'POST count/{magId}/{monthes}' => 'count',
                        'GET,HEAD' => 'index',
                        '{id}' => 'options',
                        '' => 'options'
                    ],
                    'extraPatterns' => [
                        'GET orders/{id}' =>  'orders',
                        // 'GET for_mag/{mag}' =>  'for-mag',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\d[\\d,]*>',
                        '{magId}' => '<magId:\\d[\\d,]*>',
                        '{monthes}' => '<monthes:\\d[\\d,]*>',
                        // '{mag}' => '<mag_id:\\d[\\d,]*>',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'pay',
                    'pluralize' => false,
                    'patterns' => [
                        // 'PUT,PATCH {id}' => 'update',
                        // 'DELETE {id}' => 'delete',
//                        'GET,HEAD {id}' => 'one',
                        'POST ya/checkorder' => 'ya-check-order',
                        'POST ya/notify' => 'ya-notify',
                        'GET,POST ya_api/checkorder' => 'ya-api-check-order',
                        'GET,POST by_ya_api' => 'pay-by-yandex-api',
                    ],
                    'tokens' => [
//                        '{id}' => '<id:\\d[\\d,]*>',
//                        '{magId}' => '<magId:\\d[\\d,]*>',
//                        '{monthes}' => '<monthes:\\d[\\d,]*>',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'order',
                    'pluralize' => false,
                    'patterns' => [
                        'POST update-pay-system' => 'update-payment-system',
                        'GET,POST calculate-on-data' => 'calculate-sum',
                        'POST add-order' =>  'add-order',
                        'GET,HEAD,OPTIONS is-paid/{id}' => 'is-paid'
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\d[\\d,]*>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'edition',
                    // 'pluralize' => false,
                    'patterns' => [
                        // 'PUT,PATCH {id}' => 'update',
                        // 'DELETE {id}' => 'delete',
                        'GET,HEAD {id}' => 'single',
                        'GET price/{publisher_id}/{subscriber_type_id}/{edition_id}/{delivery_type_id}/{country_id}' => 'price',
                        // 'POST' => 'create',
                        'GET,HEAD' => 'all',
                        '{id}' => 'options',
//                        '' => 'options'
                    ],
                    'tokens' => [
                        '{publisher_id}' => '<publisherId:\\d+>',
                        '{id}' => '<id:\\d+>',
                        '{subscriber_type_id}' => '<subscriberTypeId:\\d+>',
                        '{edition_id}' => '<editionId:\\d[\\d,]*>',
                        '{delivery_type_id}' => '<deliveryTypeId:\\d+>',
                        '{country_id}' => '<countryId:\\d+>',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'person',
                    'pluralize' => false,
                    'patterns' => [
                        // 'PUT,PATCH {id}' => 'update',
                        // 'DELETE {id}' => 'delete',
                        'GET,HEAD {id}' => 'one',
                        // 'POST' => 'create',
                        'GET,HEAD' => 'all',
                        '{id}' => 'options',
                        '' => 'options'
                    ],
                    'extraPatterns' => [
                        'GET orders/{id}' =>  'orders',
                        // 'GET for_mag/{mag}' =>  'for-mag',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\d[\\d,]*>',
                        // '{mag}' => '<mag_id:\\d[\\d,]*>',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'adverts',
                ],
                '<controller:multymag>/<action:js>/<pId:\d+>/<width:auto|\d+>/<showCovers:0|1>' => 'site/multy-mag-subscribe-form-js',
//                '<controller:multymag>/<action:js>' => 'site/multy-mag-subscribe-form-js',
                '<controller:subscr>/<action:js>/<pId:\d+>/<eId:\d+>' => 'site/get-subscribe-js',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ['class' => 'yii\rest\UrlRule',
                    'controller' => 'subscribe',
                    'patterns' => [
                        'GET,POST' => 'getForm',
                    ],
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST get-price' =>  'get-price',
                        'POST count-release' =>  'count-release',
                        'POST get-promo' =>  'get-promo',
                        'GET unsetPromo/{order_id}' =>  'unset-promo',
                        'GET getForm/{mag_id}/{publisher_id}' =>  'form',
                        'POST get-common-form' =>  'multy-mag-form',
                        'POST add-mag-item' =>  'add-mag-item',
                        'GET getFormOne/{issue_id}' =>  'form-one',
                        'GET getFormOne/{mag_id}/{year}/{month}' =>  'form-one',
                        'POST create-order' =>  'create-order',
                        'POST create-test' =>  'create-test',
                        'POST create-pdf' =>  'create-pdf',
                        'GET payMethod/{order_hash}/{method_pay}/{transaction_id}' =>  'pay-method',
                        'GET payAllMethod/{order_hash}/{transaction_id}' =>  'pay-all-method',
                        'POST set-pay-all' =>  'set-pay-all',
                        'GET payDoc/{order_hash}/{type_id}' =>  'pay-doc',
                        'GET saveTransaction/{order_hash}' =>  'save-transaction',

                    ],
                    'tokens' => [
                        '{customer_type_id}' => '<customer_type_id:\\d[\\d,]*>',
                        '{item_id}' => '<item_id:\\d[\\d,]*>',
                        '{promo_id}' => '<promo_id:\\w+>',
                        '{mag_id}' => '<mag_id:\\d[\\d,]*>',
                        '{order_hash}' => '<order_hash:\\w+>',
                        '{method_pay}' => '<method_pay:\\d[\\d,]*>',
                        '{period}' => '<period:\\d[\\d,]*>',
                        '{type_id}' => '<type_id:\\d[\\d,]*>',
                        '{date_start}' => '<date_start:\\w+>',
                        '{order_id}' => '<order_id:\\d[\\d,]*>',
                        '{transaction_id}' => '<transaction_id:\\d[\\d,]*>',
                        '{year}' => '<year:\\d[\\d,]*>',
                        '{month}' => '<month:\\d[\\d,]*>',
                        '{issue_id}' => '<issue_id:\\d[\\d,]*>',
                        '{publisher_id}' => '<publisher_id:\\d[\\d,]*>',

                    ],

                ],
                 '<module:[\w|\-]+>/<controller:[\w|\-]+>/<action:[\w|\-]+>' => '<module>/<controller>/<action>',
                 '<module:[\w|\-]+>/<controller:[\w|\-]+>/<action:[\w|\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            ],
        ]
        /*
        */
    ],
    'params' => $params,
];
