<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // ['InsideAdminAccess', 'isAdmin','skipOnEmpty' => false,],
            // ['User_ID', 'isAdmin', 'skipOnEmpty' => false], 
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Логин и(или) пароль не верны или запрошеный пользователь не найден.');
            }
        }
    }
    // public function isAdmin($attribute, $params) 
    // {
    //     if (!$this->hasErrors()) {
    //         $user = $this->getUser();
    //         if ( intval($user->InsideAdminAccess) != 1 ) {
    //             $this->addError($attribute, 'У вас не достаточно прав. Обратитесь к администратору.');
    //         }
    //     }
    // }
    public function isAdmin($attribute, $params) 
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ( !$user->isAdmin() ) {
                $this->addError($attribute, 'У вас не достаточно прав. Обратитесь к администратору.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            
            if ( empty($user->publisher_id)) {
                $this->addError('password', 'Данный пользователь не относится ни к одному из издателей');
                return false;
            }

            // $user->LastLoginDate = date('Y-m-d h:i:s', time());
//            $user->LastLoginDate = gmdate("Y-m-d\TH:i:s\Z");
            $user->LastLoginDate = gmdate("Y-m-d H:i:s");
            $user->Password = null;
            $user->update(false, ['LastLoginDate']);
            Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
            if ($this->_user === NULL) {
                $this->_user = User::findByEmail($this->username);
            }
        }

        return $this->_user;
    }
    
    public function attributeLabels()
    {
        return [
            'username'      => 'Логин',
            'password'      => 'Пароль',
            'rememberMe'    => 'Запомнить',
        ];
    }    
}
