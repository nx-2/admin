<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_ship_channel".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Shipment[] $nxShipments
 */
class ShipmentChanel extends \common\models\CommonModel
{
    const CHANNEL_MAP               = 1;
    const CHANNEL_COURIER           = 2;
    const CHANNEL_ENVELOPE          = 3;
    const CHANNEL_ENVELOPE_ABROAD   = 4;
    const CHANNEL_ENVELOPE_COMPANY  = 5;
    const CHANNEL_PDF               = 6;
    const CHANNEL_OFFICE            = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_ship_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipments()
    {
        return $this->hasMany(Shipment::className(), ['channel_id' => 'id']);
    }
}
