<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-index">

    <h1><?= Html::encode($this->title); ?></h1>

    <p>
        <?= Html::a('Добавить акцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'date_start',
                'value' => function($model,$key,$index,$column){
                    return date('Y-m-d',strtotime($model->date_start));
                },
                'filter' => DatePicker::widget([
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'layout' => '{input}{remove}',
                    'size' => 'xs',
//                    'convertFormat' => true,
                    'model' => $searchModel,
                    'attribute' => 'date_start',
                    'options' => [
                        'placeholder' => 'Укажите дату',
                        'value' => $searchModel->date_start,
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
                'format' => 'html',
            ],
            [
                'attribute' => 'date_end',
                'value' => function($model,$key,$index,$column){
                    return date('Y-m-d',strtotime($model->date_end));
                },
                'filter' => DatePicker::widget([
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'layout' => '{input}{remove}',
                    'size' => 'xs',
//                    'convertFormat' => true,
                    'model' => $searchModel,
                    'attribute' => 'date_end',
                    'options' => [
                        'placeholder' => 'Укажите дату',
                        'value' => $searchModel->date_end,
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
                'format' => 'html',
            ],

            'action_code',
            'name',
            // 'desc:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}',

            ],
        ],
    ]); ?>
</div>
