<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AceCodeEditorAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets-data/AceCodeEditor';
    public $css = [
        'lib/ace/css/editor.css',
        'api/resources/csses/ace_api.css',
        'widget.css',
    ];
    public $js = [
        'build/src/ace.js',
        'build/src/ext-static_highlight.js',
        'init.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
//        'backend\assets\AdminMainAsset',
    ];
    
}
