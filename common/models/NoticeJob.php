<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

class NoticeJob extends  \yii\base\Model implements \yii\queue\Job
{

    public $html;
//    public $user;
    public $theme;
    public $ResetPassword;
    public $Password;
    public $data;

    public function execute( $queue )
    {
        \Yii::$app->mailer->compose(
            ['html' => $this->html],
            ['user' =>$this->user,
                'ResetPassword'=>$this->ResetPassword,
                'Password'=>$this->Password,
                'data'=>$this->data
            ]
        )
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($this->user->Email)
            ->setSubject($this->theme)
            ->send();
    }
}