<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\event_handlers;

use common\models\EmailNotice;
use common\models\events\CustomIssueEvent;
use common\models\Issue;
use common\models\IssueDownloadFile;
use common\models\NxOrder;
use common\queue_jobs\SendPdfIssueJob;
use common\services\PdfLogService;
use Exception;
use Yii;
use yii\base\BaseObject;
use yii\base\Event;
use yii\queue\Queue;


class OrderEventHandler extends BaseObject
{
    /**
     * @param Event $event
     * @return void
     * обработчик события "hasBecomeActive" модели NXOrder
     * событие происходит когда заказ переходит в состояние, требующее выполнения обязательств по нему (отгрузки, рассылки и т.д.).
     * Относится к заказам либо оплаченным, либо установленных как безоплатные
     */
    public static function onBecomeActive(Event $event)
    {
        /** @var NxOrder $order */
        $order = $event->sender;
        $pdfLogService = new PdfLogService();
        $pdfLogService->updateLogsForOrder(intval($order->id));
    }
}