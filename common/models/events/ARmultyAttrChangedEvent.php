<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\events;

/**
 * Description of ARattrChangedEvent
 *
 * @author andrey
 */
class ARmultyAttrChangedEvent extends \yii\base\Event {
    public $attrsList=[]; //Полный Список аттрибутов, находящихся по наблюдением
    public $changedAttrsList = []; //список атрибутов фактически изменившихся
    public $oldValues = []; //старые значения атрибутов из списка $attrsList
    public $attrArray=[];//структурировано

}
