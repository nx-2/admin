<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
$homeHost = getenv('THE_SCHEME') . getenv('THE_ADMIN_HOST');
$backRootPath = realpath(__DIR__ . '/../../backend/web');
return [
    'adminEmail' => 'admin@example.com',
    'testAlias' => '@webroot/sdfd',
    'supportEmail' => 'support@example.com',
//    'noAnswerEmail' => 'no-answer-me@osp.ru',
    'noAnswerEmail' => 'info@dimeo.ru',
    'subscribeFormId' => 'mrof-ebircsbus123',
    'subscribeFormWrapperId' => 'direpparwmrofebircsbus',
//    'homeHost' => 'https://nx2-admin.dimeo.ru',
    'homeHost' => $homeHost,
    'multyMagFormId' => 'mrofgamytlum_0709',
    'multyMagFormWrapperId' => 'direpparwmrofgamytlum',

    'allPaymentMethodsWrapperId' => 'repparwsdohtemstnemyaplla',
    'user.passwordResetTokenExpire' => 3600,
    'scheme' => preg_replace('/[^h|t|p|s]/', '', getenv('THE_SCHEME')),
    'files' => [
        'Publisher' => [
            'logo' => [
                'host' => $homeHost,
                'root_path' => $backRootPath,
                'uri' => '/FileStorage/publishers',
                'resize' => [
                    ['width' => 100, 'height' => null],
                    ['width' => 50, 'height' => 50],
                ]
            ],
        ],
        'User' => [
            'avatar' => [
                'host' => $homeHost,
                'root_path' => $backRootPath,
                'uri' => '/FileStorage/users',
                'resize' => [
                    ['width' => 100, 'height' => null],
                    ['width' => 50, 'height' => 50],
                ]
            ],
        ],
        'Issue' => [
            'Picture' => [
                'host' => $homeHost,
                'root_path' => $backRootPath,
                'uri' => '/FileStorage/issue_covers',
                'resize' => [
//                    ['width' => 100, 'height' => null],
//                    ['width' => 200, 'height' => null],
                    ['width' => 300, 'height' => null],
                ]
            ],
        ],
//        'images' => [
//            'marketing_blocks' => [
//                'host' => 'https://www.osp.ru',
//                'root_path' => '/var/www/osp.loc/dev/wrk4',
//                'uri' => '/FileStorage/marketingBlocks',
//                'resize' => [
//                    ['width' => 300, 'height' => 144],
//                    ['width' => 185, 'height' => 144],
//                ]
//            ],
//            'sliders' => [
//                'host' => 'http://www.osp.loc',
//                'root_path' => '/var/www/osp.loc/dev/wrk4',
//                'uri' => '/FileStorage/sliders',
//                'resize' => [
//                    ['width' => 300, 'height' => 144],
//                    ['width' => 800, 'height' => 500],
//                ]
//            ],
//        ],
    ],
    'issueFiles' => [
//        'path' => '@app/issueFiles/',
        'path' => '@backend/issueFiles/',
    ],
//    'nxAddress'=>'https://nx2.dimeo.ru',
    'nxAddress'=> getenv('NX_HOST') ?? 'https://nx2.dimeo.ru',
    'nxApiKey' => getenv('NX_PASSPORT_API_TOKEN'),
    'api' => [
//        'host' => 'https://nx2-api.dimeo.ru', //изменить когда станет изветсно на что
        'host' => getenv('THE_SCHEME') . getenv('THE_API_HOST'), //изменить когда станет изветсно на что
        'token' => 'sdfsdfsdf',
    ],
    'admin' => [
        'schema' => getenv('THE_SCHEME'),
        'url' => getenv('THE_ADMIN_HOST'),
    ],
    'emailNoticeTestTemplatePath' => '@common/mail',
    'emailNoticeTestTemplateFile' => 'temp_test',
];

