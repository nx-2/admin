<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Issue;

/**
 * IssueSearch represents the model behind the search form about `common\models\Issue`.
 */
class IssueSearch extends Issue
{
    public $magazine;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Message_ID', 'User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Priority', 'Checked', 'Parent_Message_ID', 'last_user_id', 'MagazineID', 'Number', 'IssueID', 'status_id', 'available_online', 'no_full', 'year', 'oldID', 'showDescription', 'release_month', 'end_number', 'xpress_id', 'create_user_id', 'type', 'pdf_link_life_time'], 'integer'],
            [['Keyword', 'TimeToDelete', 'TimeToUncheck', 'IP', 'UserAgent', 'created', 'last_updated', 'LastIP', 'LastUserAgent', 'Name', 'StartDate', 'PublicDate', 'Picture', 'annotation', 'issue_title', 'inCompositionDate', 'ncTitle', 'ncKeywords', 'ncDescription', 'subscription_code','magazine'], 'safe'],
            [['ArticlePrice'], 'number'],
        ];
    }

    public function behaviors()
    {
        return [];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $pId = Yii::$app->user->identity->publisher_id;
        $publisher = \common\models\Publisher::findOne($pId);
        $ids = $publisher->getMyEditionsIds();

        $query = Issue::find()->where(['MagazineID' => $ids]);
        $query->joinWith(['magazine']);
        // $query->orderBy('PublicDate DESC');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['PublicDate'=>SORT_DESC],
                'attributes' => [
                    'Message_ID',
                    'type',
                    'Name',
                    'issue_title',
                    'status_id',
                    'PublicDate',
                    'StartDate',
                    'year',
                    'release_month',
                    'Number',
                    'magazine' => [
                        'asc' => ['Edition.HumanizedName' => SORT_ASC],
                        'desc' => ['Edition.HumanizedName' => SORT_DESC],
                    ],
                ],
                'route' => \yii\helpers\Url::to(['issue/index']),
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
            
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Message_ID' => $this->Message_ID,
            'User_ID' => $this->User_ID,
            'Subdivision_ID' => $this->Subdivision_ID,
            'Sub_Class_ID' => $this->Sub_Class_ID,
            'Priority' => $this->Priority,
            'Checked' => $this->Checked,
            'TimeToDelete' => $this->TimeToDelete,
            'TimeToUncheck' => $this->TimeToUncheck,
            'Parent_Message_ID' => $this->Parent_Message_ID,
            'created' => $this->created,
            'last_updated' => $this->last_updated,
            'last_user_id' => $this->last_user_id,
            'MagazineID' => $this->MagazineID,
            'StartDate' => $this->StartDate,
            'Number' => $this->Number,
            'PublicDate' => $this->PublicDate,
            'IssueID' => $this->IssueID,
            'status_id' => $this->status_id,
            'available_online' => $this->available_online,
            'no_full' => $this->no_full,
            'year' => $this->year,
            'oldID' => $this->oldID,
            'inCompositionDate' => $this->inCompositionDate,
            'showDescription' => $this->showDescription,
            'ArticlePrice' => $this->ArticlePrice,
            'release_month' => $this->release_month,
            'end_number' => $this->end_number,
            'xpress_id' => $this->xpress_id,
            'create_user_id' => $this->create_user_id,
            'type' => $this->type,
            'pdf_link_life_time' => $this->pdf_link_life_time,
        ]);

        $query->andFilterWhere(['like', 'Keyword', $this->Keyword])
            ->andFilterWhere(['like', 'IP', $this->IP])
            ->andFilterWhere(['like', 'UserAgent', $this->UserAgent])
            ->andFilterWhere(['like', 'LastIP', $this->LastIP])
            ->andFilterWhere(['like', 'LastUserAgent', $this->LastUserAgent])
            ->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'Picture', $this->Picture])
            ->andFilterWhere(['like', 'annotation', $this->annotation])
            ->andFilterWhere(['like', 'issue_title', $this->issue_title])
            ->andFilterWhere(['like', 'ncTitle', $this->ncTitle])
            ->andFilterWhere(['like', 'ncKeywords', $this->ncKeywords])
            ->andFilterWhere(['like', 'ncDescription', $this->ncDescription])
            ->andFilterWhere(['like', 'Edition.HumanizedName', $this->magazine])
            ->andFilterWhere(['like', 'subscription_code', $this->subscription_code]);

        return $dataProvider;
    }
}
