<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\NxPriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Прайсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nx-price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить прайс-лист', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php 
    Pjax::begin(['id' => 'prices-list', 'enablePushState' => true]); 
//    Pjax::begin(['enablePushState' => false]); 
?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'id',
                'filterInputOptions' => ['style' => 'width:80px','class' => 'form-control'],
                'filterOptions' => ['style' => 'width:97px'],
            ],
//            'publisher_id',
//            'item_type',
            'name',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'customer_type_id',
                'content' => function($model,$key,$index,$column){
                    switch ($model->customer_type_id) {
                        case 0:
                            return 'Текущая';
                            break;
                        case 1:
                            return 'Физ.лица';
                            break;
                        case 2:
                            return 'Юр.лица';
                            break;
                    }
                },
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'filter' => ['1' => 'физ.лица', '2' => 'юр.лица','0' => 'текущий'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%;padding:6px 0px', 'class' => 'form-control'],
            ],
//            'customer_type_id',
//             'date',
            [
                'attribute' => 'date',
                'filterInputOptions' => ['placeholder' => 'ГГГГ-ММ-ДД','class' => 'form-control'],
            ],
//             'checked',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'checked',
                'content' => function ($model, $key, $index, $column) {
                    $checkbox = Html::checkbox('', (Boolean)$model->checked, [
                        'id' => 'checked_' . $model->id,
                        'value' => $model->checked,
                        // 'disabled' => true,
                        'onclick' => 'enabledClicked(event, "Price","checked")',
                    ]);
                    $label = Html::label('', 'checked_' . $model->id, ['class' => 'radio-check-label'], '');
                    $ajaxLoader = Html::tag('div', '', ['class' => 'ajax-load']);
                    $content = $ajaxLoader . $checkbox . $label;
                    $result = Html::tag('div', $content, ['class' => 'radio-check-wrap']);

                    return $result;
                },
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'filter' => ['1' => 'вкл', '0' => 'выкл'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
            ],




            
            // 'create_user_id',
            // 'last_user_id',
            // 'created',
            // 'last_updated',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = '/price/clone?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'Создать новый прайс на основе данного',
                                'aria-label' => 'Клонировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-clone fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = '/price/update?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать прайс',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            '/price/delete?id=' . $key,
                            [
                                'title' => 'Удалить прайс',
                                'aria-label' => 'Удалить',
                                'data-confirm' => 'Вы уверены, что хотите удалить этот прайс?',
                                'data-method' => 'post',
                                'data-pjax' => '#prices-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
            
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
