<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EmaiLog;

/**
 * EmailLogSearch represents the model behind the search form about `\common\models\EmaiLog`.
 */
class EmailLogSearch extends EmaiLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'success'], 'integer'],
            [['from', 'to', 'subject', 'body', 'time', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmaiLog::find();

        // add conditions that should always apply here

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'success' => $this->success,
//            'time' => $this->time,
        ]);
        if (isset(Yii::$app->user) && null !== Yii::$app->user->identity) {
            $query->andFilterWhere([
                'publisher_id' => Yii::$app->user->identity->publisher->id,
            ]);
        } else {
            $query->andWhere('publisher_id IS NULL');
        }

        $query->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'to', $this->to])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
