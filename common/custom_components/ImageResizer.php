<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;


class ImageResizer extends Component
{
    /**
     * @param string $imageName - full path to origin image file, whick should be resized
     * @param array() $sizes - list of arrays of required sizes ['width' => int, 'height' => int]
     * @param Boolean $clearOld - flag, to know is deleting of all existing resized files is required
     * @param Boolean $overWrite - flag, to know is overwrite old file if it is exists
     * @return Array() - list of strings with full pathes of resized image files
     **/
    public function generateRisizedImages($imageName = false, $sizes = [], $clearOld = false, $overWrite = false)
    {
       
        if (!$imageName || !file_exists($imageName))
            return [];
        
        $imageFileData = pathinfo($imageName);
        
        $resizedImagesFolder = $imageFileData['dirname'] . '/' . 'resized/';
        $toReturn = [];
        
        foreach($sizes as $size) {
            if (!is_array($size) || (!isset($size['width']) && !isset($size['height'])) )
                continue;
            $w = $size['width'] !== null ? $size['width'] : '_';
            $h = $size['height'] !== null ? $size['height'] : '_';
            $s = '_size(' . $w . 'x' . $h . ')';
            $resizedFileName = $imageFileData['filename'] . $s . '.' . $imageFileData['extension'];
            if (is_dir($resizedImagesFolder) && $clearOld) {
                $command = 'rm -rf ' . escapeshellarg($resizedImagesFolder);
                $r =  system($command, $r);
            }

            if (!is_dir($resizedImagesFolder)) 
                mkdir ($resizedImagesFolder, 0755);
            

            $resizedFileFullPath = $resizedImagesFolder . $resizedFileName;

            if(file_exists($resizedFileFullPath) && !$overWrite)
                continue;

            $img = Image::getImagine()->open($imageName);

            $currentSize = $img->getSize();

            if ($size['width'] === null || $size['height'] === null) {
                $w_h = $currentSize->getWidth() / $currentSize->getHeight();
                $size['height'] = $size['height'] === null ? $size['width'] / $w_h : $size['height'];
                $size['width'] = $size['width'] === null ? $size['height'] * $w_h : $size['width'];
            }
            
            $k_w =  $currentSize->getWidth() / $size['width'];
            $k_h = $currentSize->getHeight() / $size['height'];

            $k = min([$k_w, $k_h]);

            $sizeForResize = new Box($currentSize->getWidth() / $k, $currentSize->getHeight() / $k);
            $sizeForCrop = new Box($size['width'], $size['height']);
            // print_r($size);die($k);
            $x = ($sizeForResize->getWidth() - $size['width']) / 2;
            $y = ($sizeForResize->getHeight() - $size['height']) / 2;
            $x = $x < 0 ? 0 : $x;
            $y = $y < 0 ? 0 : $y;
            $pointForCrop = new Point( $x, $y);

            $img->resize($sizeForResize)->crop($pointForCrop, $sizeForCrop)->save($resizedFileFullPath);
            
            if (file_exists($resizedFileFullPath))
                $toReturn [] = $resizedFileFullPath;
        }
        return $toReturn;
    }

    /**
    * @param string - full path to image file
    * @param string - imageUrl
    * @return array - list of arrays 
    * Принимает полный путь к файлу изображения и url изображения.
    * ожидается что в той же папке, где находится этот файл имеется папка resized внутри которой лежат 
    * файлы различного размера с именами типа originImageFileName_size(300x144).jpg 
    * возвращает список массивов типа 
    [
        width  => 300,
        height => 144,
        url => 'blah-blah/blah/blah.img',
        path => 'dfdfgfd/dfdfg/dfgfd',
     ]
     **/
    public function getResizedImages($fileUrl = false, $fullFileName = false)
    {
        $toReturn = array();

        if (!$fileUrl || empty($fileUrl) || !$fullFileName || empty($fullFileName) || !file_exists($fullFileName))
            return array();

        if (!is_dir( $resizedDir = pathinfo($fullFileName, PATHINFO_DIRNAME) . '/resized' ))
            return array();

        $urlPart = preg_replace('/\.jpg|png|gif/', '', trim($fileUrl, '/'));
        $urlPart = explode('/', $urlPart);
        if (count($urlPart) > 0)
            unset($urlPart[count($urlPart) - 1]);

        $urlPart = implode('/', $urlPart) . '/resized/';
        
        foreach (scandir($resizedDir) as $fileName) {
            $sizeParts = [];
            preg_match('/_size\(.+\)/', $fileName, $sizeParts);
            
            if (count($sizeParts) == 0) 
                continue;
            
            $sizeParts = $sizeParts[0];
            $sizeParts = str_replace('_size(', '', $sizeParts);
            $sizeParts = str_replace(')', '', $sizeParts);
            $sizeParts = explode('x', $sizeParts);
            $w = isset($sizeParts[0]) ? $sizeParts[0] : false;
            $h = isset($sizeParts[1]) ? $sizeParts[1] : false;
            $f = [];
            if ($w && $h) {
                $f['width'] = $w;
                $f['height'] = $h;
                $f['url'] = $urlPart . $fileName;
                $f['path'] = $fullFileName;
                $toReturn [] = $f;
            } 
        }

        return $toReturn;
    }
}