<?php

use yii\db\Migration;

/**
 * Handles the creation of table `adsplaces`.
 */
class m170112_092030_create_adsplaces_table extends Migration
{
    public function init()
    {
          $this->db = 'ads_db';
          parent::init();
    }    
    /**
     * @inheritdoc
     */
    public function up()
    {
        // $tables = Yii::$app->db->schema->getTableNames();
        $tables = $this->db->schema->getTableNames();

        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('ads_places', $tables))  { 
        if ($dbType == "mysql") {
            $this->createTable('{{%ads_places}}', [
                'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                0 => 'PRIMARY KEY (`id`)',
                'magazine_id' => 'INT(11) NOT NULL',
                'name' => 'VARCHAR(45) NULL',
                'description' => 'VARCHAR(255) NULL',
                'enabled' => 'TINYINT(1) NOT NULL DEFAULT \'1\'',
            ], $tableOptions_mysql);
        }
        }
         
         
        $this->createIndex('fk_ads_places_magazines_idx','ads_places','magazine_id',0);
         
        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_ads_places_magazines','{{%ads_places}}', 'magazine_id', 'osp.Edition', 'Message_ID', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `ads_places`');
        $this->execute('SET foreign_key_checks = 1;');
    }
}
