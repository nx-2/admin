<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $data Array */
?>


<div id='person-update-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="person-update-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="person-update-label">Создать новое физлицо</h4>
            </div>
            <div class="modal-body">
                <?=$this->render('/person/_form',['model' => $personModel, 'addressModel' => $addressModel,'submitAjax' => true]); ?>
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
            <!-- 
            <div class="modal-footer">
            </div>
             -->
        </div>
    </div>
</div>
