<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = 'Обновить компанию: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['company/index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="company-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
        $data = ['model' => $model, 'addressModel' => $addressModel];
        if (isset($showMessage))
        	$data ['showMessage']  = $showMessage;
    ?>

    <?= $this->render('_form', $data); ?>

</div>
