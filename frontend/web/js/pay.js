/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

var jqueryExists = (typeof(jQuery) == 'function') ? true : false;

if (!jqueryExists) {
        var script = document.createElement('script');

        script.type = 'text/javascript';
        script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

        document.getElementsByTagName('head')[0].appendChild(script);

        script.addEventListener('load', runAfterJqueryLoad, false);
} else {
        runAfterJqueryLoad();
}
function runAfterJqueryLoad(){
    $(document).ready(function() {
        $(".tabs-menu__ a").click(function(event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
        });
    });
}
