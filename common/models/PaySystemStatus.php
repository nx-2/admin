<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use \common\models\CommonModel;


/**
 * This is the model class for table "pay_system_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $comment
 *
 * @property Transactions[] $transactions
 */
class PaySystemStatus extends CommonModel
{
    const STATUS_CANCELED                           = -3; // Отказано в оплате по нашей инициативе
    const STATUS_CANCELED_BY_PAYMENT_SYSTEM         = -1; // Оплата НЕ была успешной на стороне платежной системы
    const STATUS_INITIAL                            =  0; // начальное состояние
    const STATUS_PAYMENT_IN_PROGRESS                =  1; // после проверки заказа по запросу платежной системы в процессе оплаты этого заказа
    const STATUS_PAID_IMMEDIATELY                   =  2; // после подтверждения платежной системой успешного платежа
    const STATUS_PAID_ON_REQUEST_TO_PAYMENT_SYSTEM  =  3; // после того как платежная система ответила на наш запрос что платеж успешный

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_system_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['comment'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'comment' => 'Comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transactions::className(), ['sys_status' => 'id']);
    }
}
