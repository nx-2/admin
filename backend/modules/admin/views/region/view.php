<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Regions */

$this->title = $model->RegionUser_ID;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['region/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['region/update', 'id' => $model->RegionUser_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['region/delete', 'id' => $model->RegionUser_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'RegionUser_ID',
            'RegionUser_Name',
            'RegionUser_Priority',
            'Value:ntext',
            'enabled',
        ],
    ]) ?>

</div>
