<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NxAddress;
use yii\helpers\Url;

/**
 * AddressSearch represents the model behind the search form about `common\models\NxAddress`.
 */
class AddressSearch extends NxAddress
{
    public $country;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'is_onclaim', 'is_enveloped', 'xpress_id'], 'integer'],
            [['area', 'region', 'city', 'city1', 'zipcode', 'address', 'street', 'house', 'building', 'structure', 'apartment', 'phone', 'fax', 'comment', 'telcode', 'post_box', 'last_updated', 'label','country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NxAddress::find();
        $query->joinWith(['country']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'country' => [
                        'asc' => ['Classificator_Country.Country_Name' => SORT_ASC],
                        'desc' => ['Classificator_Country.Country_Name' => SORT_DESC],
                    ],
                    'city',
                    'zipcode',
                    'address',
                    'street',
                    'house',
                    'building',
                    'phone',
                ],
                'route' => Url::to(['address/index']),
            ],
        ]);



        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'is_onclaim' => $this->is_onclaim,
            'is_enveloped' => $this->is_enveloped,
            'last_updated' => $this->last_updated,
            'xpress_id' => $this->xpress_id,
        ]);

        $query->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'city1', $this->city1])
            ->andFilterWhere(['like', 'zipcode', $this->zipcode])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'building', $this->building])
            ->andFilterWhere(['like', 'structure', $this->structure])
            ->andFilterWhere(['like', 'apartment', $this->apartment])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'telcode', $this->telcode])
            ->andFilterWhere(['like', 'post_box', $this->post_box])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'Classificator_Country.Country_Name', $this->country]);

        return $dataProvider;
    }
}
