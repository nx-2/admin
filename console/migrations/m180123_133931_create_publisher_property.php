<?php

use yii\db\Migration;

/**
 * Class m180123_133931_create_publisher_property
 */
class m180123_133931_create_publisher_property extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $sqlCode = "
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
            ALTER TABLE `publisher_property_type` 
            DROP FOREIGN KEY `fk_publisher_property_type_publisher_property_group1`;
            
            CREATE TABLE IF NOT EXISTS `publisher_property` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `publisher_id` INT(11) NOT NULL,
              `property_type_id` INT(11) NOT NULL,
              `value` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `publisher_type` (`publisher_id` ASC, `property_type_id` ASC),
              INDEX `fk_publisher_property_publisher_property_type1_idx` (`property_type_id` ASC),
              CONSTRAINT `fk_publisher_property_publisher1`
                FOREIGN KEY (`publisher_id`)
                REFERENCES `publisher` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_publisher_property_publisher_property_type1`
                FOREIGN KEY (`property_type_id`)
                REFERENCES `publisher_property_type` (`id`)
                ON DELETE RESTRICT
                ON UPDATE CASCADE)
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8;
            
            ALTER TABLE `publisher_property_type` 
            ADD CONSTRAINT `fk_publisher_property_type_publisher_property_group1`
              FOREIGN KEY (`group_id`)
              REFERENCES `publisher_property_group` (`id`)
              ON DELETE RESTRICT
              ON UPDATE CASCADE;
            
            
            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;        
        ";
        $this->execute($sqlCode);

    }

    public function down()
    {
        echo "m180123_133931_create_publisher_property cannot be reverted.\n";

        return false;
    }
}
