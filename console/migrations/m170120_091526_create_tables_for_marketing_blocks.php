<?php

use yii\db\Migration;

class m170120_091526_create_tables_for_marketing_blocks extends Migration
{
    public function up()
    {
        $code = '
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=\'TRADITIONAL,ALLOW_INVALID_DATES\';

            CREATE TABLE IF NOT EXISTS `ads`.`market_block_zones` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(50) NOT NULL,
              `keyword` VARCHAR(50) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `keyword_UNIQUE` (`keyword` ASC))
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8
            COLLATE = utf8_general_ci
            COMMENT = \'Зоны(площадки) для маркетинговых блоков\';

            CREATE TABLE IF NOT EXISTS `ads`.`market_blocks` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NOT NULL COMMENT \'заголовок блока\',
              `teaser` VARCHAR(255) NULL DEFAULT NULL COMMENT \'текст, который появляется при наведении мыши\',
              `text` TEXT NULL DEFAULT NULL COMMENT \'всегда видимое текстовое сообщение\n\',
              `zeropixel` VARCHAR(100) NULL DEFAULT NULL COMMENT \'ссылка для статистики показов\',
              `link` VARCHAR(100) NULL DEFAULT NULL COMMENT \'кликовая ссылка для перехода\',
              `image` VARCHAR(255) NULL DEFAULT NULL,
              `go_to_new` TINYINT(1) NULL DEFAULT 0 COMMENT \'открывать ссылку в новом окне или в этом же\',
              PRIMARY KEY (`id`))
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8mb4
            COMMENT = \'Маркетинговые блоки\';

            CREATE TABLE IF NOT EXISTS `ads`.`market_blocks_to_zones` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `zone_id` INT(11) NOT NULL,
              `market_block_id` INT(11) NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_to_zones_idx` (`zone_id` ASC),
              INDEX `fk_to__market_blocks_idx` (`market_block_id` ASC),
              CONSTRAINT `fk_to_zones`
                FOREIGN KEY (`zone_id`)
                REFERENCES `ads`.`market_block_zones` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_to_market_blocks`
                FOREIGN KEY (`market_block_id`)
                REFERENCES `ads`.`market_blocks` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8
            COLLATE = utf8_general_ci
            COMMENT = \'Связующаая таблица для отношения многие ко многим\';


            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
       ';
        $this->execute($code);
    }

    public function down()
    {
        $code = '
          DROP TABLE IF EXISTS `ads`.`market_blocks_to_zones`;
          DROP TABLE IF EXISTS `ads`.`market_blocks`;
          DROP TABLE IF EXISTS `ads`.`market_block_zones`;
        ';
        $this->execute($code);
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
