<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $data Array */
?>
<?php //echo($data['name']);?>

<div id='company-info-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="company-info-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="company-info-label"><?=$data['name']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#company-info-general" data-toggle="tab" aria-expanded="false">Общее</a></li>
                        <?php if(!empty($data['address'])): ?>
                            <li><a href="#company-info-address" data-toggle="tab" aria-expanded="true">Адрес</a></li>
                        <?php endif;?>
                        <?php if(!empty($data['legalAddress'])): ?>
                            <li><a href="#company-info-legal-address" data-toggle="tab" aria-expanded="true">Юридический адрес</a></li>
                        <?php endif;?>
                        <?php if(!empty($data['contactPerson'])): ?>
                            <li><a href="#company-info-person" data-toggle="tab">Контактн. лицо</a></li>
                        <?php endif;?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="company-info-general">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['general'] as $name => $label):?>
                                                <?php if(!empty($data[$name]) && !is_array($data[$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'><?=$label; ?></th>
                                                        <td><?=$data[$name];?></td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($data['address'])): ?>
                        <div class="tab-pane" id="company-info-address">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['address'] as $name => $label):?>
                                                <?php if(!empty($data['address'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['address'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($data['contactPerson'])): ?>
                        <div class="tab-pane" id="company-info-person">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['person'] as $name => $label):?>
                                                <?php if(!empty($data['contactPerson'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['contactPerson'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($data['legalAddress'])): ?>
                        <div class="tab-pane" id="company-info-legal-address">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['address'] as $name => $label):?>
                                                <?php if(!empty($data['legalAddress'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['legalAddress'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
