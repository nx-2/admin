<?php

use yii\db\Migration;

/**
 * Class m180123_133705_create_publisher_property_group
 */
class m180123_133705_create_publisher_property_group extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $sqlCode = "
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
            CREATE TABLE IF NOT EXISTS `publisher_property_group` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `group_name` VARCHAR(100) NOT NULL,
              `icon-name` VARCHAR(45) NULL DEFAULT '' COMMENT 'font awesome icon name',
              PRIMARY KEY (`id`))
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8;
            ALTER TABLE `nxnew`.`publisher_property_type` 
            ADD CONSTRAINT `fk_publisher_property_type_publisher_property_group1`
              FOREIGN KEY (`group_id`)
              REFERENCES `nxnew`.`publisher_property_group` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        ";

        $this->execute($sqlCode);
    }

    public function down()
    {
        echo "m180123_133705_create_publisher_property_group cannot be reverted.\n";

        return false;
    }
}
