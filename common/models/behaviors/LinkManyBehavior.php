<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\behaviors;
class LinkManyBehavior extends \yii2tech\ar\linkmany\LinkManyBehavior
{
    public $triggerEvent = false;
    
    public function afterSave($event)
    {
        if (!$this->getIsRelationReferenceAttributeValueInitialized()) {
            return;
        }


        $linkModels = [];
        $unlinkModels = [];
        $oldRelatedModels = [];

        $newReferences = array_unique($this->getRelationReferenceAttributeValue());
        foreach ($this->owner->{$this->relation} as $relatedModel) {
            /* @var $relatedModel ActiveRecordInterface */
            if ($this->triggerEvent)
                $oldRelatedModels [] = $relatedModel;//Запомнить старые модели для передачи в событие
            
            $primaryKey = $this->normalizePrimaryKey($relatedModel->getPrimaryKey());
            if (($primaryKeyPosition = array_search($primaryKey, $newReferences)) === false) {
                $unlinkModels[] = $relatedModel;
            } else {
                unset($newReferences[$primaryKeyPosition]);
            }
        }

        if (!empty($newReferences)) {
            $relation = $this->owner->getRelation($this->relation);
            /* @var $relatedClass ActiveRecordInterface */
            $relatedClass = $relation->modelClass;
            $linkModels = $relatedClass::findAll(array_values($newReferences));
        }

        $removeAttr = $unlinkModels;
        $addAttr = $linkModels;

        foreach ($unlinkModels as $model) {
            $this->owner->unlink($this->relation, $model, $this->deleteOnUnlink);
        }

        foreach ($linkModels as $model) {
            $this->owner->link($this->relation, $model, $this->composeLinkExtraColumns($model));
        }

        if ((!empty($unlinkModels) || !empty($linkModels)) && $this->triggerEvent) {
            $event = new \common\models\events\ARrelatedModelChangedEvent();
            $event->relationName = $this->relation;
            $event->oldRelatedModels = $oldRelatedModels;
            $this->owner->trigger($this->relation . 'Changed', $event);
        }
    }

}