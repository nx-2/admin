<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "MagazineSubscribes".
 *
 * @property int $id
 * @property int $magazineID
 * @property int $issueID
 * @property int $templateID
 * @property string $type
 * @property int $sended
 * @property int $queue
 * @property string $created
 *
 * @property Issue $issue
 * @property Edition $magazine
 * @property AecmsFileStorage $issue0
 * @property MagazineSubscribesQueue[] $magazineSubscribesQueues
 */
class MagazineSubscribes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'MagazineSubscribes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['magazineID', 'issueID', 'templateID', 'sended', 'queue'], 'integer'],
            [['type'], 'string'],
            [['created'], 'safe'],
            [['issueID'], 'exist', 'skipOnError' => true, 'targetClass' => Issue::class, 'targetAttribute' => ['issueID' => 'Message_ID']],
            [['magazineID'], 'exist', 'skipOnError' => true, 'targetClass' => Edition::class, 'targetAttribute' => ['magazineID' => 'Message_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'magazineID' => 'ID издания',
            'issueID' => 'ID выпуска',
            'templateID' => 'ID шаблона',
            'type' => 'Тип',
            'sended' => 'Отправлен',
            'queue' => 'В очереди',
            'created' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssue(): ActiveQuery
    {
        return $this->hasOne(Issue::class, ['Message_ID' => 'issueID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazine(): ActiveQuery
    {
        return $this->hasOne(Edition::class, ['Message_ID' => 'magazineID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMagazineSubscribesQueues(): ActiveQuery
    {
        return $this->hasMany(MagazineSubscribesQueue::class, ['subscribeID' => 'id']);
    }
}
