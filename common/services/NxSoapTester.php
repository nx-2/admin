<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\services;
use common\models\Integration1cVerify;
use common\services\interfaces\SoapServiceTesterI;
use Yii;
use yii\helpers\ArrayHelper;

class NxSoapTester implements SoapServiceTesterI

{
    public $service  = null;
    private $functionsList = [
        [
            'type' => 'PaymentFromNxTo1CResponse',
            'name' => 'PaymentFromNxTo1C',
            'parameters' => ['PaymentFromNxTo1C']
        ],
        [
            'type' => 'PaymentOrderFromNxResponse',
            'name' => 'PaymentOrderFromNx',
            'parameters' => ['PaymentOrderFromNx']
        ],
        [
            'type' => 'SaleFromNXResponse',
            'name' => 'SaleFromNX',
            'parameters' => ['SaleFromNX']
        ],
        [
            'type' => 'ContractorsResponse',
            'name' => 'Contractors',
            'parameters' => ['Contractors']
        ],
        [
            'type' => 'ContactsResponse',
            'name' => 'Contacts',
            'parameters' => ['Contacts']
        ],
        [
            'type' => 'PaymentFromNxTo1CResponse',
            'name' => 'PaymentFromNxTo1C',
            'parameters' => ['PaymentFromNxTo1C']
        ],
        [
            'type' => 'PaymentOrderFromNxResponse',
            'name' => 'PaymentOrderFromNx',
            'parameters' => ['PaymentOrderFromNx']
        ],
        [
            'type' => 'SaleFromNXResponse',
            'name' => 'SaleFromNX',
            'parameters' => ['SaleFromNX']
        ],
        [
            'type' => 'ContractorsResponse',
            'name' => 'Contractors',
            'parameters' => ['Contractors']
        ],
        [
            'type' => 'ContactsResponse',
            'name' => 'Contacts',
            'parameters' => ['Contacts']
        ],
    ];
    private $typesList = [
        [
            'name' => 'Acquirer',
            'typeData' => [
                [
                    'name' => 'NxID',
                    'type' => 'integer',
                ]
            ],
        ],
        [
            'name' => 'Contact',
            'typeData' => [
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'LegalAddr',
                    'type' => 'string',
                ],
                [
                    'name' => 'ActualAddr',
                    'type' => 'string',
                ],
                [
                    'name' => 'Phone',
                    'type' => 'Phone',
                ],
            ],
        ],
        [
            'name' => 'ContactResponse',
            'typeData' => [
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'IsError',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Decoding',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'ContactsList',
            'typeData' => [
                [
                    'name' => 'Contact',
                    'type' => 'Contact',
                ],
            ],
        ],
        [
            'name' => 'ContactsResponse',
            'typeData' => [
                [
                    'name' => 'КонтактОтвет',
                    'type' => 'ContactResponse',
                ],
            ],
        ],
        [
            'name' => 'Contractor',
            'typeData' => [
                [
                    'name' => 'INN',
                    'type' => 'string',
                ],
                [
                    'name' => 'KPP',
                    'type' => 'string',
                ],
                [
                    'name' => 'Name',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'DocSale',
            'typeData' => [
                [
                    'name' => 'document_date',
                    'type' => 'dateTime',
                ],
                [
                    'name' => 'SaleId',
                    'type' => 'string',
                ],
                [
                    'name' => 'Organization',
                    'type' => 'Organization',
                ],
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'DocSaleRow',
                    'type' => 'DocSaleRow',
                ],
                [
                    'name' => 'Update',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'payment_date',
                    'type' => 'date',
                ],
                [
                    'name' => 'OnlyBill',
                    'type' => 'boolean',
                ],
            ],
        ],
        [
            'name' => 'DocSaleRow',
            'typeData' => [
                [
                    'name' => 'Nomenclature',
                    'type' => 'string',
                ],
                [
                    'name' => 'Quantity',
                    'type' => 'double',
                ],
                [
                    'name' => 'Price',
                    'type' => 'double',
                ],
                [
                    'name' => 'NDS',
                    'type' => 'double',
                ],
                [
                    'name' => 'periodical_name',
                    'type' => 'string',
                ],
                [
                    'name' => 'year',
                    'type' => 'double',
                ],
                [
                    'name' => 'issue_num',
                    'type' => 'string',
                ],
                [
                    'name' => 'SeasonTicketDataStart',
                    'type' => 'date',
                ],
                [
                    'name' => 'SeasonTicketPeriod',
                    'type' => 'integer',
                ],
            ],
        ],
        [
            'name' => 'ListContractors',
            'typeData' => [
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
            ],
        ],
        [
            'name' => 'Organization',
            'typeData' => [
                [
                    'name' => 'ID',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'PaymentOrder',
            'typeData' => [
                [
                    'name' => 'PaymentOrderRow',
                    'type' => 'PaymentOrderRow',
                ],
            ],
        ],
        [
            'name' => 'PaymentOrderRow',
            'typeData' => [
                [
                    'name' => 'IDDocument',
                    'type' => 'string',
                ],
                [
                    'name' => 'Year',
                    'type' => 'int',
                ],
                [
                    'name' => 'PaymentRowSum',
                    'type' => 'PaymentRowSumRow',
                ],
                [
                    'name' => 'Contact',
                    'type' => 'Contact',
                ],
            ],
        ],
        [
            'name' => 'PaymentRow',
            'typeData' => [
                [
                    'name' => 'Organization',
                    'type' => 'Organization',
                ],
                [
                    'name' => 'Acquirer',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'IDTransaction',
                    'type' => 'integer',
                ],
                [
                    'name' => 'PaymentRowSum',
                    'type' => 'PaymentRowSumRow',
                ],
                [
                    'name' => 'document_date',
                    'type' => 'date',
                ],
            ],
        ],
        [
            'name' => 'PaymentRowSumRow',
            'typeData' => [
                [
                    'name' => 'PaymentSum',
                    'type' => 'double',
                ],
                [
                    'name' => 'NDS',
                    'type' => 'double',
                ],
            ],
        ],
        [
            'name' => 'Payments',
            'typeData' => [
                [
                    'name' => 'PaymentRow',
                    'type' => 'PaymentRow',
                ],
            ],
        ],
        [
            'name' => 'Phone',
            'typeData' => [
                [
                    'name' => 'Code',
                    'type' => 'string',
                ],
                [
                    'name' => 'Number',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'PrintParams',
            'typeData' => [
                [
                    'name' => 'Bill',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Selling',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Invoice',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Format',
                    'type' => 'string',
                ],
                [
                    'name' => 'Signed',
                    'type' => 'boolean',
                ],
            ],
        ],
        [
            'name' => 'Response',
            'typeData' => [
                [
                    'name' => 'ResponseRow',
                    'type' => 'ResponseRow',
                ],
            ],
        ],
        [
            'name' => 'ResponsePaymentOrder',
            'typeData' => [
                [
                    'name' => 'ResponsePaymentOrderRow',
                    'type' => 'ResponsePaymentOrderRow',
                ],
            ],
        ],
        [
            'name' => 'ResponsePaymentOrderRow',
            'typeData' => [
                [
                    'name' => 'IsError',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Number',
                    'type' => 'string',
                ],
                [
                    'name' => 'Year',
                    'type' => 'int',
                ],
                [
                    'name' => 'Summ',
                    'type' => 'double',
                ],
                [
                    'name' => 'Comment',
                    'type' => 'string',
                ],
                [
                    'name' => 'Status',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'ResponseRow',
            'typeData' => [
                [
                    'name' => 'IsError',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'IDTransaction',
                    'type' => 'integer',
                ],
                [
                    'name' => 'Decoding',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'ResponseSale',
            'typeData' => [
                [
                    'name' => 'ResponseSaleRow',
                    'type' => 'ResponseSaleRow',
                ],
            ],
        ],
        [
            'name' => 'ResponseSaleRow',
            'typeData' => [
                [
                    'name' => 'SaleId',
                    'type' => 'string',
                ],
                [
                    'name' => 'IsError',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'Organization',
                    'type' => 'Organization',
                ],
                [
                    'name' => 'Contractor',
                    'type' => 'Contractor',
                ],
                [
                    'name' => 'Bill',
                    'type' => 'base64Binary',
                ],
                [
                    'name' => 'Act',
                    'type' => 'base64Binary',
                ],
                [
                    'name' => 'Torg12',
                    'type' => 'base64Binary',
                ],
                [
                    'name' => 'Invoice',
                    'type' => 'base64Binary',
                ],
                [
                    'name' => 'Komm',
                    'type' => 'string',
                ],
            ],
        ],
        [
            'name' => 'Sale',
            'typeData' => [
                [
                    'name' => 'DocSale',
                    'type' => 'DocSale',
                ],
                [
                    'name' => 'PrintParams',
                    'type' => 'PrintParams',
                ],
            ],
        ],
        [
            'name' => 'PaymentFromNxTo1C',
            'typeData' => [
                [
                    'name' => 'Payments',
                    'type' => 'Payments',
                ],
            ],
        ],
        [
            'name' => 'PaymentFromNxTo1CResponse',
            'typeData' => [
                [
                    'name' => 'return',
                    'type' => 'Response',
                ],
            ],
        ],
        [
            'name' => 'PaymentOrderFromNx',
            'typeData' => [
                [
                    'name' => 'PaymentOrder',
                    'type' => 'PaymentOrder',
                ],
            ],
        ],
        [
            'name' => 'PaymentOrderFromNxResponse',
            'typeData' => [
                [
                    'name' => 'return',
                    'type' => 'ResponsePaymentOrder',
                ],
            ],
        ],
        [
            'name' => 'SaleFromNX',
            'typeData' => [
                [
                    'name' => 'Sale',
                    'type' => 'Sale',
                ],
            ],
        ],
        [
            'name' => 'SaleFromNXResponse',
            'typeData' => [
                [
                    'name' => 'return',
                    'type' => 'ResponseSale',
                ],
            ],
        ],
        [
            'name' => 'Contractors',
            'typeData' => [
            ],
        ],
        [
            'name' => 'ContractorsResponse',
            'typeData' => [
                [
                    'name' => 'return',
                    'type' => 'ListContractors',
                ],
            ],
        ],
        [
            'name' => 'Contacts',
            'typeData' => [
                [
                    'name' => 'ContactsList',
                    'type' => 'ContactsList',
                ],
            ],
        ],
        [
            'name' => 'ContactsResponse',
            'typeData' => [
                [
                    'name' => 'return',
                    'type' => 'ContactsResponse',
                ],
            ],
        ],
    ];
    private $testingResults = ['functions' => [], 'types' => [],'overall_result' => false];

    public function __construct(SoapService $service = null)
    {
        if (!($service instanceof SoapService) || $service == null)
            return false;

        $this->service = $service;
    }
    public function mustHaveFunctions()
    {
        $result =  true;
        $functionsTestingResults = [];
        foreach($this->functionsList as $function) {
            //$function = ожидаемая функция
            if($this->service->isFunctionExists($function['name'])) {
                $functionsTestingResults[] = ['testName' => 'Проверка наличия функции "' . $function['name'] . '".', 'testSuccess' => true ];
                $functionInfo = $this->service->getFunctionInfo($function['name']);
                //$functionInfo = исследуемая функция Soap сервиса
                $funcUnderStudyData = isset($functionInfo['functionData']) ? $functionInfo['functionData'] : [];
                if ( isset($funcUnderStudyData['type']) && $funcUnderStudyData['type'] == $function['type']) {
                    $functionsTestingResults[] = ['testName' => 'Проверка типа функции ' . $function['name'] . '. Ожидаемый тип:"' . $function['type'] . '".' , 'testSuccess' => true ];
                } else {
                    $functionsTestingResults[] = ['testName' => 'Проверка типа функции ' . $function['name'] . '. Ожидаемый тип:"' . $function['type'] . '".' , 'testSuccess' => false ];
                    $result = false;
                }
                if (isset($funcUnderStudyData['parameters']) && is_array($funcUnderStudyData['parameters'])) {
                    $funcUnderStudyParams = array_map(function($elem){
                        $r = is_array($elem) && isset($elem['type']) ?  $elem['type'] : false;
                        if ($r)
                            return $r;
                        else
                            return;
                    }, $funcUnderStudyData['parameters']);
                } else {
                    $funcUnderStudyParams = [];
                }
                // $funcUnderStudyParams = массив с именами типов параметров исследуемой функции сервиса
                // $function['parameters'] = массив с ожидаемыми(требуемыми) именами типов параметров
                foreach ($function['parameters'] as $requiredParamType) {
                    if (in_array($requiredParamType, $funcUnderStudyParams)) {
                        $functionsTestingResults[] = ['testName' => 'Проверка типа входного параметра "' . $requiredParamType . '" функции "' . $function['name'] . '".' , 'testSuccess' => true ];
                    } else {
                        $functionsTestingResults[] = ['testName' => 'Проверка типа входного параметра "' . $requiredParamType . '" функции "' . $function['name'] . '".' , 'testSuccess' => false ];
                        $result = false;
                    }
                }
            } else {
                $functionsTestingResults[] = ['testName' => 'Проверка наличия функции ' . $function['name'], 'testSuccess' => false ];
                $result = false;
            }
        }
        $this->testingResults['functions'] = $functionsTestingResults;
        return $result;
    }
    public function mustHaveTypes()
    {
        $result =  true;
        $this->testingResults['types'] = [];
        foreach($this->typesList as $index => $type) {
            //$type = ожидаемый тип
            if ($this->service->isTypeExists($type['name'])) {
                $this->testingResults['types'][] = ['testName' => 'Проверка наличия типа  "' . $type['name'] . '".', 'testSuccess' => true];
                $typeUnderStudyData = $this->service->getTypeInfo($type['name']);
                foreach( $type['typeData'] as $mockSubTypeItem ) {
                    $typeIsOK = $this->detailTestType($typeUnderStudyData, $mockSubTypeItem, $type['name']);
                    if (!$typeIsOK)
                        $result = false;
                }
            } else {
                $this->testingResults['types'][] = ['testName' => 'Проверка наличия типа  "' . $type['name'] . '".', 'testSuccess' => false ];
                $result = false;
            }
        }
        return $result;
    }
    private function detailTestType($realTypeData = [], $mockTypeData = [], $typeName = '')
    {
        //$realTypeData = исследуемые типы данных Soap сервиса с определенным имененм. Это массив массивов вида:
        // [
            // [
            //     'typeName' => '...',
            //     'typeDesc' => [
            //         [
            //            'type' => 'typeStr'
            //            'name' => '...'
            //         ],
            //         [
            //            'type' => 'typeStr1'
            //            'name' => '...'
            //         ],
            //     ]
            // ],
        //     [...],
        //     [...]
        // ]
        // То что мы внутри $realTypeData ищем ($mockTypeData) имеет вид.
        //         [
        //              'name' => '...',
        //              'type' => 'typeStr',
        //          ],
            $typesEqual = false;
            foreach ( $realTypeData as $index=> $realTypeDataItem ) {
                if ($typesEqual)
                    break;
                $realTypeItemContent = is_array($realTypeDataItem) && isset($realTypeDataItem['typeDesc']) ? $realTypeDataItem['typeDesc'] : [];
                if (is_array($realTypeItemContent) && !empty($realTypeItemContent)) {
                    foreach($realTypeItemContent as $realSubTypeItem) {
                        $realSubtypeName =  (is_array($realSubTypeItem) && isset($realSubTypeItem['name'])) ? trim($realSubTypeItem['name']) : false;
                        $realSubtypeType = (is_array($realSubTypeItem) && isset($realSubTypeItem['type'])) ? trim($realSubTypeItem['type']) : false;
                        if (trim($mockTypeData['name']) == $realSubtypeName && trim($mockTypeData['type']) == $realSubtypeType) {
                            $typesEqual = true;
                            break;
                        }
                    }
                }
            }
            $resultStr = ['testName' => 'Проверка содержания типа  "' . $typeName . '". Ожидается наличие в нем подтипа "' . $mockTypeData['name'] . '" типа ' . $mockTypeData['type'], 'testSuccess' => $typesEqual ];
            $this->testingResults['types'][] = $resultStr;
            return $typesEqual; 
    }
    public function executeTesting(?int $publisherId)
    {
        if ($err = $this->service->getError()) {
            $this->testingResults['overall_result'] = false;
            $resultStr = ['testName' => 'Проверка отклика SOAP сервиса "' . $this->service->getUrl(), 'testSuccess' => false ];
            $this->testingResults['functions'][] = $resultStr;
            $resultStr = ['testName' => $err, 'testSuccess' => false ];
            $this->testingResults['functions'][] = $resultStr;
            $this->updateTestingResults($publisherId);
            return false;
        }
        $t = $this->mustHaveTypes();
        $f = $this->mustHaveFunctions();
        // $result = $this->mustHaveTypes() && $this->mustHaveFunctions(); не катит. т.к. нужно обязательно запустить на выполнение ОБЕ функции, а в такой формулировке вторая функция может вообще не запуститься если первая сразу вернет false
        $result = $t && $f;
        $this->testingResults['overall_result'] = $result;
        $this->updateTestingResults($publisherId);

        return $result;
    }
    public function getTestingResults()
    {
        return $this->testingResults;
    }

    private function updateTestingResults(?int $publisherId): void
    {
        if (empty($publisherId)) {
            return;
        }
        $i = Integration1cVerify::findOne(['publisher_id' => $publisherId]);
        if (empty($i)) {
            return;
        }
        $i->verification_result = serialize($this->getTestingResults());
        $i->is_verified = intval($this->testingResults['overall_result'] ?? 0);
        $i->save();
    }

}