<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\queue_jobs;

use common\custom_components\CustomHelpers;
use common\models\Edition;
use common\models\EmaiLog;
use common\models\Issue;
use common\models\NxOrder;
use common\models\OrderItemRelease;
use common\models\Payment;
use common\models\Publisher;
use common\models\Shipment;
use common\models\ShipmentItem;
use common\models\User;
use Exception;
use Yii;
use yii\base\Model;
use yii\mail\MessageInterface;
use yii\queue\JobInterface;

/**
 * @property string $templateName
 * @property array $data
 * @property string $subject
 * @property string $fromEmail
 * @property string $toEmail
 * @property int $publisherId
 */

class EmailNotificationJob extends Model implements JobInterface
{
    const IDS_NAMES_TO_ENTITY_MAP = [
        'user_id' => [
            'class' => User::class,
            'varName' => 'user',
            'pmName' => 'User_ID'
        ],
        'publisher_id' => [
            'class' => Publisher::class,
            'varName' => 'publisher',
            'pmName' => 'id'
        ],
        'edition_id' => [
            'class' => Edition::class,
            'varName' => 'edition',
            'pmName' => 'Message_ID'
        ],
        'issue_id' => [
            'class' => Issue::class,
            'varName' => 'issue',
            'pmName' => 'Message_ID'
        ],
        'item_release_id' => [
            'class' => OrderItemRelease::class,
            'varName' => 'orderItemRelease',
            'pmName' => 'id'
        ],
        'shipment_id' => [
            'class' => Shipment::class,
            'varName' => 'shipment',
            'pmName' => 'id'
        ],
        'shipment_item_id' => [
            'class' => ShipmentItem::class,
            'varName' => 'shipmentItem',
            'pmName' => 'id'
        ],
        'payment_id' => [
            'class' => Payment::class,
            'varName' => 'payment',
            'pmName' => 'id'
        ],
        'order_id' => [
            'class' => NxOrder::class,
            'varName' => 'order',
            'pmName' => 'id'
        ],
    ];

    public $templateName;
    public $data;
    public $subject;
    public $fromEmail;
    public $toEmail;

    public $publisherId;

    public function __construct(int $publisherId, $config = [])
    {
        parent::__construct($config);
        $this->publisherId = $publisherId;
    }

    public function rules()
    {
        return [
            [['templateName', 'data', 'subject', 'fromEmail', 'toEmail', 'publisherId'], 'required'],
            [['templateName', 'subject'], 'string'],
            [['fromEmail', 'toEmail'], 'email'],
            ['data', 'validateData'],
        ];

    }

    public function validateData($attribute, $params)
    {
        $data = $this->$attribute;

        if (!is_array($data) || empty($data))
            $this->addError($attribute, 'data должен быть непустым массивом');
    }

    public function execute($queue)
    {
        $this->convertIdsToEntities();

        //Изврат т.к. объект сообщения, кот. возвр. compose НЕ содержит возможностей получить из него HTML
        //и получить из него готовый код письма никак не удается
        $messageBody = Yii::$app->mailer->render($this->templateName, $this->data, 'layouts/html');

        try {
            CustomHelpers::configureMailerComponent($this->publisherId);
            $message = Yii::$app->mailer->compose($this->templateName, $this->data);
            $message->setFrom($this->fromEmail)
                ->setTo($this->toEmail)
                ->setSubject($this->subject);
            if ($message->send()) {
                $this->logEmail($message, $messageBody, true, $this->subject);
            } else {
                $this->logEmail($message, $messageBody, false, 'НЕ ОТПАРВЛЕНО: ' . $this->subject);
            }
        } catch (Exception $ex) {
            $this->logEmail($ex, $messageBody, false, 'Exception при попытке отправить Email');
            return;
        }
    }

    /**
     *
     * @param MIX \yii\swiftmailer\Message | \Exception $emailMessage
     * @param bool $success
     * @param string $messageBody
     * @param string $type
     * @return void
     *      */
    public function logEmail($emailMessage = null, $messageBody = '', $success = true, $type = '')
    {
        if ($emailMessage == null || (!($emailMessage instanceof MessageInterface) && !($emailMessage instanceof Exception))) {
            return;
        }
        $logItem = new EmaiLog();


        if ($emailMessage instanceof Exception) {
            $logItem->from = !empty($this->fromEmail) ? $this->fromEmail : '';
            $logItem->to = !empty($this->toEmail) ? $this->toEmail : '';
            $logItem->success = intval($success);
            $logItem->subject = !empty($this->subject) ? $this->subject : '';
            $logItem->body = '<pre>' . $emailMessage->__toString() . '</pre>';
            $logItem->type = $type;
//            $logItem->publisher_id = intval($this->tryToGetPublisherId());
            $logItem->publisher_id = intval($this->publisherId);
        } else {
            $logItem->from = $this->fromEmail;
            $logItem->to = implode(',', $emailMessage->getTo());
            $logItem->to = $this->toEmail;
            $logItem->success = intval($success);
            $logItem->subject = $emailMessage->getSubject();
            $logItem->body = $messageBody;
            $logItem->type = $type;
//            $logItem->publisher_id = intval($this->tryToGetPublisherId());
            $logItem->publisher_id = intval($this->publisherId);
        }

        $additionalKeysList = ['order_id', 'issue_id', 'payment_id', 'shipment_id', 'shipment_item_id', 'user_id', 'item_release_id', 'edition_id'];
        foreach ($additionalKeysList as $keyName) {
            if (isset($this->data[$keyName]) && !empty($this->data[$keyName])) {
                $logItem->{$keyName} = intval($this->data[$keyName]);
            }
        }
        if (!$logItem->save()) {
            print_r($logItem->getErrors());
        }
    }

    private function tryToGetPublisherId()
    {
        $namesMap = [
            'publisher' => ['publisher_id', 'publisherid', 'publisher'],
            'order' => ['order_id', 'orderid', 'oid', 'o_id', 'order'],
            'issue' => ['issue_id', 'issueid', 'issue'],
            'user' => ['user_id', 'userid', 'user'],
        ];
        $publisherClassName = Publisher::class;
        $orderClassName = NxOrder::class;
        $issueClassName = Issue::class;
        $userClassName = User::class;

        foreach ($this->data as $key => $value) {
            if ($value instanceof $publisherClassName) {
                return $value->id;
            }
            if ($value instanceof $userClassName) {
                $p = $value->publisher;
                return $p->id;
            }
            if ($value instanceof $orderClassName) {
                $p = $value->publisher;
                return $p->id;
            }
            if ($value instanceof $issueClassName) {
                $p = $value->magazine->ownerid;
                return $p->publisher_id;
            }
            if (in_array(strtolower($key), $namesMap['publisher'])) {
                return intval($value);
            }
            if (in_array(strtolower($key), $namesMap['user'])) {
                $u = User::findOne(['User_ID' => intval($value)]);
                if (null !== $u)
                    return $u->publisher->id;
            }
            if (in_array(strtolower($key), $namesMap['order'])) {
                $o = NxOrder::findOne(['id' => intval($value)]);
                if (null !== $o)
                    return $o->publisher->id;
            }
            if (in_array(strtolower($key), $namesMap['issue'])) {
                $i = Issue::findOne(['Message_ID' => intval($value)]);
                if (null !== $i)
                    return $i->magazine->ownerid->publisher_id;
            }
        }
        return false;
    }

    public function convertIdsToEntities(): void
    {
        foreach(self::IDS_NAMES_TO_ENTITY_MAP as $idKeyName => $entityData) {
            if (isset($this->data[$idKeyName]) && !empty($this->data[$idKeyName])) {
                if (isset($this->data[$entityData['varName']])) {
                    continue;
                }
                $pmName = $entityData['pmName'];
                $pmVal = intval($this->data[$idKeyName]);
                $className = $entityData['class'];
                $this->data[$entityData['varName']] = $className::findOne([$pmName => $pmVal]);
            }
        }
    }
}