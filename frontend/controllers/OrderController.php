<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\NxOrder;



class OrderController extends ActiveController
{
    public $modelClass = 'common\models\NxOrder';

    public function verbs()
    {
        $res = parent::verbs();
        $res['is-order-paid'] = ['HEAD', 'OPTIONS', 'GET'];

        return $res;
    }

    public function actionIndex()
    {
        $query = Order::find();
//        $query->joinWith(['legalAddress']);
//        $query->joinWith(['contactPerson']);
//        $query->joinWith(['address']);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    
    public function actionCalculateSum() {
        
        header('Access-Control-Allow-Origin: *');
        $subscribeService = new \common\services\SubscribeService();
        return $subscribeService->calculateSumOnData( Yii::$app->request->post() );
        
    }
    public function actionAddOrder() {
        header('Access-Control-Allow-Origin: *');
        $service = new \common\services\SubscribeService();
        
        return $service->addOrder( Yii::$app->request->post() );
    }
    
    public function actionTest()
    {
        return ['test' => 'test'];
    }
    /**
    * 
    **/
    public function actionUpdatePaymentSystem()
    {
        if (!Yii::$app->request->isPost)
            throw new \yii\web\BadRequestHttpException('wrong request type');
        
        $orderHash = Yii::$app->request->post('hash', false);
        $publisherId = Yii::$app->request->post('pId', false);
        $paymentSystemId = Yii::$app->request->post('sId', false);
        
        if (!$orderHash || !$publisherId || !$paymentSystemId)
            return ['status' => 'err', 'message' => 'not enough data.'];
        
        $queryStr = "CALL update_order_payment_system(:orderHash, :systemId, :publisherId);";
        
        $result = \Yii::$app->db->createCommand($queryStr)
                ->bindParam(':orderHash', $orderHash, \PDO::PARAM_STR)
                ->bindParam(':systemId', $paymentSystemId, \PDO::PARAM_INT)
                ->bindParam(':publisherId', $publisherId, \PDO::PARAM_INT)
                ->queryOne();
        
        header('Access-Control-Allow-Origin: *');
        
        return $result;
    }

    public function actionIsPaid(int $id)
    {
        $queryString = "CALL is_order_paid(:orderId)";
        $result = \Yii::$app->db->createCommand($queryString)
            ->bindParam(':orderId', $id, \PDO::PARAM_INT)
            ->queryOne();

        header('Access-Control-Allow-Origin: *');

        return ['result' => boolval($result['paid'])];

    }
}
