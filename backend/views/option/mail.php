<?php

use common\models\PublisherProperty;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var PublisherProperty[] $mailOptions
 */

$this->title = 'Почтовые настройки';
?>
<?php
$formOptions = [
    'enableAjaxValidation' => false,
    'id' => 'mail-options-form',
    'validateOnBlur' => false,
    'options' => ['class' => 'form-of-mail-options'],
//        'action' => ''
];

$form = ActiveForm::begin($formOptions);
?>
<div class='box box-primary'>
    <div class='box-header with-border'>
        <h3 class='box-title'>
            Почтовые настройки
        </h3>
    </div>
    <div class='box-body'>
        <div class="row">
            <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                <?php foreach ($mailOptions as $index => $option): ?>
                    <?php
                        if (!$option->isNewRecord) {
                            echo(yii\helpers\Html::activeHiddenInput($option, "[$index]id"));
                        }
                    ?>
                    <?php echo(yii\helpers\Html::activeHiddenInput($option, "[$index]property_type_id")); ?>
                    <?php echo(yii\helpers\Html::activeHiddenInput($option, "[$index]publisher_id")); ?>

                    <div>
                        <label style="font-size:12px;margin-bottom:0px"
                               for="publisherproperty-<?php echo($index); ?>-value"><?php echo($option->propertyType->name); ?></label>
                        <?= $form->field(
                            $option,
                            "[$index]value",
                            [
                                'template' => "{input}\n{hint}\n{error}",
                                'labelOptions' => ['style' => 'font-size:12px;margin-bottom:0px']
                            ]
                        )->textInput(['class' => 'form-control input-sm', 'data-prop_name' => $option->propertyType->name]) ?>


                    </div>
                <?php endforeach; ?>
            </div>
            <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                <button type="button" class="btn btn-info pull-right" id="checkConnectionButton" style="margin-top: 10px" onclick="checkMailConnection(event)">Проверить соединение</button>
                <div class="pull-right" style="margin-top: 10px;line-height:34px;display: none" id="checking-in-progress">
                    <img src="/images/ajax-loader.gif" style="margin-left: 10px">
                    Проверка соединения
                </div>
                <div class="form-group" style="clear: both">
                    <div class="help-block pull-right"  id="connectionCheckResult"></div>
                </div>
                <?php
                // echo ($form->field($model, 'address',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm','readonly' => true]));
                ?>
            </div>
        </div>

    </div>
    <div class='box-footer'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary pull-left']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    function checkMailConnection(e) {
        $('#checking-in-progress').show();
        $('#connectionCheckResult').hide();
        $('#checkConnectionButton').hide();

        $.ajax({
            url: '/option/check-mail-connection',
            type: 'post',
            dataType: 'json',
            data: $('#mail-options-form').serialize(),
        })
        .done(function (response) {
            console.log(response);
            if (response.success) {
                $('#connectionCheckResult').text('УСПЕШНО!').parent('.form-group').addClass('has-success').removeClass('has-error');
            } else {
                $('#connectionCheckResult').text('Нет соединения! ' + response.message).parent('.form-group').removeClass('has-success').addClass('has-error');
            }
            $('#checking-in-progress').hide();
            $('#connectionCheckResult').show();
            $('#checkConnectionButton').show();
        })
        .fail(function (a,b,c) {
            console.log(a,b,c);
            alert('Что-то пошло не так. См.консоль браузера');
            $('#checking-in-progress').hide();
            $('#connectionCheckResult').show();
            $('#checkConnectionButton').show();
        });
    }
</script>


