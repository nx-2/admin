<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>

<!--    <img src='https://pay.osp.ru/img/logo/yandex.gif'>
    <br>
    <span class='db'>Яндекс.Деньги</span>
-->
<!--
<form action='https://demomoney.yandex.ru/eshop.xml' method='post'>
    <input type="hidden" name="system" value="yandex">
    <input type='hidden' name='scid' value='<?php //echo($paySystems->systems->shop_param);?>'>
    <input type='hidden' name='shopId' value='<?php //echo($paySystems->systems->shop_name);?>'>
    <input type='hidden' name='sum' value='<?php //echo($model->price);?>'>
    <input type='hidden' name='customerNumber' value='<?php //echo($transaction_id);?>'>
    <input type='hidden' name='orderNumber' value='<?php //echo($model->id);?>'>
    <input type="submit">
</form>
-->
<div style='overflow: hidden;'>
    <div style='float:left;'>
        <?php 
        $host = yii::$app->params['api']['host'];
        $images = [
//            $host . '/images/visa_mastercard.png',
            $host . '/images/visa_mastercard_mir.png',
            $host . '/images/yooMoney.png',
//            $host . '/images/ya_money_logo.png',
//            $host . '/images/QIWI_logo.jpg',
//            $host . '/images/cash_logo.png',
//            $host . '/images/alfa_logo.png',
//            $host . '/images/psb_logo.jpg',
        ];
        ?>
        <?php foreach($images as $i): ?>
        <img src='<?= $i; ?>' style='max-width:100px;display: inline;vertical-align: top;'/>
        <?php endforeach; ?>
    </div>
    <div style='float: right;padding: 5px;'>
        <form action="https://money.yandex.ru/eshop.xml" method="post">
            <!-- Обязательные поля -->
            <input name="paymentType" value="" type="hidden">
            <input name="shopId" value="<?=$system->param_1; ?>" type="hidden"/>
            <input name="scid" value="<?=$system->param_2; ?>" type="hidden"/>
            <input name="sum" type="hidden" value="<?=$order->price;?>"/>
            <input name="customerNumber" type="hidden" value="<?=$order->person_id;?>"/>
            <input name="orderNumber" type="hidden" value="<?=$order->id;?>"/>
            <input name="cps_phone" type="hidden" value="<?=$order->person->phone;?>"/>
            <input name="cps_email" type="hidden" value="<?=$order->person->email;?>"/>
            <input name="publisherId" type="hidden" value="<?=$publisherId;?>"/>
            <input type="submit" value="Оплатить" class='pay-button'/>
        </form>
    </div>
</div>