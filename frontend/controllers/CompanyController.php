<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\Company;



class CompanyController extends ActiveController
{
    public $modelClass = 'common\models\Company';

    public function actionIndex()
    {
        $query = Company::find();
        $query->joinWith(['legalAddress']);
        $query->joinWith(['contactPerson']);
        $query->joinWith(['address']);

		return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    public function actionOne($id)
    {

		// $columns = [
		// 		'nx_company.name', 'nx_company.org_form', 'nx_company.address_id', 'nx_company.legal_address_id', 'nx_company.contact_person_id','nx_company.email', 'nx_company.url', 
		// 		'nx_company.description', 'nx_company.shortname', 'nx_company.othernames', 'nx_company.wrongnames', 'nx_company.inn', 'nx_company.okonh', 'nx_company.pay_account', 
		// 		'nx_company.corr_account', 'nx_company.kpp','nx_company.bik','nx_company.bank',
		// ];

		$columns = [
				'c.name', 'c.org_form', 'c.address_id', 'c.legal_address_id', 'c.contact_person_id','c.email', 'c.url', 
				'c.description', 'c.shortname', 'c.othernames', 'c.wrongnames', 'c.inn', 'c.okonh', 'c.pay_account', 
				'c.corr_account', 'c.kpp','c.bik','c.bank',
		];
		return Company::find()
			->select($columns)
			->from('nx_company c')
			->where(['c.id' => $id])
			->joinWith(
			[
				//получить "жадно" legalAdress->Country, Address->country, contactPerson->address->country
				'legalAddress la' => function($q) {
					$q->joinWith(['country lac' => function($q) {
						$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
					}]);
				},
				'contactPerson p' => function($q) {
					$q->joinWith(['address pad' => function($q) {
						$q->joinWith(['country cpc' => function($q) {
							$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
						}]);
					}]);
				},
				'address a' => function($q) {
					$q->joinWith(['country adc' => function($q) {
						$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
					}]);
				},
			])
			->asArray(true)
			->one();
	}
}
