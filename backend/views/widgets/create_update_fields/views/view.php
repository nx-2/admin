<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
use yii\helpers\Html;
use yii\helpers\Url;
 ?>
 
 <div>
	<?php if($model->hasAttribute($createdFieldName) && !empty($value = $model->getAttribute($createdFieldName))): ?>
		<?php 
			$label = $model->getAttributeLabel($createdFieldName); 
		?>
		<div class="info-box small box" style='border-top:none'>
			<div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="remove">
	            	<i class="fa fa-times"></i>
	            </button>
	        </div>		
			<span class="info-box-icon bg-primary"><i class="fa fa-calendar-check-o"></i></span>
			<div class="info-box-content">
				<span class="info-box-text"><?=$label;?>:</span>
				<span class="info-box-number"><?= Yii::$app->formatter->asDatetime($value);?></span>
			</div>
		</div>
	<?php endif; ?>

	<?php if($model->hasAttribute($createUserFieldName) && !empty($value = $model->getAttribute($createUserFieldName)) && method_exists($model, 'getCreateUser') && $model->createUser !== null): ?>
		<?php 
			$label = $model->getAttributeLabel($createUserFieldName); 
		?>
	<div class="info-box small box" style='border-top:none'>
		<div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove">
            	<i class="fa fa-times"></i>
            </button>
        </div>		
		<span class="info-box-icon bg-primary"><i class="fa fa-user-circle-o"></i></span>
		<div class="info-box-content">
			<span class="info-box-text"><?=$label;?>:</span>
			<span class="info-box-number"><?= 'Имя: ' . $model->createUser->FullName . ', Логин: ' . $model->createUser->Login . ', Email: ' . $model->createUser->Email;?></span>
		</div>
	</div>
	<?php endif; ?>

	<?php if($model->hasAttribute($lastUpdatedFieldName) && !empty($value = $model->getAttribute($lastUpdatedFieldName))): ?>
		<?php 
			$label = $model->getAttributeLabel($lastUpdatedFieldName); 
		?>
	<div class="info-box small box" style='border-top:none'>
		<div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove">
            	<i class="fa fa-times"></i>
            </button>
        </div>		
		<span class="info-box-icon bg-primary"><i class="fa fa-calendar-check-o"></i></span>
		<div class="info-box-content">
			<span class="info-box-text"><?=$label;?>:</span>
			<span class="info-box-number"><?= Yii::$app->formatter->asDatetime($value);?></span>
		</div>
	</div>
	<?php endif; ?>

	<?php if($model->hasAttribute($lastUserFieldName) && !empty($value = $model->getAttribute($lastUserFieldName)) && method_exists($model, 'getLastUser') && $model->lastUser !== null): ?>
		<?php 
			$label = $model->getAttributeLabel($lastUserFieldName); 
		?>
	<div class="info-box small box" style='border-top:none'>
		<div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove">
            	<i class="fa fa-times"></i>
            </button>
        </div>		
		<span class="info-box-icon bg-primary"><i class="fa fa-user-circle-o"></i></span>
		<div class="info-box-content">
			<span class="info-box-text"><?=$label;?>:</span>
			<span class="info-box-number"><?= 'Имя: ' . $model->lastUser->FullName . ', Логин: ' . $model->lastUser->Login . ', Email: ' . $model->lastUser->Email;?></span>
		</div>
	</div>
	<?php endif; ?>
 </div>