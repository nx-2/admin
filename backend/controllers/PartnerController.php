<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

class PartnerController extends \yii\web\Controller
{
    public function actionIndex()
    {
//        $order = \common\models\NxOrder::findOne(['id' => '72984']);
//        $order->order_state_id = 3;
//        $order->update(true);

        return $this->render('index');
    }

}
