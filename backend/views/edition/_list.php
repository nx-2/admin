<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<div class="publisher-index">

    <p>
        <?= Html::a('Добавить издание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'editions-list', 'enablePushState' => true]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;width:38px'],
                // 'template' => '{update}',
                'template' => '{view}',
                // 'controller' => '',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = '/edition/getinfo/' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'Посмотреть',
                                'aria-label' => 'быстрый просмотр',
                                'data-pjax' => 0,
                                'class' => 'fa fa-eye fa-font-18',
                                'onclick' => 'getInfo("edition")',
                            ]
                        );
                    },
                ],
            ],
            //'Message_ID',
            //'User_ID',
            //'Subdivision_ID',
            //'Sub_Class_ID',
           // 'Priority',
            // 'issn',
            // 'Keyword',
            // 'Checked',
            // 'TimeToDelete',
            // 'TimeToUncheck',
            // 'IP',
            // 'UserAgent',
            // 'Parent_Message_ID',
            // 'Created',
            // 'LastUpdated',
            // 'confines',
            // 'start_subscription',
            // 'LastUser_ID',
            // 'LastIP',
            // 'LastUserAgent',
             'HumanizedName',
             'EnglishName',
            //'full_name',
            // 'mask_for_reference',
            // 'issue_mask',
            // 'mask_for_task',
            // 'data_format',
            // 'subscribe',
            // 'esubscribe',
            // 'deleted',
            // 'Passport:ntext',
            // 'ArticlePrice',
            // 'ncTitle',
            // 'ncKeywords',
            // 'ncDescription:ntext',

            // 'titleId',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = '/edition/update?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать издание',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '' ,
                            '/edition/delete?id=' . $key,
                            [
                                'title' => 'Удалить издание',
                                'aria-label' => 'Удалить',
                                'data-confirm'=>'Вы уверены, что хотите удалить это издание?',
                                'data-method'=> 'post',
                                'data-pjax' => '#editions-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    </div>
<div id='edition-info-modal-wrapper'></div>



