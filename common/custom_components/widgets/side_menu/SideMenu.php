<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\widgets\side_menu;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class SideMenu extends Widget
{
    public $menu;
    private $currentUrl;
    private $thisUserRolesList;

    public function init()
    {
        parent::init();
        if ($this->menu === null) {
            $this->menu = [];
        }
        $this->currentUrl = str_replace('?' . Yii::$app->request->queryString, '', Yii::$app->request->url);
        $this->thisUserRolesList = $this->getThisUserRolesFullList();
    }

    public function run()
    {
        return $this->renderUl($this->menu);
//        return $this->render('sideMenu', ['menu' => $this->menu] );
    }
    
    private function renderUl($paramArr = [])
    {
        if (empty($paramArr))
            return '';
        
        $liLines = [];
        $items = (isset($paramArr['items']) && is_array($paramArr['items'])) ? $paramArr['items'] : [];

        foreach($items as $item) {
            $li = isset($item['li']) && is_array($item['li']) ? $item['li'] : [];
            if (!$this->isItemVisible($item)) {
                continue;
            }

            if (!empty($li) && isset($item['allowedRoles']))
                $li['allowedRoles'] = $item['allowedRoles'];
            
            $liLines[] = $this->renderLi($li);
        }
        
        $options = (isset($paramArr['ulAttributes']) && is_array($paramArr['ulAttributes'])) ? $paramArr['ulAttributes'] : [];
        if ($this->areItemsHasCurrentUrl($items)) {
            $options['class'] = isset($options['class']) ? $options['class'] . ' menu-open' : 'menu-open';
            $options['style'] = 'display:block';
        }
        $content = implode('', $liLines);
        $content = preg_replace('~>\s*\n\s*<~', '><', $content);
        $html = Html::tag('ul', $content, $options);
        return $html;
        
    }
    
    private function  renderLi($paramArr = [])
    {
        if (!is_array($paramArr) || empty($paramArr))
            return '';
        if (isset($paramArr['childMenu'])) {
            $childUlHtml = $this->renderUl($paramArr['childMenu']);//recursion
            unset($paramArr['childMenu']);
        } else {
            $childUlHtml = '';
        }

        $html = $this->render(
            'li',
            [
                'data' => $paramArr,
                'childUl' => $childUlHtml,
                'currentUrl' => $this->currentUrl,
            ]
        );
        $html = preg_replace('~>\s*\n\s*<~', '><', $html);
        return $html;
    }
    private function areItemsHasCurrentUrl($items = [])
    {
        if(!is_array($items) || empty($items))
            return false;
        
        foreach($items as $item) {
            $href = '';
            if (
                    isset($item['li']) && isset($item['li']['link']) && 
                    isset($item['li']['link']['attributes']) && 
                    isset($item['li']['link']['attributes']['href'])
                ) {
                $href = $item['li']['link']['attributes']['href'];
            }
            if ($href == $this->currentUrl)
                return true;
        }
        return false;
    }

    private function getThisUserRolesFullList(): array
    {
        $thisUserRoles = Yii::$app->getUser()->getIdentity()->roles;
        $fullUserRolesList = ['@'];
        foreach ($thisUserRoles as $userRole) {
//            $fullUserRolesList[] = $userRole['code'];
            foreach(Yii::$app->getAuthManager()->getChildRoles($userRole['code']) as $childRole) {
                $fullUserRolesList[] = $childRole->name;
            }
        }
        return array_unique($fullUserRolesList);
    }

    private function isItemVisible(array $itemData): bool
    {
        $result = true;
        if (empty($itemData)) {
            return false;
        }

        $requiredPermissions = $itemData['requiredPermissions'] ?? false;
        if(!empty($requiredPermissions)) {
//            $am = Yii::$app->getAuthManager();
            $webUser = Yii::$app->getUser();
            if (is_array($requiredPermissions)) {
                foreach($requiredPermissions as $permName) {
                    $result = $webUser->can($permName, []) && $result;
                }
            } else {
                $result = $webUser->can($requiredPermissions, []);
            }
            return $result;
        }
        $allowedRoles = $itemData['allowedRoles'] ?? false;

        if (!$allowedRoles) {
            return true;
        }
        return count(array_intersect($allowedRoles, $this->thisUserRolesList)) > 0;
    }

}