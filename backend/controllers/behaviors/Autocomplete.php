<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers\behaviors;

use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;

/**
 * поставляет данные для автокомплита 
 * для использования в контроллере нужно подключить поведение в методе behaviors контроллера
 * следующим образом
 * 
 * public function behaviors()
 * {
 *     return [
            'autocomplete (or whatever)' => [
                'class' => \backend\controllers\behaviors\Autocomplete::className(),
                'modelName' => 'Country',
                'idFieldName' => 'Country_ID',
                'labelFieldName' => 'Country_Name',
                'modelNameSpace' => '\backend\models\\', //optional
                'count' =>'100' // optional
            ]
 *     ];
 * }
 * и после этого в соответствующем методе контроллера явно вызвать метод  autocompleteRequest()
 * например 
     public function actionAutocomplete()
    {
        return $this->autocompleteRequest();
    }
 */
class Autocomplete extends Behavior
{
	public $modelName = '';
	public $modelNameSpace = "\common\models\\";
	public $idFieldName = 'id';
	public $labelFieldName = 'name';
	public $count = 50;

    /**
     * Returns data for autocomplete widget.
     * @return json
     */
    public function autocompleteRequest()
    {
        $searchStr = Yii::$app->request->get('term', '');
        $className = $this->modelNameSpace . $this->modelName;
        $result = $className::find()
            ->select(['id' => $this->idFieldName, 'label' => $this->labelFieldName])
            ->andFilterWhere(['like', $this->labelFieldName, $searchStr])
            ->asArray()
            ->orderBy($this->labelFieldName)
            ->limit($this->count)
            ->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }


}
