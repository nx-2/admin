<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\validators;

class Pay extends \yii\base\Model
{

    public $pay_id;
    public $hash;
    public $transaction_id;
    public $customer_id=1;

    public function rules()
    {
        return [
            [['pay_id',  'hash','transaction_id','customer_id'], 'required'],
            [['pay_id','transaction_id','customer_id'], 'integer'],
        ];
    }
}