<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'empty';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goHome();
            // return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'title' => 'Авторизация',
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTest()
    {
        return $this->render('test',['var' => 'value']);
    }
    
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id=='error')
                 $this->layout ='error';
            return true;
        } else {
            return false;
        }
    }
    public function actionAjaxEnabled()
    {
        $name = Yii::$app->request->post('name', false);
        $id = Yii::$app->request->post('id', false);
        $value = Yii::$app->request->post('value', null);
        $fieldName = Yii::$app->request->post('field', 'enabled');

        \Yii::$app->response->format = 'json';

        if (!$name || !$id || $value === null) {
            return ['success' => false, 'errMessage' => 'Not enough data.'];
        $value = (boolean) $value;
        }
        switch ($name) {
            case 'Publisher':
                $model = \common\models\Publisher::findOne(intval($id));
                break;
            case 'AdvertsCodes':
                $model = \common\models\AdvertsCodes::findOne(intval($id));
                break;
            case 'MarketBlock':
                $model = \common\models\MarketBlock::findOne(intval($id));
                break;
            case 'User':
                $model = \common\models\User::findOne(intval($id));
                break;
            default:
                return ['success' => false, 'errMessage' => 'Unknown model name'];
                break;
        }
        if (empty($model))
                return ['success' => false, 'errMessage' => 'Record not found.'];

        // $model->enabled = intval(!(boolean) $model->enabled);// ($value == true) ? 1 : 0;
        $model->setAttribute($fieldName, intval(!(boolean) $model->getAttribute($fieldName)));

        if ($model->update(false, [$fieldName])) {
            return ['success' => true, 'id' => $id, 'checked' => $model->getAttribute($fieldName)];
        } else {
            return ['success' => false, 'errMessage' => 'Update failed', 'e' => $model->errors, 'enabled' => $model->getAttribute($fieldName)];
        }
    }
}
