<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\IssueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Message_ID') ?>

    <?= $form->field($model, 'User_ID') ?>

    <?= $form->field($model, 'Subdivision_ID') ?>

    <?= $form->field($model, 'Sub_Class_ID') ?>

    <?= $form->field($model, 'Priority') ?>

    <?php // echo $form->field($model, 'Keyword') ?>

    <?php // echo $form->field($model, 'Checked') ?>

    <?php // echo $form->field($model, 'TimeToDelete') ?>

    <?php // echo $form->field($model, 'TimeToUncheck') ?>

    <?php // echo $form->field($model, 'IP') ?>

    <?php // echo $form->field($model, 'UserAgent') ?>

    <?php // echo $form->field($model, 'Parent_Message_ID') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <?php // echo $form->field($model, 'last_user_id') ?>

    <?php // echo $form->field($model, 'LastIP') ?>

    <?php // echo $form->field($model, 'LastUserAgent') ?>

    <?php // echo $form->field($model, 'Name') ?>

    <?php // echo $form->field($model, 'MagazineID') ?>

    <?php // echo $form->field($model, 'StartDate') ?>

    <?php // echo $form->field($model, 'Number') ?>

    <?php // echo $form->field($model, 'PublicDate') ?>

    <?php // echo $form->field($model, 'Picture') ?>

    <?php // echo $form->field($model, 'IssueID') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'annotation') ?>

    <?php // echo $form->field($model, 'available_online') ?>

    <?php // echo $form->field($model, 'issue_title') ?>

    <?php // echo $form->field($model, 'no_full') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'oldID') ?>

    <?php // echo $form->field($model, 'inCompositionDate') ?>

    <?php // echo $form->field($model, 'showDescription') ?>

    <?php // echo $form->field($model, 'ArticlePrice') ?>

    <?php // echo $form->field($model, 'ncTitle') ?>

    <?php // echo $form->field($model, 'ncKeywords') ?>

    <?php // echo $form->field($model, 'ncDescription') ?>

    <?php // echo $form->field($model, 'release_month') ?>

    <?php // echo $form->field($model, 'end_number') ?>

    <?php // echo $form->field($model, 'subscription_code') ?>

    <?php // echo $form->field($model, 'xpress_id') ?>

    <?php // echo $form->field($model, 'create_user_id') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'pdf_link_life_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
