<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NxPerson */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Nx People', 'url' => ['person/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nx-person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['person/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['person/delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'position',
            'position_type_id',
            'speciality',
            'phone',
            'telcode',
            'address_id',
            'email:email',
            'channel_id',
            'source_id',
            'name',
            'f',
            'i',
            'o',
            'gender',
            'checked',
            'enabled',
            'comment',
            'created',
            'last_updated',
            'create_user_id',
            'last_user_id',
            'label',
            'xpress_id',
            'xpress_type',
        ],
    ]) ?>

</div>
