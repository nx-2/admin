<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "Issue".
 *
 * @property integer $Message_ID
 * @property integer $User_ID
 * @property integer $Subdivision_ID
 * @property integer $Sub_Class_ID
 * @property integer $Priority
 * @property string $Keyword
 * @property integer $Checked
 * @property string $TimeToDelete
 * @property string $TimeToUncheck
 * @property string $IP
 * @property string $UserAgent
 * @property integer $Parent_Message_ID
 * @property string $Created
 * @property string $LastUpdated
 * @property integer $LastUser_ID
 * @property string $LastIP
 * @property string $LastUserAgent
 * @property string $Name
 * @property integer $MagazineID
 * @property string $StartDate
 * @property integer $Number
 * @property string $PublicDate
 * @property string $Picture
 * @property integer $IssueID
 * @property integer $status_id
 * @property string $annotation
 * @property integer $available_online
 * @property string $issue_title
 * @property integer $no_full
 * @property integer $year
 * @property integer $oldID
 * @property string $inCompositionDate
 * @property integer $showDescription
 * @property double $ArticlePrice
 * @property string $ncTitle
 * @property string $ncKeywords
 * @property string $ncDescription
 * @property integer $release_month
 * @property integer $end_number
 * @property string $subscription_code
 * @property integer $xpress_id
 */
class Release extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Issue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Keyword', 'Created', 'LastUser_ID', 'Name', 'StartDate', 'Number', 'PublicDate', 'IssueID', 'inCompositionDate'], 'required'],
            [['User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Priority', 'Checked', 'Parent_Message_ID', 'LastUser_ID', 'MagazineID', 'Number', 'IssueID', 'status_id', 'available_online', 'no_full', 'year', 'oldID', 'showDescription', 'release_month', 'end_number', 'xpress_id'], 'integer'],
            [['TimeToDelete', 'TimeToUncheck', 'Created', 'LastUpdated', 'StartDate', 'PublicDate', 'inCompositionDate'], 'safe'],
            [['annotation', 'issue_title', 'ncDescription'], 'string'],
            [['ArticlePrice'], 'number'],
            [['Keyword', 'UserAgent', 'LastUserAgent', 'Name', 'Picture', 'ncTitle', 'ncKeywords', 'subscription_code'], 'string', 'max' => 255],
            [['IP', 'LastIP'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Message_ID' => 'Message  ID',
            'User_ID' => 'User  ID',
            'Subdivision_ID' => 'Subdivision  ID',
            'Sub_Class_ID' => 'Sub  Class  ID',
            'Priority' => 'Priority',
            'Keyword' => 'Keyword',
            'Checked' => 'Checked',
            'TimeToDelete' => 'Time To Delete',
            'TimeToUncheck' => 'Time To Uncheck',
            'IP' => 'Ip',
            'UserAgent' => 'User Agent',
            'Parent_Message_ID' => 'Parent  Message  ID',
            'Created' => 'Created',
            'LastUpdated' => 'Last Updated',
            'LastUser_ID' => 'Last User  ID',
            'LastIP' => 'Last Ip',
            'LastUserAgent' => 'Last User Agent',
            'Name' => 'Name',
            'MagazineID' => 'Magazine ID',
            'StartDate' => 'Start Date',
            'Number' => 'Number',
            'PublicDate' => 'Public Date',
            'Picture' => 'Picture',
            'IssueID' => 'Issue ID',
            'status_id' => 'Status ID',
            'annotation' => 'Annotation',
            'available_online' => 'Available Online',
            'issue_title' => 'Issue Title',
            'no_full' => 'No Full',
            'year' => 'Year',
            'oldID' => 'Old ID',
            'inCompositionDate' => 'In Composition Date',
            'showDescription' => 'Show Description',
            'ArticlePrice' => 'Article Price',
            'ncTitle' => 'Nc Title',
            'ncKeywords' => 'Nc Keywords',
            'ncDescription' => 'Nc Description',
            'release_month' => 'Release Month',
            'end_number' => 'End Number',
            'subscription_code' => 'Subscription Code',
            'xpress_id' => 'Xpress ID',
        ];
    }
}
