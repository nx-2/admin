<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use common\models\NxAddress;
// use backend\modules\admin\models\AddressSearch;
use backend\controllers\SiteController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use backend\controllers\behaviors\Autocomplete;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * AddressController implements the CRUD actions for NxAddress model.
 */
class AddressController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                parent::behaviors()['access'],
                [
                    'rules' => [
                        //залогиненым разрешить кое что только
                        [
                            'actions' => ['autocomplete', 'create', 'update', 'ValidateAddress'],
                            'allow' => true,
                            'roles' => ['@'],
                            // 'matchCallback' => function ($rule, $action) {
                            //     $allowedRoles = ['publisher'];
                            //     $allowed = Yii::$app->user->identity->isRole($allowedRoles);
                            //     return (boolean)$allowed;
                            // },
                        ],
                    ],
                ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'autocomplete' => [
                'class' => Autocomplete::className(),
                'modelName' => 'NxAddress',
                // 'idFieldName' => 'Country_ID',
                'labelFieldName' => 'address',
            ],
        ];
    }
    public function actionValidateAddress()
    {
    	$model = new NxAddress();
    	if ($model->load(Yii::$app->request->post()) &&  Yii::$app->request->isAjax) {
    	    Yii::$app->response->format = 'json';
    	    return ActiveForm::validate($model);
    	}
    }
    /**
     * Lists all NxAddress models.
     * @return mixed
     */
    // public function actionIndex()
    // {
        // throw new NotFoundHttpException('fdsfsdf');
        // throw new \yii\web\ForbiddenHttpException('Извините, но вам сюда нельзя!'); 

        
        // $searchModel = new AddressSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    // }

    /**
     * Displays a single NxAddress model.
     * @param integer $id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new NxAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($formName = 'address-form')
    {
        $model = new NxAddress();
        $model->scenario = 'create';

        if (empty($formName))
            $formName = 'address-form';

        switch (Yii::$app->request->isAjax) {
            case true:
                return $this->ajaxEdit($model, $formName);
                break;
            case false:
                return $this->edit($model, $formName);
                break;
        }
    }

    /**
     * Updates an existing NxAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $formName = 'address-form')
    {
        $model = $this->findModel($id);

        if (empty($formName))
            $formName = 'address-form';

        switch (Yii::$app->request->isAjax) {
            case true:
                return $this->ajaxEdit($model, $formName);
                break;
            case false:
                return $this->edit($model, $formName);
                break;
        }
    }
    
    private function edit($model, $formName)
    {
        $data = ['model' => $model, 'formName' => $formName];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $showMessage = ['type' => 'success', 'text' => 'Успешно сохранено'];
        }
        if (isset($showMessage))
            $data['showMessage'] = $showMessage;

        $viewName = $model->isNewRecord ? 'create' : 'update';
        return $this->render($viewName, $data);
    }
    
    private function ajaxEdit($model, $formName = NULL)
    {
        $data = ['model' => $model, 'formName' => $formName];

        Yii::$app->response->format = 'json';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $showMessage = ['type' => 'success', 'text' => 'Успешно сохранено'];
        } 
        $data['submitAjax'] = true;
        if (isset($showMessage))
            $data['showMessage'] = $showMessage;

        return \yii\helpers\Json::encode([
                    'html'      => $this->renderAjax('_form', $data), 
                    'callback'  => 'afterAddressSubmitted',
                    'model'     => ['errors' => $model->getErrors(), 'attributes' => $model->getAttributes()]
                ]);
    }

    /**
     * Returns data for autocomplete widget.
     * @return json
     */
    public function actionAutocomplete()
    {
        return $this->autocompleteRequest();
    }

    /**
     * Deletes an existing NxAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->actionIndex();
    //     // return $this->redirect(['index']);
    // }

    /**
     * Finds the NxAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NxAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NxAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
