<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$this->title = 'Обновить издание: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Список журналов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['update', 'id' => $model->Message_ID]];
$this->params['breadcrumbs'][] = '';
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li ><a href="/edition" data-toggle_="tab" aria-expanded="true">Список</a></li>
        <li ><a href="/edition/create" data-toggle_="tab" aria-expanded="false">Новое издание</a></li>

        <li class="pull-left header">
            <i class="fa fa-th"></i>
            <h1 style='display:inline-block;'>
                <?= Html::encode($this->title); ?>
            </h1>
        </li>
    </ul>
    <div class="tab-content">
<div id='company-form-wrapper'>
    <div class='box box-primary nx-company-form'>

        <div class='box-header with-border'>

        </div>
        <div class='box-body'>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#edition-edit-general" data-toggle="tab" aria-expanded="false">Общее</a>
                    </li>
                    <li><a href="#edition-edit-passport" data-toggle="tab" aria-expanded="true">Паспорт издания</a></li>
                    <li><a href="#edition-edit-period" data-toggle="tab" aria-expanded="true">Периоды подписки</a></li>
                    <li><a href="#edition-edit-subscribe-form" data-toggle="tab" aria-expanded="true">Форма подписки</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="edition-edit-general">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>


                                    <div class="edition-form">

                                        <?= $this->render('_form', [
                                            'model' => $model,

                                        ]) ?>



                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="edition-edit-passport">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <?= $this->render('_form_passport', [
                                    'model' => $modelPassport,
                                    'modelEdition' => $model,
                                ]) ?>


                            </div>
                        </div>
                    </div>
                    <div class='tab-pane' id='edition-edit-period'>
                        <div class="box">
                            <div class='box-body no-padding'>
                                <?= $this->render('_form_period', [
                                    'model' => $modelPeriod,
                                    'modelEdition' => $model,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class='tab-pane' id='edition-edit-subscribe-form'>
                        <div class="box">
                            <div class='box-body no-padding'>
                                <?php 
                                    $embededCode = $model->getFormEmbedingCode(Yii::$app->user->identity->publisher_id);
                                    echo(Html::label('Код для вставки подписной формы на страницы сайта', 'embed-form-code', ['class' => 'control-label']));
                                    echo(Html::textarea('',$embededCode , ['id' => 'embed-form-code','class' => 'form-control','rows' => 2]));
                                ?>
                                <?php echo($embededCode); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


    </div>









