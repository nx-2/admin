<?php

use yii\db\Migration;

class m170113_080718_change_description_field_size extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `ads`.`ads_places` CHANGE COLUMN `description` `description` VARCHAR(512) NULL DEFAULT '' ;");

    }

    public function down()
    {
        echo "m170113_080718_change_description_field_size cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
