<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
<a href='/subscribe/payDoc/<?=$model->hash?>/1'>
    <img src='http://www.osp.ru/sites/subscribe/images/icon/PD-4sb.png'/>
    <span class='db'>Квитанция (Сбербанк)</span>
</a>
