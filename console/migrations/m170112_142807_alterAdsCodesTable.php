<?php

use yii\db\Migration;

class m170112_142807_alterAdsCodesTable extends Migration
{
    public function init()
    {
          $this->db = 'ads_db';
          parent::init();
    }    
    public function up()
    {

        /** 
        ALTER TABLE `ads`.`ads_codes` 
        DROP FOREIGN KEY `fk_ads_codes_places`;

        ALTER TABLE `ads`.`ads_codes` 
        DROP INDEX `fk_ads_codes_places_idx` ;

        ALTER TABLE `ads`.`ads_places` 
        ADD COLUMN `code_id` INT(11) NULL DEFAULT NULL AFTER `enabled`,
        ADD INDEX `fk_ads_places_ads_codes_idx` (`code_id` ASC)
        
        ALTER TABLE `ads`.`ads_places` 
        ADD CONSTRAINT `fk_ads_places_ads_codes`
          FOREIGN KEY (`code_id`)
          REFERENCES `ads`.`ads_codes` (`id`)
          ON DELETE NO ACTION
          ON UPDATE CASCADE
        **/
        $options = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $this->dropForeignKey('fk_ads_codes_ads_places', 'ads_codes');
        $this->dropIndex('fk_ads_codes_ads_places_idx', 'ads_codes');
        $this->dropColumn('ads_codes','place_id');
        
        $this->addColumn('ads_places', 'code_id', 'INT(11) NULL DEFAULT NULL AFTER `enabled');
        $this->createIndex('fk_ads_places_ads_codes_idx', 'ads_places', 'code_id');

        $this->execute('SET foreign_key_checks = 0');

        $this->addForeignKey('fk_ads_places_ads_codes', 'ads_places', 'code_id', 'ads_codes', 'id', 'NO ACTION', 'CASCADE');

        $this->execute('SET foreign_key_checks = 1');
        

    }

    public function down()
    {
        echo "m170112_142807_alterAdsCodesTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
