<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@commonrun' => '@common/runtime',
    ],
    'language' => 'ru',
    'charset' => 'utf-8',
    'timeZone' => 'Etc/GMT-3',
    // 'timeZone' => 'UTC+3',
    'bootstrap' => [
        'queue','log',
    ],
    'components' => [
        'assetManager' => [
            //append time stamps to assets for cache busting
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'db' => 'db', // Компонент подключения к БД или его конфиг
//            'tableName' => '{{%queue}}', // Имя таблицы
            'tableName' => 'queue', // Имя таблицы
            'channel' => 'default', // Выбранный для очереди канал
            'mutex' => \yii\mutex\MysqlMutex::class, // Мьютекс для синхронизации запросов
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=' . getenv('MYSQL_NX2_HOST') . ';dbname=' . getenv('MYSQL_NX2_DBNAME'),
            'username' => getenv('MYSQL_NX2_USER'),
            'password' => getenv('MYSQL_NX2_PWD'),
            'charset' => 'utf8',
        ],
        'logs' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=' . getenv('MYSQL_LOG_HOST') . ';dbname=' . getenv('MYSQL_LOG_DBNAME'),
            'username' => getenv('MYSQL_LOG_USER'),
            'password' => getenv('MYSQL_LOG_PWD'),
            'charset' => 'utf8',
        ],
//        'ads_db' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=mysql;dbname=ads',
//            'username' => '',
//            'password' => '',
//            'charset' => 'utf8',
//        ],
//        'pay_db' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=mysql;dbname=payosp',
//            'username' => '',
//            'password' => '',
//            'charset' => 'utf8',
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'class' => ['class' => 'yii\swiftmailer\Mailer', 'messageClass' => 'yii\swiftmailer\Message'],
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,

             'transport' => [
               'class' => 'Swift_SmtpTransport',
//               'constructArgs' => ['stove.opensystems.ru', 475],

//               'constructArgs' => [getenv('MAIL_SERVER_HOST'), getenv('MAIL_SERVER_PORT')],
//               'host' => getenv('MAIL_SERVER_HOST'),
//               'username' => getenv('MAIL_SERVER_USER'),
//               'password' => getenv('MAIL_SERVER_PWD'),
//               'port' => getenv('MAIL_SERVER_PORT'),
//               'encryption' => getenv('MAIL_SERVER_ENCRYPTION'),

                // параметры почтового сервиса индивидуальны для каждого издателя, хранятся в БД
                // (см. таблицы publisher_property, publisher_property_type, publisher_property_group)
                // Mailer конфигурируется на лету по мере надобности в \common\custom_components\CustomHelpers::configureMailerComponent
               'constructArgs' => [],
               'host' => '',
               'username' => '',
               'password' => '',
               'port' => '',
               'encryption' => '',
             ],
        ],
//        'cache' => [
//            'class' => 'yii\caching\MemCache',
//            'servers' => [
//                [
//                    'host' => 'memcached',
//                    'port' => 11211,
//                    'weight' => 100,
//                ],
//            ],
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cacheFileSuffix' => 'cache',
            'cachePath' => '@commonrun/common_cache',
            'keyPrefix' => 	'commonCache',
        ],
        'imageResizer' => [
        	'class' => 'common\custom_components\ImageResizer',
        ],
        'customHelpers' => [
            'class' => 'common\custom_components\CustomHelpers',
        ],
        'formatter' => [
            'dateFormat' => 'd.MM.Y',
            'timeFormat' => 'H:mm:ss',
            'datetimeFormat' => 'd.MM.Y H:mm:ss',
            'decimalSeparator' => '.',
            'currencyCode' => 'RUB',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['info'],
                    'categories' => ['yandexKassaInteraction'],
                    'except' => ['application'],
                    'db' => 'logs',
                    'logTable' => 'yandex',
                    'prefix' => function($message) {
                        $ip = 'unknown';
                        $ipAddrKeys = ['HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR'];
                        foreach ( $ipAddrKeys as $k ) {
                            if (isset($_SERVER[$k]) && !empty($_SERVER[$k])) {
                                $ip = $_SERVER[$k];
                                break;
                            }
                        }
//                        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown';
                        return gmdate("d-m-Y \T H:i:s\Z") . ' UTC, IP:' . $ip;
                    },
//                    'messages' => ['timestamp', 'message'],
                    'logVars' => [],// ['_POST', '_SERVER'],
                    'exportInterval' => 1,
                ],
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['info'],
                    'categories' => ['APIyandexKassaInteraction'],
                    'except' => ['application'],
                    'db' => 'logs',
                    'logTable' => 'yandex_api',
                    'prefix' => function($message) {
//                        $_SERVER['HTTP_X_FORWARDED_FOR']
                        $ipAddrKeys = ['HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR'];
                        $ip = 'unknown';
                        foreach ( $ipAddrKeys as $k ) {
                            if (isset($_SERVER[$k]) && !empty($_SERVER[$k])) {
                                $ip = $_SERVER[$k];
                                break;
                            }
                        }
//                        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown';
                        return gmdate("d-m-Y \T H:i:s\Z") . ' UTC, IP:' . $ip;
                    },
//                    'messages' => ['timestamp', 'message'],
                    'logVars' => [],// ['_POST', '_SERVER'],
                    'exportInterval' => 1,
                ],
            ],
        ],
        'authManager' => [
            'class' => \yii\rbac\PhpManager::class,
            'itemFile' => '@common/rbac/items.php',
            'assignmentFile' => '@common/rbac/assignments.php',
            'ruleFile' => '@common/rbac/rules.php',
        ],
//        'container' => [
//            //here can be connfiguration of DI
//        ],
    ],
];
