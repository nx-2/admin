<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    // require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
    // require(__DIR__ . '/params-local.php')
);

if (file_exists(__DIR__ . '/../../common/config/params-local.php')) {
    $params = yii\helpers\ArrayHelper::merge(
        $params, 
        require(__DIR__ . '/../../common/config/params-local.php')
    );  
}

if (file_exists(__DIR__ . '/params-local.php')) {
    $params = yii\helpers\ArrayHelper::merge(
        $params, 
        require(__DIR__ . '/params-local.php')
    );  
}

return [
    'id' => 'padmin-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
//            'baseUrl' => 'https://padmin.teamosp.ru/',
            'baseUrl' => getenv('THE_SCHEME') . getenv('THE_ADMIN_HOST'),
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                'company/getinfo/<id:\d+>' => 'company/getinfo',
//                'person/getinfo/<id:\d+>' => 'person/getinfo',
                '<controller:download>/issuefile/<filehash:.+>' => '<controller>/issue-file',
            ],
        ],

    ],
    'params' => $params,
];
