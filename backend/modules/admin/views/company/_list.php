<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div class="publisher-index">

    <p>
        <?= Html::a('Создать новую', Url::to(['company/create']),
            [
                'class' => 'btn btn-success',
                // 'data-url' => '/publisher/create',
            ]
        ); ?>
    </p>

<?php Pjax::begin(); ?>    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'name',
        //'source_id',
        //'channel_id',
        //'org_form',
        [
            'attribute' => 'address',
            'value' => 'address.address',
        ],
      //  'legal_address_id',
        [
            'attribute' => 'contactPerson',
            'value' => 'contactPerson.name',
        ],
        'email:email',
        // 'url:url',
        // 'description:ntext',
        // 'shortname',
        // 'othernames',
        // 'wrongnames',
        // 'sorthash',
        // 'xpressid',
        // 'inn',
        // 'okonh',
        // 'okpo',
        // 'pay_account',
        // 'corr_account',
        // 'kpp',
        // 'bik',
        // 'bank',
        // 'checked',
        // 'enabled',
        // 'ws1c_synced',
        // 'created',
        // 'last_updated',
        // 'create_user_id',
        // 'last_user_id',
        // 'label',
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            // 'template' => '{update}',
            'template' => '{update}',
            // 'controller' => '',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    $url = Url::to(['company/update','id' => $key]);
                    return Html::a(
                        '',
                        $url,
                        [
                            'title' => 'редактировать издателя',
                            'aria-label' => 'редактировать',
                            'data-pjax' => 0,
                            'class' => 'fa fa-edit fa-font-18',

                        ]
                    );
                },
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'vertical-align: middle;'],
            // 'template' => '{update}',
            'template' => '{delete}',
            // 'controller' => '',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a(
                        '' ,
                        Url::to(['company/delete','id' => $key]),
                        [
                            'title' => 'Удалить издателя',
                            'aria-label' => 'Удалить',
                            'data-confirm'=>'Вы уверены, что хотите удалить этого издателя?',
                            'data-method'=> 'post',
                            'data-pjax' => '#publishers-list',
                            'class' => 'fa fa-trash fa-font-18',
                        ]
                    );
                },
            ],
        ],
    ],
]); ?>
<?php Pjax::end(); ?></div>



