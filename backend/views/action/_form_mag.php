<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Action */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="action-form">

    <fieldset <?= (!$model->id) ? "disabled" : "" ?>>
        <?php $form = ActiveForm::begin(['action' => ['createitems'], 'id' => 'forum_post', 'method' => 'post',]); ?>
        <?= $form->errorSummary($modelItems); ?>

        <?php
        echo $form->field($modelItems, 'mag_id')->dropDownList($modelItems::getAllJournals(), [
            'prompt' => Yii::t('app', 'Укажите журнал'),
            'data-trigger' => 'dep-drop',
            'data-target' => '#select-moths',
            'data-url' => \yii\helpers\Url::to(['/action/months']),
            'data-name' => 'journal_id',
            'id' => 'select-journal',
        ]   );
        ?>
        <?= $form->field($modelItems, 'months')->dropDownList([], [
            'prompt' => Yii::t('app', 'Укажите срок подписки'),
            'disabled' => true,
            'id' => 'select-moths',
        ]) ?>


        <?php

        echo $form->field($modelItems, 'type')->widget(Select2::classname(), [
            'data' =>['paper' => 'бумажная версия',
                'pdf' => 'PDF'],
            'language' => 'ru',
            'theme' => 'default', //'default classic bootstrap, krajee'
            'options' => ['placeholder' => 'Укажите тип'],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ]);
        ?>

        <?= $form->field($modelItems, 'discount')->textInput() ?>
        <?= $form->field($modelItems, 'action_id')->hiddenInput(['value' => $model->id])->label(false); ?>


        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </fieldset>
</div>
<?php if (!$model->id): ?>
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Внимание!</h4>
        В начале необходимо сохранить акцию
    </div>
<?php endif; ?>



<?php
if (!empty($model->actionItems)) {

    $query = \common\models\ActionItems::find()->where(['=', 'action_id', $model->id]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            /*         [
                         'attribute' => 'action_id',
                         'value' => 'action.name'
                     ],*/
            ['attribute' => 'mag_id',
                'value' => function ($model, $key, $index) {
                    $name = \common\models\ActionItems::getOneJournals($model->mag_id);
                    return $name['full_name'];
                },
                'format' => 'raw'],

            'months',
            'type',
            'discount',

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Действие',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'delete') {
                        $url = 'delete-items/?id=' . $model->id;
                        return $url;
                    }

                }


            ],
        ],
    ]);
}
?>



    <?php
    $script = <<< JS
$("[data-trigger=dep-drop]").on("change", function() {
    // Собираем данные для отправки в действие контроллера
    var data = {};
    data[$(this).attr("data-name")] = $(this).val();
    // Контейнер для помещения ответа от сервера
    var target = $(this).attr("data-target");
    // Непосредственно отправка запроса на сервер
    $.getJSON(
        $(this).attr("data-url"), //URL
        data, // GET-параметры
        // Обработчик ответа сервера
        function(response, statusCode, xhr) {
            var slct = $(target); // jQuery-объект целевого тега
            slct.empty(); // Очищаем текущие <option>

            // Обходим каждый элемент массива из ответа сервера
            for (el in response) {
                // И добавляем его в конец <select>
                $("<option value=\"" + el + "\">" + response[el] + "</option>").appendTo(slct);
            }

            slct.removeAttr("disabled"); // Удаляем атрибут запрещающий изменение <select>
        }
    );
});
JS;
    $this->registerJs($script, yii\web\View::POS_END);
    ?>


