<?php

use yii\db\Migration;

/**
 * Handles the creation of table `adscodes`.
 */
class m170112_104026_create_adscodes_table extends Migration
{
    public function init()
    {
          $this->db = 'ads_db';
          parent::init();
    }    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = $this->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('ads_codes', $tables))  { 
        if ($dbType == "mysql") {
            $this->createTable('{{%ads_codes}}', [
                'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                0 => 'PRIMARY KEY (`id`)',
                'place_id' => 'INT(11) NOT NULL',
                'head_code_text' => 'TEXT NULL',
                'code_text' => 'TEXT NULL',
                'name' => 'VARCHAR(45) NULL',
                'description' => 'VARCHAR(255) NULL',
                'enabled' => 'TINYINT(1) NULL DEFAULT \'1\'',
                'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ',
            ], $tableOptions_mysql);
        }
        }
         
         
        $this->createIndex('fk_ads_codes_ads_places_idx','ads_codes','place_id',0);
         
        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_ads_codes_ads_places','{{%ads_codes}}', 'place_id', '{{%ads_places}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `ads_codes`');
        $this->execute('SET foreign_key_checks = 1;');

    }
}
