<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace console\controllers;
use common\models\User;
use common\rbac\rule\IsOwnerRule;
use common\services\RbacService;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionCreate()
    {
        $service = new RbacService();
        $service->initialCreate();
        $this->stdout(
            "RBAC created  \n",
            Console::FG_CYAN,
            Console::BOLD
        );

        return ExitCode::OK;
    }

    public function actionReassignUsers()
    {
        $service = new RbacService();
        $service->reassignAllUsers();
        $this->stdout(
            "Roles are assigned to users  \n",
            Console::FG_CYAN,
            Console::BOLD
        );

        return ExitCode::OK;
    }
}


