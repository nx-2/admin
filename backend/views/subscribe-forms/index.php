<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\RelationSystems */

$this->title = 'Платежные формы ';
?>
<div class="relation-systems-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div id='multy-mag-subscribe-wrapper' class="">
        <div class='row'>
            <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='box box-primary pay-system-form'>
                    <div class='box-header with-border'>

                    </div>
                    <div class='box-body'>
                        <div class='row' >
                            <div class='col-md-8'>
                                <div class='form-group'>
                                    <p>Включить в подписную форму следующие издания: </p>
                                    <?php 
                                    $publisher = Yii::$app->user->identity->publisher;
                                    $editions = $publisher->getAllMyActiveEditions();
                                    $editionsList =  yii\helpers\ArrayHelper::map($editions, 'Message_ID', 'HumanizedName');
                                    $editionsList = yii\helpers\ArrayHelper::merge(['all' => 'ВСЕ'], $editionsList );

                                    echo Select2::widget([
                                        'name' => 'eIds',
                                        'value' => 'all',
                                        'data' => $editionsList,
                                        'language' => 'ru',
                                        'theme' => 'default', //'default classic bootstrap, krajee'
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'multiple' => true,
                                        ],
                                        'options' => ['multiple' => true, 'placeholder' => 'Выбрать издания ...', 'id' => 'editionsForForm']
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label class="control-label" for="publisher-name">ширина подписной формы ("auto" = во всю ширину контейнера)</label>
                                    <input type="text" id="subscribeFormWidth" class="form-control" value="auto" placeholder='ширина подписной формы' >
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label class="control-label" id='showCoversWrapper'>
                                        <span style='display:block;margin-bottom: 10px;'>Показывать Обложки?</span>
                                        <input type='radio' name='showCovers' value='1' style='margin-left: 10px;width: 15px;height: 15px;margin-right: 5px;vertical-align: middle;margin-top: 0px;cursor: pointer'> 
                                        <span style='vertical-align: middle;'>ДА</span>
                                        <input type='radio' name='showCovers' value='0' checked style='margin-left: 10px; width: 15px; height: 15px;margin-right: 5px;vertical-align: middle;margin-top: 0px;cursor: pointer'>
                                        <span style='vertical-align: middle;'>НЕТ</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-6'>
                                <div>
                                    <button id='buildCode' class='btn bg-maroon btn-flat' onclick='buildFormCode(event)' >Сгенерировать код</button>
                                    <button id='testForm' class='btn bg-navy btn-flat' onclick='testForm(event)' >Тестировать форму</button>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-12'> 
                                <?php 
                                    echo(Html::label('Код для вставки подписной формы на страницы сайта', 'embed-form-code', ['class' => 'control-label']));
                                ?>
                                <div style='position:relative'>
                                    <?php 
                                        echo(Html::textarea('','' , ['id' => 'multy-mag-form-code','class' => 'form-control','rows' => 2, 'readonly' => true,]));
                                    ?>
                                    <div style='position: absolute;top: 5px;right: 5px;cursor: pointer;'>
                                        <i id='copyToClipboard' class='fa fa-clipboard' title='скопировать в буфер' style='font-size:20px' onclick='copyToClip()'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='box-footer'>
                    <div class="form-group">
                        <div style='margin:0px auto;' id='testFormPlace'></div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript'>
    var formWrapperId = '<?=Yii::$app->params['multyMagFormWrapperId'];?>';
    var apiUrl = '<?=Yii::$app->params['api']['host']?>';
    var publisherId = '<?=Yii::$app->user->identity->publisher_id?>';

    function buildFormCode( event ) {
        var editions = $('#editionsForForm').val();
        if ( !editions ) {
            alert ('Не выбрано ни одного издания!! Это обязательно');
        }
        
        editions = editions.indexOf('all') == -1 ? editions.join(',') : 'all';
        
        var formWidth = $('#subscribeFormWidth').val();
        if ( formWidth != 'auto') {
            formWidth = parseInt(formWidth);
        }
        
        var showCovers = $('#showCoversWrapper input[type="radio"]:checked').val();

        var code = 
                '<div id="' 
                + formWrapperId 
                + '"></div><script type="text/javascript" src="' 
                + apiUrl 
                + '/multymag/js/' 
                + publisherId 
                + '/' 
                + formWidth 
                + '/' 
                + showCovers 
                + '?eIds=' 
                + editions 
                + '"><\/script>';
        $('#multy-mag-form-code').val(code);
        return;
    }
    function testForm( event ) {
        var code = $( $('#multy-mag-form-code').val() );
        if ( !code || code == '' ) {
            return;
        }
        var testPlace = $('#testFormPlace');
        testPlace.html('');
        testPlace.append(code);
        setTimeout(function(){
            <?=Yii::$app->params['multyMagFormWrapperId'];?>Start();    
        },2000);
    }
    function copyToClip() {
        var code = document.getElementById("multy-mag-form-code");
        code.select();
        document.execCommand("Copy");
    }
</script>
