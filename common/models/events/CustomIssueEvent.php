<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\events;
use common\models\Issue;
use \yii\base\Event;

/**
 *  Объек, предназначенный для работы в случаях каких либо кастомных событий
 * генерируемых моделью Issue
 * @property Issue $issue
 *
 */
class CustomIssueEvent extends Event
{
    public $issue;
}