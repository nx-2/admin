<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
?>

<div class="nx-person-index">

    <p>
        <?= Html::a('Создание адреса', [Url::to(['address/create'])], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'country',
                'value' => 'country.Country_Name',
            ],
            //'area',
            //'region',
            'city',
            // 'city1',
            'zipcode',
            'address',
            'street',
            'house',
            'building',
            // 'structure',
            // 'apartment',
            'phone',
            // 'fax',
            // 'comment:ntext',
            // 'telcode',
            // 'post_box',
            // 'is_onclaim',
            // 'is_enveloped',
            // 'last_updated',
            // 'label',
            // 'xpress_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = Url::to(['address/update', 'id' => $key]); //'/address/update?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать адрес',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '' ,
                            Url::to(['address/delete', 'id' => $key]), // '/address/delete?id=' . $key, 
                            [
                                'title' => 'Удалить адрес',
                                'aria-label' => 'Удалить',
                                'data-confirm'=>'Вы уверены, что хотите удалить этого адрес?',
                                'data-method'=> 'post',
                                'data-pjax' => '#publishers-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>