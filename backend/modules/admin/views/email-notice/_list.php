<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div>
    <p>
            <?= Html::a('Создать новый', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'email-notices-list', 'enablePushState' => false]); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//                'id',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'vertical-align: middle;'],
                    // 'template' => '{update}',
                    'template' => '{update}',
                    // 'controller' => '',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            $url = Url::to(['email-notice/update','id' => $key]);
                            return Html::a(
                                '',
                                $url,
                                [
                                    'title' => 'редактировать',
                                    'aria-label' => 'редактировать',
                                    'data-pjax' => 0,
                                    'class' => 'fa fa-gear fa-font-18',

                                ]
                            );
                        },
                    ],
                ],
                'name',
//                'email_subject:email',
                'email_subject',
                'key_string',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'vertical-align: middle;'],
                    // 'template' => '{update}',
                    'template' => '{delete}',
                    // 'controller' => '',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a(
                                '' ,
                                Url::to(['email-notice/delete','id' => $key]),
                                [
                                    'title' => 'Удалить издателя',
                                    'aria-label' => 'Удалить',
                                    'data-confirm'=>'Вы уверены, что хотите удалить этот тип уведомлений? В случае удаления также будут удалены все файлы шаблонов всех пользователей системы, относящиеся к этому типу уведомлений',
                                    'data-method'=> 'post',
                                    'data-pjax' => '#email-notices-list',
                                    'class' => 'fa fa-trash fa-font-18',
                                ]
                            );
                        },
                    ],
                ],
                
                
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
    
