<?php

use yii\db\Migration;

class m170217_094022_updatePromo extends Migration
{
    public function up()
    {
        $mysqlCode = '
            ALTER TABLE `osp`.`Promo_codes_old` 
            ADD COLUMN `order_id` int(11) NOT NULL;
        ';
        $this->execute($mysqlCode);
    }

    public function down()
    {
        echo "m170217_094022_updatePromo cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
