<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = 'Создание новой компании';
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['company/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li ><a href="<?=Url::to(['company/index']); ?>" data-toggle_="tab" aria-expanded="true">Список</a></li>
        <li class="active"><a href="<?=Url::to(['company/create']); ?>" data-toggle_="tab" aria-expanded="false">Новая компания</a></li>

        <li class="pull-left header">
            <i class="fa fa-th"></i>
            <h1 style='display:inline-block;'>
                <?= Html::encode($this->title); ?>
            </h1>
        </li>
    </ul>

<div class="company-create">

    <?php
        $data = ['model' => $model, 'addressModel' => $addressModel];
        if (isset($showMessage))
        	$data ['showMessage']  = $showMessage;
    ?>
    <?= $this->render('_form', $data); ?>

</div>
