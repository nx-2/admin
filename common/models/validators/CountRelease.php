<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\validators;

class CountRelease extends \yii\base\Model
{

    public $period;
    public $magazineID;
    public $date_start;

    public function rules()
    {
        return [
            [['period', 'magazineID', 'date_start'], 'required'],
            [['period', 'magazineID'], 'integer'],
            [['date_start'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }
}