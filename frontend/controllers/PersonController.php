<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\NxPerson as Person;


class PersonController extends ActiveController
{
    public $modelClass = 'common\models\NxPerson';

    public function actionAll()
    {
        $query = Person::find();
        $query->joinWith(['address']);
        $query->joinWith(['company']);
        $query->joinWith(['address']);

	return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],            
        ]);
    }

    public function actionOne($id)
    {

        $columns = [
                        'p.id', 'p.company_id', 'p.position', 'p.speciality', 'p.phone','p.telcode', 'p.address_id', 
                        'p.email', 'p.name', 'p.f', 'p.i', 'p.o', 'p.gender', 'p.comment', 
        ];
        return Person::find()
                ->select($columns)
                ->from('nx_person p')
                ->where(['p.id' => $id])
                ->joinWith(
                [
                        //получить "жадно" adress->Country, company
                        'company c',
                        'address a' => function($q) {
                                $q->joinWith(['country adc' => function($q) {
                                        $q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
                                }]);
                        },
                ])
                ->asArray(true)
                ->one();
	}
}
