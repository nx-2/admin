<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "publisher_property_group".
 *
 * @property integer $id
 * @property string $group_name
 * @property string $icon-name
 * @property string $route
 * @property string $key
 *
 * @property PublisherPropertyType[] $publisherPropertyTypes
 */
class PublisherPropertyGroup extends \common\models\CommonModel
{
    const ACCOUNTING_INTEGRATION = 1; //id записи из этой таблицы для 1с интеграции
    const MAIL_GROUP = 2; //id для настроек почты
    const MAIL_KEY = 'mail';
    const JWT_KEY = 'jwt';
    const POLICY_KEY = 'policy';
    const ACCOUNTING_KEY = '1C';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_property_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_name'], 'required'],
            [['group_name'], 'string', 'max' => 100],
            [['icon-name','key'], 'string', 'max' => 45],
            [['route'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Название группы',
            'icon-name' => 'Имя font awesome иконки',
            'route' => 'Внутренний адрес страницы',
            'key' => 'Служебный ключ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherPropertyTypes()
    {
        return $this->hasMany(PublisherPropertyType::className(), ['group_id' => 'id'])->orderBy('id');
    }
}
