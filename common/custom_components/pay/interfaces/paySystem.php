<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\pay\interfaces;

/**
 *
 * @author andrey
 */
interface paySystem {
    
    public function __construct($data);

    /** 
    * метод обрабатывает запросы от платежных систем о подтверждении зявки на оплату заказа
    * предполагается, что плат. система до осуществления платежа отправляет запрос с данными о предполагаемом платеже, 
    * в ответ на который мы можем подтвердить или отказать (тогда оплата будет прервана) 
    **/
    public function checkOrder();
    
    /** 
    * метод обрабатывает запросы от платежных систем об отмене оплаты
    * предполагается, что плат. система до осуществления платежа отправляет запрос с данными о несостоявшемся платеже, 
    * 
    **/
    public function cancelOrder();
    
    /** 
    * метод обрабатывает запросы от платежных систем уведомляющие о состоявшемся платеже
    * предполагается, что плат. система по результатам осуществления платежа, отправляет запрос, содержащий всю информацию о платеже 
        * 
    **/
    public function paymentNotify();
    
    /** 
     * логирует запрос от платежной системы и ответ ей тоже
     **/
    public function log($data);
    
}
