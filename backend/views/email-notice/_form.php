<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\EmailNotice */
/* @var $form yii\widgets\ActiveForm */
?>
<div id='email-template-modal-wrapper'></div>
<div id='email-notice-form-wrapper' class='box box-primary email-notice-form'>
    <div class='box-header with-border'>
    </div>
    <div class='box-body'>
        <div class="row_">
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <div class="form-group">
                    <?php echo(Html::tag('label', $model->getAttributeLabel('name'), ['style' => 'display: block;', 'class' => 'control-label'])); ?>
                    <?php echo(Html::tag('span', $model->name, ['style' => 'display: block;border: 1px solid #d2d6de;padding: 6px 12px;'])); ?>
                </div>                
                <div class="form-group">
                    <?php echo(Html::tag('label', $model->getAttributeLabel('email_subject'), ['style' => 'display: block;', 'class' => 'control-label'])); ?>
                    <?php echo(Html::tag('span', $model->email_subject, ['style' => 'display: block;border: 1px solid #d2d6de;padding: 6px 12px;'])); ?>
                </div>                
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <h3>Выберете подходящий шаблон:</h3>
                <input type="hidden" id="publishernoticetemplate-publisher_id" class="form-control" name="PublisherNoticeTemplate[publisher_id]" value="35">
                <input type="hidden" id="publishernoticetemplate-notice_id" class="form-control" name="PublisherNoticeTemplate[notice_id]" value="2">
                <input type="hidden" id="publishernoticetemplate-template_id" class="form-control" name="PublisherNoticeTemplate[template_id]" value="3">
                <?php 
                    if ($relatedPublisherTemplate !== null) {
                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-publisher_id', 'name' => 'PublisherNoticeTemplate[publisher_id]','value' => $relatedPublisherTemplate->publisher_id ]));
                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-notice_id', 'name' => 'PublisherNoticeTemplate[notice_id]','value' => $relatedPublisherTemplate->notice_id ]));
//                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-template_id', 'name' => 'PublisherNoticeTemplate[template_id]','value' => $relatedPublisherTemplate->template_id ]));
                        $templateId = $relatedPublisherTemplate->template_id;
                    } else {
                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-publisher_id', 'name' => 'PublisherNoticeTemplate[publisher_id]','value' => $publisher->id ]));
                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-notice_id', 'name' => 'PublisherNoticeTemplate[notice_id]','value' => $model->id ]));
//                        echo(Html::tag('input','' , ['type' => 'hidden', 'id' => 'publishernoticetemplate-template_id', 'name' => 'PublisherNoticeTemplate[template_id]','value' => '' ]));
                        $templateId = '';
                    }
                    if ( !empty($model->emailNoticeTemplates) ) {
                        foreach( $model->emailNoticeTemplates as $template ) {
                            $span = Html::tag('span', $template->name, ['style' => 'vertical-align: top;margin-right:10px']);
                            $radioLabel = Html::label('', 'template_' . $template->id, ['class'=> 'radio-check-label', 'style' => 'vertical-align: middle;margin:0px 10px 0px 0px;']);
                            $radioInput = Html::radio('PublisherNoticeTemplate[template_id]', $template->id == $templateId, ['id' => 'template_' . $template->id, 'value' => $template->id,]);
                            $showLink = Html::a('Посмотреть', '#showTemplate', ['onclick' => 'showTemplateContent(event)', 'data-id' => $template->id, 'style' => 'vertical-align: top;']);
                            $wrapDiv = Html::tag('div', $radioInput . $radioLabel . $span.  $showLink, ['class' => 'radio-check-wrap', 'style' => 'text-align: left;line-height: 30px;}']);
//                            echo(Html::label($template->name, 'template_' . $template->id , []));
//                            echo(Html::radio('PublisherNoticeTemplate[template_id]', $template->id == $templateId, ['id' => 'template_' . $template->id, 'value' => $template->id]));
//                            echo(Html::a('Посмотреть', '#showTemplate', ['onclick' => 'showTemplateContent(event)', 'data-id' => $template->id]));
//                            echo(Html::tag('div'));
                            echo($wrapDiv);
                        }
                    } else {
                        echo(Html::tag('h3', 'Извините... Но для этого типа Email оповещения пока что отсутствуют какие-либо шаблоны. Администраторы системы работают над этим:)..', []));
                    }
                ?>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding: 0px 25px;margin-top: 20px;border-top: 1px solid rgba(0,0,0,0.1);padding-top: 20px;background: rgba(0,0,0,0.05);padding-bottom: 20px;'>
                <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class='box-footer'>
    </div>
    <script type="text/javascript">
    function showTemplateContent(event){
        if ( typeof event == 'undefined' ) {
            event = window.event;
        }
        
        event.preventDefault();
        $.ajax({
            url : '<?php echo( Url::to(['email-notice/get-template-html'], Yii::$app->request->getIsSecureConnection() ) ); ?>' + '/' + $(event.target).data('id'),
            type: 'get',
            dataType : 'json',
        }).done(function(response){
            if (response.status == 'OK') {
                $('#email-template-modal-wrapper').html(response.html);
                var modal = $('#email-template-modal-wrapper #email-notice-template-modal');
                if (modal.length > 0) {
                    modal.on('shown.bs.modal', function (event) {
                        setIframeSizes();
                    })
                    modal.modal('show');
                }
            } else {
                if ( response.hasOwnProperty('errMessage') ) {
                    alert( response.errMessage );
                }
            }
        })
        .fail(function(a,b,c){
            alert('что-то пошло не так. См. консоль');
            console,log(a);
            console,log(b);
            console,log(c);
        });
        return false;
    }
    function setIframeSizes( w, h ){
        w = !w ? 700 : w;
        h = !h ? false : h;
        
        var iframe = $('#popupTemplateIframe');
        iframe.width(w + 'px');
        if ( h == false ) {
            var iframeBody = iframe.contents().find('body');
            h = iframeBody.height() + 20;
        }
        iframe.height(h + 'px');
    }
</script>
