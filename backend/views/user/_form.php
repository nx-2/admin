<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\create_update_fields\CreateUpdateFields;
use backend\views\widgets\image_field\ImageField;
use yii\jui\AutoComplete;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div id='user-form-wrapper'>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id' => 'user-form']]); ?>
    <?php $form->errorSummaryCssClass = 'alert alert-warning alert-dismissible'; ?>
    <div class='box box-primary user-form'>
        <div class='box-header with-border'>
            <?php echo($model->FullName); ?>
        </div>
        <div class='box-body'>
            <div>
                <?=$form->errorSummary($model,['header' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>']); ?>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#user-edit-required" data-toggle="tab" aria-expanded="true">Основное <sup style="color: red; font-size: 16px">*</sup> </a></li>
                    <li><a href="#user-edit-avatar" data-toggle="tab" aria-expanded="false">Аватар</a></li>
                    <li><a href="#user-edit-personal" data-toggle="tab" aria-expanded="false">Личные данные</a></li>
                    <li><a href="#user-edit-address" data-toggle="tab" aria-expanded="false">Адрес</a></li>
                    <?php if(!$model->isNewRecord): ?>
                        <li><a href="#user-edit-access" data-toggle="tab" aria-expanded="false">Доступ</a></li>
                    <?php endif; ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="user-edit-required">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-3 col-sm-3 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-3 col-sm-3 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'FirstName')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'FullName')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class='col-md-3 col-sm-3 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'Login')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-3 col-sm-3 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?php
//                                            if (Yii::$app->getUser()->getIdentity()->isAdmin()) {
                                            if (Yii::$app->getUser()->getIdentity()->isRole(['admin', 'developer'])) {
//                                                $roles = \common\models\NxRole::find()->orderBy('name DESC')->all();
                                                $roles = \common\models\NxRole::find()->orderBy('name DESC')->all();
                                            } elseif (Yii::$app->getUser()->getIdentity()->isRole(['publisher'])) {
                                                $roles = \common\models\NxRole::getCommonAccessableRoles();
                                            } else {
//                                                $roles = \common\models\NxRole::find()->where('code <> "admin"')->orderBy('name DESC')->all();
//                                                $roles = \common\models\NxRole::getSafeAccessableRoles();
                                                $roles = false;
                                            }
                                            if (!empty($roles)) {
                                                echo $form->field($model, 'rolesIds')->widget(Select2::classname(), [
                                                    'data' => \yii\helpers\ArrayHelper::map($roles,'id','name'),
                                                    'language' => 'ru',
                                                    'theme' => 'default', //'default classic bootstrap, krajee'
                                                    'options' => ['placeholder' => 'Выберете роли...'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'multiple' => true,
                                                    ],
                                                ]);
                                            }
                                            ?>
                                        </div>
                                        <div class='col-md-2 col-sm-2 col-xs-12' style='padding:0px 25px'>
                                            <?php
                                            if ($model->isNewRecord) {
                                                $model->publisher_id = Yii::$app->user->identity->publisher_id;
                                                echo (yii\helpers\Html::activeHiddenInput ( $model, 'publisher_id', $options = ['id' => 'publisher-id',]));
                                            }
                                            ?>
                                            <?php if (!$model->hasErrors() && $model->isNewRecord) {$model->Checked = 1;} ?>
                                            <?= $form->field($model, 'Checked', ['labelOptions'=>[]])->checkbox() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                            <?php if($model->isNewRecord): ?>
                                                <div class="box box-primary box-solid">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            Задать пароль
                                                        </h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="box-body" style="display: block;">
                                                        <?php
                                                        $fieldOptions = ['labelOptions' => ['style' => 'display:none']];
                                                        $inputOptions = ['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Пароль','value' => '',];
                                                        echo($form->field($model,'Password',$fieldOptions)->passwordInput($inputOptions));
                                                        $inputOptions['placeholder'] = 'Пароль повторно';
                                                        echo($form->field($model,'Password_repeat',$fieldOptions)->passwordInput($inputOptions));
                                                        echo(Html::tag('span','Сгенерировать',['class' => 'btn btn-primary pull-right', 'onclick' => 'createRandomPassword()']));
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <div class="box box-primary box-solid collapsed-box">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">
                                                            Изменить пароль
                                                        </h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="box-body" style="display: none;">
                                                        <?php
                                                        $fieldOptions = ['labelOptions' => ['style' => 'display:none'],'validateOnType' => true];
                                                        $inputOptions = ['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Новый пароль','value' => '',];
                                                        echo($form->field($model,'ResetPassword',$fieldOptions)->passwordInput($inputOptions));
                                                        $inputOptions['placeholder'] = 'Новый пароль повторно';
                                                        echo($form->field($model,'Password_repeat',$fieldOptions)->passwordInput($inputOptions));
                                                        echo(Html::tag('span','Сгенерировать',['class' => 'btn btn-primary pull-right', 'onclick' => 'createRandomPassword()']));
                                                        ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>

                    <div class="tab-pane" id="user-edit-personal">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'MidName')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'birthday')->textInput(['data-inputmask'=>'"mask": "d.m.y"', 'data-mask' => '','value' => $model->birthday !== null ?  date('d.m.Y',strtotime($model->birthday)) : '', ]) ?>
                                            <?= $form->field($model, 'sex',['labelOptions'=>[]])->dropDownList([ 0 => 'жен.', 1 => 'муж.', '' => 'не указ.', ], ['prompt' => '','class' => 'form-control']) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'Language')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'data-inputmask'=>'"mask": "+7 (999) 999-99-99"', 'data-mask' => '']) ?>
                                            <?= $form->field($model, 'fax')->textInput(['maxlength' => true,'data-inputmask'=>'"mask": "+7 (999) 999-99-99"', 'data-mask' => '']) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'job')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'Text')->textarea(['rows' => 2]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>
                    <div class="tab-pane" id="user-edit-address">
                        <div class="box">
                            <div class='box-body no-padding'>

                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'country',['labelOptions'=>[]])->widget(AutoComplete::classname(), [
                                                    'options' => [
                                                        'placeholder' => 'Начните набирать...',
                                                        'class' => 'form-control ',
                                                        'name' => '',
                                                        'id' => 'country_name',
                                                        'value' =>  $model->usercountry === null ? '' : $model->usercountry->Country_Name,
                                                    ],
                                                    'clientOptions' => [
                                                        'source' =>  Url::to(['country/autocomplete']),
                                                        'autoFill'=>false,
                                                        'minLength'=>'3',
                                                        'appendTo' => 'country_name',
                                                        'delay' => '300',
                                                        'select' => new yii\web\JsExpression('function(event, ui){$("#country_id").val(ui.item.id);}'),
                                                        'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#country_id").val("");}}'),
                                                        // 'disabled' => true,

                                                    ],
                                            ]) ?>
                                            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'country', $options = ['id' => 'country_id',])); ?>

                                            <?= $form->field($model, 'region',['labelOptions'=>[]])->widget(AutoComplete::classname(), [
                                                    'options' => [
                                                        'placeholder' => 'Начните набирать...',
                                                        'class' => 'form-control ',
                                                        // 'name' => '',
                                                        'id' => 'region-name',
                                                        'value' => $model->useregion === null ? '' : $model->useregion->RegionUser_Name,
                                                    ],
                                                    'clientOptions' => [
                                                        'source' => Url::to(['region/autocomplete']),
                                                        'autoFill'=>false,
                                                        'minLength'=>'3',
                                                        'appendTo' => 'region-name',
                                                        'delay' => '300',
                                                        'select' => new yii\web\JsExpression('function(event, ui){$("#region-id").val(ui.item.id);}'),
                                                        'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#region-id").val("");}}'),
                                                        // 'disabled' => true,

                                                    ],
                                            ]) ?>
                                            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'region', $options = ['id' => 'region-id',])); ?>

                                            <?= $form->field($model, 'district')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'index_num')->textInput(['maxlength' => true,'data-inputmask'=>'"mask": "9","repeat" : "6"', 'data-mask' => '']) ?>
                                            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'building')->textInput(['maxlength' => true]) ?>
                                            <?= $form->field($model, 'flat')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class='col-md-12 col-sm-12 col-xs-12'>
                                            <?= $form->field($model, 'address')->textarea(['rows' => 2,'readonly' =>true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div> <!--END #user-edit-address -->

                    <div class="tab-pane" id="user-edit-access">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= CreateUpdateFields::widget(['model' => $model, 'lastUpdatedFieldName' => 'LastUpdated', 'createdFieldName' => 'Created']); ?>
                                            <?= $form->field($model, 'LastLoginDate')->textInput(['readonly' => true, 'value' => !empty($model->LastLoginDate) ?  Yii::$app->formatter->asDatetime($model->LastLoginDate) : '']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div> <!--END #user-edit-company -->
                    <div class="tab-pane" id="user-edit-avatar">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                                            <?php
                                            echo $form->field($model, 'avatarFile')->widget(FileInput::classname(), [
                                                'options' => ['accept' => 'image/*'],
                                            ]);
                                            ?>
                                            <?php if(!empty($model->avatar)): ?>
                                                <?= ImageField::widget(['model' => $model, 'imageFieldName' => 'avatar']); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>

                </div>
            </div>
        </div>
        <div class='box-footer'>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?php if(!$model->isNewRecord): ?>
                    <?=Html::a('Удалить', Url::to(['user/delete','id' => $model->id]), ['class' => 'btn btn-danger pull-right', 'onclick' => 'confirmDelete()','title' => 'Удалить этого пользователя']); ?>
                    <script type="text/javascript">
                        function confirmDelete(event){
                            if (typeof(event) == 'undefined')
                                event = window.event;

                            if (confirm('Вы действительно желаете удалить этого пользователя?')) {
                                return true;
                            } else {
                                event.preventDefault();
                                return false;
                            }
                        }
                    </script>
                <?php endif; ?>
            </div>
        </div><!--End bos-footer  -->
    </div> <!--END box -->
    <?php ActiveForm::end(); ?>
</div><!--End #person-form-wrapper -->
<script type="text/javascript">
    if (window.addEventListener) // W3C standard
    {
      window.addEventListener('load', addNamePartsListener, false);
      window.addEventListener('load', addPersonPartsListener, false);
      window.addEventListener('load', runAfterLoad, false);
    }
    else if (window.attachEvent) // Microsoft
    {
      window.attachEvent('onload', addNamePartsListener);
      window.attachEvent('onload', addPersonPartsListener);
      window.attachEvent('onload', runAfterLoad);
    }
    function addNamePartsListener() {
        var selectors = '#user-form-wrapper #user-lastname, #user-form-wrapper #user-firstname, #user-form-wrapper #user-midname';
        $('body').on('change', selectors, function(event){
            if (typeof event == 'undefined')
                event = window.event;

            var fullName = $('#user-form-wrapper #user-lastname').val() + ' ' + $('#user-form-wrapper #user-firstname').val() + ' ' + $('#user-form-wrapper #user-midname').val();
            $('#user-form-wrapper #user-fullname').val(fullName);
        });

    }

    function addPersonPartsListener() {
        var selectors = '#user-form-wrapper #country_name, #user-form-wrapper #region-name, #user-form-wrapper #user-district, #user-form-wrapper #user-index_num, #user-form-wrapper #user-city, #user-form-wrapper #user-street, #user-form-wrapper #user-house, #user-form-wrapper #user-building, #user-form-wrapper #user-flat';

        $('body').on('change', selectors, function(event){
            if (typeof event == 'undefined')
                event = window.event;
            var parts = [];

            var country     = $('#user-form-wrapper #country_name').val(); country  = country != '' ? country + ', ' : '';parts.push(country);
            var region      = $('#user-form-wrapper #region-name').val(); region = region != '' ? region + ', ' : '';parts.push(region);
            var district    = $('#user-form-wrapper #user-district').val();district = district != '' ? district + ' район, ' : '';parts.push(district);
            var postCode    = $('#user-form-wrapper #user-index_num').val();postCode = postCode != '' ? postCode + ', ' : '';parts.push(postCode);
            var city        = $('#user-form-wrapper #user-city').val(); city = city != '' ? city + ', ' : '';parts.push(city);
            var street      = $('#user-form-wrapper #user-street').val();street = street != '' ? 'ул. ' + street + ', ' : '';parts.push(street);
            var house       = $('#user-form-wrapper #user-house').val();house = house != '' ? 'д.' + house + ', ' : '';parts.push(house);
            var building    = $('#user-form-wrapper #user-building').val();building = building != '' ? 'корп.' +  building + ', ' : '';parts.push(building);
            var flat        = $('#user-form-wrapper #user-flat').val();flat = flat != '' ? 'кв.' + flat + ', ' : '';parts.push(flat);

            var fullAddress = parts.join('');

            $('#user-form-wrapper #user-address').val(fullAddress);
        });
    }
    function runAfterLoad(){
        $('#user-form').on('afterValidateAttribute',function(event,attribute,messages){
            if (attribute.name != 'Password_repeat') {
                return;
            }
            $('#user-form').yiiActiveForm('validateAttribute','user-resetpassword');
        })
    }
    function createRandomPassword(){
        const pwd = genPassword();
        $('#user-password, #user-password_repeat, #user-resetpassword').prop('value', pwd);
    }
    function genPassword() {
        const data = {
            digits: '0123456789',
            lowcaseChars: 'abcdefghijklmnopqrstuvwxyz',
            uppercaseChars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            specialChars: '~!@#$%^&*()-=+[]{}',
        };
        const rules = {
            lowcaseChars: 4,
            uppercaseChars: 4,
            digits: 2,
            specialChars: 2
        }
        const totalLen = Object.values(rules).reduce((r,v) => r + v, 0);
        let result = ','.repeat(totalLen - 1).split(',');
        let posInResult = 0;
        let posInData = 0;
        Object.keys(data).forEach(function (keyString) {
            for (let i = 1; i <= rules[keyString]; i++) {
                posInResult = Math.floor(Math.random() * totalLen);
                while(result[posInResult] !== '' && result.join('').length < totalLen) {
                    posInResult = Math.floor(Math.random() * totalLen);
                }
                posInData = Math.floor(Math.random() * data[keyString].length);
                let char = data[keyString].substring(posInData, posInData + 1);
                result[posInResult] = char;
            }
        })
        return result.join('');
    }

</script>
