<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use backend\views\widgets\create_update_fields\CreateUpdateFields;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\NxPerson */
/* @var $form yii\widgets\ActiveForm */
?>

<div id='person-form-wrapper'>
    <div class='box box-primary nx-person-form'>
        <?php 
            $action = [];
            if ($model->isNewRecord) {
                $action[] = Url::to(['person/create']);
            } else {
                $action[] = 'person/update';
                $action['id'] = $model->id;
            }
            $formOptions = [
                    'enableAjaxValidation' => true,
                    'validationUrl' => Url::to(['person/validate']),
                    // 'options' => ['data-pjax' => true],
                    'id' => 'person-form',
                    'action' => $action,
            ];
            if (isset($submitAjax))
                $formOptions['options'] = ['data-submitAjax'=> intval($submitAjax)];

        ?>
        <div class='box-header with-border'>

        </div>
        <div class='box-body'>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#person-edit-general" data-toggle="tab" aria-expanded="false">Общее</a></li>
                    <li><a href="#person-edit-address" data-toggle="tab" aria-expanded="true">Адрес</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="person-edit-general">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <?php $form = ActiveForm::begin($formOptions); ?>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'f',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'i',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'o',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'gender',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->dropDownList([ 0 => 'жен.', 1 => 'муж.', '' => 'не указ.', ], ['prompt' => '','class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'email',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'telcode',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                                            
                                            <?= $form->field($model, 'phone',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'position',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?= $form->field($model, 'company_id', [
                                                                                        'form' => 'person-form', 
                                                                                        'options' => ['class'=>'form-group field-person-company_id'], 
                                                                                        'labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px'],
                                                                                    ])
                                            ->widget(AutoComplete::classname(), [
                                                    'options' => [
                                                        'placeholder' => 'Начните набирать...',
                                                        'class' => 'form-control input-sm',
                                                        'name' => '',
                                                        'id' => 'person-company-name',
                                                        'style' => 'display:inline-block;margin-right:5px;',
                                                        'value' => $model->isNewRecord  || $model->company === null ? '' : $model->company->name,
                                                    ],
                                                    'clientOptions' => [
                                                        'source' => Url::to(['company/autocomplete']),
                                                        'autoFill'=>false,
                                                        'minLength'=>'3',
                                                        'appendTo' => 'person-company-name',
                                                        'delay' => '300',
                                                        'select' => new yii\web\JsExpression('function(event, ui){$("#person_company_id").val(ui.item.id);}'),
                                                        'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#person_company_id").val("");}}'),
                                                    ],
                                            ]) ?>
                                            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'company_id', $options = ['id' => 'person_company_id'])); ?>

                                            <?= $form->field($model, 'speciality',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                        </div>
                                        <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            
                                            <?= CreateUpdateFields::widget(['model' => $model]); ?>
                                            
                                            <?= $form->field($model, 'label',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                                            <?php if (!$model->hasErrors() && $model->isNewRecord) {$model->enabled = 1;} ?>
                                            <?= $form->field($model, 'enabled', ['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->checkbox() ?>
                                        </div>
                                        <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'comment',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textArea(['rows' => 2,'class' => 'form-control input-sm']) ?>
                                            <?= $form->field($model, 'name',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm','readonly' => true]) ?>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>
                    <div class="tab-pane" id="person-edit-address">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div style='margin:10px;min-width:300px;'>
                                    <?= $form->field($model, 'address_id', ['form' => 'person-form', 'options' => ['style' => 'width:90%;display:inline-block']])->widget(AutoComplete::classname(), [
                                            'options' => [
                                                'placeholder' => 'Начните набирать...',
                                                'class' => 'form-control',
                                                'name' => '',
                                                'id' => 'person-address-name',
                                                'style' => 'display:inline-block;margin-right:5px;',
                                                'value' => $model->isNewRecord || $model->address === null ? '' : $model->address->address,
                                            ],
                                            'clientOptions' => [
                                                'source' => Url::to(['address/autocomplete']),
                                                'autoFill'=>false,
                                                'minLength'=>'3',
                                                'appendTo' => 'person-address-name',
                                                'delay' => '300',
                                                'select' => new yii\web\JsExpression('function(event, ui){$("#person-address-id").val(ui.item.id);}'),
                                                'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#person-address-id").val("");}}'),
                                                // 'disabled' => true,

                                            ],
                                    ]) ?>
                                    <a href='' rel='nofollow' onclick='toggleElementDisplay("#person-address-form-wrapper", "slide", 600, event)' title='не нашли в списке подходящего адреса? Нажмите и дабавьте новый'>
                                        <i class='fa fa-fw fa-envelope'></i>
                                    </a>
                                    <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'address_id', $options = ['id' => 'person-address-id', 'form' => 'person-form'])); ?>
                                </div>
                                <div id='person-address-form-wrapper' style='display:none;'>
                                    <?= $this->render('/address/_form', ['model' => $addressModel, 'submitAjax' => true, 'formName' => 'person-address-form']); ?>
                                </div>
                            </div> <!--END box-body -->
                        </div> <!--END box -->
                    </div> <!--END #person-edit-address -->
                </div>
            </div>                
        </div>
        <div class='box-footer'>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                <?= Html::submitButton('Сохранить физ.лицо', ['class' => 'btn btn-primary pull-left', 'form' => 'person-form']); ?>
                <?php if(isset($showMessage)): ?>
                    <?php 
                        switch ($showMessage['type']) {
                            case 'success':
                                $alertBg = 'bg-green';
                                $alerticon = 'fa-check';
                                break;
                            case 'error':
                                $alertBg = 'bg-orange';
                                $alerticon = 'fa-warning';
                                break;
                            case 'info':
                                $alertBg = 'bg-aqua';
                                $alerticon = 'fa-info';
                                break;
                            case 'danger':
                                $alertBg = 'dg-red';
                                $alerticon = 'fa-ban';
                                break;
                            default:
                                $alertBg = 'dg-aqua';
                                $alerticon = 'fa-info';
                                break;
                        }
                    ?>
                <div class='alert alert-dismissible pull-right <?=$alertBg; ?>' style='position:relative'>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style='position:absolute;top:2px;right:7px'>×</button>
                    <div>
                        <i class="icon fa <?=$alerticon;?>"></i>
                        <?=$showMessage['text']; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div><!--End bos-footer  -->
    </div> <!--END box -->
</div><!--End #person-form-wrapper -->


