<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use common\models\Publisher;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RelationSystems */
/* @var $form yii\widgets\ActiveForm */
?>
<div id='pay-system-form-wrapper' class="relation-systems-form">
    <div class='row'>
        <div class='col-md-6 col-sm-8 col-xs-12'>

            <?php $form = ActiveForm::begin(); ?>

            <?php $form->errorSummaryCssClass = 'alert alert-warning alert-dismissible'; ?>

            <?php echo(yii\helpers\Html::activeHiddenInput($model, 'publisher_id')); ?>
            <div class='box box-primary pay-system-form'>
                <div class='box-header with-border'>

                </div>
                <div class='box-body'>
                    <div>
                        <?= $form->errorSummary($model, ['header' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>']); ?>
                    </div>
                    <?php
                    $pId = Yii::$app->user->identity->publisher_id;
                    $publisher = Publisher::findOne($pId);
//                    $queryString = <<<SQL
//                                    SELECT s.id, s.name
//                                    FROM systems s
//                                        LEFT JOIN relation_systems rs ON s.id = rs.system_id
//                                    WHERE rs.enabled = 1 AND rs.publisher_id = $pId
//                                    ORDER BY s.name
//SQL;
                    $queryString = <<<SQL
                                    SELECT s.id, s.name 
                                    FROM systems s 
                                    ORDER BY s.name
SQL;
                    $avalableSystems = Yii::$app->db->createCommand($queryString)->queryAll();
                    $avalableSystems = ArrayHelper::map($avalableSystems, 'id', 'name');
                    $dropDownoptions = ['prompt' => 'Выберете платежную систему...'];
                    echo($form->field($model, 'system_id')->dropdownList($avalableSystems, $dropDownoptions));
                    ?>

                    <?= $form->field($model, 'param_1')->textInput(['maxlength' => true]); ?>
                    <?= $form->field($model, 'param_2')->textInput(['maxlength' => true]); ?>
                    <?= $form->field($model, 'param_3')->textInput(['maxlength' => true]); ?>
                    <?= $form->field($model, 'param_4')->textInput(['maxlength' => true]); ?>
                    <?= $form->field($model, 'param_5')->textInput(['maxlength' => true]); ?>
                    <?= $form->field($model, 'enabled')->checkbox([]); ?>
                    <?= $form->field($model, 'send_data_for_FZ54')->checkbox([]); ?>
                    <?= $form->field($model, 'is_default')->checkbox([]); ?>
                </div>
                <div class='box-footer'>
                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
