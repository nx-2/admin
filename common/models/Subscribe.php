<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use  yii\db\Query;

class Subscribe extends \yii\base\Model
{


    /**
     * Get price one items
     * @return array
     */
    public function getPrice($model)
    {
//        $delivery_type_id = ($model->delivery_type_id == 1) ? 'почта' : 'PDF';
        $countryId = (property_exists($model, 'country_id') && !empty($model->country_id)) ? $model->country_id : 165;
        $params = [
            'publisherId' => $model->publisher_id, 
            'subscriberTypeId' => $model->subscriber_type_id,
            'editionId' => $model->magazineID,
            'deliveryTypeId' => $model->delivery_type_id,
            'countryId' => $countryId,
        ];
        
        return \common\models\Edition::getPrice($params);
//          закомментировано 12.08.2017  (А.Щ.)
//        $subQuery1 = (new Query())->select('id')->from('nx_price p')
//            ->where(['>=', 'p.date', new \yii\db\Expression('NOW()')])
//            ->where([
//                'p.customer_type_id' => $model->subscriber_type_id,
//                'p.publisher_id' => $model->publisher_id
//            ])->orderBy([
//                'p.date' => SORT_DESC,
//            ])->one();
//
//        $subQuery2 = (new Query())->select('zone_id')->from('nx_zone_country')
//            ->where([
//                'country_id' => 165
//            ])->one();
//
//        $subQuery3 = (new Query())->select('id')->from('nx_delivery_type')
//            ->where([
//                'name' => $delivery_type_id
//            ])->one();
//
//        $rows = (new \yii\db\Query())
//            ->select(['price','publisher_id'])
//            ->from('nx_price_item i')
//            ->join('LEFT JOIN', 'nx_price p', 'p.id = i.price_id')
//            ->where([
//                'p.publisher_id'=>$model->publisher_id,
//                'i.item_id' => $model->magazineID,
//                'p.id' => $subQuery1,
//                'i.zone_id' => $subQuery2,
//                'i.delivery_type_id' => $subQuery3,
//            ])
//            // ->limit(10)
//            ->one();
//
//
//        return [
//            'price'=>intval($rows['price']),
//            'publisher_id'=>$rows['publisher_id']
//        ];
    }


    /**
     * Check promocode
     * @param $promo_id
     * @param $magazineID
     * @param $period
     * @param $type_id
     * @return int|backend\models\NxOrder|backend\models\NxPerson|backend\models\PromoCodes
     */
    public function getPromo($model)
    {
        if (!$promo = $this->checkActionCode($model))
            return 0;

        if ($promo->actions->date_end < date('Y-m-d h:i:s'))
            return 0;


        if (empty($promo->items))
            return 0;

        $discount=[];

        $array_search = [];
        foreach ($promo->items as $v) {
            $array_search[$v->mag_id] = [
                'months' => intval($v->months),
                'type' => mb_strtolower($v->type),
            ];

            $discount[$v->mag_id]=intval($v->discount);
        }

        //временно
        if (array_key_exists($model->magazineID, $array_search)) {
            $getArray[$model->magazineID] = [
                'months' => intval($model->period),
                'type' => ($model->type == 1) ? 'paper' : 'pdf',
            ];

            if (sizeof($array_search) > 1)
                return 0;


            $result=array_diff($array_search[$model->magazineID], $getArray[$model->magazineID]);
            if (empty($result))
                return $discount[$model->magazineID];
        }

        return 0;

    }

    /**
     * Unset promo
     * @param $order_id
     * @return bool
     * @throws \yii\web\HttpException
     */
    public function setPromo($order_id)
    {

        if (!$order = \common\models\NxOrder::getOrderID($order_id))
            throw  new \yii\web\HttpException(200, 'Order not found');

        if (!$order->action_code)
            throw  new \yii\web\HttpException(200, 'Promocode not found');

        if (!$promocodes = \common\models\PromoCodes::find()->where(['code' => $order->action_code])->one())
            throw  new \yii\web\HttpException(200, 'Promocode not found');


        $oldPromoCodes = new \common\models\PromoCodesOld;
        $oldPromoCodes->action_id = $promocodes->action_id;
        $oldPromoCodes->code = $promocodes->code;
        $oldPromoCodes->order_id = $order->id;

        if ($oldPromoCodes->save()) {
            \common\models\PromoCodes::findOne($promocodes->id)->delete();
        } else {
            throw  new \yii\web\HttpException(200, 'PromocodeOld not save');
        }

        return true;


    }

    /**
     * Get Promo Code
     * @param int $code
     * @return array|null|\yii\db\ActiveRecord
     */
    private function checkActionCode($model)
    {

        $promocode= \common\models\PromoCodes::find()->where(['code' => $model->promocode])->one();
        if(isset($promocode->actions->publisher_id) && $promocode->actions->publisher_id==$model->publisher_id)
            return $promocode;

        return false;

    }

    /**
     * Get array regions
     * @return array
     */
    public static function getRegions()
    {

        $rows = (new \yii\db\Query())
            ->select(['RegionUser_ID', 'RegionUser_Name'])
            ->from('regions')
            ->all();

        return $rows;

    }


    /**
     * Save order for nx
     * @param SubscribeForm $model
     * @return mixed
     */
    public function saveBuy(SubscribeFormOne $model)
    {


        $params = array(
            'apikey' => md5('e;zol]pt9'),
            'method' => 'order_add',
            'params' => array(
                'orderData' => [
                    'enabled' => 1,
                    'user_id' => 0,
                    'order_state_id' => 2,
                    'email' => $model->email,
                    'label' => "store_issue_pdf",
                    'payment_type_id' => 2,
                    'create_user_id' => 0
                ],
                'itemsData'=>[
                    'Message146'=>[
                        $model->issue=>1
                    ]
                ],
                'personData'=>[
                    'email' => $model->email,
                    'name' => $model->username,
                    'speciality' => "",
                    'create_user_id' => 0
                ]
            )
        );

        return $this->sendRequest($params, \Yii::$app->params['nxAddress']. '/index/api');

    }

    /**
     * Save order for nx
     * @param SubscribeForm $model
     * @return mixed
     */
    public function saveOrder(SubscribeForm $model)
    {

        $promo_id = 0;
        $registeredUser = \common\models\User::find()->where(['Email' => $model['email']])->one();
        $address = $model['street'] . ',' . $model['home'] . ',' . $model['flat'];
        if ($model['promocode'])
            $promo_id = \common\models\PromoCodes::find()->where(['code' => $model['promocode']])->one();

        $price=$this->getPrice($model)['price'];
        
//        $price=!empty($getPrice) ? $getPrice : 0;

        $countryId = (property_exists($model, 'country_id') && !empty($model->country_id)) ? $model->country_id : 165;

        $subscriptionData = [

            'subscriptionData' => [
//                'price' => $price,
                'order_state_id' => 2,//2 = Готов, 3 = Оплачено, 1 = Формируется 4 = Отказ
                'label' => 'subscribe_form',
                'payment_type_id' => 2,//тип оплаты (на данный момент эл.платеж по умолчанию)
                'email' => $model['email'],
                'subscriber_type_id' => $model->subscriber_type_id, // 1 физ лицо 2 юр лицо,
                'user_id' => (!empty($registeredUser)) ? $registeredUser->User_ID : 0,
                'created' => date('Y-m-d H:i:s'),
                'create_user_id' => (!empty($registeredUser)) ? $registeredUser->User_ID : 0,
                'action_id' => ($promo_id) ? $promo_id->actions->id : 0,
                'action_code' => $model['promocode'],
                'price_id'=>$model['price_id'],
            ],
            'addressData' => [
                'country_id' => "165",
                'city' => $model['city'],
                'city1' =>  $model['city1'],
                'area' => $model['region'],
                'region' => $model['region1'],
                'zipcode' => $model['index'],
                'address' => $address,
                'street' => $model['street'],
                'house' => $model['home'],
                'building' => $model['housing'],
                'apartment' => $model['flat'],
                'phone' => $model['phone'],
                'fax' => "",
                'label' => "subscribeForm",
            ],
            'mapAddressData' => [
                'country_id' => $countryId,
                'city' => $model['city'],
                'city1' =>  $model['city1'],
                'area' => $model['region'],
                'region' => $model['region1'],
                'zipcode' => $model['index'],
                'address' => $address,
                'street' => $model['street'],
                'house' => $model['home'],
                'building' => $model['housing'],
                'apartment' => $model['flat'],
                'phone' => $model['phone'],
                'fax' => "",
                'label' => "subscribeForm",
            ],
            'itemsData' => [
                0 => [
                    'periodical_id' => $model->magazineID,
                    'date_start' => $model->date_start,
                    'period' => $model->period,
                    'delivery_type_id' => $model->delivery_type_id,
                    'discount' => $model->discount,
                    'price' => $price,
                ]
            ]

        ];

        $personData = [
            'personData' => [
                'position' => '',
                'speciality' => '',
                'phone' => $model->phone,
                'email' => $model->email,
                'name' => $model->username,
                'f' => '',
                'i' => '',
                'o' => '',
                'checked' => 0,
                'created' => date('Y-m-d H:i:s'),
                'label' => '',
                'comment' => '',
            ]
        ];

        $companyData = [
            'companyData' => [
                'name' => $model->company_name,
                'inn' => $model->inn_code,
                'kpp' =>  $model->kpp_code,
                'label' => "subscription_form",
                'checked' => 0,
                'created' => date('Y-m-d H:i:s'),
            ],
            'legalAddressData' => [
                'address' => $model->legal_address,
                'label' => "new_subscribe"
            ]
        ];


        $params = array(
            'apikey' => md5('e;zol]pt9'),
            'method' => 'subscription_add',
            'params' => array(
                'subscriptionData' => $subscriptionData,
                'personData' => $personData,
                'companyData' => $companyData,
                'orderData' => [],
            )
        );

        return $this->sendRequest($params, \Yii::$app->params['nxAddress']. '/index/api');

    }

    /**
     * Send NX
     * @param array $params
     * @param string $url
     * @return mixed
     */
    private function sendRequest($params = array(), $url = '')
    {

        if (empty($params['apikey'])) $params['apikey'] = md5('e;zol]pt9');
        $ps = $params;
        $params = http_build_query($params);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Referer: ' . (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'invisible man')));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        if (!isset($result->success) || $result->success !== true){
            print_r($ps);
            die();
            throw  new \yii\web\HttpException(200, json_encode($result));
        }
        return intval($result->result);
    }


    /**
     * Get API
     * @param array $params
     * @param string $url
     * @return mixed
     */
    public static function sendRequestApi($params, $url = '',$type='GET')
    {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($type == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }
        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result);

        if (!$result)
            throw  new \yii\web\HttpException(200, 'No data');


        return $result;
    }

    /**
     * Сount release
     * @param $magazineID
     * @param $startDate
     * @param $period
     * @return int|backend\models\NxOrder|backend\models\NxPerson|backend\models\PromoCodes
     */
    public function getMagazineCount($model)
    {
        $start_stamp = strtotime($model->date_start);
        $start_year = date('Y', $start_stamp);
        $start_month = date('n', $start_stamp);
        $end_stamp = mktime(0, 0, 0, date('n', $start_stamp) + $model->period - 1, date('j', $start_stamp), date('Y', $start_stamp));
        $end_year = date('Y', $end_stamp);
        $end_month = date('n', $end_stamp);

        $issues = \common\models\Release::find()
            ->where(['MagazineID' => $model->magazineID])
            ->andWhere(['>=', 'year', $start_year])
            ->orderBy([
                'year' => SORT_ASC,
                'release_month' => SORT_ASC
            ])
            ->groupBy(['year', 'Number'])
            ->all();

        $count = 0;
        if (!empty($issues)) {
            $issues_by_year = array();
            foreach ($issues as $issue) {
                $issues_by_year[$issue->year][] = $issue;
            }
            if (empty($issues_by_year[$start_year])) {
                $issues_by_year[$start_year] = end($issues_by_year);
            }
            if (empty($issues_by_year[$start_year + 1])) {
                $issues_by_year[$start_year + 1] = $issues_by_year[$start_year];
            }
            if (empty($issues_by_year[$start_year + 2])) {
                $issues_by_year[$start_year + 2] = $issues_by_year[$start_year + 1];
            }

            $break = 0;
            foreach ($issues_by_year as $year => $issues) {
                foreach ($issues as $issue) {
                    if ($year > $end_year || ($year == $end_year && $issue->release_month > $end_month)) {
                        $break = 1;
                        break;
                    }
                    if ($year == $start_year && $issue->release_month < $start_month) {
                        continue;
                    }
                    $count++;
                }
                if ($break) {
                    break;
                }
            }
        }
        return intval($count);
    }


    /**
     * Get txt error
     * @param $model
     * @return bool|string|stdClass
     */
    public static function getError($model){

        if(!$model->errors)
            return false;

        $textError='';
        foreach ($model->errors as $v){
            $textError.=$v[0].PHP_EOL;
        }

        return $textError;
    }


}