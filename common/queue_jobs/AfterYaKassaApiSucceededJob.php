<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\queue_jobs;

use common\custom_components\pay\YandexApiPay;
use common\models\NxOrderState;
use common\models\Payment;
use common\models\PaymentType;
use common\models\PaySystems;
use common\models\PaySystemStatus;
use common\models\YaKassaApiPayment;
use Yii;
use yii\log\Logger;
use yii\queue\JobInterface;

/**
 * Предназначен для выполнения действий, необходимых после успешной оплаты заказа через АПИ яндекс кассы.
 * Общая последовательность действий такая:
 * 1. После создания заказа и транзакции, а также нажатия клиентом кнопки "Оплатить" он отправляется на страницу Яндекс кассы для оплаты
 * 2. В случае успешной оплаты происходит одно из двух: Яндекс уведомляет про успешную оплату или наш крон-таб скрипт спрашивает яндекс кассу про этот платеж
 * 3. После того как нам стало известно от яндекса об успешной оплате для соответствующей модели \common\models\YaKassaApiPayment поле last_status изменяет значени на succeeded
 * 4. Это приводит к тому, что генерируется событие "yaApiPaidSuccess" в обработчике которого создается экземпляр текущего (AfterYaKassaApiSucceededJob) класса и помещается в очередь задач
 * 5. Далее выполняется метод execute этого класса
 *
 * На момент выполнения execute известен только ИД модели  YaKassaApiPayment которая стала "succeeded".
 * Статусы заказа и транзакции НЕ обновлены, модели Payment для заказа НЕ существует. Вот это вот все и требуется сделать
 */
class AfterYaKassaApiSucceededJob implements JobInterface
{
    private $yaKassaApiPaymentId = null; //ссылка на запись в таблице ya_kassa_api_payment

    public function __construct($yaKassaApiPaymentId)
    {
        $this->yaKassaApiPaymentId = intval($yaKassaApiPaymentId);

    }

    public function execute($queue)
    {
        $model = YaKassaApiPayment::findOne(['id' => $this->yaKassaApiPaymentId]);
        if (null == $model)
            return false;

        $payYandexApi = new YandexApiPay($model->order_id, $model->publisher_id);

        if ($payYandexApi->hasErrors) {
            $this->log($payYandexApi->getErrors());
            print_r($payYandexApi->getErrors());
            return false;
        }

        $payYandexApi->setPaymentModel($model);

        $payment = $payYandexApi->getInfoApiYaKassa(); //получить объект платежа от апи яндекс кассы
        if (!$payment)
            return false;

        $order = $payYandexApi->getOrder();

        $isAlreadyPaid = $order->order_state_id == NxOrderState::ORDER_STATE_PAID //статус заказа уже "оплачен"
            && $order->payment != null //платеж уже существует
            && (
                $order->transaction->sys_status == PaySystemStatus::STATUS_PAID_IMMEDIATELY
                ||
                $order->transaction->sys_status == PaySystemStatus::STATUS_PAID_ON_REQUEST_TO_PAYMENT_SYSTEM
            ) //статус транзакции уже 'Оплачен'
            && $order->payment->sum == $payment->amount->value; //Сумма оплаты правильная

        if ($isAlreadyPaid)
            return true;

        $transaction = Yii::$app->db->beginTransaction();
        $d = !(null == $payment->captured_at) ? $payment->captured_at : $payment->created_at;
        $order->setAttributes(
            [
                'order_state_id' => NxOrderState::ORDER_STATE_PAID, // оплачен
//                'payment_date' => date('Y-m-d'),
                'payment_date' => $d->format('Y-m-d H:i:s'),
                'payment_type_id' => PaymentType::PAYMENT_TYPE_ONLINE,
                'payment_agent_id' => 47, // Временно захардкоджено под "Открытые системы"
                'payment_sum' => $payment->amount->value,
                'epay_result' => 'approved',
            ], false
        );

        $oUpdated = $order->update(true);
        //оплачено, сегодня, электронным платежем, агент не указан, сумма
        // $oUpdated хранит affectedRowsNumber (т.е. 0 или 1)

//        $tUpdated = $this->order->transaction->updateAttributes(['sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY]); //оплачено
//        $order->transaction->setAttributes(['system' => $payYandexApi::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY], false);
        $order->transaction->setAttribute('sys_status', PaySystemStatus::STATUS_PAID_IMMEDIATELY);

        $availableSystems = [PaySystems::SYSTEM_YOO_KASSA_API, PaySystems::SYSTEM_YOO_KASSA_API_TEST];
        if (!in_array(intval($order->transaction->system), $availableSystems)) {
            $order->transaction->setAttribute('system', PaySystems::SYSTEM_YOO_KASSA_API);
        }

        $tUpdated = $order->transaction->update(true);
        // $tUpdated хранит affectedRowsNumber (0 или 1)

        $paymentData = [
            'order_id' => $order->id,
            'transaction_id' => $order->transaction->id,
            'sum' => $payment->amount->value,
            'date' => $d->format('Y-m-d H:i:s'),
            'year' => $d->format('Y'),
            'payment_agent_id' => 47, // Временно захардкоджено под "Открытые системы"
//            'purpose' => '',
//            'comment' => '',
            'status' => Payment::STATUS_NOT_SENT_TO_1C_YET,
//            'status' => Payment::STATUS_SENT_TO_1C_SUCCESSFULL, //это ВРЕМЕННО!!! Строка выше - правильная.
            'result' => serialize(['comment' => 'initial creating payment for order' . $order->id]),
            'request_data' => serialize(['comment' => 'this payment was not send to 1C yet']),
            'label' => 'transaction_payment',
            'publisher_id' => $model->publisher_id,
        ];

        if ($order->payment != null) {
            $order->payment->setAttributes($paymentData, false);
            $pUpdated = $order->payment->update(true);
            // $pUpdated хранит affectedRowsNumber т.е. 0 или 1
        } else {
            $nxPayment = new Payment();
            $nxPayment->setAttributes($paymentData);
            $pUpdated = $nxPayment->insert(true); // insert возвращает boolean
        }

        if (boolval($oUpdated) && boolval($tUpdated) && boolval($pUpdated)) {
            $transaction->commit();
        } else {
            $oErr = print_r($order->getErrors(), true);
            $tErr = print_r($order->transaction->getErrors(), true);
            $pErr = isset($nxPayment) ? print_r($nxPayment->getErrors(), true) : print_r($order->payment->getErrors(), true);
            $logMessage = 'API Yandex касса сообщила про успешную оплату заказа №' . $order->id
                . ', но при обновлении заказа, транзакции, платежа что-то пошло не так. Статусы НЕ БЫЛИ обновлены ' . $oErr . ', ' . $tErr . ', ' . $pErr;
            $payYandexApi->log($logMessage);
            $transaction->rollBack();
            print_r($logMessage);
            return false;
        }
        return true;
    }

    private function log($data)
    {
        $logger = Yii::getLogger();
        $logger->log($data, Logger::LEVEL_INFO, YandexApiPay::LOG_CATEGORY);
        return;

    }
}