<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_payment_agent".
 *
 * @property integer $id
 * @property string $name
 * @property string $ws1c_id
 *
 * @property NxOrder[] $nxOrders
 * @property NxPayment[] $nxPayments
 */
class PaymentAgent extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_payment_agent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['ws1c_id'], 'string', 'max' => 20],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ws1c_id' => 'Ws1c ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrders()
    {
        return $this->hasMany(NxOrder::className(), ['payment_agent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPayments()
    {
        return $this->hasMany(NxPayment::className(), ['payment_agent_id' => 'id']);
    }
}
