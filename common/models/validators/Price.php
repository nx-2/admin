<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\validators;

class Price extends \yii\base\Model
{

    public $subscriber_type_id;
    public $magazineID;
    public $delivery_type_id;
    public $publisher_id;
    public $country_id;

    public function rules()
    {
        return [
            [['subscriber_type_id', 'magazineID', 'delivery_type_id','publisher_id','country_id'], 'required'],
            [['subscriber_type_id', 'magazineID','delivery_type_id','publisher_id', 'country_id'], 'integer'],
        ];
    }
}