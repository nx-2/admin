<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

    $formId = '#' . Yii::$app->params['subscribeFormId'];
    $formWrapperId = '#' . Yii::$app->params['subscribeFormWrapperId'];
    $randomPrefix = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnm1234567890'), 0 ,12);
?>
var severUrl='<?=$apiUrl;?>';
var mag_id=<?=$editionId;?>;
var publisher_id=<?=$publisherId;?>;
var formSubmittable = true;

setTimeout(function(){
    var jqueryExists = (typeof(jQuery) == 'function') ? true : false;

    if (!jqueryExists) {
            var script = document.createElement('script');

            script.type = 'text/javascript';
            script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

            document.getElementsByTagName('head')[0].appendChild(script);

            script.addEventListener('load', runAfterJqueryLoad, false);
    } else {
            runAfterJqueryLoad();
    }
},1000);

function subscribeFormSubmitHandler(event) {
    if (typeof event == 'undefined')
        event = window.event;

    event.preventDefault();
    event.stopImmediatePropagation();
    var $form = $("<?=$formId;?>");
    if (!formSubmittable) {
        return false;
    }
    formSubmittable = false;
    $('<?=$formId;?> #oform-zak').addClass('loading');
    jQuery.ajax({
        url: severUrl + '/subscribe/create-order',
        type: "POST",
        dataType: "json",
        data: jQuery("<?=$formId;?>").serialize(),
        success: function (response) {
            response = $.parseJSON(response);
            if (response.status == 'OK') {
                var html = response.html;
                $('<?=$formWrapperId;?>').html(html);
            } else {
                if (typeof response.errors != 'undefined') {
                    var errorsList = "";
                    for(var k in response.errors ) {
                        for (var n = 0; n < response.errors[k].length; n++) {
                            var item = "<li style='color: red;background: rgba(0,0,0,0.1);padding: 5px 10px;'>" + response.errors[k][n] + "</li>";
                            errorsList += item;
                        }
                    }
                    $('<?=$formWrapperId;?> <?=$formId;?> .errors-place .errors-list').html($(errorsList));
                }
            }
            formSubmittable = true;
            $('<?=$formId;?> #oform-zak').removeClass('loading');
        },
        error: function (response) {
            alert('Что то пошло не так. Обратитесь в поддержку');
            formSubmittable = true;
            $('<?=$formId;?> #oform-zak').removeClass('loading');
        }
    });

    return false;
}

function runAfterJqueryLoad() {
    $(document).ready(function () {
        var basePrice = 0; //базовый прайс
        var promoPrice = 0; // цена со скидкой
        var type_develery = 1; //1-почта,2-pdf
        var customer_type_id = 1;  //1-физ лицо
        var release = 0; //кол-во выпусков за период

        var isNumber = function(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        };

        function isEmpty(str) {
            return (!str || 0 === str.length);
        }


        function start() {

            $.ajax({
                type: "GET",
                url:  severUrl + '/subscribe/getForm/' + mag_id + '/' + publisher_id + '?_format=json',
                dataType:"json",
                success: function(response,b,c){

                    if (!response)
                        alert("Ошибка при получении данных. Обратитесь в поддержку");

                    $('<?=$formWrapperId;?>').html(response);

                    // $('<?=$formWrapperId;?>').html('').append(formElem);
                    setTimeout(function(){
                        var formElem = $('<?=$formWrapperId;?> <?=$formId;?>');
                        formElem.off();
                        formElem.submit(subscribeFormSubmitHandler);
                        getPrice();
                    },2000);
                },
                error: function(response,b,c){
                    alert('Что то пошло не так. Обратитесь в поддержку');
                    console.log(response);
                    console.log(b);
                    console.log(c);

                }
            });

        }


        function getPrice() {

            countryId = $('<?=$formWrapperId?> <?=$formId?> #country-id');
            if (countryId.length > 0) {
                countryId = countryId.val();
            } else {
                countryId = 165;
            }
            var url = severUrl + '/editions/price/' + publisher_id + '/' + customer_type_id + '/' + mag_id + '/' + type_develery + '/' + countryId
            $.ajax({
                type: "GET",
                //url: severUrl + "/subscribe/get-price",
                url: url,
                dataType:"json",
                success: function(jsondata){

                   if (!isNumber(jsondata.price))
                            alert("Ошибка при расчетах. Обратитесь в поддержку");

                    basePrice = jsondata.price * 1;
                    //$('#price').html(basePrice);
                    $('<?=$formId;?> #price_id').val(jsondata.price_id * 1);
                    getRelease();
                },
                error: function(jsondata){
                   alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });


        }


        function getRelease() {

            var period = $('#period').val();
            if (!period) {
               period = 12;
            }
            var date_start = $('#date_start').val();
            var count = 0;

            $.ajax({
                type: "POST",
                //url: severUrl + "/subscribe/count-release",
                url: severUrl + "/issue/count/" + mag_id + '/' + period,
                dataType:"json",
                data:{
                    // period: period,
                    // magazineID:mag_id,
                    // date_start:date_start,
                    startDate: date_start,
                    // _format: 'json'
                },
                success: function(response, b,c){

                    if (!isNumber(response.count)) {
                        console.log(response.count);
                        console.log(b);
                        console.log(c);
                        return;
                    }
                    release = response.count;
                    $("<?=$formId;?> #release").html(response.count + ' выпусков');

                    changePrice();
                },
                error: function(response,b,c){
                    alert('Что то пошло не так. Обратитесь в поддержку ' + b + ' ' + c );
                }
            });



        }


        function changePrice() {

            var period = $('<?=$formId;?> #period').val() * 1;
            var price = release * basePrice;


            if (promoPrice != 0) {
                price = price - (price * promoPrice / 100);
            }
            $('<?=$formId;?> #price').html(price);

        }

        function getPromocode() {

            var promocode = $('<?=$formId;?> #subscribeform-promocode').val();
            var publisher_id = $('<?=$formId;?> #publisher_id').val();

            if(promocode == '')
                return;

            var period = $('<?=$formId;?> #period').val() * 1;
            var type = $("<?=$formId;?> #dev_type").val() * 1;
            var PromoPrice = 0;

            $.ajax({
                type: "POST",
                url: severUrl + "/subscribe/get-promo",
                dataType:"json",
                data:{
                    promocode: promocode,
                    magazineID: mag_id,
                    period: period,
                    type: type,
                    customer_type_id: customer_type_id,
                    publisher_id:publisher_id,
                    _format: 'json'
                },
                success: function(jsondata){

                    if (!isNumber(jsondata))
                        alert("Ошибка при расчетах. Обратитесь в поддержку");

                    promoPrice = jsondata;
                    $('<?=$formId;?> #discount').val(promoPrice);
                    changePrice();
                },
                error: function(jsondata){
                    alert('Что то пошло не так. Обратитесь в поддержку');
                }
            });

        }


        $(document.body).on('change', "<?=$formId;?> #period, <?=$formId;?> #date_start", function (e) {
            promoPrice = 0;
            getPrice();
        });


        $(document.body).on('click', "<?=$formId;?> #promoSubmit", function (e) {
            getPromocode();
            return false;
        });


        $(document.body).on('click', "<?=$formId;?> #podpiskaUr", function (e) {
            $("<?=$formId;?> #subscribeform-company_name").prop('required',true);
            $("<?=$formId;?> #subscribeform-legal_address").prop('required',true);
            $("<?=$formId;?> #subscribeform-inn_code").prop('required',true);
            $("<?=$formId;?> #subscribeform-kpp_code").prop('required',true);

            $('<?=$formId;?> .hiddenBlock').slideToggle('slow');
            if ($(this).text() == 'Подписка Юр.лицо') {
                $(this).text('Подписка Физ.лицо');
                customer_type_id = 2;
                $('<?=$formId;?> #sub_type').val(customer_type_id);
                promoPrice=0;
                getPrice();

            }
            else {
                $("<?=$formId;?> #subscribeform-company_name").prop('required',false);
                $("<?=$formId;?> #subscribeform-legal_address").prop('required',false);
                $("<?=$formId;?> #subscribeform-inn_code").prop('required',false);
                $("<?=$formId;?> #subscribeform-kpp_code").prop('required',false);
                $(this).text('Подписка Юр.лицо');
                customer_type_id = 1;
                getPrice();
                $('<?=$formId;?> #sub_type').val(customer_type_id);
            }
        });


        $(document.body).on('click', "<?=$formId;?> #paper", function (e) {
            promoPrice = 0;
            type_develery = 1;
            $("<?=$formId;?> #dev_type").val(type_develery);
            getPrice();
            $('<?=$formId;?> #paper').attr("class", "version-button act");
            $('<?=$formId;?> #pdf').attr("class", "version-button");

        });


        $(document.body).on('click', "<?=$formId;?> #pdf", function (e) {
            promoPrice = 0;
            type_develery = 5;
            $("<?=$formId;?> #dev_type").val(type_develery);
            getPrice();
            $('<?=$formId;?> #pdf').attr("class", "version-button act");
            $('<?=$formId;?> #paper').attr("class", "version-button");

        });

        start();

    });
}
