<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\AdminMainAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

use common\custom_components\widgets\side_menu\SideMenu;

// AppAsset::register($this);
AdminMainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	 <meta charset="<?= Yii::$app->charset ?>">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <?= Html::csrfMetaTags() ?>
	 <title><?= Html::encode($this->title) ?></title>
	 <?php $this->head() ?>
         <script type="text/javascript">var apiUrl = '<?=yii::$app->params['apiUrl']; ?>';</script>
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

  <header class="main-header">
	 <!-- Logo -->
	 <a href='<?=Yii::$app->homeUrl; ?>' class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><?= Yii::$app->name; ?></span>
	 </a>
	 <!-- Header Navbar: style can be found in header.less -->
	 <nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		  <span class="sr-only">переключить навигационную панель</span>
		</a>
                <div style='float:left'>
                    <a href="<?php echo(Yii::$app->params['nxAddress']); ?>" style='display: block;height: 50px;color: white;line-height: 50px;font-size: 16px;background: rgba(0,0,0,0.4);padding: 0px 10px;'>Перейти к Панели заказов</a>
                </div>

		<?php $flashMessages = Yii::$app->session->getAllFlashes(); ?>
		<?php if( count($flashMessages) > 0 ): ?>
		<?php
			$typesMap = ['success','warning','danger','info'];
		?>
		<div class='flash-messages-place'>
			 <?php foreach($flashMessages as $type => $messages): ?>
			 	<?php $t = in_array($type, $typesMap) ? $type : 'info';?>
				<?php foreach($messages as $message): ?>
					 <div class='alert alert-<?=$t;?> alert-dismissible'>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style='top: -7px;right: -3px;'>×</button>
						<span style='margin-right: 10px;'><?=$message; ?></span>
					 </div>
				<?php endforeach; ?>
			 <?php endforeach; ?>
		</div>
		<?php endif; ?>

		<div class="navbar-custom-menu">
		  <ul class="nav navbar-nav">
			 <!-- Messages: style can be found in dropdown.less-->
              <li>
                  <a href="#" data-toggle="control-sidebar" title="О программе/about">
                      О программе
                  </a>
              </li>
			 <!-- messages -->
			 <!--
			 <li class="dropdown messages-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <i class="fa fa-envelope-o"></i>
				  <span class="label label-success">4</span>
				</a>
				<ul class="dropdown-menu">
				  <li class="header">Вам 4 сообщения</li>
				  <li>
					 <ul class="menu">
						<li>
						  <a href="#">
							 <div class="pull-left">
								<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
							 </div>
							 <h4>
								Support Team
								<small><i class="fa fa-clock-o"></i> 5 mins</small>
							 </h4>
							 <p>Why not buy a new awesome theme?</p>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <div class="pull-left">
								<img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
							 </div>
							 <h4>
								AdminLTE Design Team
								<small><i class="fa fa-clock-o"></i> 2 hours</small>
							 </h4>
							 <p>Why not buy a new awesome theme?</p>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <div class="pull-left">
								<img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
							 </div>
							 <h4>
								Developers
								<small><i class="fa fa-clock-o"></i> Today</small>
							 </h4>
							 <p>Why not buy a new awesome theme?</p>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <div class="pull-left">
								<img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
							 </div>
							 <h4>
								Sales Department
								<small><i class="fa fa-clock-o"></i> Yesterday</small>
							 </h4>
							 <p>Why not buy a new awesome theme?</p>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <div class="pull-left">
								<img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
							 </div>
							 <h4>
								Reviewers
								<small><i class="fa fa-clock-o"></i> 2 days</small>
							 </h4>
							 <p>Why not buy a new awesome theme?</p>
						  </a>
						</li>
					 </ul>
				  </li>
				  <li class="footer"><a href="#">See All Messages</a></li>
				</ul>
			 </li>
			 -->
			 <!-- end messages -->
			 <!-- Notifications: style can be found in dropdown.less -->
			 <!-- оповещения -->
			 <!--
			 <li class="dropdown notifications-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <i class="fa fa-bell-o"></i>
				  <span class="label label-warning">10</span>
				</a>
				<ul class="dropdown-menu">
				  <li class="header">You have 10 notifications</li>
				  <li>
					 <ul class="menu">
						<li>
						  <a href="#">
							 <i class="fa fa-users text-aqua"></i> 5 new members joined today
						  </a>
						</li>
						<li>
						  <a href="#">
							 <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
							 page and may cause design problems
						  </a>
						</li>
						<li>
						  <a href="#">
							 <i class="fa fa-users text-red"></i> 5 new members joined
						  </a>
						</li>
						<li>
						  <a href="#">
							 <i class="fa fa-shopping-cart text-green"></i> 25 sales made
						  </a>
						</li>
						<li>
						  <a href="#">
							 <i class="fa fa-user text-red"></i> You changed your username
						  </a>
						</li>
					 </ul>
				  </li>
				  <li class="footer"><a href="#">View all</a></li>
				</ul>
			 </li>
			  -->
			 <!-- конец оповещения -->
			 <!-- Tasks: style can be found in dropdown.less -->
			 <!-- Задачи -->
			 <!--
			 <li class="dropdown tasks-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <i class="fa fa-flag-o"></i>
				  <span class="label label-danger">9</span>
				</a>
				<ul class="dropdown-menu">
				  <li class="header">You have 9 tasks</li>
				  <li>
					 <ul class="menu">
						<li>
						  <a href="#">
							 <h3>
								Design some buttons
								<small class="pull-right">20%</small>
							 </h3>
							 <div class="progress xs">
								<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">20% Complete</span>
								</div>
							 </div>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <h3>
								Create a nice theme
								<small class="pull-right">40%</small>
							 </h3>
							 <div class="progress xs">
								<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">40% Complete</span>
								</div>
							 </div>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <h3>
								Some task I need to do
								<small class="pull-right">60%</small>
							 </h3>
							 <div class="progress xs">
								<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">60% Complete</span>
								</div>
							 </div>
						  </a>
						</li>
						<li>
						  <a href="#">
							 <h3>
								Make beautiful transitions
								<small class="pull-right">80%</small>
							 </h3>
							 <div class="progress xs">
								<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">80% Complete</span>
								</div>
							 </div>
						  </a>
						</li>
					 </ul>
				  </li>
				  <li class="footer">
					 <a href="#">View all tasks</a>
				  </li>
				</ul>
			 </li>
			  -->
			 <!-- Конец Задачи -->
			 <!-- User Account: style can be found in dropdown.less -->
			 <li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <?php if(!Yii::$app->user->isGuest): ?>
				  <?php $src = empty(Yii::$app->user->identity->avatar) ? '/images/default-user.png' : Yii::$app->user->identity->avatar ?>
				  <img src="<?=$src;?>" class="user-image" alt="<?=Yii::$app->user->identity->Login; ?> image">
				  <span class="hidden-xs"><?=Yii::$app->user->identity->Login; ?></span>
				  <?php endif; ?>
				</a>
				<ul class="dropdown-menu">
				  <!-- User image -->
				  <?php if(!Yii::$app->user->isGuest): ?>
				  <li class="user-header">
					 <img src="<?=Yii::$app->user->identity->avatar ?>" class="img-circle" alt="<?=Yii::$app->user->identity->Login; ?> image">

					 <p>
						<?php
						  $roles = '';
						  $n = 1;
						  foreach(Yii::$app->user->identity->roles as $role) {
							 $roles .= ($n == 1) ?  $role['name'] : ', ' . $role['name'];
							 $n++;
						  }
						?>
						<?=Yii::$app->user->identity->LastName . ' ' . Yii::$app->user->identity->FirstName ?> (<?=Yii::$app->user->identity->job; ?>)
						<small>зарегистрирован с  <?= date('d.m.Y', strtotime(Yii::$app->user->identity->Created)); ?></small>
						<small><?=$roles; ?></small>
					 </p>
				  </li>
				  <?php endif; ?>
				  <!-- Menu Body -->
				  <!--
				  <li class="user-body">
					 <div class="row">
						<div class="col-xs-4 text-center">
						  <a href="#">Followers</a>
						</div>
						<div class="col-xs-4 text-center">
						  <a href="#">Sales</a>
						</div>
						<div class="col-xs-4 text-center">
						  <a href="#">Friends</a>
						</div>
					 </div>
				  </li>
					-->
				  <!-- Menu Footer-->
				  <li class="user-footer">
					 <div class="pull-left">
						<a href="/user/update?id=<?= Yii::$app->getUser()->getIdentity()->getId(); ?>" class="btn btn-default btn-flat" target='_blank'>Профиль</a>
					 </div>
					 <div class="pull-right">
						<a href="#" class="btn btn-default btn-flat" onclick='$("#logoutForm").submit();return false;'>Выйти</a>
					 </div>
				  </li>
				</ul>
			 </li>
              <li class="dropdown messages-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" id='logout' onclick='$("#logoutForm").submit();return false;' title='Выйти'>
                      <i class="fa fa-sign-out"></i>
                      <span class="label label-success">вых.</span>
                  </a>
                  <?=Html::beginForm(['site/logout'], 'post', ['id' => 'logoutForm']); ?>
                  <?=Html::endForm(); ?>
              </li>
		  </ul>
		</div>
	 </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" style="padding-bottom: 40px;">
	 <!-- sidebar: style can be found in sidebar.less -->
	 <section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
		  <?php if(!Yii::$app->user->isGuest): ?>
		  <div class="pull-left image">
		  	<?php if( !empty(Yii::$app->user->identity->avatar)): ?>
				<img src="<?=Yii::$app->user->identity->avatar;?>" class="img-circle__" alt="<?=Yii::$app->user->identity->Login;?>`s image">
		  	<?php else: ?>
				<img src="/images/default-user.png" class="img-circle__" alt="<?=Yii::$app->user->identity->Login;?>`s image">
		  	<?php endif; ?>
		  </div>
		  <div class="pull-left info">
			 <p><?=Yii::$app->user->identity->FirstName . ' ' . Yii::$app->user->identity->LastName;?></p>
                         <!--
			 <a href="#"><i class="fa fa-circle text-success"></i> онлайн</a>
                         -->
		  </div>
		  <?php endif; ?>
		</div>
		<!-- search form -->
		<!--
		<form action="#" method="get" class="sidebar-form">
		  <div class="input-group">
			 <input type="text" name="q" class="form-control" placeholder="Search...">
				  <span class="input-group-btn">
					 <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					 </button>
				  </span>
		  </div>
		</form>
		 -->
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<?php //echo SideMenu::widget(['menu' => Yii::$app->params['sideMenu']]); ?>
		<?php if(!Yii::$app->user->isGuest): ?>
			<?php
				$menuParams = require(__DIR__ . '/sideMenuParams.php');
			?>
			<?= SideMenu::widget(['menu' => $menuParams]); ?>

		<?php endif; ?>

	 </section>
	 <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	 <!-- Content Header (Page header) -->
	 <section class="content-header">
		<h1>
		  <small>Панель издателя</small>
		</h1>
		<ol class="breadcrumb">
		  <li><a href="<?php echo(Url::home()); ?>"><i class="fa fa-dashboard"></i>Главная</a></li>

		  <?php if(isset($this->params['breadcrumbs'])): ?>
			 <?php foreach( $this->params['breadcrumbs'] as $key => $item ): ?>
			 <?php
				if (is_array($item) && array_key_exists('label', $item)) {
					 $text = $item['label'];
				} elseif (gettype($item) == 'string') {
					 $text = $item;
				} else {
					 $text = '';
				}
				$url = ( is_array($item) && isset($item['url']) ) ? $item['url'] : false;
				$url = $url ? Url::to($url) : $url;
			 ?>
			 <li <?php if(!$url) {echo('class="active"');} ?>>
				<?php if($url): ?>
				  <a href="<?=$url ?>">
					 <?php echo($text);?>
				  </a>
				<?php else: ?>
					 <?php echo($text);?>
				<?php endif; ?>
			 </li>
			 <?php endforeach; ?>
		  <?php endif; ?>
		</ol>
	 </section>

	 <!-- Main content -->
	 <section class="content">
		  <?= $content ?>
	 </section>
	 <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer" style="position: relative">
      <div id='aside-logo'>
          <img src="/images/logo.png" alt="dimeo-logo" style="max-width: 100%;max-height: 40px;">
      </div>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
	 <div class="tab-content">
         ПО «Димео.Подписка» автоматизирует процессы редакционной подписки. Разработано ООО «Димео», 2017, 2018, 2022 (<a target="_blank" href="https://dimeo.ru/dimeo-podpiska.html">dimeo.ru/dimeo-podpiska.html</a>)
         <br/>
         <hr/>
         Dimeo.Podpiska software automates editorial subscription processes. Developed by Dimeo LLC, 2017, 2018, 2022 (<a target="_blank" href="https://dimeo.ru/dimeo-podpiska.html">dimeo.ru/dimeo-podpiska.html</a>)
     </div>
  </aside>
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
