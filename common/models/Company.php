<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_company".
 *
 * @property integer $id
 * @property string $name
 * @property integer $source_id
 * @property integer $channel_id
 * @property string $org_form
 * @property integer $address_id
 * @property integer $legal_address_id
 * @property integer $contact_person_id
 * @property string $email
 * @property string $url
 * @property string $description
 * @property string $shortname
 * @property string $othernames
 * @property string $wrongnames
 * @property string $sorthash
 * @property integer $xpressid
 * @property string $inn
 * @property string $okonh
 * @property string $okpo
 * @property string $pay_account
 * @property string $corr_account
 * @property string $kpp
 * @property string $bik
 * @property string $bank
 * @property integer $checked
 * @property integer $enabled
 * @property integer $ws1c_synced
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $label
 *
 * @property NxAddress $legalAddress
 * @property NxAddress $address
 * @property NxPerson $contactPerson
 * @property NxOrder[] $nxOrders
 * @property NxPerson[] $nxPeople
 */
class Company extends \common\models\WatchableModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['name','email', 'address_id'], 'required'], // по идее ИНН тоже должен быть обязательным (КПП у ип может не быть) 
            [['name', 'address_id', 'inn'], 'required'], // 
            [['source_id', 'channel_id', 'address_id', 'corr_account', 'bik', 'inn', 'okpo','kpp','okonh','legal_address_id', 'contact_person_id', 'xpressid', 'checked', 'enabled', 'ws1c_synced', 'create_user_id', 'last_user_id', 'inn'], 'integer'],
            [['description'], 'string'],
            ['email', 'email'],
            ['url', 'url', 'defaultScheme' => 'https'],
            [['created', 'last_updated', 'last_user_id','create_user_id'], 'safe'],
            [['name', 'email', 'url', 'shortname', 'othernames', 'wrongnames', 'sorthash', 'bank', 'label'], 'string', 'max' => 255],
            [['org_form'], 'string', 'max' => 100],
            // [['inn', 'okonh', 'okpo', 'kpp'], 'string', 'max' => 20],

            [['pay_account'], 'string', 'max' => 25],
            // [['bik'], 'string', 'max' => 15],
            [['legal_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxAddress::className(), 'targetAttribute' => ['legal_address_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxAddress::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['contact_person_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxPerson::className(), 'targetAttribute' => ['contact_person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'name'              => 'Название',
            'source_id'         => 'Source ID',
            'channel_id'        => 'Channel ID',
            'org_form'          => 'Орг. форма',
            'address_id'        => 'Адрес',
            'legal_address_id'  => 'Юридический адрес ',
            'contact_person_id' => 'Контактное лицо',
            'email'             => 'Email',
            'url'               => 'сайт',
            'description'       => 'Описание',
            'shortname'         => 'Короткое название',
            'othernames'        => 'Др.названия',
            'wrongnames'        => 'Wrongnames',
            'sorthash'          => 'Sorthash',
            'xpressid'          => 'Xpressid',
            'inn'               => 'Инн',
            'okonh'             => 'Код отрасли народного хоз-ва',
            'okpo'              => 'Код ОКПО',
            'pay_account'       => 'Расч. счет',
            'corr_account'      => 'Кор. счет',
            'kpp'               => 'КПП',
            'bik'               => 'БИК банка',
            'bank'              => 'Банк',
            'checked'           => 'Checked',
            'enabled'           => 'Активен',
            'ws1c_synced'       => 'Ws1c Synced',
            'created'           => 'Создан',
            'last_updated'      => 'Последнее обновление',
            'create_user_id'    => 'Создан пользователем',
            'last_user_id'      => 'Обновлен пользователем',
            'label'             => 'Метка',
            'address'           => 'Адрес',
            'contactPerson'     => 'Контактное лицо',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalAddress()
    {
        return $this->hasOne(NxAddress::className(), ['id' => 'legal_address_id']);//->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(NxAddress::className(), ['id' => 'address_id']); //->inverseOf('companies0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactPerson()
    {
        return $this->hasOne(NxPerson::className(), ['id' => 'contact_person_id']);//->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrders()
    {
        return $this->hasMany(NxOrder::className(), ['company_id' => 'id']);//->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPeople()
    {
        return $this->hasMany(NxPerson::className(), ['company_id' => 'id']);//->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);//->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);//->inverseOf('company');
    }
}
