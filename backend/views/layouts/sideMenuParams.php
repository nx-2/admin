<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Url;

/**
 * структура массива настроек:
 * массив для построения ul имеет всего два ключа 
 * ulAttributes = [] - любые атрибуты 
 * и items = [] - массивы для построения элементов меню
 *
 * items имеет два ключа li - для построения li
 * и allowedRoles - массив с ролями кому доступен этот пункт меню

  Массив для построения li может иметь такие ключи:
 * attributes = [] - массив с атрибутами тега
 * text = '' - строка текстового наполнения тега li
 * link = [] - массив для посторения встроенного внутрь li тега a
 * childMenu = [] - массив для построрния вложенного в li тега ul (структуру см. выше)
 *
 * Массив для построения вложенного тега a (ключ link) может иметь
 * text = '' строка текста
 * attributes = атрибуты
 * icon = [] - массив для построения вложенного в a тега i
 * hasChildrenMark = boolean - флаг, предписывающий нужно ли вложить внутрь ссылки следующий html 
 * <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
 *
 * */


$propertyGroups = common\models\PublisherPropertyGroup::getDb()->cache(function(){
    return common\models\PublisherPropertyGroup::find()->asArray(true)->all();
},300);

$propertiesSubmenuItems = [];
foreach($propertyGroups as $group) {
    $iconClass = !empty($group['icon-name']) ? 'fa ' . $group['icon-name'] : 'fa fa-circle-o-notch';
    $submenuItem = [
                        'li' => [
                            'attributes' => [],
                            'link' => [
                                'attributes' => ['href' => Url::to([$group['route']])],
                                'text' => $group['group_name'],
                                'icon' => [
                                    'attributes' => ['class' => $iconClass],
                                ],
                                'hasChildrenMark' => false,
                            ],
                        ],
                        'allowedRoles' => ['publisher'],
                ];
    $propertiesSubmenuItems [] = $submenuItem;
}
return [
    'ulAttributes' => [
        'class' => 'sidebar-menu',
    ],
    'items' => [
//        [
//            'li' => [
//                'attributes' => ['class' => 'header'],
//                'text' => 'НАВИГАЦИЯ',
//            ],
//        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Издатель',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['publisher/update', 'id' => Yii::$app->user->identity->publisher_id]),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-leanpub'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Профиль',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['user/update', 'id' => Yii::$app->user->identity->User_ID]),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-user'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['accountant', 'ship_manager'],
            'requiredPermissions' => ['updateOwnProfile'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Настройки',
                    'attributes' => [
                        'href' => '#',
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-cogs'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => true,
                ],
                'childMenu' => [
                    'ulAttributes' => ['class' => 'treeview-menu'],
                    'items' => $propertiesSubmenuItems,
                ],
            ],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Издатели',
                    'attributes' => [
                        'href' => \yii\helpers\Url::to(['/adm/publisher/list']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-leanpub'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['admin'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Пользователи',
                    'attributes' => [
                        'href' => \yii\helpers\Url::to(['/adm/user/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-users'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['admin'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Роли',
                    'attributes' => [
                        'href' => \yii\helpers\Url::to(['/adm/roles/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-user-plus'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['developer'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Справочники',
                    'attributes' => [
                        'href' => '#',
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-database'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => true,
                ],
                'childMenu' => [
                    'ulAttributes' => ['class' => 'treeview-menu'],
                    'items' => [
                        [
                            'li' => [
                                'attributes' => [],
                                'link' => [
                                    'attributes' => ['href' => Url::to(['/adm/company/index'])],
                                    'text' => 'Компании',
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                    ],
                                    'hasChildrenMark' => false,
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'link' => [
                                    'text' => 'Физ.лица',
                                    'attributes' => ['href' => Url::to(['/adm/person/index'])],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                    ],
                                    'hasChildrenMark' => false,
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'link' => [
                                    'text' => 'Адреса',
                                    'attributes' => ['href' => Url::to(['/adm/address/index'])],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                    ],
                                    'hasChildrenMark' => false,
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'link' => [
                                    'text' => 'Страны',
                                    'attributes' => ['href' => Url::to(['/adm/country/index'])],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                    ],
                                    'hasChildrenMark' => false,
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'link' => [
                                    'text' => 'Регионы',
                                    'attributes' => ['href' => Url::to(['/adm/region/index'])],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                    ],
                                    'hasChildrenMark' => false,
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => ['class'=> 'treeview'],
                                'link' => [
                                    'text' => 'Уведомления',
//                                    'attributes' => ['href' => Url::to(['/adm/region/index'])],
                                    'attributes' => ['href' => '#', 'class' => 'fixed'],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-envelope'],
                                    ],
                                    'hasChildrenMark' => true,
                                ],
                                'childMenu' => [
                                    'ulAttributes' => ['class' => 'treeview-menu'], 
                                    'items' => [
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                    'text' => 'типы уведомлений',
                                                    'attributes' => ['href' => Url::to(['/adm/email-notice/index']), 'class' => 'fixed'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-th-list'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
                                            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                    'text' => 'шаблоны уведомлений',
                                                    'attributes' => ['href' => Url::to(['/adm/email-notice-template/index']), 'class' => 'fixed'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-sticky-note'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
                                            'allowedRoles' => ['admin'],
                                        ],
                                    ],
                                ],
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                    ],
                ],
            ],
            'allowedRoles' => ['admin'],
        ],
//        [
//            'li' => [
//                'attributes' => ['class' => 'treeview'],
//                'text' => '',
//                'link' => [
//                    'text' => 'Администрирование',
//                    'attributes' => [
//                        'href' => '#',
//                    ],
//                    'icon' => [
//                        'attributes' => ['class' => 'fa fa-leanpub'],
//                        'text' => '',
//                    ],
//                    'hasChildrenMark' => true,
//                ],
//                'childMenu' => [
//                    'ulAttributes' => ['class' => 'treeview-menu'],
//                    'items' => [
//                        [
//                            'li' => [
//                                'attributes' => [],
//                                'text' => '',
//                                'link' => [
//                                    'text' => 'Издатели',
//                                    'attributes' => ['href' => '#'],
//                                    'icon' => [
//                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                        'text' => '',
//                                    ],
//                                    'hasChildrenMark' => true,
//                                ],
//                                'childMenu' => [
//                                    'ulAttributes' => ['class' => 'treeview-menu'],
//                                    'items' => [
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Список',
//                                                    'attributes' => ['href' => Url::to(['/adm/publisher/list'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Создать',
//                                                    'attributes' => ['href' => Url::to(['/adm/publisher/create'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                    ],
//                                ],
//                            ],
//				            'allowedRoles' => ['admin'],
//                        ],
//                        [
//                            'li' => [
//                                'attributes' => [],
//                                'text' => '',
//                                'link' => [
//                                    'text' => 'Пользователи',
//                                    'attributes' => ['href' => '#'],
//                                    'icon' => [
//                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                        'text' => '',
//                                    ],
//                                    'hasChildrenMark' => true,
//                                ],
//                                'childMenu' => [
//                                    'ulAttributes' => ['class' => 'treeview-menu'],
//                                    'items' => [
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Список',
//                                                    'attributes' => ['href' => Url::to(['/adm/user/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Создать',
//                                                    'attributes' => ['href' => Url::to(['/adm/user/create'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                    ],
//                                ]
//                            ],
//				            'allowedRoles' => ['admin'],
//                        ],
//                        [
//                            'li' => [
//                                'attributes' => [],
//                                'text' => '',
//                                'link' => [
//                                    'text' => 'Справочники',
//                                    'attributes' => ['href' => '#'],
//                                    'icon' => [
//                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                        'text' => '',
//                                    ],
//                                    'hasChildrenMark' => true,
//                                ],
//                                'childMenu' => [
//                                    'ulAttributes' => ['class' => 'treeview-menu'],
//                                    'items' => [
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                    'attributes' => ['href' => Url::to(['/adm/company/index'])],
//                                                	'text' => 'Компании',
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Физ.лица',
//                                                    'attributes' => ['href' => Url::to(['/adm/person/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                	'text' => 'Адреса',
//                                                    'attributes' => ['href' => Url::to(['/adm/address/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//								            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                    'text' => 'Страны',
//                                                    'attributes' => ['href' => Url::to(['/adm/country/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//                                            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                    'text' => 'Регионы',
//                                                    'attributes' => ['href' => Url::to(['/adm/region/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//                                            'allowedRoles' => ['admin'],
//                                        ],
//                                    ],
//                                ]
//                            ],
//                            'allowedRoles' => ['admin'],
//                        ],
//                        [
//                            'li' => [
//                                'attributes' => [],
//                                'text' => '',
//                                'link' => [
//                                    'text' => 'Роли',
//                                    'attributes' => ['href' => '#'],
//                                    'icon' => [
//                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                        'text' => '',
//                                    ],
//                                    'hasChildrenMark' => true,
//                                ],
//                                'childMenu' => [
//                                    'ulAttributes' => ['class' => 'treeview-menu'],
//                                    'items' => [
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                    'text' => 'Список',
//                                                    'attributes' => ['href' => Url::to(['/adm/roles/index'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//                                            'allowedRoles' => ['admin'],
//                                        ],
//                                        [
//                                            'li' => [
//                                                'attributes' => [],
//                                                'link' => [
//                                                    'text' => 'Создать',
//                                                    'attributes' => ['href' => Url::to(['/adm/roles/create'])],
//                                                    'icon' => [
//                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
//                                                    ],
//                                                    'hasChildrenMark' => false,
//                                                ],
//                                            ],
//                                            'allowedRoles' => ['admin'],
//                                        ],
//                                    ],
//                                ],
//                            ],
//				            'allowedRoles' => ['admin'],
//                        ],
//                    ]
//                ]
//            ],
//            'allowedRoles' => ['admin'],
//        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Сотрудники',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['user/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-users'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Издания',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/edition/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-book'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Выпуски',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/issue/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-vcard-o'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['ship_manager'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Цены',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/price/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-rouble'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['subscribe_manager'],
        ],
//        [
//            'li' => [
//                'attributes' => ['class' => 'treeview'],
//                'text' => '',
//                'link' => [
//                    'text' => 'Партнеры',
//                    'attributes' => [
//                        'href' => \yii\helpers\Url::toRoute(['/partner/index']),
//                    ],
//                    'icon' => [
//                        'attributes' => ['class' => 'fa fa-handshake-o'],
//                        'text' => '',
//                    ],
//                    'hasChildrenMark' => false,
//                ]
//            ],
//            'allowedRoles' => ['publisher'],
//        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Акции',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/action/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-arrow-down'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['subscribe_manager'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Платежные системы',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/paysystem/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-money'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['publisher'],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Подписные формы',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/subscribe-forms/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-life-bouy'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['ship_manager'],
            'requiredPermissions' => 'accessSubscribeForms'
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Уведомления',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['/email-notice/index']),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-envelope'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
//            'allowedRoles' => ['@'],
            'allowedRoles' => ['publisher'],
        ],
    ]
];
