<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->User_ID;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->User_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->User_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'User_ID',
            'Password',
            'Checked',
            'Language',
            'Created',
            'LastUpdated',
            'Email:email',
            'Login',
            'FullName',
            'LastName',
            'FirstName',
            'MidName',
            'Confirmed',
            'RegistrationCode',
            'Auth_Hash:ntext',
            'company',
            'job',
            'phone',
            'country',
            'city',
            'address:ntext',
            'street',
            'house',
            'building',
            'flat',
            'url:url',
            'company_profile',
            'sex',
            'birthday',
            'phone_prefix',
            'fax_prefix',
            'fax',
            'region',
            'district',
            'index_num',
            'open_email:email',
            'avatar',
            'UserType',
            'Text:ntext',
            'LastLoginDate',
            'publisher_id',
        ],
    ]) ?>

</div>
