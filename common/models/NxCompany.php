<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "nx_company".
 *
 * @property integer $id
 * @property string $name
 * @property string $source_id
 * @property string $channel_id
 * @property string $org_form
 * @property string $address_id
 * @property string $legal_address_id
 * @property integer $contact_person_id
 * @property string $email
 * @property string $url
 * @property string $description
 * @property string $shortname
 * @property string $othernames
 * @property string $wrongnames
 * @property string $sorthash
 * @property integer $xpressid
 * @property string $inn
 * @property string $okonh
 * @property string $okpo
 * @property string $pay_account
 * @property string $corr_account
 * @property string $kpp
 * @property string $bik
 * @property string $bank
 * @property integer $checked
 * @property integer $enabled
 * @property integer $ws1c_synced
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $label
 */
class NxCompany extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'created', 'create_user_id', 'last_user_id'], 'required'],
            [['source_id', 'channel_id', 'address_id', 'legal_address_id', 'contact_person_id', 'xpressid', 'checked', 'enabled', 'ws1c_synced', 'create_user_id', 'last_user_id'], 'integer'],
            [['description'], 'string'],
            [['created', 'last_updated'], 'safe'],
            [['name', 'email', 'url', 'shortname', 'othernames', 'wrongnames', 'sorthash', 'corr_account', 'bank', 'label'], 'string', 'max' => 255],
            [['org_form'], 'string', 'max' => 100],
            [['inn', 'okonh', 'okpo', 'kpp'], 'string', 'max' => 20],
            [['pay_account'], 'string', 'max' => 25],
            [['bik'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'source_id' => 'Source ID',
            'channel_id' => 'Channel ID',
            'org_form' => 'Org Form',
            'address_id' => 'Address ID',
            'legal_address_id' => 'Legal Address ID',
            'contact_person_id' => 'Contact Person ID',
            'email' => 'Email',
            'url' => 'Url',
            'description' => 'Description',
            'shortname' => 'Shortname',
            'othernames' => 'Othernames',
            'wrongnames' => 'Wrongnames',
            'sorthash' => 'Sorthash',
            'xpressid' => 'Xpressid',
            'inn' => 'Inn',
            'okonh' => 'Okonh',
            'okpo' => 'Okpo',
            'pay_account' => 'Pay Account',
            'corr_account' => 'Corr Account',
            'kpp' => 'Kpp',
            'bik' => 'Bik',
            'bank' => 'Bank',
            'checked' => 'Checked',
            'enabled' => 'Enabled',
            'ws1c_synced' => 'Ws1c Synced',
            'created' => 'Created',
            'last_updated' => 'Last Updated',
            'create_user_id' => 'Create User ID',
            'last_user_id' => 'Last User ID',
            'label' => 'Label',
        ];
    }
}
