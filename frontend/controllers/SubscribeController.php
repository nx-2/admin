<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use common\models\NxOrder;
use common\models\Subscribe;
use Yii;
use yii\db\Transaction;
use yii\rest\Controller;


class SubscribeController extends Controller
{

    public $modelClass = 'common\models\Subscribe';

    /**
     * @return array
     * не используется. 
     * Вместо нее написана хранимая процедура, запускающаяся в frontend/controllers/EditionController->actionPrice()
     */
    public function actionGetPrice()
    {

        $model = new \common\models\validators\Price();
        $model->attributes = Yii::$app->request->post();;

        if (!$model->validate())
            throw  new \yii\web\HttpException(200, \common\models\Subscribe::getError($model));

        return $this->getModel()->getPrice($model);


    }
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        header('Access-Control-Allow-Origin: *');
        return true;
    }

    /**
     * Get realease
     * @return int
     * @throws \yii\web\HttpException
     * Не используется.
     * Вместо нее написана хранимая процедура issues_count, запускаемая в 
     * frontend/controllers/IssueController->actionCount()
     */

    public function actionCountRelease()
    {

        $model = new \common\models\validators\CountRelease();
        $model->attributes = Yii::$app->request->post();

        if (!$model->validate())
            throw  new \yii\web\HttpException(200, \common\models\Subscribe::getError($model));

        return $this->getModel()->getMagazineCount($model);

    }

    /**
     * Get Promo
     * @return int|void
     */
    public function actionGetPromo()
    {

        $model = new \common\models\validators\Promo();
        $model->attributes = Yii::$app->request->post();;

        if (!$model->validate())
            throw  new \yii\web\HttpException(200, \common\models\Subscribe::getError($model));

        if($model->customer_type_id==2)
            return 0;

        return $this->getModel()->getPromo($model);

    }


    /**
     * Unset promocode
     */
    public function actionUnsetPromo()
    {
        $order_id = Yii::$app->request->getQueryParam('order_id', 0);
        $this->getModel()->setPromo($order_id);

    }


    /**
     * Get Form
     * @return string html code of subscription form
     * 
     */
    public function actionForm()
    {

        $mag_id = intval(Yii::$app->request->getQueryParam('mag_id', false));
        $publisher_id= intval(Yii::$app->request->getQueryParam('publisher_id', false));
        
        if (!$mag_id || !$publisher_id)
            return '<div>magId and publisherId are required!!!</div>';

        $journal = \common\models\Edition::findOne($mag_id);
        if ($journal == null)
            return '<div>Edition was not found!!!</div>';
        
        if ($journal->publisher->id != $publisher_id)
            return '<div>This edition does not belong to this publisher or publisher was not found!!!</div>';

        
        //ищем цены
//        if(!$journal=\common\models\NxPrice::find()->where(['publisher_id'=>$publisher_id])->one())
//             throw  new \yii\web\HttpException(200, 'Price  not found');

//        $journal->mag_id=$mag_id;

//        if(!$journal=$journal->nxPriceItemsOne->edition)
//            throw  new \yii\web\HttpException(200, 'Journals  not found');

        $model = new \common\models\SubscribeForm();
        
        return $this->renderPartial('//subscribe/subscribeForm', [
            'model' => $model,
//            'cover' => $journal->lastCoverUrl(),
            'magazineID' => $mag_id,
            'publisher_id' => $publisher_id,
            'journal'=>$journal
        ]);
    }
    public function actionMultyMagForm()
    {
        /**
        Принимает ajax запрос со страницы, где должна быть размещена подписная форма
        Ожидается post запрос, в котором присутствуют: Ид издателя, ширина формы, флаг "Показывать(НЕ показывать) обложки", список изданий для формы
        В результате обработки запроса возвращается html код формы вместе со стилями. Весь необходимый js к этому моменту уже должен быть внедрен в страницу.
        **/
        $pId = Yii::$app->request->post('pId', false);
        $width = Yii::$app->request->post('width', 'auto');
        $showCovers = Yii::$app->request->post('showCovers', true);
        $editionsIds = Yii::$app->request->post('editionsList', 'ALL');
        
        if ($editionsIds != 'ALL' && $editionsIds != 'all') {
            $editionsIds = explode(',', $editionsIds);
            $editionsIds = array_map(function($elem) {return intval($elem); }, $editionsIds );
            $editionsIds = array_filter($editionsIds);
        } else {
            $editionsIds = 'ALL';
        }
        
        $width = (strtolower($width) != 'auto') ? intval($width) . 'px' : '100%';
        $showCovers = (Boolean) $showCovers;
        
        $data = [
            'width' => $width,
            'showCovers' => $showCovers,
            'publisherId' => $pId,
            'editions' => $editionsIds,
        ];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = ['status' => 'err','html' => '', 'errMessage' => ''];
        
        if (!$pId) {
            $response['errMessage'] = 'Not enough data';
            return $response;
        }
        
        $publisher = \common\models\Publisher::findOne(['id' => intval($pId)]);
        if ( $publisher === null ) {
            $response['errMessage'] = 'Invalid data';
            return $response;
        }

        $avalableEditionsIds = $publisher->allMyActiveEditionsIds;
        if ( $editionsIds != 'ALL' && is_array($editionsIds) ) {
            $avalableEditionsIds = array_intersect($avalableEditionsIds, $editionsIds);
        }
        $editions = \common\models\Edition::findAll(['Message_ID' => $avalableEditionsIds]);
                
//        $editions = $publisher->getEditionsByIds($avalableEditionsIds);
        
        $html =  $this->renderPartial('multyMagForm', [
            'width' => $width,
            'showCovers' => $showCovers,
            'publisherId' => $pId,
            'editions' => $editions,
            'publisher' => $publisher,
            'deliveryTypes' => \common\models\DeliveryType::find()->where(['enabled' => 1])->andWhere(['<>','id',5])->all(), //PDF исключить
            'deliveryZones' => \common\models\NxZone::findAll(['enabled' => 1]),
        ]);

        $html = preg_replace('/\n/', '', $html);
        
        $response['status'] = 'OK';
        $response['html'] = $html;
        unset($response['errMessage']);
        
        return $response;
    }
    
    public function actionAddMagItem()
    {
        /** 
         * принимает ajax запрос от подписной формы на добавление в "заказ" нового издания
         * В ответ возвращает html код 
        **/
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $publisherId = Yii::$app->request->post('pId', false);
        $editionId = Yii::$app->request->post('eId', false);
        $index = Yii::$app->request->post('index', false);
        
        $response = ['status' => 'err', 'errMessage' => ''];
        
        if ( !$publisherId || !$editionId || !$index ) {
            $response['errMessage'] = 'Not enough data.';
            return $response;
        }
        $publisher = \common\models\Publisher::findOne(intval($publisherId));
        $allAvalableEditionsIds = $publisher->getAllMyActiveEditionsIds();

        if ($publisher === null || !in_array(intval($editionId), $allAvalableEditionsIds)) {
            $response['errMessage'] = 'Invalid data.';
            return $response;
            
        }
        $edition = \common\models\Edition::findOne(intval($editionId));
        $html = $this->renderPartial('edition_item_for_multyform', ['edition' => $edition, 'index' => intval($index)]);
        $html = preg_replace('/\n/', '', $html);
        $response['status'] = 'OK';
        $response['html'] = $html;
        unset($response['errMessage']);
        return $response;
    }


    /**
     * Get Form One
     * @return bool|string|\yii\web\Response
     */
    public function actionFormOne()
    {

        $mag_id = Yii::$app->request->getQueryParam('mag_id', 0);
        $year = Yii::$app->request->getQueryParam('year', 0);
        $month = Yii::$app->request->getQueryParam('month', 0);
        $issue_id = Yii::$app->request->getQueryParam('issue_id', 0);



        if($issue_id){
            $issue=\common\models\Issue::find()->where(['Message_ID'=>$issue_id])->one();
        }else{
            if (!$journal=\common\models\ActionItems::getOneJournals($mag_id))
                throw  new \yii\web\HttpException(200, 'Journals not found');

            $issue=\common\models\Issue::find()->where(['year' => $year,'MagazineID'=>$mag_id,'release_month'=>$month])->one();
        }


        if (empty($issue))
            throw  new \yii\web\HttpException(200, 'Issue not found');


        $model = new \common\models\SubscribeFormOne();

        return $this->render('//subscribe/subscribeFormOne', [
            'model' => $model,
            'issue'=>$issue,
            'magazineID' => $mag_id,
            'journal'=>$journal
        ]);
    }

    public function actionCreateTest()
    {
        $model = new \common\models\SubscribeForm();

        if (!\Yii::$app->request->post()) {
            return json_encode(['status' => 'Err', 'errors' => ['не получены данные']]);
        }
        $model->load(\Yii::$app->request->post());

        if (!$model->validate()) {
            return  $model->errors;
//            return json_encode(['status' => 'err', 'errors' => $model->errors]);
        }
        $subscribeService = new \common\services\SubscribeService();
        return $subscribeService->createSubscribe($model->attributes);
        
    }
    public function actionAddOrder() {
        $result = ['post' => $_POST];
        return json_encode($result);
    }
    public function actionCreateOrder()
    {
        $model = new \common\models\SubscribeForm();

        if (!\Yii::$app->request->post()) {
            return json_encode(['status' => 'Err', 'errors' => ['не получены данные']]);
        }
        $model->load(\Yii::$app->request->post());

        if (!$model->validate()) {
            return json_encode(['status' => 'err', 'errors' => $model->errors]);
        }

        $orderId = $this->getModel()->saveOrder($model);
// тест создания заказа в "местном" сервисе
//        $orderId_ = \common\services\SubscribeService::createSubscribe($model->attributes);
        $order = \common\models\NxOrder::findOne($orderId);

        //ищем цены
//        if(!$journal=\common\models\NxPrice::find()->where(['publisher_id'=>$publisher_id])->one())
//             throw  new \yii\web\HttpException(200, 'Price  not found');
        
        if (empty($order))
            return json_encode(['status' => 'err', 'errors' => ['order' => 'Some Error while order saving']]);
            
        $transaction_id = $this->actionSaveTransaction($order->hash);

        if (!is_int($transaction_id))
            return json_encode(['status' => 'err', 'errors' => ['transaction' => 'Transaction could not created']]);
        
        $orderCreatedEvent = new \common\models\events\ARcreatedEvent(['model' => $order]);
        $order->trigger(NxOrder::NEW_ORDER_EVENT_NAME, $orderCreatedEvent);
                
       
//        $html = $this->saveOrder($subscriptionID, $model->publisher_id);
        $html = $this->actionPayAllMethod($order->hash, $model->publisher_id);
        return json_encode(['status' => 'OK', 'html' => $html]);

//        return $this->saveOrder($subscriptionID);
    }

    public function actionCreatePdf()
    {
        $model = new \common\models\SubscribeFormOne();

        if (!\Yii::$app->request->post())
            return false;

        $model->load(\Yii::$app->request->post());

        if (!$model->validate()) {
            \Yii::$app->getSession()->setFlash('error', \common\models\Subscribe::getError($model));
            return false;
        }

        $subscriptionID = $this->getModel()->saveBuy($model);

        return $this->saveOrder($subscriptionID);
    }


    /**
     * create-order for nx
     * @return bool|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    private function saveOrder($subscriptionID, $publisherId = false)
    {
// !!!! 12.08.2017 Этот метод скорее всего вообще не нужен. НЕ удален сразу просто на всякий случай. Подлежит удалению. А.Щ.
        if (!$orderNew = \common\models\NxOrder::getOrderID($subscriptionID))
            throw  new \yii\web\HttpException(200, 'Order not found');

//        $transaction_id = \common\models\Subscribe::sendRequestApi([], \yii\helpers\Url::to('@domain/subscribe/saveTransaction/'.$orderNew->hash.'?_format=json'));
        $transaction_id = $this->actionSaveTransaction($orderNew->hash);
        

        if (!is_int($transaction_id))
            throw  new \yii\web\HttpException(200, 'Transaction for order '. $orderNew->hash . ' was not created');

//        $pay_method= \common\models\Subscribe::sendRequestApi([], \yii\helpers\Url::to('@domain/subscribe/payAllMethod/'.$orderNew->hash.'/'.$transaction_id.'?_format=json'));
        $pay_method=  $this->actionPayAllMethod($orderNew->hash,$publisherId);

        if (!$pay_method)
            throw  new \yii\web\HttpException(200, 'No pay method');
        
        if(is_object($pay_method))
            return json_encode ($pay_method);

//        $pay_method= str_replace("\n", "", $pay_method);
//        $pay_method= str_replace("\r", "", $pay_method);

        return $pay_method;
//        return stripcslashes($pay_method);

    }

    /**
     * getPayMethod
     * 2 -yandex, 6- Assist, QIWI-5, 1 - Робокасса
     * @return bool|string
     */
    public function actionPayMethod()
    {
        // 12.08.2017 Этот метод скорее всего не нужен. В модели RelationSystems есть метод buildPayingForm()
        // который принимает на вход заказ и ИД издателя, а возвращает html платежной формы. Т.е. делает тоже самое что этот метод
        // Метод не удален сразу из-за отсутствия полной уверенности, что он где-то может вызываться
        // TODO: пофайндить по проекту на предмет вызова этого метода, затем удалить его нах.
        $order_hash = Yii::$app->request->getQueryParam('order_hash', 0);
        $method_pay = Yii::$app->request->getQueryParam('method_pay', 0);
        $transaction_id = Yii::$app->request->getQueryParam('transaction_id', 0);

        if (!$transaction = \common\models\Transactions::getTransactionId($transaction_id))
            throw  new \yii\web\HttpException(200, 'Transactions not found');

        if (!$order = \common\models\NxOrder::getOrder($order_hash))
            throw  new \yii\web\HttpException(200, 'Order not found');

        $paySystems = \common\models\PaySystems::find()->where(['id' => $method_pay])->one();

        if (!$person = \common\models\NxPerson::find()->where(['id' => $order->person_id])->one())
            throw  new \yii\web\HttpException(200, 'Person not found');

        $transaction->system = $method_pay;
        $transaction->date = date('Y-m-d h:i:s');
        $transaction->update();

        return $this->renderPartial('//subscribe/pay/' . trim(mb_strtolower(($paySystems->workname) ? $paySystems->workname : 'paydoc')), [
            'model' => $order,
            'paySystems' => $paySystems,
            'transaction_id' => $transaction->id,
            'person' => $person
        ]);


    }


    /**
     * Save Transctions
     * @throws \yii\web\HttpException
     */

    public function actionSaveTransaction($order_hash = false)
    {
        $order_hash = !$order_hash ?  Yii::$app->request->getQueryParam('order_hash', 0) : $order_hash;

        if (!$order = \common\models\NxOrder::getOrder($order_hash))
            throw  new \yii\web\HttpException(200, 'Order not found');

        if ($transaction_id = \common\models\Transactions::getTransaction($order->id))
            return $transaction_id->id;

        return intval(\common\models\Transactions::setTransaction($order)->id);
    }

    /**
     * Get all pay method
     *  * 2 -yandex, 6- Assist, QIWI-5
     * @return bool|string
     * returns raw html of all avalable pay methods 
     */
    public function actionPayAllMethod($order_hash = false, $publisher_id = false)
    {
        $order_hash = !$order_hash ? Yii::$app->request->getQueryParam('order_hash', 0) : $order_hash;
        $publisher_id = !$publisher_id ? Yii::$app->request->getQueryParam('publisher_id', 0) : $publisher_id;

        if (!$order = \common\models\NxOrder::findOne(['hash' => $order_hash]))
            throw new \yii\web\BadRequestHttpException('Order not found');
//            throw  new \yii\web\HttpException(200, 'Order not found');

        if ($order->company_id != null)
            throw new \yii\web\BadRequestHttpException('This order belongs to company. Not to person.');

        if ($order->publisher->id != $publisher_id)
            throw new \yii\web\BadRequestHttpException('order ' . $order->id . ' does not belong to publisher ' . $publisher_id .' or publisher was not found');
//            throw  new \yii\web\HttpException(200, 'this order does not belong to this publisher or publisher was not found');


        //проверить какие методы оплаты доступны магазину в будущем
        
        if (!$paySystems = \common\models\RelationSystems::find()->where(['publisher_id' => intval($publisher_id), 'enabled' => 1])->all())
            throw new \yii\web\BadRequestHttpException('No payment systems for this publisher');
//            throw  new \yii\web\HttpException(200, 'No payment systems for this publisher');

        return $this->renderPartial('//subscribe/pay/allmethod', [
            'model' => $order,
            'paySystems' => $paySystems,
            'publisherId' => $publisher_id,
        ]);

    }

    /**
     * Refresh pay systems
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionSetPayAll()
    {
        $model = new \common\models\validators\Pay();
        $model->attributes = Yii::$app->request->post();

        if (!$model->validate())
            throw  new \yii\web\HttpException(200, \common\models\Subscribe::getError($model));

        $order = \common\models\NxOrder::findOne(['hash' => $model->hash]);
        
        if (!$order)
            throw  new \yii\web\HttpException(200, 'Order not found');

        if (!$transaction = \common\models\Transactions::getTransactionId($model->transaction_id))
            throw  new \yii\web\HttpException(200, 'transaction  not found');

        if (!$person = \common\models\NxPerson::find()->where(['id' => $order->person_id])->one())
            throw  new \yii\web\HttpException(200, 'Person not found');

        if(!$paySystems = \common\models\RelationSystems::find()->where(['system_id' => $model->pay_id])->one())
            throw  new \yii\web\HttpException(200, 'PaySystems not found');


        $transaction->system = $model->pay_id;
        $transaction->date = date('Y-m-d h:i:s');
        $transaction->update();


        return $this->renderPartial('//subscribe/pay/' . trim(mb_strtolower($paySystems->systems->workname)) , [
            'model' => $order,
            'paySystems' => $paySystems,
            'transaction_id' => $transaction->id,
            'person' => $person
        ]);

    }

    /**
     * Get Doc Payment
     * @return bool|string
     */
    public function actionPayDoc()
    {
        $order_hash = Yii::$app->request->getQueryParam('order_hash', 0);
        $customer_type = Yii::$app->request->getQueryParam('type_id', 1);

        if (!$order = \common\models\NxOrder::getOrder($order_hash))
            throw  new \yii\web\HttpException(200, 'Order not found');

        if (!$person = \common\models\NxPerson::find()->where(['id' => $order->person_id])->one())
            throw  new \yii\web\HttpException(200, 'Person not found');

        if($customer_type==2)
            if (!$legal = \common\models\NxCompany::find()->where(['id' => $order->company_id])->one())
                throw  new \yii\web\HttpException(200, 'Company not found');

        $templates = ($customer_type == 1) ? "individualTempalte" : "legalTempalte";

        return $this->renderPartial('//subscribe/pay/' . $templates, [
            'model' => $order,
            'person' => $person,
            'legal' => ($legal)?$legal:null
        ]);
    }

    /**
     * Get result Doc Payment
     * @return bool|string
     */
    public function getPayDoc($order_hash,$customer_type=1,$templates=1)
    {

        if (!$order = \common\models\NxOrder::getOrder($order_hash))
            throw  new \yii\web\HttpException(200, 'Order not found');

        if (!$person = \common\models\NxPerson::find()->where(['id' => $order->person_id])->one())
            throw  new \yii\web\HttpException(200, 'Person not found');

        if($customer_type==2)
            if (!$legal = \common\models\NxCompany::find()->where(['id' => $order->company_id])->one())
                throw  new \yii\web\HttpException(200, 'Company not found');

        $templates = ($customer_type == 1) ? "individualTempalte" : "legalTempalte";

        return $this->renderPartial('//subscribe/pay/' . $templates, [
            'model' => $order,
            'person' => $person,
            'legal' => (isset($legal))?$legal:null
        ]);
    }


    /**
     * @return Subscribe
     */
    private function getModel()
    {
        return new Subscribe();
    }

    /**
     * @param $model
     */
    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            echo json_encode(\yii\widgets\ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }

}
