<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\validators;

use common\models\Publisher;

class Promo extends \yii\base\Model
{

    public $promocode;
    public $magazineID;
    public $period;
    public $type;
    public $customer_type_id;
    public $publisher_id;

    public function rules()
    {
        return [
            [['period', 'magazineID', 'promocode', 'customer_type_id'], 'required'],
            [['period', 'magazineID', 'type', 'customer_type_id', 'publisher_id'], 'integer']
        ];
    }
}