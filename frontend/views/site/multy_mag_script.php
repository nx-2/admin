<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
    $formId = Yii::$app->params['multyMagFormId'];
    $formWrapperId = Yii::$app->params['multyMagFormWrapperId'];
    $randomPrefix = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnm1234567890'), 0 ,12);
?>
// (function(window, document){
// })(window, document);

//add some custom functionality to Element js interface
( function( e ) {
	
	//matches polyfil
	e.matches || ( e.matches = e.matchesSelector || function( selector ) {
	  var matches = document.querySelectorAll( selector ), th = this;
	  return Array.prototype.some.call(matches, function(e){
	     return e === th;
	  });
	});
    e.matches = e.matches || e.mozMatchesSelector || e.msMatchesSelector || e.oMatchesSelector || e.webkitMatchesSelector;
    //=============
    
    // closest polyfil
    e.closest = e.closest || function closest( selector ) {
        if (!this) return null;
        if (this.matches(selector)) return this;
        if (!this.parentElement) {return null}
        else return this.parentElement.closest(selector)
      };
  	//=============

  	// build selector string (without referencies to parent owners)
  	e.mySelector = function mySelector () {
		var id = this.id == '' ? '' : '#' + this.id;
		var tag = this.tagName;
		var classNames = this.className == '' ? '' : this.className.split(' ');
		var classString = '';
		if ( classNames != '' ) {
			for (var k = 0; k < classNames.length; k++) {
				classString += classNames[k] == '' ? '' : '.' + classNames[k];
			}
		}
		if ( id  != '' ) {
			return id + classString;
		} else {
			return tag + classString;
		}
  	}

  	// generate random id attribute for any kind of temp goals
  	e.setTempId = function setTempId () {
  		if (!this.id || this.id == '') {
  			this.id = 'tmp_id_' + new Date.valueOf();

  		}
  		return this;
  	}

  	// remove temp id
  	e.removeTempId = function removeTempId () {
  		if ( !this.id || this.id == '' || this.id.indexOf('tmp_id_') == -1 )
  			return this;
  		this.removeAttribute('id');
  		return this;
  	}

  	// return  all siblings, which matches selector (if any)
  	e.getAllSiblings = function getSiblings(selector) {
  		var result = [];
  		var current = this;
  		var candidates = this.parentElement.children;

  		for (var i = 0; i < candidates.length; i++) {
  			if (candidates[i] != this) {
  				if (typeof selector != 'undefined') {
  					try {
  						if ( candidates[i].matches(selector) ) {
		  					result.push(candidates[i]);
  						}
  					} catch(err) {
  						console.log(err);
  					}
  				} else {
  					result.push(candidates[i]);
  				}
  			}
  		}
  		return result;
  	}
	
	//hide or display with animation effects
	e.fadeIn = function fadeIn() {
		this.classList.add('fadeIn');
	}
	e.fadeInUp = function fadeInUp() {
		this.classList.add('fadeInUp');
	}
	e.fadeInDown = function fadeInDown() {
		this.classList.add('fadeInDown');
	}
	e.fadeOut = function fadeOut() {
		this.classList.add('fadeOut');
	}
	e.fadeOutUp = function fadeOutUp() {
		this.classList.add('fadeOutUp');
	}
	e.fadeOutDown = function fadeOutDown() {
		this.classList.add('fadeOutDown');
	}
	e.flipInX = function flipInX() {
		this.classList.add('flipInX');
	}
	e.flipOutX = function flipOutX() {
		this.classList.add('flipOutX');
	}
	// <R> letter means that element will be removed after
	e.fadeOutR = function fadeOutR() {
		this.classList.add('fadeOutR');
	}
	e.fadeOutUpR = function fadeOutUpR() {
		this.classList.add('fadeOutUpR');
	}
	e.fadeOutDownR = function fadeOutDownR() {
		this.classList.add('fadeOutDownR');
	}
	e.flipOutXR = function flipOutXR() {
		this.classList.add('flipOutXR');
	}
	// append or prepend element inside of me. Position = 'start' | 'end' (default).
	e.customAddElement = function customAddElement(newElem, position) {
		// expected newElem as array of new elements
		if ( typeof position == 'undefined' || position == null || (position != 'start' && position != 'end') )  {
			position = 'end';
		}
		if ( !newElem.length || newElem.length == 0 )
			return this;

		var len = newElem.length;
		for ( var i = 0; i < len; i++ )  {
			if (position == 'end') {
				try {
					if ( newElem[0].tagName.toLowerCase() == 'script' ) {
						// Чтобы вставленные скрипты выполнились после добавления
						var scriptNode = document.createElement('script');
						scriptNode.innerHTML = newElem[0].text;
						document.querySelector('body').appendChild(scriptNode);
						newElem[0].parentNode.removeChild(newElem[0]);
					} else {
						this.append(newElem[0]);
					}
				} catch( e ) {
					console.log(e);
				}
			} else if ( position == 'start' ) {
				try {
					this.prepend(newElem[0]);
				} catch( e ) {
					console.log(e);
				}
			}
		}
		return this;
	}


})( Element.prototype );

//custom tool for ajax request send
function ajaxRequest( params ) {

	// params
	this.url = '';
	this.method = 'get';
	this.async = true;
	this.autoRun = false;
	this.data = {};

	this.init = function( params ) {
		
		if ( typeof params == 'object' ) {
			this.setOptions(params);
		} else if ( typeof params == 'string' ) {
			this.url = params;	
		}
		if ( this.autoRun )
			this.send();
		// return this;	
		return;
	}

	this.setOptions = function( options ) {
		if ( typeof options != 'object' ) {
			console.log('parameter <options> must be an object type');
		} else {
			for ( option in options ) {
				if ( this.hasOwnProperty(option) ) {
					this[option] = options[option];
				}
			}
		}
		return this;
	}

	this.setOption = function( optionName, optionValue ) {
		if ( typeof optionName !== 'string' || typeof optionValue !== 'string' ) {
			console.log('both parameters optionName and optionValue must be string');
			return this;
		}

		if ( !this.hasOwnProperty(optionName) ) {
			console.log('property ' + optionName + ' does not exists');
			return this;
		}

		this[optionName] = optionValue;
		return this;
	}

	//default implementation of "done" callback
	this.done = function ( response ) {
		console.log(response);
	}

	//default implementation of "fail" callback
	this.fail = function ( error ) {
		console.log(error);
	}

	this.objectToUrl = function ( object, name ) {
		if (typeof object != 'object')
			return '';
		
		if ( typeof name == 'undefined' || typeof name != 'string' ) {
			name = false; 
		} else {
			name =  name ; 
		}

		var urlParamStr = [];
		for ( var k in object ) {
			if ( object.hasOwnProperty(k) ) {
				if ( typeof object[k] == 'object' ) {
					if ( !name ) {
						urlParamStr.push( this.objectToUrl( object[k],  k  ) );
					} else {
						urlParamStr.push( this.objectToUrl( object[k],  name + '[' + k + ']' ) );
					}
				} else {
					if ( !name ) {
						urlParamStr.push( k + '=' + object[k]);
					} else {
						urlParamStr.push( name + '[' + k + ']' + '=' + object[k] );
					}
				}
			}
		}

		return urlParamStr.join('&');
	}
	
	// this.send = function( done, fail ) {
	this.send = function( params ) {
		
		if ( typeof params == 'object' ) {
			this.setOptions(params);
		}

		if ( typeof this.done !== 'function' ) {
			throw new TypeError('parameter <done> must be callback function');
			return false;
		}
		var done = this.done;
		
		if ( typeof this.fail !== 'function' ) {
			throw new TypeError('parameter <fail> must be callback function');
			return false;
		}
		var fail = this.fail;

		var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
		var xhr = new XHR();

		xhr.open( this.method, this.url, this.async );
		xhr.setRequestHeader('Accept', 'application/json, text/javascript, */*');

		if ( this.method.toLowerCase() == 'post' )
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xhr.onload = function() {
			if ( this.status != 200 ) {
				var errText = this.statusText ? this.statusText : 'An error has occurred while XHR request to url ' + this.responseURL;
				fail( errText );
			} else {
				var response = this.response;
				if ( typeof response == 'string' ) {
					try {
						response = JSON.parse( response );
					} catch ( e ) {
						fail( e.message );
					}
				}
				done( response );
			}
		}
		xhr.onerror = function() {
			var errText = this.statusText ? this.statusText : 'An error has occurred while XHR request to url ' + this.responseURL;
			fail( errText );
		}

		var result = xhr.send( this.objectToUrl(this.data) );
	}

	this.init( params );
}// end ajaxRequest

// run after document ready
document.onreadystatechange = function () { 
	var state = document.readyState;
	
	if (state == 'complete') {
		<?=$formWrapperId?>Start();
	}
}
function <?=$formWrapperId?>Start() {
		//build global object with some custom tools
		ospTools = {
			
			formWrapperId : '<?=$formWrapperId; ?>',
			apiUrl : '<?=$apiUrl; ?>',
			refreshUri : '/order/calculate-on-data',
			submitUri : '/order/add-order',

			ajaxRequest : new ajaxRequest(),
			
			// attach handler to ONE event for existing element
			addEvent : function addEvent(el, eName, handler) {if (el.attachEvent) el.attachEvent('on' + eName, handler); else el.addEventListener(eName, handler);},
			
			// attach handler to ONE event for existing and(or) all future elements, which matches selector. "context" parameter is option (document as default).  
			addLiveEvent : function addLiveEvent(selector, event, callback, context) {
							    this.addEvent(context || document, event, function(e) {
							        var found, el = e.target || e.srcElement;
							        while (el && el.matches && el !== context && !(found = el.matches(selector))) el = el.parentElement;
							        if (found) callback.call(el, e);
							    });
							},
			//get string with html code, return array of Elements
			createElement : function createElement( html ) {
				if (typeof html == 'undefined' || html == '')
					return false;

				var wrapper = document.createElement('div');
				wrapper.innerHTML = html;
				return wrapper.children;
			},

			getCheckedInput : function( owner ) {
				if ( typeof owner != 'object' )
					return false;

				var checked = owner.querySelector('input[type="radio"][name]:checked,input[type="checkbox"][name]:checked');
				if ( checked )
					return checked.value;

				return false;
			},

			refreshForm : function( ) {
				var data = osp_dimeo_order.collectData('price');

				// if ( !data.items ||  Object.keys(data.items).length == 0 ) {
				// 	return false;
				// }

				var publisherId = osp_dimeo_order.publisher.value;
				var deliveryType = osp_dimeo_order.deliveryMethod.get();
				var deliveryZone = osp_dimeo_order.deliveryZone.get();
				var country = deliveryZone == 4 ? 165 : 164;
				var subscriber = osp_dimeo_order.subscriberType.get();
				var edition = 1;
				
				var url = this.apiUrl + this.refreshUri;
				var selectedMags = osp_dimeo_order.subscribeForm.querySelectorAll('#selected-mags-block .selected-mag-item');
				for ( var i = 0; i < selectedMags.length; i++ ) {
					selectedMags[i].classList.add('loading');
				}
				
				this.ajaxRequest.send({
					url : url,
					method : 'post',
					data : data,
					done : function (response) {
                        osp_dimeo_order.editions.setData( response.items );
						osp_dimeo_order.setData( response );

						for ( var i = 0; i < selectedMags.length; i++ ) {
							selectedMags[i].classList.remove('loading');
						}

                        if(response.failed && response.failed == true) {
                            alert(response.errMessage);
                        }
					},
                    fail: function (a) {
                      console.log(a);
                    }
				});

			},


			
			// EVENT HANDLERS
			//handler for all animated elements. Hide(display) elements with animations effects
			onAnimationEnd : function onAnimationEnd( event ) {
				var animationList = {
					'fadeIn' : {type : 'in'},
					'flip' : {type : 'none', callback : function(element, animationName){return}},
					'flipInX' : {type : 'in', callback : function(element, animationName){return}},
					'slideInUp' : {type : 'in', callback : function(element, animationName){return}},
					'fadeInDown' : {type : 'in', callback : function(element, animationName){return}},
					'fadeInUp' : {type : 'in', callback : function(element, animationName){return}},
					'fadeOut' : {type : 'out'},
					'fadeOutDown' : {type : 'out', callback : function(element, animationName){return}},
					'fadeOutUp' : {type : 'out', callback : function(element, animationName){return}},
					'slideOutDown' : {type : 'out', callback : function(element, animationName){return}},
					'slideOutUp' : {type : 'out', callback : function(element, animationName){return}},
					'flipOutX' : {type : 'out', callback : function(element, animationName){return}},

					'fadeOutR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
					'fadeOutDownR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
					'fadeOutUpR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
					'slideOutDownR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
					'slideOutUpR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
					'flipOutXR' : {type : 'out', callback : function(element, animationName){element.remove();return;}},
				};
				var element = event.target;
				var classList = element.classList;
				var animationName = event.animationName;

				if (!animationList.hasOwnProperty(animationName)) 
					return;
				
				if ( animationList[animationName].type == 'in' ) {
					var display = element.tagName.toLowerCase() == 'li' ? 'list-item' : 'block';
					element.style.display = display; 
				} else if (animationList[animationName].type == 'out') {
					element.style.display = 'none'; 
				}
				element.classList.remove(animationName);
				
				//run call back for concrete animation name if any
				if ( animationList[animationName].hasOwnProperty('callback') && typeof animationList[animationName].callback == 'function' )
					animationList[animationName].callback(element, animationName);

			}, //END onAnimationEnd

			// handler of remove item click
			removeItem : function removeItem( event ) {
				var item = event.target.closest('.selected-mag-item');
				var eId = item.getAttribute('data-id');
				var avalableItemSelector = '#<?=$formWrapperId?> #<?=$formId?> #mags-list-block .avalable-mag-item[data-id="' + eId + '"]';
				var avalableItem = document.querySelector(avalableItemSelector);

				// var animationName = 'flipOutXR';
				var animationName = 'fadeOutUpR';
				if (item != null) {
					item[animationName]();
					if( item.getAllSiblings('[data-id="' + eId + '"]').length == 0  && avalableItem)
						avalableItem.classList.remove('is-selected');
				}
			}, //END removeItem

			addItem : function addItem( event ) {
				var item = event.target.closest('.avalable-mag-item');
				var pId = document.querySelector('#<?=$formWrapperId?> #<?=$formId?> #publisher_id').getAttribute('value');
				var eId = item.getAttribute('data-id');
				var magsListWrapperSelector = '#<?=$formWrapperId?> #<?=$formId?> #selected-mags-block ul';
				var items = document.querySelectorAll(magsListWrapperSelector + ' li.selected-mag-item');
				var index = (items.length > 0) ? parseInt( items[items.length - 1].getAttribute( 'data-index' ) ) + 1 : 1; 

				if ( items.length > 0 ) {
					for(var i = 0; i < items.length; i++) {
						if ( items[i].getAttribute('data-id') == eId ) {
							var agree = confirm('Такое издание уже присутствует в заказе. Вам действительно нужно еще?');
							if (!agree) {
								if ( items[i].scrollIntoView ) {
									items[i].scrollIntoView( {behavior: "smooth", block: "center", inline: "nearest"} );
								} 
								return;
							}
							break;
						}
					}
				}

				item.classList.add('loading');
				ospTools.ajaxRequest.send({
					method : 'post',
					url : '<?=$apiUrl?>/subscribe/add-mag-item',
					data : {pId : pId, eId : eId, index : index},
					done : function( response ) {
						if ( response.status == 'OK' ) {
							var newItemElem = ospTools.createElement(response.html);
							// var animationName = 'flipInX';
							var animationName = 'fadeIn';

							document.querySelector(magsListWrapperSelector).customAddElement(newItemElem, 'end');
							var justAddedItem = document.querySelector( magsListWrapperSelector + ' .selected-mag-item[data-index="' + index + '"]');
							justAddedItem[animationName]();
							// document.querySelector( magsListWrapperSelector + ' .selected-mag-item[data-index="' + index + '"]')[animationName]();
							item.classList.add('is-selected');
							if ( justAddedItem.scrollIntoView ) {
								justAddedItem.scrollIntoView( {behavior: "smooth", block: "center", inline: "nearest"} );
							} 
						} else {
							alert('что-то пошло не так. См. консоль');
							console.log(response.errMessage);
						}
						item.classList.remove('loading');
					},
					fail : function( response ) {
						alert('Что-то пошло не так! См. консоль.');
						item.classList.remove('loading');
					},
				});
			}, // END addItem

		}; // END build ospTools

		var Order = function ( formWrapperId ) {
			
			this.subscribeForm = false;
			this.deliveryMethod = false;
			this.deliveryZone = false;
			this.publisherId = false;
			this.subscriberType = false;
			this.promocode = false;
			this.user_agreement = false;
			this.editions = false;
			this.price_id = false;
			this.action_id = false;
			this.errMessages = {
				noMagSelected : '',
				requiredFieldEmpty : '',
				userAgreementUnchecked : '', 
			};

			var self = this;

			this.init = function( formWrapperId ) {
				try {
					var form = document.querySelector('#' + formWrapperId);
				} catch (e) {
					var form = false;
				}
				if ( form ) {
					this.subscribeForm = form; //ссылка на контейнер с формой
					
					this.deliveryMethod = form.querySelector('#delivery-method-wrapper');
					this.deliveryMethod.get = function() {
						return ospTools.getCheckedInput( this );
					}
					//повесить обработчик на изменение способа доставки
					this.deliveryMethod.querySelectorAll('input[type="radio"][name]').forEach( function( item, index, array ) {
						item.onchange = function( event ) {
							ospTools.refreshForm();
						}   
					});

					this.deliveryZone = form.querySelector('#delivery-zone-wrapper');
					this.deliveryZone.get = function() {
						return ospTools.getCheckedInput( this );
					}
					// повесить обработчик на изменение территории доставки
					this.deliveryZone.querySelectorAll('input[type="radio"][name]').forEach( function( item, index, array ) {
						item.onchange = function( event ) {
							ospTools.refreshForm();
						}   
					});

					this.publisher = form.querySelector('input[name="order[publisher_id]"]');

					this.subscriberType = form.querySelector('#subscriber-type-wrapper');
					this.subscriberType.get = function() {
						return ospTools.getCheckedInput( this );
					}
					// обработчик при изменении типа подписчика (физ лицо <-> юр.лицо)
					this.subscriberType.querySelectorAll('input[type="radio"][name]').forEach( function( item, index, array ) {
						item.onchange = function( event ) {
							
							var wrap = event.target.closest('#subscriber-type-wrapper');
							var checked = ospTools.getCheckedInput ( wrap );
							var companyBlock = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #company-data-block');
							if ( checked == 1 ) {
								//person. Hide Company
								companyBlock.fadeOutUp();
							} else if ( checked == 2 ) {
								//company. Show Company
								companyBlock.fadeInDown();
							}
							ospTools.refreshForm();
						}   
					});

					this.promocode = form.querySelector('#promo_code');
					this.promocode.onchange = function ( event ) {
						ospTools.refreshForm();
					}

					this.user_agreement = form.querySelector('input[type="checkbox"].user-agreement');

					this.price_id = form.querySelector('#price_id');
					this.action_id = form.querySelector('#action_id');

					//Издания
					this.editions = form.querySelectorAll('#selected-mags-block .selected-mag-item');
					this.editions.getData = function getData() {
						// вернуть объект, внутри которого объекты, описывающие выбранные издания с их текущими значениями
						var selector = '#<?=$formWrapperId;?> #<?=$formId;?> #selected-mags-block .selected-mag-item';
						var items = document.querySelectorAll( selector );
						var result = {};

						if ( items.length > 0 ) {
							for ( var i = 0; i < items.length; i++ ) {
								var index = items[i].getAttribute('data-index');
								if ( index ) {
									var discount = items[i].querySelector('[name*="discount"]');
									if ( discount )
										discount = parseFloat(discount.value.replace('%', ''));
									var item = {
										index 		: index,
										edition_id 	: items[i].querySelector('.magId').value,
										start_date 	: items[i].querySelector('[name*="date_start"]').value,
										duration 	: items[i].querySelector('[name*="duration"]').value,
										issuesCount	: items[i].querySelector('[name*="rqty"]').value,
										sum 		: items[i].querySelector('[name*="sum"]').value,
										price 		: items[i].querySelector('[name*="price"]').value,
										discount	: discount,
									};
									var isPdf = ospTools.getCheckedInput(items[i].querySelector('.type-block')) == 'pdf';
									isPdf = isPdf == true ? 1 : 0;
									item.is_pdf = isPdf;
									result[index] = item;
								}
							}
						}
						return result;
					};
					
					this.editions.setData = function setData( data ) {
						// получаем объект, внутри которого объекты с данными для выбранных изданий
						// нужно этими данными заменить в соответствующих DOM элементах, то что там есть на данный момент
						if (typeof data == 'undefined' || typeof data != 'object')
							return;

						var keys = Object.keys( data );
						if ( keys.length > 0 ) {
							var selector = '#<?=$formWrapperId;?> #<?=$formId;?> #selected-mags-block';
							var wrapper = document.querySelector(selector);

							for ( var k = 0; k < keys.length; k++ ) {
								var key = keys[k];
								var item = data[ key ]; // объект с данными. (Не элемент DOM!!)
								var index = item.hasOwnProperty('index') ? item.index : false;
								var elemItem = wrapper.querySelector('.selected-mag-item[data-index="' + index + '"]'); //соответствующий DOM элемент
								if ( elemItem ) {
									var issuesAmount = elemItem.querySelector('.issues-amount-block input[name*="rqty"]');
									if ( issuesAmount )
										issuesAmount.value = item.hasOwnProperty('issuesCount') ? item.issuesCount : 0;

									var sum = elemItem.querySelector('.cost-block input[name*="sum"]');
									if ( sum )
										sum.value = item.hasOwnProperty('sum') ? item.sum : 0;

									var price = elemItem.querySelector('.cost-block input[name*="price"]');
									if ( price )
										price.value = item.hasOwnProperty('price') ? item.price : 0;

									
									// для акции (корректного промокода)
									if ( item.hasOwnProperty('price') && item.hasOwnProperty('oldPrice')  ) {
										var oldPrice = elemItem.querySelector('.cost-block .with-note-under');
										if ( oldPrice ) {
											if ( item.price != item.oldPrice ) {
												//показать надпись про надичие скидки
												oldPrice.setAttribute('data-after','(с учетом скидки ' + item.discount + ')');
												oldPrice.classList.remove('note-under-hidden');
											} else {
												//спрятать надпись про наличие скидки
												oldPrice.classList.add('note-under-hidden');
											}
										}
										var discount = elemItem.querySelector('input[name*="discount"]');
										if ( discount ) 
											discount.value = item.discount;
									}
								}
							}
						}
					};

					this.editions.count = function() {
						return Object.keys( this.getData() ).length;
					}

					this.setData = function setCommonData( data ) {
						// получаем объект с данными, которые используем для заменты в DOM элементах
						// меняем сумму, сумму без скидки (если есть) инфу про акцию 
						var totalSum = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #total-sum');
						if ( totalSum )
							totalSum.textContent = data.hasOwnProperty('totalSum') ? data.totalSum : 0;
						
						var oldTotalSum = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #total-sum-old');
						if ( oldTotalSum ) {
							if( data.hasOwnProperty('totalSum') && data.hasOwnProperty('oldTotalSum') ) {
								oldTotalSum.textContent = data.oldTotalSum;
								var wrapper = oldTotalSum.closest('.animated')
								if ( data.totalSum !=  data.oldTotalSum ) {
									wrapper.flipInX();
								} else {
									wrapper.flipOutX();
								}
							} 
						}
						var priceId = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #price_id');
						if ( priceId && data.hasOwnProperty('price_id') )
							priceId.value = data.price_id;

						var actionInfo = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #action-info');
						var actionId = document.querySelector('#<?=$formWrapperId;?> #<?=$formId;?> #action_id');
						
						if ( data.hasOwnProperty('action') && data.promo_code != '' ) {
							actionInfo.innerHTML = data.action.actionTerms;
							actionId.value = data.action.id;
							if ( window.getComputedStyle(actionInfo).display == 'none' ) 
								actionInfo.flipInX();
						} else if( !data.hasOwnProperty('action') && data.promo_code != '' ) {
							actionId.value = 0;
							actionInfo.innerHTML = 'Введенный промокод НЕ соответствует ни одной действующей акции.';
							if ( window.getComputedStyle(actionInfo).display == 'none' ) 
								actionInfo.flipInX();	
						} else if ( data.promo_code == '' ) {
							actionInfo.innerHTML = '';
							actionId.value = 0;
							actionInfo.flipOutX();	
						}

						
					}


					// установить наблюдение за изменениеми в DOM дереве списка выбранных  изданий
					var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
					var magsListWrapper = form.querySelector('#selected-mags-block');

					var observer = new MutationObserver( function( mutations ) {  
					    mutations.forEach( function( mutation ) {
					    	if (mutation.type == 'childList' )
					    		ospTools.refreshForm(); // добавилось новое издание или удалено что-то - произвести перерасчет всей формы (на сервере)
					    } );
				    });
        			observer.observe( magsListWrapper, {attributes: false, childList: true, characterData: false, subtree : true} );

				}
			};

			this.collectData = function( goal ) {
				//goal is String which value can be 'price','full'... Means what data should be collected for
				// собрать данные о заказе из DOM.  В случае сбора в режиме 'price'  - сокращенный набор данных, достаточный для перерасчета формы.
				// В случае сбора в режиме 'full' нужно собрать все для отправки всей формы с целью создания заказа.

				var data = {};
				
				if (typeof goal == 'undefined' || ( goal != 'price' && goal != 'full' ) )
					goal = 'full';

				data.publisher_id = this.publisher.value;
				data.subscriber_type_id = this.subscriberType.get();
				data.delivery_type_id = this.deliveryMethod.get();
				data.delivery_zone_id = this.deliveryZone.get();
				data.country_id = data.delivery_zone_id == 4 ? 165 : 164;
				data.promo_code = this.promocode.value;
				data.user_agreement = this.user_agreement.checked ? 1 : 0;
				data.price_id = this.price_id.value;
				data.action_id = this.action_id.value;
				data.items = this.editions.getData();

				
				if ( goal == 'full' ) {
					//additionally add information about subscriber Person and company, if need
					data.personData = {};
					var subscriberFields = this.subscribeForm.querySelectorAll('.userData');
					for ( var k = 0; k < subscriberFields.length; k++ ) {
						data.personData[ subscriberFields[k].name ] = subscriberFields[k].value;
					}

					if ( this.subscriberType.get() == 2 ) { // TODO перепроверить действительно ли 2 соответствует юр. лицу
						data.companyData = {};
						var companyFields = this.subscribeForm.querySelectorAll('.companyData');

						for ( var k = 0; k < companyFields.length; k++ ) {
							data.companyData[ companyFields[k].name ] = companyFields[k].value;
						}

					}
					data.wrapperWidth = window.getComputedStyle( this.subscribeForm ).width.replace('px', '');
				} 
				return data;
			};
			this.validateElem = function ( inputElem, type ) {
				var validateData = {
					email : {
						pattern : /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
						message : 'Необходимо указать корректный Email адрес',
					},
					integer : {
						pattern : /^\d+$/,
						message : 'Поле должно содержать только цифры',
					},
					phone : {
						pattern : /^[\+]?[\d|\(|\)|\-| ]+$/,
						message : 'Допускается использование только цифр, пробелов и символов +, -, (, )',
					},
					string : {
						pattern : /[<|>|\{|\}]/,
						message : 'В поле присутствуют недопустимые символы',
						exclude : true,
					},
					empty : {
						pattern : /.+/,
						message : 'Поле обязательно к заполнению',
					},
				};

				if (typeof inputElem == 'undefined' || !inputElem)
					return true;

				var data = inputElem.value;
				
				var validateType = ( typeof type == 'string'  && validateData.hasOwnProperty( type ) )  ?  type :  inputElem.getAttribute('data-contentType');
				
				if ( !validateData.hasOwnProperty(validateType) )
					return true;

				if ( data == '' &&  validateType != 'empty' )
					var valid = true;
					// return true;

				var validateItem = validateData[validateType];
				
				if ( !valid && ( !validateItem.hasOwnProperty('exclude') || validateItem.exclude == false ) ) {
					var valid = validateItem.pattern.test( data );
				} else {
					var valid = data.match( validateItem.pattern );
					valid = !valid; 
				}

				try {
					var errWrapper = inputElem.closest('.user-data-field').querySelector('.error-message');
				} catch (e) {
					console.log(e);
					return valid;
				}
				
				if ( !errWrapper )
					return valid;

				var message = validateItem.message;
				if ( valid === false ) {
					if ( errWrapper.innerHTML.indexOf( message ) == -1) {
						errWrapper.innerHTML += message;
					}
					errWrapper.fadeIn();
				} else {
					if ( errWrapper.innerHTML.indexOf( message ) != -1) {
						errWrapper.innerHTML = errWrapper.innerHTML.replace(message, '');
					}
					if ( errWrapper.innerHTML  == '')
						errWrapper.fadeOut();
				}
				return valid;
			}
			this.preValidate = function () {
				var elemsToValidate = this.subscribeForm.querySelectorAll('#user-data-block input[required]:not(.companyData)');
				var validated = true;
				for ( var k = 0; k < elemsToValidate.length; k++ ) {
					var elemIsValid = this.validateElem(elemsToValidate[k], 'empty');
					if ( validated == true ) 
						validated = elemIsValid;
				}

				if ( this.subscriberType.get() == 2 ) {
					var elemsToValidate = this.subscribeForm.querySelectorAll('#user-data-block #company-data-block input[required]');
					var validated = true;
					for ( var k = 0; k < elemsToValidate.length; k++ ) {
						var elemIsValid = this.validateElem(elemsToValidate[k], 'empty');
						if ( validated == true ) 
							validated = elemIsValid;
					}
				}
				if ( validated == false ) {
					this.toggleErrMessage( 'someRequiredFieldsAreEmpty', 'show');
				} else {
					this.toggleErrMessage( 'someRequiredFieldsAreEmpty', 'hide');
				}
				
				elemsToValidate = this.subscribeForm.querySelectorAll('#user-data-block input[data-contenttype]:not(.companyData)');
				for ( var k = 0; k < elemsToValidate.length; k++ ) {
					var elemIsValid = this.validateElem(elemsToValidate[k]);
					if ( validated == true ) 
						validated = elemIsValid;
				}
				if ( this.subscriberType.get() == 2 ) {
					elemsToValidate = this.subscribeForm.querySelectorAll('#company-data-block input[data-contenttype]');
					for ( var k = 0; k < elemsToValidate.length; k++ ) {
						var elemIsValid = this.validateElem(elemsToValidate[k]);
						if ( validated == true ) 
							validated = elemIsValid;
					}
				}

				if ( this.user_agreement.checked == false ) {
					validated = false;
					this.toggleErrMessage( 'noUserAgreement', 'show' );
				} else {
					this.toggleErrMessage( 'noUserAgreement', 'hide' );
				}

				if ( this.editions.count() == 0 ) {
					validated = false;
					this.toggleErrMessage( 'noMagsSelected', 'show' );
				} else {
					this.toggleErrMessage( 'noMagsSelected', 'hide' );
				}
				return validated;
			}
			this.toggleErrMessage = function ( messageType, action ) {
				
				var messagesMap = {
					noUserAgreement : '<li class="err-message-item animated noUserAgreement" style="display:none">Для оформления заказа необходимо согласие на обработку персональных данных и с условиями пользовательского соглашения</li>',
					noMagsSelected : '<li class="err-message-item animated noMagsSelected" style="display:none">Необходимо выбрать хотя бы одно издание и указать параметры подписки</li>',
					someRequiredFieldsAreEmpty : '<li class="err-message-item animated someRequiredFieldsAreEmpty" style="display:none">Не все обязательные поля заполнены!!!</li>',
				}

				if ( typeof messageType == 'undefined' || !messagesMap.hasOwnProperty( messageType ) )
					return;
				var errPlace = this.subscribeForm.querySelector('#err-messages-list-block');
				if ( !errPlace )
					return;

				var isErrorsVisible = window.getComputedStyle( errPlace ).display == 'none' ? false : true;
				
				if ( typeof action == 'undefined' || ( action != 'show' && action != 'hide' ) ) {
					action = 'show';
				}

				var selector = '.err-message-item.' + messageType;
				var isAlreadyExists = errPlace.querySelector( selector );
				
				if ( action == 'show' ) {
					if ( isAlreadyExists ) {
						return;
					} else  {
						var errorsWrapper = errPlace.querySelector('.err-messages-list');
						if ( errorsWrapper ) {
							var newErrItem = ospTools.createElement( messagesMap[ messageType ] );
							errorsWrapper.customAddElement( newErrItem , 'end' );
							errorsWrapper.querySelector('.' + messageType).flipInX();
						}
					}
					if ( !isErrorsVisible )
						errPlace.fadeIn();

				} else if ( action == 'hide') {
					if ( isAlreadyExists ) {
						isAlreadyExists.flipOutXR();
						if ( errPlace.querySelectorAll('.err-message-item').length == 1) 
							errPlace.fadeOut();
					}
					if ( errPlace.querySelectorAll('.err-message-item').length == 0) 
						errPlace.fadeOut();
				}
			}
			this.submitForm = function submitForm( event ) {
				if ( !osp_dimeo_order ) 
					return false;

				if ( osp_dimeo_order.preValidate() == false ) 
					return false;

				var data = osp_dimeo_order.collectData( 'full' );
				var url = ospTools.apiUrl + ospTools.submitUri;
				ospTools.ajaxRequest.send({
					url : url,
					data : data,
					method : 'post',
					done : function ( response ) {
						if ( typeof response != 'object' || !response.hasOwnProperty('status') ) {
							alert ('При создании заказа что-то пошло не так. См. консоль');
							console.log( response );
							return;
						}
						if ( response.status == 'OK' ) {
							var formContainer = document.querySelector('#<?=$formWrapperId?>');
							var newContents = ospTools.createElement( response.html );

							formContainer.innerHTML = '';
							formContainer.customAddElement( newContents );
							// delete ospTools;
							// delete osp_dimeo_order;
						} else if (response.status == 'invalidData') {
							alert('Заказ не был создан в связи с тем, что присланные данные не соответствуют требованиям.');
							console.log(response.errors);
							return;
						} else {
							alert('При создании заказа возникли ошибки. См. консоль.');
							console.log(response);
							return;
						}
					},
					fail : function ( response ) {
						alert ('что-то пошло не так. См. консоль');
						console.log( response );
					}
				});
			}

			this.init( formWrapperId );
		}
		
		
		// ATTACH ALL HANDLERS
		//attach all kinds of animationend handlers for animated elements (they does not exist right now)
		ospTools.addLiveEvent('#<?=$formWrapperId?> .animated', 'webkitAnimationEnd', ospTools.onAnimationEnd );
		ospTools.addLiveEvent('#<?=$formWrapperId?> .animated', 'mozAnimationEnd', ospTools.onAnimationEnd );
		ospTools.addLiveEvent('#<?=$formWrapperId?> .animated', 'MSAnimationEnd', ospTools.onAnimationEnd );
		ospTools.addLiveEvent('#<?=$formWrapperId?> .animated', 'onanimationend', ospTools.onAnimationEnd );
		ospTools.addLiveEvent('#<?=$formWrapperId?> .animated', 'animationend', ospTools.onAnimationEnd );
		
		// attach handler for click event on avalable magazine item
		ospTools.addLiveEvent('#<?=$formWrapperId; ?> .avalable-mag-item', 'click', ospTools.addItem );
		
		// attach handler for click event on remove selected magazine item
		ospTools.addLiveEvent('#<?=$formWrapperId; ?> #<?=$formId; ?> #selected-mags-block .remove-item-block', 'click', ospTools.removeItem );

		// attach handler for click event on up arrow icon to scroll up to avalable magazines block
		ospTools.addLiveEvent('#<?=$formWrapperId?> #<?=$formId?> #selected-mags-block .selected-mag-item .go-to-avalabeles', 'click', 
			function( event ) {
				var target = document.querySelector('#<?=$formWrapperId?> #<?=$formId?> #mags-list-block');
				if ( target && target.scrollIntoView ) {
					target.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
				}
		});

		ospTools.addLiveEvent('#<?=$formWrapperId?> #<?=$formId?> #selected-mags-block select[name*="duration"]', 'change', function( event ) {
			ospTools.refreshForm();
		});

		ospTools.addLiveEvent('#<?=$formWrapperId?> #<?=$formId?> #selected-mags-block .type-block input[type="radio"]', 'change', function( event ) {
			ospTools.refreshForm();
		});

		ospTools.addLiveEvent('#<?=$formWrapperId?> #<?=$formId?> #selected-mags-block .start-date-block [name*="date_start"]', 'change', function( event ) {
			ospTools.refreshForm();
		});

		ospTools.addLiveEvent( '#<?=$formWrapperId?> #<?=$formId?> #user-data-block input[data-contenttype]', 'change', function( event ) {
			var input = event.target;
			
			if ( input.getAttribute('required') != null )
				osp_dimeo_order.validateElem( input, 'empty');

			osp_dimeo_order.validateElem( input );
		})


		//get form
		var params = {
			url : ospTools.apiUrl + '/subscribe/get-common-form',
			method : 'post',
			data : {
				width : '<?=$width?>',
				showCovers : '<?=$showCovers?>',
				pId: '<?=$publisherId?>',
				editionsList : '<?=$editions?>',
			},
			done : function(response) {
				if (response.status == 'OK') {
					var place = document.getElementById('<?=$formWrapperId?>');
					if (place != null) {
						place.innerHTML = response.html;
						osp_dimeo_order = new Order('<?=$formWrapperId;?>');
						var submitBtn = osp_dimeo_order.subscribeForm.querySelector('#send-subscribe-form-button');
						if ( submitBtn )
							ospTools.addEvent(submitBtn, 'click', osp_dimeo_order.submitForm);
					}
				} else {
					alert('Sory... but subscribe form could not be load. Reason: ' + response.errMessage);
					console.log(response.errMessage);
				}
			}
		};
		ospTools.ajaxRequest.send(params);
}

