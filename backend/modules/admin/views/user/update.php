<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Редактирование пользователя "' . $model->Login . '"';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи(список)', 'url' => ['user/index']];
// $this->params['breadcrumbs'][] = ['label' => $model->FullName, 'url' => ['view', 'id' => $model->User_ID]];
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
