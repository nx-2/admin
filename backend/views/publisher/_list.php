<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="publisher-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый издатель', '/publisher/create',
            [
                'class' => 'btn btn-success',
                // 'data-url' => '/publisher/create',
            ]
        ); ?>
    </p>
    <?php Pjax::begin(['id' => 'publishers-list', 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'description:ntext',
            'audience',
            [
                'attribute' => 'company',
                'value' => 'company.name',
            ],
            // 'company_id',
            // 'contact_person_id',
            [
                'attribute' => 'contactPerson',
                'value' => 'person.name',
            ],
            // 'logo',
            [
                'attribute' => 'logo',
                'content' => function ($model, $key, $index, $column) {
                    if (empty($model->logo)) {
                        return Html::tag('span', '--', ['style' => 'text-align:center;display:inline-block;width:100%']);
                    } else {
                        $img = Html::img($model->logo, ['width' => '100']);
                        return Html::tag('span', $img, ['style' => 'display: inline-block;width: 100%;text-align: center;']);
                    }
                },
                'filter' => false,
            ],

            // 'distribution',
            'official_email:email',
            // 'created',
            // 'last_updated',
            // 'enabled',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'enabled',
                'content' => function ($model, $key, $index, $column) {
                    $checkbox = Html::checkbox('', (Boolean)$model->enabled, [
                        'id' => 'enabled_' . $model->id,
                        'value' => $model->enabled,
                        // 'disabled' => true,
                        'onclick' => 'enabledClicked(event, "Publisher")',
                    ]);
                    $label = Html::label('', 'enabled_' . $model->id, ['class' => 'radio-check-label'], '');
                    $ajaxLoader = Html::tag('div', '', ['class' => 'ajax-load']);
                    $content = $ajaxLoader . $checkbox . $label;
                    $result = Html::tag('div', $content, ['class' => 'radio-check-wrap']);

                    return $result;
                },
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'filter' => ['1' => 'вкл', '0' => 'выкл'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = '/publisher/update?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать издателя',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            '/publisher/delete?id=' . $key,
                            [
                                'title' => 'Удалить издателя',
                                'aria-label' => 'Удалить',
                                'data-confirm' => 'Вы уверены, что хотите удалить этого издателя?',
                                'data-method' => 'post',
                                'data-pjax' => '#publishers-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],


            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
