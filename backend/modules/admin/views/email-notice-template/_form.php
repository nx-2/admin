<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\views\widgets\ace_code_editor\AceCodeEditor;

/* @var $this yii\web\View */
/* @var $model common\models\EmailNoticeTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['onsubmit' => 'beforeSubmitTemplate(event)'] ] ); ?>
<div id='email-notice-template-form-wrapper' class='box box-primary email-notice-template-form'>
    <div class='box-header with-border'>
        <h3 class='box-title'>
            Шаблон Email уведомления
        </h3>
    </div>
    <div class='box-body'>
        <div class="row">
            
            <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                <?php 
//                    echo (Html::activeDropDownList($model, 'notice_id', yii\helpers\ArrayHelper::map( common\models\EmailNotice::find()->all() , 'id', 'name' ) ));
                    echo($form->field($model, 'notice_id')->dropDownList(yii\helpers\ArrayHelper::map( common\models\EmailNotice::find()->all() , 'id', 'name' ), []) );
                ?>
                <?php //echo($form->field($model, 'notice_id')->textInput()); ?>
            </div>
            <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                <?php 
//                    echo($form->field($model, 'is_default')->textInput()); 
                    echo($form->field($model, 'is_default')->checkbox([ 'style' => 'margin-right:5px;width:15px;height:15px;vertical-align:middle;cursor:pointer', 'labelOptions' => ['style' => 'padding-top: 28px;cursor:pointer']])); 
                ?>
            </div>
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <!-- 
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
            </div>
            -->
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px;position: relative' >
                <button  type='button' onclick='testEmailNoticeTemplate(event)' id='test-email-template-button' class='btn btn-primary' style='position: absolute;top:0px;right: 25px;'>
                    Тестировать шаблон
                </button>
                <?php echo $form->field($model, 'template_body')->hiddenInput([]); ?>
                <?php 
                    echo(AceCodeEditor::widget(['content' => $model->template_body, 'codeWrapperOptions' => ['id' => 'email-notice-template-body']]));
                ?>
            </div>
        </div>
    </div>
    <div class='box-footer'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
            <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php 
            if ( !$model->isNewRecord  ) {
                echo (Html::a('Удалить', Url::to(['email-notice-template/delete', 'id' => $model->id]), ['class' => 'btn btn-danger pull-right', 'onclick' => 'if(!confirm("Вы действительно хотите удалить этот шаблон?")) {return false;}']));
            }
            ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div id='email-template-modal-wrapper'></div>
<script type='text/javascript'>
    function testEmailNoticeTemplate(event){
        var noticeId = $('#emailnoticetemplate-notice_id').val();
        if ( typeof noticeId == 'undefined' || noticeId == 0 || !noticeId ) {
            alert('необходимо выбрать тип уведомления');
            return;
        }
        var editor = getAceCodeEditor('#email-notice-template-body');
        var testHtml = editor != false ? editor.getValue() : false;
        $.ajax({
            url : '<?php echo( Url::to(['email-notice-template/get-template-html']) ); ?>' + '/0', // + $(event.target).data('id'),
            type : 'post',
            dataType : 'json',
            data : {notice_id : noticeId, testHtml : testHtml, testTemplate : 1 }
        })
        .done(function(response) {
            if (response.status == 'OK') {
                $('#email-template-modal-wrapper').html(response.html);
                var modal = $('#email-template-modal-wrapper #email-notice-template-modal');
                if (modal.length > 0) {
                    modal.on('shown.bs.modal', function (event) {
                        setTimeout(setIframeSizes, 500);
                    })
                    modal.modal('show');
                }
            } else {
                if ( response.hasOwnProperty('errMessage') ) {
                    alert( response.errMessage );
                }
            }
        })
        .fail(function(a,b,c) {
            alert('Что-то пошло не так. См. консоль');
            console.log(a);
            console.log(b);
            console.log(c);
        });
    }
    function beforeSubmitTemplate(event){
        var editor = getAceCodeEditor('#email-notice-template-body');
        if ( editor != false ) {
            $('#emailnoticetemplate-template_body').val( editor.getValue() );
        }
    }
    function setIframeSizes( w, h ){
        w = !w ? 700 : w;
        h = !h ? false : h;
        
        var iframe = $('#popupTemplateIframe');
        iframe.width(w + 'px');
        if ( h == false ) {
            var iframeBody = iframe.contents().find('body');
            h = iframeBody.height() + 20;
        }
        iframe.height(h + 'px');
    }
    
</script>
