<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\event_handlers;

use common\models\EmailNotice;
use common\models\events\CustomIssueEvent;
use common\models\Issue;
use common\models\IssueDownloadFile;
use common\models\NxOrder;
use common\queue_jobs\SendPdfIssueJob;
use common\services\PdfLogService;
use Exception;
use Yii;
use yii\base\BaseObject;
use yii\base\Event;
use yii\queue\Queue;


class IssueEventHandler extends BaseObject
{
    public static function onNotShippedPdfFound($event)
    {
        // TODO: Разослать всем, кому нужно письма с указанием ссылки на скачивание этого выпуска
        // 1.Сгенерить ссылки на скачивание
        // 2. Добавить в очередь задания на отправку писем
        // 3. По факту отправки пометить отгруженность
        /** @var Issue $issue */
        $issue = $event->issue;
        if (empty($issue))
            return;

        $notShippedOrders = $issue->notShippedOrders;

        // TODO: notShippedOrders теоретически может содержать неприемлемо много заказов, что приведет к перерасходу памяти
        // нужно переделать.
        foreach ($notShippedOrders as $order) {
            try {
                self::sendPdfForOrder($order, $issue, $event->name);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    public static function onSendNotShippedRequested(CustomIssueEvent $event): void
    {
        $issue = $event->issue;
        if (empty($issue)) {
            return;
        }
        $orderIdsToSend = $issue->getNotShippedOrderIds();

        if (empty($orderIdsToSend)) {
            return;
        }
        $typeOfNotice = $event->data['typeOfNotice'] ?? 'foundNotShippedPDF';
        $chunkSize = 50;
        $orderIdsChunks = array_chunk($orderIdsToSend, $chunkSize);
        foreach ($orderIdsChunks as $idsPart) {
            $orders = NxOrder::find()->where(['IN', 'id', $idsPart])->all();
            foreach ($orders as $order) {
                try {
                    self::sendPdfForOrder($order, $issue, $typeOfNotice);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
    }

    public static function onIssueHasReleased (Event $event): void
    {
        /** @var Issue $issue */
        $issue = $event->sender;
        $pdfLogService = new PdfLogService();
        $pdfLogService->updateLogsForIssue(intval($issue->Message_ID));
    }

    /**
     * @param NxOrder $order
     * @param Issue $issue
     * @param string $typeOfNotice
     * @return bool
     * @throws Exception
     */
    private static function sendPdfForOrder(NxOrder $order, Issue $issue, string $typeOfNotice): bool
    {
        IssueDownloadFile::createLink($order->id, $issue->Message_ID);

        $publisher = $order->publisher;
        $emailNoticeType = EmailNotice::findOne(['key_string' => $typeOfNotice]);

        if (empty($emailNoticeType)) {
            throw new yii\base\ErrorException('Email notice type <' . $typeOfNotice . '>  and publisher <' . $publisher->id . '> does not exist');
        }

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();
        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }

        if (empty($emailNoticeTemplate)) {
            throw new yii\base\ErrorException('Email notice template for <' . $emailNoticeType->name . '>  and publisher <' . $publisher->id . '> does not exist');
        }

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $paymentId = !empty($order->payment) ? $order->payment->id : NULL;

        $data = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
                'publisher_id' => $publisher->id,
                'order_id' => $order->id,
                'issue_id' => $issue->Message_ID,
                'payment_id' => $paymentId,
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $order->email,
        ];

        $job = new SendPdfIssueJob(intval($publisher->id), $data);
        if ($job->validate()) {
            /** @var Queue $queue */
            $queue = Yii::$app->queue;
//            $jobId = $queue->push($job);
                $job->execute($queue);
        } else {
            $errAttrs = array_keys($job->getErrors());
            $errMessage = 'incorrect data was passed to EmailNotificationJob model. There are errors in attributes: ' . implode(',', $errAttrs);
            throw new Exception($errMessage);
        }
        return $jobId !== NULL;
    }
}