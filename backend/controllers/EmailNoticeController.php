<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use common\models\EmailNotice;
use backend\models\EmailNoticeSearch;
use backend\controllers\SiteController;
use yii\web\NotFoundHttpException;
use \common\services\EmailNoticeTestDataProvider;
use yii\filters\VerbFilter;
use backend\models\EmailLogSearch;
use common\models\EmaiLog;

/**
 * EmailNoticeController implements the CRUD actions for EmailNotice model.
 */
class EmailNoticeController extends SiteController
{
    public $dataProvider;
    
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->dataProvider = new EmailNoticeTestDataProvider();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                parent::behaviors()['access'],
                [
                    'rules' => [
                        [
                            'actions' => ['log', 'email-log','get-email-message','get-template-html', 'get-template'],
                            'allow' => true,
                            'roles' => ['subscribe_manager'],
                        ],
                        [
                            'actions' => ['index', 'resend-logged-email', 'update'],
                            'allow' => true,
                            'roles' => ['publisher'],
                        ],
                    ],
                ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailNotice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailNoticeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionLog()
    {
        $searchModel = new  EmailLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('log-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionEmailLog($emailId = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (null == $emailId) {
            return ['status' => 'err', 'message' => 'email id is not supplied.'];
        }
        return [
            'status' => 'OK',
//            'html' => $email->body,
            'html' => $this->renderPartial('_email-message-modal', ['emailId' => $emailId]), // модальное окно bootstrap
        ];
    }
    public function actionGetEmailMessage($emailId = null)
    {
        if (null == $emailId) {
            return ['status' => 'err', 'message' => 'email id is not supplied.'];
        }
        $email = EmaiLog::findOne(['id' => intval($emailId)]);

        if (null == $email) {
            throw new NotFoundHttpException('The requested email message not found.');
        }
        return $this->renderPartial('_email-message-content', ['message' => $email]);
    }
    public function actionResendLoggedEmail($emailId = null, $recipient = 'currentUser')
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $recipient = ($recipient == 'currentUser' || $recipient == 'same') ? $recipient : 'currentUser';

        if (null == $emailId)
            return ['status' => 'err', 'message' => 'emailId was not supplied'];

        $loggedEmail = EmaiLog::findOne(['id' => intval($emailId)]);
        if (null == $loggedEmail)
            return ['status' => 'err', 'message' => 'requested message was not found'];
        $sent = $loggedEmail->resendImmediately($recipient);
        if ($sent) {
            return ['status' => 'OK'];
        } else {
            return ['status' => 'err', 'message' => 'При попытке отправить это письмо произошла ошибка. Письмо НЕ было отправлено.'];
        }
    }
    public function actionGetTemplateHtml( $templateId = false ){
        // возвращает результат рендеринга запрошенного шаблона (с тестовыми данными)
                
        Yii::$app->response->format = 'json';
        $toReturn = [];

        if ( !$templateId ) {
            $toReturn['status'] = 'err';
            $toReturn['errMessage'] = 'Отсутствует ИД шаблона';
            return $toReturn;
        }
        
        $data = ['templateId' => $templateId];
        
        $toReturn = [
            'status' => 'OK',
            'html' => $this->renderPartial('_email-template-content', $data), // модальное окно bootstrap 
        ];
        return $toReturn;
    }
    
    public function actionGetTemplate( $templateId=false ) 
    {
        if ( !$templateId ) {
            throw new \yii\web\NotFoundHttpException();
        }
        $this->layout = '@common/mail/layouts/html';
        $template = \common\models\EmailNoticeTemplate::findOne(['id' => $templateId]);

        if ( null == $template ) {
            throw new \yii\web\NotFoundHttpException();
        }

        try {
            $templateView = Yii::getAlias($template->template_file, true) . '.php';

        } catch (Exception $ex) {
            throw new \yii\web\NotFoundHttpException();
        }
        if ( !file_exists($templateView) ) {
            throw new \yii\web\NotFoundHttpException();
        }
        $noticeKeyString = $template->notice->key_string;
        $data = $this->dataProvider->getDataFor( $noticeKeyString );
        return $this->render($template->template_file, $data);
    }
    /**
     * Updates an existing EmailNotice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $publisher = Yii::$app->user->identity->publisher;
        $relatedPublisherTemplate = \common\models\PublisherNoticeTemplate::findOne(['notice_id' => $model->id, 'publisher_id' => $publisher->id]);
        
        if ( Yii::$app->request->isPost ) {
            $data = Yii::$app->request->post('PublisherNoticeTemplate');
            if ($relatedPublisherTemplate == null) {
                $relatedPublisherTemplate = new \common\models\PublisherNoticeTemplate();
                $data['publisher_id'] = $publisher->id;
                $data['notice_id'] = $model->id;
            } 
            if ( $relatedPublisherTemplate->load($data, '') && $relatedPublisherTemplate->save() ) {
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            } else {
                Yii::$app->session->addFlash('danger', 'Не сохранено!!.');
            }
        }
        return $this->render('update', [
            'model' => $model,
            'publisher' => $publisher,
            'relatedPublisherTemplate' => $relatedPublisherTemplate,
        ]);
    }
    /**
     * Finds the EmailNotice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailNotice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailNotice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
