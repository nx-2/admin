<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\NxPerson */

$this->title = 'Редактирование адреса';
$this->params['breadcrumbs'][] = ['label' => 'Адреса', 'url' => ['address/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li> <a href="<?=Url::to(['address/index']); ?>" data-toggle_="tab" aria-expanded="true">Список</a></li>
        <li ><a href="<?=Url::to(['address/create']); ?>" data-toggle_="tab" aria-expanded="false">Создание адреса</a></li>

        <li class="pull-left header">
            <i class="fa fa-th"></i>
            <h1 style='display:inline-block;'>
                <?= Html::encode($this->title); ?>
            </h1>
        </li>
    </ul>


    <div class="nx-person-update">


    <?php 
    	$data = ['model' => $model, 'formName' => $formName];
    	if (isset($showMessage))
    		$data['showMessage'] = $showMessage;
    ?>
    <?= $this->render('_form', $data); ?>

</div>
</div>