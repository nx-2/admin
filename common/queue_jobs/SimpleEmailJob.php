<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\queue_jobs;

use common\custom_components\CustomHelpers;
use common\models\Edition;
use common\models\EmaiLog;
use common\models\Issue;
use common\models\NxOrder;
use common\models\OrderItemRelease;
use common\models\Payment;
use common\models\Publisher;
use common\models\Shipment;
use common\models\ShipmentItem;
use common\models\User;
use Exception;
use Yii;
use yii\base\Model;
use yii\mail\MessageInterface;
use yii\queue\JobInterface;

/**
 * @property string $templateName
 * @property array $data
 * @property string $subject
 * @property string $fromEmail
 * @property string $toEmail
 * @property int $publisherId
 */

class SimpleEmailJob extends Model implements JobInterface
{
    public $data;
    public $subject;
    public $fromEmail;
    public $toEmail;
    public $publisherId;
    public $userId;
    public $body;

    public function __construct(int $publisherId, $config = [])
    {
        parent::__construct($config);
        $this->publisherId = $publisherId;
    }

    public function rules()
    {
        return [
            [['subject', 'fromEmail', 'toEmail', 'publisherId', 'body', 'userId'], 'required'],
            [['subject', 'body'], 'string'],
            [['fromEmail', 'toEmail'], 'email'],
            [['publisherId','userId'], 'integer'],
            ['data', 'safe'],
        ];

    }

    public function execute($queue)
    {

        try {
            CustomHelpers::configureMailerComponent($this->publisherId);
            $mailer = Yii::$app->getMailer();
            $message = $mailer->compose();

            $message->setFrom($this->fromEmail)
                ->setTo($this->toEmail)
                ->setSubject($this->subject)
                ->setHtmlBody($this->body)
                ->setTextBody(strip_tags($this->body));

            if ($message->send()) {
                $this->logEmail($message, $this->body, true, $this->subject);
            } else {
                $this->logEmail($message, $this->body, false, 'НЕ ОТПАРВЛЕНО: ' . $this->subject);
            }
        } catch (Exception $ex) {
            $this->logEmail($ex, $this->body, false, 'Exception при попытке отправить Email');
            return;
        }
    }

    /**
     *
     * @param MIX \yii\swiftmailer\Message | \Exception $emailMessage
     * @param bool $success
     * @param string $messageBody
     * @param string $type
     * @return void
     *      */
    public function logEmail($emailMessage = null, $messageBody = '', $success = true, $type = '')
    {
        if ($emailMessage == null || (!($emailMessage instanceof MessageInterface) && !($emailMessage instanceof Exception))) {
            return;
        }
        $logItem = new EmaiLog();


        if ($emailMessage instanceof Exception) {
            $logItem->from = !empty($this->fromEmail) ? $this->fromEmail : '';
            $logItem->to = !empty($this->toEmail) ? $this->toEmail : '';
            $logItem->success = intval($success);
            $logItem->subject = !empty($this->subject) ? $this->subject : '';
            $logItem->body = '<pre>' . $emailMessage->__toString() . '</pre>';
            $logItem->type = $type;
//            $logItem->publisher_id = intval($this->tryToGetPublisherId());
            $logItem->publisher_id = intval($this->publisherId);
        } else {
            $logItem->from = $this->fromEmail;
            $logItem->to = implode(',', $emailMessage->getTo());
            $logItem->to = $this->toEmail;
            $logItem->success = intval($success);
            $logItem->subject = $emailMessage->getSubject();
            $logItem->body = $messageBody;
            $logItem->type = $type;
//            $logItem->publisher_id = intval($this->tryToGetPublisherId());
            $logItem->publisher_id = intval($this->publisherId);
        }

        $additionalKeysList = ['order_id', 'issue_id', 'payment_id', 'shipment_id', 'shipment_item_id', 'user_id', 'item_release_id', 'edition_id'];
        foreach ($additionalKeysList as $keyName) {
            if (isset($this->data[$keyName]) && !empty($this->data[$keyName])) {
                $logItem->{$keyName} = intval($this->data[$keyName]);
            }
        }
        if (!$logItem->save()) {
            print_r($logItem->getErrors());
        }
    }

}