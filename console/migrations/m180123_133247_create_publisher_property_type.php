<?php

use yii\db\Migration;

/**
 * Class m180123_133247_create_publisher_property_type
 */
class m180123_133247_create_publisher_property_type extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $sqlCode="
            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
            CREATE TABLE IF NOT EXISTS `publisher_property_type` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `group_id` INT(11) NOT NULL,
              `name` VARCHAR(70) NOT NULL,
              `type` ENUM('STRING', 'INTEGER', 'TEXT') NULL DEFAULT 'STRING',
              PRIMARY KEY (`id`),
              UNIQUE INDEX `group_name` (`group_id` ASC, `name` ASC))
            ENGINE = InnoDB
            DEFAULT CHARACTER SET = utf8;
            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;        
        ";
        $this->execute($sqlCode);
    }

    public function down()
    {
        echo "m180123_133247_create_publisher_property_type cannot be reverted.\n";

        return false;
    }

}
