<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\helpers\Url;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    public $publisher;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_ID', 'Checked', 'Confirmed', 'country', 'company_profile', 'sex', 'region', 'open_email', 'publisher_id'], 'integer'],
            [['Password', 'Language', 'Created', 'LastUpdated', 'Email', 'Login', 'FullName', 'LastName', 'FirstName', 'MidName', 'RegistrationCode', 'Auth_Hash', 'company', 'job', 'phone', 'city', 'address', 'street', 'house', 'building', 'flat', 'url', 'birthday', 'phone_prefix', 'fax_prefix', 'fax', 'district', 'index_num', 'avatar', 'UserType', 'Text', 'LastLoginDate', 'publisher'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['publisher']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'User_ID',
                    'Checked',
                    'Email',
                    'Login',
                    'FirstName',
                    'LastName',
                    'MidName',
                    'phone',
                    'address',
                    'publisher' => [
                        'asc' => ['publisher.name' => SORT_ASC],
                        'desc' => ['publisher.name' => SORT_DESC],
                    ],
                ],
                'route' => Url::to(['user/index']),
            ],
            'pagination' => [
                'pageSize' => 30,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'User_ID' => $this->User_ID,
            'Checked' => $this->Checked,
            'Created' => $this->Created,
            'LastUpdated' => $this->LastUpdated,
            'Confirmed' => $this->Confirmed,
            'country' => $this->country,
            'company_profile' => $this->company_profile,
            'sex' => $this->sex,
            'birthday' => $this->birthday,
            'region' => $this->region,
            'open_email' => $this->open_email,
            'LastLoginDate' => $this->LastLoginDate,
        ]);

        $query->andFilterWhere(['like', 'Password', $this->Password])
            ->andFilterWhere(['like', 'Language', $this->Language])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Login', $this->Login])
            ->andFilterWhere(['like', 'FullName', $this->FullName])
            ->andFilterWhere(['like', 'LastName', $this->LastName])
            ->andFilterWhere(['like', 'FirstName', $this->FirstName])
            ->andFilterWhere(['like', 'MidName', $this->MidName])
            ->andFilterWhere(['like', 'RegistrationCode', $this->RegistrationCode])
            ->andFilterWhere(['like', 'Auth_Hash', $this->Auth_Hash])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'job', $this->job])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'building', $this->building])
            ->andFilterWhere(['like', 'flat', $this->flat])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'phone_prefix', $this->phone_prefix])
            ->andFilterWhere(['like', 'fax_prefix', $this->fax_prefix])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'index_num', $this->index_num])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'UserType', $this->UserType])
            ->andFilterWhere(['like', 'publisher.name', $this->publisher])
            ->andFilterWhere(['like', 'Text', $this->Text]);

        return $dataProvider;
    }
}
