<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>


    <?php // echo($form->field($model, 'User_ID')->textInput()); ?>
    <?php // echo($form->field($model, 'Subdivision_ID')->textInput()); ?>
    <?php // echo($form->field($model, 'Sub_Class_ID')->textInput()); ?>
    <?php // echo($form->field($model, 'Priority')->textInput()); ?>
    <?php // echo($form->field($model, 'Keyword')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'TimeToDelete')->textInput()); ?>
    <?php // echo($form->field($model, 'TimeToUncheck')->textInput()); ?>
    <?php // echo($form->field($model, 'IP')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'UserAgent')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'Parent_Message_ID')->textInput()); ?>
    <?php // echo($form->field($model, 'IssueID')->textInput()); ?>
    <?php // echo($form->field($model, 'available_online')->textInput()); ?>
    <?php // echo($form->field($model, 'no_full')->textInput()); ?>
    <?php // echo($form->field($model, 'oldID')->textInput()); ?>
    <?php // echo($form->field($model, 'inCompositionDate')->textInput()); ?>
    <?php // echo($form->field($model, 'showDescription')->textInput()); ?>
    <?php // echo($form->field($model, 'ncTitle')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'ncKeywords')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'ncDescription')->textarea(['rows' => 6])); ?>
    <?php // echo($form->field($model, 'end_number')->textInput()); ?>
    <?php // echo($form->field($model, 'subscription_code')->textInput(['maxlength' => true])); ?>
    <?php // echo($form->field($model, 'xpress_id')->textInput()); ?>
    

<?php

// use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\create_update_fields\CreateUpdateFields;
use backend\views\widgets\image_field\ImageField;
use yii\jui\AutoComplete;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Issue */
/* @var $form yii\widgets\ActiveForm */
?>
<div id='issue-form-wrapper'>
    <?php
    $action = $model->isNewRecord ?   Url::toRoute('issue/create') : Url::toRoute(['issue/update', 'id' => $model->Message_ID]);
    $form = ActiveForm::begin(['options' => [ 'enctype' => 'multipart/form-data'] ]);
    $form->action = $action;
    ?>
    <?php $form->errorSummaryCssClass = 'alert alert-warning alert-dismissible'; ?>
    <div class='box box-primary issue-form'>
        <div class='box-header with-border'>
            <?php echo('Выпуск: ' . $model->Name . '('. $model->issue_title) . ')'; ?>
        </div>
        <div class='box-body'>
            <div>
                <?=$form->errorSummary($model,['header' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>']); ?>
            </div>                
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#issue-general" data-toggle="tab" aria-expanded="false">Данные</a></li>
                    <li><a href="#issue-pdf" data-toggle="tab" aria-expanded="true">Файлы</a></li>
                    <li><a href="#issue-info" data-toggle="tab" aria-expanded="true">Инфо</a></li>
                    <?php if($model->isPdfLoaded()): ?>
                        <li><a href="#send-issue-tab" data-toggle="tab" aria-expanded="true">Рассылка PDF</a></li>
                    <?php endif; ?>
<!--
                    <li><a href="#user-edit-roles" data-toggle="tab" aria-expanded="true">Роли</a></li>
                    <li><a href="#user-edit-access" data-toggle="tab" aria-expanded="true">Доступ</a></li>
 -->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="issue-general">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>

                                            <?php 
                                            $pId = Yii::$app->user->identity->publisher_id;
                                            $publisher = \common\models\Publisher::findOne($pId);
                                            $allMyEditions = $publisher->myEditions;
                                            $allMyEditions = \yii\helpers\ArrayHelper::map($allMyEditions,'Message_ID','HumanizedName');
                                            $dropDownoptions = ['prompt' => 'Выберете издание...'];
                                            if (!$model->isNewRecord) {
                                                $dropDownoptions ['disabled'] = true;
                                            }
                                            echo($form->field($model, 'MagazineID')->dropdownList($allMyEditions, $dropDownoptions));
                                            ?>
                                            <?php if($model->magazine != null):?>
                                            <script>var issueNameMask = '<?=$model->magazine->issue_mask;?>';</script>
                                            <?php endif;?>
                                            
                                            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

                                            <?php 
                                                $items = [1 => 'Готовится' , 2 => 'Выпущен' , 3 => 'Отменен'];
                                                $options = [];
                                                echo($form->field($model, 'status_id')->dropdownList($items, $options)); 
                                            ?>
                                            
                                            <?php 
                                                echo $form->field($model, 'PublicDate')->widget(DatePicker::classname(), [
                                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                                    'layout' => '{picker}{input}{remove}',
                                                    // 'convertFormat' => true,
                                                    'options' => [
                                                        'placeholder' => 'Укажите дату выхода ...',
                                                        'value' => $model->PublicDate !== null ?  date('d.m.Y',strtotime($model->PublicDate)) : '',
                                                    ],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true
                                                    ]
                                                ]);
                                            ?>

                                            <?php 
                                                echo $form->field($model, 'StartDate')->widget(DatePicker::classname(), [
                                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND, //TYPE_INPUT, TYPE_COMPONENT_PREPEND, TYPE_COMPONENT_APPEND, TYPE_RANGE, TYPE_INLINE, TYPE_BUTTON
                                                    // 'size' => 'sm',
                                                    // 'removeButton' => false,
                                                    'layout' => '{picker}{input}{remove}',
                                                    'options' => [
                                                        'placeholder' => 'Укажите дату рассылки ПДФ ...',
                                                        'value' => $model->StartDate !== null ?  date('d.m.Y',strtotime($model->StartDate)) : '',
                                                    ],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                    ],
                                                    // 'pluginEvents' => [
                                                        // "show" => "function(event) { console.log('show event triggered'); console.log(event); }",
                                                        // "hide" => "function(event) {  # `e` here contains the extra attributes }",
                                                        // "clearDate" => "function(event) {  # `e` here contains the extra attributes }",
                                                        // "changeDate" => "function(event) {  # `e` here contains the extra attributes }",
                                                        // "changeYear" => "function(event) {  # `e` here contains the extra attributes }",
                                                        // "changeMonth" => "function(event) {  # `e` here contains the extra attributes }",
                                                    // ],
                                                ]);
                                            ?>

                                            <?= $form->field($model, 'Number')->textInput() ?>
                                            
                                            <?= $form->field($model, 'type')->dropdownList([1 => 'Периодический', 2 => 'Одиночный']); ?>
                                            
                                            <?php 
                                            if($model->type != 2) {
                                                $priceDisplay = 'none';
                                            } else {
                                                $priceDisplay = 'block';
                                            }
                                            ?>
                                            <div id='issueprice-wrapper' style='display:<?=$priceDisplay?>'>
                                                <?= $form->field($model, 'ArticlePrice')->textInput(['style' => 'display']) ?>
                                            </div> 
                                            
                                        </div>
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                            <?= $form->field($model, 'annotation')->textarea(['rows' => 2, 'class' => 'form-control ckeditor-element']) ?>
                                            <?= $form->field($model, 'issue_title')->textarea(['rows' => 2]) ?>
                                            <?= $form->field($model, 'year')->textInput(['maxlength' => true,'data-inputmask'=>'"mask": "y"', 'data-mask' => '']) ?>
                                            <?= $form->field($model, 'release_month')->textInput(['maxlength' => true,'data-inputmask'=>'"mask": "m"', 'data-mask' => '']) ?>
                                            
                                            <?php if (!$model->hasErrors() && $model->isNewRecord) {$model->Checked = 1;} ?>
                                            <?= $form->field($model, 'Checked', ['labelOptions'=>[]])->checkbox() ?>

                                        </div>
                                        <!-- 
                                        <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                                        </div>
                                         -->
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>
                    <div class="tab-pane" id="issue-pdf">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-6 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?php 
                                                echo $form->field($model, 'coverFile')->widget(FileInput::classname(), [
                                                    'options' => ['accept' => 'image/*'],
                                                ]);    
                                            ?>
                                            <?php if(!empty($model->Picture)): ?>
                                                <?= ImageField::widget(['model' => $model, 'imageFieldName' => 'Picture']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class='col-md-6 col-sm-4 col-xs-12' style='padding:0px 25px'>
                                            <?php 
                                                echo $form->field($model, 'issue_file')->widget(FileInput::classname(), [
                                                    // 'options' => ['accept' => 'image/*'],
                                                ]);
                                            ?>
                                            <?php if($model->isPdfLoaded()): ?>
                                                <?php 
                                                    echo(
                                                        yii\helpers\Html::label('Удалить загруженный файл?', 'delete-issue-file', ['style' => 'margin-left: 15px;font-weight: normal;color: #a54c4c;']) . 
                                                        yii\helpers\Html::checkbox('Issue[deleteIssueFile]', false, ['id' => 'delete-issue-file', 'value' => 1, 'style' => 'margin-left: 10px;width: 15px;height: 15px;vertical-align: top;']) 
                                                    );  
                                                ?>
                                                <div class="alert alert-info alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-info"></i> Файл загружен</h4>
                                                    загруженный файл:<b style='color: #942828;'> <?=$model->issue_file; ?></b>
                                                </div>
                                                <?php if ($masterLink = $model->buildMasterDownloadUrl()):?>
                                                    <div class="form-group">
                                                        <label>Мастер-ссылка на скачивание</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon" onclick='$("#issueMasterLinkInput").select();document.execCommand("copy");$("#issueMasterLinkInput").blur();' title="Скопировать в буфер">
                                                                <i class="fa fa-copy"></i>
                                                            </div>
                                                            <?=Html::textInput('', Url::to($masterLink, true) , ['class' => 'form-control pull-right','id' => 'issueMasterLinkInput', 'readonly' => true]); ?>
                                                        </div>
                                                    </div>                                            
                                                <?php endif;?>
                                            <?php endif; ?>
                                           <?= $form->field($model, 'pdf_link_life_time')->textInput(['title'=> 'Чтобы не ограничивать срок активности ссылки оставьте поле пустым', 'data-toggle' => 'tooltip','data-replacement' => 'left']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div> <!--END #user-edit-company -->
                    <div class="tab-pane" id="issue-info">
                        <div class="box">
                            <div class='box-body no-padding'>
                                <div>
                                    <div class="row" style='padding-top:10px;'>
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                            <?= CreateUpdateFields::widget(['model' => $model]); ?>

                                            <?php // echo($form->field($model, 'created')->textInput()); ?>
                                            <?php // echo($form->field($model, 'create_user_id')->textInput()); ?>
                                            <?php // echo($form->field($model, 'last_updated')->textInput()); ?>
                                            <?php // echo($form->field($model, 'last_user_id')->textInput()); ?>
                                            <?php echo($form->field($model, 'LastIP')->textInput(['maxlength' => true, 'disabled' => true])); ?>
                                            <?= $form->field($model, 'LastUserAgent')->textInput(['maxlength' => true, 'disabled' => true]) ?>
                                        </div>
                                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                        </div>
                                        <!-- 
                                        <div class='col-md-12 col-sm-12 col-xs-12'>
                                        </div>
                                         -->
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div> <!--END #user-edit-company -->

                    <?php if($model->isPdfLoaded()): ?>
                        <div class="tab-pane" id="send-issue-tab">
                        <div class="box">
                            <div class='box-body' style="padding: 30px 10px 40px 10px; background: rgba(0,0,0,0.07)" >
                                <div class="row" style='padding-top:10px;'>
                                    <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                        <?php
                                        $notShippedOrdersIds = $model->getNotShippedOrderIds();
                                        $notShippedCount = count($notShippedOrdersIds);
                                        ?>
                                            <button id="sendNotShippedButton"
                                                    type="button"
                                                    class="btn btn-flat btn-primary"
                                                    onclick="sendNotShipped(event)"
                                                    title="Отправить всем, кому еще не было отправлено"
                                                    <?php if($notShippedCount == 0): ?> disabled <?php endif; ?>
                                            >
                                                Отправить неотправленные (<?= $notShippedCount; ?> шт.)
                                            </button>

                                    </div>
                                    <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                                    </div>
                                </div>
                            </div><!--END  bos-body -->
                        </div> <!--END box -->
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class='box-footer'>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat', 'style' => 'margin-right: 30px']) ?>
                <?php if(!$model->isNewRecord): ?>
                    <?php echo(Html::a('Создать новый',Url::toRoute('issue/create'),['class' => 'btn btn-info btn-flat']));  ?>
                    <?php
                        $nextMonthTooltip = 'Создать новый выпуск с почти такими же данными кроме: Дата выхода = + 1 месяц, Номер = +1, Месяц выхода = +1, Дата рассылки  = +1 месяц';
                        $nextYearTooltip = 'Создать новый выпуск с почти такими же данными кроме: Год = +1 год, Дата рассылки  = +1 год, Дата выхода = +1 год';
                    ?>
                    <?php echo(Html::a('Копировать в след. месяц',Url::toRoute('issue/copy/to-next-month/' . $model->Message_ID),['class' => 'btn btn-flat bg-purple', 'data-toggle' => "tooltip", 'title' => $nextMonthTooltip]));  ?>
                    <?php echo(Html::a('Копировать в след. год',Url::toRoute('issue/copy/to-next-year/' . $model->Message_ID),['class' => 'btn btn-flat bg-purple', 'data-toggle' => "tooltip", 'title' => $nextYearTooltip]));  ?>
                <?php endif ?>
                <?php if(!$model->isNewRecord): ?>
                    <?=Html::a('Удалить', Url::to(['issue/delete','id' => $model->Message_ID]), ['class' => 'btn btn-danger pull-right', 'onclick' => 'confirmDelete()','title' => 'Удалить выпуск']); ?>
                    <script type="text/javascript">
                        function confirmDelete(event){
                            if (typeof(event) == 'undefined') 
                                event = window.event;

                            if (confirm('Вы действительно желаете удалить этот выпуск?')) {
                                return true;
                            } else {
                                event.preventDefault();
                                return false;
                            }
                        }
                    </script>
                <?php endif; ?>
            </div>
        </div><!--End bos-footer  -->
    </div> <!--END box -->
    <?php ActiveForm::end(); ?>
</div><!--End #person-form-wrapper -->
<script>
    function sendNotShipped(event) {
        bootbox.confirm({
            message: 'Всем подписчикам на этот выпуск, которым он еще не был отправлен, будет отправлено письмо со ссылкой на ПДФ.',
            buttons: {
                confirm: {
                    label: 'Да, разослать.',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Отмена',
                    className: 'btn-danger'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    $.ajax({
                        url: '/issue/send-not-shipped/<?= $model->Message_ID; ?>',
                        dataType: 'json',
                        type: 'GET'
                    })
                    .done(function (response) {
                        $('#sendNotShippedButton').remove();
                        bootbox.alert('Рассылка добавлена в очередь');
                    })
                    .fail(function (a,b,c) {
                        bootbox.alert('Что то пошло не так. См. консоль браузера.');
                        console.log(a,b,c);
                    });
                } else {
                    return;
                }
            }
        });

    }
</script>

