<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php  
$columnsList = \common\models\NxPriceItem::getAllDeliveryPositions();
$tableHead = [];
foreach($columnsList as $column) {
    $tableHead[$column['zone_id'] . '_' . $column['delivery_type_id']] = $column;
}
$tableData = ['head' => $tableHead, 'body' => []];

foreach($priceItems as $index => $priceItem) {
    $magId = $priceItem->item_id;
    
    if (!isset($tableData['body'][$magId])) 
        $tableData['body'][$magId] = [];
    
    $tableData['body'][$magId][ $priceItem->zone_id . '_' . $priceItem->delivery_type_id ] = $priceItem;
    
}

ksort($tableData['body']);
foreach($tableData['body'] as $row) {
    ksort($row);
}
ksort($tableData['head']);

//foreach($allEditions as $$edition) {
//    if (!isset($tableData[$edition->Message_ID])) {
//        $table[$edition->Message_ID] = [];
//    }
//}

?>

<div class="box box-success box-solid" style='width: auto;float: left;overflow: hidden;min-width: 300px;'>
    <div class="box-header with-border">
        <h3 class="box-title">Цены за один выпуск</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: block;">
        <table class="table table-striped table-bordered" id='price-items-table' style='width:auto;'>
            <thead>
                <tr>
                    <th>Издание</th>
                <?php foreach($tableData['head'] as $columnName=>$headColumn):?>
                    <th data-zome_id='<?=$headColumn['zone_id']?>' data-delivery_type_id='<?=$headColumn['delivery_type_id']?>' >
                        <span><?=$headColumn['value']?></span>
                    </th>
                <?php endforeach;?>
                </tr>
            </thead>
            <tbody>
                <?php $k = 0;?>
                <?php foreach($tableData['body'] as $rowNumber => $row): ?>
                <tr data-key="<?=$rowNumber;?>">
                        <td style='padding: 3px;'>
                            <?php 
                                $key = array_keys($row)[0];
                                echo($row[$key]->edition->HumanizedName); 
                            ?>
                        </td>
                    <?php foreach($row as $columnName => $column): ?>
                        <td data-zone_id='<?= $column->zone_id; ?>' data-delivery_type_id='<?= $column->delivery_type_id; ?>' style='text-align: center;padding: 3px;'<?php if($column->hasErrors()){echo('class="has-error"');} ?> >
                            <?php 
                            $textInputOptions = [
                                'class' => 'form-control input-sm',
                                'style' => 'width:80px;height:25px;display:inline-block',
                            ];
                            if ($column->hasErrors()) {
                                $textInputOptions['data-toggle'] = 'tooltip';
                                $textInputOptions['data-placement'] = 'top';
                                $errorText = '';
                                foreach($column->errors as $attr) {
                                    foreach($attr as $errTxt) {
                                        $errorText .= $errTxt . "\n";
                                    }
                                }
                                $textInputOptions['title'] = $errorText;
                            }
                            echo(Html::activeTextInput($column, "[$k]price", $textInputOptions));
                            echo(Html::activeHiddenInput($column, "[$k]id", []));
                            echo(Html::activeHiddenInput($column, "[$k]zone_id", []));
                            echo(Html::activeHiddenInput($column, "[$k]delivery_type_id", []));
                            echo(Html::activeHiddenInput($column, "[$k]price_id", []));
                            echo(Html::activeHiddenInput($column, "[$k]item_id", []));
//                            if($column->hasErrors())
//                                print_r($column->errors);
                            ?>
                        </td>
                        <?php $k++; ?>
                    <?php endforeach;?>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
