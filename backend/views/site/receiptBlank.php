<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
<style type="text/css">
    body {
        margin: 0;
        background: white!important;
    }
    table tr td:first-child {
        padding-left: 20px;
    }
    table tr td:last-child {
        padding-right: 20px;
    }
    
</style>

<table width="700" border="0" cellspacing="0" cellpadding="1" zbordercolor="#000000" height="520" style="height:520px; width:700px; border-collapse: collapse;	border: solid 1px black;">
    <tbody><tr>
            <td width="200" height="290" valign="top" style="border: solid 1px black; height:290px; width:200px;">
                <table width="100%" border="0" cellspacing="4" cellpadding="4" height="100%" style="height:100%; width:100%;">
                    <tbody><tr valign="top">
                            <td align="right" valign="top" style="font: bold 13px Arial; margin-right:20px; text-align:right;padding-top: 10px;">
                                ИЗВЕЩЕНИЕ
                            </td>
                        </tr>
                        <tr valign="bottom">
                            <td align="middle" valign="bottom" style="font: bold 13px Arial; text-align: center;padding-bottom: 10px">
                                Кассир
                            </td>
                        </tr>
                    </tbody></table>
            </td>
            <td height="290" align="left" valign="top" style="border: solid 1px black; height:290px; text-align:left;">
                <table width="100%" border="0" cellspacing="1" cellpadding="1" height="100%">
                    <tbody><tr>
                            <td align="right" valign="top" style="text-decoration: underline; font: 13px Arial; text-align: right;padding-top: 10px;">
                                Форма № ПД-4
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 13px Arial;">
                                <?php echo($order->publisher->company->org_form . ' ' . $order->publisher->company->name); ?><br>
                                ИНН <?php echo($order->publisher->company->inn);?><br>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (получатель платежа)
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                                Расч. счет <?php echo($order->publisher->company->pay_account);?> в <?php echo($order->publisher->company->bank);?>
                                <br>
                                <!--br-->
                                <u>Корр/сч. <?php echo($order->publisher->company->corr_account);?> БИК <?php echo($order->publisher->company->bik);?></u>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (наименование банка, другие банковские реквизиты)
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                                <?php echo($order->person->name);?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 10px Arial;">
                                <?php echo( $order->address->buildAddressString()); ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (ФИО, адрес плательщика)
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style='padding:0px;'>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#000000">
                                    <tbody><tr valign="middle">
                                            <td style="font: 13px Arial;">
                                                Вид платежа
                                            </td>
                                            <td align="center" width="100" style="text-align: center; width: 100px; font: 13px Arial;">
                                                Дата
                                            </td>
                                            <td align="center" style="text-align: center; font: 13px Arial;">
                                                Сумма
                                            </td>
                                        </tr>
                                        <tr style="font: 13px Arial;">
                                            <td style="font: 13px Arial;">
                                                Оплата заказа № <?php echo($order->id); ?></td>
                                            <td width="100" style="width:100px;">
                                                &nbsp;
                                            </td>
                                            <td valign="middle" align="center" style="text-align: center; font: 13px Arial;">
                                                <?php
                                                    $parts = explode('.', (String)$order->price);
                                                    $sum = $parts[0] . ' руб. ' . $parts[1] . ' коп.';
                                                    echo($sum);
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font: 13px Arial;">
                                Плательщик
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </tbody></table>
            </td>
        </tr>
        <tr>
            <td width="200" height="290" valign="top" style="border: solid 1px black; height:290px; width:200px;">
                <table width="100%" border="0" cellspacing="4" cellpadding="4" height="100%" style="height:100%; width:100%;">
                    <tbody><tr valign="top">
                            <td align="right" valign="top" style="font: bold 13px Arial; margin-right:20px; text-align:right;padding-top: 10px;">
                                КВИТАНЦИЯ
                            </td>
                        </tr>
                        <tr valign="bottom">
                            <td align="middle" valign="bottom" style="font: bold 13px Arial; text-align: center;padding-bottom: 10px">
                                Кассир
                            </td>
                        </tr>
                    </tbody></table>
            </td>
            <td height="290" align="left" valign="top" style="border: solid 1px black; height:290px; text-align:left;">
                <table width="100%" border="0" cellspacing="1" cellpadding="1" height="100%">
                    <tbody><tr>
                            <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 13px Arial;">
                                <?php echo($order->publisher->company->org_form . ' ' . $order->publisher->company->name); ?><br>
                                ИНН <?php echo($order->publisher->company->inn);?><br>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (получатель платежа)
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                                Расч. счет <?php echo($order->publisher->company->pay_account);?> в <?php echo($order->publisher->company->bank);?><br>
                                <u>
                                    Корр/сч. <?php echo($order->publisher->company->corr_account);?> БИК <?php echo($order->publisher->company->bik);?>
                                </u>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (наименование банка, другие банковские реквизиты)
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                                <?php echo($order->person->name);?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 10px Arial;">
                                <?php echo($order->address->buildAddressString());?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                                (ФИО, адрес плательщика)
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style='padding:0px;'>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#000000">
                                    <tbody><tr valign="middle">
                                            <td style="font: 13px Arial;">
                                                Вид платежа
                                            </td>
                                            <td align="center" width="100" style="text-align: center; width: 100px; font: 13px Arial;">
                                                Дата
                                            </td>
                                            <td align="center" style="text-align: center; font: 13px Arial;">
                                                Сумма
                                            </td>
                                        </tr>
                                        <tr style="font: 13px Arial;">
                                            <td style="font: 13px Arial;">
                                                Оплата заказа № <?php echo($order->id);?>
                                            </td>
                                            <td width="100" style="width:100px;">
                                                &nbsp;
                                            </td>
                                            <td valign="middle" align="center" style="text-align: center; font: 13px Arial;">
                                                <?php echo($sum);?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font: 13px Arial;">
                                Плательщик
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
