<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/** 
* структура массива настроек:
* массив для построения ul имеет всего два ключа 
* ulAttributes = [] - любые атрибуты 
* и items = [] - массивы для построения элементов меню
*
* items имеет два ключа li - для построения li
* и allowedRoles - массив с ролями кому доступен этот пункт меню

Массив для построения li может иметь такие ключи: 
* attributes = [] - массив с атрибутами тега
* text = '' - строка текстового наполнения тега li
* link = [] - массив для посторения встроенного внутрь li тега a
* childMenu = [] - массив для построрния вложенного в li тега ul (структуру см. выше)
*
* Массив для построения вложенного тега a (ключ link) может иметь
* text = '' строка текста
* attributes = атрибуты
* icon = [] - массив для построения вложенного в a тега i
* hasChildrenMark = boolean - флаг, предписывающий нужно ли вложить внутрь ссылки следующий html 
* <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
*
**/

return [
    'ulAttributes' => [
        'class' => 'sidebar-menu',
    ],
    'items' => [
        [
            'li' => [
                'attributes' => ['class' => 'header'],
                'text' => 'НАВИГАЦИЯ',
            ],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Профиль',
                    'attributes' => [
                        'href' => \yii\helpers\Url::toRoute(['publisher/update', 'id' => Yii::$app->user->identity->publisher_id]),
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-leanpub'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => false,
                ]
            ],
            'allowedRoles' => ['publisher'],
        ],
        [
            'li' => [
                'attributes' => ['class' => 'treeview'],
                'text' => '',
                'link' => [
                    'text' => 'Администрирование',
                    'attributes' => [
                        'href' => '#',
                    ],
                    'icon' => [
                        'attributes' => ['class' => 'fa fa-leanpub'],
                        'text' => '',
                    ],
                    'hasChildrenMark' => true,
                ],
                'childMenu' => [
                    'ulAttributes' => ['class' => 'treeview-menu'],
                    'items' => [
                        [
                            'li' => [
                                'attributes' => [],
                                'text' => '',
                                'link' => [
                                    'text' => 'Издатели',
                                    'attributes' => ['href' => '#'],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                        'text' => '',
                                    ],
                                    'hasChildrenMark' => true,
                                ],
                                'childMenu' => [
                                    'ulAttributes' => ['class' => 'treeview-menu'],
                                    'items' => [
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Список',
                                                    'attributes' => ['href' => '/publisher/list'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Создать',
                                                    'attributes' => ['href' => '/publisher/create'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                    ],
                                ],
                            ],
				            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'text' => '',
                                'link' => [
                                    'text' => 'Пользователи',
                                    'attributes' => ['href' => '#'],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                        'text' => '',
                                    ],
                                    'hasChildrenMark' => true,
                                ],
                                'childMenu' => [
                                    'ulAttributes' => ['class' => 'treeview-menu'],
                                    'items' => [
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Список',
                                                    'attributes' => ['href' => '/user'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Создать',
                                                    'attributes' => ['href' => '/user/create'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                    ],
                                ]
                            ],
				            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'text' => '',
                                'link' => [
                                    'text' => 'Справочники',
                                    'attributes' => ['href' => '#'],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                        'text' => '',
                                    ],
                                    'hasChildrenMark' => true,
                                ],
                                'childMenu' => [
                                    'ulAttributes' => ['class' => 'treeview-menu'],
                                    'items' => [
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                    'attributes' => ['href' => '/company'],
                                                	'text' => 'Компании',
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Физ.лица',
                                                    'attributes' => ['href' => '/person'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                	'text' => 'Адреса',
                                                    'attributes' => ['href' => '/address'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
								            'allowedRoles' => ['admin'],
                                        ],
                                    ],
                                ]
                            ],
                            'allowedRoles' => ['admin'],
                        ],
                        [
                            'li' => [
                                'attributes' => [],
                                'text' => '',
                                'link' => [
                                    'text' => 'Роли',
                                    'attributes' => ['href' => '#'],
                                    'icon' => [
                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                        'text' => '',
                                    ],
                                    'hasChildrenMark' => true,
                                ],
                                'childMenu' => [
                                    'ulAttributes' => ['class' => 'treeview-menu'],
                                    'items' => [
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                    'text' => 'Список',
                                                    'attributes' => ['href' => '/roles'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
                                            'allowedRoles' => ['admin'],
                                        ],
                                        [
                                            'li' => [
                                                'attributes' => [],
                                                'link' => [
                                                    'text' => 'Создать',
                                                    'attributes' => ['href' => '/roles/create'],
                                                    'icon' => [
                                                        'attributes' => ['class' => 'fa fa-circle-o-notch'],
                                                    ],
                                                    'hasChildrenMark' => false,
                                                ],
                                            ],
                                            'allowedRoles' => ['admin'],
                                        ],
                                    ],
                                ],
                            ],
				            'allowedRoles' => ['admin'],
                        ],
                    ]
                ]
            ],
            'allowedRoles' => ['admin'],
        ]
    ]
];
