/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

var severUrl='//papi.teamosp.ru';

var mag_id=1;
var publisher_id=35;
var jqueryExists = (typeof(jQuery) == 'function') ? true : false;

if (!jqueryExists) {
    var script = document.createElement('script');

    script.type = 'text/javascript';
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

    document.getElementsByTagName('head')[0].appendChild(script);

    script.addEventListener('load', runAfterJqueryLoad, false);
} else {
    runAfterJqueryLoad();
}

function runAfterJqueryLoad() {
    $(document).ready(function () {
        var basePrice = 0; //базовый прайс
        var promoPrice = 0; // цена со скидкой
        var type_develery = 1; //1-почта,2-pdf
        var customer_type_id = 1;  //1-физ лицо
        var release = 0; //кол-во выпусков за период


        var isNumber = function(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        };

        function isEmpty(str) {
            return (!str || 0 === str.length);
        }


        function start() {

            $.ajax({
                type: "GET",
                url:  severUrl + '/subscribe/getForm/' + mag_id + '/' + publisher_id + '?_format=json',
                dataType:"json",
                success: function(jsondata){

                    if (!jsondata)
                        alert("Ошибка при получении данных. Обратитесь в поддержку");

                    $('#form_sub_insert').html(jsondata);
                    getPrice();
                },
                error: function(jsondata){
                    alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });

        }



        function getPrice() {


            $.ajax({
                type: "POST",
                url: severUrl + "/subscribe/get-price",
                dataType:"json",
                data:{
                    publisher_id:publisher_id,
                    subscriber_type_id: customer_type_id,
                    magazineID: mag_id,
                    delivery_type_id: type_develery,
                    _format: 'json'
                },
                success: function(jsondata){

                   if (!isNumber(jsondata.price))
                            alert("Ошибка при расчетах. Обратитесь в поддержку");

                    basePrice = jsondata.price * 1;
                    $('#price').html(basePrice);
                    $('#price_id').val(jsondata.publisher_id*1);
                    getRelease();
                },
                error: function(jsondata){
                   alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });


        }


        function getRelease() {

            var period = $('#period').val();
            if (!period) {
                period = 12;
            }
            var date_start = $('#date_start').val();
            var count = 0;

            $.ajax({
                type: "POST",
                url: severUrl + "/subscribe/count-release",
                dataType:"json",
                data:{
                    period: period,
                    magazineID:mag_id,
                    date_start:date_start,
                    _format: 'json'
                },
                success: function(data){

                    if (!isNumber(data))
                        alert("Ошибка при расчетах. Обратитесь в поддержку");

                    release = data;
                    $("#release").html(data + ' выпусков');
                },
                error: function(jsondata){
                    alert('Что то пошло не так. Обратитесь в поддержку')
                }
            });



        }


        function changePrice() {

            var period = $('#period').val() * 1;
            var price = release * basePrice;

            console.log(period);
            console.log(price);

            if (promoPrice != 0) {
                price = price - (price * promoPrice / 100);
            }
            $('#price').html(price);

        }

        function getPromocode() {

            var promocode = $('#subscribeform-promocode').val();

            if(promocode == '')
                return;

            var period = $('#period').val() * 1;
            var type = $("#dev_type").val() * 1;
            var PromoPrice = 0;

            $.ajax({
                type: "POST",
                url: severUrl + "/subscribe/get-promo",
                dataType:"json",
                data:{
                    promocode: promocode,
                    magazineID: mag_id,
                    period: period,
                    type: type,
                    customer_type_id: customer_type_id,
                    _format: 'json'
                },
                success: function(jsondata){

                    if (!isNumber(jsondata))
                        alert("Ошибка при расчетах. Обратитесь в поддержку");

                    promoPrice = jsondata;
                    $('#discount').val(promoPrice);
                    changePrice();
                },
                error: function(jsondata){
                    alert('Что то пошло не так. Обратитесь в поддержку');
                }
            });

        }


        $(document.body).on('change', "#period", function (e) {
            promoPrice = 0;
            getPrice();
        });

        $(document).ajaxComplete(function (event, xhr, settings) {
            changePrice();
        });


        $(document.body).on('click', "#promoSubmit", function (e) {
            getPromocode();
            return false;
        });


        $(document.body).on('click', "#podpiskaUr", function (e) {
            $("#subscribeform-company_name").prop('required',true);
            $("#subscribeform-legal_address").prop('required',true);
            $("#subscribeform-inn_code").prop('required',true);
            $("#subscribeform-kpp_code").prop('required',true);

            $('.hiddenBlock').slideToggle('slow');
            if ($(this).text() == 'Подписка Юр.лицо') {
                $(this).text('Подписка Физ.лицо');
                customer_type_id = 2;
                $('#sub_type').val(customer_type_id);
                promoPrice=0;
                getPrice();

            }
            else {
                $("#subscribeform-company_name").prop('required',false);
                $("#subscribeform-legal_address").prop('required',false);
                $("#subscribeform-inn_code").prop('required',false);
                $("#subscribeform-kpp_code").prop('required',false);
                $(this).text('Подписка Юр.лицо');
                customer_type_id = 1;
                getPrice();
                $('#sub_type').val(customer_type_id);
            }
        });


        $(document.body).on('click', "#paper", function (e) {
            promoPrice = 0;
            type_develery = 1;
            $("#dev_type").val(type_develery);
            getPrice();
            $('#paper').attr("class", "version-button act");
            $('#pdf').attr("class", "version-button");

        });


        $(document.body).on('click', "#pdf", function (e) {
            promoPrice = 0;
            type_develery = 5;
            $("#dev_type").val(type_develery);
            getPrice();
            $('#pdf').attr("class", "version-button act");
            $('#paper').attr("class", "version-button");

        });


        $(document.body).on('submit', "#subscribe-form", function (e) {
            e.preventDefault();

            var $form = $("#subscribe-form");

            jQuery.ajax({
                url: severUrl + '/subscribe/create-order',
                type: "POST",
                dataType: "html",
                data: jQuery("#subscribe-form").serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);

                    $('#form_sub_insert').html(json);
                },
                error: function (response) {
                    alert('Что то пошло не так. Обратитесь в поддержку');
                }
            });

            return false;
        });


        start();

    });
}



