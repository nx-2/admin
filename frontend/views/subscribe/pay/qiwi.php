<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>

    <img src='http://pay.osp.ru/img/logo/QIWI.gif'><br>
    <span class='db'>ОСМП — «QIWI»</span>

<?php
$phone = (!empty($person->phone) ? $person->phone : '');
$phone = substr(preg_replace("/\D/", '', $phone), -10);
?>
<form action='http://w.qiwi.ru/setInetBill.do' method='post'>
    <input type='hidden' name='from' value='<?=$paySystems->shop_name?>'>
    <input type='hidden' name='system' value='QIWI'>
    <input type='hidden' name='to' value='<?=$phone;?>'>
    <input type='hidden' name='summ' value='<?=$model->price?>'>
    <input type='hidden' name='txn_id' value='<?=$transaction_id?>'>
    <input type='hidden' name='com' value='описание'>
    <input type='hidden' name='check_agt' value='false'>
    <input type='hidden' name='lifetime' value='10'>
    <input type="submit">
</form>
