<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\services;

use common\models\Issue;
use common\models\NxOrder;
use Yii;

/**
 * Description of EmailNoticeTestDataProvider
 *
 * @author andrey
 */
class EmailNoticeTestDataProvider
{
    private $emailNoticeToMethodMap = [
        // $EmailNotice->key_string  => methodToRun
        'userRolesChanged' => 'forUserRolesChanged',
        'userDataChanged' => 'forUserDataChanged',
        'userPasswordChanged' => 'forUserPasswordChanged',
        'newUserCreated' => 'forNewUserCreated',
        'newOrderCreated' => 'forNewOrderCreated',
        'foundNotShippedPDF' => 'forPdfIssueSend',
        'orderHasPaid' => 'forNewOrderCreated',
        'userPasswordIsLost' => 'forPasswordLost',
    ];

    public function getDataFor($emailNotice = false)
    {
        if (!$emailNotice || !array_key_exists($emailNotice, $this->emailNoticeToMethodMap)) {
            if (method_exists($this, 'for' . $emailNotice)) {
                $methodName = 'for' . $emailNotice;
                return $this->$methodName();
            }
            return [];
        }

        $methodName = $this->emailNoticeToMethodMap[$emailNotice];
        return $this->$methodName();
    }

    public function forUserRolesChanged()
    {
        return [
            'user' => Yii::$app->user->identity,
            'publisher' => Yii::$app->user->identity->publisher,
            'oldRoles' => 'Роль1, Роль2, Роль3',
            'newRoles' => 'Роль1, Роль2, Роль3, Роль4',
        ];
    }

    public function forUserDataChanged()
    {
        return [
            'user' => Yii::$app->user->identity,
            'publisher' => Yii::$app->user->identity->publisher,
            'attrsList' => ['FirstName', 'LastName', 'MidName', 'FullName'],
            'changedAttrs' => ['FirstName', 'LastName', 'MidName', 'FullName'],
            'oldValues' => ['FirstName' => 'Иван', 'LastName' => 'Иванов', 'MidName' => 'Иванович', 'FullName' => 'Иванов Иван Иванович'],
        ];
    }

    public function forUserPasswordChanged()
    {
        return [
            'user' => Yii::$app->user->identity,
            'publisher' => Yii::$app->user->identity->publisher,
            'oldPassword' => 'oldPasswordValueTestData',
            'newPassword' => 'newPasswordValueTestData',
        ];
    }

    public function forPasswordLost()
    {
        return [
            'user' => Yii::$app->user->identity,
            'publisher' => Yii::$app->user->identity->publisher,
            'resetPasswordLink' => 'https://nx2-admin.dimeo.ru/site/reset-password?hash=sdkfhsdfkjfhkjfdhw4876dfkjh98475jkhfg97dgf'
        ];
    }

    public function forNewUserCreated()
    {
        return [
            'user' => Yii::$app->user->identity,
            'publisher' => Yii::$app->user->identity->publisher,
        ];
    }

    public function forNewOrderCreated()
    {
        $publisher = Yii::$app->user->identity->publisher;
        $pricesIds = $publisher->getPricesIds();
        $pricesIds = '(' . implode(',', $pricesIds) . ')';
        //взять последний заказ издателя
        $order = NxOrder::findBySql('SELECT * FROM nx_order o WHERE o.price_id IN ' . $pricesIds . ' ORDER BY id DESC LIMIT 1')->one();
        return [
            'order' => $order->id,
            'publisher' => $publisher->id,
        ];
    }

    /**
     * @return array
     */
    public function forPdfIssueSend()
    {
        $publisher = Yii::$app->user->identity->publisher;

        $getIssueWithPdfQueryString = 'SELECT i.* FROM publisher_editions pe
                                      LEFT JOIN Edition e ON pe.magazine_id = e.Message_ID
                                      LEFT JOIN Issue i ON i.MagazineID = e.Message_ID
                                      INNER JOIN nx_order_item_release oir ON i.Message_ID = oir.release_id           
                                      WHERE pe.publisher_id = ' . $publisher->id . '
                                      AND i.issue_file <> \'\' LIMIT 1';
        $getAnyIssueQueryString = 'SELECT i.* FROM publisher_editions pe
                                      LEFT JOIN Edition e ON pe.magazine_id = e.Message_ID
                                      LEFT JOIN Issue i ON i.MagazineID = e.Message_ID
                                      WHERE pe.publisher_id = ' . $publisher->id . ' LIMIT 1';

        $issue = Issue::findBySql($getIssueWithPdfQueryString)->one(); // выпуск с загруженным ПДФ предпочтительнее
        if (empty($issue))
            $issue = Issue::findBySql($getAnyIssueQueryString)->one();

        if (empty($issue))
            return [];

        $order = $issue->getOrders()->where(['order_state_id' => 3])->one(); // Оплаченный заказ предпочтительнее

        if (empty($order)) {
            $orders = $issue->orders;
            if (empty($orders))
                return [];

            $order = $orders[0];
        }

        // здесь вполне может оказаться, что заказ НЕ оплачен, ПДФ не загружен, Выпуск Не выпущен и дата начала рассылки еще не наступила
        // в принципе именно здесь нам это не важно. Здесь просто демонстрационные данные.
        // Эти данные предназначены для последующей передачи в шаблон. Аналогичные данные будут передаваться в реальных случаях. В реальных случаях
        // в очередь задач будут добавляться задания на отправку писем. Поскольку хранить огромные взаимосвязанные объекты AR в очереди до момента
        // выполнения задания крайне накладно и бессмысленно, передаются только ИД соответствующих записей, а в шаблоне они будут заново восстановлены и использованы
        // В случае с демонстрационными данными это выглядит очень тупо, но это нужно исключительно для совместимости с реальными ситуациями с очередями..
        return [
            'order_id' => $order->id,
            'publisher_id' => $publisher->id,
            'issue_id' => $issue->Message_ID,
        ];
    }
}

