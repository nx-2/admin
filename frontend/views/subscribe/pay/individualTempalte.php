<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

$banks = array(
    'Сбербанк Москва' => array(
        'name' 			=> 'Сбербанк России, г. Москва',
        'corr_account' 	=> '30101810400000000225',
        'bik_code' 		=> '044525225'
    )
);


$companies = array(
    'OOO_OSP' 				=> array(
        'name' 				=> 'ООО «Издательство «Открытые Системы»',
        'legal_address'		=> '127254, Россия, Москва, проезд Добролюбова д. 3, стр. 3, каб. 13',// '127254, Москва, ул. Руставели д. 12а, стр 2',
        'post_address'		=> '127254, Россия, Москва, проезд Добролюбова д. 3, стр. 3, каб. 13',// 'Россия, 127254, Москва, ул. Руставели д. 12а, стр 2',
        'phone'				=> '+7 (495) 725-4780',
        'fax'				=> '+7 (495) 725-4785',
        'inn_code' 			=> '9715004017',
        'kpp_code'			=> '771501001',
        'bank_account' 		=> '40702810438170101424 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/ooo-shtamp.jpg" width="170" />',
        'CEO'				=> 'Герасина Г.А.',
        'chief_accountant'	=> 'Федорчуков С.Д.',
        'getNDS'			=>	function($value){
            return ($value / 118) * 18;
        }
    ),
    'OSP' 				=> array(
        'name' 				=> 'ЗАО «Издательство «Открытые Системы»',
        'legal_address'		=> '123056, Москва, Электрический пер., 8, cтр.3',
        'post_address'		=> 'Россия, 123056, Москва, Электрический пер., 8, cтр.3',
        'phone'				=> '+7 (495) 725-4785',
        'fax'				=> '+7 (495) 725-4788',
        'inn_code' 			=> '7706128372',
        'kpp_code'			=> '771001001',
        'bank_account' 		=> '40702810438170101424 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/pechat_zao_color.gif" />',
        'CEO'				=> 'Герасина Г.А.',
        'chief_accountant'	=> 'Федорчуков С.Д.',
        'getNDS'			=>	function($value){
            return ($value / 118) * 18;
        }
    ),
    'ОСП-Курьер' 		=> array(
        'name' 				=> 'ООО «ОСП-Курьер»',
        'legal_address'		=> '123056, Москва, Электрический пер., 8, cтр.3',
        'post_address'		=> 'Россия, 123056, Москва, Электрический пер., 8, cтр.3',
        'phone'				=> '+7 (495) 725-4785',
        'fax'				=> '+7 (495) 725-4788',
        'inn_code' 			=> '7710297197',
        'kpp_code'			=> '771001001',
        'bank_account' 		=> '40702810638170101680 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/pechat.gif" />',
        'CEO'				=> 'Борисов М.Е.',
        'chief_accountant'	=> 'Илюхин В.Ю.',//'Федорчукова М.Д.',
        'getNDS'			=> function($value){
            return $value / 11;
        }
    )
);
$recipient = $companies['OOO_OSP'];

// отдает сумму в рублях и копейках
function getRoubleSumShort($sum){
    // получить сумму копеек
    $kops = round($sum * 100) % 100;
    if($kops==0){
        $kops=='00';
    }

    // получить сумму рублей
    $sum = (int)$sum;

    return $sum.' руб. '.$kops.' коп.';
}
?>






    <style type="text/css">
        body {
            margin: 0;
            background			: #FFFFFF;
        }
    </style>

    <table width="570" border="0" cellspacing="0" cellpadding="1" zbordercolor="#000000" height="520" style="height:520px; width:570px; border-collapse: collapse;	border: solid 1px black;">
        <tr>
            <td width="200" height="290" valign="top" style="border: solid 1px black; height:290px; width:200px;">
                <table width="100%" border="0" cellspacing="4" cellpadding="4" height="100%" style="height:100%; width:100%;">
                    <tr valign="top">
                        <td align="right" valign="top" style="font: bold 13px Arial; margin-right:20; text-align:right;">
                            ИЗВЕЩЕНИЕ
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="middle" valign="bottom" style="font: bold 13px Arial; text-align: center;">
                            Кассир
                        </td>
                    </tr>
                </table>
            </td>
            <td height="290" align="left" valign="top" style="border: solid 1px black; height:290px; text-align:left;">
                <table width="100%" border="0" cellspacing="1" cellpadding="1" height="100%">
                    <tr>
                        <td align="right" valign="top" style="text-decoration: underline; font: 13px Arial; text-align: right;">
                            Форма № ПД-4
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 13px Arial;">
                            <?php echo $recipient['name']; ?>
                            <br>
                            ИНН <?php echo $recipient['inn_code'] ?>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (получатель платежа)
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                            <br>
                            <?php //echo $recipient['bank']['name']; ?>
                            Расч. счет <?php echo $recipient['bank_account']; ?>
                            <!--br-->
                            <u>
                                Корр/сч. <?php echo $recipient['bank']['corr_account']; ?>
                                БИК <?php echo $recipient['bank']['bik_code']; ?>
                            </u>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (наименование банка, другие банковские реквизиты)
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                            <?php echo $person['name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 10px Arial;">
                            <?php echo ($model->address->address)?$model->address->address:''; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (ФИО, адрес плательщика)
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#000000">
                                <tr valign="middle">
                                    <td style="font: 13px Arial;">
                                        Вид платежа
                                    </td>
                                    <td align="center" width="100" style="text-align: center; width: 100px; font: 13px Arial;">
                                        Дата
                                    </td>
                                    <td align="center" style="text-align: center; font: 13px Arial;">
                                        Сумма
                                    </td>
                                </tr>
                                <tr style="font: 13px Arial;">
                                    <td style="font: 13px Arial;">
                                        Оплата заказа № <?php echo $model->id; ?>
                                    </td>
                                    <td width="100" style="width:100px;">
                                        &nbsp;
                                    </td>
                                    <td valign="middle" align="center" style="text-align: center; font: 13px Arial;">
                                        <?php echo getRoubleSumShort($model->price); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font: 13px Arial;">
                            Плательщик
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="200" height="290" valign="top" style="border: solid 1px black; height:290px; width:200px;">
                <table width="100%" border="0" cellspacing="4" cellpadding="4" height="100%" style="height:100%; width:100%;">
                    <tr valign="top">
                        <td align="right" valign="top" style="font: bold 13px Arial; margin-right:20; text-align:right;">
                            КВИТАНЦИЯ
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="middle" valign="bottom" style="font: bold 13px Arial; text-align: center;">
                            Кассир
                        </td>
                    </tr>
                </table>
            </td>
            <td height="290" align="left" valign="top" style="border: solid 1px black; height:290px; text-align:left;">
                <table width="100%" border="0" cellspacing="1" cellpadding="1" height="100%">

                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 13px Arial;">
                            <?php echo $recipient['name']; ?>
                            <br>
                            ИНН <?php echo $recipient['inn_code'] ?>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (получатель платежа)
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                            <br>
                            <?php //echo $recipient['bank']['name']; ?>
                            Расч. счет <?php echo $recipient['bank_account']; ?>
                            <!--br-->
                            <u>
                                Корр/сч. <?php echo $recipient['bank']['corr_account']; ?>
                                БИК <?php echo $recipient['bank']['bik_code']; ?>
                            </u>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (наименование банка, другие банковские реквизиты)
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; font: 13px Arial;">
                            <?php echo $person['name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="text-align: center; text-decoration: underline; font: 10px Arial;">
                            <?php echo ($model->address->address)?$model->address->address:''; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="text-align: center; font: 10px Arial;">
                            (ФИО, адрес плательщика)
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#000000">
                                <tr valign="middle">
                                    <td style="font: 13px Arial;">
                                        Вид платежа
                                    </td>
                                    <td align="center" width="100" style="text-align: center; width: 100px; font: 13px Arial;">
                                        Дата
                                    </td>
                                    <td align="center" style="text-align: center; font: 13px Arial;">
                                        Сумма
                                    </td>
                                </tr>
                                <tr style="font: 13px Arial;">
                                    <td style="font: 13px Arial;">
                                        Оплата заказа № <?php echo $model->id; ?>
                                    </td>
                                    <td width="100" style="width:100px;">
                                        &nbsp;
                                    </td>
                                    <td valign="middle" align="center" style="text-align: center; font: 13px Arial;">
                                        <?php echo getRoubleSumShort($model->price); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font: 13px Arial;">
                            Плательщик
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

