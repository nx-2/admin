<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use \common\models\behaviors\ARattrChangedToValue;
use \common\custom_components\pay\YandexApiPay;

/**
 * This is the model class for table "ya_kassa_api_payment".
 *
 * @property int $id
 * @property string $ya_payment_id id платежа на стороне yandex кассы
 * @property int $order_id
 * @property int $publisher_id
 * @property string $last_status Список статусов предопределен документацией к api yandex kassa
 * @property string $last_idempotence_key см. "Идемпотентность" документации api yandex kassa
 *
 * @property NxOrder $order
 * @property Publisher $publisher
 * @property Transactions $transaction
 */
class YaKassaApiPayment extends \common\models\CommonModel
{
    public function init()
    {
        parent::init();
        $this->on('yaApiPaidSuccess', ['common\custom_components\event_handlers\ARattrChangedHandler', 'yaApiPaymentObtained']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ya_kassa_api_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ya_payment_id', 'order_id', 'publisher_id', 'last_status'], 'required'],
            [['order_id','publisher_id'], 'integer'],
            [['last_status'], 'string'],
            ['last_status', 'in', 'range' => ['pending', 'waiting_for_capture', 'succeeded', 'canceled','dropped'], 'messsage' => 'Недопустимое значение статуса платежа'],
            [['ya_payment_id', 'last_idempotence_key'], 'string', 'max' => 60],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrder::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transactions::className(), 'targetAttribute' => ['order_id' => 'transactionid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ya_payment_id' => 'ID платежа на стороне yandex kassa',
            'order_id' => 'ID заказа',
            'publisher_id' => 'ID издателя',
            'last_status' => 'Последний известный статус',
            'last_idempotence_key' => 'Последний ID идемпотентности ',
        ];
    }

    public function behaviors()
    {
        $myBehaviors =  [
            [
                'class' => ARattrChangedToValue::class,
                'options' => [
                    [
                        'attributes' => [
                            'last_status' => YandexApiPay::PAYMENT_STATUS_SUCCEEDED,
                        ],
                        'eventName' => 'yaApiPaidSuccess',
                    ]
                ],
            ],


        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(NxOrder::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transactions::className(), ['transactionid' => 'order_id']);
    }
}
