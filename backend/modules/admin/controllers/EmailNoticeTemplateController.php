<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\controllers;

use Yii;
use common\models\EmailNoticeTemplate;
use backend\modules\admin\models\EmailNoticeTemplateSearch;
use backend\modules\admin\controllers\SiteController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \common\services\EmailNoticeTestDataProvider;


/**
 * EmailNoticeTemplateController implements the CRUD actions for EmailNoticeTemplate model.
 */
class EmailNoticeTemplateController extends SiteController
{
    public $dataProvider;

    public function __construct($id, $module, $config = array())
    {
        parent::__construct($id, $module, $config);
        $this->dataProvider = new EmailNoticeTestDataProvider();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailNoticeTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailNoticeTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailNoticeTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailNoticeTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailNoticeTemplate();

        if ($model->load(Yii::$app->request->post())) {
            $model->buildTemplateFile();
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Создан новый шаблон.');
                $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('danger', 'Новый шаблон НЕ был создан.');
//                print_r($model->getErrors());
            }
//            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmailNoticeTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            else
                Yii::$app->session->addFlash('danger', 'Не сохранено.');
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmailNoticeTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->actionIndex();
    }

    public function actionTestTemplate($noticeId = false)
    {
        //показать результат рендеринга предопределенного шаблона (предположительно @common/mail/temp_test.php, но вообще то это в конфигах указано)
        $this->layout = '@common/mail/layouts/html';

        //тип уведомления обязательно нужен, чтобы знать какие тестовые данные подготовить и передать в шаблон
        if (!$noticeId) {
            throw new \yii\web\NotFoundHttpException();
        }

        $noticeType = \common\models\EmailNotice::findOne(['id' => $noticeId]);
        if (null == $noticeType) {
            throw new \yii\web\NotFoundHttpException();
        }

        $noticeKeyString = $noticeType->key_string;

        $testTemplateAlias = Yii::$app->params['emailNoticeTestTemplatePath'];
//        $testTemplateFile = Yii::getAlias( $testTemplateAlias ) . '/' . Yii::$app->params['emailNoticeTestTemplateFile'] . '.php';
        $data = $this->dataProvider->getDataFor($noticeKeyString);
        return $this->render($testTemplateAlias . '/' . Yii::$app->params['emailNoticeTestTemplateFile'], $data);
    }

    public function actionGetTemplate($templateId = false)
    {
        if (!$templateId) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->layout = '@common/mail/layouts/html';

        $template = \common\models\EmailNoticeTemplate::findOne(['id' => $templateId]);

        if (null == $template) {
            throw new \yii\web\NotFoundHttpException();
        }

        try {
            $templateView = Yii::getAlias($template->template_file, true) . '.php';

        } catch (\Exception $ex) {
            throw new \yii\web\NotFoundHttpException();
        }
        if (!file_exists($templateView)) {
            throw new \yii\web\NotFoundHttpException();
        }
        $noticeKeyString = $template->notice->key_string;
        $data = $this->dataProvider->getDataFor($noticeKeyString);

        return $this->render($template->template_file, $data);
    }

    public function actionGetTemplateHtml($templateId = false)
    {
        // возвращает результат рендеринга запрошенного шаблона (с тестовыми данными)
        // либо результат рендеринга присланного тестового html кода (testingMode)

        Yii::$app->response->format = 'json';
        $toReturn = [];

        $isTestingMode = (Boolean)Yii::$app->request->post('testTemplate', false); //проверка флага проверки тестового html
        if ($isTestingMode) {
            // в этом случае также обязательны сам хтмл код и ид типа email уведомления
            $testHtml = Yii::$app->request->post('testHtml', false);
            $noticeId = Yii::$app->request->post('notice_id', false);
        }

        if ($isTestingMode && (!$testHtml || !$noticeId)) {
            $toReturn['status'] = 'err';
            $toReturn['errMessage'] = 'Отсутствет HTML код, подлежащий тестрованию либо не указан тип шаблона';
            return $toReturn;
        }

        if (!$isTestingMode && !$templateId) {
            // если это не быстрый просмотр тестового хтмл кода (тестовый режим), то обязательно нужно ид шаблона
            $toReturn['status'] = 'err';
            $toReturn['errMessage'] = 'Отсутствует ИД шаблона';
            return $toReturn;
        }

        if (!$isTestingMode) {
            //обычный (не тестовый) режим
            $data = ['templateId' => $templateId];
        } else {
            // быстрый просмотр тестового хтмл кода.
            // весь присланный хтмл код закинем в специально для этого существующий файл

            $noticeType = \common\models\EmailNotice::findOne(['id' => $noticeId]);
            if ($noticeType == null) {
                $toReturn['status'] = 'err';
                $toReturn['errMessage'] = 'Запрошенный тип уведомления не найден';
                return $toReturn;
            }
            $noticeKeyString = $noticeType->key_string;

            $testTemplateAlias = Yii::$app->params['emailNoticeTestTemplatePath'];
            $testTemplateFile = Yii::getAlias($testTemplateAlias) . '/' . Yii::$app->params['emailNoticeTestTemplateFile'] . '.php';
            file_put_contents($testTemplateFile, $testHtml);
            $data = ['templateId' => 'test', 'noticeId' => $noticeId];
        }

        $toReturn = [
            'status' => 'OK',
            'html' => $this->renderPartial('_email-template-content', $data), // модальное окно bootstrap
        ];
        return $toReturn;
    }

    /**
     * Finds the EmailNoticeTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailNoticeTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailNoticeTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
