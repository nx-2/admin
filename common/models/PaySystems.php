<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "systems".
 *
 * @property integer $id
 * @property string $name
 * @property string $workname
 * @property integer $smin
 * @property integer $smax
 * @property string $description
 * @property string $inn
 * @property string $kpp
 * @property integer $block
 * @property string $pass1
 * @property string $pass2
 * @property string $shop_name
 * @property string $shop_param
 *
 * @property RelationSystems[] $relationSystems
 * @property ShopsSystems[] $shopsSystems
 * @property Shops[] $shops
 * @property Transactions[] $transactions
 */
class PaySystems extends \common\models\CommonModel
{
    const SYSTEM_YANDEX_KASSA       = 2;
    const SYSTEM_YANDEX_KASSA_DEMO  = 8;
    const SYSTEM_YANDEX_CASH        = 9;
//    const SYSTEM_YANDEX_KASSA_API   = 10;
    const SYSTEM_YANDEX_KASSA_API   = 1;
    const SYSTEM_YOO_KASSA_API_TEST = 10;

    const SYSTEM_YOO_KASSA_API = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'systems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['smin', 'smax', 'block'], 'integer'],
            [['inn', 'kpp', 'pass1', 'pass2', 'shop_name', 'shop_param'], 'required'],
            [['name', 'workname'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 500],
            [['inn', 'kpp'], 'string', 'max' => 50],
            [['pass1', 'pass2', 'shop_name', 'shop_param'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'workname' => 'Workname',
            'smin' => 'Smin',
            'smax' => 'Smax',
            'description' => 'Description',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'block' => 'Block',
            'pass1' => 'Pass1',
            'pass2' => 'Pass2',
            'shop_name' => 'Shop Name',
            'shop_param' => 'Shop Param',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationSystems()
    {
        return $this->hasMany(RelationSystems::className(), ['system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopsSystems()
    {
        return $this->hasMany(ShopsSystems::className(), ['system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(Shops::className(), ['id' => 'shop_id'])->viaTable('shops_systems', ['system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transactions::className(), ['system' => 'id']);
    }
}
