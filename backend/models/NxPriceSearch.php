<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NxPrice;

/**
 * NxPriceSearch represents the model behind the search form about `common\models\NxPrice`.
 */
class NxPriceSearch extends NxPrice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'publisher_id', 'item_type', 'customer_type_id', 'checked', 'create_user_id', 'last_user_id'], 'integer'],
            [['name', 'date', 'created', 'last_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NxPrice::find()->where(['publisher_id' => Yii::$app->user->identity->publisher_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publisher_id' => $this->publisher_id,
            'item_type' => $this->item_type,
            'customer_type_id' => $this->customer_type_id,
//            'date' => $this->date,
            'checked' => $this->checked,
            'create_user_id' => $this->create_user_id,
            'last_user_id' => $this->last_user_id,
            'created' => $this->created,
            'last_updated' => $this->last_updated,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
