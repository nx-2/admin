<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div id='email-notice-template-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="email-notice-template-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="email-notice-template-label">Ваш шаблон</h4>
                <div><input type='text' placeholder='ширина...' onchange='setIframeSizes(this.value)' style='width: 70px;padding: 5px;border: 1px solid rgba(0,0,0,0.1);'></div>
            </div>
            <div class="modal-body">
                <?php if($templateId == 'test'): ?>
                    <iframe src='<?php echo(Yii::$app->params['homeHost']);?>/adm/email-notice-template/test-template/<?php echo($noticeId); ?>' style='border:none;outline: none' id='popupTemplateIframe'>
                            <?php // echo($content); ?>
                    </iframe>
                <?php else: ?>
                    <iframe src='<?php echo(Yii::$app->params['homeHost']);?>/adm/email-notice-template/get-template/<?php echo($templateId); ?>' style='border:none;outline: none' id='popupTemplateIframe'>
                            <?php // echo($content); ?>
                    </iframe>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
