<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "publisher_property".
 *
 * @property integer $id
 * @property integer $publisher_id
 * @property integer $property_type_id
 * @property string $value
 *
 * @property Publisher $publisher
 * @property PublisherPropertyType $propertyType
 */
class PublisherProperty extends \common\models\CommonModel
{
//    private $urlTypesIds = [1];
    private $urlTypesIds = [9];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $valueRule = $this->buildRuleForValue();
        $rules =  [
            [['publisher_id', 'property_type_id'], 'required'],
            [['publisher_id', 'property_type_id'], 'integer'],
            ['value', 'string'],
//            [['value'], 'string', 'max' => 255],
            [['publisher_id', 'property_type_id'], 'unique', 'targetAttribute' => ['publisher_id', 'property_type_id'], 'message' => 'Для указанного издателя уже существует такое свойство'],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id'], 'message' => 'Указанный издатель отсутствует' ],
            [['property_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PublisherPropertyType::className(), 'targetAttribute' => ['property_type_id' => 'id'], 'message' => 'неизвестный тип свойства'],
        ];
        $rules[] = $valueRule;

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher_id' => 'ID издателя',
            'property_type_id' => 'Свойство',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyType()
    {
        return $this->hasOne(PublisherPropertyType::className(), ['id' => 'property_type_id']);
    }

    private function buildRuleForValue()
    {
        $rule = [['value'], 'string', 'max' => 255];

        if ( in_array($this->property_type_id, $this->urlTypesIds) ) {
            $rule = ['value', 'url', 'defaultScheme' => 'https'];
        }
        return $rule;
    }
}
