<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\InvalidParamException;
use common\models\events\ARattrChangedToValueEvent;
/**
 * Класс поведения, предназначенный для подключения к ActiveRecord и проверяющий что среди измененных атрибутов AR указанным аттрибутам этой AR присвоены
 * указанные значения. Если это так, то генерируется указанное события. Комплектов (атрибут/значение, атрибут/значение..) + имя события  может быть более одного.
 * при генерировании события  создается и передается в обработчик экземпляр события common\models\events\ARattrChangedToValueEvent
 * Пример:
 * Если у заказа изменились значения order_state_id и payment_type_id и их новые значение равны "3" и  "2" соответственно, то сгенерировать событие 'ЗаказОплаченЭлектроннымПлатежем'
 **/
class ARattrChangedToValue extends Behavior
{
    /**
     * @var array
     the following structure expected:
     *
     [
        [
            'attributes' => [
                'name' => 'value' | 'any',
                'name1' => 'value1',
            ],
            'eventName' => 'someName',
        ],
        [
            'attributes' => [
                'name' => 'value',
            ],
            'eventName' => 'someName1',
        ],
     ]
     * 'any' - is a special value
     **/
    public $options = []; // массив, содержащий необходимые данные для генерирования события (событий). Должен содержать список подмассивов, каждый из которых имеет ключи attributes и eventName
    // attributes - это массив, в котором ключи указывают на имя атрибута модели, а значение содержит то значение, которое должен иметь атрибут. Для генерации события все указанные аттрибуты должны
    // иметь указанные значения.

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }
    public function afterSave($event)
    {
        $changedAttrs = array_keys($event->changedAttributes);
        foreach( $this->options as $option ) {
            $attrsUnderWatch = array_keys( $option['attributes'] );
            if ( count(array_intersect($attrsUnderWatch,$changedAttrs)) == count($attrsUnderWatch) ) {
                //все указанные атрибуты изменились
                $eventShouldBeTriggered = true;
                foreach ( $option['attributes'] as $attrName => $attrExpectedValue) {
                    if ($attrExpectedValue != 'any' && $this->owner->{$attrName} != $attrExpectedValue ) {
                        $eventShouldBeTriggered = false;
                        break;
                    }
                }
                if ($eventShouldBeTriggered) {
                    $event = new ARattrChangedToValueEvent();
                    $event->attrsUnderWatch = $attrsUnderWatch;
                    $this->owner->trigger($option['eventName'], $event);
                }
            }
        }
    }

    public function attach($owner)
    {
        if ( !($owner instanceof yii\db\ActiveRecord) )
            throw new InvalidParamException('attrChangeWatchBehavior can be attached to ActiveRecord class only!');

        if (empty($this->options) || !is_array($this->options))
            throw new InvalidParamException( 'The options parameter of ' . $this::className() . ' must be not empty array.' );


        foreach ($this->options as $option) {
            if (!is_array($option) || !array_key_exists('attributes', $option) || !array_key_exists('eventName', $option)) {
                throw new InvalidParamException( 'Each entry of the options parameter of ' . $this::className() . ' must be not empty array, which has attributes and eventName keys' );
            }
            if ( empty($option['attributes']) || empty($option['eventName'])) {
                throw new InvalidParamException( 'attributes and eventName cannot be empty in the options parameter of ' . $this::className() );
            }
            foreach( $option['attributes'] as $arAttrName => $arAttrValue ) {
                if (!in_array($arAttrName, $owner->attributes())) {
                    throw new InvalidParamException( 'The property <'.  $arAttrName . '> does not exist in ' . $owner->className());
                }
            }
        }
        parent::attach($owner);
    }
}
