<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\custom_components\pay\clients;

use YandexCheckout\Client\CurlClient as Client;
use YooKassa\Client\CurlClient as YooClient;

//class YaKassaApiCustomCurlClient extends Client
class YaKassaApiCustomCurlClient extends YooClient
{
    private $lastResponse = null;

    public function call($path, $method, $queryParams, $httpBody = null, $headers = array())
    {
        $result = parent::call($path, $method, $queryParams, $httpBody, $headers);
        $this->lastResponse = $result;
        return $result;
    }
    public function getLastResponse()
    {
        return $this->lastResponse;
    }
}