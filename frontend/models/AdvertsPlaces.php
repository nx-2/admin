<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\models;

use Yii;

/**
 * ContactForm is the model behind the contact form.
 */
class AdvertsPlaces extends \common\models\AdvertsPlaces
{
	public function fields()
	{
		$fields = parent::fields();
		// $fields[] = 'magazine';
		$fields[] = 'code';
		return $fields;
	}
}
