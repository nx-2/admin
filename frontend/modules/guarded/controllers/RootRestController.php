<?php

namespace frontend\modules\guarded\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class RootRestController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('Доступ запрещен. Недостаточно прав');
                },
                'rules' => [],
            ],

        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);

    }

}