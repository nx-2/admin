<?php

namespace frontend\modules\guarded;
use frontend\modules\guarded\model\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use frontend\modules\guarded\model\User as GuardedUser;
use yii\helpers\Url;

/**
 * protected module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\guarded\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::configure($this, require  __DIR__ . '/module_config.php');
        $userConfig = [
            'class' => \yii\web\User::class,
            'identityClass' => GuardedUser::class,
            'enableAutoLogin' => true,
            'enableSession' => false,
        ];

        Yii::$app->set('user', $userConfig);

    }

    public function behaviors()
    {
        $parentBehaviors = parent::behaviors();
        $thisBehaviors = [
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Allow-Headers' => ['content-type'],
//                'Access-Control-Expose-Headers' => [
//                    'X-Pagination-Current-Page',
//                    'X-Pagination-Page-Count',
//                    'X-Pagination-Per-Page',
//                    'X-Pagination-Total-Count',
//                ]
                ]
            ],
        ];
        if (!$this->request->isOptions) {
            $thisBehaviors['authenticator'] = [
                'class' => HttpBearerAuth::class,
            ];
        }
        return ArrayHelper::merge($parentBehaviors, $thisBehaviors);
    }

}
