<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
<!--<link rel="stylesheet" href="<?php  //echo(Yii::getAlias('@domain/css/pay.css')); ?>">-->
<?php
use yii\widgets\ActiveForm;
$wrapperId = Yii::$app->params['allPaymentMethodsWrapperId'];
$randStr = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, 7) . substr(str_shuffle('0123456789'), 0, 3);
$apiHost = Yii::$app->params['api']['host'];
?>
<style>
#<?=$wrapperId;?> #tabs-container__{
    /*width: 600px;*/
    overflow: hidden;
    padding: 20px;
}
#<?=$wrapperId;?> .tabs-menu__ {
    /* float: left; */
    clear: both;
    /* width: 780px; */
    padding: 0px;
    overflow: hidden;
    margin: 0px;
    margin-top:20px;
    background:white;
}

#<?=$wrapperId;?> .tabs-menu__ li {
    height: 50px;
    line-height: 50px;
    float: left;
    border-top: 1px solid #d4d4d1;
    border-left: 1px solid #d4d4d1;
    list-style-type: none;
    border-radius: 8px 8px 0px 0px;
    color: #000;
    border-bottom: 1px solid #d4d4d1;
    padding-bottom: 9px;
    padding-left: 15px;
    padding-right: 15px;
    border-right: 1px solid #d4d4d1;
    box-sizing: border-box;
    position:relative;
}
#<?=$wrapperId;?> .tabs-menu__ li .ajax-loading {
    position:absolute;
    width:20px;
    height:20px;
    background:url(<?=$apiHost;?>/images/ajax-loader.gif) no-repeat center;
    background-size: contain;
    top:1px;
    right:1px;
    display:none;
}
#<?=$wrapperId;?> .tabs-menu__ li.ajax-load .ajax-loading{
    display:block;
}
#<?=$wrapperId;?> .lefttd{
    text-align: left;width: 200px;    font-size: 14px;
}
#<?=$wrapperId;?> .tabs-menu__ li.current {
    position: relative;
    background-color: #fff;
    border-bottom: 1px solid #fff;
    z-index: 5;
    box-shadow: inset 0px 2px 4px rgba(0,0,255,0.15);
}

#<?=$wrapperId;?> .tabs-menu__ li a {
    /*padding: 10px;*/
    text-transform: uppercase;
    color: #000;
    text-decoration: none;
}
#<?=$wrapperId;?> #tabs-container__ > ul > li > a > img {
    max-height:100%;
}
/*#<?=$wrapperId;?> #tabs-container__ > ul > li:nth-child(1) > a > img{
    padding-top: 3px;
}*/
#<?=$wrapperId;?> #tabs-container__ > ul > li > a > img{
    padding-top: 3px;
}
/*#<?=$wrapperId;?> #tabs-container__ > ul > li:nth-child(2) > a > img{
    padding-top: 3px;
}*/

#<?=$wrapperId;?> .tabs-menu__ .current a {
    color: #2e7da3;
}

#<?=$wrapperId;?> .name{
    font-size: 16px;
    font-weight: 900;
}
#<?=$wrapperId;?> .name-m{

}

#<?=$wrapperId;?> .tab {
    background-color: #fff;
/*    float: left;*/
    margin-bottom: 20px;
    width: auto;
    /*margin-left: 37px;*/
    /*margin-top: 20px;*/
    border:1px solid #d4d4d1;
    margin-top:-1px;
    box-shadow: 0px 0px 2px rgba(0,0,0,0.2);
    min-height: 70px;
}

#<?=$wrapperId;?> .sum{
    font-size: 21px;
    margin-top: 20px;
}

#<?=$wrapperId;?> #oform-zak {
    padding: 10px 30px;
    border: none;
    background: #0167d5;
    color: white;
    font-size: 15px;
}

#<?=$wrapperId;?> .buttons2{
    margin-left: 320px;
}

#<?=$wrapperId;?> .buttons{
    /*margin-left: 420px;*/
}

#<?=$wrapperId;?> .red{
    color: #b4281b;
    font-size: 21px;

}

#<?=$wrapperId;?> .tab-content_ {
    /*width: 100%;*/
    padding: 10px;
    display: none;
    box-sizing:border-box;

}
#<?=$wrapperId;?> .tab-content_ form input[type='submit'],
#<?=$wrapperId;?> .tab-content_ .pay-button{
    padding: 10px 25px;
    border: none;
    background: #a9d2e0;
    color: #121b67;
    cursor: pointer;
    font-size: 16px;
    border-radius: 2px;
    box-shadow: 2px 2px 2px rgba(0,0,0,0.3);
}
#<?=$wrapperId;?> .tab-content_ form input[type='submit']:hover,
#<?=$wrapperId;?> .tab-content_ .pay-button:hover {
    background:#8dcbe0;
}
#<?=$wrapperId;?> #tabs-container__ > ul > li:nth-child(4){
    border-right: 1px solid #d4d4d1;
}

#<?=$wrapperId;?> #tab-1 {
    /*display: block;*/
}


#<?=$wrapperId;?>.main-container {
    width: <?php if(isset($width) && !empty($width)): ?><?=$width;?><?php if($width != 'auto'){echo('px');} ?>;<?php else: ?>780px;<?php endif; ?>
    max-width: 100%;
    border: 1px solid rgba(0,0,0,0.2);
    box-shadow: 0px 0px 3px rgba(0,0,0,0.2);
    overflow: hidden;
    margin: 0 auto;
    margin-top: 5%;
    font-family: sans-serif;
    color:#393939;
}
#<?=$wrapperId;?> #order-data{
    width: 100%;
    background: rgba(0,0,0,0.05);
    border: 1px solid #cdcdcd;
    border-spacing: 0;
    border-collapse: collapse;
}
#<?=$wrapperId;?> #order-data td {
    border: 1px solid #cdcdcd;
    padding:10px;
}
#<?=$wrapperId;?> #change-ord-data-btn {
    padding: 10px;
    border: none;
    background: white;
    font-size: 15px;
    color: #393939;
    cursor: pointer;
    border: 1px solid rgba(0,0,0,0.4);
    box-shadow: 0px 0px 2px rgba(0,0,0,0.3);
}
#<?=$wrapperId;?> #change-ord-data-btn:hover{
    background: #eee;
}
</style>
<div id='<?=$wrapperId;?>' class="main-container">
    <div id="tabs-container__">
        <?php if ($paySystems && count($paySystems) > 0): ?>
            <?php
                $defaultSystemId = false;
                foreach($paySystems as $item){
                    if((boolean)$item->is_default == true){
                        $defaultSystemId = $item->system_id;
                        reset($paySystems);
                        break;
                    }
                }
            ?>
        <ul class="tabs-menu__">
            <?php foreach ($paySystems as $k => $item): ?>
                <?php
                    $currentClass = $item->system_id == $defaultSystemId ? 'current' : '' ;

                    if ( $defaultSystemId === false && $k == 0)
                        $currentClass = 'current';

                ?>
                <li class="<?php echo($currentClass); ?>" onclick='changePayMethod(event,<?=$item->system_id;?>)'>
                    <a href="#tab-<?php echo $k + 1; ?>">
                        <img src="<?php echo Yii::getAlias('@domain/images/' . $item->system->workname . '.png') ?>">
                    </a>
                    <span class='ajax-loading'></span>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab">
            <?php
                reset($paySystems);
            ?>
            <?php foreach($paySystems as $k => $item): ?>
                <?php
                    $currentStyle = $item->system_id == $defaultSystemId ? 'style="display:block"' : '' ;
                    if ($defaultSystemId === false && $k == 0) {
                        $currentStyle = 'style="display:block"';
                    }

                ?>
                <div id="tab-<?php echo $k + 1; ?>" class="tab-content_" <?php echo($currentStyle); ?>>
                    <!-- start -->
                    <?php if (!empty($model->items)): ?>
                        <table id='order-data' border="0" style="">
                            <?php foreach ($model->items as $mag): ?>
                                <?php if (!empty($mag->subscription)): ?>
                                <tr align="center" valign="middle" style="text-align: center;">
                                    <td align="left" class="lefttd">
                                        Подписка на <?= ($mag->subscription->type == 'paper') ? 'бумажную' : 'электронную' ?> версию журнала <br> <span class="name">«<?= $mag->subscription->periodical->HumanizedName; ?>»</span>
                                        <span class="name-m">
                                            с <?= \Yii::$app->formatter->asDate($mag->subscription->date_start, 'php:F Y'); ?>
                                        </span>,
                                        <span class="red"> <?= $mag->subscription->period ?></span>&nbsp;месяца(ев),
                                        <span class="red"> <?= $mag->subscription->rqty ?></span>&nbsp;выпуска(ов)
                                    </td>
                                    <!--
                                    <td>
                                        <span class="red"> <?= $mag->subscription->period ?></span><br>месяцев
                                    </td>
                                    <td>
                                        <span class="red"> <?= $mag->subscription->rqty ?></span><br>выпуска(ов)
                                    </td>
                                    -->
                                </tr>
                                <?php endif; ?>
                                <!--
                                <br>
                                <span style="font-size: 11px; color: #F5F5F5">debug: прайс
                                    <?php //echo($mag->price); ?>
                                    Заказ #<?php //echo($model->id); ?>
                                </span>
                                -->
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                    <div>
                        <?php $cellStyle = 'style="padding: 7px 10px;border: 1px solid #cdcdcd;"';?>
                        <table border='0' style='width:100%;border: 1px solid #cdcdcd;border-spacing: 0;border-collapse: collapse;margin-top: 15px;'>
                            <tbody>
                                <tr>
                                    <td <?=$cellStyle;?>>Подписчик:</td>
                                    <td <?=$cellStyle;?>>
                                        <?php echo($model->person->buildFullNameString()); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td <?=$cellStyle;?>>Адрес доставки:</td>
                                    <td <?=$cellStyle;?>>
                                        <?php echo($model->address->buildAddressString()); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td <?=$cellStyle;?>>Номер заказа:</td>
                                    <td <?=$cellStyle;?>>
                                        <?php echo($model->id); ?>
                                    </td>
                                </tr>
                                <!--
                                <tr>
                                    <td colspan="2" <?=$cellStyle;?>>
                                        <button id='change-ord-data-btn' type='button' onclick='loadSubscribeForm()'>
                                            Изменить заказ
                                        </button>
                                    </td>
                                </tr>
                                -->
                            </tbody>
                        </table>
                    </div>
                    <div class="sum">Сумма к оплате: <span class="red"><?php echo $model->price ?></span> руб.</div>
                    <div style='border-top: 1px solid #cdcdcd;margin: 10px 0px;'></div>
                    <?php
                    echo ($item->buildPayingForm($model));
                    ?>

                    <!-- end -->
                </div>
            <?php endforeach; ?>
        </div><!-- end tab -->
        <?php endif; ?>

        <div>
            <?php
                $action = $apiHost . '/order/update-pay-system';
            ?>
            <?php $form = ActiveForm::begin(['action' => $action, 'id' => $randStr, 'method' => 'post',]); ?>
            <input type='hidden' name='hash'value='<?=$model->hash;?>' />
            <input type='hidden' name='sId' value='<?=$defaultSystemId;?>' id='<?=$randStr?>_sid' />
            <input type='hidden' name='pId' value='<?=$publisherId;?>' />
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script type='text/javascript' id='allPayMethodsScript'>
    var jqueryExists = (typeof(jQuery) == 'function') ? true : false;
    var form<?=$randStr?>Sendabble = true;
    if (!jqueryExists) {
            var script = document.createElement('script');

            script.type = 'text/javascript';
            script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

            document.getElementsByTagName('head')[0].appendChild(script);

            script.addEventListener('load', runAfterJqueryLoad_apm, false);
    } else {
            runAfterJqueryLoad_apm();
    }
    function runAfterJqueryLoad_apm(){
//        $(document).ready(function() {
//            $(".tabs-menu__ a").click(function(event) {
//                event.preventDefault();
//                $(this).parent().addClass("current");
//                $(this).parent().siblings().removeClass("current");
//                var tab = $(this).attr("href");
//                $(".tab-content_").not(tab).css("display", "none");
//                $(tab).fadeIn();
//            });
//        });
    }
    function changePayMethod(event, sid){
        if (!sid)
            return;

        if (typeof event == 'undefined')
            event = window.event;

        event.preventDefault();
        event.stopPropagation();

        var form = $('form[id="<?=$randStr?>"]');
        var tab = $(event.target);

//        if (tab.context.tagName != 'LI')
        if (tab[0].tagName.toUpperCase() != 'LI')
            tab = tab.closest('li');

        if (form.length == 0 || tab.length == 0)
            return;

        form.find('#<?=$randStr?>_sid').val(sid);
        if ( form<?=$randStr?>Sendabble ) {
            form<?=$randStr?>Sendabble = false;
            tab.addClass('ajax-load');
            $.ajax({
                url : form.attr('action'),
                dataType : 'json',
                type : 'post',
                data : form.serialize(),
            }).done(function(response,b,c){
                form<?=$randStr?>Sendabble = true;
                tab.removeClass('ajax-load');
                if (response.state != 'OK') {
                    alert('something went wrong. ' + response.message);
                    return false;
                } else {
                    tab.addClass("current");
                    tab.siblings().removeClass("current");
                    var tabId = tab.find('a').attr("href");
                    $("#<?=$wrapperId?> .tab-content_").not(tabId).css("display", "none");
                    $('#<?=$wrapperId?> ' + tabId).fadeIn();

                }
                console.log(response);
                console.log(b);
                console.log(c);
            }).fail(function(a,b,c){
                form<?=$randStr?>Sendabble = true;
                tab.removeClass('ajax-load');
                alert('something went wrong. See console.');
                console.log(a);
                console.log(b);
                console.log(c);
            });
        }
    }
</script>
