<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
//Доступные переменные:
// $edition, $index
?>
<li class='selected-mag-item animated' data-index ='<?=$index;?>' data-id='<?=$edition->Message_ID;?>'  style='display:none'>
    <div class='selected-mag-item-content flex-wrapper flex-dir-row flex-multy-rows'>
        <div class='mag-info-block'>
            <input class='magId' type='hidden' name='order[items][<?=$index; ?>][periodical_id]' value='<?=$edition->Message_ID; ?>'/>
            <input type='text' name='order[items][<?=$index; ?>][HumanizedName]' value='<?=$edition->HumanizedName; ?>' readonly='true'/>
        </div>
        <div class='start-date-block'>
            <span class='field-label'>Начало подписки</span>
            <?php 
            echo (Html::dropDownList('order[items][' . $index . '][date_start]', null, $edition->getMonthesList() , []));
            ?>
        </div>
        <div class='duration-block'>
            <span class='field-label'>Период подписки (мес.)</span>
            <?php 
            $periods = [];
            $defaultPeriod = '';
            foreach( $edition->getEditionPeriods()->orderBy('period')->all() as $period ) {
                $periods[$period->period] = $period->period . ' мес.';
                if ($period->default == 1)
                    $defaultPeriod = $period->period;
            }
            echo(Html::dropDownList("order[items][$index][duration]", $defaultPeriod, $periods));
            ?>
        </div>
        <div class='issues-amount-block'>
            <span class='field-label'>Колич. выпусков</span>
            <input type='text' name='order[items][<?=$index; ?>][rqty]' value='' readonly='true'/>
        </div>
        <div class='type-block'>
            <span class='field-label'>Тип</span>
            <div>
                <div class='radio-check-wrap'>
                    <input type='radio' name='order[items][<?=$index; ?>][type]' value='paper' id='qwerty_oi_<?=$index; ?>_t1' checked='true'></input>
                    <label for='qwerty_oi_<?=$index; ?>_t1'>
                        <span>Печатное</span>
                    </label>
                </div>
                <div class='radio-check-wrap'>
                    <input type='radio' name='order[items][<?=$index; ?>][type]' value='pdf' id='qwerty_oi_<?=$index; ?>_t2'></input>
                    <label for='qwerty_oi_<?=$index; ?>_t2'>
                        <span>PDF</span>
                    </label>
                </div>
            </div>
        </div>
        <div class='cost-block'>
            <span class='field-label with-note-under note-under-hidden' data-after=''>Стоимость</span>
            <input type='text' name='order[items][<?=$index; ?>][sum]' value='' readonly='true'/>
            <input type='hidden' name='order[items][<?=$index; ?>][discount]' value='0' />
            <input type='hidden' name='order[items][<?=$index; ?>][price]' value='0' />
        </div>
    </div>
    <div class='remove-item-block transition-all' title='Удалить из заказа'>
        <span class='remove-item'></span>
    </div>
    <div class='go-to-avalabeles transition-all' title='К выбору издания'></div>
    
    <div class='ajax-loading'></div>
</li>


