<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
    $imageFieldValue = (isset($model) && !empty($model) && $model->hasAttribute($imageFieldName)) ? $model->getAttribute($imageFieldName) : null;
    
    $requriedBehaviorIsAttached = false;
    $requiredBehavior = 'ImageUploadBehavior';

    foreach($model->behaviors() as $b) {
        $temp = explode( '\\', $b['class'] );
        $bName = end( $temp );
        if ($bName == $requiredBehavior) {
            $requriedBehaviorIsAttached = true;
            break;
        }
    }
    $temp = explode( '\\', get_class($model) );
    $modelName = end( $temp );
?>
<?php if(!empty($imageFieldValue) && $requriedBehaviorIsAttached): ?>
    <div style='overflow: hidden;padding: 15px; background: rgba(0,0,0,0.1);margin-bottom: 20px;'>
            <?php 
                $name = explode('/', parse_url($imageFieldValue, PHP_URL_PATH));
                $name = end($name);
                $imagePath = $model->getImagePath();
                if(!empty($imagePath)) {
                    $sizes = getimagesize($imagePath);
                    $name .= '(' . $sizes[0] . 'x' . $sizes[1] . ')';
                }
            ?>
            <button type="button" data-toggle="modal" data-target="#<?=$modelName?>_full_image" class='btn btn-flat bg-olive' title='Нажмите для просмотра полноразмерной картинки'>
                <?=$name;?>
            </button>
            <!-- всплывашка для полноразмерной картинки -->
            <div id='<?=$modelName;?>_full_image' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full_<?=$modelName;?>_label">
                <div class="modal-dialog" role="document" style='width:auto;text-align:center'>
                    <div class="modal-content" style='display:inline-block;text-align:initial'>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="full_<?=$modelName;?>_label"><?=$name; ?></h4>
                        </div>
                        <div class="modal-body">
                            <img src="<?=$imageFieldValue;?>">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                echo(
                    yii\helpers\Html::label('Удалить этот файл?', 'delete-file', ['style' => 'margin-left: 15px;font-weight: normal;color: #a54c4c;']) . 
                    yii\helpers\Html::checkbox('delete-file', false, ['id' => 'delete-file', 'value' => 1, 'style' => 'margin-left: 10px;width: 15px;height: 15px;vertical-align: top;']) 
                );  
            ?>
        <?php if(!empty($model->getResizedImages())): ?>
            <div style='overflow: hidden;padding-top: 20px;'>
            <?php foreach($model->getResizedImages() as $ri): ?>
            
                <div class="box box-primary collapsed-box" style='float: left; margin-right: 20px;overflow: hidden;width:auto;min-width: 130px;'>
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?= '(' . $ri['width'] . 'x' . $ri['height'] . ')'; ?>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $imgSrc = \yii\helpers\Url::to($ri['url'], Yii::$app->params['scheme']); ?>
                        <?= yii\helpers\Html::img($imgSrc, []) ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            <?php endforeach; ?>



            </div>
        <?php endif; ?>
    </div>
<?php else: ?>
    <div>
        image fields is empty or behavior ImageUploadBehavior is not attached.
    </div>
<?php endif; ?>
