<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "Edition".
 *
 * @property integer $Message_ID
 * @property integer $User_ID
 * @property integer $Subdivision_ID
 * @property integer $Sub_Class_ID
 * @property integer $Priority
 * @property string $Keyword
 * @property integer $Checked
 * @property string $TimeToDelete
 * @property string $TimeToUncheck
 * @property string $IP
 * @property string $UserAgent
 * @property integer $Parent_Message_ID
 * @property string $Created
 * @property string $LastUpdated
 * @property integer $LastUser_ID
 * @property string $LastIP
 * @property string $LastUserAgent
 * @property string $HumanizedName
 * @property string $EnglishName
 * @property string $full_name
 * @property string $name_for_applications
 * @property string $mask_for_reference
 * @property string $issue_mask
 * @property string $mask_for_task
 * @property integer $data_format
 * @property integer $subscribe
 * @property integer $esubscribe
 * @property integer $deleted
 * @property string $Passport
 * @property double $ArticlePrice
 * @property string $ncTitle
 * @property string $ncKeywords
 * @property string $ncDescription
 * @property integer $closeNum
 * @property integer $titleId
 *
 * @property AdsPlaces[] $adsPlaces
 */
class Magazines extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return  parent::getDbName() . '.Edition';
    }

    public static function getDb() {
        return Yii::$app->db;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Keyword', 'Created', 'LastUser_ID'], 'required'],
            [['User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Priority', 'Checked', 'Parent_Message_ID', 'LastUser_ID', 'data_format', 'subscribe', 'esubscribe', 'deleted', 'closeNum', 'titleId'], 'integer'],
            [['TimeToDelete', 'TimeToUncheck', 'Created', 'LastUpdated'], 'safe'],
            [['Passport', 'ncDescription'], 'string'],
            [['ArticlePrice'], 'number'],
            [['Keyword', 'UserAgent', 'LastUserAgent', 'HumanizedName', 'EnglishName', 'full_name', 'name_for_applications', 'mask_for_reference', 'issue_mask', 'mask_for_task', 'ncTitle', 'ncKeywords'], 'string', 'max' => 255],
            [['IP', 'LastIP'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Message_ID'            => 'Message  ID',
            'User_ID'               => 'User  ID',
            'Subdivision_ID'        => 'Subdivision  ID',
            'Sub_Class_ID'          => 'Sub  Class  ID',
            'Priority'              => 'Priority',
            'Keyword'               => 'Keyword',
            'Checked'               => 'Checked',
            'TimeToDelete'          => 'Time To Delete',
            'TimeToUncheck'         => 'Time To Uncheck',
            'IP'                    => 'Ip',
            'UserAgent'             => 'User Agent',
            'Parent_Message_ID'     => 'Parent  Message  ID',
            'Created'               => 'Created',
            'LastUpdated'           => 'Last Updated',
            'LastUser_ID'           => 'Last User  ID',
            'LastIP'                => 'Last Ip',
            'LastUserAgent'         => 'Last User Agent',
            'HumanizedName'         => 'Humanized Name',
            'EnglishName'           => 'English Name',
            'full_name'             => 'Full Name',
            'name_for_applications' => 'Name For Applications',
            'mask_for_reference'    => 'Mask For Reference',
            'issue_mask'            => 'Issue Mask',
            'mask_for_task'         => 'Mask For Task',
            'data_format'           => 'Data Format',
            'subscribe'             => 'Subscribe',
            'esubscribe'            => 'Esubscribe',
            'deleted'               => 'Deleted',
            'Passport'              => 'Passport',
            'ArticlePrice'          => 'Article Price',
            'ncTitle'               => 'Nc Title',
            'ncKeywords'            => 'Nc Keywords',
            'ncDescription'         => 'Nc Description',
            'closeNum'              => 'Количество закрытых номеров',
            'titleId'               => 'Title ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdsPlaces()
    {
        return $this->hasMany(AdvertsPlaces::className(), ['magazine_id' => 'Message_ID'])->inverseOf('magazine');
    }
}
