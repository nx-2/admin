<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 if (isset($message)):?>
    <div style="background: white">
        <span>Адрес отправителя: </span><strong><?php echo($message->from); ?></strong><br>
        <span>Адрес получателя: </span><strong><?php echo($message->to); ?></strong><br>
        <span>Тема письма: </span><strong><?php echo($message->subject); ?></strong><br>
        <span>Дата: </span><strong><?php echo($message->time); ?></strong><br>
    </div>
    <h4 class="modal-title" id="email-message-label" style="padding-bottom: 15px;background: white">Содержание письма</h4>
    <div>
        <?php echo($message->body); ?>
    </div>
<?php endif; ?>
