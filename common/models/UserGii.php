<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "User".
 *
 * @property integer $User_ID
 * @property string $Password
 * @property integer $Checked
 * @property integer $PermissionGroup_ID
 * @property string $Language
 * @property string $Created
 * @property string $LastUpdated
 * @property string $Email
 * @property string $Login
 * @property string $FullName
 * @property string $LastName
 * @property string $FirstName
 * @property string $MidName
 * @property integer $Confirmed
 * @property string $RegistrationCode
 * @property string $Keyword
 * @property integer $isPartner
 * @property string $Auth_Hash
 * @property integer $SubscribeStatus
 * @property integer $UserStatus
 * @property string $company
 * @property string $job
 * @property string $phone
 * @property integer $country
 * @property string $city
 * @property string $address
 * @property string $street
 * @property string $house
 * @property string $building
 * @property string $flat
 * @property string $url
 * @property integer $company_profile
 * @property integer $sex
 * @property string $birthday
 * @property string $phone_prefix
 * @property string $fax_prefix
 * @property string $fax
 * @property integer $region
 * @property string $district
 * @property string $index_num
 * @property integer $open_email
 * @property string $city_other
 * @property integer $old_id
 * @property string $avatar
 * @property string $UserType
 * @property string $ForumSignature
 * @property string $ForumAvatar
 * @property string $Text
 * @property string $LastLoginDate
 * @property integer $publisher_id
 *
 * @property ClassificatorCountry $country0
 * @property Publisher $publisher
 * @property UserGroup[] $userGroups
 * @property NxUserRole[] $nxUserRoles
 * @property NxRole[] $roles
 */
class UserGii extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Checked', 'PermissionGroup_ID', 'Confirmed', 'isPartner', 'SubscribeStatus', 'UserStatus', 'country', 'company_profile', 'sex', 'region', 'open_email', 'old_id', 'publisher_id'], 'integer'],
            [['Created', 'LastUpdated', 'birthday', 'LastLoginDate'], 'safe'],
            [['Auth_Hash', 'address', 'UserType', 'Text'], 'string'],
            [['Password'], 'string', 'max' => 45],
            [['Language', 'Email', 'Login', 'FullName', 'LastName', 'FirstName', 'RegistrationCode', 'Keyword', 'company', 'job', 'phone', 'city', 'url', 'phone_prefix', 'fax_prefix', 'fax', 'index_num', 'city_other', 'avatar', 'ForumSignature', 'ForumAvatar'], 'string', 'max' => 255],
            [['MidName'], 'string', 'max' => 50],
            [['street', 'house', 'building', 'flat', 'district'], 'string', 'max' => 64],
            [['country'], 'exist', 'skipOnError' => true, 'targetClass' => ClassificatorCountry::className(), 'targetAttribute' => ['country' => 'Country_ID']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_ID' => 'User  ID',
            'Password' => 'Password',
            'Checked' => 'Checked',
            'PermissionGroup_ID' => 'Permission Group  ID',
            'Language' => 'Language',
            'Created' => 'Created',
            'LastUpdated' => 'Last Updated',
            'Email' => 'Email',
            'Login' => 'Login',
            'FullName' => 'Full Name',
            'LastName' => 'Last Name',
            'FirstName' => 'First Name',
            'MidName' => 'Mid Name',
            'Confirmed' => 'Confirmed',
            'RegistrationCode' => 'Registration Code',
            'Keyword' => 'Keyword',
            'isPartner' => 'Is Partner',
            'Auth_Hash' => 'Auth  Hash',
            'SubscribeStatus' => 'Subscribe Status',
            'UserStatus' => 'User Status',
            'company' => 'Company',
            'job' => 'Job',
            'phone' => 'Phone',
            'country' => 'Country',
            'city' => 'City',
            'address' => 'Address',
            'street' => 'Street',
            'house' => 'House',
            'building' => 'Building',
            'flat' => 'Flat',
            'url' => 'Url',
            'company_profile' => 'Company Profile',
            'sex' => 'Sex',
            'birthday' => 'Birthday',
            'phone_prefix' => 'Phone Prefix',
            'fax_prefix' => 'Fax Prefix',
            'fax' => 'Fax',
            'region' => 'Region',
            'district' => 'District',
            'index_num' => 'Index Num',
            'open_email' => 'Open Email',
            'city_other' => 'City Other',
            'old_id' => 'Old ID',
            'avatar' => 'Avatar',
            'UserType' => 'User Type',
            'ForumSignature' => 'Forum Signature',
            'ForumAvatar' => 'Forum Avatar',
            'Text' => 'Text',
            'LastLoginDate' => 'Last Login Date',
            'publisher_id' => 'Publisher ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(ClassificatorCountry::className(), ['Country_ID' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        return $this->hasMany(UserGroup::className(), ['User_ID' => 'User_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxUserRoles()
    {
        return $this->hasMany(NxUserRole::className(), ['user_id' => 'User_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(NxRole::className(), ['id' => 'role_id'])->viaTable('nx_user_role', ['user_id' => 'User_ID']);
    }
}
