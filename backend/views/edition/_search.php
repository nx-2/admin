<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\EditionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="edition-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Message_ID') ?>

    <?= $form->field($model, 'Checked') ?>

    <?= $form->field($model, 'fullNameEn') ?>

    <?= $form->field($model, 'TimeToDelete') ?>

    <?= $form->field($model, 'TimeToUncheck') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'create_user_id') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <?php // echo $form->field($model, 'start_subscription') ?>

    <?php // echo $form->field($model, 'name_for_applications') ?>

    <?php // echo $form->field($model, 'last_user_id') ?>

    <?php // echo $form->field($model, 'HumanizedName') ?>

    <?php // echo $form->field($model, 'data_format') ?>

    <?php // echo $form->field($model, 'EnglishName') ?>

    <?php // echo $form->field($model, 'full_name') ?>

    <?php // echo $form->field($model, 'mask_for_reference') ?>

    <?php // echo $form->field($model, 'issue_mask') ?>

    <?php // echo $form->field($model, 'ncDescriptionEn') ?>

    <?php // echo $form->field($model, 'subscribe') ?>

    <?php // echo $form->field($model, 'esubscribe') ?>

    <?php // echo $form->field($model, 'ncDescription') ?>

    <?php // echo $form->field($model, 'User_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
