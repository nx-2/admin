<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PublisherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Html;

switch ($action) {
    case 'list':
        $this->title = 'Издатели';
        break;
    case 'create':
        $this->title = 'Новый издатель';
        break;
    case 'update':
//        $this->title = 'Изменить издателя "' . $model->name . '"';
        $this->title = 'Редактирование профиля издателя "' . $model->name . '"';
        
        break;
}
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 style='display:inline-block;'>
    <?= Html::encode($this->title); ?>
</h1>
<div class="nav-tabs-custom">
    <?php if(Yii::$app->user->identity->isRole(['admin'])):?> 
    <ul class="nav nav-tabs pull-right">
        <li <?php if($action == 'list'){echo('class="active"');} ?>><a href="/publisher/list" data-toggle_="tab" aria-expanded="true">Список</a></li>
        <li <?php if($action == 'create'){echo('class="active"');} ?>><a href="/publisher/create" data-toggle_="tab" aria-expanded="false">Новый</a></li>
<!--        
        <li class="pull-left header">
        </li>
-->
    </ul>
    <?php endif; ?>
        <div class="tab-pane active" id="active" style='overflow: hidden;'>
            <?php if($action == 'list'): ?>    
                <div class='col-md-12 col-sm-12 col-xs-12'>
                    <?=$this->render('_list', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
                </div>
            <?php elseif($action == 'create' || $action == 'update'): ?>
                <?=$this->render('_form', ['model' => $model,'titleText' => '']); ?>
            <?php endif; ?>
        </div>
<!-- 
    <div class="tab-content">
    </div>
-->
  </div>
