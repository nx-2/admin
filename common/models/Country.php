<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "Classificator_Country".
 *
 * @property integer $Country_ID
 * @property string $Country_Name
 * @property integer $Country_Priority
 * @property string $Value
 * @property integer $Checked
 *
 * @property User[] $users
 * @property NxAddress[] $nxAddresses
 * @property NxZoneCountry[] $nxZoneCountries
 * @property NxZone[] $zones
 */
class Country extends \common\models\CommonModel
{
    const RUSSIA = 165;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Classificator_Country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Country_Priority', 'Checked'], 'integer'],
            [['Value'], 'string'],
            [['Country_Name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Country_ID' => 'ID',
            'Country_Name' => 'Название',
            'Country_Priority' => 'Код',
            'Value' => 'Значение',
            'Checked' => 'Вкл./Выкл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['country' => 'Country_ID'])->inverseOf('country0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxAddresses()
    {
        return $this->hasMany(NxAddress::className(), ['country_id' => 'Country_ID'])->inverseOf('country');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxZoneCountries()
    {
        return $this->hasMany(NxZoneCountry::className(), ['country_id' => 'Country_ID'])->inverseOf('country');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZones()
    {
        return $this->hasMany(NxZone::className(), ['id' => 'zone_id'])->viaTable('nx_zone_country', ['country_id' => 'Country_ID']);
    }
}
