<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Edition;

/**
 * EditionSearch represents the model behind the search form about `common\models\Edition`.
 */
class EditionSearch extends Edition
{

    public $owner;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Message_ID', 'Checked', 'create_user_id', 'last_user_id', 'subscribe', 'esubscribe', 'User_ID'], 'integer'],
            [['fullNameEn', 'TimeToDelete', 'TimeToUncheck', 'created', 'last_updated', 'start_subscription', 'name_for_applications', 'HumanizedName', 'data_format', 'EnglishName', 'full_name', 'mask_for_reference', 'issue_mask', 'ncDescriptionEn', 'ncDescription','owner'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Edition::find();
        $query->joinWith(['publisherEditions']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Message_ID' => $this->Message_ID,
            'Checked' => $this->Checked,
            'TimeToDelete' => $this->TimeToDelete,
            'TimeToUncheck' => $this->TimeToUncheck,
            'created' => $this->created,
            'create_user_id' => $this->create_user_id,
            'last_updated' => $this->last_updated,
            'start_subscription' => $this->start_subscription,
            'last_user_id' => $this->last_user_id,
            'subscribe' => $this->subscribe,
            'esubscribe' => $this->esubscribe,
            'User_ID' => $this->User_ID,
        ]);

        $query->andFilterWhere(['like', 'fullNameEn', $this->fullNameEn])
            ->andFilterWhere(['like', 'name_for_applications', $this->name_for_applications])
            ->andFilterWhere(['like', 'HumanizedName', $this->HumanizedName])
            ->andFilterWhere(['like', 'data_format', $this->data_format])
            ->andFilterWhere(['like', 'EnglishName', $this->EnglishName])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'mask_for_reference', $this->mask_for_reference])
            ->andFilterWhere(['like', 'issue_mask', $this->issue_mask])
            ->andFilterWhere(['like', 'ncDescriptionEn', $this->ncDescriptionEn])
            ->andFilterWhere(['like', 'ncDescription', $this->ncDescription])
            ->andFilterWhere(['like', 'publisher_editions.publisher_id', Yii::$app->user->identity->publisher_id]);
        if($this->owner)
            $query->andFilterWhere(['like', 'publisher_editions.owner', $this->owner]);

        return $dataProvider;
    }
}
