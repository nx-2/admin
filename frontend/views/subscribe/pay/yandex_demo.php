<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
    // Following varables are avalable here: $system (instance of common\models\RelationSystems) and $order (instance of \common\models\NxOrder)
    // param_1 = shopId param_2 = scid param3 = secretWord
?>

<div style='overflow: hidden;'>
    <div style='float:left;'>
        <?php 
        $host = yii::$app->params['api']['host'];
        $images = [
//            $host . '/images/visa_mastercard.png',
            $host . '/images/visa_mastercard_mir.png',
            $host . '/images/yooMoney.png',
//            $host . '/images/ya_money_logo.png',
//            $host . '/images/QIWI_logo.jpg',
//            $host . '/images/cash_logo.png',
//            $host . '/images/alfa_logo.png',
//            $host . '/images/psb_logo.jpg',
        ];
        ?>
        <?php foreach($images as $i): ?>
        <img src='<?=$i;?>' style='max-width:100px;display: inline;vertical-align: top;'/>
        <?php endforeach; ?>
    </div>
    <div style='float: right;padding: 5px;'>
        <form action="https://demomoney.yandex.ru/eshop.xml" method="post">
            <!-- Обязательные поля -->
            <input name="paymentType" value="" type="hidden">
            <input name="shopId" value="<?=$system->param_1; ?>" type="hidden"/>
            <input name="scid" value="<?=$system->param_2; ?>" type="hidden"/>
            <input name="sum" type="hidden" value="<?=$order->price;?>"/>
            <input name="customerNumber" type="hidden" value="<?=$order->person_id;?>"/>
            <input name="orderNumber" type="hidden" value="<?=$order->id;?>"/>
            <input name="cps_phone" type="hidden" value="<?=$order->person->phone;?>"/>
            <input name="cps_email" type="hidden" value="<?=$order->person->email;?>"/>
            <input name="publisherId" type="hidden" value="<?=$publisherId;?>"/>
            <input type="submit" value="Оплатить"/>
        </form>
    </div>
</div>
