<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\events;

/**
 * Description of ARattrChangedEvent
 *
 * @author andrey
 */
class ARsingleAttrChangedEvent extends \yii\base\Event {
//    public $attrArray=[];
    public $attrName;
    public $oldValue;
    public $newValue;
//    public $changedAttributes;

}
