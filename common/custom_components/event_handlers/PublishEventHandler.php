<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\custom_components\event_handlers;
use common\models\User;
use Yii;
use yii\base\BaseObject;
use yii\base\ExitException;


class PublishEventHandler extends BaseObject
{
    public static function newPublishCreated (\yii\base\Event $event): void
    {
        // TODO: handle event code here
        // Здесь нужно новому издателю например попривязывать ко всем событиям соответствующие шаблоны уведомлений из списка по умолчанию
        // Может быть отправить какое то письмо. И т.д. и т.п
        return;
    }

    private static function sendEmail($data, $publisherId): void
    {
        $job = new \common\queue_jobs\EmailNotificationJob($publisherId, $data);
        if ($job->validate()) {
            $queue = \Yii::$app->queue;
            $queue->push($job);
//            $job->execute($queue);
        } else {
            $errAttrs = array_keys($job->getErrors());
            $errMessage = 'incorrect data was passed to EmailNotificationJob model. There are errors in attributes: ' . implode(',', $errAttrs);
            Yii::$app->session->addFlash('danger', $errMessage);
        }
    }

}