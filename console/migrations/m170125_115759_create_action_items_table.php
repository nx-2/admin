<?php

use yii\db\Migration;

/**
 * Handles the creation of table `action_items`.
 */
class m170125_115759_create_action_items_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = $this->db->schema->getTableNames();

        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('Action_items', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%Action_items}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'action_id' => 'int(11) NOT NULL',
                    'mag_id' => 'int(11) NOT NULL',
                    'months' => 'int(11) NOT NULL',
                    'type' => 'varchar(45) NOT NULL',
                    'discount' => 'int(11) NOT NULL',
                ], $tableOptions_mysql);
            }
        }

        $this->createIndex(
            'action_id',
            'Action_items',
            'action_id'
        );

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('action_items_ibfk_1','{{%Action_items}}', 'action_id', 'osp.Action', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->dropTable('Action_items');
        $this->execute('SET foreign_key_checks = 1;');
    }
}
