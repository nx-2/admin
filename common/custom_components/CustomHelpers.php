<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components;

use common\models\Publisher;
use common\models\PublisherProperty;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;

class CustomHelpers extends Component
{
    public static function transliterate($text = '') 
    {
        if (empty($text))
            return '';

        $text = mb_strtolower($text, 'UTF-8');
    
        $tr = array('а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g',
            'д'=>'d', 'е'=>'e', 'ё'=>'e', 'ж'=>'j',
            'з'=>'z', 'и'=>'i', 'й'=>'i', 'к'=>'k', 
            'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 
            'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t',
            'у'=>'u', 'ф'=>'f', 'х'=>'kh', 'ц'=>'ts', 
            'ч'=>'ch', 'ш'=>'sh', 'щ'=>'shch', 'ы'=>'y', 
            'ь'=>'', 'э'=>'e', 'ъ'=>'', 'ю'=>'u', 
            'я'=>'ya', ' '=>'-', '_'=>'-');

        $text = strtr($text, $tr);
        
        $text = preg_replace('#[^-a-z0-9]+#is', '', $text);

        $text = trim($text, "-");
        
        $text = preg_replace('#[-]{2,}#is','-',$text);

        return $text;

    } 
    public static function arrayToAttrs($arr = [])
    {
        if (!is_array($arr) || empty($arr))
            return '';

        $attributes = array_map(function($value, $key) {
            return $key.'="'.$value.'"';
        }, array_values($arr), array_keys($arr));
        
        return $attributes = implode(' ', $attributes);
    }   
    
    public static function curlPostRequest ( $params, $url = false ) 
    {
        if ( !$url ) 
            return false;
        
//        if (empty($params['apikey'])) {
//            $apiKey = isset(Yii::$app->params['nxApiKey']) ? Yii::$app->params['nxApiKey'] : false;
//            $params['apikey'] = !$apiKey ?  md5('e;zol]pt9') : md5($apiKey);
//        }
        
        $params = http_build_query($params);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Referer: ' . (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'invisible man')));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    public static function requestApi($url = false, $method = 'get', $data = [], $asJson = true)
    {
        if (!$url) 
            return false;

        if ($method == 'post' && empty($data))
            return false;

        $apiHost = Yii::$app->params['api']['host'];
        $address = $apiHost . $url;
        
        $httpClient = new Client();

        $request = $httpClient->createRequest()
            ->setMethod($method)
            ->setUrl($address)
            ->setData($data);
        
        $headers = [];
        if ($asJson) {
            $headers['Accept'] = 'application/json';
            $request->setFormat(Client::FORMAT_JSON);
        } else {
            $headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        }
        $headers['User-Agent'] = 'publisher`s admin (www.osp.ru)';
        $request->setHeaders($headers);
        
        $response = $request->send();
        $r = [];
        $r['headers'] = $response->getHeaders();
        $r['isOk'] = $response->isOk;
        if ($response->isOk) {
            if ( strpos($response->getHeaders()->get('content-type'), 'application/json') !== false  ) {
                $r['isJson'] = true; 
//                $r['content'] = json_decode($response->content, true);
            } else {
                $r['isJson'] = false; 
//                $r['content'] = $response->content;
            }
            $r['content'] = $response->content;
        } 
        
        return $r;
    }

    /**
     * @throws InvalidConfigException
     */
    public static function configureMailerComponent(int $publisherId): void
    {
        $publisher = Publisher::findOne(['id' => $publisherId]);

        if (empty($publisher)) {
            return;
        }

        $mailerConfigs = [
            'class' => yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
        ];

        $transportOptionsModels = $publisher->mailOptions;
        $availableTransportKeys = ['host', 'port', 'password', 'encryption', 'username'];
        $transportOptions = array_reduce($transportOptionsModels, function ($res, $propertyModel) use ($availableTransportKeys) {
            /** @var PublisherProperty $propertyModel */
            $optionName = $propertyModel->propertyType->key;
            $optionValue = $propertyModel->value;
            if (in_array($optionName, $availableTransportKeys) && !empty($optionValue)) {
                $res[$optionName] = $optionValue;
            }
            return $res;
        }, []);
        $transportOptions['constructArgs'] = [$transportOptions['host'] ?? '', $transportOptions['port'] ?? ''];
//        $transportOptions['class'] = 'Swift_SmtpTransport';
        $transportOptions['class'] = \Swift_SmtpTransport::class;

        $mailerConfigs['transport'] = $transportOptions;
        Yii::$app->set('mailer', $mailerConfigs);
    }
}