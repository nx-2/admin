<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "relation_systems".
 *
 * @property integer $id
 * @property integer $system_id
 * @property integer $publisher_id
 * @property string $param_1
 * @property string $param_2
 * @property string $param_3
 * @property string $param_4
 * @property string $param_5
 * @property boolean $is_default
 * @property boolean $enabled
 * @property boolean $send_data_for_FZ54
 */
class RelationSystems extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_systems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['system_id', 'publisher_id'], 'integer'],
            [['system_id', 'publisher_id'], 'required'],
            ['system_id', 'unique', 'targetAttribute' => ['publisher_id', 'system_id'],'message' => 'Такая платежная система уже существует'],
            [['param_1', 'param_2', 'param_3','param_4','param_5'], 'string', 'max' => 255],
            [['enabled','is_default','send_data_for_FZ54'],'boolean'],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
            [['system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaySystems::className(), 'targetAttribute' => ['system_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'system_id' => 'Платежная система',
            'publisher_id' => 'Издатель',
            'param_1' => 'Параметр 1 (id магазина)',
            'param_2' => 'Параметр 2 (id витрины)',
            'param_3' => 'Параметр 3(Секретное слово)',
            'param_4' => 'Параметр 4(...)',
            'param_5' => 'Параметр 5(...)',
            'is_default' => 'По умолчанию',
            'enabled' => 'Активен',
            'system' => 'Платежная система',
            'send_data_for_FZ54' => 'Отправлять данные заказа для регистрации чека по 54-ФЗ?',
        ];
    }


//    public function getSystems()
    public function getSystem()
    {
        return $this->hasOne(PaySystems::className(), ['id' => 'system_id']);
    }
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(),['id' => 'publisher_id']);
    }
    /** 
     * @inheritdoc
     * @param mixed (int|\common\models\NxOrder) $order - order Id or instance of NxOrder class
     * @return String
     * returns html code of paying form for this system and for concrete order
    **/
    public function buildPayingForm($order = false, $publisherId = false)
    {
        if (!$order || $this->id == null)
            return 'not enough parameters. Order required and relation system must exists.';
       
        if (!($order instanceof \common\models\NxOrder)) {
            $order = intval($order);
            if (!is_int($order))
                return 'order should be integer or instance of NxOrder Class';
            $order = \common\models\NxOrder::findOne($order);
        }
        
        if ($order == null || $order->id == null)
            return 'order was not found';
        
        if (!$publisherId) 
            $publisherId = $order->publisher->id;
        
        
        $viewName = '/subscribe/pay/' . trim(mb_strtolower($this->system->workname));
        return Yii::$app->view->render($viewName, ['system' => $this, 'order' => $order, 'publisherId' => $publisherId]);
    }
//    public function beforeSave($insert) {
//        if (!parent::beforeSave($insert)) {
//            return false;
//        }
//        
//        return true;
//        
//    }
    
    public function afterSave($insert, $changedAttributes) {
        if(array_key_exists('is_default', $changedAttributes)) {
            $currentDefaultId = $this->id;
            $publisherId = Yii::$app->user->identity->publisher_id;
            $condition = 'publisher_id = ' . $publisherId . ' AND is_default = 1 AND id <> ' . $currentDefaultId;
            Yii::$app->db->createCommand()->update($this->tableName(), ['is_default' => 0], $condition)->execute();
        } 
        if(array_key_exists('enabled', $changedAttributes) && $this->enabled == 0 && $this->is_default == 1) {
            $currentDisabledId = $this->id;
            $publisherId = Yii::$app->user->identity->publisher_id;
            Yii::$app->db->createCommand()->update($this->tableName(), ['is_default' => 0] , 'id = ' . $currentDisabledId)->execute();
            $this->is_default = 0;
            $queryStr = 'SELECT id FROM ' . $this->tableName() . ' WHERE publisher_id = ' . $publisherId . ' AND enabled = 1';
            $newDefaultId = Yii::$app->db->createCommand($queryStr)->queryOne();
            if ($newDefaultId !== false) {
                $newDefaultId = $newDefaultId['id'];
                Yii::$app->db->createCommand()->update($this->tableName(), ['is_default' => 1], 'id = ' . $newDefaultId)->query();
            }
            

        }
        
        parent::afterSave($insert, $changedAttributes);
    }

}
