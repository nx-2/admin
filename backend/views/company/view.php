<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'source_id',
            'channel_id',
            'org_form',
            'address_id',
            'legal_address_id',
            'contact_person_id',
            'email:email',
            'url:url',
            'description:ntext',
            'shortname',
            'othernames',
            'wrongnames',
            'sorthash',
            'xpressid',
            'inn',
            'okonh',
            'okpo',
            'pay_account',
            'corr_account',
            'kpp',
            'bik',
            'bank',
            'checked',
            'enabled',
            'ws1c_synced',
            'created',
            'last_updated',
            'create_user_id',
            'last_user_id',
            'label',
        ],
    ]) ?>

</div>
