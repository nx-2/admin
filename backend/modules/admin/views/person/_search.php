<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NxPersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nx-person-search">

    <?php $form = ActiveForm::begin([
        'action' => ['person/index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'company_id') ?>

    <?= $form->field($model, 'position') ?>

    <?= $form->field($model, 'position_type_id') ?>

    <?= $form->field($model, 'speciality') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'telcode') ?>

    <?php // echo $form->field($model, 'address_id') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'channel_id') ?>

    <?php // echo $form->field($model, 'source_id') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'f') ?>

    <?php // echo $form->field($model, 'i') ?>

    <?php // echo $form->field($model, 'o') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'checked') ?>

    <?php // echo $form->field($model, 'enabled') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <?php // echo $form->field($model, 'create_user_id') ?>

    <?php // echo $form->field($model, 'last_user_id') ?>

    <?php // echo $form->field($model, 'label') ?>

    <?php // echo $form->field($model, 'xpress_id') ?>

    <?php // echo $form->field($model, 'xpress_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
