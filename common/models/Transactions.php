<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $shopid
 * @property string $transactionid
 * @property integer $system
 * @property double $sum
 * @property string $description
 * @property string $person
 * @property integer $personid
 * @property string $email
 * @property string $date
 * @property string $sys_date_1
 * @property string $sys_date_2
 * @property integer $sys_status
 * @property string $shop_date
 * @property integer $shop_status
 * @property string $last_check
 * @property integer $buh_status
 * @property string $source
 */
class Transactions extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shopid', 'system', 'personid', 'sys_status', 'shop_status', 'transactionid','buh_status'], 'integer'],
            [['sum'], 'number'],
            [['date', 'sys_date_1', 'sys_date_2', 'shop_date', 'last_check','source'], 'safe'],
            [['email'], 'string', 'max' => 40],
            [['description'], 'string', 'max' => 4000],
            [['person'], 'string', 'max' => 200],
            [['source'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shopid' => 'Shopid',
            'transactionid' => 'Transactionid',
            'system' => 'System',
            'sum' => 'Sum',
            'description' => 'Description',
            'person' => 'Person',
            'personid' => 'Personid',
            'email' => 'Email',
            'date' => 'Date',
            'sys_date_1' => 'Sys Date 1',
            'sys_date_2' => 'Sys Date 2',
            'sys_status' => 'Sys Status',
            'shop_date' => 'Shop Date',
            'shop_status' => 'Shop Status',
            'last_check' => 'Last Check',
            'buh_status' => 'Buh Status',
            'source' => 'Source',
        ];
    }

    /**
     * Save transaction
     * @param $model
     * @return Transactions
     * @throws \yii\web\HttpException
     */
    static function setTransaction($model = false)
    {
        // here $mpodel is \common\models\NxOrder class
        if (!$model)
            throw new \yii\web\BadRequestHttpException('instance of NxOrder required to create new transaction.');
        
        $person=\common\models\NxPerson::find()->where(['id' => $model->person_id])->one();
        $defaultPaySystem = $model->publisher->defaultPaySystem;
        
        if ( $defaultPaySystem == null ) {
            $defaultPaySystem = \common\models\RelationSystems::findOne(['publisher_id' => $model->publisher->id, 'enabled' => 1]);
        } 
        $defaultPaySystem = $defaultPaySystem->system_id;
        
        
        $transaction = new self();
        $transaction->shopid = 4;// подписка (не покупка одиночного)
        $transaction->transactionid = $model->id;
        $transaction->system = $defaultPaySystem;// 2 = yandex деньги 8 = yandex касса демо
        $transaction->sum = $model->price;
        $transaction->description = 'Оплата заказа  №'.$model->id;
        $transaction->person = $person->name;
        $transaction->personid = $person->id;
        $transaction->email = $person->email;
        $transaction->date =  date('Y-m-d H:i:s');
        $transaction->source=' ';

        if (!$transaction->save()) {
//            throw  new \yii\web\HttpException(200 ,'Error transaction save');
            return false;
        }
        return $transaction;
    }


    /**
     * Ger order transactionid
     * @param $id
     * @return bool
     */
    static function getTransaction($id){
        if(!$id)
            return false;

        return self::find()->where(['transactionid'=>$id])->orderBy(['id' => SORT_DESC])->one();
    }


    /**
     * Ger id transaction
     * @param $id
     * @return bool
     */
    static function getTransactionId($id){
        if(!$id)
            return false;

        return self::find()->where(['id'=>$id])->one();
    }
}
