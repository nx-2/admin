<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace backend\modules\admin;
use Yii;
use yii\filters\AccessControl;


/**
 * adminadmin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // незалогиненым (гостям) разрешить только страницу входа и страницу ошибки
                    [
                        'controllers' => ['site'],
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    //Админам можно все
                    [
                        // 'controllers' => ['publisher'],
                        'allow' => true,
                        'roles' => ['admin'],
//                        'matchCallback' => function ($rule, $action) {
//                            $allowedRoles = ['admin'];
//                            $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                            return (boolean)$allowed;
//                        },
                    ],

                    //залогиненым разрешить все
                    // [
                    //     // 'actions' => ['logout', 'index', 'test'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                ],
            ],
        ];
    }



}
