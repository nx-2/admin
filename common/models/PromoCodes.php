<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "Promo_codes".
 *
 * @property integer $id
 * @property integer $action_id
 * @property string $code
 *
 * @property Action $action
 */
class PromoCodes extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Promo_codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'code'], 'required'],
            [['action_id'], 'integer'],
            [['code'], 'string', 'max' => 45],
            [['code'], 'unique', 'message' => 'Такой код уже существует'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }


    public function getItems()
    {
        return $this->hasMany(ActionItems::className(), ['action_id' => 'action_id']);
    }
    
    public static function repealPromoCode( $promoCode = false, $orderId = false ) 
    {
//        Списать промокод
        if ( !$promoCode || !$orderId )
            return false;
        $promo = self::findOne(['code' => $promoCode]);
        
        if ( $promo == null )
            return true;
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            $oldPromo = new PromoCodesOld();
            $oldPromo->code = $promo->code;
            $oldPromo->action_id = $promo->action_id;
            $oldPromo->order_id = intval($orderId);
            $oldPromo->save();
            $promo->delete();
            $transaction->commit();
            return true;
        } catch ( \Exception $e ) {
            $transaction->rollBack();
            return false;
        }
       

    }



}
