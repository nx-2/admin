<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    // require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php')
    // require(__DIR__ . '/../config/main-local.php')
);

if (file_exists(__DIR__ . '/../../common/config/main-local.php')) {
	$config = yii\helpers\ArrayHelper::merge(
		$config, 
    	require(__DIR__ . '/../../common/config/main-local.php')
	);	
}

if (file_exists(__DIR__ . '/../config/main-local.php')) {
	$config = yii\helpers\ArrayHelper::merge(
		$config, 
    	require(__DIR__ . '/../config/main-local.php')
	);	
}

(new yii\web\Application($config))->run();
