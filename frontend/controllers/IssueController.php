<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\Issue;



class IssueController extends ActiveController
{
    public $modelClass = 'common\models\Issue';

    public function actionIndex()
    {
        $query = Issue::find();
//        $query->joinWith(['legalAddress']);
//        $query->joinWith(['contactPerson']);
//        $query->joinWith(['address']);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    /**
    * 
    **/
    public function actionCount($magId = false, $monthes = false)
    {
        $startDate = Yii::$app->request->post('startDate', false);
        $result = \common\models\Edition::getIssuesCount(['startDate' => $startDate, 'magId' => $magId, 'monthes' => $monthes]);
        
//        if (!$magId || !$monthes || !$startDate)
//            throw new \yii\web\BadRequestHttpException('mag Id, period and startdate are required');
//        
//        $d = \DateTime::createFromFormat('Y-m-d', $startDate);
//        if(!($d && $d->format('Y-m-d') === $startDate))
//            throw new \yii\web\BadRequestHttpException('startDate should be in format YYYY-MM-DD');
//        
//        $result = \Yii::$app->db->createCommand("CALL issues_count(:startDate, :monthes, :magId)") 
//                      ->bindValue(':startDate' , $startDate )
//                      ->bindValue(':monthes', $monthes)
//                      ->bindValue(':magId', $magId)
//                      ->queryOne();

        header('Access-Control-Allow-Origin: *');
        return $result;
    }
//    public function actionTestxml() 
//    {
//        if (!Yii::$app->request->isPost) 
//            throw new \yii\web\BadRequestHttpException('post reqi=uests only');
//        
//        header('Access-Control-Allow-Origin: *');
//        Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
//        $answer = ['key1' => 'value', 'key2' => 'value2', 'key3' => 'value3'];
//        return $answer;
//    }
}

//publisher_id:36
//subscriber_type_id:1
//magazineID:13
//delivery_type_id:1
