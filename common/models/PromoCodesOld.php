<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "Promo_codes_old".
 *
 * @property integer $id
 * @property integer $action_id
 * @property string $code
 * @property integer $order_id
 *
 * @property Action $action
 */
class PromoCodesOld extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Promo_codes_old';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'code','order_id'], 'required'],
            [['action_id','order_id'], 'integer'],
            [['code'], 'string', 'max' => 45],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Action ID',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }
}
