<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_zone".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enabled
 *
 * @property NxPriceItem[] $nxPriceItems
 * @property NxZoneCountry[] $nxZoneCountries
 * @property ClassificatorCountry[] $countries
 * @property NxZoneDelivery[] $nxZoneDeliveries
 * @property NxDeliveryType[] $deliveryTypes
 */
class DeliveryZone extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название зоны',
            'enabled' => 'Активна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPriceItems()
    {
        return $this->hasMany(NxPriceItem::className(), ['zone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxZoneCountries()
    {
        return $this->hasMany(NxZoneCountry::className(), ['zone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(ClassificatorCountry::className(), ['Country_ID' => 'country_id'])->viaTable('nx_zone_country', ['zone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxZoneDeliveries()
    {
        return $this->hasMany(NxZoneDelivery::className(), ['zone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryTypes()
    {
        return $this->hasMany(DeliveryType::className(), ['id' => 'delivery_type_id'])
                ->viaTable('nx_zone_delivery', ['zone_id' => 'id']);
//                ->where(['enabled' => 1]);
    }
}
