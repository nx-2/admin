<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace frontend\controllers;

use common\custom_components\pay\YandexApiPay;
use common\custom_components\pay\YandexPay;
use common\custom_components\responseFormatter\XMLresponseFormatter;
use common\models\YaKassaApiPayment;
use Yii;
use yii\log\Logger;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\NotificationEventType;


class PayController extends ActiveController
{
    public $modelClass = 'common\models\NxOrder';

    public function actionYaCheckOrder()
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        if (!Yii::$app->request->isPost)
            throw new BadRequestHttpException('post type requests only');

        $data = Yii::$app->request->post();
        $methodName = (isset($data['action']) && $data['action'] == 'cancelOrder') ? 'cancelOrder' : 'checkOrder';

        $yandexPay = new YandexPay($data);

        $formatter = new XMLresponseFormatter(['rootTag' => 'checkOrderResponse', 'rootTagAttributes' => $yandexPay->{$methodName}()]);

        Yii::$app->response->formatters[Response::FORMAT_XML] = $formatter;
        Yii::$app->response->data = [];//$conditions;
        Yii::$app->response->send();
        return;
    }

    public function actionYaNotify()
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        if (!Yii::$app->request->isPost)
            throw new BadRequestHttpException('post type requests only');

        $yandexPay = new YandexPay(Yii::$app->request->post());

        $formatter = new XMLresponseFormatter(['rootTag' => 'paymentAvisoResponse', 'rootTagAttributes' => $yandexPay->paymentNotify()]);

        Yii::$app->response->formatters[Response::FORMAT_XML] = $formatter;
        Yii::$app->response->data = [];//$conditions;
        Yii::$app->response->send();
        return;
    }

    /**
     * Здесь обрабатываются запросы (уведомления) Api Yandex Kassa.
     * В настройках магазина (в интерфейсе Yandex кассы ) у издателя указан урл, ведущий сюда
     * см. документацию https://kassa.yandex.ru/docs/guides/#uwedomleniq
     * Уведомления должны приходить при изменении статуса платежа на waiting_for_capture или succeeded
     * waiting_for_capture - означает, что деньги клиента уже у яндекса, но он их отдаст нам если мы подтвердим этот платеж
     * succeeded - значит окончательный успешный статс платежа, можно считать, что деньги уже у нас.
     * Яндекс будет слать уведомления 24 часа до тех пор пока не получит ответ 200. Тело ответа значения НЕ имеет.
     * Для подтверждения платежа нужно отправить отдельный запрос.
     * Т.о. при изменении статуса платежа сюда придет уведомление и если мы ответим 200, ничего не делая, то повторений больше не будет!
     */
    public function actionYaApiCheckOrder()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $payment = json_decode(file_get_contents("php://input"), true); //это прислает Яндекс
//        $payment = json_decode( file_get_contents("php://input")); //это прислает Яндекс

        if (empty($payment)) // false добавлен для тестов
            return ['sorry' => 'body is empty, so... nothing to do'];


        $logger = Yii::getLogger();
        $logger->log($payment, Logger::LEVEL_INFO, YandexApiPay::LOG_CATEGORY);

        //получить номер заказа и ид издателя
        if (isset($payment['object']['metadata']) && isset($payment['object']['metadata']['orderId']) && isset($payment['object']['metadata']['publisherId'])) {
            $orderId = intval($payment['object']['metadata']['orderId']);
            $publisherId = intval($payment['object']['metadata']['publisherId']);
        } elseif (isset($payment['object']['id']) && !empty($payment['object']['id'])) {
            $paymentModel = YaKassaApiPayment::findOne(['ya_payment_id' => $payment['object']['id']]);
            if (null == $paymentModel) {
                return ['error' => 'payment with supplied id is unknown '];
            }
            $orderId = intval($paymentModel->order_id);
            $publisherId = intval($paymentModel->publisher_id);
        }

        $yaApiPay = new YandexApiPay($orderId, $publisherId);
        if ($yaApiPay->hasErrors) {
            return ['errors' => $yaApiPay->errors];
        }

//        $yaApiPay->setPayment($payment);
//        $yaApiPay->setPayment(null); //TEMP!!! just for test
        $yaApiPay->paymentNotify();

        return [];//пустой ответ норм. Лишь бы 200
    }

    public function actionPayByYandexApi()
    {
        if (Yii::$app->request->isPost) {
            $orderId = Yii::$app->request->post('orderId', false);
            $publisherId = Yii::$app->request->post('publisherId', false);
        } else {
            // TODO: Обработку get запросов нужно будет убрать вообще. Оставить только Post (в настройках доступа и в конфигах тоже!)
            $orderId = Yii::$app->request->get('orderId', false);
            $publisherId = Yii::$app->request->get('publisherId', false);
        }

        $orderId = $orderId ? intval($orderId) : $orderId;
        $publisherId = $publisherId ? intval($publisherId) : $publisherId;

        $failedUrl = Yii::$app->params['homeHost'] . '/site/pay-failed';

        $yaApiPay = new YandexApiPay($orderId, $publisherId);
        if ($yaApiPay->hasErrors) {
            return $this->redirect($failedUrl . '?' . http_build_query(['reasons' => $yaApiPay->errors]));
        }

        $payment = $yaApiPay->createApiYaKassaPayment();
        if ($yaApiPay->hasErrors) {
            //редирект на страницу с ошибкой
            return $this->redirect($failedUrl . '?' . http_build_query(['reasons' => $yaApiPay->errors]));
        } else {
            //редирект в яндекс
            return $this->redirect($payment->confirmation->getConfirmationUrl());
        }
    }
}