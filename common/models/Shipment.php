<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_shipment".
 *
 * @property integer $id
 * @property integer $channel_id
 * @property string $date
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $created
 * @property string $last_updated
 *
 * @property OrderItemRelease[] $nxOrderItemReleases
 * @property ShipmentChanel $channel
 * @property ShipmentItem[] $nxShipmentItems
 */
class Shipment extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_id', 'create_user_id', 'last_user_id'], 'integer'],
            [['date'], 'required'],
            [['date', 'created', 'last_updated'], 'safe'],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShipmentChanel::className(), 'targetAttribute' => ['channel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_id' => 'Channel ID',
            'date' => 'Date',
            'create_user_id' => 'Create User ID',
            'last_user_id' => 'Last User ID',
            'created' => 'Created',
            'last_updated' => 'Last Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrderItemReleases()
    {
        return $this->hasMany(OrderItemRelease::className(), ['shipment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(ShipmentChanel::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxShipmentItems()
    {
        return $this->hasMany(ShipmentItem::className(), ['shipment_id' => 'id']);
    }
}
