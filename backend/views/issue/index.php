<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\IssueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Выпуски';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Добавить выпуск', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(['id' => 'issues-list', 'enablePushState' => true]); ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-bordered'],
        'rowOptions' => function($model, $key, $index, $grid){
            $attributes = [
                'data-key' => $key,
            ];
            //Одиночные подсвечивать фоновым цветом
            if ($model->type == 2) {
                $attributes['style'] = 'box-shadow: inset 0px 0px 7px rgba(255,0,0,0.15);background:rgba(0,0,0,0.1)';
            }
            return $attributes;
        },
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = '/issue/update?id=' . $key;
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать выпуск',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-gear fa-font-18',

                            ]
                        );
                    },
                ],
            ],

//            ['class' => 'yii\grid\SerialColumn'],

//            'Message_ID',
//            'User_ID',
//            'Subdivision_ID',
//            'Sub_Class_ID',
//            'Priority',
            // 'Keyword',
            // 'Checked',
            // 'TimeToDelete',
            // 'TimeToUncheck',
            // 'IP',
            // 'UserAgent',
            // 'Parent_Message_ID',
            // 'created',
            // 'last_updated',
            // 'last_user_id',
            // 'LastIP',
            // 'LastUserAgent',
            [
                'attribute' => 'magazine',
                'value' => 'magazine.HumanizedName',
                'filter' => \yii\helpers\ArrayHelper::map(Yii::$app->user->identity->publisher->myEditions, 'HumanizedName', 'HumanizedName'),
            ], 
//            'magazine',
//            [
//                'class' => 'yii\grid\DataColumn',
//                'attribute' => 'type',
//                'content' => function ($model, $key, $index, $column) {
//                    return $model->type == 2 ? 'одиночн' : 'периодич';
//                },
//                'contentOptions' => ['style' => 'vertical-align: middle;'],
//                'filter' => ['1' => 'период.', '2' => 'одиночн.'],
//                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
//            ],
                        
//             'Name',
             'issue_title:ntext',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status_id',
                'content' => function($model,$key,$index,$column){
                    switch ($model->status_id) {
                        case 1:
                            return 'готовится';
                            break;
                        case 2:
                            return 'выпущен';
                            break;
                        case 3:
                            return 'отменен';
                            break;
                    }
                },
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'filter' => ['1' => 'готовится', '2' => 'выпущен','3' => 'отменен'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%;padding:6px 0px', 'class' => 'form-control'],
            ],
            [
                'attribute' => 'PublicDate',
                'value' => function($model,$key,$index,$column){
                   return date('d.m.Y',strtotime($model->PublicDate)); 
                },
                'filter' => DatePicker::widget([
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'layout' => '{input}{remove}',
                    'size' => 'xs',
//                    'convertFormat' => true,
                    'model' => $searchModel,
                    'attribute' => 'PublicDate',
                    'options' => [
                        'placeholder' => 'Укажите дату выхода ...',
                        'value' => $searchModel->PublicDate,
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
                'format' => 'html',
            ],
            [
                'attribute' => 'StartDate',
                'value' => function($model,$key,$index,$column){
                   return date('d.m.Y',strtotime($model->PublicDate)); 
                },
                'filter' => DatePicker::widget([
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'layout' => '{input}{remove}',
                    'size' => 'xs',
//                    'convertFormat' => true,
                    'model' => $searchModel,
                    'attribute' => 'StartDate',
                    'options' => [
                        'placeholder' => 'дата рассылки...',
                        'value' => $searchModel->StartDate,
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
                'format' => 'html',
            ],
            // 'Picture',
            // 'IssueID',
            // 'annotation:ntext',
            // 'available_online',
            // 'no_full',
             'year',
             'release_month',
//             'Number',
            // 'oldID',
            // 'inCompositionDate',
            // 'showDescription',
            // 'ArticlePrice',
            // 'ncTitle',
            // 'ncKeywords',
            // 'ncDescription:ntext',
            // 'end_number',
            // 'subscription_code',
            // 'xpress_id',
            // 'create_user_id',
//             'pdf_link_life_time:datetime',

//            ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => '',
                'content' => function($model,$key,$index,$column){
                    if ($model->isPdfLoaded()) {
                        return '<i class="fa fa-check-square" title="Файл загружен" style="color:#3c8dbc">';
                    } else {
                        return '<i class="fa fa-file-pdf-o" title="Не загружено!!" style="color:#3c8dbc">';
                    }
                },
                'contentOptions' => ['style' => 'vertical-align: middle;text-align:center'],
                'header' => 'PDF файл',
                'headerOptions' => [],
                'filter' => false,
//                'filter' => ['1' => 'готовится', '2' => 'выпущен','3' => 'отменен'],
//                'filterInputOptions' => ['style' => 'width:100%;height:100%;padding:6px 0px', 'class' => 'form-control'],

            ],
                        
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            '/issue/delete?id=' . $key,
                            [
                                'title' => 'Удалить выпуск',
                                'aria-label' => 'Удалить',
//                                'data-confirm' => 'Вы уверены, что хотите удалить этот выпуск?',
//                                'data-method' => 'post',
                                'data-id' => $key,
//                                'data-pjax' => '#issues-list',
                                'class' => 'fa fa-trash fa-font-18',
                                'onclick' => "deleteIssue(event)",
                            ]
                        );
                    },
                ],
            ],
            
            
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<script>
    function deleteIssue(event) {
        if (!event)
            var event = window.event;

        var elem = $(event.target);
        var id = elem.data('id');

        if ( confirm('Вы уверены, что хотите удалить этот выпуск?') ) {
            $.ajax({
                'url' : '/issue/delete?id=' + id,
                'type' : 'post',
                'dataType' : 'html',
            })
            .done(function(response) {
                $.pjax.reload({container:'#issues-list'});
            })
            .fail(function(a,b,c) {
                console.log(a);
                console.log(b);
                console.log(c);
                alert('При попытке удаления произошла ошибка. См. консоль');
            })

        }
        console.log(elem);
        event.preventDefault();
        return false;
    }
</script>
