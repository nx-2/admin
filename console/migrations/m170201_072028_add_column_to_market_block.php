<?php

use yii\db\Migration;

class m170201_072028_add_column_to_market_block extends Migration
{
    public function up()
    {
        $mysqlCode = '
            ALTER TABLE `ads`.`market_blocks` 
            ADD COLUMN `enabled` TINYINT(1) NULL DEFAULT 1 AFTER `go_to_new`;
        ';
        $this->execute($mysqlCode);
    }

    public function down()
    {
        echo "m170201_072028_add_column_to_market_block cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
