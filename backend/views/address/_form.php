<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\jui\AutoComplete;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\NxAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<?php  //yii\widgets\Pjax::begin(['id' => 'address-form-container', 'enablePushState' => false, 'clientOptions' => ['push' => false]]) ?>
<?php 
    $action = [];
    if ($model->isNewRecord) {
        $action[] = Url::to(['address/create']);
    } else {
        $action[] = Url::to(['address/update']);
        $action['id'] = $model->id;
    }
    $formOptions = [
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['address/validate-address']),
            // 'options' => ['data-pjax' => true],
            'id' => $formName,
            'validateOnBlur' => false,
            'action' => $action,
            'options' => ['class' => 'address-form'],
    ];
    if (isset($submitAjax))
        $formOptions['options']['data-submitAjax'] = intval($submitAjax);
    $form = ActiveForm::begin($formOptions); ?>

<div class='box box-primary'>
    <div class='box-header with-border'>
        <h3 class='box-title'>
            Адрес
        </h3>
    </div>
    <div class='box-body'>
        <div class="row">
            <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'country_id',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->widget(AutoComplete::classname(), [
                        'options' => [
                            'placeholder' => 'Начните набирать...',
                            'class' => 'form-control input-sm',
                            'name' => '',
                            'id' => $formName . '_country_name',
                            'value' =>  $model->country === null ? '' : $model->country->Country_Name,
                        ],
                        'clientOptions' => [
                            'source' => '/country/autocomplete',
                            'autoFill'=>false,
                            'minLength'=>'3',
                            'appendTo' => $formName . '_country_name',
                            'delay' => '300',
                            'select' => new yii\web\JsExpression('function(event, ui){$("#country_id").val(ui.item.id);}'),
                            // 'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#country_id").val("");}}'),
                            // 'disabled' => true,

                        ],
                ]) ?>
                <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'country_id', $options = ['id' => 'country_id',])); ?>

                <?= $form->field($model, 'area',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->widget(AutoComplete::classname(), [
                        'options' => [
                            'placeholder' => 'Начните набирать...',
                            'class' => 'form-control input-sm',
                            // 'name' => '',
                            'id' => $formName . '_area-name',
                            // 'value' => $model->isNewRecord ? '' : $model->area,
                        ],
                        'clientOptions' => [
                            'source' => '/region/autocomplete',
                            'autoFill'=>false,
                            'minLength'=>'3',
                            'appendTo' => $formName . '_area-name',
                            'delay' => '300',
                            'select' => new yii\web\JsExpression('function(event, ui){$("#area-name").val(ui.item.id);}'),
                            // 'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#area-name").val("");}}'),
                            // 'disabled' => true,

                        ],
                ]) ?>

                <?= $form->field($model, 'region',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'city',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'zipcode',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

            </div>
            <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'post_box',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>
                
                <?= $form->field($model, 'street',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'house',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true,'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'building',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'structure',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                
            </div>
            <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'apartment',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>

                <?= $form->field($model, 'telcode',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                
                <?= $form->field($model, 'phone',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                
                <?= $form->field($model, 'fax',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm']) ?>
                
                <?= $form->field($model, 'comment',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textarea(['rows' => 2, 'class' => 'form-control input-sm']) ?>
            </div>
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'address',['labelOptions'=>['style'=>'font-size:12px;margin-bottom:0px']])->textInput(['maxlength' => true, 'class' => 'form-control input-sm','readonly' => true]) ?>
            </div>
        </div>
        
    </div>
    <div class='box-footer'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
            <?= Html::submitButton('Сохранить адрес', ['class' => 'btn btn-primary pull-left', 'form' => $formName]); ?>
            <?= Html::button('Сохранить как новый', [
                                    'class' => 'btn btn-warning pull-left', 
                                    'form' => $formName,
                                    // 'onclick' => '$("#' + $formName + '").prop("action","/address/create");$("#' + $formName + ' button[type=\"submit\"]").click();',
                                    'onclick' => '$("#' . $formName . '").prop("action","'. Url::to(['address/create']) .'");$("#' . $formName . ' button[type=\"submit\"]").click();',
                                    'style' => 'margin-left:20px;'
            ]); ?>
            <?php if(isset($showMessage)): ?>
                <?php 
                    switch ($showMessage['type']) {
                        case 'success':
                            $alertBg = 'bg-green';
                            $alerticon = 'fa-check';
                            break;
                        case 'error':
                            $alertBg = 'bg-orange';
                            $alerticon = 'fa-warning';
                            break;
                        case 'info':
                            $alertBg = 'bg-aqua';
                            $alerticon = 'fa-info';
                            break;
                        case 'danger':
                            $alertBg = 'dg-red';
                            $alerticon = 'fa-ban';
                            break;
                        default:
                            $alertBg = 'dg-aqua';
                            $alerticon = 'fa-info';
                            break;
                    }
                ?>
            <div class='alert alert-dismissible pull-right <?=$alertBg; ?>' style='position:relative'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style='position:absolute;top:2px;right:7px'>×</button>
                <div>
                    <i class="icon fa <?=$alerticon;?>"></i>
                    <?=$showMessage['text']; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php  // Pjax::end(); ?>
