<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

$banks = array(
    'Сбербанк Москва' => array(
        'name' 			=> 'Сбербанк России, г. Москва',
        'corr_account' 	=> '30101810400000000225',
        'bik_code' 		=> '044525225'
    )
);


$companies = array(
    'OOO_OSP' 				=> array(
        'name' 				=> 'ООО «Издательство «Открытые Системы»',
        'legal_address'		=> '127254, Россия, Москва, проезд Добролюбова д. 3, стр. 3, каб. 13',// '127254, Москва, ул. Руставели д. 12а, стр 2',
        'post_address'		=> '127254, Россия, Москва, проезд Добролюбова д. 3, стр. 3, каб. 13',// 'Россия, 127254, Москва, ул. Руставели д. 12а, стр 2',
        'phone'				=> '+7 (495) 725-4780',
        'fax'				=> '+7 (495) 725-4785',
        'inn_code' 			=> '9715004017',
        'kpp_code'			=> '771501001',
        'bank_account' 		=> '40702810438170101424 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/ooo-shtamp.jpg" width="170" />',
        'CEO'				=> 'Герасина Г.А.',
        'chief_accountant'	=> 'Федорчуков С.Д.',
        'getNDS'			=>	function($value){
            return ($value / 118) * 18;
        }
    ),
    'OSP' 				=> array(
        'name' 				=> 'ЗАО «Издательство «Открытые Системы»',
        'legal_address'		=> '123056, Москва, Электрический пер., 8, cтр.3',
        'post_address'		=> 'Россия, 123056, Москва, Электрический пер., 8, cтр.3',
        'phone'				=> '+7 (495) 725-4785',
        'fax'				=> '+7 (495) 725-4788',
        'inn_code' 			=> '7706128372',
        'kpp_code'			=> '771001001',
        'bank_account' 		=> '40702810438170101424 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/pechat_zao_color.gif" />',
        'CEO'				=> 'Герасина Г.А.',
        'chief_accountant'	=> 'Федорчуков С.Д.',
        'getNDS'			=>	function($value){
            return ($value / 118) * 18;
        }
    ),
    'ОСП-Курьер' 		=> array(
        'name' 				=> 'ООО «ОСП-Курьер»',
        'legal_address'		=> '123056, Москва, Электрический пер., 8, cтр.3',
        'post_address'		=> 'Россия, 123056, Москва, Электрический пер., 8, cтр.3',
        'phone'				=> '+7 (495) 725-4785',
        'fax'				=> '+7 (495) 725-4788',
        'inn_code' 			=> '7710297197',
        'kpp_code'			=> '771001001',
        'bank_account' 		=> '40702810638170101680 в Московском банке ПАО «Сбербанк России»',
        'bank' 				=> $banks['Сбербанк Москва'],
        'stamp_scan'		=> '<img src="http://www.osp.ru/images/pechat.gif" />',
        'CEO'				=> 'Борисов М.Е.',
        'chief_accountant'	=> 'Илюхин В.Ю.',//'Федорчукова М.Д.',
        'getNDS'			=> function($value){
            return $value / 11;
        }
    )
);


$recipient = $companies['OSP'];
?>


    <style type="text/css">
        body {
            margin: 0;
            background			: #FFFFFF;
        }
        div.hdn {
            margin-top      	: 25px;
            width           	: 300px;

        }
    </style>
    <style type="text/css" media="print">
        div.hdn {
            display         	: none;
            width           	: 0px;
            height          	: 0px;
        }
    </style>

    <table width="600" border="0" cellpadding="0" cellspacing="4" style="width:600px;">
        <tr valign="top">
            <td align="right" style="text-align: right;">
                <b><?php echo $recipient['name']; ?></b><br/>
                <?php echo $recipient['post_address']; ?><br/>
                Телефон: <?php echo $recipient['phone']; ?><br/>
                Факс: <?php echo $recipient['fax']; ?>
            </td>
        </tr>
        <!--tr><td align="center" style="text-align: center;">Срок оплаты данного счета - ПЯТЬ БАНКОВСКИХ ДНЕЙ</td></tr-->

    </table>


    <table width="600" border="0" cellpadding="0" cellspacing="4" style="width:600px;">
        <tr valign="top">
            <td style="font-size:13px; background-color: #fff;">
                Поставщик:
                <br/>
                <b>
                    <?php echo $recipient['name']; ?>,
                    ИНН/КПП <?php echo $recipient['inn_code']; ?>/<?php echo $recipient['kpp_code']; ?>
                </b>
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size:13px; background-color: #fff;">
                Юридический адрес поставщика:
                <br/><b><?php echo $recipient['legal_address']; ?></b></td></tr>
        <tr valign="top">
            <td style="font-size:13px; background-color: #fff;">
                Расчетный счет:
                <br/>
                <b>
                    <?php echo $recipient['bank_account']; ?>,
                    к/с <?php echo $recipient['bank']['corr_account']; ?>,
                    БИК <?php echo $recipient['bank']['bik_code']; ?>
                </b>
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size:13px; background-color: #fff;">
                Потребитель:
                <br/>
                <b>
                    <?php echo $legal->name; ?>,
                    ИНН/КПП <?php echo $legal->inn; ?>/<?php echo $legal->kpp; ?>
                </b>
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size:13px; background-color: #fff;">
                Юридический адрес потребителя:
                <br/>
                <b>
                    <?php echo ($model->address->address)?$model->address->address:''; ?>
                </b>
            </td>
        </tr>
        <tr valign="top">
            <td align="center" style="text-align: center;">
                <b>Счет № П<?php echo $model->id; ?></b>
            </td>
        </tr>
        <tr valign="top">
            <td align="center" style="text-align: center;">
                <b>Дата: <?php echo date('d').'.'.date('m').'.'.date('Y'); ?></b>
            </td>
        </tr>
    </table>

    <table width="600" border="1" cellpadding="3" cellspacing="0" style="width:600px;">
        <tr align="center" valign="top" style="text-align: center;">
            <td>№</td>
            <td>Наименование товара</td>
            <td>Количество</td>
            <td>Цена</td>
            <td>Ставка НДС</td>
            <td>НДС</td>
            <td>Сумма</td>
        </tr>

        <?php
        $nds = array();
        $itemNum=0;
        ?>

        <?php if (!empty($model->items)): ?>
        <?php foreach ($model->items as $item): ?>
                <?php   $nds_item=($item->delivery_type_id == 1) ? 10 : 18;?>
            <tr align="center" valign="top" style="text-align: center;">
                <td>
                    <?php echo ($itemNum+1); ?>
                </td>
                <td align="left" style="text-align: left;">
                    <?php echo $item->subscription->periodical->HumanizedName; ?>
                </td>
                <td >
                    <?php echo $item->quantity; ?>
                </td>
                <td align="right" style="text-align: right;">
                    <?php echo $item->price ?>
                </td>
                <td>
                    <?php echo $nds_item . '%'; ?>
                </td>
                <td align="right" style="text-align: right;">
                    <?php
                    $item_nds = $item->price * $nds_item / (100 + $nds_item);
                    if(!isset($nds[$nds_item])) {
                        $nds[$nds_item] = $item_nds*$item->quantity;
                    }
                    else {
                        $nds[$nds_item] += $item_nds*$item->quantity;
                    }
                    echo sprintf('%01.2f', $item_nds);
                    ?>
                </td>
                <td align="right" style="text-align: right;">
                    <?php echo $item->price; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>


        <tr valign="top" colspan="2">
            <td colspan="2" align="right" style="text-align: right;">
                Итого:
            </td>
            <td colspan="5">
                <?php echo sprintf('%01.2f', $model->price) ?> руб.
                (<?php echo getRoubleSumInWords($model->price); ?>),
                в том числе <?php
                foreach($nds as $percent => $value) {
                    echo 'НДС ' . $percent . '% - ' . sprintf('%01.2f', $value) . ' руб.; ';
                }
                ?>
            </td>
        </tr>
    </table>

    <br>

    <table width="600" border="0" cellpadding="0" cellspacing="4">
        <tr valign="middle">
            <td>
                Генеральный директор:
            </td>
            <td rowspan="2" valign="top" style="padding-top:20px;">
                <?php echo $recipient['stamp_scan']; ?>
            </td>
            <td align="right" style="text-align: right;">
                / <?php echo $recipient['CEO']; ?> /
            </td>
        </tr>
        <tr>
            <td valign="middle">
                Главный бухгалтер:
            </td>
            <td align="right" valign="middle" style="text-align: right;">
                <br>
                / <?php echo $recipient['chief_accountant']; ?> /
            </td>
        </tr>
    </table>

    <div class="hdn" style="color:#c00; width: 550px !important;text-align:justify;">
        Внимание!  В платежном поручении в назначении платежа обязательно укажите номер счета.
        Для оперативности получения издания, передайте по факсу (495)&nbsp;725-47-88 копию платежного поручения  с отметкой банка об оплате.
    </div>

<?php
function getRoubleSumInWords($sum)
{
    // получить сумму копеек
    $kops = intval($sum * 100) % 100;

    // получить сумму рублей
    $sum = (int)$sum;

    if($sum==0 && $kops==0) {
        return 'ноль рублей';
    }

    $roubles = $sum % 1000;
    $thousands = (int)(($sum % 1000000 - $roubles) / 1000);
    $millions = (int)(($sum % 1000000000 - $thousands - $roubles) / 1000000);

    $result=array();

    if($millions){
        $result[] = getNumberInWords($millions).' '.getPlural($millions, 'миллион', 'миллиона', 'миллионов');
    }

    if($thousands){
        $result[] = getNumberInWords($thousands, false).' '.getPlural($thousands, 'тысяча', 'тысячи', 'тысяч');
    }


    if($roubles==0) {
        $result[] = 'рублей';
    } else {
        $result[] = getNumberInWords($roubles).' '.getPlural($roubles, 'рубль', 'рубля', 'рублей');
    }

    if($kops) $result[] = getNumberInWords($kops, false).' '.getPlural($kops, 'копейка', 'копейки', 'копеек');

    return trim(implode(' ', $result));
}

// отдает расшифровку числительных
function getNumberInWords($number, $male=true)
{
    $number=(int)$number;

    $numUnits = $number % 10;
    $numTens = (int)(($number % 100 - $numUnits) / 10);
    $numHundreds = (int)(($number % 1000 - $numTens - $numUnits) / 100);

    $_UNITS_MALE = array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять');
    $_UNITS_FEMALE = array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять');
    $_TENS = array('', 'десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $_TENS_PLUS = array('', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $_HUNDREDS = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');


    $numberInWords=array($_HUNDREDS[$numHundreds]);

    if($numTens==1 && $numUnits>0) {
        $numberInWords[]=$_TENS_PLUS[$numUnits];
    } else {
        $numberInWords[]=$_TENS[$numTens];

        if($male){
            $numberInWords[]=$_UNITS_MALE[$numUnits];
        } else {
            $numberInWords[]=$_UNITS_FEMALE[$numUnits];
        }
    }

    return ' '.trim(implode(' ', $numberInWords));
}

function getPlural($n, $nomSingular, $genSingular, $genPlural) {
    switch(true) {
        case ($n % 10 == 1 && $n % 100 != 11):
            return $nomSingular;
            break;
        case ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)):
            return $genSingular;
            break;
        default:
            return $genPlural;
    }
}


?>