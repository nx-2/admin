<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>

<?php 
    //use Yii;
?>
<div>
    <h3>Оплата наличными в банке</h3>
    <p>
        Для оплаты заказа наличными в отделении банка необходимо распечатать квитанцию. После нажатия на  кнопку квитанция откроется в новой вкладке.
    </p>
    <div>
        <a class='pay-button' target="_blank" href="<?php echo(Yii::$app->params['admin']['schema'] . Yii::$app->params['admin']['url'] . '/site/get-receipt?hash=' . $order->hash); ?>" style='display:inline-block;margin-bottom: 5px;text-decoration: none;'>
            Открыть квитанцию
        </a>
    </div>
 </div>
