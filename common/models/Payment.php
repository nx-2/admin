<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_payment".
 *
 * @property integer $id
 * @property string $ws1c_id
 * @property integer $order_id
 * @property integer $transaction_id
 * @property integer $payment_agent_id
 * @property integer $contractor_id
 * @property string $contractor_name
 * @property string $contractor_inn
 * @property string $contractor_kpp
 * @property string $docno
 * @property string $sum
 * @property string $date
 * @property string $year
 * @property string $purpose
 * @property string $comment
 * @property integer $status
 * @property string $result_comment
 * @property string $result
 * @property string $request_data
 * @property string $label
 * @property integer $enabled
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property integer $publisher_id
 *
 * @property Transactions $transaction
 * @property NxOrder $order
 * @property NxPaymentAgent $paymentAgent
 */
class Payment extends \yii\db\ActiveRecord
{
    const STATUS_NOT_SENT_TO_1C_YET = 0;
    const STATUS_SENT_TO_1C_SUCCESSFULL = 1;
    const STATUS_FAIL_TRY_TO_SENT_TO_1C = -1;
    const STATUS_FAIL_BY_TIME_SENT_TO_1C = -2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'transaction_id', 'payment_agent_id', 'contractor_id', 'status', 'enabled', 'create_user_id', 'last_user_id', 'publisher_id'], 'integer'],
            [['sum'], 'number'],
            [['date', 'year', 'created', 'last_updated'], 'safe'],
//            [['year', 'purpose', 'comment', 'result', 'request_data', 'label'], 'required'],
            [['year', 'result', 'request_data', 'label'], 'required'],
            
            [['purpose', 'comment', 'result', 'request_data'], 'string'],
            [['ws1c_id'], 'string', 'max' => 100],
            [['contractor_name', 'contractor_inn', 'contractor_kpp', 'docno', 'result_comment', 'label'], 'string', 'max' => 255],
            [['transaction_id'], 'unique'],
            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transactions::className(), 'targetAttribute' => ['transaction_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrder::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['payment_agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentAgent::className(), 'targetAttribute' => ['payment_agent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ws1c_id' => 'Ws1c ID',
            'order_id' => 'Order ID',
            'transaction_id' => 'Transaction ID',
            'payment_agent_id' => 'Payment Agent ID',
            'contractor_id' => 'Contractor ID',
            'contractor_name' => 'Contractor Name',
            'contractor_inn' => 'Contractor Inn',
            'contractor_kpp' => 'Contractor Kpp',
            'docno' => 'Docno',
            'sum' => 'Sum',
            'date' => 'Date',
            'year' => 'Year',
            'purpose' => 'Purpose',
            'comment' => 'Comment',
            'status' => 'Status',
            'result_comment' => 'Result Comment',
            'result' => 'Result',
            'request_data' => 'Request Data',
            'label' => 'Label',
            'enabled' => 'Enabled',
            'created' => 'Created',
            'last_updated' => 'Last Updated',
            'create_user_id' => 'Create User ID',
            'last_user_id' => 'Last User ID',
            'publisher_id' => 'Издатель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(NxOrder::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentAgent()
    {
        return $this->hasOne(PaymentAgent::className(), ['id' => 'payment_agent_id']);
    }
}
