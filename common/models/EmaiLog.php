<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property integer $success
 * @property string $from
 * @property string $to
 * @property string $subject
 * @property string $body
 * @property string $time
 * @property string $type
 * @property integer $publisher_id
 *
 * @property integer $order_id
 * @property integer $issue_id
 * @property integer $edition_id
 * @property integer $payment_id
 * @property integer $shipment_id
 * @property integer $shipment_item_id
 * @property integer $item_release_id
 *
 */
class EmaiLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('logs');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['success','publisher_id', 'order_id', 'issue_id', 'payment_id', 'shipment_id', 'shipment_item_id', 'order_id'], 'integer'],
            [['order_id', 'issue_id', 'payment_id', 'shipment_id', 'shipment_item_id', 'order_id'], 'integer', 'skipOnEmpty' => true],
            [['body'], 'string'],
            [['time'], 'safe'],
            [['from', 'to'], 'string', 'max' => 100],
            [['subject'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'success' => 'Результат',
            'from' => 'Адрес отправителя',
            'to' => 'Адрес получателя',
            'subject' => 'Тема письма',
            'body' => 'Содержание',
            'time' => 'Время отправки',
            'type' => 'Тип',
            'publisher_id' => 'Id Издателя',
        ];
    }

    /**
     * @param string 'same'|'currentUser' $reсipient
     * @return bool is message sent successfully or not
     */
    public function resendImmediately($reсipient = 'currentUser')
    {
        if (null == $this)
            return false;

        if ($reсipient != 'same' && $reсipient != 'currentUser')
            $reсipient = 'currentUser';

        $message = Yii::$app->mailer->compose();

        if ($reсipient == 'currentUser' && (isset(Yii::$app->user) && Yii::$app->user->identity !== null) ) {
            $message->setTo(Yii::$app->user->identity->Email);
        } else {
            $message->setTo($this->to);
        }
        $message->setFrom($this->from)
            ->setSubject($this->subject)
            ->setHtmlBody($this->body);

        return $message->send();
    }
}
