<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $data Array */
?>
<?php //echo($data['name']);?>

<div id='person-info-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="person-info-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="person-info-label"><?php echo(!empty($data['name']) ? $data['name'] : $data['f'] . ' ' . $data['i'] . ' ' . $data['o']); ?></h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#person-info-general" data-toggle="tab" aria-expanded="false">Общее</a></li>
                        <?php if(!empty($data['address'])): ?>
                            <li><a href="#person-info-address" data-toggle="tab" aria-expanded="true">Адрес</a></li>
                        <?php endif;?>
                        <?php if(!empty($data['company'])): ?>
                            <li><a href="#person-info-company" data-toggle="tab" aria-expanded="true">Компания</a></li>
                        <?php endif;?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="person-info-general">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['general'] as $name => $label):?>
                                                <?php if(!empty($data[$name]) && !is_array($data[$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'><?=$label; ?></th>
                                                        <td><?=$data[$name];?></td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($data['address'])): ?>
                        <div class="tab-pane" id="person-info-address">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['address'] as $name => $label):?>
                                                <?php if(!empty($data['address'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['address'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($data['company'])): ?>
                        <div class="tab-pane" id="person-info-company">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['company'] as $name => $label):?>
                                                <?php if(!empty($data['company'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['company'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
