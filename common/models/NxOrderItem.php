<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "nx_order_item".
 *
 * @property string $id
 * @property string $order_id
 * @property integer $item_id
 * @property string $item_type
 * @property string $price
 * @property double $discount
 * @property integer $quantity
 * @property integer $completed
 * @property string $delivery_type_id
 * @property integer $enabled
 * @property integer $prolong_state
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id

 * @property \common\models\NxSubscription $subscription
 * @property \common\models\Issue $issue
 * @property \common\models\OrderItemRelease[] $itemIssues
 */
class NxOrderItem extends \common\models\WatchableModel
{
    const ITEM_TYPE_SUBSCRIPTION = 'nx_subscription';
    const ITEM_TYPE_ISSUE = 'Issue';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'item_id', 'item_type', 'created', 'create_user_id', 'last_user_id'], 'required'],
            [['order_id', 'item_id', 'quantity', 'completed', 'delivery_type_id', 'enabled', 'prolong_state', 'create_user_id', 'last_user_id'], 'integer'],
            [['price', 'discount'], 'number'],
            [['created', 'last_updated'], 'safe'],
            [['item_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'item_id' => 'Item ID',
            'item_type' => 'Item Type',
            'price' => 'Price',
            'discount' => 'Discount',
            'quantity' => 'Quantity',
            'completed' => 'Completed',
            'delivery_type_id' => 'Delivery Type ID',
            'enabled' => 'Enabled',
            'prolong_state' => 'Prolong State',
            'created' => 'Created',
            'last_updated' => 'Last Updated',
            'create_user_id' => 'Create User ID',
            'last_user_id' => 'Last User ID',
        ];
    }

    /**
     * @return null|\yii\db\ActiveQuery
     */
    public function getIssue()
    {
        if ($this->item_type == 'Issue')
            return $this->hasOne(Issue::class, ['Message_ID' => 'item_id']);
        else
            return null;
    }

    /**
     * @return null|\yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        if ($this->item_type == 'nx_subscription')
            return $this->hasOne(NxSubscription::class, ['id' => 'item_id']);
        else
            return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        if ($this->item_type == 'Issue')
            return $this->hasOne(Issue::class, ['Message_ID' => 'item_id']);
        else
            return $this->hasOne(NxSubscription::class, ['id' => 'item_id']);

    }
    /**
     * @return null|\yii\db\ActiveQuery
     */
    public function getItemIssues()
    {
        return $this->hasMany(OrderItemRelease::class, ['item_id' => 'id']);
    }

    public function toString()
    {
        $str = '';
        if ($this === null)
            return $str;
        if ($this->item_type == 'nx_subscription') {
            $str = 'Подписка на издание ';
            $subscription = $this->subscription;
            $magName = $subscription->edition->HumanizedName;
            $type = $subscription->type == 'pdf' ? '(pdf версия)' : '(печатная версия)';
            $magName .= $type;
            $startDate = date('d.m.Y', strtotime($subscription->date_start) );
            $issuesAmount = $subscription->rqty;

            $monthes = $subscription->period;
            $monthes = $monthes == 1 ? $monthes . ' месяц' : $monthes > 4 ? $monthes . ' месяцев' : $monthes . ' месяца';
            $str .= $magName . ' c ' . $startDate . ' на ' . $monthes . ', количество выпусков: ' . $issuesAmount;
        } else {
            $str = ' Покупка PDF версии журнала ';
            $issue = $this->issue;
            $magName = $issue->magazine->HumanizedName;
            $str .= $magName . ' выпуск ' . $issue->Name . ' ' .$issue->issue_title . ' от ' . $issue->PublicDate ;
        }
        return $str;
    }

}
