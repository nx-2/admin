<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Issue */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Issues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Message_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Message_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Message_ID',
            'User_ID',
            'Subdivision_ID',
            'Sub_Class_ID',
            'Priority',
            'Keyword',
            'Checked',
            'TimeToDelete',
            'TimeToUncheck',
            'IP',
            'UserAgent',
            'Parent_Message_ID',
            'created',
            'last_updated',
            'last_user_id',
            'LastIP',
            'LastUserAgent',
            'Name',
            'MagazineID',
            'StartDate',
            'Number',
            'PublicDate',
            'Picture',
            'IssueID',
            'status_id',
            'annotation:ntext',
            'available_online',
            'issue_title:ntext',
            'no_full',
            'year',
            'oldID',
            'inCompositionDate',
            'showDescription',
            'ArticlePrice',
            'ncTitle',
            'ncKeywords',
            'ncDescription:ntext',
            'release_month',
            'end_number',
            'subscription_code',
            'xpress_id',
            'create_user_id',
            'type',
            'pdf_link_life_time:datetime',
        ],
    ]) ?>

</div>
