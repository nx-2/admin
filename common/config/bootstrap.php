<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@bower', dirname(dirname(__DIR__)) . '/bower-asset');
Yii::setAlias('@root_dir', dirname(dirname(__DIR__)) );
