<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Action */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="action-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <label class="control-label" for="action-action_code">Код акции</label>
    <?=  DatePicker::widget([
         'name' => 'date_start',
        'type' => DatePicker::TYPE_INPUT,
         'value' => date('Y-m-d', strtotime($model->date_start)),
         'options' => ['placeholder' => 'Выберите дату'],
         'pluginOptions' => [
             'autoclose'=>true,
             'format' => 'yyyy-mm-dd',
         ]
     ]);?>

    <label class="control-label" for="action-action_code">Код акции</label>
    <?=  DatePicker::widget([
        'name' => 'date_end',
        'type' => DatePicker::TYPE_INPUT,
        'value' => date('Y-m-d', strtotime($model->date_end)),
        'options' => ['placeholder' => 'Выберите дату'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
        ]
    ]);?>

    <?= $form->field($model, 'action_code')->textInput(['maxlength' => true,  'readonly'=>$model->isNewRecord ?  false : true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'enableAjaxValidation' => true]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>



    <div class="form-group">

        <?= $form->field($model, 'promocodes')->textarea(['rows' => 6, 'id' => 'promocodes']) ?>

    </div>

    <div class="input-group input-group-sm col-md-6">
        <input id="count" type="text" class="form-control" value="0">
        <span class="input-group-btn">
                      <button id="generated" type="button" class="btn btn-info btn-flat">Генерировать</button>
                    </span>
    </div>


    <div class="clearfix"></div>
<br>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(
        '<i class=\"fa fa-icon\"></i> Удалить акцию ' ,
        '/action/delete?id=' . $model->id,
        [
        'label' => 'Удалить',
        'aria-label' => 'Удалить',
        'data-confirm'=>'Вы уверены, что хотите удалить акцию?',
        'data-method'=> 'post',
        'data-pjax' => '1',
        'class' => 'btn btn-danger',
        ]);?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Url;

$url = Url::toRoute(['generated', 'id' => $model->action_code]);
$script = <<< JS

$('#generated').click(function() {
var count= $('#count').val(); 
 $.ajax({
        dataType: 'json',
        type: "POST",
        url: "$url&count="+count,
        success: function(jsondata){
            $('#promocodes').html(jsondata.string);
        },
        error: function(jsondata) {
                $('#promocodes').html('ошибка генерации');
         }
      });

})


JS;

$this->registerJs($script, yii\web\View::POS_END);
?>
