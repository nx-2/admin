<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace backend\controllers;

use common\models\AdvertsCodes;
use common\models\events\ARsingleAttrChangedEvent;
use common\models\Integration1cVerify;
use common\models\Issue;
use common\models\LoginForm;
use common\models\MarketBlock;
use common\models\NxOrder;
use common\models\NxPrice;
use common\models\PaymentType;
use common\models\Publisher;
use common\models\RecoverPasswordForm;
use common\models\RelationSystems;
use common\models\User;
use common\services\NxSoapTester;
use common\services\PdfLogService;
use Exception;
use Yii;
use yii\base\InlineAction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function ($rule, $action) {
                    /** @var InlineAction $action */
                    if (Yii::$app->user->isGuest) {
                        $schema = Yii::$app->params['admin']['schema'] ?? 'https://';
                        $schema = str_replace('://', '', $schema);
                        $this->redirect(Url::toRoute('/site/login', $schema));
                    } else {
                        throw new ForbiddenHttpException('Доступ запрещен');
                    }
                },
                'rules' => [
                    // незалогиненым (гостям) разрешить только страницу входа и страницу ошибки
                    [
                        'controllers' => ['site'],
                        'actions' => ['login', 'error', 'pay-success', 'pay-failed', 'get-receipt', 'pay-notify', 'password-is-lost', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    //залогиненым разрешить
                    [
                        'controllers' => ['site'],
                        'actions' => ['logout', 'login', 'index', 'error', 'ajax-enabled', 'pay-success', 'pay-failed', 'get-receipt', 'test-soap-server', 'pay-notify', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * на эту страницу будет вести кнопка "Вернуться в магазин" после оплаты через АПИ Яндекс кассы
     * Яндекс касса будет присылать объект платежа (см. документацию к апи яндекс кассы), который может быть в любом
     * из предусмотренных статусов (т.е. не обязательно оплачен, может и нет)
     * Рекомендуется НЕ верить полученному объекту, а перезапросить яндекс по известному ИД платежа
     * */
    public function actionPayNotify()
    {
        // TODO: реализовать всю необходтмую логику приема данных от Яндекс кассы и показа нужной информации в различных случаях

        $this->layout = 'empty';
        return $this->render('pay_success');
    }

    public function actionPaySuccess()
    {
//        Yandex прислал в get параметрах нижеседующие данные
//        orderNumber=72871
//        orderSumAmount=750.00
//        cdd_exp_date=1225
//        shopArticleId=459558
//        paymentPayerCode=4100322062290
//        paymentDatetime=2017-08-09T12%3A47%3A20.665%2B03%3A00
//        cdd_rrn=192979674124
//        external_id=deposit
//        paymentType=AC
//        requestDatetime=2017-08-09T12%3A47%3A23.927%2B03%3A00
//        depositNumber=tmFPWWoXmbS0sumq-PwKjZ4ZbfEZ.001f.201708
//        nst_eplPayment=true
//        cdd_response_code=00
//        cps_user_country_code=PL
//        orderCreatedDatetime=2017-08-09T12%3A47%3A20.282%2B03%3A00
//        sk=u9f0f345470d9d51bbebce8b13ef43da2
//        action=PaymentSuccess
//        shopId=152258
//        scid=556747
//        rebillingOn=false
//        orderSumBankPaycash=1003
//        cps_region_id=213
//        orderSumCurrencyPaycash=10643
//        merchant_order_id=72871_090817124637_00000_152258
//        cdd_pan_mask=444444%7C4448
//        customerNumber=36126
//        yandexPaymentId=25700101894649
//        invoiceId=2000001366209

        $this->layout = 'empty';
        return $this->render('pay_success');
    }

    public function actionPayFailed()
    {
        $this->layout = 'empty';
        if (Yii::$app->request->isGet) {
            $params = Yii::$app->request->get();
        } else {
            $params = Yii::$app->request->post();
        }

        return $this->render('pay_failed', ['params' => $params]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'empty';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $scheme = Yii::$app->params['scheme'] ?? 'https';
            $scheme = str_replace('://', '', $scheme);
            $this->redirect(Url::to('/', $scheme));
//            return $this->goHome();
            // return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'title' => '«Димео.Подписка» - Панель издателя',
            ]);
        }
    }

    public function actionPasswordIsLost()
    {
        $this->layout = 'empty';
        $model = new RecoverPasswordForm();
        $message = false;
        $done = false;
        if (Yii::$app->getRequest()->getIsPost()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user = User::findOne(['Email' => trim($model->email)]);
                if (empty($user)) {
                    $model->addError('User_ID', 'Пользователь не найден.');
                } else {
                    $user->trigger(User::PASSWORD_IS_LOST_EVENT_NAME);
                    $message = 'На указанную Вами почту в ближайшее время будет отправлено письмо с инструкциями по восстановлению пароля. Проверяйте почту.';
                    $done = true;
                }
                // do email send
            }
        }
        return $this->render('forgotPwd', [
            'title' => '«Димео.Подписка» - восстановление пароля',
            'model' => $model,
            'message' => $message,
            'done' => $done,
        ]);
    }

    public function actionResetPassword()
    {
        $hash = Yii::$app->getRequest()->getQueryParam('hash', false);
        $user = User::findByPasswordResetHash($hash);
        $newPassword = Yii::$app->security->generateRandomString(8);
        $user->setPassword($newPassword);
        $user->Auth_Hash = NULL;
        $user->update(false);

        $pwdResetEvent = new ARsingleAttrChangedEvent(
            [
                'attrName' => 'Password',
                'oldValue' => 'Неизвестен',
                'newValue' => $newPassword
            ]);
        $user->trigger('passwordChanged', $pwdResetEvent);
        $this->layout = 'empty';
        return $this->render('resetPwd', [
            'title' => '«Димео.Подписка» - сброс пароля',
            'message' => 'Ваш пароль сброшен. Старый пароль теперь недействителен. В ближайшее время Вам будет отправлено письмо с новым паролем. Проверяйте почту.'
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(Url::to('/site/login', DEF_SCHEMA));

        return $this->goHome();
    }

    public function actionTest()
    {
        $orderId = 5;
        $order = NxOrder::findOne(['id' => $orderId]);
        $order->payment_type_id = PaymentType::PAYMENT_TYPE_FREE;
        $order->save(false);

        // Тестовая отправка письма
//        $publisher = Publisher::findOne(['id' => 2]);
//        CustomHelpers::configureMailerComponent(intval($publisher->id));
//        $dp = new EmailNoticeTestDataProvider();
//        $data = $dp->forUserRolesChanged();
//        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
//        $message = Yii::$app->mailer->compose('@common/mail/userRolesChanged/6', $data);
//        $message->setFrom($senderAddr)
//            ->setTo('1970naur@gmail.com')
//            ->setSubject('Тестовое письмо');
//        $r = $message->send();
        // END Тестовая отправка письма


//        $notShippedIssueIds = Issue::getLastNotShippedPdfIssuesIds(50);
//        $issues = Issue::findAll(['Message_ID' => $notShippedIssueIds]);
//        $event = new CustomIssueEvent();
//        foreach ($issues as $issue) {
//            $publisherId = $issue->magazine->ownerid->publisher_id;
//            $issue->attachBehavior('uploadFile', [
//                'class' => '\yiidreamteam\upload\FileUploadBehavior',
//                'attribute' => 'issue_file',
//                'filePath' => Yii::$app->params['issueFiles']['path'] . $publisherId . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
//                'fileUrl' => '/download/issuefile/' . $publisherId . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
//            ]);
//
//            if ($issue->isPdfLoaded()) {
//                $event->issue = $issue;
//                $issue->trigger($issue::FOUND_NOT_SHIPPED_PDF, $event);
//            }
//        }


        die('test');

    }

    public function actionTestSoapServer()
    {
        Yii::$app->response->format = 'json';
        try {
            $pId = intval(Yii::$app->getUser()->getIdentity()->publisher_id);
        } catch (Exception $e) {
            $pId = NULL;
        }

        if (!Yii::$app->request->isAjax) {
            return [];
        }
        $options = [
            'url' => Yii::$app->request->post('soap_url', ''),
            'auth' => [
                'login' => Yii::$app->request->post('soap_login', ''),
                'password' => Yii::$app->request->post('soap_password', ''),
            ],
        ];
        $soapService = Yii::$container->get('\common\services\SoapService', [$options]);

//        $err = $soapService->getError();
//        if ($err) {
//            $response = ['success' => false, 'errMessage' => $err];
//            $i = Integration1cVerify::findOne(['publisher_id' => $pId]);
//            if (!empty($i)) {
//                $i->is_verified = false;
//                $i->save(false);
//            }
//            die(json_encode($response));
//        }
        $serviceTester = new NxSoapTester($soapService);
        $serviceTester->executeTesting($pId);
        $results = $serviceTester->getTestingResults();

        return ['success' => true, 'testingResult' => $results];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'error')
                $this->layout = 'error';
            return true;
        } else {
            return false;
        }
    }

    public function actionAjaxEnabled()
    {
        $name = Yii::$app->request->post('name', false);
        $id = Yii::$app->request->post('id', false);
        $value = Yii::$app->request->post('value', null);
        $fieldName = Yii::$app->request->post('field', 'enabled');

        Yii::$app->response->format = 'json';

        if (!$name || !$id || $value === null) {
            return ['success' => false, 'errMessage' => 'Not enough data.'];
            $value = (boolean)$value;
        }
        switch ($name) {
            case 'Publisher':
                $model = Publisher::findOne(intval($id));
                break;
            case 'AdvertsCodes':
                $model = AdvertsCodes::findOne(intval($id));
                break;
            case 'MarketBlock':
                $model = MarketBlock::findOne(intval($id));
                break;
            case 'User':
                $model = User::findOne(intval($id));
                if ($model->isAdmin() && !Yii::$app->getUser()->getIdentity()->isAdmin()) {
                    return ['success' => false, 'errMessage' => 'this action allowed to admins only'];
                }
                break;
            case 'Price':
                $model = NxPrice::findOne(intval($id));
                break;
            case 'RelationSystems':
                $model = RelationSystems::findOne(intval($id));
                break;
            default:
                return ['success' => false, 'errMessage' => 'Unknown model name'];
                break;
        }
        if (empty($model))
            return ['success' => false, 'errMessage' => 'Record not found.'];

        // $model->enabled = intval(!(boolean) $model->enabled);// ($value == true) ? 1 : 0;
        $model->setAttribute($fieldName, intval(!(boolean)$model->getAttribute($fieldName)));

        if ($model->update(false, [$fieldName])) {
            return ['success' => true, 'id' => $id, 'checked' => $model->getAttribute($fieldName)];
        } else {
            return ['success' => false, 'errMessage' => 'Update failed', 'e' => $model->errors, 'enabled' => $model->getAttribute($fieldName)];
        }
    }

    public function actionGetReceipt()
    {
        $orderHash = Yii::$app->request->get('hash', false);
        if (!$orderHash) {
            throw new NotFoundHttpException('Не указаны данные заказа');
        }

        $order = NxOrder::findOne(['hash' => $orderHash]);

        if ($order == null) {
            throw new NotFoundHttpException('заказ не найден');
        }
        $this->layout = 'empty';

        return $this->render('receiptBlank', ['order' => $order]);
    }

}
