<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
    $formId = Yii::$app->params['subscribeFormId'];// 'mrof-ebircsbus123';
?>
<style>
    #<?=$formId;?> .main-container {
        width: 780px;
        /*	border:1px solid black;*/
        overflow: hidden;
        margin: 0 auto;
        font-family: Arial;
    }

    #<?=$formId;?> .reg-sel {
        height: 32px;
        width: 100%;
        /*margin-top: 1px;*/
        background: transparent;
        line-height: 1;
        border: 1px solid #d7d5d5;        
        border-radius: 1px;
        -webkit-appearance: none;
        padding-left: 4px;
        font-weight: 100;
        background: url(<?php echo yii\helpers\Url::to('@domain', Yii::$app->params['scheme'])?>/images/ar.jpg) no-repeat right;
    }
    #<?=$formId;?> .reg-sel:focus {
        border-color: #3c8dbc;
        box-shadow: none;
        outline: none;        
    }

    #<?=$formId;?> .btn-promo-code{
        padding: 5px 20px;
        border: none;
        background: #0167d5;
        color: white;
        font-size: 15px;
        margin-top: 21px;
        text-align: center;
    }
    #<?=$formId;?> .btn-promo-code a{
    color: #fff;
        font-size: 14px;
        text-decoration: none;
    }


    #<?=$formId;?> .form-pic {
        width: 20%;
        height: 200px;
        background-size: contain;
        background-position: center;
        margin-top: 20px;
    }

    #<?=$formId;?> .picture {
        max-width: 100%;
        height: auto;
        display: block;
    }

    #<?=$formId;?> .form-header {
        margin-bottom: 20px;
        overflow: hidden;
    }

    #<?=$formId;?> .header-block {
        width: 70%;
        margin-left: 5%;
    }

    #<?=$formId;?> .form-pic, .header-block {
        float: left;
    }

    #<?=$formId;?> .header-text {
        font-size: 25px;

    }

    #<?=$formId;?> .name {
        font-weight: 900;
    }

    #<?=$formId;?> .version, 
    #<?=$formId;?> .subscride, 
    #<?=$formId;?> .block-summa {
        margin-top: 30px;
        font-weight: 600;
    }

    #<?=$formId;?> .version-button {
        height: 30px;
    }

    #<?=$formId;?> .version-button:focus {
        background-color: blue;
        color: white;
        outline: none;
        border: none;
        height: 30px;
    }

    #<?=$formId;?> .act {
        background-color: blue;
        color: white;
        outline: none;
        border: none;
        height: 30px;
    }

    #<?=$formId;?> .kolvo-month,
    #<?=$formId;?> .month {
        margin-left: 10px;
    }

    #<?=$formId;?> .styled-select {
        /*	height: 26px!important;*/
        display: inline-block;
        background: url(<?php echo yii\helpers\Url::to('@domain', Yii::$app->params['scheme'])?>/images/ar.jpg) no-repeat right;

    }

    #<?=$formId;?> .styled-select select {
        width: 50px;
        background: transparent;
        line-height: 1;
        /*border: 1px solid rgb(169, 169, 169);*/
        border: 1px solid #d7d5d5;        
        border-radius: 1px;
        height: 32px;
        -webkit-appearance: none;
        padding-left: 4px;
        font-weight: 100;
        /*margin-top: 1%;*/
    }
    #<?=$formId;?> .styled-select select:focus {
        border-color: #3c8dbc;
        box-shadow: none;
        outline: none;        
    }
    #<?=$formId;?> .styled-select-2 select {
        width: 165px;
        /*margin-top: 1%;*/
    }

    #<?=$formId;?> span.styled-select.styled-select-2 {
        height: 31px !important;
    }

    #<?=$formId;?> .select-img {
        /*	background: url(ar.jpg) no-repeat right;
            width: 23px;
            height: 26px;
            position: absolute;
            right: 0px;*/
    }

    #<?=$formId;?> .kolvo-month, .month {

        height: 25px;
        overflow: hidden;
        /*width: 240px;*/
    }

    #<?=$formId;?> .month {
        width: 200px;
    }

    #<?=$formId;?> .summa {
        color: red;
    }

    #<?=$formId;?> .dannie {
        font-weight: 600;
    }

    #<?=$formId;?> .row-1,
    #<?=$formId;?> .row-2, 
    #<?=$formId;?> .row-3, 
    #<?=$formId;?> .row-4, 
    #<?=$formId;?> .row-5 {
        width: 100%;
        float: left;
        margin-top: 2%;
    }

    #<?=$formId;?> .form-block {
        float: left;
        margin-right: 3%;
        position: relative;
    }

    #<?=$formId;?> .main-form {
        overflow: hidden;
    }

    #<?=$formId;?> input {
        border: 1px solid #d7d5d5;
        border-radius: 1px;
        font-size: 14px;
        width: 100%;
        height: 32px;
        padding-left: 8px;

    }
    #<?=$formId;?> input:focus {
        border-color: #3c8dbc;
        outline:none;
    }
    #<?=$formId;?> #oform-zak {
        padding: 10px 10px;
        border: none;
        background: #0167d5;
        border: none;
        color: white;
        font-size: 15px;
        position:relative;
    }
    
    #<?=$formId;?> #oform-zak.loading:after {
        content:'';
        position:absolute;
        top: 0px;
        left : 100%;
        width:30px;
        height : 100%;
        background : url(<?=Yii::$app->params['api']['host'];?>/images/ajax-loader.gif) no-repeat center;
    }

    #<?=$formId;?> #oform-zak:focus,
    #<?=$formId;?> #oform-zak:active{
        border:none;
        outline:none;
    }
    
    #<?=$formId;?> #podpiskaUr {
        padding: 10px 10px;
        border: none;
        background: #ffffff;
        border: 1px solid black;
        color: black;
        font-weight: 600;
        font-size: 15px;
    }

    #<?=$formId;?> .hiddenBlock {
        display: none;
    }

    #<?=$formId;?> .form {
        overflow: hidden;
    }

    #<?=$formId;?> .buttons {
        width: 98%;
        margin: 2% auto;
    }

    #<?=$formId;?> #podpiskaUr {
        float: right;
    }

    #<?=$formId;?> .star {
        color: red;
    }

    #<?=$formId;?> .first {
        width: 24%;
    }

    #<?=$formId;?> .second {
        width: 15%;
    }

    #<?=$formId;?> .third {
        width: 52%;
    }

    #<?=$formId;?> .four {
        width: 10%;
    }

    #<?=$formId;?> .five {
        width: 29%;
    }

    #<?=$formId;?> .six {
        width: 47%;
    }

    #<?=$formId;?> .six2 {
        width: 49%;
        margin-bottom: 2%;
    }

    #<?=$formId;?> .seven {
        width: 13%;
    }

    #<?=$formId;?> .question-pic {
        position: absolute;
        top: -6px;
        right: -4px;
        width: 15px;
    }

    #<?=$formId;?> .hiddenBlock .six {
        margin-bottom: 2%;
    }

    #<?=$formId;?> .form-block label {
        font-size: 12px;
    }

    @media screen and (min-width: 100px)  and (max-width: 920px) {
        #<?=$formId;?> .main-container {
            width: 100%;
        }

        #<?=$formId;?> .row-1 .first {
            width: 35%;
        }

        #<?=$formId;?> .row-1 .third {
            width: 41%;
        }

        #<?=$formId;?> .month {
            margin-top: 1%;
        }
    }

    @media screen and (min-width: 100px)  and (max-width: 610px) {
        #<?=$formId;?> .main-container {
            width: 100%;
            margin-top: 20px;
        }

        #<?=$formId;?> .row-1 .first {
            width: 41%;
        }

        #<?=$formId;?> .row-1 .third {
            width: 35%;
        }

        #<?=$formId;?> .buttons {
            width: 100%;
        }

        @media screen and (min-width: 100px)  and (max-width: 580px) {
            #<?=$formId;?> .form-block {
                width: 100% !important;
            }

            #<?=$formId;?> .form-block input {
                width: 99% !important;
            }

            #<?=$formId;?> .form-block {
                margin-bottom: 2%;
            }

            #<?=$formId;?> .form-pic {
                height: auto;
                float: none;
                margin: 3% auto;
                width: 35%;
            }

            #<?=$formId;?> .header-block {
                width: 100%;
            }
        }
</style>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => $formId,
    'action' => yii\helpers\Url::to('@domain/subscribe/create-order',Yii::$app->params['scheme']),
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
]);
?>


<div class="main-container">
    <div class="form-header">
        <div class="form-pic">
            <?php $cover = $journal->lastCoverUrl();?>
            <?php if (!empty($cover)): ?> 
                <img src="<?php echo $cover; ?>" alt="" class="picture">
            <?php endif; ?>
        </div>
        <div class="header-block">
            <span class="header-text">
                Подписка на журнал<br/>
                <span class="name">"<?php echo $journal->full_name ?>"</span><br/>
            </span>
            <div class="version">
                Версии
                <button type="button" class="version-button act" id="paper">Печатное</button>
                <button type="button" class="version-button" id="pdf">PDF</button>
                <input type="hidden" name="SubscribeForm[delivery_type_id]" id="dev_type" value="1">
                <input type="hidden" name="SubscribeForm[subscriber_type_id]" id="sub_type" value="1">
            </div>
            <div class="subscride">
                <label>Подписка на</label>
                <span class="styled-select">
                    <select class="kolvo-month" id="period" name="SubscribeForm[period]">
                        <?php if (!empty($journal->editionPeriods)): ?>
                            <?php foreach ($journal->editionPeriods as $k => $item): ?>
                                <option value="<?php echo $item['period']; ?>"<?php echo ($item['default'] == 1) ? 'selected' : '' ?>><?php echo $item['period']; ?></option>
                            <?php endforeach; ?>
                        <?php endif ?>
                    </select>

                </span>
                <span class="styled-select styled-select-2" style="padding-left: 10px">
                    <label>месяцев с</label>
                    <select class="month" id="date_start" name="SubscribeForm[date_start]">
                        <?php if (!empty($model->date_start)): ?>
                            <?php foreach ($model->date_start as $k => $item): ?>
                                <option value="<?php echo $k; ?>"<?php echo ($item === reset($model->date_start)) ? 'selected' : '' ?>>
                                    <?php echo $item; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif ?>
                    </select>
                </span>
            </div>
            <div class="block-summa">
                Сумма к оплате <span class="summa" id="price">0</span> руб <span id="release"></span>
            </div>
        </div>
    </div>
    <hr style="border: 1px solid;margin-bottom: 30px;">
    <div class="main-form">
        <span class="dannie">Данные для доставки:</span>
        <form action="" class="form">
            <div class="row-1">
                <div class="form-block first">
                    <label for="">Укажите Фамилию и Имя: <span class="star">*</span></label><br>
                    <?php echo $form->field($model, 'username', ['options' => []])->textInput([ 'class' => 'block-1-input-1 form-control', 'required' => 'required'])->label(false); ?>
                </div>
                <div class="form-block second">
                    <label for="">Ваш телефон:<span class="star">*</span>
                        <!--
                        <img class="question-pic" src="/assets/question.png" alt="">
                        -->
                    </label><br>
                    <?php echo $form->field($model, 'phone', ['options' => []])->input('phone', ['required' => 'required', 'class' => 'form-control block-1-input-2'])->label(false); ?>
                </div>
                <div class="form-block third">
                    <label for="">E-mail: <span class="star">*</span></label><br>
                    <?php echo $form->field($model, 'email', ['options' => []])->input('email', ['required' => 'required', 'class' => 'form-control block-1-input-3'])->label(false); ?>
                </div>
            </div>
            <div class="row-2">
                <div class="form-block four">
                    <label for="">Индекс:</label><br>
                    <?php echo $form->field($model, 'index', ['options' => []])->textInput([ 'class' => 'form-control block-2-input-1'])->label(false); ?>
                </div>
                <div class="form-block five">
                    <input type="hidden" name="SubscribeForm[country_id]" value="165" id="country-id"/>
                    <label for="">Регион:</label><br>
                    <?php
                    $items = yii\helpers\ArrayHelper::map(\common\models\Subscribe::getRegions(), 'RegionUser_ID', 'RegionUser_Name');
                    ?>
                    <select class="reg-sel" name="SubscribeForm[region]" required="required" aria-required="true">
                        <option value=' ' selected></option>
                        <?php if (!empty($items)): ?>
                            <?php foreach ($items as $k => $item): ?>
                                <option value=" <?php echo $item; ?>" <?php //echo ($item === reset($model->date_start)) ? 'selected' : ''  ?>><?php echo $item ?></option>
                            <?php endforeach; ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="form-block third">
                    <label for="">Район:</label><br>
                    <?php echo $form->field($model, 'region1', ['options' => []])->textInput(['placeholder' => '', 'class' => 'form-control block-2-input-3'])->label(false); ?>
                </div>
            </div>

            <div class="row-3">
                <div class="form-block six">
                    <label for="">Город:<span class="star">*</span></label><br>
                    <?php echo $form->field($model, 'city')->textInput(['required' => 'required', 'class' => 'form-control',])->label(false); ?>
                </div>
                <div class="form-block six">
                    <label for="">Входящий нас.пункт:</label><br>

                    <?php echo $form->field($model, 'city1')->textInput(['placeholder' => '', 'class' => 'form-control'])->label(false); ?>

                </div>
            </div>

            <div class="row-4">
                <div class="form-block six">
                    <label for="">Улица:<span class="star">*</span></label><br>

                    <?php echo $form->field($model, 'street', ['options' => []])->textInput(['required' => 'required', 'class' => 'form-control'])->label(false); ?>

                </div>
                <div class="form-block seven">
                    <label for="">Дом:<span class="star">*</span></label><br>

                    <?php echo $form->field($model, 'home', ['options' => []])->textInput(['required' => 'required', "pattern" => "\d*", 'class' => 'form-control'])->label(false); ?>

                </div>
                <div class="form-block seven">
                    <label for="">Корпус:</label><br>

                    <?php echo $form->field($model, 'housing', ['options' => []])->textInput(['placeholder' => '', 'class' => 'form-control', "pattern" => "\d*"])->label(false); ?>

                </div>
                <div class="form-block seven">
                    <label for="">Квартира:</label><br>

                    <?php echo $form->field($model, 'flat', ['options' => []])->textInput(['placeholder' => '', 'class' => 'form-control', "pattern" => "\d*"])->label(false); ?>

                </div>
            </div>

            <div class="row-4">
                <div class="form-block six">
                    <label for="">Введите промокод (если есть):</label><br>
                    <?php echo $form->field($model, 'promocode', ['options' => ['id' => 'promocode']])->textInput(['placeholder' => '', 'class' => 'form-control'])->label(false); ?>
                </div>

                <div class="form-block seven">
                    <div class="btn-promo-code">
                        <a href="#" id="promoSubmit">Применить</a>
                    </div>
                </div>


                <?php echo $form->field($model, 'discount', ['inputOptions' => ['id' => 'discount']])->hiddenInput(['value' => ""])->label(false); ?>
                <?php echo $form->field($model, 'magazineID')->hiddenInput(['value' => "$magazineID"])->label(false); ?>
                <?php echo $form->field($model, 'price_id', ['inputOptions' => ['id' => 'price_id']])->hiddenInput(['value' => ""])->label(false); ?>
                <?php echo $form->field($model, 'publisher_id', ['inputOptions' => ['id' => 'publisher_id']])->hiddenInput(['value' => "$publisher_id"])->label(false); ?>


            </div>


    </div>
    <hr style="border: 1px solid;margin-top: 30px;">
    <div class="hiddenBlock">
        <div class="row-5">
            <?php echo $form->field($model, 'company_name', ['options' => ['id' => 'company']])->textInput(['placeholder' => 'Название компании', 'class' => 'six2'])->label(false); ?>
            <?php echo $form->field($model, 'legal_address', ['options' => ['id' => 'company']])->textInput(['placeholder' => 'Юридический адрес', 'class' => 'six2'])->label(false); ?>
            <?php echo $form->field($model, 'inn_code', ['options' => ['id' => 'company']])->input('phone', ['placeholder' => 'ИНН', 'class' => 'six2', "pattern" => "\d*"])->label(false); ?>
            <?php echo $form->field($model, 'kpp_code', ['options' => ['id' => 'company']])->input('phone', ['placeholder' => 'КПП', 'class' => 'six2', "pattern" => "\d*"])->label(false); ?>
        </div>

    </div>


    <div class="buttons">
        <button type="submit" id="oform-zak">Оформить заказ</button>
        <button type="button" id="podpiskaUr" value="">Подписка Юр.лицо</button>
    </div>
    <div class='errors-place'>
        <ul class='errors-list' style="list-style-type: none;padding: 0px;"></ul>
    </div>
</div>

<?php ActiveForm::end(); ?>

