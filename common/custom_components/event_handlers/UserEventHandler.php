<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\custom_components\event_handlers;
use common\models\User;
use Yii;
use \yii\base\BaseObject;
use \common\models\IssueDownloadFile;
use \common\queue_jobs\SendPdfIssueJob;
use \yii\base\ExitException;
use \common\models\EmailNotice;


class UserEventHandler extends BaseObject
{
    public static function PasswordIsLostHandler (\yii\base\Event $event)
    {
        /** @var User $user  */
        $user = $event->sender;
        $unique = md5(uniqid('lostpwd', true));
        $user->generateAuthKey(50);
        $user->update(false);
        $publisher = $user->publisher;
        if (empty($publisher)) {
            return false;
        }

        $emailNoticeType = \common\models\EmailNotice::findOne(['key_string' => $event->name]);
        if (empty($emailNoticeType)) {
            return false;

        }
        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();
        if (empty($emailNoticeTemplate)) {
            return false;
        }

        $dataForTemplate = [
//            'user' => $user,
            'user_id' => $user->User_ID,
//            'publisher' => $user->publisher,
            'publisher_id' => $user->publisher->id,
            'resetPasswordLink' => $user->generatePasswordRecoveryLink()
        ];

        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => $dataForTemplate,
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => Yii::$app->params['noAnswerEmail'],
            'toEmail' => $user->Email,
        ];
        try {
            self::sendEmail($emailJobData, intval($publisher->id));
        } catch (ExitException $e) {
            return false;
        }
        return true;
    }

    private static function sendEmail($data, $publisherId)
    {
        $job = new \common\queue_jobs\EmailNotificationJob($publisherId, $data);
        if ($job->validate()) {
            $queue = \Yii::$app->queue;
            $queue->push($job);
//            $job->execute($queue);
        } else {
            $errAttrs = array_keys($job->getErrors());
            $errMessage = 'incorrect data was passed to EmailNotificationJob model. There are errors in attributes: ' . implode(',', $errAttrs);
            throw new \yii\base\ExitException($errMessage);
        }
    }

}