<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NxPerson;
use yii\helpers\Url;

/**
 * NxPersonSearch represents the model behind the search form about `common\models\NxPerson`.
 */
class NxPersonSearch extends NxPerson
{
    public  $company;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'position_type_id', 'address_id', 'channel_id', 'source_id', 'checked', 'enabled', 'create_user_id', 'last_user_id', 'xpress_id'], 'integer'],
            [['position', 'speciality', 'phone', 'telcode', 'email', 'name', 'f', 'i', 'o', 'gender', 'comment', 'created', 'last_updated', 'label', 'xpress_type','company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NxPerson::find();
        $query->alias('P');
        $query->joinWith(['company C']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'company' => [
                        'asc' => ['C.name' => SORT_ASC],
                        'desc' => ['C.name' => SORT_DESC],
                    ],
                    'position',
                    'speciality',
                    'phone',
                    'name'
                ],
                'route' => Url::to(['person/index']),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'position_type_id' => $this->position_type_id,
            'address_id' => $this->address_id,
            'channel_id' => $this->channel_id,
            'source_id' => $this->source_id,
            'checked' => $this->checked,
            'enabled' => $this->enabled,
            'created' => $this->created,
            'last_updated' => $this->last_updated,
            'create_user_id' => $this->create_user_id,
            'last_user_id' => $this->last_user_id,
            'xpress_id' => $this->xpress_id,
        ]);

        $query->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'speciality', $this->speciality])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'telcode', $this->telcode])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'P.name', $this->name])
            ->andFilterWhere(['like', 'f', $this->f])
            ->andFilterWhere(['like', 'i', $this->i])
            ->andFilterWhere(['like', 'o', $this->o])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'xpress_type', $this->xpress_type])
            ->andFilterWhere(['like', 'C.name', $this->company]);

        return $dataProvider;
    }
}
