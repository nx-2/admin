<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\EmailNotice */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin(); ?>
<div id='email-notice-form-wrapper' class='box box-primary email-notice-form'>
    <div class='box-header with-border'>
        <h3 class='box-title'>
            Тип Email уведомления
        </h3>
    </div>
    
    <div class='box-body'>
        <div class="row">
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email_subject')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'key_string')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
    <div class='box-footer'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
            <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php 
            if (!$model->isNewRecord) {
                echo (Html::a('Удалить', Url::to(['email-notice/delete', 'id' => $model->id]), ['class' => 'btn btn-danger pull-right', 'onclick' => 'if(!confirm("Вы действительно хотите удалить этот тип уведомлений?")) {return false;}']));
            }
            ?>

        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>