<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RelationSystems */

$this->title = 'Добавление платежной системы';
$this->params['breadcrumbs'][] = ['label' => 'Платежные системы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relation-systems-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
