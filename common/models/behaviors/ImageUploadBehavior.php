<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\behaviors;

use Yii;
use Closure;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\base\InvalidParamException;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;

use yii\validators\ImageValidator;

class ImageUploadBehavior extends Behavior 
{
	public $attribute = null;
	public $fileField = null;
	
	private $fileOptions = false;
	private $resizedImages;

	public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }
    public function attach($owner)
    {
    	parent::attach($owner);

    	if ($this->attribute == null || $this->fileField == null)
    		throw new InvalidParamException('При вызове ImageUploadBehavior не были указаны значения <attribute> и <fileField>');
    	
    	$className = explode( '\\', $owner::className() );
    	$className = end( $className );

    	$allParams = Yii::$app->params;
    	
    	if (
    		isset($allParams['files']) && 
    		isset($allParams['files'][$className]) &&
    		isset($allParams['files'][$className][$this->attribute])
		)
    		$this->fileOptions = Yii::$app->params['files'][$className][$this->attribute];
    	else 
    		throw new InvalidParamException('В конфигурации приложения не найдено где хранятся файлы для атрибута ' . $this->attribute . ' модели ' . $className);

		
    }
    public function afterInsert($event)
    {
			//if new record have just created we should move temp imageFile into correct place (directory named as id of new record) and then delete it
			$fileNames = $this->buildImageNames();

			$tempFileName = $this->getImagePath($temp = true);
			if (file_exists($tempFileName)) {
				rename ($tempFileName, $fileNames->filePath);
				$this->owner->{$this->attribute} = $fileNames->fileUrl;
				$this->owner->update();
				
				if (isset($this->fileOptions['resize']))
					$resizedFiles  = Yii::$app->imageResizer->generateRisizedImages($fileNames->filePath, $this->fileOptions['resize']);
			}

    }
	public function getResizedImages()
	{
		if ($this->resizedImages !== null)
			return $this->resizedImages;

		$filePath = $this->owner->{$this->attribute};
		$temp = explode('/', $filePath);
		$filePath = end( $temp );
		$filePath = $this->fileOptions['root_path'] . $this->fileOptions['uri'] . '/' . $this->owner->getPrimaryKey() . '/' . $filePath;

		$this->resizedImages = Yii::$app->imageResizer->getResizedImages($this->owner->{$this->attribute}, $filePath);
		
		return $this->resizedImages;
	}

    public function afterDelete($event)
    {
		$this->deleteOldImagesFiles();
    }
	public function uploadImage()
	{
		$file = UploadedFile::getInstance($this->owner, $this->fileField);
		
		if ( (!$file || empty($file)) )
					return $this->owner->validate();

		$this->owner{$this->fileField} = $file;
		
		if ($this->owner->isNewRecord) {
			//create
			$fileNames = $this->buildImageNames(true); //временный файл
		} else {
			//update
			$this->deleteOldImagesFiles();
			$fileNames = $this->buildImageNames();

			if ( Yii::$app->request->getBodyParam('delete-file', false) ) 
				Yii::$app->request->setBodyParams(['delete-file' => 0]);
		}

		$this->owner->{$this->attribute} = $fileNames->fileUrl;
		// $this->image_full_path = $fileNames->filePath;

		if ($this->owner->validate()) {
			$this->owner->{$this->fileField}->saveAs($fileNames->filePath, false);
			
			if (!$this->owner->isNewRecord)
				$resizedFiles  = Yii::$app->imageResizer->generateRisizedImages($fileNames->filePath, $this->fileOptions['resize']);
			
			return true;
		} else {
			return false;
		}
	}
	public function getImagePath($temp = false)
	{
		$temp = !is_bool($temp) ? false : $temp;
                
                if (!$this->owner->{$this->attribute} || empty($this->owner->{$this->attribute}))
			return '';
		$temp = explode('/', $this->owner->{$this->attribute});
		$fileName = end( $temp );
                $folder = !$temp ? $this->owner->getPrimaryKey() : 'temp';

                $fileName = $this->fileOptions['root_path'] . $this->fileOptions['uri'] . '/' . $folder . '/' . $fileName;
                    
		return file_exists($fileName) ? $fileName : '';

	}
	private function buildTempImageNames()
	{
		$toReturn = new \stdClass();
		$toReturn->fileUrl = '';
		$toReturn->filePath = '';
		
		if (
			$this->owner{$this->fileField} == null || 
			!($this->owner{$this->fileField} instanceof UploadedFile) 
		) 
			return $toReturn;

		$fileOptions = $this->fileOptions;
		
		$helper = Yii::$app->customHelpers;
		$fileName = $helper::transliterate($this->owner{$this->fileField}->baseName) . '_(' . time() . ')' .  '.' . $this->owner{$this->fileField}->extension;

		$filePath = $fileOptions['root_path'] . $fileOptions['uri'];
		if( !is_dir($filePath) )
			mkdir ($filePath, 0755);



		$filePath = $filePath .  '/temp/';
		$fileUrl = $fileOptions['host'] . $fileOptions['uri'] .  '/temp/';
		
		if (!is_dir($filePath))
			mkdir ($filePath, 0755);

		$toReturn->fileUrl = $fileUrl . $fileName;
		$toReturn->filePath = $filePath . $fileName;
		
		return $toReturn;
	}
	/** 
	* @return object
	* Служебный метод для построения полного имени и УРЛ файла  изображения, присланного пользователем
	* Место, где должны храниться изображения, указано в конфигурации в разделе files. Там же хранится хост, по которому
	* можно будет получить доступ к указанным папкам. Метод используется при обновлении записи
	*
	**/
	private function buildImageNames($temp = false)
	{
		$temp = !is_bool($temp) ? false : $temp;
		$toReturn = new \stdClass();
		$toReturn->fileUrl = '';
		$toReturn->filePath = '';
		
		if ($this->owner{$this->fileField} == null || !($this->owner{$this->fileField} instanceof UploadedFile) ) {
			return $toReturn;
		}

		$fileOptions = $this->fileOptions;
		$helper = Yii::$app->customHelpers;

		
		$filePath = $fileOptions['root_path'] . $fileOptions['uri'];
		if( !is_dir($filePath) )
			mkdir ($filePath, 0755, true);

		if ($temp) {
			$fileName = $helper::transliterate($this->owner{$this->fileField}->baseName) . '_(' . time() . ')' .  '.' . $this->owner{$this->fileField}->extension;
			$filePath = $filePath .  '/temp/';
			$fileUrl = $fileOptions['host'] . $fileOptions['uri'] .  '/temp/';

		} else {
			$fileName = $helper::transliterate($this->owner{$this->fileField}->baseName) .  '.' . $this->owner{$this->fileField}->extension;
			$filePath = $filePath .  '/' . $this->owner->getPrimaryKey() . '/';
			$fileUrl = $fileOptions['host'] . $fileOptions['uri'] .  '/' . $this->owner->getPrimaryKey() . '/';
		}
		
		if (!is_dir($filePath))
			mkdir ($filePath, 0755, true);

		$toReturn->fileUrl = $fileUrl . $fileName;
		$toReturn->filePath = $filePath . $fileName;
		
		return $toReturn;
	}
	public function deleteOldImagesFiles()
	{
		if (empty($this->owner->getPrimaryKey()))
			return;
		
		$fileOptions = $this->fileOptions;
		
		$dir = $fileOptions['root_path'] . $fileOptions['uri'] .  '/' . $this->owner->getPrimaryKey();
		if (is_dir($dir)){
			system("rm -rf ".escapeshellarg($dir), $r);
		}
		return;
	}

}