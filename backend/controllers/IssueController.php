<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use common\models\events\CustomIssueEvent;
use common\models\events\SendNotShippedRequestedEvent;
use common\models\NxOrder;
use Yii;
use common\models\Issue;
use backend\models\IssueSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;
use yii\web\Response;

/**
 * IssueController implements the CRUD actions for Issue model.
 */
class IssueController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                        parent::behaviors()['access'],
                        [
                            // 'class' => AccessControl::className(),
                            'rules' => [
                                //залогиненым издателям разрешить кое что только
                                [
                                    'actions' => ['index','update','create','delete', 'test', 'view', 'copy', 'send-not-shipped'],
                                    'allow' => true,
//                                    'roles' => ['@'],
                                    'roles' => ['ship_manager'],
//                                    'matchCallback' => function ($rule, $action) {
//                                        $allowedRoles = ['publisher','subscribe_manager', 'admin'];
//                                        $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                        return (boolean)$allowed;
//                                    },

                                ],
                            ],
                        ]
            ),
            'verbs' => [
                'class' => VerbFilter::class,
                 'actions' => [
                     'send-not-shipped' => ['GET', 'OPTION'],
                 ],
            ],
        ];
    }

    /**
     * Lists all Issue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Issue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->getUser()->can('manageIssues') || $model->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен'); //роль не издатель или пытается изменять выпуск не своего издателя

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        $link = \common\models\IssueDownloadFile::createLink(72693 ,11547943);
        var_dump($link);
        die();
    }
    /**
     * Creates a new Issue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Issue();

        // $model->setScenario('create');

        // $pr = Yii::$app->request->post('User', ['Password_repeat' => null])['Password_repeat'];
        // $model->Password_repeat = $pr;
        
        if ($model->load(Yii::$app->request->post()) && $model->uploadImage()) {

            if ($delCurrentImage = (Boolean) Yii::$app->request->post('delete-file', false) ) {
                $model->deleteOldImagesFiles();
                $model->Picture = null;
            }

            if ($model->save(false)) {
                Yii::$app->session->addFlash('success', 'Успешно создан новый выпуск.');
                if (!empty($model->issue_file))
                    \common\models\IssueDownloadFile::createMasterLink($model->Message_ID);
            } else {
                Yii::$app->session->addFlash('danger', 'Не сохранено!');
            }

            return $this->redirect(['update', 'id' => $model->Message_ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Issue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // редактировать выпуск имеет право пользователь с правами(ролью) "Издатель"
        // если этот выпуск относится к изданию, владельцем которого является издатель текущего пользователя
        // Т.е. 1. роль = "Издатель"
        // 2. Издатель выпуска и издатель текущего юзера совпадают
        
//        if (!Yii::$app->user->identity->isRole(\common\models\User::PUBLISHER) || $model->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
        if (!Yii::$app->getUser()->can('manageIssues') || $model->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
            throw new \yii\web\ForbiddenHttpException('Прав на редактирование этого выпуска у вас НЕТ!'); //роль не издатель или пытается изменять выпуск не своего издателя

        if ($model->load(Yii::$app->request->post()) && $model->uploadImage()) {
            
            if ($delCurrentImage = (Boolean) Yii::$app->request->post('delete-file', false) ) {
                $model->deleteOldImagesFiles();
                $model->avatar = null;
            }

            if ($model->save(false))
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            else
                Yii::$app->session->addFlash('danger', 'Не сохранено!');
        } 

        return $this->render('update', [
            'model' => $model,
        ]);
    }    
    /**
     * Deletes an existing Issue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $name = $model->Name;
        if ($model  === null) {
            throw new NotFoundHttpException('The requested issue does not exist.');
        } 

//        if (!Yii::$app->user->identity->isRole(\common\models\User::PUBLISHER) || $model->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
        if (!Yii::$app->getUser()->can('manageIssues') || $model->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
            throw new \yii\web\ForbiddenHttpException('Прав на удаление этого выпуска у вас НЕТ!'); //роль не издатель или пытается изменять выпуск не своего издателя


        if( $model->delete() ) {
//            if (Yii::$app->request->isAjax) {
//                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//                return ['status' => 'OK'];
//            }
            Yii::$app->session->addFlash('success', 'выпуск "' . $name . '" удален.');
            // return $this->redirect(['index']);
            return $this->actionIndex();
        } else {
//            if (Yii::$app->request->isAjax) {
//                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//                return ['status' => 'ERR'];
//            }
            Yii::$app->session->addFlash('danger', 'Пользователь "' . $name . '" НЕ был удален.');
            return $this->redirect(['update', 'id' => $model->User_ID]);
        }

        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    public function actionCopy($type='to-next-month', $id = false)
    {
        $sourceModel = $this->findModel(intval($id));
        $model  = yii::createObject(['class' => '\common\models\Issue']);
        $model->load($sourceModel->getAttributes(), '');

        switch ($type) {
            case 'to-next-month':
                if (intval($model->release_month) == 12) {
                    $model->release_month = 1;
                    $model->year = $model->year + 1;
                    $model->Number = 1;
                } else {
                    $model->release_month = $model->release_month + 1;
                    $model->Number = $model->Number + 1;
                }
                $model->release_month = sprintf("%02d", $model->release_month);

                $model->PublicDate = $this->addMonth($model->PublicDate, 1);
                $model->StartDate = $this->addMonth($model->StartDate, 1);
                break;
            case 'to-next-year':
                $model->year = $model->year + 1;
                $model->PublicDate = $this->addYear($model->PublicDate, 1);
                $model->StartDate = $this->addYear($model->StartDate, 1);
                break;
        }

        $model->Picture = '';
        $model->issue_file = null;
        $model->status_id = 1; //готовится

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSendNotShipped(int $id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Ajax only');
        }
        $issue = Issue::findOne(['Message_ID' => intval($id)]);

        if (empty($issue)) {
            throw new BadRequestHttpException("Issue $id not found");
        }

        if (!Yii::$app->getUser()->can('manageIssues') || $issue->magazine->publisher->id != Yii::$app->user->identity->publisher_id)
            throw new \yii\web\ForbiddenHttpException('Access Denied!');

        Yii::$app->response->format = Response::FORMAT_JSON;

        $event = new CustomIssueEvent(['issue' => $issue]);

        /**
         * @see \common\custom_components\event_handlers\IssueEventHandler::onSendNotShippedRequested
         */
        $issue->trigger(Issue::SEND_NOT_SHIPPED_PDFS_REQUESTED, $event);
        return ['OK'];
    }

    /**
     * Finds the Issue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Issue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Issue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function addMonth( $dateString, $monthsNumber )
    {
        $date = strtotime($dateString);
        $newDate = strtotime('+'. $monthsNumber . ' month', $date);
        return date('Y-m-d H:i:s', $newDate);
    }
    protected function addYear( $dateString, $yearNumber )
    {
        $date = strtotime($dateString);
        $newDate = strtotime('+'. $yearNumber . ' year', $date);
        return date('Y-m-d H:i:s', $newDate);
    }
}
