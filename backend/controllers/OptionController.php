<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;
use common\models\Publisher;
use common\models\PublisherProperty;
use common\models\PublisherPropertyGroup;
use yii;
use \backend\controllers\SiteController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * @property Publisher $publisher
 */

class OptionController extends SiteController
{
    public $publisher = NULL;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                parent::behaviors()['access'],
                [
                    'rules' => [
                        //залогиненым разрешить
                        [
                            'actions' => ['accounting', 'mail', 'check-mail-connection', 'jwt','policy'],
                            'allow' => true,
                            'roles' => ['publisher'],
//                            'roles' => ['@'],
//                            'matchCallback' => function ($rule, $action) {
//                                $allowedRoles = ['publisher', 'admin'];
//                                $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                return (boolean)$allowed;
//                            },
                        ],
                    ],
                ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->publisher = Yii::$app->user->identity->publisher;
        if (empty($this->publisher)) {
            throw new yii\base\Exception('Publisher instance is required', 500);
        }

        return parent::beforeAction($action);
    }

    public function actionAccounting()
    {
        /** @var Publisher $publisher */
//        $publisher = Yii::$app->user->identity->publisher;
        $publisher = $this->publisher;
        $properties = $publisher->accountingIntegrationOptions;

        if (empty($properties)) {
            $group = PublisherPropertyGroup::findOne(['key' => PublisherPropertyGroup::ACCOUNTING_KEY]);
            $properties = $this->getEmptyModelsForGroup(intval($group->id), intval($publisher->id));

//            $group_1c = \common\models\PublisherPropertyGroup::findOne(['id' => \common\models\PublisherPropertyGroup::ACCOUNTING_INTEGRATION ]);
//            $props = $group_1c->publisherPropertyTypes;
//            $properties = [];
//            foreach( $props as $propData ) {
//                $prop = new \common\models\PublisherProperty(['publisher_id' => $publisher->id, 'property_type_id' => $propData->id]);
//                $properties[] = $prop;
//            }
        }
        if (Yii::$app->request->isPost) {
            if (
                \yii\base\Model::loadMultiple($properties,Yii::$app->request->post())
                && \yii\base\Model::validateMultiple($properties)
            ) {

                $transaction = Yii::$app->db->beginTransaction();
                $successful = true;
                foreach( $properties as $property ) {
                    $successful = $property->save(false) && $successful;

                }
                if ($successful)
                    $transaction->commit();
                else
                    $transaction->rollBack();

            } else {
                $successful = false;
            }
            if ($successful) {
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
                $options = $publisher->soapServerConnectionData();
                try {
                    $service = Yii::$container->get('\common\services\SoapService', [$options]);
                } catch (Exception $exc) {
                    // echo $exc->getTraceAsString();
                }

                $err = $service->getError();
                if (!$err) {
                    $serviceTester = Yii::$container->get('\common\services\NxSoapTester', [$service]);
                    $testOk = $serviceTester->executeTesting($this->publisher->id);
                    $verificationResulData = $serviceTester->getTestingResults();
                } else {
                    $testOk = false;
                    $verificationResulData = [
                        'functions' => [
                            ['testName' =>  $err, 'testSuccess' => false],
                        ],
                    ];
                }

                $soapVerificationModel =  \common\models\Integration1cVerify::findOne(['publisher_id' => $publisher->id]);
                if ($soapVerificationModel == null)
                    $soapVerificationModel = new \common\models\Integration1cVerify(['publisher_id' => $publisher->id ]);

                $soapVerificationModel->verification_result = serialize($verificationResulData);
                $soapVerificationModel->is_verified = $testOk ?  1 : 0;
                $soapVerificationModel->save();
                if ($testOk) {
                    Yii::$app->session->addFlash('success', 'Указанные параметры SOAP сервиса прошли проверку и будут использоваться');
                } else {
                    Yii::$app->session->addFlash('danger', 'Указанные параметры SOAP сервиса НЕ прошли проверку и использоваться НЕ будут!!');
                }
            } else {
                Yii::$app->session->addFlash('danger', 'Не сохранено.');
            }
        }

        return $this->render('accounting', ['models' => $properties]);
    }

    public function actionMail()
    {
        /** @var Publisher $publisher */
//        $publisher = Yii::$app->user->identity->publisher;
//        if (empty($publisher)) {
//            throw new yii\base\Exception('Publisher instance is required', 500);
//        }

        $publisher = $this->publisher;

        $properties = $publisher->mailOptions;

        if (empty($properties)) {
            $properties = $this->getEmptyModelsForGroup(PublisherPropertyGroup::MAIL_GROUP, intval($publisher->id));
        }

        if (Yii::$app->request->isPost) {
            $isValid = false;
            if (
                \yii\base\Model::loadMultiple($properties,Yii::$app->request->post())
                && \yii\base\Model::validateMultiple($properties)
            ) {
                $isValid = true;
                $successfullySaved = true;
                $transaction = Yii::$app->getDb()->beginTransaction();
                foreach ($properties as $property) {
                    $successfullySaved = $property->save('false') && $successfullySaved;
                }
                if ($successfullySaved) {
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', 'Почтовые настройки успешно сохранены.');
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('danger', 'Ошибки при сохранении.');
                }
            } else {
                Yii::$app->session->addFlash('danger', 'Ошибки. Не сохранено.');
            }
        }

        return $this->render('mail', ['mailOptions' => $properties]);
    }

    public function actionJwt()
    {
        /** @var Publisher $publisher */
//        $publisher = Yii::$app->user->identity->publisher;
//        if (empty($publisher)) {
//            throw new yii\base\Exception('Publisher instance is required', 500);
//        }

        $publisher = $this->publisher;
        $properties = $publisher->jwtOptions;

        if (empty($properties)) {
            $jwtGroup = PublisherPropertyGroup::findOne(['key' => PublisherPropertyGroup::JWT_KEY]);
            $properties = $this->getEmptyModelsForGroup(intval($jwtGroup->id), intval($publisher->id));
        }

        if (Yii::$app->request->isPost) {
            $isValid = false;
            if (
                \yii\base\Model::loadMultiple($properties,Yii::$app->request->post())
                && \yii\base\Model::validateMultiple($properties)
            ) {
                $isValid = true;
                $successfullySaved = true;
                $transaction = Yii::$app->getDb()->beginTransaction();
                foreach ($properties as $property) {
                    $successfullySaved = $property->save('false') && $successfullySaved;
                }
                if ($successfullySaved) {
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', 'успешно сохранено.');
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('danger', 'Ошибки при сохранении.');
                }
            } else {
                Yii::$app->session->addFlash('danger', 'Ошибки. Не сохранено.');
            }
        }

        return $this->render('jwt', ['options' => $properties]);



    }

    public function actionCheckMailConnection()
    {
        $data = Yii::$app->request->post('PublisherProperty');
        if (empty($data)) {
            throw new yii\web\BadRequestHttpException('data required', 500);
        }

        /** @var Publisher $publisher */
//        $publisher = Yii::$app->user->identity->publisher;
//        if (empty($publisher)) {
//            throw new yii\base\Exception('Publisher instance is required', 500);
//        }

        $publisher = $this->publisher;
        $properties = $publisher->mailOptions;
        $result = [
            'success' => false,
            'message' => ''
        ];
        if (!\yii\base\Model::loadMultiple($properties,Yii::$app->request->post())) {
            $result['message'] = 'supported data was not load as models';
            return $result;
        }

        Yii::$app->getResponse()->format = yii\web\Response::FORMAT_JSON;

        return Publisher::checkEmailServerConnection($properties);
    }

    public function actionPolicy()
    {
        $properties = $this->publisher->policyOptions;

        if (empty($properties)) {
            $group = PublisherPropertyGroup::findOne(['key' => PublisherPropertyGroup::POLICY_KEY]);
            $properties = $this->getEmptyModelsForGroup(intval($group->id), intval($this->publisher->id));
        }
        if (Yii::$app->request->isPost) {
            $isValid = false;
            if (
                \yii\base\Model::loadMultiple($properties,Yii::$app->request->post())
                && \yii\base\Model::validateMultiple($properties)
            ) {
                $isValid = true;
                $successfullySaved = true;
                $transaction = Yii::$app->getDb()->beginTransaction();
                foreach ($properties as $property) {
                    $successfullySaved = $property->save('false') && $successfullySaved;
                }
                if ($successfullySaved) {
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', 'успешно сохранено.');
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('danger', 'Ошибки при сохранении.');
                }
            } else {
                Yii::$app->session->addFlash('danger', 'Ошибки. Не сохранено.');
            }
        }

        return $this->render('policy', ['options' => $properties]);
    }

    /**
     * @param integer $groupId
     * @param integer $publisherId
     * @return PublisherProperty[]
     * @throws yii\base\Exception
     */
    private function getEmptyModelsForGroup(int $groupId, int $publisherId): array
    {
        $result = [];
        $groupModel = PublisherPropertyGroup::findOne(['id' => $groupId]);
        if (empty($groupModel)) {
            throw new yii\base\Exception("options group $groupId not found" , 500);
        }
        $propsTypesList = $groupModel->publisherPropertyTypes;
        if (empty($propsTypesList)) {
            return $result;
        }
        foreach ($propsTypesList as $propertyType) {
//            $prop = new \common\models\PublisherProperty(['publisher_id' => $publisherId, 'property_type_id' => $propertyType->id]);
            $prop = new \common\models\PublisherProperty();
            $prop->setAttributes(['publisher_id' => $publisherId, 'property_type_id' => $propertyType->id, 'value' => NULL]);
            $result[] = $prop;
        }
        return $result;
    }

}
