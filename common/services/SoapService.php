<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\services;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Description of SoapService
 *
 * @author andrey
 */
class SoapService {
    private $client = null;
    private $options = [
            'soap_version'       => SOAP_1_1,
            'exceptions'         => true,
            'trace'              => true,
            'cache_wsdl'         => WSDL_CACHE_NONE,
            'connection_timeout' => 3,
    ];
    private $auth = [
        'login' => false,
        'password' => false,
    ];
    private $url = false;
    
    private $functionsList = null;
    
    private $typesList = null;

    private $error = false;


    /**
     * SoapService constructor.
     * @param integer|\common\models\Publisher $publisher
     */
    public function __construct($options = []) {
        if (empty($options) || !is_array($options))
            return false;

        $auth = !isset($options['auth']) || !is_array($options['auth']) ?  [] : $options['auth'];
        $url =  !isset($options['url']) || empty($options['url']) ?  $this->url : $options['url'];

        $this->auth = ArrayHelper::merge($this->auth, $auth);
        if ($this->auth['login'] && $this->auth['password']) {
            $options = ArrayHelper::merge($this->options, $auth);
        } else {
            $options = [];
        }

        $this->options = ArrayHelper::merge($this->options, $options);

        $this->url = $url;
        if (!$this->url) {
            $this->error = 'URL required';
            return false;
        }

        try {
            $this->client = new \SoapClient($this->url, $this->options);
            $this->functionsList = $this->getFunctionsList();
            $this->typesList = $this->getTypesList();

//        } catch (\Exception $fault) {
        } catch (\SoapFault $fault) {
            $this->client = null;
            $this->error = $fault->getMessage();
            Yii::$app->session->addFlash('danger', $fault->getMessage());
            
            if (function_exists('error_clear_last'))
                error_clear_last();
            
            Yii::$app->errorHandler->unregister();
            
            return false;
        }
    }

    public function getClient()
    {
        return $this->client;
    }
    
    public function getOptions(){
        return $this->options;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function getError()
    {
        return $this->error;
    }
    public function getOption($optionName)
    {
        $properties =  get_object_vars($this->client);
        if (array_key_exists($optionName, $properties)) {
            return $this->client->$optionName;
        } else {
            return false;
        }
    }
    /**
     * Return TRUE if web service's functions list contains function of incoming name. Otherwise return FALSE.
     * @param array $functionName
     * @return boolean
     **/
    public function isFunctionExists($functionName = fasle)
    {
        if (!$functionName || empty($this->functionsList))
            return false;

        $resultArr =  array_filter($this->functionsList, function($funcArr) use ($functionName) {
            return isset($funcArr['functionData']) && isset($funcArr['functionData']['name']) && $funcArr['functionData']['name'] == $functionName;
        });
        return count($resultArr) > 0;
    }
    /**
     * Return TRUE if web service's functions list contains all functions from incoming list of functions. Otherwise return FALSE.
     * @param array $functionsList
     * @return boolean
     **/
    public function isFunctionsExists($functionsList = array())
    {
        if (empty($functionsList))
            return false;

        $result = true;
        foreach($functionsList as $functionName) {
            if (!$this->isFunctionExists($functionName)) {
                $result = false;
                break;
            }
        }
        return $result;
    }
    /**
     *@return array List of available functions. Each array element is an array with such structure
     * [
     *      'raw => 'STRING'
            'functionData' => [
     *          'type' => 'STRING',
     *          'name' => 'STRING',
     *          'parameters' => [
     *              ['type' => 'STRING', 'name' => 'STRING']
     *              ['type' => 'STRING', 'name' => 'STRING']
     *              ...
     *          ]
     *      ]
     *  ]
     **/
    public function getFunctionsList(){
        if ($this->functionsList !== null)
            return $this->functionsList;
        
        if ($this->client !== null && $this->client instanceof \SoapClient) {
            $listArr = $this->client->__getFunctions();
//            return $listArr;
            $resultArr = [];
            foreach($listArr as $functionString) {
                $resultArr[] = $this->parseFunctionString($functionString);
            }
            return $resultArr;
        } else {
            return [];
        }
    }

    /**
     * @param string $functionName
     * @return array
     */
    public function getFunctionInfo($functionName = '')
    {
        if ( !is_string($functionName) || empty($functionName) )
            return [];

        $filteredArr = array_filter($this->getFunctionsList(), function ( $functionArr ) use( $functionName ) {
            return is_array($functionArr) && isset($functionArr['functionData']) && isset($functionArr['functionData']['name']) && $functionArr['functionData']['name'] == trim($functionName);
        });
        $keys = array_keys($filteredArr);
        return count($keys) > 0 ? $filteredArr[$keys[0]] : [];
    }

    /**
     * @param string $typeName
     * @return array
     */
    public function getTypeInfo($typeName = '')
    {
        if (empty($typeName))
            return [];

        $foundType = array_filter($this->getTypesList(), function($type) use($typeName) {
            return is_array($type) && isset($type['typeName']) && $type['typeName'] == $typeName;
        });
        return $foundType;
//        $keys = array_keys($foundType);
//
//        return count($keys) > 0 ?  $foundType[$keys[0]] : [];
    }

    /**
     * @return array|null
     */
    public function getTypesList() {
        if ($this->typesList !== null)
            return $this->typesList; 
        
        if ($this->client !== null && $this->client instanceof \SoapClient) {
            $typesArr = $this->client->__getTypes();
            $resultArr = [];
            foreach($typesArr as $typeString) {
                $resultArr [] = $this->parseTypeString($typeString);
            }
            return $resultArr;
        } else {
           return [];
        }
    }

    /**
     * @param string $typeName
     * @return bool
     */
    public function isTypeExists($typeName = '')
    {
        if (empty($typeName))
            return false;

        $typeName = trim($typeName);

        $filteredArr = array_filter($this->getTypesList(), function($typeArr)use($typeName){
            return isset($typeArr['typeName']) && $typeArr['typeName'] == $typeName;
        });
        return count($filteredArr) > 0;
    }

    /**
     * @param array $typesList
     * @return bool
     */
    public function isTypesExists($typesList = array())
    {
        if(empty($typesList))
            return false;

        $result = true;
        foreach($typesList as $typeName) {
            $typeName = trim($typeName);
            if (!$this->isTypeExists($typeName)) {
                $result = false;
                break;
            }
        }
        return $result;
    }
    /**
     * @param string $functionString
     * @return array
     */
    private function parseFunctionString($functionString = '')
    {
        if(empty($functionString))
            return [];

        $temp = $functionString;
        $paramsPart = [];
        preg_match('/\(.+\)/', $temp, $paramsPart);
        $paramsPart = count($paramsPart) > 0 ? $paramsPart[0] : '';
        $temp = str_replace($paramsPart, '', $temp);

        $parts = explode(' ', $temp);
        $functionType = count($parts) > 0 ?  $parts[0] : false;
        $functionName = count($parts) > 1 ? $parts[1] : false;

        $paramsPart = str_replace('(', '', $paramsPart);
        $paramsPart = str_replace(')', '', $paramsPart);
//        $params = explode(',', $paramsPart);
        $params = preg_split('/\,|\;/', $paramsPart);

        $arguments = [];
        foreach($params as &$param) {
            $param = trim($param);
            $argStr = explode(' ', $param);
            $argType = count($argStr) > 0 ? $argStr[0] : false;
            $argName = count($argStr) > 1 ? $argStr[1] : false;
            $arguments [] = [
                'type' => $argType,
                'name' => $argName,
            ];
        }
        return [
            'raw' => $functionString,
            'functionData' => ['type' => $functionType, 'name' => $functionName, 'parameters' => $arguments],
        ];
    }

    /**
     * @param string $typeString
     * @return array
     */
    private function parseTypeString($typeString = '')
    {
        $result = [];
        if (empty($typeString))
            return $result;

        $typeString = preg_replace('/\n|\r|\r\n/','', $typeString);
        $dataPart = [];
        preg_match('/\{.+\}/',$typeString, $dataPart);
        $dataPart = count($dataPart) > 0 ? $dataPart[0] : '';
        $namePart = str_replace($dataPart, '', $typeString);
        $namePart = preg_split('/\s+/', $namePart);
        if (count($namePart) > 1) {
            $namePart = $namePart[1];
        } else {
            $namePart = count($namePart) > 0 ? $namePart[0] : false;
        }
        $dataPart = preg_replace('/\{|\}/', '',$dataPart);
        $dataPart = preg_split('/\,|\;/', $dataPart);

        $typeContent = [];
        foreach($dataPart as &$dataLine){
            $dataLine = trim($dataLine);
            if (!empty($dataLine)) {
                $data = preg_split('/\s+/', $dataLine);
                $type = count($data) > 0 ? $data[0] : false;
                $name = count($data) > 1 ? $data[1] : false;
                $typeContent[] = ['type' => $type, 'name' => $name];
            }
        }
        $result['typeName'] = $namePart;
        $result['typeDesc'] = $typeContent;
        $result['rawStr'] = $typeString;
        return $result;
    }
}
