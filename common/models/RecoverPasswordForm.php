<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class RecoverPasswordForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            [['email'], 'isExist']
        ];
    }

    /**
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function isExist($attribute, $params)
    {
        $exists = User::isExistWithAttr($attribute, $this->{$attribute});
        if (!$exists) {
            $this->addError($attribute, 'Пользователь с такими данными не найден');
        }
//        if (!$this->hasErrors()) {
//        }
    }
    public function attributeLabels()
    {
        return [
            'email'      => 'Email',
            'login'      => 'логин',
        ];
    }    
}
