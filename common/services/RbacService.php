<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\services;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\ManagerInterface;

/**
 *
 * @author andrey
 * @property ManagerInterface $authManager
 */
class RbacService {

    private $authManager;

    public function __construct()
    {
        $this->authManager = Yii::$app->getAuthManager();
    }

    public function initialCreate(): void
    {
        // roles:  admin, accountant, subscribe_manager, developer, ship_manager, publisher

        // permissions: manageRoles, updateRole, createPublisher, updatePublisher, deletePublisher
        // createUser, updateUser, updateOwnProfile
        // manageIssues, managePrices, manageActions
        // manageEditions, manageDirectories, accessSubscribeForms, managePaySystems

        $am = $this->authManager;
        if (empty($am)) {
            return;
        }

        // формирование списка правил
        $rules = [];

        $isOwnerRule = new \common\rbac\rule\IsOwnerRule;
        $rules[] = $isOwnerRule;
        foreach ($rules as $rule) {
            $exist = $am->getRule($rule->name);
            if (!empty($exist)) {
                $am->remove($exist);
            }
            $am->add($rule);
        }
        // END формирование списка правил

        // Формирование списка разрешений
        $permissions = [];

        $mrP = $am->createPermission('manageRoles');
        $mrP->description = 'any manipulations to roles entities';
        $permissions[] = $mrP;

        $urP = $am->createPermission('updateRole');
        $urP->description = 'change content of role entity';
        $permissions[] = $urP;

        $cpP = $am->createPermission('createPublisher');
        $cpP->description = 'create new publisher entity';
        $permissions[] = $cpP;

        $upP = $am->createPermission('updatePublisher');
        $upP->description = 'change content of publisher entity';
        $permissions[] = $upP;

        $dpP = $am->createPermission('deletePublisher');
        $dpP->description = 'delete publisher entity';
        $permissions[] = $dpP;

        $createUserP = $am->createPermission('createUser');
        $createUserP->description = 'create new User';
        $permissions[] = $createUserP;

        $updateUserP = $am->createPermission('updateUser');
        $updateUserP->description = 'create new User';
        $permissions[] = $updateUserP;

        $manPricesP = $am->createPermission('managePrices');
        $manPricesP->description = 'any manipulations with prices';
        $permissions[] = $manPricesP;

        $manIssuesP = $am->createPermission('manageIssues');
        $manIssuesP->description = 'any manipulations with issues';
        $permissions[] = $manIssuesP;

        $manActionsP = $am->createPermission('manageActions');
        $manActionsP->description = 'any manipulations with actions';
        $permissions[] = $manActionsP;

        $ownProfP = $am->createPermission('updateOwnProfile');
        $ownProfP->description = 'change content of own profile';
        $ownProfP->ruleName = 'isOwner';
        $permissions[] = $ownProfP;

        $eManP = $am->createPermission('manageEditions');
        $eManP->description = 'any manipulations with editions';
        $permissions[] = $eManP;

        $dirsManP = $am->createPermission('manageDirectories');
        $dirsManP->description = 'any manipulations with help tables (companies, persons, addresses, countries..) ';
        $permissions[] = $dirsManP;

        $subscFormP = $am->createPermission('accessSubscribeForms');
        $subscFormP->description = 'access to page of subscription forms';
        $permissions[] = $subscFormP;

        $payManP = $am->createPermission('managePaySystems');
        $payManP->description = 'payment systems managment';
        $permissions[] = $payManP;

        // END Формирование списка разрешений


        // Формирование списка ролей
        $roles = [];
        $adminR = $am->createRole('admin');
        $adminR->description = 'administrator role';
        $roles[] = $adminR;

        $devR = $am->createRole('developer');
        $devR->description = 'developer role. God level.';
        $roles[] = $devR;

        $subsManR = $am->createRole('subscribe_manager');
        $subsManR->description = 'Staff, managing subscriptions.';
        $roles[] = $subsManR;

        $accR = $am->createRole('accountant');
        $roles[] = $accR;

        $shipManR = $am->createRole('ship_manager');
        $shipManR->description = 'Staff, managing shipping';
        $roles[] = $shipManR;

        $pbshrR = $am->createRole('publisher');
        $pbshrR->description = 'Top manager of publisher.';
        $roles[] = $pbshrR;
        // END Формирование списка ролей

        // добавление ролей и разрешений
        foreach ($permissions as $permission) {
            $exist = $am->getPermission($permission->name);
            if (!empty($exist)) {
                $am->remove($exist);
            }
            $am->add($permission);
        }
        foreach ($roles as $role) {
            $exist = $am->getRole($role->name);
            if (!empty($exist)) {
                $am->remove($exist);
            }
            $am->add($role);
        }

        // Формирование дерева разрешений и ролей

        $am->addChild($accR, $ownProfP); // бухгалтер может править свой профиль

        $am->addChild($shipManR, $ownProfP); // менеджер отгрузок управляет своим профилем
        $am->addChild($shipManR, $manIssuesP); // менеджер отгрузок управляет выпусками
        $am->addChild($shipManR, $subscFormP); // менеджер отгрузок имеет доступ к подписным формам

        $am->addChild($subsManR, $manPricesP); // менеджер подписок управляет ценами
        $am->addChild($subsManR, $manActionsP); // менеджер подписок управляет акциями
        $am->addChild($subsManR, $shipManR); // менеджер подписок может все, что может менеджер отгрузок

        $am->addChild($pbshrR, $upP); // издатель может обновлять издателя
        $am->addChild($pbshrR, $createUserP); // издатель может создавать новых пользователей
        $am->addChild($pbshrR, $updateUserP); // издатель может изменять данные пользователей
        $am->addChild($pbshrR, $eManP); // издатель может управлять изданиями
        $am->addChild($pbshrR, $payManP); // издатель может управлять платежными системами
        $am->addChild($pbshrR, $subsManR); // издатель может все, что может менеджер подписок (включая подчиненные роли)

        $am->addChild($adminR, $ownProfP); // админ может изменять свой профиль
        $am->addChild($adminR, $cpP); // админ может создавать новых издателей
        $am->addChild($adminR, $upP); // админ может править издателей
        $am->addChild($adminR, $dpP); // админ может удалять издателей
        $am->addChild($adminR, $dirsManP); // админ может управлять Справочниками
        $am->addChild($adminR, $subscFormP); // доступ к подписным формам

        $am->addChild($devR, $mrP); // разработчик может управлять ролями
        $am->addChild($devR, $adminR); // разработчик может все, что может админ
        $am->addChild($devR, $pbshrR); // разработчик может все, что может издатель (включая подчиненные роли)

//        $am->removeAllAssignments();

//        $this->reassignAllUsers();

        return;
    }

    public function reassignAllUsers(): void
    {
        $this->authManager->removeAllAssignments();

        foreach (User::find()->all() as $user) {
            $this->updateUserRoles($user);
        }
        return;
    }

    public function addPermissionToRole(string $permName, string $roleName): bool
    {
        if (empty($this->authManager)) {
            return false;
        }

        $perm = $this->authManager->getPermission($permName);
        if (empty($perm)) {
            $perm = $this->authManager->createPermission($permName);
            $this->authManager->add($perm);
        }
        $role = $this->authManager->getRole($roleName);
        if (empty($role)) {
            $role = $this->authManager->createRole($roleName);
            $this->authManager->add($role);
        }
        $existsPerms = $this->authManager->getPermissionsByRole($roleName);
        $existsPerm = array_filter($existsPerms, function ($p) use ($perm) { return $p->name == $perm->name; });

        if (count($existsPerm) > 0) {
            return true;
        }
        try {
            return $this->authManager->addChild($role, $perm);
        }catch (\Exception $e) {
            return false;
        }
    }

    public function getRolePermissions (string $roleName): array
    {
        if (empty($this->authManager)) {
            return [];
        }

        $role = $this->authManager->getRole($roleName);
        if (empty($role)) {
            return [];
        }
        return $this->authManager->getPermissionsByRole($roleName);
    }

    public function updateUserRoles(User $user): bool
    {
        if (empty($this->authManager)) {
            return false;
        }
        $this->removeUserAssignments(intval($user->User_ID));

        foreach ($user->roles as $uRoleArr) {
            $role = $this->authManager->getRole($uRoleArr['code']);
            if (empty($role)) {
                continue;
            }
            $this->authManager->assign($role, intval($user->User_ID));
        }
        return true;
    }

    public function removeUserAssignments(int $userId): void
    {
        if (empty($this->authManager)) {
            return;
        }

        $nowExists = $this->authManager->getAssignments($userId);
        foreach ($nowExists as $revokeCandidate) {
            $role = $this->authManager->getRole($revokeCandidate->roleName);
            if (empty($role)) {
                continue;
            }
            $this->authManager->revoke($role, $userId);
        }
        return;
    }

    public function revokeUserAssignment(string $roleName, int $userId): bool
    {
        if (empty($this->authManager)) {
            return false;
        }

        $role = $this->authManager->getRole($roleName);
        if (empty($role)) {
            return true;
        }
        return $this->authManager->revoke($role, $userId);
    }
}
