<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "nx_subscription".
 *
 * @property string $id
 * @property string $periodical_id
 * @property string $date_start
 * @property string $date_end
 * @property string $type
 * @property integer $enabled
 * @property integer $period
 * @property integer $byear
 * @property integer $bnum
 * @property integer $rqty
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id

 * @property Edition $edition
 */
class NxSubscription extends \common\models\WatchableModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['periodical_id', 'enabled', 'period', 'byear', 'bnum', 'rqty'], 'integer'],
            [['date_start', 'date_end', 'type', 'created'], 'required'],
            [['date_start', 'date_end', 'created', 'last_updated','create_user_id','last_user_id'], 'safe'],
            [['type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periodical_id' => 'Periodical ID',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'type' => 'Type',
            'enabled' => 'Enabled',
            'period' => 'Period',
            'byear' => 'Byear',
            'bnum' => 'Bnum',
            'rqty' => 'Rqty',
            'created' => 'Created',
            'last_updated' => 'Last Updated',
            'create_user_id' => 'Create User ID',
            'last_user_id' => 'Last User ID',
        ];
    }


    public function getPeriodical()
    {
        return $this->hasOne(\common\models\Magazines::className(), ['Message_ID' => 'periodical_id']);
    }
    public function getEdition()
    {
        return $this->hasOne(\common\models\Magazines::className(), ['Message_ID' => 'periodical_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);
    }
}
