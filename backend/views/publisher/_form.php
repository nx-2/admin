<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use kartik\file\FileInput;
use backend\views\widgets\create_update_fields\CreateUpdateFields;
use backend\views\widgets\image_field\ImageField;


/* @var $this yii\web\View */
/* @var $model common\models\Publisher */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
    if (window.addEventListener) // W3C standard
    {
      window.addEventListener('load', initInfo, false); 
    } 
    else if (window.attachEvent) // Microsoft
    {
      window.attachEvent('onload', initInfo);
    }
    
    function initInfo(){
        if (typeof $ != 'undefined') {
            getInfo('company');
            getInfo('person');
        }
    };

</script>
<div class='box box-primary'>
    <div class='box-header with-border'>
        <!-- 
        <h3 class='box-title'>
            <?php // echo(Html::encode($titleText)); ?>
        </h3>
        -->
    </div>
    <?php 
    if ($model->id == null) {
        $act[] = '/publisher/create';
    } else {
        $act[] = 'publisher/update';
        $act['id'] = $model->id;
    }
    ?>
    <?php $form = ActiveForm::begin(['action' => $act, 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class='col-md-6 col-sm-10 col-xs-12' style='padding: 20px;' >

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'company_id')->widget(AutoComplete::classname(), [
                    'options' => [
                        'placeholder' => 'Начните набирать...',
                        'class' => 'form-control',
                        'name' => '',
                        'id' => 'publisher-company-name',
                        'value' => ($model->isNewRecord  || $model->company == null) ? '' : $model->company->name,
                    ],
                    'clientOptions' => [
                        'source' => '/company/autocomplete',
                        'autoFill'=>false,
                        'minLength'=>'3',
                        'appendTo' => 'publisher-company-name',
                        'delay' => '300',
                        'select' => new yii\web\JsExpression('function(event, ui){$("#publisher-company-id").val(ui.item.id).change();}'),
                        'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#publisher-company-id").val("");}}'),
                        // 'disabled' => true,

                    ],
            ]) ?>
            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'company_id', $options = ['id' => 'publisher-company-id', 'onchange' => 'getInfo("company")'])); ?>
            <div id='company-service-wrap' style="margin-bottom: 15px;border-bottom: 1px dashed rgba(0,0,0,0.3);">
                <a class="btn btn-app" id='show-company-info' title="Посмотреть подробности про компанию" style="margin-left: 0px" data-toggle="modal" data-target="#company-info-modal">
                    <i class="fa fa-info-circle"></i> Инфо
                </a>
                <a class="btn btn-app" title="Не нашли в списке нужную комманию? Создайте новую." style="margin-left: 0px" data-toggle="modal" data-target="#company-update-modal"> 
                    <i class="fa fa-plus-square"></i> Создать
                </a>
                <div id='company-info-modal-wrap'></div> 
                <div id='company-new-modal-wrap'></div> 
            </div>
            <?= $form->field($model, 'contact_person_id')->widget(AutoComplete::classname(), [
                    'options' => [
                        'placeholder' => 'Начните набирать...',
                        'class' => 'form-control',
                        'name' => '',
                        'id' => 'publisher-person-name',
                        'value' => ($model->isNewRecord || $model->person == null) ? '' : $model->person->name,
                    ],
                    'clientOptions' => [
                        'source' => '/person/autocomplete',
                        'autoFill'=>false,
                        'minLength'=>'3',
                        'appendTo' => 'publisher-person-name',
                        'delay' => '300',
                        'select' => new yii\web\JsExpression('function(event, ui){$("#publisher-person-id").val(ui.item.id).change();}'),
                        'change' => new yii\web\JsExpression('function(event,ui){if(!ui.item){$("#publisher-person-id").val("");}}'),
                        // 'disabled' => true,

                    ],
            ]) ?>
            <?php echo (yii\helpers\Html::activeHiddenInput ( $model, 'contact_person_id', $options = ['id' => 'publisher-person-id', 'onchange' => 'getInfo("person")'])); ?>
            <div id='person-service-wrap' style="margin-bottom: 15px;border-bottom: 1px dashed rgba(0,0,0,0.3);">
                <a class="btn btn-app" id='show-person-info' title="Посмотреть подробности про контактное лицо" style="margin-left: 0px" data-toggle="modal" data-target="#person-info-modal">
                    <i class="fa fa-info-circle"></i> Инфо
                </a>
                <a class="btn btn-app" title="Не нашли в списке нужного человека? Создайте нового." style="margin-left: 0px" data-toggle="modal" data-target="#person-update-modal">
                    <i class="fa fa-plus-square"></i> Создать
                </a>
                <div id='person-info-modal-wrap'></div> 
            </div>

            <?= $form->field($model, 'distribution')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'audience')->textInput(['maxlength' => true]) ?>

            <?php 
                echo $form->field($model, 'logoFile')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                ]);    
            ?>
            <?php if(!empty($model->logo)): ?>
                <?= ImageField::widget(['model' => $model, 'imageFieldName' => 'logo']); ?>
            <?php endif; ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
    </div>
    <div class='col-md-6 col-sm-10 col-xs-12' style='padding: 20px;'>
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'official_email')->textInput(['maxlength' => true]) ?>

            <?php
            $allTaxSystems = yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand('SELECT ts.id, ts.name FROM tax_system ts')->queryAll(), 'id','name');
            $taxDropDownoptions = ['prompt' => 'Выберете систему налогооблажения...'];

            $allVatRates = yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand('SELECT vr.id, vr.name FROM vat_rate vr')->queryAll(), 'id','name');
            $vatDropDownoptions = ['prompt' => 'Выберете ставку НДС...'];
            ?>

            <?= $form->field($model, 'tax_system_id')->dropDownList($allTaxSystems, $taxDropDownoptions); ?>

            <?= $form->field($model, 'paper_vat_rate_id')->dropDownList($allVatRates, $vatDropDownoptions); ?>

            <?= $form->field($model, 'pdf_vat_rate_id')->dropDownList($allVatRates, $vatDropDownoptions); ?>

            <?= CreateUpdateFields::widget(['model' => $model]); ?>

            <?php //echo($form->field($model, 'enabled')->checkbox()); ?>

    </div>
    <?php ActiveForm::end(); ?>
</div> 
<!-- Должно быть за пределами основной формы!!! -->
<div id='person-new-modal-wrap'>
    <?= $this->render('/person/_form-modal', ['personModel' => new \common\models\NxPerson(), 'addressModel' => new \common\models\NxAddress()]); ?>
</div> 
<div id='company-new-modal-wrap'>
    <?= $this->render('/company/_form-modal', ['companyModel' => new \common\models\Company(), 'addressModel' => new \common\models\NxAddress()]); ?>
</div> 





