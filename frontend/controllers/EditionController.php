<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;

use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\models\Edition;

class EditionController extends ActiveController
{
    public $modelClass = 'common\models\Company';

    public function actionAll()
    {
        $query = Edition::find();
        $query->joinWith(['editionPassport']);
        $query->joinWith(['editionPeriods']);
        $query->joinWith(['publisher']);
        $query->asArray(true);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    public function actionPrice($publisherId = false, $subscriberTypeId = false, $editionId = false, $deliveryTypeId = false, $countryId = false)
    {

        if (!$publisherId || !$subscriberTypeId || !$editionId || !$deliveryTypeId || !$countryId)
            throw new \yii\web\BadRequestHttpException('not enough parameters to get current price of editiond');
        
        $params = [
            'publisherId' => $publisherId, 
            'subscriberTypeId' => $subscriberTypeId,
            'editionId' => $editionId,
            'deliveryTypeId' => $deliveryTypeId,
            'countryId' => $countryId,
        ];
        header('Access-Control-Allow-Origin: *');
        
        return \common\models\Edition::getPrice($params);
    }
    public function actionSingle($id = false)
    {
                if (!$id)
                    throw new NotFoundHttpException('Edition Id is required');

                $query = Edition::find()->where(['Message_ID' => intval($id)]);
                $query->joinWith(['editionPassport']);
                $query->joinWith(['editionPeriods']);
                $query->joinWith(['publisher']);
                $query->asArray(true);
                $result = $query->one();
                if ($result == null)
                    throw new NotFoundHttpException('edition not found.');

                header('Access-Control-Allow-Origin: *');
                return $result;

        
//                $columns = [
//				'c.name', 'c.org_form', 'c.address_id', 'c.legal_address_id', 'c.contact_person_id','c.email', 'c.url', 
//				'c.description', 'c.shortname', 'c.othernames', 'c.wrongnames', 'c.inn', 'c.okonh', 'c.pay_account', 
//				'c.corr_account', 'c.kpp','c.bik','c.bank',
//		];
//		return Company::find()
//			->select($columns)
//			->from('nx_company c')
//			->where(['c.id' => $id])
//			->joinWith(
//			[
//				//получить "жадно" legalAdress->Country, Address->country, contactPerson->address->country
//				'legalAddress la' => function($q) {
//					$q->joinWith(['country lac' => function($q) {
//						$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
//					}]);
//				},
//				'contactPerson p' => function($q) {
//					$q->joinWith(['address pad' => function($q) {
//						$q->joinWith(['country cpc' => function($q) {
//							$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
//						}]);
//					}]);
//				},
//				'address a' => function($q) {
//					$q->joinWith(['country adc' => function($q) {
//						$q->select(['id' => 'Country_ID', 'name' => 'Country_Name']);
//					}]);
//				},
//			])
//			->asArray(true)
//			->one();
    }
    
}
