<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\models;

use common\custom_components\event_handlers\IssueEventHandler;
use common\models\behaviors\ImageUploadBehavior;
use DateTime;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Url;


// use yii2tech\ar\linkmany\LinkManyBehavior;


/**
 * This is the model class for table "Issue".
 *
 * @property integer $Message_ID
 * @property integer $User_ID
 * @property integer $Subdivision_ID
 * @property integer $Sub_Class_ID
 * @property integer $Priority
 * @property string $Keyword
 * @property integer $Checked
 * @property string $TimeToDelete
 * @property string $TimeToUncheck
 * @property string $IP
 * @property string $UserAgent
 * @property integer $Parent_Message_ID
 * @property string $created
 * @property string $last_updated
 * @property integer $last_user_id
 * @property string $LastIP
 * @property string $LastUserAgent
 * @property string $Name
 * @property integer $MagazineID
 * @property string $StartDate
 * @property integer $Number
 * @property string $PublicDate
 * @property string $Picture
 * @property integer $IssueID
 * @property integer $status_id
 * @property string $annotation
 * @property integer $available_online
 * @property string $issue_title
 * @property integer $no_full
 * @property integer $year
 * @property integer $oldID
 * @property string $inCompositionDate
 * @property integer $showDescription
 * @property double $ArticlePrice
 * @property string $ncTitle
 * @property string $ncKeywords
 * @property string $ncDescription
 * @property integer $release_month
 * @property integer $end_number
 * @property string $subscription_code
 * @property integer $xpress_id
 * @property integer $create_user_id
 * @property integer $type
 * @property integer $pdf_link_life_time
 * @property integer $issue_file
 *
 * @property Edition $magazine
 * @property OrderItemRelease[] $orderItemReleases
 * @property NxOrder[] $orders
 * @property NxOrder[] $activeOrders
 * @property NxOrder[] $notShippedOrders
 */
class Issue extends WatchableModel
{
    const FOUND_NOT_SHIPPED_PDF = 'foundNotShippedPDF';
    const SEND_NOT_SHIPPED_PDFS_REQUESTED = 'sendNotShippedRequested';
    const BECAME_ISSUED_EVENT_NAME = 'hasReleased';

    const STATUS_GETTING_READY = 1; // готовится
    const STATUS_ISSUED = 2; // выпущен
    const STATUS_CANCELLED = 3; // отменен

    public $coverFile = null;
    public $deleteIssueFile = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Issue';
    }

    public static function getCover($id)
    {
        $cover = self::find()->where(['status_id' => self::STATUS_ISSUED, 'MagazineID' => $id])->orderBy(['PublicDate' => SORT_DESC])->one();
        if (!empty($cover))
            return 'http://www.osp.ru/data' . $cover->Picture;

        return '';

    }

    /**
     * @param int $daysBefore
     * @return array Список Ид выпусков на стадии "выпущен", которые относятся к оплаченным ПДФ заказам (подпискам) для которых срок рассылки ПДФ наступил
     * в за daysBefore дней до текущего момента,  но они не были отгружены
     * @throws Exception
     */
    public static function getLastNotShippedPdfIssuesIds($daysBefore = 365)
    {
        //Предполагается, что этот метод будет вызываться из кронзадачи в консольном приложении с определенной периодичностью
        //Если для daysBefore указывать небольшие значения, то может произойти ситуация, когда кто-то подпишется на старые выпуски и не получит этот свой ПДФ
        //Поэтому лучше daysBefore брать побольше, год например...(трудно представить человека, кот. захочет подписаться на то, что вышло более чем год назад)
        $result = Yii::$app->db->createCommand("CALL last_not_shipped_pdf_issues_ids(:daysBefore)")
            ->bindValue(':daysBefore', intval($daysBefore))
            ->queryAll();
        $toReturn = [];
        foreach ($result as $item) {
            $toReturn[] = $item['id'];
        }
        return $toReturn;
    }

    public function init()
    {
        parent::init();
        $this->on(self::FOUND_NOT_SHIPPED_PDF, ['\common\custom_components\event_handlers\IssueEventHandler', 'onNotShippedPdfFound']);
        $this->on(self::SEND_NOT_SHIPPED_PDFS_REQUESTED, ['\common\custom_components\event_handlers\IssueEventHandler', 'onSendNotShippedRequested'], ['typeOfNotice' => 'foundNotShippedPDF']);
        $this->on(self::BECAME_ISSUED_EVENT_NAME, [IssueEventHandler::class, 'onIssueHasReleased']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_ID', 'Subdivision_ID', 'Sub_Class_ID', 'Priority', 'Checked', 'Parent_Message_ID', 'last_user_id', 'MagazineID', 'Number', 'IssueID', 'status_id', 'available_online', 'no_full', 'year', 'oldID', 'showDescription', 'end_number', 'xpress_id', 'create_user_id', 'type', 'pdf_link_life_time'], 'integer'],
            ['release_month', 'integer', 'min' => 1, 'max' => 12],
            ['pdf_link_life_time', 'default', 'value' => null, 'skipOnEmpty' => true],
            ['year', 'default', 'value' => date('Y', time())],
            ['release_month', 'default', 'value' => date('m', time())],
            ['Checked', 'default', 'value' => '1'],
            [['Subdivision_ID', 'Sub_Class_ID', 'Parent_Message_ID'], 'default', 'value' => '0'],
            ['Keyword', 'default', 'value' => ''],
            [['Name', 'MagazineID', 'StartDate', 'Number'], 'required'],
            [['MagazineID', 'year', 'Number'], 'unique', 'targetAttribute' => ['MagazineID', 'year', 'Number'], 'message' => 'Выпуск с таким номером уже существует в этом году'],
            [['TimeToDelete', 'TimeToUncheck', 'created', 'create_user_id', 'last_updated', 'last_user_id', 'StartDate', 'PublicDate', 'inCompositionDate', 'deleteIssueFile'], 'safe'],
            [['annotation', 'issue_title', 'ncDescription'], 'string'],
            [['ArticlePrice'], 'number', 'skipOnEmpty' => 'true'],
            ['ArticlePrice', 'required',
                'when' => function ($model) {
                    return $model->type == 2;
                },
                'whenClient' => "function (attribute, value) {return $('#issue-type option:checked').val() == 2;}",
                'message' => 'Для одиночных подписок должна быть указана цена'],
            [['Keyword', 'UserAgent', 'LastUserAgent', 'Name', 'Picture', 'ncTitle', 'ncKeywords', 'subscription_code'], 'string', 'max' => 255],
            [['IP', 'LastIP'], 'string', 'max' => 15],
            [['MagazineID'], 'exist', 'skipOnError' => true, 'targetClass' => Edition::class, 'targetAttribute' => ['MagazineID' => 'Message_ID']],
            ['coverFile', 'image', 'extensions' => 'png, jpg, gif,',
                'skipOnEmpty' => true,
                'minWidth' => 50, 'maxWidth' => 1200,
                // 'minHeight' => 100, 'maxHeight' => 1000,
                'message' => 'Принимаются изображения с расширением png, jpg, gif и шириной от 50 до 1000 пикселов',
            ],
            ['issue_file', 'file', 'maxSize' => 1024 * 1024 * 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Message_ID' => 'ID',
            'User_ID' => 'Создан пользователем',
            'Subdivision_ID' => 'Subdivision  ID',
            'Sub_Class_ID' => 'Sub  Class  ID',
            'Priority' => 'Priority',
            'Keyword' => 'Keyword',
            'Checked' => 'Активен',
            'TimeToDelete' => 'Дата удаления',
            'TimeToUncheck' => 'Дата деактивации',
            'IP' => 'IP адрес',
            'UserAgent' => 'User Agent',
            'Parent_Message_ID' => 'Parent  Message  ID',
            'created' => 'Создан',
            'last_updated' => 'Обновлен',
            'last_user_id' => 'Обновлен пользователем',
            'LastIP' => 'Посл. IP адрес',
            'LastUserAgent' => 'Last User Agent',
            'Name' => 'Название',
            'MagazineID' => 'Издание',
            'magazine' => 'Издание',
            'StartDate' => 'Дата рассылки',
            'Number' => 'Номер с начала года',
            'PublicDate' => 'Дата выхода',
            'Picture' => 'Обложка',
            'IssueID' => 'Issue ID',
            'status_id' => 'Статус',
            'annotation' => 'Описание',
            'available_online' => 'Available Online',
            'issue_title' => 'Заголовок',
            'no_full' => 'No Full',
            'year' => 'Год',
            'oldID' => 'Old ID',
            'inCompositionDate' => 'In Composition Date',
            'showDescription' => 'Show Description',
            'ArticlePrice' => 'Цена',
            'ncTitle' => 'Nc Title',
            'ncKeywords' => 'Nc Keywords',
            'ncDescription' => 'Nc Description',
            'release_month' => 'Месяц выхода',
            'end_number' => 'End Number',
            'subscription_code' => 'Subscription Code',
            'xpress_id' => 'Xpress ID',
            'create_user_id' => 'Создан пользователем',
            'type' => 'Тип',
            'pdf_link_life_time' => 'Ссылка на скачивание ПДф активна (дней)',
            'issue_file' => 'Файл с электронной версией выпуска',
            'coverFile' => 'Обложка',
        ];
    }

    public function behaviors()
    {
        $myBehaviors = [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'create_user_id',
                'updatedByAttribute' => 'last_user_id',
            ],
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false, //это поле будет обновляться средствами БД (CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)
                // 'updatedAtAttribute' => 'last_updated',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created',
                    // ActiveRecord::EVENT_BEFORE_UPDATE => 'last_updated',
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
//                'value' => gmdate("Y-m-d\TH:i:s\Z"),// current UTC timestamp
                'value' => gmdate("Y-m-d H:i:s"),// current UTC timestamp
            ],
            'uploadCover' => [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'Picture',
                'fileField' => 'coverFile',
            ],
            // 'uploadFile' => [
            //     'class' => '\yiidreamteam\upload\FileUploadBehavior',
            //     'attribute' => 'issue_file',
            //     'filePath' => Yii::$app->params['issueFiles']['path'] . Yii::$app->user->identity->publisher_id . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
            //     // 'fileUrl' => '@webroot/issues/[[pk]].[[extension]]',
            //     'fileUrl' => '/download/issuefile/' . Yii::$app->user->identity->publisher_id . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
            // ],

            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'PublicDate',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'PublicDate',
                ],
                'value' => function ($event) {
                    $date = DateTime::createFromFormat('d.m.Y', $event->sender->PublicDate);
                    return $date != false ? $date->format('Y-m-d') : $event->sender->PublicDate;
                },
            ],

            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'StartDate',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'StartDate',
                ],
                'value' => function ($event) {
                    $date = DateTime::createFromFormat('d.m.Y', $event->sender->StartDate);
                    return $date != false ? $date->format('Y-m-d') : $event->sender->StartDate;
                },
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'LastIP',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'LastIP',
                ],
                'value' => isset(Yii::$app->request->userIP) ? Yii::$app->request->userIP : '0.0.0.0',
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'LastUserAgent',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'LastUserAgent',
                ],
                'value' => isset(Yii::$app->request->userAgent) ? Yii::$app->request->userAgent : 'Unknown',
            ],
            // 'linkGroupBehavior' => [
            //     'class' => LinkManyBehavior::class,
            //     'relation' => 'roles_', // relation, which will be handled
            //     'relationReferenceAttribute' => 'rolesIds', // virtual attribute, which is used for related records specification
            // ],
        ];
        if (isset(Yii::$app->user) && Yii::$app->user->identity != null) { //только в случае залогиненого юзера
            $myBehaviors ['uploadFile'] = [
                'class' => '\yiidreamteam\upload\FileUploadBehavior',
                'attribute' => 'issue_file',
                'filePath' => Yii::$app->params['issueFiles']['path'] . Yii::$app->user->identity->publisher_id . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
                // 'fileUrl' => '@webroot/issues/[[pk]].[[extension]]',
                'fileUrl' => '/download/issuefile/' . Yii::$app->user->identity->publisher_id . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
            ];
        }
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }

    public function isPdfLoaded()
    {
        if ($this->Message_ID == null) {
            return false;
        }
        return !empty($this->issue_file) && file_exists($this->getUploadedFilePath('issue_file'));
    }

    public function buildMasterDownloadUrl()
    {
        if ($this->masterLink != null) {
            return Url::to(['download/issuefile/' . $this->masterLink->file_hash]);
        } else {
            return false;
        }
    }

    public function buildDownloadUrl($orderId = false)
    {
        $linkData = $this->getDownloadLink(intval($orderId))->one();

        if (empty($linkData)) {
            IssueDownloadFile::createLink($orderId, $this->Message_ID);
            $linkData = $this->getDownloadLink(intval($orderId))->one();
            if(empty($linkData)) {
                return false;
            }
        }
//        $scheme = preg_replace('/[^\w]+/','', Yii::$app->params['scheme']);
//        if (empty($scheme)) {
//            $scheme = 'https';
//        }
//        return Url::to(['/download/issuefile/' . $linkData->file_hash], Yii::$app->params['scheme']);
        return Url::to(['/download/issuefile/' . $linkData->file_hash], true);
    }

    public function getDownloadLink($orderId = false)
    {
        if (!$orderId)
            return null;

        return $this->hasOne(IssueDownloadFile::class, ['issue_id' => 'Message_ID'])
            ->where(['order_id' => $orderId])
            ->andWhere(['<>', 'master_link', 1]);
    }

    /**
     * @return ActiveQuery
     */
    public function getMagazine()
    {
        return $this->hasOne(Edition::class, ['Message_ID' => 'MagazineID']);
    }

    public function getOrders()
    {
        return $this->hasMany(NxOrder::class, ['id' => 'order_id'])->via('orderItems');
    }
    /**
     * возвращать оплаченные, безоплатные и акционные со скидкой 100% заказы
     */
    public function getActiveOrders(): ActiveQuery
    {
        return $this->hasMany(NxOrder::class, ['id' => 'order_id'])
            ->alias('o')
            ->via('orderItems')
            ->leftJoin('nx_order_item oi', 'oi.order_id = o.id')
            ->onCondition(
                ['or',
                    ['o.order_state_id' => 3], // оплаченные
                    ['and',
                        ['o.order_state_id' => [2,3]],
                        ['o.payment_type_id' => 0], // безоплатные
                    ],
                    ['and',
                        ['not in', 'o.action_id', [0, NULL]],
                        ['oi.discount' => 100] // акционные со скидкой 100%
                    ]
                ]
            );
    }

    public function getOrderItems()
    {
//    * @param array $link the primary-foreign key constraint. The keys of the array refer to
//    * the attributes of the record associated with the `$class` model, while the values of the
//    * array refer to the corresponding attributes in **this** AR class.

        return $this->hasMany(NxOrderItem::class, ['id' => 'item_id'])->via('orderItemReleases');
    }

    /**
     * @return ActiveQuery
     */
    public function getOrderItemReleases()
    {
        return $this->hasMany(OrderItemRelease::class, ['release_id' => 'Message_ID']);
    }

    /**
     *
     * @return array \yii\db\ActiveRecord | boolean
     * Order`s list which contains this issue AND still not shipped
     * @throws Exception
     */
    public function getNotShippedOrders()
    {
        if ($this->Message_ID == null)
            return false;

//        $oIds = Yii::$app->db->createCommand("CALL issue_not_shipped_order_ids(:issueId)")->bindValue(':issueId', $this->Message_ID)->queryAll();
//        $ordersIds = [];
//        foreach ($oIds as $item) {
//            $ordersIds [] = $item['id'];
//        }
        $ordersIds = $this->getNotShippedOrderIds();

        $query = new ActiveQuery(NxOrder::class);
        $query->where(['id' => $ordersIds]);
        return $query->all();
    }

    public function getNotShippedOrderIds(): array
    {
        if ($this->Message_ID == NULL)
            return [];

        //Список ИД оплаченных заказов с доставкой ПДФ в которые входит данный выпуск и он еще не был отгружен
        $oIds = Yii::$app->db->createCommand("CALL issue_not_shipped_order_ids(:issueId)")->bindValue(':issueId', $this->Message_ID)->queryAll();
        $ordersIds = array_map(function ($i) {
            return intval($i['id']);
        }, $oIds);

        return $ordersIds;
    }

    /**
     * @return ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['User_ID' => 'create_user_id']);
    }


//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getMasterLink()
//    {
//
//        return md5($publisherId . '&' . $issueId);
//        $issueId = $this->Message_ID;
//
//        if ($this->MagazineID != null) {
//            $publisherId = $this->magazine->pulisher->id;
//        } else {
//            $publisherId = false;
//        }
//
//        $hash = \common\models\IssueDownloadFile::generateMasterHash($issueId, $publisherId);
//        return $this->hasOne(IssueDownloadFile::class, ['issue_id' => 'Message_ID'])->where(['file_hash' => $hash, 'expire' => null]);
//    }

    /**
     * @return ActiveQuery
     */
    public function getMasterLink()
    {
        return $this->hasOne(IssueDownloadFile::class, ['issue_id' => 'Message_ID'])->where(['master_link' => 1]);
    }

    /**
     * @return ActiveQuery
     */
    public function getMasterLinks()
    {
        return $this->hasMany(IssueDownloadFile::class, ['issue_id' => 'Message_ID']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::class, ['User_ID' => 'last_user_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {

        if (array_key_exists('issue_file', $changedAttributes) && $this->masterLink == null && !empty($this->issue_file)) {
            IssueDownloadFile::createMasterLink($this->primaryKey);
        }
        if (array_key_exists('status_id', $this->dirtyAttributes) && $this->status_id == self::STATUS_ISSUED) {
            $this->trigger(self::BECAME_ISSUED_EVENT_NAME);
        }

        parent::afterSave($insert, $changedAttributes);

    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        //Если заказали удаление существующего файла
        if (!array_key_exists('issue_file', $this->dirtyAttributes) && (boolean)$this->deleteIssueFile == true) {
            $this->deleteIssueFile();
        }

        if (array_key_exists('status_id', $this->dirtyAttributes) && $this->status_id == self::STATUS_ISSUED) {
            if (!$this->isPdfLoaded()) {
                $this->addError('status_id', 'Выпуск не может быть выпущенным пока не будет загружен ПДФ');
                return false;
            }
        }
        return true;

    }

    public function deleteIssueFile()
    {
        $file = $this->getFileName();
        if ($file && file_exists($file)) {
            $this->issue_file = null;
            return unlink($file);
        } else {
            return false;
        }
    }

    public function getFileName()
    {
        if ($this->Message_ID == null)
            return false;

        $pId = $this->magazine->publisher->id;
        $fileName = $this->Message_ID . '_' . $this->issue_file;
        $fullFileName = Yii::getAlias(Yii::$app->params['issueFiles']['path'] . $pId . '/' . $fileName);
        return $fullFileName;
    }

    public function afterFind()
    {
        $this->release_month = sprintf("%02d", $this->release_month);
        parent::afterFind();
    }


}
