<?php

namespace frontend\modules\guarded\controllers;

use common\queue_jobs\SimpleEmailJob;
use frontend\modules\guarded\model\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;
use yii\queue\Queue;

class EmailController extends RootRestController
{
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'controllers' => ['guarded/email'],
                        'actions' => ['send','test'],
                        'allow' => true,
                        'roles' => ['ship_manager', 'accountant'],
//                        'matchCallback' => function($rule, $action) {
//                            return true;
//                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'send' => ['POST'],
                    'test' => ['GET'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);

    }

    public function actionSend()
    {
        $receivedData = Yii::$app->request->post();
//  Ожидаются ключи
//        'to_email'
//        'cc'
//        'from'
//        'subject'
//        'text'
//        'entity_name'
//        'entity_id'

        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        $publisherId = intval($user->publisher_id);
        $publisher = $user->publisher;

        $data = [];
        if(isset($receivedData['entity_name'])
            && trim(strtolower($receivedData['entity_name'])) == 'order'
            && isset($receivedData['entity_id'])
            && !empty($receivedData['entity_id'])
        ) {
            $data['order_id'] = intval($receivedData['entity_id']);
        }

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];

        $dataForEmail = [
            'data' => $data,
            'subject' => $receivedData['subject'] ?? '',
//            'fromEmail' => $receivedData['from'] ?? '',
            'fromEmail' => $senderAddr,
            'toEmail' => $receivedData['to_email'] ?? '',
            'publisherId' => $publisherId,
            'userId' => intval($user->User_ID),
            'body' => $receivedData['text'] ?? '',
        ];
        /** @var Queue $queue */
        $queue = Yii::$app->get('queue');
        $job = new SimpleEmailJob($publisherId, $dataForEmail);
//        $job->execute($queue);
        $jobId = $queue->push($job);

        return ['success' => !empty($jobId), 'jobId' => $jobId];
    }

    public function actionTest()
    {
        return ['dfdsfsd'];
    }
}