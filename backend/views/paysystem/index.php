<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\RelationSystemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Способы приема платежей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relation-systems-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить платежную систему', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(['id' => 'paySystemsList']); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered table-hover dataTable table-condensed',
            'role' => 'grid',
        ],
        'rowOptions' => function ($model, $key, $index, $grid){
            if ($model->enabled == 0) {
                return ['class' => 'disabled'];
            }
            return [];
        },
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;text-align:center;max-width:30px;'],
                'headerOptions' => ['style' => 'width:40px;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            '/paysystem/update?id=' . $key,
                            [
                                'title' => 'Редактировать',
                                'aria-label' => 'Редактировать',
                                'class' => 'fa fa-gear fa-font-18',
                            ]
                        );
                    },                            
                ],
            ],

//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'system_id',
            [
                'attribute' => 'system',
                'value' => 'system.name',
            ],
//            'publisher_id',
//            'param_1',
//            'param_2',
//             'param_3',
//             'is_default',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'is_default',
                'contentOptions' => ['style' => 'text-align:center'],
                'content' => function ($model, $key, $index, $column) {
                    $checkbox = Html::checkbox('is_system_default', (Boolean)$model->is_default, [
                                'id'        => 'isDefault_' . $model->id, 
                                'value'     => $model->is_default,
                                'data-id'   => $model->id,
                                'onclick'   => 'defaultSystemClicked(event)',

//                                 'disabled'  => true,
                    ]);
                    
                    $label = Html::label('', 'isDefault_' . $model->id, ['class' => 'radio-check-label'], '');
//                    $ajaxLoader = Html::tag('div','',['class' => 'ajax-load']);
//                    $content = $ajaxLoader . $checkbox . $label;
                    $content = $checkbox . $label;
                    $result = Html::tag('div', $content ,['class' => 'radio-check-wrap', 'style' => 'max-width:60px;display:inline-block']);

                    return $result;
                },
                'filter' => ['1' => 'вкл','0' => 'выкл'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'enabled',
                'contentOptions' => ['style' => 'text-align:center'],
                'content' => function ($model, $key, $index, $column) {
                    $checkbox = Html::checkbox('', (Boolean)$model->enabled, [
                                'id'        => 'enabled_' . $model->id, 
                                'value'     => $model->enabled,
//                                'onclick'   => 'enabledClicked(event, "RelationSystems", "enabled", function(){$.pjax.reload({container:\'#paySystemsList\'});})',
                                'onclick'   => 'enabledClicked(event, "RelationSystems", "enabled", "$.pjax.reload({container:\'#paySystemsList\'})");',

                                // 'disabled'  => true,
                    ]);
                    $label = Html::label('', 'enabled_' . $model->id, ['class' => 'radio-check-label'], '');
//                    $ajaxLoader = Html::tag('div','',['class' => 'ajax-load_']);
//                    $content = $ajaxLoader . $checkbox . $label;
                    $content = $checkbox . $label;
                    $result = Html::tag('div', $content ,['class' => 'radio-check-wrap', 'style' => 'max-width:60px;display:inline-block']);

                    return $result;
                },
                'filter' => ['1' => 'вкл','0' => 'выкл'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;text-align:center'],
                'headerOptions' => ['style' => 'width:40px;'],
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            '/paysystem/delete?id=' . $key,
                            [
                                'title' => 'Удалить способ платежа',
                                'aria-label' => 'Удалить',
                                'data-confirm' => 'Вы уверены, что хотите удалить этот способ платежа?',
                                'data-method' => 'post',
                                'data-pjax' => '#paySystemsList',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
<script>

function defaultSystemClicked(event){
    if (typeof event == 'undefined')
        event = window.event;
    
    var me = $(event.target);
    
    var value = me.prop('checked') ? 1 : 0;

    if ( !requestAllowed ) {
        me.prop('checked', !value);
        return false;
    }

    requestAllowed = false;
    
    me.closest('div').addClass('loading');
    
    $.ajax({
        url : '/paysystem/is-default-toggle',
        dataType : 'json',
        type : 'post',
        data : {id : me.data('id')},
    })
    .done(function(response){
        if (response.success) {
        	me.prop('checked', response.checked);
        } else {
        	me.prop('checked', !value);
        }
        me.closest('div').removeClass('loading');
        $.pjax.reload({container : '#paySystemsList'});
        requestAllowed = true;
    })
    .fail(function(a,b,c){
        requestAllowed = true;
        alert ('Error. See console...')
        console.log (a);
        console.log (b);
        console.log (c);
        me.closest('div').removeClass('loading');
    });
}

</script>
