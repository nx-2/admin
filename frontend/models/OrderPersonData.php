<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\models;
use Yii;
/**
 *
 * @property integer $apartment
 * @property integer $area
 * @property integer $building
 * @property string $city
 * @property string $city1
 * @property integer $country_id
 * @property string $email
 * @property string $f
 * @property string $i
 * @property string $name
 * @property integer $house
 * @property string $phone
 * @property string $region
 * @property string $street
 * @property string $structure
 * @property integer $zipcode
 * 
 * 
 */

class OrderPersonData extends \yii\base\Model
{
    public $f;
    public $i;
    public $name;
    public $email;
    public $phone;
    public $country_id;
    public $area;
    public $region;
    public $zipcode;
    public $city;
    public $city1;
    public $street;
    public $house;
    public $building;
    public $structure; 
    
    public function rules() {
        return [
            [['f', 'i', 'email','phone'], 'required'],
            [['country_id', 'zipcode','building'],'integer'],
            [['f', 'i','name', 'phone', 'area', 'region', 'city', 'city1', 'street', 'house', 'structure'],'string', 'max' => 50],
        ];
    }
    public function attributeLabels() {
        return [
            'f' => 'Фамилия',
            'i' => 'Имя',
            'name' => 'Полное имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'country_id' => 'Страна',
            'area' => 'Область',
            'region' => 'Район',
            'zipcode' => 'Почтовый индекс',
            'city' => 'Город',
            'city1' => 'входящий нас.пункт',
            'street' => 'Улица',
            'house' => 'Дом',
            'building' => 'Корпус',
            'structure' => 'Строение',
        ];
    }
    
    
}
