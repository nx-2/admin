<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
<style>
    .main-container {
        width: 780px;
        /*	border:1px solid black;*/
        overflow: hidden;
        margin: 0 auto;
        font-family: Arial;
    }

    .reg-sel {
        height: 28px;
        width: 100%;
        border: 1px solid #d7d5d5;
        border-radius: 4px;
        margin-top: 1px;
        background: transparent;
        line-height: 1;
        border: 1px solid rgb(169, 169, 169);
        border-radius: 2px;
        height: 26px;
        -webkit-appearance: none;
        padding-left: 4px;
        font-weight: 100;
        background: url(<?php echo yii\helpers\Url::to('@domain')?>/images/ar.jpg) no-repeat right;
    }

    .btn-promo-code {
        padding: 5px 20px;
        border: none;
        background: #0167d5;
        color: white;
        font-size: 15px;
        margin-top: 21px;
        text-align: center;
    }

    .btn-promo-code a {
        color: #fff;
        font-size: 14px;
        text-decoration: none;
    }

    .form-pic {
        width: 20%;
        height: 200px;
        background-size: contain;
        background-position: center;
        margin-top: 20px;
    }

    .picture {
        max-width: 100%;
        height: auto;
        display: block;
    }

    .form-header {
        margin-bottom: 20px;
        overflow: hidden;
    }

    .header-block {
        width: 70%;
        margin-left: 5%;
    }

    .form-pic, .header-block {
        float: left;
    }

    .header-text {
        font-size: 25px;

    }

    .name {
        font-weight: 900;
    }

    .version, .subscride, .block-summa {
        margin-top: 30px;
        font-weight: 600;
    }

    .version-button {
        height: 30px;
    }

    .version-button:focus {
        background-color: blue;
        color: white;
        outline: none;
        border: none;
        height: 30px;
    }

    .act {
        background-color: blue;
        color: white;
        outline: none;
        border: none;
        height: 30px;
    }

    .kolvo-month, .month {
        margin-left: 10px;
    }

    .styled-select {
        /*	height: 26px!important;*/
        display: inline-block;
        background: url(<?php echo yii\helpers\Url::to('@domain')?>/images/ar.jpg) no-repeat right;

    }

    .styled-select select {
        width: 50px;
        background: transparent;
        line-height: 1;
        border: 1px solid rgb(169, 169, 169);
        border-radius: 2px;
        height: 26px;
        -webkit-appearance: none;
        padding-left: 4px;
        font-weight: 100;
        margin-top: 1%;

    }

    .styled-select-2 select {
        width: 165px;
        margin-top: 1%;

    }

    span.styled-select.styled-select-2 {
        height: 31px !important;
    }

    .select-img {
        /*	background: url(ar.jpg) no-repeat right;
            width: 23px;
            height: 26px;
            position: absolute;
            right: 0px;*/
    }

    .kolvo-month, .month {

        height: 25px;
        overflow: hidden;
        /*width: 240px;*/
    }

    .month {
        width: 200px;
    }

    .summa {
        color: red;
    }

    .dannie {
        font-weight: 600;
    }

    .row-1, .row-2, .row-3, .row-4, .row-5 {
        width: 100%;
        float: left;
        margin-top: 2%;
    }

    .form-block {
        float: left;
        margin-right: 3%;
        position: relative;
    }

    .main-form {
        overflow: hidden;
    }

    input {
        border: 1px solid #d7d5d5;
        border-radius: 4px;
        font-size: 12px;
        width: 100%;
        height: 27px;
        padding-left: 8px;

    }

    #oform-zak {
        padding: 10px 10px;
        border: none;
        background: #0167d5;
        border: none;
        color: white;
        font-size: 15px;
    }

    #podpiskaUr {
        padding: 10px 10px;
        border: none;
        background: #ffffff;
        border: 1px solid black;
        color: black;
        font-weight: 600;
        font-size: 15px;
    }

    .hiddenBlock {
        display: none;
    }

    .form {
        overflow: hidden;
    }

    .buttons {
        width: 98%;
        margin: 2% auto;
    }

    #podpiskaUr {
        float: right;
    }

    .star {
        color: red;
    }

    .first {
        width: 24%;
    }

    .second {
        width: 15%;
    }

    .third {
        width: 52%;
    }

    .four {
        width: 10%;
    }

    .five {
        width: 29%;
    }

    .six {
        width: 47%;
    }

    .six2 {
        width: 49%;
        margin-bottom: 2%;
    }

    .seven {
        width: 13%;
    }

    .question-pic {
        position: absolute;
        top: -6px;
        right: -4px;
        width: 15px;
    }

    .hiddenBlock .six {
        margin-bottom: 2%;
    }

    .form-block label {
        font-size: 12px;
    }

    @media screen and (min-width: 100px)  and (max-width: 920px) {
        .main-container {
            width: 100%;
        }

        .row-1 .first {
            width: 35%;
        }

        .row-1 .third {
            width: 41%;
        }

        .month {
            margin-top: 1%;
        }
    }

    @media screen and (min-width: 100px)  and (max-width: 610px) {
        .main-container {
            width: 100%;
            margin-top: 20px;
        }

        .row-1 .first {
            width: 41%;
        }

        .row-1 .third {
            width: 35%;
        }

        .buttons {
            width: 100%;
        }

        @media screen and (min-width: 100px)  and (max-width: 580px) {
            .form-block {
                width: 100% !important;
            }

            .form-block input {
                width: 99% !important;
            }

            .form-block {
                margin-bottom: 2%;
            }

            .form-pic {
                height: auto;
                float: none;
                margin: 3% auto;
                width: 35%;
            }

            .header-block {
                width: 100%;
            }
        }
</style>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'subscribe-form',
    'action' => yii\helpers\Url::to('@domain/subscribe/create-pdf'),
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
]);
?>


<div class="main-container">
    <div class="form-header">
        <div class="form-pic">
            <img src="https://www.osp.ru/data<?php echo $issue->Picture ?>" alt="" class="picture">
        </div>
        <div class="header-block">
				<span class="header-text">
				Купить журнал <br/>
				<span class="name">"<?php echo $journal->full_name ?>" <?php echo $issue->issue_title ?></span><br/>
				</span>
            <div class="version">
                <form action="" class="form">
                    <div class="row-1">
                        <div class="form-block third">
                            <label for="">Укажите Фамилию и Имя: <span class="star">*</span></label><br>
                            <?php echo $form->field($model, 'username', ['options' => ['id' => 'myid']])->textInput(['required' => 'required'])->label(false); ?>
                        </div>
                    </div>

                    <div class="row-1">
                        <div class="form-block third">
                            <label for="">Email: <span class="star">*</span></label><br>
                            <?php echo $form->field($model, 'email', ['options' => ['id' => 'myid']])->input('email',['required' => 'required'])->label(false); ?>
                        </div>
                    </div>

                    <?php echo $form->field($model, 'issue')->hiddenInput(['value' => $issue->Message_ID])->label(false); ?>

                 </form>
            </div>

            <div class="block-summa" style="margin-top: 150px">
                Сумма к оплате <span class="summa" id="price">0</span> руб <span id="release"></span>
            </div>
        </div>
    </div>
    <hr style="    border: 1px solid;
    margin-bottom: 30px;">


    <div class="buttons">
        <button type="submit" id="oform-zak">Оформить заказ</button>

    </div>
</div>


<?php ActiveForm::end(); ?>

