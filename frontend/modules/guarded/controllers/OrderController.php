<?php

namespace frontend\modules\guarded\controllers;

use common\models\NxOrder;
use common\models\Publisher;
use common\queue_jobs\SimpleEmailJob;
use common\services\PdfLogService;
use frontend\modules\guarded\model\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;
use yii\web\BadRequestHttpException;

class OrderController extends RootRestController
{

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'controllers' => ['guarded/order'],
                        'actions' => ['become-active', 'test'],
                        'allow' => true,
                        'roles' => ['ship_manager', 'accountant'],
//                        'matchCallback' => function($rule, $action) {
//                            return true;
//                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'become-active' => ['GET'],
                ],
            ],
        ];
        return ArrayHelper::merge(parent::behaviors(), $behaviors);

    }

    public function actionBecomeActive($id)
    {
        try {
            /** @var User $user */
            $user = Yii::$app->getUser()->getIdentity();
            if (empty($user)) {
                throw new BadRequestHttpException('user  not found',400);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        $orderId = intval($id);
        $order = NxOrder::findOne(['id' => $orderId]);

        if (empty($order)) {
            throw new BadRequestHttpException('order ' . $orderId .  ' not found',400);
        }

        if (intval($order->publisher->id) !== intval($user->publisher_id)) {
            throw new BadRequestHttpException('this order does not belong to you.',403);
        }

        $pdfLogService = new PdfLogService();
        $pdfLogService->updateLogsForOrder($orderId);
        return ['OK'];
    }

}