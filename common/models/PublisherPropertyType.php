<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "publisher_property_type".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $name
 * @property string $type
 * @property string $key
 *
 * @property PublisherProperty[] $publisherProperties
 * @property Publisher[] $publishers
 * @property PublisherPropertyGroup $group
 */
class PublisherPropertyType extends \common\models\CommonModel
{
    const SOAP_URL_FOR_1C = 11; // 1 это ID записи. Ожидается, что они не будут изменяться. К тому же это запрещено установленными правилами Restrict для внешних ключей
    const SOAP_LOGIN_FOR_1C = 12; //
    const SOAP_PASSWORD_FOR_1C = 13; //

    const SOAP_URL_FOR_1C_KEY = 'soapUrlFor1C';
    const SOAP_LOGIN_FOR_1C_KEY = 'soapLoginFor1C';
    const SOAP_PASSWORD_FOR_1C_KEY = 'soapPasswordFor1C';

    const EMAIL_SENDER_KEY_NAME = 'senderEmail';
    const POLICY_LINK_KEY_NAME = 'policyLink';
    const POLICY_LINK_TEXT_KEY_NAME = 'policyLinkText';
    const POLICY_LABEL_TEXT_KEY_NAME = 'labelText';



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_property_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'name'], 'required'],
            [['group_id'], 'integer'],
            [['type','key'], 'string'],
            [['name'], 'string', 'max' => 70],
            [['group_id', 'name'], 'unique', 'targetAttribute' => ['group_id', 'name'], 'message' => 'В указанной группе свойств уже есть свойство с таким именем.'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => PublisherPropertyGroup::class, 'targetAttribute' => ['group_id' => 'id'], 'message' => 'Указанная группа свойств отсутствует'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Группа',
            'name' => 'Название свойства',
            'type' => 'Тип',
            'key' => 'Служебный ключ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherProperties()
    {
        return $this->hasMany(PublisherProperty::className(), ['property_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['id' => 'publisher_id'])->viaTable('publisher_property', ['property_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(PublisherPropertyGroup::className(), ['id' => 'group_id']);
    }
}
