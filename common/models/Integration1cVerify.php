<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "integration1c_verify".
 *
 * @property integer $publisher_id
 * @property integer $is_verified
 * @property string $last_verification_date
 * @property string $verification_result
 *
 * @property Publisher $publisher
 */
class Integration1cVerify extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'integration1c_verify';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher_id'], 'required'],
            [['publisher_id', 'is_verified'], 'integer'],
            [['last_verification_date'], 'safe'],
            [['verification_result'], 'string'],
            [['publisher_id'], 'unique'],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'publisher_id' => 'Piblisher ID',
            'is_verified' => 'Is Verified',
            'last_verification_date' => 'Last Verification Date',
            'verification_result' => 'Verification Result',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPiblisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
}
