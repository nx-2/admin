<?php

return [
    'subscribeManager' => [
        'type' => 1,
        'description' => 'Staff, managing subscriptions.',
    ],
    'manageRoles' => [
        'type' => 2,
        'description' => 'any manipulations to roles entities',
    ],
    'updateRole' => [
        'type' => 2,
        'description' => 'change content of role entity',
    ],
    'createPublisher' => [
        'type' => 2,
        'description' => 'create new publisher entity',
    ],
    'updatePublisher' => [
        'type' => 2,
        'description' => 'change content of publisher entity',
    ],
    'deletePublisher' => [
        'type' => 2,
        'description' => 'delete publisher entity',
    ],
    'createUser' => [
        'type' => 2,
        'description' => 'create new User',
    ],
    'updateUser' => [
        'type' => 2,
        'description' => 'create new User',
    ],
    'managePrices' => [
        'type' => 2,
        'description' => 'any manipulations with prices',
    ],
    'manageIssues' => [
        'type' => 2,
        'description' => 'any manipulations with issues',
    ],
    'manageActions' => [
        'type' => 2,
        'description' => 'any manipulations with actions',
    ],
    'updateOwnProfile' => [
        'type' => 2,
        'description' => 'change content of own profile',
        'ruleName' => 'isOwner',
    ],
    'manageEditions' => [
        'type' => 2,
        'description' => 'any manipulations with editions',
    ],
    'manageDirectories' => [
        'type' => 2,
        'description' => 'any manipulations with help tables (companies, persons, addresses, countries..) ',
    ],
    'accessSubscribeForms' => [
        'type' => 2,
        'description' => 'access to page of subscription forms',
    ],
    'managePaySystems' => [
        'type' => 2,
        'description' => 'payment systems managment',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'administrator role',
        'children' => [
            'updateOwnProfile',
            'createPublisher',
            'updatePublisher',
            'deletePublisher',
            'manageDirectories',
            'accessSubscribeForms',
        ],
    ],
    'developer' => [
        'type' => 1,
        'description' => 'developer role. God level.',
        'children' => [
            'manageRoles',
            'admin',
            'publisher',
        ],
    ],
    'subscribe_manager' => [
        'type' => 1,
        'description' => 'Staff, managing subscriptions.',
        'children' => [
            'managePrices',
            'manageActions',
            'ship_manager',
        ],
    ],
    'accountant' => [
        'type' => 1,
        'children' => [
            'updateOwnProfile',
        ],
    ],
    'ship_manager' => [
        'type' => 1,
        'description' => 'Staff, managing shipping',
        'children' => [
            'updateOwnProfile',
            'manageIssues',
            'accessSubscribeForms',
        ],
    ],
    'publisher' => [
        'type' => 1,
        'description' => 'Top manager of publisher.',
        'children' => [
            'subscribeManager',
            'updatePublisher',
            'createUser',
            'updateUser',
            'manageEditions',
            'managePaySystems',
            'subscribe_manager',
        ],
    ],
];
