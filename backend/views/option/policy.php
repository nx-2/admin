<?php

use common\models\PublisherProperty;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var PublisherProperty[] $options
 */

$this->title = 'Политика обработки персональных данных';
?>
<?php
$formOptions = [
    'enableAjaxValidation' => false,
    'id' => 'mail-options-form',
    'validateOnBlur' => false,
    'options' => ['class' => 'form-of-jwt-options'],
//        'action' => ''
];

$form = ActiveForm::begin($formOptions);
?>
<div class='box box-primary'>
    <div class='box-header with-border'>
        <h3 class='box-title'>
            Данные для формирования ссылки на политику обработки персональных данных в подписных формах
        </h3>
    </div>
    <div class='box-body'>
        <div class="row">
            <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                <?php foreach ($options as $index => $option): ?>
                    <?php
                        if (!$option->isNewRecord) {
                            echo(yii\helpers\Html::activeHiddenInput($option, "[$index]id"));
                        }
                    ?>
                    <?php echo(yii\helpers\Html::activeHiddenInput($option, "[$index]property_type_id")); ?>
                    <?php echo(yii\helpers\Html::activeHiddenInput($option, "[$index]publisher_id")); ?>

                    <div>
                        <label style="font-size:12px;margin-bottom:0px"
                               for="publisherproperty-<?php echo($index); ?>-value"><?php echo($option->propertyType->name); ?></label>
                        <?= $form->field(
                            $option,
                            "[$index]value",
                            [
                                'template' => "{input}\n{hint}\n{error}",
                                'labelOptions' => ['style' => 'font-size:12px;margin-bottom:0px']
                            ]
                        )->textInput(['class' => 'form-control input-sm', 'data-prop_name' => $option->propertyType->name]) ?>


                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    <div class='box-footer'>
        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary pull-left']); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    function updateSecret(event){
        console.log(event);
        $('#publisherproperty-0-value').attr('value', genRandomString());
    }
    function genRandomString() {
        const data = {
            digits: '0123456789',
            lowcaseChars: 'abcdefghijklmnopqrstuvwxyz',
            uppercaseChars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            specialChars: '~!@#$%^&*()-=+[]{}',
        };
        const rules = {
            lowcaseChars: 6,
            uppercaseChars: 6,
            digits: 3,
            specialChars: 3
        }
        const totalLen = Object.values(rules).reduce((r,v) => r + v, 0);
        let result = ','.repeat(totalLen - 1).split(',');
        let posInResult = 0;
        let posInData = 0;
        Object.keys(data).forEach(function (keyString) {
            for (let i = 1; i <= rules[keyString]; i++) {
                posInResult = Math.floor(Math.random() * totalLen);
                while(result[posInResult] !== '' && result.join('').length < totalLen) {
                    posInResult = Math.floor(Math.random() * totalLen);
                }
                posInData = Math.floor(Math.random() * data[keyString].length);
                let char = data[keyString].substring(posInData, posInData + 1);
                result[posInResult] = char;
            }
        })
        return result.join('');
    }
</script>



