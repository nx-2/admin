<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\controllers;

use Yii;
use common\models\User;
use backend\modules\admin\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                //     'delete' => ['POST'],
                // ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->setScenario('create');

        // $pr = Yii::$app->request->post('User', ['Password_repeat' => null])['Password_repeat'];
        // $model->Password_repeat = $pr;

        if ($model->load(Yii::$app->request->post()) && $model->uploadImage()) {

            if ($delCurrentImage = (Boolean) Yii::$app->request->post('delete-file', false) ) {
                $model->deleteOldImagesFiles();
                $model->avatar = null;
            }

            if ($model->save(false))
                Yii::$app->session->addFlash('success', 'Успешно создан новый пользователь.');
            else
                Yii::$app->session->addFlash('danger', 'Не сохранено!');

            return $this->redirect(['update', 'id' => $model->User_ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');
        // $pr = Yii::$app->request->post('User')['Password_repeat'];
        // $model->Password_repeat = $pr;

        if ($model->load(Yii::$app->request->post()) && $model->uploadImage()) {

            if ($delCurrentImage = (Boolean) Yii::$app->request->post('delete-file', false) ) {
                $model->deleteOldImagesFiles();
                $model->avatar = null;
            }

            if ($model->save(false))
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            else
                Yii::$app->session->addFlash('danger', 'Не сохранено!');
        }

        return $this->render('update', [
            'model' => $model,
        ]);

            // return $this->redirect(['view', 'id' => $model->User_ID]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $login = $model->Login;
        if ($model  === null) {
            throw new NotFoundHttpException('The requested user does not exist.');
        }

        if( $model->delete() ) {
            Yii::$app->session->addFlash('success', 'Пользователь "' . $login . '" удален.');
            // return $this->redirect(['index']);
            return $this->actionIndex();
        } else {
            Yii::$app->session->addFlash('danger', 'Пользователь "' . $login . '" НЕ был удален.');
            return $this->redirect(['update', 'id' => $model->User_ID]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            // $model->Password = '';
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
