<?php

use yii\db\Migration;

class m170124_110759_add_column_to_market_place extends Migration
{
    public function up()
    {
        $code = '
            ALTER TABLE `ads`.`market_blocks` 
            ADD COLUMN `image_full_path` VARCHAR(255) NULL DEFAULT NULL COMMENT \'полный путь к физическому файлу на сервере\' AFTER `image`;
        ';
        $this->execute($code);
    }

    public function down()
    {
        echo "m170124_110759_add_column_to_market_place cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
