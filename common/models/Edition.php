<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "Edition".
 *
 * @property integer $Message_ID
 * @property integer $Checked
 * @property string $fullNameEn
 * @property string $TimeToDelete
 * @property string $TimeToUncheck
 * @property string $created
 * @property integer $create_user_id
 * @property string $last_updated
 * @property string $start_subscription
 * @property string $name_for_applications
 * @property integer $last_user_id
 * @property string $HumanizedName
 * @property string $data_format
 * @property string $EnglishName
 * @property string $full_name
 * @property string $mask_for_reference
 * @property string $issue_mask
 * @property string $ncDescriptionEn
 * @property integer $subscribe
 * @property integer $esubscribe
 * @property string $ncDescription
 * @property integer $User_ID
 *
 * @property EditionPassport[] $editionPassports
 * @property Issue[] $issues
 * @property NxSubscription[] $nxSubscriptions
 * @property PublisherEditions[] $publisherEditions
 */
class Edition extends WatchableModel
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Edition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Checked', 'create_user_id', 'last_user_id', 'subscribe', 'esubscribe', 'User_ID', 'start_subscription'], 'integer'],
            ['start_subscription', 'default', 'value' => 0],
            [['TimeToDelete', 'TimeToUncheck', 'created', 'last_updated', 'name_for_applications'], 'safe'],
            [['full_name', 'HumanizedName'], 'required'],
            [['ncDescriptionEn', 'ncDescription'], 'string'],
            [['fullNameEn', 'HumanizedName', 'EnglishName', 'full_name', 'mask_for_reference', 'issue_mask'], 'string', 'max' => 255],
            [['name_for_applications', 'data_format'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Message_ID'            => 'ИД',
            'full_name'             => 'Полное название (Ru)',
            'fullNameEn'            => 'Полное название (En)',
            'HumanizedName'         => 'Сокращенное название (Ru)',
            'EnglishName'           => 'Сокращенное название (En)',
            'ncDescription'         => 'Описание (Ru)',
            'ncDescriptionEn'       => 'Описание (En)',
            'start_subscription'    => 'Начало подписки возможно через(мес.)',
            'issue_mask'            => 'Маска для формирования имени выпуска',
            'name_for_applications' => 'Системное имя',
            'mask_for_reference'    => 'Маска для построении имени файла PDF-версии',
            'subscribe'             => 'Печатная версия',
            'esubscribe'            => 'PDF-версия',
            'data_format'           => 'Формат данных',
            'Checked'               => 'Активность',
            'TimeToDelete'          => 'Удалено',
            'TimeToUncheck'         => 'Отключено',
            'created'               => 'Создано',
            'create_user_id'        => 'Создано пользователем',
            'last_updated'          => 'Обновлено',
            'last_user_id'          => 'Обновлено пользователем',
            'User_ID'               => 'Пользователь',
        ];
    }

    /**
     * @return string
     * returns existing url to cover picture for last issued issue of requested edition
     * or empty string if not any
    **/
    public function lastCoverUrl()
    {
        $url = '';
        if ($this->Message_ID == null)
            return $url;

        //получить десять последних выпусков издания со статусом "готовится" или "выпущен" и непустым полем Picture
        $issues = $this->hasMany(Issue::className(), ['MagazineID' => 'Message_ID'])
                ->select(['Picture'])
                ->where('Picture IS NOT NULL')
                ->andWhere('Picture <> ""')
                ->andWhere('status_id IN (1,2)')
                ->orderBy('PublicDate DESC')
                ->limit(3)
                ->asArray(true)
                ->all();

        $urlExists = false;

        foreach($issues as $issue) {
            //проверить существование найденных url (для http и htpps)
            $url = $issue['Picture'];

            $currentScheme = parse_url($url, PHP_URL_SCHEME);
            $anotherScheme = $currentScheme == 'http' ? 'https' : 'http';

            $headers = @get_headers($url);
            if(strpos($headers[0],'200') !== false) {
                $urlExists = true;
                break;
            }
            $url = str_replace($currentScheme, $anotherScheme, $url);

            $headers = @get_headers($url);
            if(strpos($headers[0],'200') !== false) {
                $urlExists = true;
                break;
            }

        }
        return $urlExists ? $url : '';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditionPassport()
    {
        return $this->hasOne(EditionPassport::className(), ['edition_id' => 'Message_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssues()
    {
        return $this->hasMany(Issue::className(), ['MagazineID' => 'Message_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxSubscriptions()
    {
        return $this->hasMany(NxSubscription::className(), ['periodical_id' => 'Message_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this
            ->hasOne(Publisher::className(), ['id' => 'publisher_id'])
            ->via('ownerid');
    }

    private function getClosestSubscrideDate()
    {
        if ( $this == null  || empty($this) )
            return false;

        $addMonthes = intval($this->start_subscription);

        $y = date('Y');
        $m = date('m');
        $d = '01';

        $date = new \DateTime($y . '-' . $m . '-' . $d); //начало текущего месяца
        $date->modify("+1 month"); // начало следующего
        return $date->modify(" +1 $addMonthes month")->format('Y-m-d');
    }

    public function getMonthesList($startDate = false, $monthesAmount = 12)
    {
        if (!$startDate)
            $startDate = $this->getClosestSubscrideDate();

        $monthes = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];

        $currentDate = new \DateTime($startDate);
        $finalDate = new \DateTime($startDate);
        $finalDate->modify("+$monthesAmount month");

        $toReturn = [];
        while( $currentDate < $finalDate ) {
            $toReturn[$currentDate->format('Y-m-d')] = $monthes[$currentDate->format('m')] . ' ' . $currentDate->format('Y');
            $currentDate->modify('+1 month');
        }
        return $toReturn;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherEditions()
    {
        return $this->hasMany(PublisherEditions::className(), ['magazine_id' => 'Message_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerid()
    {
        return $this->hasOne(PublisherEditions::className(), ['magazine_id' => 'Message_ID'])
        ->where(['owner' => 1]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditionPeriods()
    {
        return $this->hasMany(EditionPeriod::className(), ['edition_id' => 'Message_ID']);
    }

    /**
     * @return string
     */
    public function getFormEmbedingCode($pId = false)
    {
        if ($this->Message_ID == null)
            return '';

        if (!$pId) {
            $pId = $this->ownerid->publisher_id;
        }
        $formWrapperId =  Yii::$app->params['subscribeFormWrapperId'];
        $src = Yii::$app->params['api']['host'] . '/subscr/js/' . $pId . '/' . $this->Message_ID;
        return "<script type='text/javascript' src='$src'></script><div id='$formWrapperId'></div>";
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!$insert) {
                $searchModel = new \backend\models\EditionSearch();
                $dataProvider = $searchModel->search(['EditionSearch' => ['Message_ID' => $this->Message_ID]]);

                if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && ($dataProvider->getCount() == 0))
                        throw new \yii\web\NotFoundHttpException('Извините, но вам сюда нельзя!');
            }

            return true;
        } else {
            return false;
        }
    }


    public function afterSave($insert, $changedAttributes)
    {

        if ($insert) {
            $publisher = new PublisherEditions();
            $publisher->publisher_id = Yii::$app->user->identity->publisher_id;
            $publisher->magazine_id = $this->Message_ID;

            $this->link('publisherEditions', $publisher);

        }

        parent::afterSave($insert, $changedAttributes);
    }
    public static function getPrice($params = [])
    {
        $emptyAnswer = [
            'price' => NULL,
            'price_id' => NULL,
            'publisher_id' => 'not enough data to get price',
            'message' => 'По заданным параметрам прайс не найден'
        ];

        if (empty($params) || !is_array($params) || !isset($params['publisherId'])
                || !isset($params['subscriberTypeId'])
                || !isset($params['editionId'])
                || !isset($params['deliveryTypeId']) || !isset($params['countryId']))
            return $emptyAnswer;

        // Костыль (надеюсь временный). Цена для ПДФ и самовывоза не должна зависить от страны. Поэтому принудительно ставим страну 165 (Россия)
        if ( $params['deliveryTypeId'] == DeliveryType::PDF_TYPE || $params['deliveryTypeId'] == DeliveryType::BY_YOUR_SELF )
            $params['countryId'] = Country::RUSSIA;


        $answer = \Yii::$app->db->createCommand('CALL edition_price_for_now(:publisher_id, :subscriber_type_id, :edition_id, :delivery_type_id, :country_id)')
                ->bindValue(':publisher_id', $params['publisherId'])
                ->bindValue(':subscriber_type_id', $params['subscriberTypeId'])
                ->bindValue(':edition_id', $params['editionId'])
                ->bindValue(':delivery_type_id', $params['deliveryTypeId'])
                ->bindValue(':country_id', $params['countryId'])
                ->queryOne();

        return !$answer ? $emptyAnswer : $answer;
    }

    public static function getIssuesCount ( $params = [] ) {

        $startDate = isset($params['startDate']) ? $params['startDate'] : false;
        $magId = isset($params['magId']) ? $params['magId'] : false;
        $monthes = isset($params['monthes']) ? $params['monthes'] : false;


        if ( !$magId || !$monthes || !$startDate )
            throw new \yii\web\BadRequestHttpException('mag Id, period and startdate are required');

        $d = \DateTime::createFromFormat('Y-m-d', $startDate);
        if( !( $d && $d->format('Y-m-d') === $startDate ) )
            throw new \yii\web\BadRequestHttpException('startDate should be in format YYYY-MM-DD');

        $result = \Yii::$app->db->createCommand("CALL issues_count(:startDate, :monthes, :magId)")
                      ->bindValue(':startDate' , $startDate )
                      ->bindValue(':monthes', $monthes)
                      ->bindValue(':magId', $magId)
                      ->queryOne();

        return $result;
    }

}
