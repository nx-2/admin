<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class DownloadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // незалогиненым (гостям) разрешить только страницу входа и страницу ошибки
                    [
                        'controllers' => ['download'],
                        'actions' => ['issue-file'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIssueFile($filehash)
    {
        if (empty($filehash))
            throw new \yii\web\NotFoundHttpException("requested file not found.");

        $link = \common\models\IssueDownloadFile::findOne(['file_hash' => $filehash]);
        
        if ($link == null) 
            throw new \yii\web\NotFoundHttpException("requested file not found.");
        
        if ( intval($link->expire) > 0 && time() > strtotime($link->expire) ) {
            throw new \yii\web\NotFoundHttpException("This link has expired. \n Срок действия этой ссылки истек.");
        }


        $fullFileName = $link->issue->getFileName();
        if ($fullFileName && file_exists($fullFileName)) {
            $link->downloaded_times = $link->downloaded_times + 1;
            $link->last_ip = Yii::$app->request->getUserIP();
            $link->last_user_agent = Yii::$app->request->getUserAgent();

            $link->update(['downloaded_times', 'last_ip', 'last_user_agent']);
            return Yii::$app->response->sendFile($fullFileName);
        } else {
            throw new \yii\web\NotFoundHttpException("requested file does not exist.");
        }
    }

}
