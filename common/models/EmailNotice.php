<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "email_notice".
 *
 * @property integer $id
 * @property string $name
 * @property string $email_subject
 * @property string $key_string
 *
 * @property EmailNoticeTemplate[] $emailNoticeTemplates
 * @property EmailNoticeTemplate | NULL $defaultTemplate
 */
class EmailNotice extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'key_string'], 'required'],
            [['name', 'key_string'], 'string', 'max' => 50],
            [['email_subject'], 'string', 'max' => 100],
            [['key_string'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название уведомления',
            'email_subject' => 'Тема письма',
            'key_string' => 'Имя события, генерируемого моделью',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultTemplate()
    {
        return $this->hasOne(EmailNoticeTemplate::className(), ['notice_id' => 'id'])->where(['is_default' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailNoticeTemplates()
    {
        return $this->hasMany(EmailNoticeTemplate::className(), ['notice_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherTemplate($publisherId = false)
    {
        if (!$publisherId) {
            return null;
        }
        return $this->hasOne(EmailNoticeTemplate::class, ['id' => 'template_id'])
            ->viaTable(
                'publisher_notice_template',
                ['notice_id' => 'id'],
                function ( $query )use ($publisherId) {
                    $query->where(['publisher_id' => $publisherId]);
                    return $query;
                }
        );

//        return $this->hasMany(EmailNoticeTemplate::className(), ['notice_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        //манипуляции с папками и файлами там где хранятся шаблоны писем
        $viewPath = Yii::getAlias(Yii::$app->components['mailer']['viewPath']);
        if ( $insert ) {
            $dirName = $viewPath . '/' . $this->key_string;
//            if ( !dir($dirName) )
                mkdir($dirName, 0775, true); // создать по ключевому имени
        } else {
            // при изменении ключевого имени перенести всю старую папку (если она есть) во вновь созданную новую
            if ( array_key_exists('key_string', $changedAttributes) ) {
                if ( is_dir( $viewPath . '/' . $changedAttributes['key_string'] ) ) {
                    rename( $viewPath . '/' . $changedAttributes['key_string'] , $viewPath . '/' . $this->key_string );
                } else {
                    mkdir($viewPath . '/' . $this->key_string, 0775, true);
                }
            }
        }
    }
    public function afterDelete() {
        parent::beforeDelete();

        // манипуляции с файлами

        $viewPath = Yii::getAlias(Yii::$app->components['mailer']['viewPath']);
        if ( is_dir($viewPath . '/' . $this->key_string) ) {
            try {
                //удалить папку вместе со всем содержимым
                array_map( 'unlink', glob( $viewPath . '/' . $this->key_string . '/*' ) ); //delete all files in dir
                rmdir($viewPath . '/' . $this->key_string);
            } catch (Exception $ex) {

            }
        }
    }
}
