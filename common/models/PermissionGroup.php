<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "PermissionGroup".
 *
 * @property integer $PermissionGroup_ID
 * @property string $PermissionGroup_Name
 *
 * @property UserGroup[] $userGroups
 */
class PermissionGroup extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PermissionGroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PermissionGroup_Name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PermissionGroup_ID' => 'Permission Group  ID',
            'PermissionGroup_Name' => 'Permission Group  Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        return $this->hasMany(UserGroup::className(), ['PermissionGroup_ID' => 'PermissionGroup_ID']);
    }
}
