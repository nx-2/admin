<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class CommonModel extends ActiveRecord
{
    // Здесь мы при необходимости можем создать какую-то общую для всех моделей функциональность
    // Предполагается, что все common\models наследуются от данной модели 

    public static function getDbName()
    {
        if (preg_match('/dbname' . '=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return ($match[1]);
        } else {
            return false;
        }
    }
    public static function getConnectionHost()
    {
        if (preg_match('/host' . '=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return ($match[1]);
        } else {
            return false;
        }
    }
    public static function getConnectionPort()
    {
        if (preg_match('/port' . '=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return ($match[1]);
        } else {
            return false;
        }
    }
    
    public static function cloneModel($model = false)
    {
        if (!$model)
            return false;
        
        $modelClassName = $model::className();
        $newModel = new $modelClassName();
        $newModel->setAttributes($model->attributes);
        return $newModel;
    }
}
