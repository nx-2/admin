<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div>
    <div id='email-template-modal-wrapper'></div>
    <?php Pjax::begin(['id' => 'email-notices-list', 'enablePushState' => true]); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//                'id',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'vertical-align: middle;'],
                    // 'template' => '{update}',
                    'template' => '{update}',
                    // 'controller' => '',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            $url = Url::to(['email-notice/update','id' => $key]);
                            return Html::a(
                                '',
                                $url,
                                [
                                    'title' => 'редактировать',
                                    'aria-label' => 'редактировать',
                                    'data-pjax' => 0,
                                    'class' => 'fa fa-gear fa-font-18',

                                ]
                            );
                        },
                    ],
                ],
                'name',
                'email_subject',
                [
//                    'attribute' => 'PublisherTemplate',
                    'content' => function( $model, $key, $index, $column ) {
                            $template = $model->getPublisherTemplate( Yii::$app->user->identity->publisher->id )->one();
                            if ( $template === null ) {
                                $template = $model->defaultTemplate;
                            }
                            if ( $template !== null ) {
                                $showLink = Html::a('Просмотр', '#showTemplate', ['data-id' => $template->id,  'onclick' => 'showTemplateContent(event)']);
    //                            $showLink = Html::a($template->name, '#showTemplate', ['data-id' => $key,  'onclick' => 'showTemplateContent(event)']);
    //                            $contentSpan = Html::tag('span',$template->template_file , ['style' => 'margin-right: 7px']);
                                $contentSpan = Html::tag('span',$template->name, ['style' => 'margin-right: 7px']);
                                return $contentSpan . $showLink;
                            } else {
                                return(Html::tag('span', 'Шаблон отсутствует', ['style' => 'border-bottom: 1px dashed #bebebf;', 'class' => 'text-muted']));
                            }
                            
                    },
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
<script type="text/javascript">
    function showTemplateContent(event){
        if ( typeof event == 'undefined' ) {
            event = window.event;
        }
        
        event.preventDefault();
        $.ajax({
            url : '<?php echo( Url::to(['email-notice/get-template-html'], Yii::$app->request->getIsSecureConnection() ) ); ?>' + '/' + $(event.target).data('id'),
            type: 'get',
            dataType : 'json',
        }).done(function(response){
            if (response.status == 'OK') {
                $('#email-template-modal-wrapper').html(response.html);
                var modal = $('#email-template-modal-wrapper #email-notice-template-modal');
                if (modal.length > 0) {
                    modal.on('shown.bs.modal', function (event) {
                        setIframeSizes();
                    })
                    modal.modal('show');
                }
            } else {
                if ( response.hasOwnProperty('errMessage') ) {
                    alert( response.errMessage );
                }
            }
        })
        .fail(function(a,b,c){
            alert('что-то пошло не так. См. консоль');
            console,log(a);
            console,log(b);
            console,log(c);
        });
        return false;
    }
    function setIframeSizes( w, h ){
        w = !w ? 700 : w;
        h = !h ? false : h;
        
        var iframe = $('#popupTemplateIframe');
        iframe.width(w + 'px');
        if ( h == false ) {
            var iframeBody = iframe.contents().find('body');
            h = iframeBody.height() + 20;
        }
        iframe.height(h + 'px');
    }
</script>
    
