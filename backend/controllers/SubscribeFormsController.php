<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;

class SubscribeFormsController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                        parent::behaviors()['access'],
                        [
                            // 'class' => AccessControl::className(),
                            'rules' => [
                                //залогиненым издателям разрешить кое что только
                                [
                                    'actions' => ['index','test'],
                                    'allow' => true,
                                    'roles' => ['ship_manager'],
                                    'permissions' => ['accessSubscribeForms'],
//                                    'roles' => ['@'],
//                                    'matchCallback' => function ($rule, $action) {
//                                        $allowedRoles = ['publisher','subscribe_manager', 'admin'];
//                                        $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                        return (boolean)$allowed;
//                                    },
                                ],
                            ],
                        ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                //     'delete' => ['POST'],
                // ],
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

}
