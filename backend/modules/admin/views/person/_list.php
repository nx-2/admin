<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div class="nx-person-index">

    <p>
        <?= Html::a('Создание физ.лица', ['person/create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'company',
                'value' => 'company.name',
            ],
            'position',
            // 'position_type_id',
            'speciality',
             'phone',
            // 'telcode',
            // 'address_id',
            // 'email:email',
            // 'channel_id',
            // 'source_id',
            // 'name',
            // 'f',
            // 'i',
            // 'o',
            // 'gender',
            // 'checked',
            // 'enabled',
            // 'comment',
            // 'created',
            // 'last_updated',
            // 'create_user_id',
            // 'last_user_id',
            // 'label',
            // 'xpress_id',
            // 'xpress_type',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = Url::to(['person/update','id' => $key]);
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать физ.лицо',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',

                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '' ,
                            Url::to(['person/delete', 'id' => $key]),
                            [
                                'title' => 'Удалить издателя',
                                'aria-label' => 'Удалить',
                                'data-confirm'=>'Вы уверены, что хотите удалить это физ.лицо?',
                                'data-method'=> 'post',
                                'data-pjax' => '#publishers-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>