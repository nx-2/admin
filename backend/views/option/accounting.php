<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use common\models\PublisherProperty;
use common\models\PublisherPropertyType;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var PublisherProperty[] $models */

$this->title = 'Интеграция 1C';
//$this->params['breadcrumbs'][] = ['label' => 'Адреса', 'url' => ['address/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #soap-test-result-place {
        height: 300px;
        border: 1px dashed rgba(0, 0, 0, 0.3);
        background: #2B2B2B;
        overflow: auto;
        display: none;
        padding: 15px;
    }

    #soap-test-result-place .test-line {
        display: block;
        color: cyan;
        font-size: 10px;
        margin-bottom: 3px;
        font-family: monospace;
    }

    #soap-test-result-place .test-result {
        float: right;
        /*background: white;*/
        padding: 0px 5px;
        font-style: normal;
        font-weight: bold;
    }

    #soap-test-result-place .test-result.done {
        color: #02c002;
    }

    #soap-test-result-place .test-result.fail {
        color: #f55353;
    }

</style>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <!--
        <li> <a href="<?php // echo(Url::to(['option/accounting/test'])); ?>" data-toggle_="tab" aria-expanded="true">Тестирование</a></li>
        -->

        <li class="pull-left header">
            <i class="fa fa-th"></i>
            <h1 style='display:inline-block;'>
                <?= Html::encode($this->title); ?>
            </h1>
        </li>
    </ul>


    <div class="option-accounting-update">
        <?php
        $formOptions = [
            'enableAjaxValidation' => false,
//            'validationUrl' => Url::to(['address/validate-address']),
            // 'options' => ['data-pjax' => true],
            'id' => 'accounting-integration-form',
            'validateOnBlur' => false,
            'options' => ['class' => 'accounting-form'],
        ];

        $form = ActiveForm::begin($formOptions);
        ?>

        <div class='box box-primary'>
            <div class='box-header with-border'>
                <h3 class='box-title'>
                    Данные для доступа к soap сервису 1с
                </h3>
            </div>
            <div class='box-body'>
                <div class="row">
                    <div class='col-md-4 col-sm-4 col-xs-12' style='padding:0px 25px'>
                        <?php foreach ($models as $index => $model): ?>
                            <?php echo(yii\helpers\Html::activeHiddenInput($model, "[$index]property_type_id", [])); ?>
                            <?php echo(yii\helpers\Html::activeHiddenInput($model, "[$index]publisher_id", [])); ?>
                            <?php
//                            switch ($model->property_type_id) {
                            switch ($model->propertyType->key ) {
                                case PublisherPropertyType::SOAP_URL_FOR_1C_KEY:
                                    $propName = 'soap_url';
                                    break;
                                case PublisherPropertyType::SOAP_LOGIN_FOR_1C_KEY:
                                    $propName = 'soap_login';
                                    break;
                                case PublisherPropertyType::SOAP_PASSWORD_FOR_1C_KEY:
                                    $propName = 'soap_password';
                                    break;
                            }
                            ?>
                            <label style="font-size:12px;margin-bottom:0px"
                                   for="publisherproperty-<?php echo($index); ?>-value"><?php echo($model->propertyType->name); ?></label>
                            <?= $form->field($model, "[$index]value", ['template' => "{input}\n{hint}\n{error}", 'labelOptions' => ['style' => 'font-size:12px;margin-bottom:0px']])->textInput(['class' => 'form-control', 'data-prop_name' => $propName]) ?>
                        <?php endforeach; ?>
                    </div>
                    <div class='col-md-8 col-sm-8 col-xs-12'>
                        <?php
                        $verify_1c = Yii::$app->user->identity->publisher->integration1cVerify;
                        $integrationStatus = $verify_1c === null ? 'neverTested' : ($verify_1c->is_verified == 1 ? 'verified' : 'not_verified');
                        ?>
                        <?php if ($integrationStatus == 'verified'): ?>
                            <div class='alert alert-success alert-dismissible' style="padding: 5px;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                        style="top: -7px;right: -3px;">×
                                </button>
                                Тестирование предварительно сохраненных данных завершилось успешно. Интеграция с 1с
                                используется.
                            </div>
                            <div id='soap-test-result-place'></div>
                        <?php elseif ($integrationStatus == 'not_verified'): ?>
                            <?php $lastTestingResults = unserialize($verify_1c->verification_result); ?>
                            <div class='alert alert-danger alert-dismissible' style="padding: 5px;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                        style="top: -7px;right: -3px;">×
                                </button>
                                Тестирование предварительно сохраненных данных завершилось неудачно. Интеграция с 1с НЕ
                                используется. Результаты последнего тестирования см. в окне ниже.
                            </div>
                            <div id='soap-test-result-place' style='display:block'>
                                <?php if (isset($lastTestingResults['functions'])): ?>
                                    <?php foreach ($lastTestingResults['functions'] as $resultLine): ?>
                                        <span class="test-line"><?php echo($resultLine['testName']); ?>
                                            <?php if ($resultLine['testSuccess']): ?>
                                                <i class="test-result done">Тест пройден</i>
                                            <?php else: ?>
                                                <i class="test-result fail">Тест НЕ пройден</i>
                                            <?php endif; ?>
                                </span>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <?php if (isset($lastTestingResults['types'])): ?>
                                    <?php foreach ($lastTestingResults['types'] as $resultLine): ?>
                                        <span class="test-line"><?php echo($resultLine['testName']); ?>
                                            <?php if ($resultLine['testSuccess']): ?>
                                                <i class="test-result done">Тест пройден</i>
                                            <?php else: ?>
                                                <i class="test-result fail">Тест НЕ пройден</i>
                                            <?php endif; ?>
                                </span>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        <?php elseif ($integrationStatus == 'neverTested'): ?>
                            <div class='alert alert-info alert-dismissible' style="padding: 5px;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                        style="top: -7px;right: -3px;">×
                                </button>
                                Тестирование интеграции с 1с не проводилось. Интеграция НЕ используется.
                            </div>
                            <div id='soap-test-result-place'></div>
                        <?php endif; ?>
                    </div>
                    <div class='col-md-12 col-sm-12 col-xs-12' style='padding:0px 25px'>
                    </div>
                </div>

            </div>
            <div class='box-footer'>
                <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary pull-left']); ?>
                    <?= Html::button('Тестировать', ['class' => 'btn btn-danger pull-right', 'onclick' => 'testSoapServer(event)']); ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>
    function testSoapServer(event) {
        var answerWrapper = $('#soap-test-result-place');
        var form = $('#accounting-integration-form');
        var valueInputs = form.find('[data-prop_name]');
        var data = {};
        for (var k = 0; k < valueInputs.length; k++) {
            var input = $(valueInputs[k]);
            data[input.data('prop_name')] = input.val();
        }
        // answerWrapper.fadeOut(500);
        answerWrapper.html('');
        $.ajax({
            url: '/site/test-soap-server',
            dataType: 'json',
            data: data,
            type: 'post',
        }).done(function (response) {
            itemsToAdd = [];
            delay = 50;
            answerWrapper.fadeIn(500, function () {
                itemsToAdd = [];
                if (response.success == true) {
                    if (response.testingResult) {
                        var testingResults = response.testingResult;
                        if (testingResults.functions) {
                            var funcResults = testingResults.functions;
                            for (var k in funcResults) {
                                if (funcResults.hasOwnProperty(k)) {
                                    if (funcResults[k].testSuccess == 1) {
                                        var rawHtml = "<span class='test-line'>" + funcResults[k].testName + "<i class='test-result done'>" + "Тест пройден" + "</i></span>";
                                    } else {
                                        var rawHtml = "<span class='test-line'>" + funcResults[k].testName + "<i class='test-result fail'>" + "Тест НЕ пройден" + "</i></span>";
                                    }
                                    var insItem = $(rawHtml);
                                    itemsToAdd.push(insItem);
                                }
                            }
                        }
                        if (testingResults.types) {
                            var typeResults = testingResults.types;
                            for (var k in typeResults) {
                                if (typeResults.hasOwnProperty(k)) {
                                    if (typeResults[k].testSuccess == 1) {
                                        var rawHtml = "<span class='test-line'>" + typeResults[k].testName + "<i class='test-result done'>" + "Тест пройден" + "</i></span>";
                                    } else {
                                        var rawHtml = "<span class='test-line'>" + typeResults[k].testName + "<i class='test-result fail'>" + "Тест НЕ пройден" + "</i></span>";
                                    }
                                    var insItem = $(rawHtml);
                                    itemsToAdd.push(insItem);
                                }
                            }
                        }
                    }
                } else {
                    var rawHtml = "<span class='test-line'>" + response.errMessage + "<i class='test-result fail'>" + "Тест НЕ пройден" + "</i></span>";
                    var insItem = $(rawHtml);
                    itemsToAdd.push(insItem);
                }
                for (var i = 0; i < itemsToAdd.length; i++) {
                    setTimeout(function () {
                        var wrapper = $('#soap-test-result-place');
                        var index = wrapper.children().length;
                        wrapper.prepend($(itemsToAdd[index]));
                    }, (i + 1) * delay);
                }
            });
        })
            .fail(function (a, b, c) {
                // alert('Что-то пошло не так во врем тестирования. См. консоль');
                var errMessage = a.responseJSON ? a.responseJSON.message : 'Во время тестирования на сервере произошла ошибка';
                var insItem = $("<span class='test-line'>" + errMessage + "<i class='test-result fail'>" + "Тест НЕ пройден" + "</i></span>");
                delay = 15;
                itemsToAdd = [];
                itemsToAdd.push(insItem);
                $('#soap-test-result-place').show();
                for (var i = 0; i < itemsToAdd.length; i++) {
                    setTimeout(function () {
                        var wrapper = $('#soap-test-result-place');
                        var index = wrapper.children().length;
                        wrapper.prepend($(itemsToAdd[index]));
                    }, (i + 1) * delay);
                }
                console.log(a);
                console.log(b);
                console.log(c);
            });

    }
</script>