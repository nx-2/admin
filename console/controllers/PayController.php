<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\BaseConsole;
use \yii\helpers\Console;
use \yii\console\ExitCode;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use \common\models\YaKassaApiPayment;
use \common\custom_components\pay\YandexApiPay;


class PayController extends Controller
{
    public function actionYandexKassaApiRetreive()
    {
        $statusesList = "('" . YandexApiPay::PAYMENT_STATUS_PENDING . "','" . YandexApiPay::PAYMENT_STATUS_WAITING_FOR_CAPTURE . "')";
//        $paymentModels = YaKassaApiPayment::findAll('last_status IN ' . $statusesList );
        $paymentModels = YaKassaApiPayment::findAll(['last_status' => [YandexApiPay::PAYMENT_STATUS_PENDING, YandexApiPay::PAYMENT_STATUS_WAITING_FOR_CAPTURE]] );
        $counter = 0;
        foreach($paymentModels as $model) {
            $yaApiPay = new YandexApiPay($model->order_id, $model->publisher_id);
            if ($yaApiPay->hasErrors) {
                $str = 'ERROR! Payment for order: ' . $model->order_id . ' and yooKassa payment ' . $model->ya_payment_id . ' ';
                $str .= print_r($yaApiPay->getErrors(), true) .  "\n";
                $this->stdout(  $str, BaseConsole::FG_RED, BaseConsole::BOLD);
                continue;
            }
            $yaApiPay->setPaymentModel($model);
            $result = $yaApiPay->paymentNotify();
            if (!$result) {
                $str = 'ERROR! Payment for order: ' . $model->order_id . ' and yooKassa payment ' . $model->ya_payment_id . ' ';
                $str .= print_r($yaApiPay->getErrors(), true) .  "\n";
                $this->stdout($str, BaseConsole::FG_RED, BaseConsole::BOLD);
                continue;
            }
            $counter ++;
        }
        if (count($paymentModels) > 0) {
            $this->stdout( count($paymentModels) . ' uncompleted Yandex Kassa payments was found.' .  "\n", BaseConsole::FG_CYAN, BaseConsole::BOLD);
            $this->stdout( $counter . ' payments successfully processed.' .  "\n", BaseConsole::FG_CYAN, BaseConsole::BOLD);
        }
        return ExitCode::OK;

    }

}