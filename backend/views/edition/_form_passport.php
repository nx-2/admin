<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EditionPassport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="edition-passport-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'founderRu')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'yearOfFoundation')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'distributionRu')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'auditoryRu')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'licenseRu')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'addressRu')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'issn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'ageRestriction')->textInput(['maxlength' => true]) ?>


    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'founderEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'distributionEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'auditoryEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'licenseEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'addressEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'commentEn')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'commentRu')->textInput(['maxlength' => true]) ?>

    </div>




    <?= $form->field($model, 'edition_id')->hiddenInput(['value' => $modelEdition->Message_ID])->label(false); ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="form-group">
        <?= Html::submitButton('Сохранить' , ['class' =>  'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>