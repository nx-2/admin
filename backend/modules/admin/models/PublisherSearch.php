<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Publisher;
use yii\helpers\Url;
/**
 * PublisherSearch represents the model behind the search form about `common\models\Publisher`.
 */
class PublisherSearch extends Publisher
{
    public  $company;
    public  $contactPerson;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'contact_person_id', 'enabled'], 'integer'],
            [['logo', 'description', 'name', 'distribution', 'audience', 'official_email', 'created', 'last_updated', 'company', 'contactPerson'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Publisher::find();
        $query->joinWith(['company']);
        $query->joinWith(['person']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'distribution',
                    'audience',
                    'official_email',
                    'name', 
                    'description', 
                    'enabled',
                    'company' => [
                        'asc' => ['nx_company.name' => SORT_ASC],
                        'desc' => ['nx_company.name' => SORT_DESC],
                    ],
                    'contactPerson' => [
                        'asc' => ['nx_person.name' => SORT_ASC],
                        'desc' => ['nx_person.name' => SORT_DESC],
                    ],
                ],
                'route' => Url::to(['publisher/list']),
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'company_id' => $this->company_id,
            // 'contact_person_id' => $this->contact_person_id,
            // 'created' => $this->created,
            // 'last_updated' => $this->last_updated,
            'publisher.enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'publisher.description', $this->description])
            ->andFilterWhere(['like', 'publisher.name', $this->name])
            ->andFilterWhere(['like', 'publisher.distribution', $this->distribution])
            ->andFilterWhere(['like', 'publisher.audience', $this->audience])
            ->andFilterWhere(['like', 'publisher.official_email', $this->official_email])
            ->andFilterWhere(['like', 'nx_company.name', $this->company])
            ->andFilterWhere(['like', 'nx_person.name', $this->contactPerson]);
        return $dataProvider;
    }
}
