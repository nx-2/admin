<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use common\models\RelationSystems;
use backend\models\RelationSystemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaysystemController implements the CRUD actions for RelationSystems model.
 */
class PaysystemController extends SiteController
{
    public $publisherId;
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->publisherId = Yii::$app->user->identity->publisher_id;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                        parent::behaviors()['access'],
                        [
                            // 'class' => AccessControl::className(),
                            'rules' => [
                                //залогиненым издателям разрешить кое что только
                                [
                                    'actions' => ['update','index','create','delete','is-default-toggle','view'],
                                    'allow' => true,
                                    'roles' => ['publisher'],
//                                    'roles' => ['@'],
//                                    'matchCallback' => function ($rule, $action) {
//                                        $allowedRoles = ['publisher'];
//                                        $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                        return (boolean)$allowed;
//                                    },
                                ],
                            ],
                        ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RelationSystems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RelationSystemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RelationSystems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RelationSystems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RelationSystems();
        $model->publisher_id = $this->publisherId;
        
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post()); 

//            if ($model->enabled == 0 && $model->is_default == 1)
//                $model->is_default = 0;

            if ($model->save()){
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('danger', 'Не сохранено!');
            }
        }
        
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//        } else {
//        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RelationSystems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if (Yii::$app->request->isPost){
            $model->load(Yii::$app->request->post()); 
//            if ($model->enabled == 0 && $model->is_default == 1)
//                $model->is_default = 0;
            
            if($model->save()){
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            } else {
                Yii::$app->session->addFlash('danger', 'Не сохранено.');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RelationSystems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        
        return $this->actionIndex();
    }
    
    public function actionIsDefaultToggle()
    {
        $id = Yii::$app->request->post('id', false);
//        $value = Yii::$app->request->post('value', null);
        $fieldName = 'is_default';

        \Yii::$app->response->format = 'json';

        if (!$id) {
            return ['success' => false, 'errMessage' => 'Not enough data.'];
        }
        $model = \common\models\RelationSystems::findOne(['id' => intval($id), 'enabled' => 1]);
        if (empty($model))
                return ['success' => false, 'errMessage' => 'Record not found.'];

        $model->setAttribute($fieldName, intval(!(boolean) $model->getAttribute($fieldName)));

        if ($model->update(false, [$fieldName])) {
            return ['success' => true, 'id' => $id, 'checked' => $model->getAttribute($fieldName)];
        } else {
            return ['success' => false, 'errMessage' => 'Update failed', 'e' => $model->errors, 'enabled' => $model->getAttribute($fieldName)];
        }
    }

    /**
     * Finds the RelationSystems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RelationSystems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RelationSystems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
