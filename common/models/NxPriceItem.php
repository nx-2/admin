<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveQuery;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;



/**
 * This is the model class for table "nx_price_item".
 *
 * @property integer $id
 * @property integer $price_id
 * @property integer $item_id
 * @property integer $zone_id
 * @property integer $delivery_type_id
 * @property double $price
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $created
 * @property string $last_updated
 *
 * @property NxPrice $price0
 * @property NxZone $zone
 */
class NxPriceItem extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_price_item';
    }
    public function behaviors()
    {
        $myBehaviors =  [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user_id',
                'updatedByAttribute' => 'last_user_id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false, 
                // 'updatedAtAttribute' => 'last_updated',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created',
                    // ActiveRecord::EVENT_BEFORE_UPDATE => 'last_updated',
                ],
//                'value' => gmdate("Y-m-d\TH:i:s\Z"),// current UTC timestamp
                'value' => gmdate("Y-m-d H:i:s"),// current UTC timestamp
            ],
        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'delivery_type_id'], 'required'],
            ['price_id','required','except' => 'create'],
            [['price_id', 'item_id', 'zone_id', 'delivery_type_id', 'create_user_id', 'last_user_id'], 'integer'],
            [['price'], 'number', 'min' => 0, 'max' => 100000],
            [['price'], 'default', 'value' => 0],
            [['created', 'last_updated'], 'safe'],
            [['price_id', 'item_id', 'zone_id', 'delivery_type_id'], 'unique', 'targetAttribute' => ['price_id', 'item_id', 'zone_id', 'delivery_type_id'], 'message' => 'The combination of Price ID, Item ID, Zone ID and Delivery Type ID has already been taken.'],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxPrice::className(), 'targetAttribute' => ['price_id' => 'id']],
            [['zone_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxZone::className(), 'targetAttribute' => ['zone_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'price_id'          => 'Price ID',
            'item_id'           => 'Издание',
            'zone_id'           => 'Зона доставки',
            'delivery_type_id'  => 'Способ доставки',
            'price'             => 'Цена',
            'create_user_id'    => 'Создан пользователем',
            'last_user_id'      => 'Изменен пользователем',
            'created'           => 'Созжан',
            'last_updated'      => 'Изменен',
        ];
    }
    public static function getAllDeliveryPositions()
    {

        $zones =  DeliveryZone::find()
                ->where(['nx_zone.enabled' => 1])
                ->joinWith(
                    [
                        'deliveryTypes'=> function($query){
                                            $query->where(['nx_delivery_type.enabled' => 1]);

                        }
                    ],
                    true, 
                    'LEFT JOIN'
                )
                ->asArray(true)
                ->all();
            $toReturn = [];
            
            foreach($zones as $zone ) {
                foreach($zone['deliveryTypes'] as $dt) {
                    $column = [
                        'value' => $zone['name'] . '(' . $dt['name'] .')',
                        'zone_id' => $zone['id'],
                        'delivery_type_id' => $dt['id'],
                    ];
                    $toReturn[] = $column;
                }
            }
        return $toReturn;
    }
    
    public static function getPriceItemsData($priceId = false)
    {
        if (!$priceId)
            return [];
        $priceId = intval($priceId);
        
        $magazineIds = self::getEditionsForPrice($priceId);

        $toReturn = [];
        foreach($magazineIds as $magId) {
            $row = \common\models\NxPriceItem::find()
                    ->select(['value' => 'price','zone_id','delivery_type_id'])
                    ->where(['price_id' => $priceId, 'item_id' => $magId])
                    ->orderBy('zone_id, delivery_type_id')
                    ->asArray(true)
                    ->all();
            $toReturn[] = $row;
        }
        return  $toReturn;
    }
    
    public static function getEditionsForPrice($priceId = false)
    {
        if (!$priceId)
            return [];
        
        $mags =  NxPriceItem::find()
                ->select(['item_id'])
                ->where(['price_id' => intval($priceId)])
                ->groupBy('item_id')
                ->orderBy('item_id')
                ->asArray(true)
                ->all();
        $r = [];
        foreach($mags as $m) {
            $r[] = $m['item_id'];
        }
        return $r;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice0()
    {
        return $this->hasOne(NxPrice::className(), ['id' => 'price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryZone()
    {
        return $this->hasOne(DeliveryZone::className(), ['id' => 'zone_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryType()
    {
        return $this->hasOne(DeliveryType::className(), ['id' => 'zone_id']);
    }

    
    
    public function getEdition()
    {
        return $this->hasOne(Edition::className(), ['Message_ID' => 'item_id']);
    }



}
