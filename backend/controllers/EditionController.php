<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;


use common\models\EditionPassport;
use common\models\EditionPeriod;
use Yii;
use common\models\Edition;
use backend\models\EditionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\controllers\SiteController;

/**
 * EditionController implements the CRUD actions for Edition model.
 */
class EditionController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                parent::behaviors()['access'],
                [
                    'rules' => [
                        //залогиненым разрешить 
                        [
                            'actions' => ['create', 'update','delete','delete-items','index','getinfo'],
                            'allow' => true,
//                            'roles' => ['@'],
                            'roles' => ['publisher'],
//                            'matchCallback' => function ($rule, $action) {
//                                $allowedRoles = ['publisher','subscribe_manager', 'admin'];
//                                $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                return (boolean)$allowed;
//                            },
                        ],
                    ],
                ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Edition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EditionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Edition models.
     * @return mixed
     */
    public function actionGetinfo($id = false)
    {
        $toReturn = [];
        Yii::$app->response->format = 'json';
        
        if (!$id) {
            $toReturn['message'] = 'id must be supplied ';
            return $toReturn;
        }
            
        $data = \common\models\Edition::find()
                ->joinWith('editionPassport')
                ->joinWith('editionPeriods')
                ->asArray(true)
                ->where(['Message_ID' => intval($id)])
                ->one();
//        var_dump($data);
        
        $editionModel = new \common\models\Edition();
        $editionLabels = $editionModel->attributeLabels();
        
        $passportModel = new \common\models\EditionPassport();
        $passportLabels = $passportModel->attributeLabels();
        
        $periodModel = new \common\models\EditionPeriod();
        $periodLabels = $periodModel->attributeLabels();
       
        $toReturn['success'] = true;
        $toReturn['html'] = $this->renderPartial('view', [
                                'data' => $data,
                                'labels' => ['edition' => $editionLabels, 'passport' => $passportLabels, 'period' => $periodLabels],
        ]);
//        $toReturn['html'] = 'ffghfghfgh';

        return $toReturn;

    }

    
    /**
     * Displays a single Edition model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Edition model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Edition();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->Message_ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Edition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelPassport = ($edition = EditionPassport::findOne(['edition_id' => $model->Message_ID])) ? $edition : new EditionPassport();
        $modelPeriod =  new EditionPeriod();

        if ($model->load(Yii::$app->request->post(), 'Edition') && $model->save()) {
            return $this->redirect(['update', 'id' => $model->Message_ID]);

        } elseif ($modelPassport->load(Yii::$app->request->post(), 'EditionPassport') && $modelPassport->save()) {
            return $this->redirect(['update', 'id' => $model->Message_ID]);

        } elseif ($modelPeriod->load(Yii::$app->request->post(), 'EditionPeriod') && $modelPeriod->save()) {
            return $this->redirect(['update', 'id' => $model->Message_ID]);

        } else {

            return $this->render('update', [
                'model' => $model,
                'modelPassport' => $modelPassport,
                'modelPeriod' => $modelPeriod
            ]);
        }
    }

    /**
     * Deletes an existing Edition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $searchModel = new EditionSearch();
        $dataProvider = $searchModel->search(['EditionSearch'=>['Message_ID'=>$id,'owner'=>1]]);

        if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && ($dataProvider->getCount() == 0))
            throw new NotFoundHttpException('Извините, но вам сюда нельзя!');

        $model = $this->findModel($id);
        try {
            $model->delete();
        } catch (\Exception $e) {
            $a = 'dfgdfg';
        }
        return $this->actionIndex();

        // return $this->redirect(['index']);
    }


    /**
     * Delete items action
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeleteItems($id)
    {
        if (($model = \common\models\EditionPeriod::findOne($id)) !== null) {
            $id=$model->edition_id;
            $model->delete();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


        return $this->actionUpdate($id);
    }




    /**
     * Finds the Edition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Edition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        $searchModel = new EditionSearch();
        $dataProvider = $searchModel->search(['EditionSearch'=>['Message_ID'=>$id,'owner'=>1]]);


        if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && ($dataProvider->getCount() == 0))
                throw new NotFoundHttpException('Извините, но вам сюда нельзя!');



        if (($model = Edition::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
