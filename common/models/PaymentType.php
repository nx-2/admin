<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_payment_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property NxOrder[] $nxOrders
 */
class PaymentType extends \common\models\CommonModel
{
    const PAYMENT_TYPE_FREE     = 0;
    const PAYMENT_TYPE_BANK     = 1;
    const PAYMENT_TYPE_ONLINE   = 2;
    const PAYMENT_TYPE_CASH     = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_payment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrders()
    {
        return $this->hasMany(NxOrder::className(), ['payment_type_id' => 'id']);
    }
}
