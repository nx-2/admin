<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "Action".
 *
 * @property integer $id
 * @property string $date_start
 * @property string $date_end
 * @property string $action_code
 * @property string $name
 * @property string $desc
 *
 * @property ActionItems[] $actionItems
 * @property PromoCodes[] $promoCodes
 * @property PromoCodesOld[] $promoCodesOlds
 */
class Action extends CommonModel
{

    public $promocodes;
    public $count;

    public function init()
    {
        parent::init();
        $this->count=0;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end','promocodes'], 'safe'],
            [['action_code', 'name', 'desc'], 'required'],
            [['desc'], 'string'],
            [['action_code'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 245],
            [['publisher_id'], 'integer'],

            ['action_code', 'validateAction', 'on' => 'create'],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'action_code' => 'Код акции',
            'name' => 'Название',
            'desc' => 'Описание',
            'promocodes'=>'Промокоды',
            'count'=>'Кол-во'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionItems()
    {
        return $this->hasMany(ActionItems::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCodes()
    {
        return $this->hasMany(PromoCodes::className(), ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCodesOlds()
    {
        return $this->hasMany(PromoCodesOld::className(), ['action_id' => 'id']);
    }

    public function validateAction()
    {

        $action = self::find()
            ->where(['action_code' => $this->action_code])
            ->exists();

        if ($action) {
            $this->addError('action_code', 'Данный код акции уже используется.');
        }


    }

    /**
     * @param int $length
     * @param int $count
     * @param string $prefix
     * @return string
     */
    function generateRandomString($length = 6, $count = 1,$prefix='')
    {
        $array = [];

        if ($count == 1) {
            return $prefix.substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);

        }

        for ($i = 0; $i < $count; $i++) {
            $array[] = $prefix.substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);

        }

        return $array;

    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->date_start = \Yii::$app->formatter->asDate($this->date_start, 'php:Y-m-d H:i:s');
            $this->date_end = \Yii::$app->formatter->asDate($this->date_end,  'php:Y-m-d H:i:s');

            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->promocodes)) {
            $array = explode(' ', $this->promocodes);
            if (is_array($array)) {
                foreach ($array as $v) {
                    $promo = new PromoCodes();
                    $promo->action_id = $this->id;
                    $code = str_replace(array("\n","\r\n","\r"), '', $v);
                    $promo->code = $code;
                    $promo->save();
                }
            }


        }
    }

    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
    
    public static function getActionById ($id = false) 
    {
        if ( !$id ) 
            return [];
        
        $cache = Yii::$app->cache->get('getActionById_' . $id);
        if ( $cache != false)
            return $cache;
        
        $action = Action::find()
                ->select('a.*')
                ->from('Action a')
                ->where(['a.id' => intval($id)])
                ->joinWith('actionItems')
                ->asArray(true)
                ->one();
        
        if ( !empty($action) ) {
            $from = date('d-m-Y', strtotime($action['date_start']));
            $until = date('d-m-Y', strtotime($action['date_end']));
            $actionTerms = 'Акция с ' . $from . ' по ' . $until . '.<br>' . 'Скидка(и) на:<br> ' ;
            foreach( $action['actionItems'] as $item ) {
                $mag = \common\models\Edition::findOne(['Message_ID' => $item['mag_id']]);
                if ( $mag != null ) {
                    $ver = $item['type'] == 'paper' ? 'печатную' : 'ПДФ(электронную)';
                    $actionTerms .= '"' . $mag->HumanizedName . '" - ' . $item['discount'] . '%' . ' при подписке на ' . $item['months'] . ' месяцев ' . ' на ' . $ver . ' версию <br>';
                }
                
            }
            $action['actionTerms'] = $actionTerms;
        }
        Yii::$app->cache->set('getActionById_' . $id, $action, 300);
        
        return $action;
    }



}
