<?php

use yii\db\Migration;

class m170113_145725_change_ads_codes_foreign_key extends Migration
{
    public function up()
    {
        $this->execute(
            "ALTER TABLE `ads`.`ads_places` 
                DROP FOREIGN KEY `fk_ads_places_ads_codes`;
            ALTER TABLE `ads`.`ads_places` 
            ADD CONSTRAINT `fk_ads_places_ads_codes`
            FOREIGN KEY (`code_id`)
            REFERENCES `ads`.`ads_codes` (`id`)
            ON DELETE SET NULL
            ON UPDATE CASCADE;"
        );
    }

    public function down()
    {
        echo "m170113_145725_change_ads_codes_foreign_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
