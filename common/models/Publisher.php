<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use common\custom_components\event_handlers\PublishEventHandler;
use common\custom_components\event_handlers\UserEventHandler;
use Yii;
use common\models\behaviors\ImageUploadBehavior;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "publisher".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $contact_person_id
 * @property string $logo
 * @property string $description
 * @property string $name
 * @property string $distribution
 * @property string $audience
 * @property string $official_email
 * @property string $created
 * @property string $last_updated
 * @property integer $enabled
 * @property integer $create_user_id
 * @property integer $tax_system_id
 * @property integer $paper_vat_rate_id
 * @property integer $pdf_vat_rate_id
 *
 * @property User[] $users
 * @property NxPrice[] $nxPrices
 * @property PublisherEditions[] $publisherEditions
 * @property PublisherPartners[] $publisherPartners
 * @property PublisherPartners[] $publisherPartners0
 * @property VatRate $pdfVat
 * @property VatRate $paperVat
 * @property PublisherProperty[] $accountingIntegrationOptions
 * @property PublisherProperty[] $mailOptions
 * @property PublisherProperty[] $jwtOptions
 * @property PublisherProperty[] $policyOptions
 * @property PublisherProperty $emailSenderAddress
 *
 */
class Publisher extends \common\models\WatchableModel
{
    const NEW_PUBLISHER_EVENT_NAME = 'newPublishCreated';
    public $logoFile = null;

    public function init() {
        parent::init();
        $this->on(self::NEW_PUBLISHER_EVENT_NAME, [PublishEventHandler::class, 'newPublishCreated']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'contact_person_id', 'enabled', 'tax_system_id', 'paper_vat_rate_id','pdf_vat_rate_id'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id'], 'message' => 'Указанная компания отсутствует. Создайте новую и выберете ее из списка'],
            [['contact_person_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxPerson::className(), 'targetAttribute' => ['contact_person_id' => 'id'], 'message' => 'Указанное контактное лицо отсутствует. Создайте новое, затем выберете его из списка'],
            [['description'], 'string'],
            [['name', 'official_email', 'company_id', 'contact_person_id'], 'required'],
            [['created', 'last_updated','last_user_id', 'create_user_id'], 'safe'],
            [['logo', 'name', 'distribution', 'audience'], 'string', 'max' => 100],
            [['official_email'], 'string', 'max' => 50],
            [['official_email'], 'email'],
            [['name'], 'unique'],
            ['enabled', 'in', 'range' => ['0','1']],
            ['logoFile', 'image', 'extensions' => 'png, jpg, gif,',
                'skipOnEmpty' => true,
                 'minWidth' => 50, 'maxWidth' => 1000,
                 // 'minHeight' => 100, 'maxHeight' => 1000,
                 'message' => 'Принимаются изображения с расширением png, jpg, gif и шириной от 50 до 1000 пикселов',
            ],
        ];
    }
    
    public function behaviors()
    {
        $myBehaviors =  [
            'uploadLogo' => [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'logo',
                'fileField' => 'logoFile',
            ]
        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'company_id'        => 'Компания',
            'contact_person_id' => 'Контактное лицо',
            'logo'              => 'Логотип',
            'description'       => 'Описание',
            'name'              => 'Название',
            'distribution'      => 'Регион распространение',
            'audience'          => 'Возрастная маркировка',
            'official_email'    => 'Email для переписки',
            'created'           => 'Создан',
            'last_updated'      => 'Последнее обновление',
            'enabled'           => 'Активен',
            'logoFile'          => 'Логотип',
            'company'           => 'Комапния',
            'contactPerson'     => 'Контактное лицо',
            'create_user_id'    => 'Создан пользователем',
            'last_user_id'      => 'Обновлен пользователем',
            'tax_system_id'     => 'Система налогообложения',
            'paper_vat_rate_id' => 'Ставка НДС для печатных версий',
            'pdf_vat_rate_id'   => 'Ставка НДС для PDF-версий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['publisher_id' => 'id'])->inverseOf('publisher');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxSystem()
    {
        return $this->hasOne(TaxSystem::class, ['id' => 'tax_system_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaperVatRate()
    {
        return $this->hasOne(VatRate::class, ['id' => 'paper_vat_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPdfVatRate()
    {
        return $this->hasOne(VatRate::class, ['id' => 'pdf_vat_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyUser($userId = false)
    {
        if (!$userId || !is_numeric($userId))
            return null;

        return $this->hasMany(User::className(), ['publisher_id' => 'id'])
                ->where(['User_ID' => $userId]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPrices()
    {
        return $this->hasMany(NxPrice::className(), ['publisher_id' => 'id'])->inverseOf('publisher');
    }
    public function getPricesIds()
    {
        $pricesIds = [];
        foreach( $this->nxPrices as $price ) {
            $pricesIds [] = $price->id;
        }
        return $pricesIds;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherNoticeTemplates()
    {
        return $this->hasMany(PublisherNoticeTemplate::className(), ['publisher_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailNoticeTemplate($noticeId = false)
    {
        if (!$noticeId)
            return null;
        
        return $this->hasOne(EmailNoticeTemplate::className(), ['id' => 'template_id'])
                ->viaTable(
                        'publisher_notice_template', 
                        ['publisher_id' => 'id'], 
                        function ( $query )use ($noticeId) {
                            $query->where(['notice_id' => $noticeId]);
                            return $query;
                        }
        );
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailNoticeTemplates()
    {
        return $this->hasMany( EmailNoticeTemplate::className(), ['id' => 'template_id'])->via('publisherNoticeTemplates');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherEditions()
    {
        return $this->hasMany(PublisherEditions::className(), ['publisher_id' => 'id'])->inverseOf('publisher');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyPublisherEditions()
    {
        return $this->hasMany(PublisherEditions::className(), ['publisher_id' => 'id'])->where(['owner' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyEditionsIds()
    {
        //ID ВСЕХ (и выключенных тоже) МОИХ (где owner = 1) изданий. Без партнерских!!!
        $ids = [];
        $records =  $this
            ->hasMany(Edition::className(), ['Message_ID' => 'magazine_id'])
            ->via('myPublisherEditions')
            ->asArray(true)->all();
        foreach($records as $record) {
            $ids[] = $record['Message_ID'];
        }

        return $ids; 
    }

    /**
     * @return Array
     */
    public function getAllMyActiveEditions () 
    {
        $records = Edition::find()
            ->alias('e')
            ->join('LEFT JOIN', 'publisher_editions pe', 'e.Message_Id = pe.magazine_id')
            ->where(['e.Checked' => '1'])
            ->andWhere(['publisher_id' => $this->id])
//            ->hasMany(Edition::className(), ['Message_ID' => 'magazine_id'])
//            ->via('myPublisherEditions')
            ->asArray(true)->all();
        return $records;
        
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllMyActiveEditionsIds()
    {
        // ID моих активных изданий (вместе с партнерскими!!)
        $ids = [];
        $records = PublisherEditions::find()
            ->select(['magazine_id'])
            ->alias('pe')
            ->join('LEFT JOIN', 'Edition e', 'e.Message_Id = pe.magazine_id')
            ->where(['e.Checked' => '1'])
            ->andWhere(['publisher_id' => $this->id]);
//            ->hasMany(Edition::className(), ['Message_ID' => 'magazine_id'])
//            ->via('myPublisherEditions')
            $sql = $records->createCommand()->rawSql;
            $records = $records->asArray(true)->all();
        foreach($records as $record) {
            $ids[] = $record['magazine_id'];
        }

        return $ids; 
    }

    
    
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyEditions()
    {
        return $this
            ->hasMany(Edition::className(), ['Message_ID' => 'magazine_id'])
            ->via('myPublisherEditions');
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getMyEdition($eId = false)
    {
        if (!$eId) 
            return null;
        
        return $this
            ->hasOne(Edition::className(), ['Message_ID' => 'magazine_id'])->via('publisherEditions')->where(['Message_ID' => $eId])->one();
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getEditionsByIds( $eIds = [] )
    {
        if ( empty($eIds) || !is_array($eIds) ) 
            return null;
        
        
        return $this->hasMany(Edition::className(), ['Message_ID' => 'magazine_id'])->via('publisherEditions')->where(['Message_ID' => $eIds])->all();
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherPartners()
    {
        return $this->hasMany(PublisherPartners::className(), ['parent_id' => 'id'])->inverseOf('parent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherPartners0()
    {
        return $this->hasMany(PublisherPartners::className(), ['child_id' => 'id'])->inverseOf('child');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * returns all avalable pay systems for this publisher
     */
    public function getPaySystems()
    {
        return $this->hasMany(\common\models\RelationSystems::className(), ['publisher_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     * returns default pay system for this publisher
     */
    public function getDefaultPaySystem()
    {
        return $this->hasOne(\common\models\RelationSystems::className(), ['publisher_id' => 'id'])->where(['is_default' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(NxPerson::className(), ['id' => 'contact_person_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaperVat()
    {
        return $this->hasOne(VatRate::className(), ['id' => 'paper_vat_rate_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPdfVat()
    {
        return $this->hasOne(VatRate::className(), ['id' => 'pdf_vat_rate_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntegration1cVerify()
    {
        return $this->hasOne(\common\models\Integration1cVerify::className(), ['publisher_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery relative models with properties of 1c integration group
     *
     */
    public function getAccountingIntegrationOptions()
    {
        $groupKey = PublisherPropertyGroup::ACCOUNTING_KEY;
        $group = PublisherPropertyGroup::findOne(['key' => $groupKey]);
        if (empty($group)) {
            return [];
        }
        $groupId = intval($group->id);

        $typesIds = Yii::$app->db->createCommand('SELECT t.id FROM ' . PublisherPropertyType::tableName() .  ' t WHERE t.group_id = ' . $groupId)->queryAll();
        $typesIds = array_map(function($elem){
            return $elem['id'];
        }, $typesIds);

        return $this->hasMany(PublisherProperty::class, ['publisher_id' => 'id'])->where(['property_type_id' => $typesIds]);
    }

    public function getMailOptions()
    {
        $groupKey = PublisherPropertyGroup::MAIL_KEY;
        $group = PublisherPropertyGroup::findOne(['key' => $groupKey]);
        if (empty($group)) {
            return [];
        }
        $groupId = intval($group->id);
        $preQuery = new Query();
        $mailOptionsIds = $preQuery->select(['id'])->from(PublisherPropertyType::tableName())->where(['group_id' => $groupId])->all();
        $mailOptionsIds = array_map(function ($i) { return intval($i['id']); }, $mailOptionsIds);
        if (empty($mailOptionsIds)) {
            return [];
        }

        return $this->hasMany(PublisherProperty::class, ['publisher_id' => 'id'])->where(['property_type_id' => $mailOptionsIds]);
    }

    public function getJwtOptions()
    {
        $groupKey = PublisherPropertyGroup::JWT_KEY;
        $group = PublisherPropertyGroup::findOne(['key' => $groupKey]);
        if (empty($group)) {
            return [];
        }
        $groupId = intval($group->id);
        $preQuery = new Query();
        $jwtOptions = $preQuery->select(['id'])->from(PublisherPropertyType::tableName())->where(['group_id' => $groupId])->all();
        $jwtOptions = array_map(function ($i) { return intval($i['id']); }, $jwtOptions);
        if (empty($jwtOptions)) {
            return [];
        }

        return $this->hasMany(PublisherProperty::class, ['publisher_id' => 'id'])->where(['property_type_id' => $jwtOptions]);
    }

    public function getPolicyOptions()
    {
        $groupKey = PublisherPropertyGroup::POLICY_KEY;
        $group = PublisherPropertyGroup::findOne(['key' => $groupKey]);
        if (empty($group)) {
            return [];
        }
        $groupId = intval($group->id);
        $preQuery = new Query();
        $policyOptions = $preQuery->select(['id'])->from(PublisherPropertyType::tableName())->where(['group_id' => $groupId])->all();
        $policyOptions = array_map(function ($i) { return intval($i['id']); }, $policyOptions);
        if (empty($policyOptions)) {
            return [];
        }

        return $this->hasMany(PublisherProperty::class, ['publisher_id' => 'id'])->where(['property_type_id' => $policyOptions]);
    }

    public function buildPersonalPolicyBlock(): string
    {
        $policyOptions = $this->policyOptions;
        $data = [
            'policyLink' => NULL,
            'policyLinkText' => NULL,
            'labelText' => NULL,
        ];
        $data = array_reduce($policyOptions, function($res, $option) {
            /** @var PublisherProperty $option */
            if ($option->propertyType->key === PublisherPropertyType::POLICY_LINK_KEY_NAME) {
                $res['policyLink'] = $option->value;
            } elseif ($option->propertyType->key === PublisherPropertyType::POLICY_LINK_TEXT_KEY_NAME) {
                $res['policyLinkText'] = $option->value;
            } elseif ($option->propertyType->key === PublisherPropertyType::POLICY_LABEL_TEXT_KEY_NAME) {
                $res['labelText'] = $option->value;
            }
            return $res;
        },$data);
        extract($data);
//        $personalDataLink = '<a href="https://www.osp.ru/personalpolicy/">обработку моих персональных данных</a>';
        $personalDataLink = Html::a($policyLinkText, $policyLink, ['target' => '_blank']);

        $result = <<<HTML
            <label for='osp_dimeo_subscription_user_agreement'> $labelText $personalDataLink </label>
HTML;
        return $result;
    }

    public function getEmailSenderAddress()
    {
        $groupKey = PublisherPropertyGroup::MAIL_KEY;
        $group = PublisherPropertyGroup::findOne(['key' => $groupKey]);
        if (empty($group)) {
            NULL;
        }
        $groupId = intval($group->id);
        $preQuery = new Query();
        $mailOptionId = $preQuery->select(['id'])
            ->from(PublisherPropertyType::tableName())
            ->where(['group_id' => $groupId, 'key' => PublisherPropertyType::EMAIL_SENDER_KEY_NAME])
            ->one();

        if (empty($mailOptionId)) {
            NULL;
        }
        $mailOptionId = intval($mailOptionId['id']);
        return $this->hasOne(PublisherProperty::class, ['publisher_id' => 'id'])->where(['property_type_id' => $mailOptionId]);
    }

    /**
     * @param PublisherProperty[] $properties
     * @return array
     */
    public static function checkEmailServerConnection(array $properties): array
    {
        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->getMailer();

        /** @var \Swift_SmtpTransport $mailTransport */
        $mailTransport = $mailer->transport;
        $options = [
            'host' => false,
            'port' => false,
            'password' => false,
            'encryption' => false,
            'username' => false
        ];
        foreach($properties as $prop) {
            $propName = $prop->propertyType->key;
            if (array_key_exists($propName, $options)) {
                $options[$propName] = $prop->value;
            }
        }
        if (!empty($options['host'])) {
            $mailTransport->setHost($options['host']);
        }
        if (!empty($options['port'])) {
            $mailTransport->setPort($options['port']);
        }
        if (!empty($options['username'])) {
            $mailTransport->setUsername($options['username']);
        }
        if (!empty($options['password'])) {
            $mailTransport->setPassword($options['password']);
        }
        if (!empty($options['encryption'])) {
            $mailTransport->setEncryption($options['encryption']);
        }
//        if (!empty($options['host']) && !empty($options['port'])) {
//            $constructArgs = [$options['host'], $options['port']];
//        }
        $result = [
            'success' => NULL,
            'message' => NULL
        ];

        try {
            $mailTransport->start();
            $result['success'] = $mailTransport->isStarted();
        } catch (\Swift_TransportException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @return array
     *
     */
    public function soapServerConnectionData()
    {
        $integration1CProperties = $this->accountingIntegrationOptions;
        $auth = [];
        $url = false;
        $result = ['url' => false, 'auth' => []];
        foreach($integration1CProperties as $property) {
            if ($property->property_type_id == \common\models\PublisherPropertyType::SOAP_URL_FOR_1C)
                $result['url'] = $property->value;

            if ($property->property_type_id == \common\models\PublisherPropertyType::SOAP_LOGIN_FOR_1C)
                $result['auth']['login'] = $property->value;

            if ($property->property_type_id == \common\models\PublisherPropertyType::SOAP_PASSWORD_FOR_1C)
                $result['auth']['password'] = $property->value;
        }
        return $result;
    }
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // if ($insert) {
        //     $this->created = date('Y-m-d H:i:s');
        //     $this->create_user_id = Yii::$app->user->identity->User_ID;
        // }
        // $this->last_updated = date('Y-m-d H:i:s');
        
//        if (Yii::$app->user->identity->isRole(['admin'])
//        createPublisher, updatePublisher, deletePublisher
        $permName = $insert ? 'createPublisher' : 'updatePublisher';
        if (Yii::$app->user->can($permName))
            return true;

        $u_id = Yii::$app->user->identity->User_ID;
        $found = $this->getMyUser($u_id)->one();
        if($found !== null) {
            return true;
        } else {
            throw new \yii\web\ForbiddenHttpException('Вам не разрешен доступ к данным этого издателя');
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(parent::afterSave($insert, $changedAttributes)) {
            if ($insert) {
                $this->trigger(self::NEW_PUBLISHER_EVENT_NAME);
            }
            return true;
        }
        return false;
    }

    public static function getJwtSecretString(int $publisherId): string
    {
        $propertyGroupKey = PublisherPropertyGroup::JWT_KEY;

        $sqlString = <<<SQL
            SELECT pt.name, pt.key, pp.value 
            FROM publisher_property pp
            LEFT JOIN publisher_property_type pt ON pt.id = pp.property_type_id
            LEFT JOIN publisher_property_group pg ON pg.id = pt.group_id
            WHERE pg.key = '$propertyGroupKey'
            AND pp.publisher_id = $publisherId
SQL;
        $secretStringData = Yii::$app->getDb()->createCommand($sqlString)->query()->readAll();
        if (empty($secretStringData)) {
            return '';
        }
        $secretString = $secretStringData[0]['value'] ?? '';

        return $secretString;
    }
    // public function afterFind()
    // {
    //     $this->created = Yii::$app->formatter->asDatetime($this->created);
    //     $this->last_updated = Yii::$app->formatter->asDatetime($this->last_updated);
    //     // $this->created = Yii::$app->formatter->asDatetime($this->created, "php:d.m.Y H:i:s");
    //     // $this->last_updated = Yii::$app->formatter->asDatetime($this->last_updated, "php:d.m.Y H:i:s");

    //     // $this->created = date('d-m-Y H:i:s',strtotime($this->created));
    //     // $this->last_updated = date('d-m-Y H:i:s',strtotime($this->last_updated));
    //     parent::afterFind();
    // }
}
