<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "MagazineSubscribesQueue".
 *
 * @property int $id
 * @property int $subscribeID
 * @property int $subscriberID
 * @property string $email
 * @property int $send
 * @property string $sendTime
 * @property string $created
 * @property int $user_id
 * @property int $subscriptionID
 *
 * @property NxOrder $order
 * @property MagazineSubscribes $subscribe
 * @property NxOrder $orderByUser
 */
class MagazineSubscribesQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'MagazineSubscribesQueue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscribeID', 'subscriberID', 'send', 'user_id', 'subscriptionID'], 'integer'],
            [['sendTime', 'created'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['subscriptionID'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrder::class, 'targetAttribute' => ['subscriptionID' => 'id']],
            [['subscribeID'], 'exist', 'skipOnError' => true, 'targetClass' => MagazineSubscribes::class, 'targetAttribute' => ['subscribeID' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrder::class, 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscribeID' => 'Subscribe ID',
            'subscriberID' => 'Subscriber ID',
            'email' => 'Email',
            'send' => 'Отправлен',
            'sendTime' => 'Время отправки',
            'created' => 'Создан',
            'user_id' => 'ID пользователя',
            'subscriptionID' => 'ID заказа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder(): ActiveQuery
    {
        return $this->hasOne(NxOrder::class, ['id' => 'subscriptionID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe(): ActiveQuery
    {
        return $this->hasOne(MagazineSubscribes::class, ['id' => 'subscribeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderByUser(): ActiveQuery
    {
        return $this->hasOne(NxOrder::class, ['user_id' => 'user_id']);
    }
}
