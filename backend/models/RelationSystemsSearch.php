<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RelationSystems;

/**
 * RelationSystemsSearch represents the model behind the search form about `common\models\RelationSystems`.
 */
class RelationSystemsSearch extends RelationSystems
{
    public $system;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'system_id', 'publisher_id', 'is_default', 'enabled'], 'integer'],
            [['param_1', 'param_2', 'param_3','system'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RelationSystems::find();
        $query->joinWith(['system']);

        $publisherId = Yii::$app->user->identity->publisher_id;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'is_default', 
                    'enabled',
                    'system' => [
                        'asc' => ['systems.name' => SORT_ASC],
                        'desc' => ['systems.name' => SORT_DESC],
                    ],
                ],
//                'route' => 'adverts/places',
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'system_id' => $this->system_id,
            'publisher_id' => $publisherId,
            'is_default' => $this->is_default,
            'enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['like', 'param_1', $this->param_1])
            ->andFilterWhere(['like', 'param_2', $this->param_2])
            ->andFilterWhere(['like', 'systems.name', $this->system])
            ->andFilterWhere(['like', 'param_3', $this->param_3]);

        return $dataProvider;
    }
}
