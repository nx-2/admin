<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Action */

$this->title = 'Создать акцию';
$this->params['breadcrumbs'][] = ['label' => 'Акции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='col-md-5 col-sm-7 col-xs-12'>
    <div class='box box-primary'>
        <div class='box-header with-border'>
            <h3 class='box-title'>
                <?= Html::encode($this->title) ?>
            </h3>
        </div>
        <div style='padding: 20px;'>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>        </div>
    </div>
</div>


<div class='col-md-5 col-sm-7 col-xs-12'>
    <div class='box box-primary'>
        <div class='box-header with-border'>
            <h3 class='box-title'>
                Издания участники
            </h3>
        </div>
        <div style='padding: 20px;'>

            <?= $this->render('_form_mag', [
                'model' => $model,
                'modelItems'=>$modelItems
            ]) ?>        </div>
    </div>
</div>





<div class="clearfix"></div>