<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "nx_role".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 *
 * @property NxUserRole[] $nxUserRoles
 * @property User[] $users
 */
class NxRole extends \common\models\WatchableModel
{
    const SENSIBLE_ACCESS_ROLES = [
        'admin',
        'developer',
    ];

    const SAFE_ACCESSABLE_ROLES = ['accountant', 'ship_manager', 'subscribe_manager'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_role';
    }

    /**
     * @return  NxRole[]
     */
    public static function getCommonAccessableRoles(): array
    {
        return self::find()->where(['NOT IN', 'code', self::SENSIBLE_ACCESS_ROLES ])->orderBy('name DESC')->all();
    }

    /**
     * @return  NxRole[]
     */
    public static function getSafeAccessableRoles(): array
    {
        return self::find()->where(['IN', 'code', self::SAFE_ACCESSABLE_ROLES ])->orderBy('name DESC')->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code' ], 'required'],
            [['created', 'last_updated', 'create_user_id','last_user_id', 'description'], 'safe'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'code'          => 'Код',
            'created'       => 'Создано',
            'last_updated'  => 'Последнее обновление',
            'create_user_id' => 'Создано пользователем',
            'last_user_id'  => 'Обновлено пользователем',
            'description'   => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxUserRoles()
    {
        return $this->hasMany(NxUserRole::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['User_ID' => 'user_id'])->viaTable('nx_user_role', ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);
    }

}
