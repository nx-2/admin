/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

var requestAllowed = true;

//window on load event listeners
if (window.addEventListener) // W3C standard
{
  window.addEventListener('load', initBeforeSubmit, false);
  window.addEventListener('load', initBuildAddress, false);
  window.addEventListener('load', initBuildPerson, false);
}
else if (window.attachEvent) // Microsoft
{
  window.attachEvent('onload', initBeforeSubmit);
  window.attachEvent('onload', initBuildAddress);
  window.attachEvent('onload', initBuildPerson);
}
//END //window on load event listeners

$(document).ready(function(){
    $('.sidebar-menu .treeview .treeview-menu li a:not(.fixed)')
    .hover(
        // in
        function(event) {
            var me = $(this);
            me.find('.fa').addClass('fa-spin fa-fw text-red');
        },
        // out
        function(event) {
            var me = $(this);
            me.find('.fa').removeClass('fa-spin fa-fw text-red');
        }
    );
    $('.sidebar-menu li.selected').parents('ul').show();

    $('.get-ajax-form').click(function(event){
        if (typeof event == 'undefined')
            event = window.event;

        getForm($(event.target));
        event.preventDefault();

    });
    $("[data-mask]").inputmask();
    $("[data-datemask]").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
    if (typeof $().datepicker !== 'undefined') {
        $(".datepicker").datepicker({ autoclose:true });
    }

    if (typeof CKEDITOR != 'undefined'){
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;

        CKEDITOR.replaceAll('ckeditor-element');
    }
//    if ( typeof ace != 'undefined' ) {
//        var codeElems = document.querySelectorAll('.ace-code-editor');
//        editors = Array();
//        for (var k = 0; k < codeElems.length; k++) {
//           var editor = ace.edit(codeElems[k]);
//            editor.setTheme("ace/theme/monokai");
//            editor.getSession().setMode("ace/mode/html");
////            editor.getSession().setMode("ace/mode/php");
//           editors.push(editor);
//        }
//    }
    $('#issue-type').change(function(event){
        if (typeof event == 'undefined')
            event = window.event;

        var value = $(event.target).find('option:checked').val();
        var priceWrapper = $('#issueprice-wrapper');

        if (value == 2) {
            priceWrapper.slideDown(700);
        } else {
            priceWrapper.slideUp(700);
        }
    });
    $('#issue-publicdate').change(function(event){
        generateIssueNameOnMask();
    });
    $('#issue-form-wrapper #issue-magazineid').change(function(event){
        //получить маску для названия выпуска и сформировать по ней название
        if (typeof event == 'undefined')
            event = window.event;
        var newValue = $(event.target).find('option:checked').val();
        var url = apiUrl + '/editions/' + newValue;
        $.ajax({
            url : url,
            type : 'get',
            dataType: 'json',
        })
        .done(function(response, textStatus, jqXHR){
            if (jqXHR.status != 200) {
                console.log('status of response from ' + url + ' is ' + jqXHR.status + '. Execution terminated.' );
                return;
            }
            issueNameMask = response.hasOwnProperty('issue_mask') ?  response.issue_mask : '';
            generateIssueNameOnMask();

        })
        .fail(function(a,b,c){
            console.log(a);
            console.log(b);
            console.log(c);
        });

    });
    //навигация стрелками в таблице с ценами прайса
    $('#price-items-table tr td input[type="text"]').keydown(function(event){
        if (event.keyCode !== 13 && event.keyCode !== 10 && event.keyCode !== 38 && event.keyCode !== 40)
            return;

        if (event.keyCode === 13 || event.keyCode === 10) {
            event.preventDefault();
        }

        var currentCell = $(this).closest('td');
        var currentRow = currentCell.closest('tr');

        switch(event.keyCode) {
          case 10, 13 :
//                var nextRow = currentRow.prev('tr');
                var nextCell = currentCell.next('td');
            break;
          case 38://up
                var nextRow = currentRow.prev('tr');
                var nextCell = $(nextRow.find('td')[currentCell.index()]);
            break;
          case 40: //down
                var nextRow = currentRow.next('tr');
                var nextCell = $(nextRow.find('td')[currentCell.index()]);
            break;
          default:

            break;
        }
        nextCell.find('input[type="text"]').focus().select();
        return;
    });
})//end $(document).ready()
function getInfo(target, event) {
    if (typeof event == 'undefined') {
        event = window.event;
    }
    event.preventDefault();

    var showModalAfterLoad = false;

    if (target == 'company') {
        var id = $('#publisher-company-id').val();
        var host = '/company/getinfo/' + id;
        var modalPlaceSelector = '#company-service-wrap #company-info-modal-wrap';
        var relatedButton = $('#show-company-info');
    } else if(target == 'person') {
        var id = $('#publisher-person-id').val();
        var host = '/person/getinfo/' + id;
        var modalPlaceSelector = '#person-service-wrap #person-info-modal-wrap';
        var relatedButton = $('#show-person-info');
    } else if (target == 'edition') {
        var me = $(event.target);
        var id = me.closest('tr[data-key]').data('key');
        var host = '/edition/getinfo/' + id;
        var modalPlaceSelector = '#edition-info-modal-wrapper';
        var relatedButton = me;
        showModalAfterLoad = true;
    } else {
        return;
    }
    if (id) {
        relatedButton.addClass('loading-inside');
        $.ajax({
            url : host,
            type : 'get',
            dataType : 'json',
        })
        .done(function(data, status){
            if (status == 'success' && data.success) {
                $(modalPlaceSelector).html(data.html);
            }
            relatedButton.removeClass('loading-inside');
            if (showModalAfterLoad) {
                var m = $(modalPlaceSelector).find('.modal');
                if (m.length > 0) {
                    $(m[0]).modal();
                }
            }
        })
        .fail(function(a,b,c){
            console.log(a);
            console.log(b);
            console.log(c);
        });
    }
}
function generateIssueNameOnMask(){
            if (typeof issueNameMask == 'undefined' || issueNameMask == '')
                return;

            bootbox.confirm('Сформировать название выпуска по маске ' + issueNameMask, function(agree){
                if(!agree)
                    return;

                var dateText = $('#issue-form-wrapper #issue-publicdate').val();
                var dateParts = dateText.split('.');
                if (dateText != '' && dateParts.length > 2) {
                    var date = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                } else {
                    var date = new Date();
                }
                var mask = issueNameMask;
                var whatToReplace = mask.match(/\{.\}/g);

                if (whatToReplace != null && whatToReplace.length > 0) {
                    var twoDigitsFormatter = new Intl.NumberFormat('ru',{minimumIntegerDigits : 2});
                    var oneDigitsFormatter = new Intl.NumberFormat('ru',{minimumIntegerDigits : 1});
                    var m = date.getMonth() + 1;
                    var y = date.getFullYear().toString();

                    var replacementsMap = {
                        '{M}' : twoDigitsFormatter.format(m),
                        '{m}' : oneDigitsFormatter.format(m),
                        '{Y}' : y,
                        '{y}' : y.split('').splice(-2,2).join(''),
                    };

                    for (var k = 0; k < whatToReplace.length; k++) {
                        if (typeof(replacementsMap[whatToReplace[k]]) != 'undefined')
                            mask = mask.replace(whatToReplace[k], replacementsMap[whatToReplace[k]]);
                    }
                }
                $('#issue-form-wrapper #issue-name').val(mask);
            });
}

//ajax submit forms
function initBeforeSubmit() {
    $('body')
    .on('beforeSubmit','form[data-submitajax=1]', function(){ //отправка аяксом любой формы с аттрибутом data-submitajax
         var form = $(this);

         // Если в форме еще есть ошибки, то ничего не делаем
         if (form.find('.has-error').length) {
              return false;
         }

         var attr = form.attr('id');
         if (typeof attr == 'undefined'){
            attr = form.attr('class');
            var selector = 'form.' + attr;
         } else {
            var selector = 'form#' + attr;
         }
        var submitButton = form.find('button[type="submit"]');

        if(submitButton.length > 0)
            submitButton.addClass('loading');

         // submit form
         $.ajax({
              url: form.attr('action'),
              type: 'post',
              data: form.serialize(),
              dataType: 'json',
              // container : '#address-form-container',
         })
         .done(function(response,status){
            if (status == 'success') {
                // ожидается json у которого присутствует ключ callback.
                // В этом случае вызываем функцию(если она в наличии) с таким именем и передаем туда весь response и объект формы
                // Если ключа callback не оказалось, то ищем ключ html и его содержимым
                // заменяем ВСЕ, что лежит внутри родителя. Если и такого ключа нет, то игнор
                if (typeof response != 'object')
                    try {
                        response = JSON.parse(response);
                    } catch (e) {
                        alert('Unexpected response format.');
                        console.log('В ответ на запрос к ' + form.attr('action') + ' ожидался Json');
                        return;
                    }

                if(submitButton.length > 0)
                    submitButton.removeClass('loading');

                if (response.callback && typeof window[response.callback] == 'function') {
                    window[response.callback](response, form);
                    return;
                } else if(response.html) {
                    $(form.parent()).html(response.html);
                }
            } else {
                alert('Error occurred. See console');
                console.log('error occurred while sending ajax request to ' + form.attr('action') + '. Response status is ' + status);
            }
         })
         .fail(function(a,b,c){
            console.log(a); console.log(b); console.log(c);
         })
         return false;
    })
    .end()
    .on('pjax:end','[data-pjax-container] form',function(){ //любая форма внутри контейнера pjax
        // здесь можно что-то сделать сразу после того, как обновленная форма заняла свое место на странице
        console.log('address form pjaxEnd!!!');
    });
}
//END //ajax submit forms

//toggle enable field with ajax
//callback can be string of js code or function object to run
function enabledClicked(event, name, field, callback){

    if (typeof(event) == 'undefined')
        event = window.event;

    var field = field ? field : 'enabled';
    var me = $(event.target);
    var tr = me.closest('tr');
    var id = tr.data('key');
    var value = me.prop('checked') ? 1 : 0;

    if ( !requestAllowed ) {
        me.prop('checked', !value);
        return false;
    }

    requestAllowed = false;
    me.closest('div').addClass('loading');
    $.ajax({
        url : '/site/ajax-enabled',
        type  : 'post',
        dataType : 'json',
        data : {
            id : id,
            value : me.prop('checked'),
            name : name,
            field : field,
        },
    })
    .done(function(response){
        if (response.success) {
        	me.prop('checked', response.checked);
        } else {
        	me.prop('checked', !value);
        }
        requestAllowed = true;
        me.closest('div').removeClass('loading');

        switch(typeof callback) {
            case 'function':
                callback();
                break;
            case 'string':
                try {
                    eval(callback);
                } catch(e) {
                    console.log(e);
                }
                break;
            default:
        }
    })
    .fail(function( a,b,c ){
        requestAllowed = true;
        alert ('Error. See console...')
        console.log (a);
        console.log (b);
        console.log (c);
        me.closest('div').removeClass('loading');
    });
}
//END //toggle enable field with ajax

//create full address string from parts just in time filling address text inputs one after one
function initBuildAddress(){
    $('body').on('change', addressPartsSelector, function(event){
        if (typeof event == 'undefined')
            event = window.event;

        var fullAddress = '';
        var formSelector = '#' + $(event.target).closest('form').attr('id') +  ' ';

        for (k in addressParts) {
            if (addressParts.hasOwnProperty(k)) {
                var elem = $(formSelector + addressParts[k].selector);
                if (elem.length > 0 && elem.val() != '') {
                    if (k != 'region')
                        fullAddress += addressParts[k].label + '' + elem.val() + ', ';
                    else
                        fullAddress += elem.val() + addressParts[k].label +  ', ';
                }
            }
        }
        $( formSelector + '#nxaddress-address').val(fullAddress);
    });
}
var addressParts = {
    country: {
        selector : '#country_name',
        label : '',
    },
    area: {
        selector : '#area-name',
        label : '',
    },
    region: {
        selector : '#nxaddress-region',
        label : ' район',
    },
    city: {
        selector : '#nxaddress-city',
        label : '',
    },
    zipCode: {
        selector : '#nxaddress-zipcode',
        label : '',
    },
    postBox: {
        selector : '#nxaddress-post_box',
        label : 'а/я',
    },
    street: {
        selector : '#nxaddress-street',
        label : 'ул.',
    },
    house: {
        selector : '#nxaddress-house',
        label : 'д.',
    },
    building: {
        selector : '#nxaddress-building',
        label : 'корп.',
    },
    structure: {
        selector : '#nxaddress-structure',
        label : 'строение ',
    },
    appart: {
        selector : '#nxaddress-apartment',
        label : 'кв.',
    },
};

var addressPartsSelector = '';
var addressFormsIds = [];
var aForms = $('.address-form');
for (k = 0 ; k < aForms.length; k++) {
    addressFormsIds.push( $(aForms[k]).attr('id') );
}
var n = 1;
for (i in  addressFormsIds) {
    if (addressFormsIds.hasOwnProperty(i)) {
        var formId = addressFormsIds[i];
        for(k in addressParts) {
            if (addressParts.hasOwnProperty(k)) {
                var comma = (n == 1) ? '' : ', '
                addressPartsSelector +=  comma +  '#' + formId + ' ' + addressParts[k].selector;
            }
            n++;
        }
    }
}
// END create full address

//slideUp(fadeIn) or slideDown(fadeOut) element
function toggleElementDisplay(selector, how, duration, event) {
    event = event || window.event;
    event.preventDefault();

    var elem = $(selector);
    if (elem.length == 0)
        return false;

    how = (how == 'fade' || how == 'slide') ? how : 'slide';
    duration = duration || 600;

    if (elem.css('display') == 'none') {
        how = how == 'fade' ? 'fadeIn' : 'slideDown';
    } else {
        how = how == 'fade' ? 'fadeOut' : 'slideUp';
    }
    eval('elem.' + how + '(' + duration + ')');
    return false;
}

// ========================= Адреса
function afterAddressSubmitted(response, form) {
    //выполняется после того как пришел ответ с сервера после отправки формы с адресом
    var relatedElementsToUpdate = {
        1 : {
                selector : '#person-address-name',
                attrToChange : 'value',
                attrNameForValue : 'address',
            },
        2 : {
                selector : '#person-address-id',
                attrToChange : 'value',
                attrNameForValue : 'id',
            },
        3 : {
                selector : '.company-update #company-address-name',
                attrToChange : 'value',
                attrNameForValue : 'address',
        },
        4 : {
                selector : '.company-update #company-address-id',
                attrToChange : 'value',
                attrNameForValue : 'id',
        },
    };

    if(response.html) {
        $(form.parent()).html(response.html);
    }

    if (!response.model.errors || $.isEmptyObject(response.model.errors))
        for(var k in relatedElementsToUpdate) {
            if (relatedElementsToUpdate.hasOwnProperty(k)) {
                var n = relatedElementsToUpdate[k];
                var elem = $(n.selector);
                var data = response.model.attributes[n.attrNameForValue] ? response.model.attributes[n.attrNameForValue] : false ;
                if (elem.length > 0 && data) {
                    elem.prop(n.attrToChange, data).change();
                }
            }
        }
}
// ==================== END Адреса=======

//====================физ Лица
var personParts = {
    f: {
        selector : '#nxperson-f',
        label : '',
    },
    i: {
        selector : '#nxperson-i',
        label : '',
    },
    o: {
        selector : '#nxperson-o',
        label : '',
    },
};

var personPartsSelector = '';
for(k in personParts) {
    if (personParts.hasOwnProperty(k)) {
        var comma = (k == 'f') ? '' : ', '
        personPartsSelector +=  comma +  '#person-form ' + personParts[k].selector;
    }
}
function initBuildPerson(){
    $('body').on('change', personPartsSelector, function(event){
        var fullName = '';
        for (k in personParts) {
            if (personParts.hasOwnProperty(k)) {
                var elem = $('#person-form ' + personParts[k].selector);
                if (elem.length > 0 && elem.val() != '') {
                    fullName += elem.val() + personParts[k].label +  ' ';
                }
            }
        }
        $('#person-form #nxperson-name').val(fullName);
    });
}
function afterPersonSubmitted(response, form) {
    //выполняется после того как пришел ответ с сервера после отправки формы с физ.лицом
    var relatedElementsToUpdate = {
        1 : {
                selector : '#publisher-person-name',
                attrToChange : 'value',
                attrNameForValue : 'name',
            },
        2 : {
                selector : '#publisher-person-id',
                attrToChange : 'value',
                attrNameForValue : 'id',
            },
    };

    if(response.html) {
        $(form.closest('#person-form-wrapper')).html(response.html);
    }

    if (!response.model.errors || $.isEmptyObject(response.model.errors))
        for(var k in relatedElementsToUpdate) {
            if (relatedElementsToUpdate.hasOwnProperty(k)) {
                var n = relatedElementsToUpdate[k];
                var elem = $(n.selector);
                var data = response.model.attributes[n.attrNameForValue] ? response.model.attributes[n.attrNameForValue] : false ;
                if (elem.length > 0 && data) {
                    elem.prop(n.attrToChange, data).change();
                }
            }
        }

}
// =================== END Физ.лица
//==================Компании
function afterCompanySubmitted(response, form) {
    //выполняется после того как пришел ответ с сервера после отправки формы с физ.лицом
    var relatedElementsToUpdate = {
        1 : {
                selector : '#publisher-company-name',
                attrToChange : 'value',
                attrNameForValue : 'name',
            },
        2 : {
                selector : '#publisher-company-id',
                attrToChange : 'value',
                attrNameForValue : 'id',
            },
    };

    if(response.html) {
        $(form.closest('#company-form-wrapper')).parent().html(response.html);
    }

    if (!response.model.errors || $.isEmptyObject(response.model.errors))
        for(var k in relatedElementsToUpdate) {
            if (relatedElementsToUpdate.hasOwnProperty(k)) {
                var n = relatedElementsToUpdate[k];
                var elem = $(n.selector);
                var data = response.model.attributes[n.attrNameForValue] ? response.model.attributes[n.attrNameForValue] : false ;
                if (elem.length > 0 && data) {
                    elem.prop(n.attrToChange, data).change();
                }
            }
        }

}
//==== END Компании

