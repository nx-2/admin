<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use common\models\Publisher;
use backend\models\PublisherSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;
use yii\filters\AccessControl;
/**
 * PublisherController implements the CRUD actions for Publisher model.
 */
class PublisherController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                        parent::behaviors()['access'],
                        [
                            // 'class' => AccessControl::className(),
                            'rules' => [
                                //залогиненым издателям разрешить кое что только
                                [
                                    'actions' => ['update'],
                                    'allow' => true,
//                                    'roles' => ['@'],
                                    'roles' => ['publisher'],
//                                    'matchCallback' => function ($rule, $action) {
//                                        $allowedRoles = ['publisher'];
//                                        $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                        return (boolean)$allowed;
//                                    },
                                ],
                            ],
                        ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Publisher models.
     * @return mixed
     */
    // public function actionList()
    // {
    //     $searchModel = new PublisherSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('publisher', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //         'action' => 'list',
    //     ]);
    // }

    /**
     * Displays a single Publisher model.
     * @param integer $id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     $model = $this->findModel($id);

    //     if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && $model->getMyUser(Yii::$app->user->identity->User_ID)->one() == null)
    //         throw new \yii\web\ForbiddenHttpException('Извините, но вам сюда нельзя!'); //не админ и не владелец

    //     return $this->render('view', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Creates a new Publisher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Publisher();

    //     $action = 'create';
    //     if ($model->load(Yii::$app->request->post()) && $model->uploadImage()) {
    //         if ($model->save(false)) {
    //             Yii::$app->session->addFlash('success', 'Успешно сохранено.');
    //             $action = 'update';
    //         } else {
    //             Yii::$app->session->addFlash('danger', 'Не сохранено!');
    //             $action = 'create';
    //         }
    //         //            return $this->redirect(['view', 'id' => $model->id]);
    //     }
    //     if ($model->isNewRecord)
    //         $model->enabled = 1;
        
    //     return $this->render('publisher', [
    //         'model' => $model,
    //         'action' => $action,
    //     ]);

    // }

    /**
     * Updates an existing Publisher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        
        if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && $model->getMyUser(Yii::$app->user->identity->User_ID)->one() == null)
            throw new \yii\web\ForbiddenHttpException('Извините, но вам сюда нельзя!'); //не админ и не владелец
         
        if ( $model->load(Yii::$app->request->post()) && $model->uploadImage()) {

            if ($delCurrentImage = (Boolean) Yii::$app->request->post('delete-file', false) ) {
                $model->deleteOldImagesFiles();
                $model->logo = null;
            }

            if ($model->save(false))
                Yii::$app->session->addFlash('success', 'Успешно сохранено.');
            else
                Yii::$app->session->addFlash('danger', 'Не сохранено!');
        }

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        // }

        return $this->render('publisher', [
            'model' => $model,
            'action' => 'update',
        ]);
    }

    /**
     * Deletes an existing Publisher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $model = $this->findModel($id);

    //     $connection = \Yii::$app->db;
    //     $transaction = $connection->beginTransaction();

    //     try {
    //       $model->delete();
    //       $transaction->commit();
    //       return $this->redirect(['user/view', 'id' => $model->id]);

    //     }catch (yii\Db\IntegrityException $e) {

    //        $transaction->rollBack();
    //        throw new \yii\web\HttpException(500, "Издатель " . $model->name . ' не может быть удален из-за внешних ключей в других таблицах.', 405);

    //     }catch (\Exception $e) {

    //       $transaction->rollBack();
    //       throw new \yii\web\HttpException(500,"Удаление не удалось", 405);
    //     }
    //     $this->actionList();
    //     // return $this->redirect(['list']);
    // }

    /**
     * Finds the Publisher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Publisher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publisher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
