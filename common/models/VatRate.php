<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "vat_rate".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $yandex_kassa_id
 * @property int $percentage
 *
 * @property Publisher[] $publishers
 * @property Publisher[] $publishers0
 */
class VatRate extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vat_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['yandex_kassa_id','percentage'], 'integer'],
            [['name', 'description'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'name'              => 'Name',
            'description'       => 'Description',
            'yandex_kassa_id'   => 'Yandex Kassa ID',
            'percentage'        => 'Ставка в %',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['paper_vat_rate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers0()
    {
        return $this->hasMany(Publisher::className(), ['pdf_vat_rate_id' => 'id']);
    }
}
