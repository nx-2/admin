    //matches polifil
    if (!Element.prototype.matches) {
        Element.prototype.matches = 
            Element.prototype.matchesSelector || 
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector || 
            Element.prototype.oMatchesSelector || 
            Element.prototype.webkitMatchesSelector ||
            function(s) {
                var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                    i = matches.length;
                while (--i >= 0 && matches.item(i) !== this) {}
                return i > -1;            
            };
    }


    //closest polyfill
    (function(e){ 
     e.closest = e.closest || function(css){ 
       var node = this;

       while (node) { 
          if (node.matches(css)) return node; 
          else node = node.parentElement; 
       } 
       return null; 
     } 
    })(Element.prototype);

   $(document).ready(function(){
        aceCodeEditors = Array();
       
        var codeElements = document.querySelectorAll('.ace-code-editor');
        for ( var k = 0; k < codeElements.length; k++ ) {
            codeElements[k].closest('.ace-code-editor-wrapper').style.height = '450px';
            codeElements[k].setAttribute('data-editorindex', k);
            var codeEditor = ace.edit(codeElements[k]);
            codeEditor.setTheme("ace/theme/monokai");
            codeEditor.setDisplayIndentGuides(true);
            codeEditor.setPrintMarginColumn(false);
            codeEditor.getSession().setMode("ace/mode/php");
            aceCodeEditors.push(codeEditor);
        }
   });
    function getAceCodeEditor( elemSelector ) {
        var tplBodyElem = document.querySelector( elemSelector );
        for( var k = 0; k < aceCodeEditors.length; k++ ) {
            if ( aceCodeEditors[k].container == tplBodyElem  ) {
                return aceCodeEditors[k];
            }
        }
        return false;
    }
   function aceCodeEditorOptionChange( event ) {
      if ( typeof event == 'undefined' ) 
          event = window.event;
      var index = event.target.closest('.ace-code-editor-wrapper').querySelector('.ace-code-editor').getAttribute('data-editorindex');
      
      var editor = aceCodeEditors[index];
      var changedElem = event.target;
      
      var aceOptions = Array('ace-editor-show-indent-lines','ace-editor-font-size','ace-editor-full-size', 'ace-editor-color-scheme');
      for (var k = 0; k < aceOptions.length; k++) {
          if ( changedElem.classList.contains( aceOptions[k] ) ) {
              switch (aceOptions[k]) {
                case 'ace-editor-show-indent-lines':
                    editor.setDisplayIndentGuides(changedElem.checked);
                    break;
                case 'ace-editor-font-size':
                    editor.setFontSize( parseInt(changedElem.value) );
                    break;
                case 'ace-editor-full-size':
                    if (changedElem.checked) {
                        changedElem.closest('.ace-code-editor-wrapper').classList.add('full-screen');
                    } else {
                        changedElem.closest('.ace-code-editor-wrapper').classList.remove('full-screen');
                    }
                    editor.resize();
                    break;
                case 'ace-editor-color-scheme':
                    editor.setTheme(changedElem.value);
                    break;
              }
               break;
          } 
      }
     
   }


