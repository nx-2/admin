<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_delivery_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enabled
 *
 * @property NxOrderItem[] $nxOrderItems
 * @property NxPriceItem[] $nxPriceItems
 * @property NxZoneDelivery[] $nxZoneDeliveries
 * @property NxZone[] $zones
 */
class DeliveryType extends \common\models\CommonModel
{
    const PDF_TYPE = 5;
    const BY_YOUR_SELF = 2;
    const POST_SERVICE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_delivery_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'enabled'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'enabled' => 'Активен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrderItems()
    {
        return $this->hasMany(NxOrderItem::className(), ['delivery_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPriceItems()
    {
        return $this->hasMany(NxPriceItem::className(), ['delivery_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxZoneDeliveries()
    {
        return $this->hasMany(NxZoneDelivery::className(), ['delivery_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZones()
    {
        return $this->hasMany(NxZone::className(), ['id' => 'zone_id'])->viaTable('nx_zone_delivery', ['delivery_type_id' => 'id']);
    }
}
