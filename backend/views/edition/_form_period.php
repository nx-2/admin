<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\EditionPeriod */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-7 col-sm-7 col-xs-12">
    <div class="edition-period-form">
        <br>
        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($model, 'period')->textInput() ?>

        <?php

        echo $form->field($model, 'default')->dropDownList([
            '1' => 'Да',
            '0' => 'Нет'
        ]);
        ?>

        <?= $form->field($model, 'edition_id')->hiddenInput(['value' => $modelEdition->Message_ID])->label(false); ?>


        <div class="form-group">   <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
<br>
<div class="col-md-5 col-sm-5 col-xs-12">
    <?php if (!empty($modelEdition->editionPeriods)): ?>

        <?php $query = \common\models\EditionPeriod::find()->where(['=', 'edition_id', $modelEdition->Message_ID]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' =>false

        ]); ?>


        <?php Pjax::begin(['id' => 'publishers-list', 'enablePushState' => false]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'job-gridview',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'period',
                ['attribute' => 'default',
                    'value' => function ($model, $key, $index) {
                        return ($model->default)?'Да':'Нет';
                    },
                    'format' => 'raw'],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'vertical-align: middle;'],
                    // 'template' => '{update}',
                    'template' => '{delete}',
                    // 'controller' => '',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a(
                                '',
                                Url::to(['edition/delete-items', 'id' => $key]),
                                [
                                    'title' => 'Удалить период',
                                    'aria-label' => 'Удалить',
                                    'data-confirm' => 'Вы уверены, что хотите удалить период?',
                                    'data-method' => 'post',
                                    'data-pjax' => '#publishers-list',
                                    'class' => 'fa fa-trash fa-font-18',
                                ]
                            );
                        },
                    ],
                ],


            ],
        ]); ?>
        <?php Pjax::end(); ?>
    <?php endif; ?>
</div>