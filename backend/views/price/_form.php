<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\views\widgets\create_update_fields\CreateUpdateFields;
use kartik\date\DatePicker;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\NxPrice */
/* @var $form yii\widgets\ActiveForm */
?>
<div id='price-form-wrapper'>
    <?php $form = ActiveForm::begin(); ?>
    <?php $form->errorSummaryCssClass = 'alert alert-warning alert-dismissible'; ?>
    <div class='box box-primary issue-form'>
        <div class='box-header with-border'>
            <?php echo('Прайс: ' . $model->name . '('. date('d-m-Y', strtotime($model->date)) ) . ')'; ?>
        </div>
        <div class='box-body'>
            <div>
                <?=$form->errorSummary($model,['header' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>']); ?>
            </div>                
            <div class="box">
                <div class='box-body no-padding'>
                    <div class="row" style='padding-top:10px;'>
                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>
                            <?php 
                                echo(Html::activeHiddenInput($model, 'publisher_id',['id' => 'nxprice-publisher_id', 'value' => $model->isNewRecord ? yii::$app->user->identity->publisher->id : $model->publisher_id]));
                            ?>
                            <?php //echo($form->field($model, 'publisher_id')->textInput()); ?>

                            <?php // echo($form->field($model, 'item_type')->textInput());?>

                            <?= $form->field($model, 'customer_type_id')->dropDownList([1=>'физ.лица',2=>'юр.лица'], []) ?>

                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                            <?php 
                                echo $form->field($model, 'date')->widget(DatePicker::classname(), [
                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                    'layout' => '{picker}{input}{remove}',
                                    // 'convertFormat' => true,
                                    'options' => [
                                        'placeholder' => 'Укажите дату начала действия ...',
                                        'value' => $model->date !== null ?  date('d.m.Y',strtotime($model->date)) : '',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true
                                    ]
                                ]);
                            ?>

                            <?php 
                                if ($model->isNewRecord)
                                    $model->checked = 1;
                                echo($form->field($model, 'checked')->checkbox([])); 
                            ?>
                        </div>
                        <div class='col-md-6 col-sm-6 col-xs-12' style='padding:0px 25px'>

                            <?= CreateUpdateFields::widget(['model' => $model]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('_items_table', [
                                'model' => $model,
                                'form' => $form,
                                'priceItems' => $priceItems,
                            ]) ?>
                        </div>
                    </div>
                </div><!--END  bos-body -->
            </div> <!--END box -->
        </div>
        <div class='box-footer'>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style='padding:0px 25px'>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
                <?= Html::button('Отменить', ['class' => 'btn btn-primary', 'style' => 'margin-left: 20px', 'onclick' => 'history.back()']) ?>
                <?php if(!$model->isNewRecord): ?>
                    <?=Html::a(
                            'Удалить прайс',
                            Url::to(['price/delete','id' => $model->id]),
                            [
                                'class' => 'btn btn-danger pull-right',
                                'onclick' => 'confirmDelete()',
                                'title' => 'Будет удален весь прайс. Удаление возможно только в случае если не существует заказов на основе этого прайса'
                            ]); ?>
                    <script type="text/javascript">
                        function confirmDelete(event){
                            if (typeof(event) == 'undefined') 
                                event = window.event;

                            if (confirm('Вы действительно желаете удалить этот прайс?')) {
                                return true;
                            } else {
                                event.preventDefault();
                                return false;
                            }
                        }
                    </script>
                <?php endif; ?>
            </div>
        </div><!--End bos-footer  -->
    </div> <!--END box -->
    <?php ActiveForm::end(); ?>
</div><!--End #person-form-wrapper -->
