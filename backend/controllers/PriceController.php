<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use Yii;
use common\models\NxPrice;
use backend\models\NxPriceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;

/**
 * PriceController implements the CRUD actions for NxPrice model.
 */
class PriceController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => yii\helpers\ArrayHelper::merge(
                        parent::behaviors()['access'],
                        [
                            // 'class' => AccessControl::className(),
                            'rules' => [
                                //залогиненым издателям разрешить кое что только
                                [
                                    'actions' => ['index','update','create','delete','clone'],
                                    'allow' => true,
//                                    'roles' => ['@'],
                                    'roles' => ['subscribe_manager'],
//                                    'matchCallback' => function ($rule, $action) {
//                                        $allowedRoles = ['publisher','subscribe_manager', 'admin'];
//                                        $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//                                        return (boolean)$allowed;
//                                    },

                                ],
                            ],
                        ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                //     'delete' => ['POST'],
                // ],
            ],
        ];
    }
   
    /**
     * Lists all NxPrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NxPriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NxPrice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NxPrice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NxPrice();
        $model->publisher_id = Yii::$app->user->identity->publisher_id;
        $priceItems = $model->nxPriceItems;
        $unfilledEditions = $model->unfilledEditions(); // список ID изданий, по которым нет цен в этом прайсе
        if (!empty($unfilledEditions)) {
            // сформируем все варианты с нулевыми ценами
            $allDeliveryWays = \common\models\NxPriceItem::getAllDeliveryPositions();
            foreach($unfilledEditions as $edition_id) {
                foreach($allDeliveryWays as $dw) {
                    $emptyItem = new \common\models\NxPriceItem();
                    $emptyItem->setScenario('create');
                    $emptyItem->item_id = $edition_id;
                    $emptyItem->price_id = $model->id;
                    $emptyItem->zone_id = $dw['zone_id'];
                    $emptyItem->delivery_type_id = $dw['delivery_type_id'];
                    $emptyItem->price = 0;
                    $priceItems [] = $emptyItem;
                }
            }
        }
        $viewName = 'create';
        if ($model->load(Yii::$app->request->post()) && $model->save() && \yii\base\Model::loadMultiple($priceItems, Yii::$app->request->post())) {
            //прайс загружен, проверен, сохранен. А цены только загружены. Еще не проверены и не сохранены
            Yii::$app->session->addFlash('success', 'Новый прайс ' . $model->name . ' успешно сохранен.');
            
            foreach($priceItems as $pi) {
                $pi->price_id = $model->id;
            }
            if (\yii\base\Model::validateMultiple($priceItems)) {
                foreach ($priceItems as $pi) {
                        $pi->save(false);
                }
                Yii::$app->session->addFlash('success', 'Все цены прайса ' . $model->name . ' успешно сохранены.');
                $viewName = 'update';
            } else {
                Yii::$app->session->addFlash('danger', 'Цены прайса ' . $model->name . ' НЕ сохранены!');
            }
        } else {
            //что-то пошло не так с прайсом и загрузкой цен
            if(Yii::$app->request->isPost)
                Yii::$app->session->addFlash('danger', 'Новый прайс НЕ сохранен.Что-то пошло не так..');
        }
        return $this->render($viewName, [
            'model' => $model,
            'priceItems' => $priceItems,
        ]);
    }

    /**
     * Updates an existing NxPrice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $priceItems = $model->nxPriceItems;
                
        // В каждом прайсе обязательно должны присутствовать цены для всех изданий издателя
        // Убедимся в этом
        $unfilledEditions = $model->unfilledEditions(); // список ID изданий, по которым нет цен в этом прайсе
        if (!empty($unfilledEditions)) {
            // сформируем все варианты с нулевыми ценами
            $allDeliveryWays = \common\models\NxPriceItem::getAllDeliveryPositions();
            foreach($unfilledEditions as $edition_id) {
                foreach($allDeliveryWays as $dw) {
                    $emptyItem = new \common\models\NxPriceItem();
                    $emptyItem->item_id = $edition_id;
                    $emptyItem->price_id = $model->id;
                    $emptyItem->zone_id = $dw['zone_id'];
                    $emptyItem->delivery_type_id = $dw['delivery_type_id'];
                    $emptyItem->price = 0;
                    $priceItems [] = $emptyItem;
                }
            }
        }
        if (
                $model->load(Yii::$app->request->post()) 
                && \yii\base\Model::loadMultiple($priceItems, Yii::$app->request->post())
                && $model->validate() 
                && \yii\base\Model::validateMultiple($priceItems)
            ) {
                $model->save(false);
                foreach($priceItems as $pi) {
                    $pi->save(false);
                }
                Yii::$app->session->addFlash('success', 'Прайс <' . $model->name . '> успешно сохранен.');
        } else {
            foreach($priceItems as $pi){
                if ($pi->hasErrors()) {
//                    var_dump($pi->getErrors());
//                    echo($pi->id . '<br>');
                }
            }
            if (Yii::$app->request->isPost)
                Yii::$app->session->addFlash('danger', 'Прайс <' . $model->name . '> НЕ сохранен!');
        }
        return $this->render('update', [
            'model' => $model,
            'priceItems' => $priceItems,
//            'itemsTableData' => $this->getItemsTableData($id),
        ]);
    }
    private function getItemsTableData($priceId = false)
    {
        if (!$priceId)
            return [];
        
        $tableData = [];
        $tableData ['head'] = \common\models\NxPriceItem::getAllDeliveryPositions();
        $tableData ['body'] = \common\models\NxPriceItem::getPriceItemsData($priceId);

        return $tableData;
    }
    /**
     * Deletes an existing NxPrice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->addFlash('success', 'Прайс ' . $id . ' Удален');
        } catch (\Exception $e) {
            Yii::$app->session->addFlash('danger', 'Прайс "' . $id . '" НЕ был удален.');
            if (intval($e->getCode()) === 23000) {
                Yii::$app->session->addFlash('danger', 'Вероятно существуют заказы на основе этого прайса');
            } else {
                Yii::$app->session->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(['index']);
    }
    /**
     * Clone existing price. Create new as copy of existing.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionClone($id)
    {
        $toBeClonedModel = $this->findModel($id);
        $newPriceId = $toBeClonedModel->clonePrice();
        
        if (!$newPriceId) {
            throw new yii\web\ServerErrorHttpException('Something went wrong. Price ' . $id . ' could not cloned. Sorry..');
        } else {
            $this->redirect(['price/update', 'id' => $newPriceId]);
        }
    }
    /**
     * Finds the NxPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NxPrice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NxPrice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
