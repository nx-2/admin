<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->params['defaultTitle'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class='col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12'>
            <div class='box box-primary' style='padding: 20px;box-shadow: 5px 5px 20px rgba(0,0,0,0.3);'>
                <div class="box-header with-border">
                    <img class="login-form-logo" src="/images/dimeo_logo.png">
                    <h1 class="box-title"><?= Html::encode($title) ?></h1>
<!--                    <p>Заполните пожалуйста все поля:</p>-->
                </div>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <?php if ($model->hasErrors() && $e = $model->getErrors('User_ID')): ?>
                        <div class='has-error'>
                            <p class='help-block help-block-error'>
                                <?php foreach($e as $err): ?>
                                    <?php print_r($err); ?>
                                <?php endforeach; ?>
                            </p>
                        </div> 
                    <?php endif; ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control','placeholder' => 'Логин']); ?>
                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Пароль']); ?>
                    <?php // echo($form->field($model, 'rememberMe')->checkbox()); ?>
                    <div class="form-group">
                        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

                        <?php
                            echo(Html::a('Восстановить пароль','/site/password-is-lost',['class' => 'pull-right']));
                        ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = center;
    function center()
    {
        var f = $('.site-login');
        var h = $(window).height();
        f.css('margin-top', (h - f.height()) / 2 + 'px');
    }
    // $(document).ready(function(){
    // })
</script>
