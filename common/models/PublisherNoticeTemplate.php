<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "publisher_notice_template".
 *
 * @property integer $id
 * @property integer $publisher_id
 * @property integer $template_id
 * @property integer $notice_id
 *
 * @property EmailNoticeTemplate $template
 * @property Publisher $publisher
 */
class PublisherNoticeTemplate extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher_notice_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher_id', 'template_id','notice_id'], 'required'],
            [['publisher_id', 'template_id','notice_id'], 'integer'],
            [['publisher_id', 'template_id'], 'unique', 'targetAttribute' => ['publisher_id', 'template_id','notice_id'], 'message' => 'The combination of Publisher ID, Email Notice ID and Template ID has already been taken.'],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailNoticeTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
            [['notice_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailNotice::className(), 'targetAttribute' => ['notice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher_id' => 'Publisher ID',
            'template_id' => 'Template ID',
            'notice_id' => 'Notice ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(EmailNoticeTemplate::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotice()
    {
        return $this->hasOne(EmailNotice::className(), ['id' => 'notice_id']);
    }
}
