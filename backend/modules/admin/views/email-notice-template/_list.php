<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div>
    <div id='email-template-modal-wrapper'>
        
    </div>
    <p>
        <?= Html::a('Создать новый', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'email-notices-template-list', 'enablePushState' => false]); ?> 
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
    //                ['class' => 'yii\grid\SerialColumn'],
    //
    //                'id',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'vertical-align: middle;'],
                        // 'template' => '{update}',
                        'template' => '{update}',
                        // 'controller' => '',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                $url = Url::to(['email-notice-template/update','id' => $key]);
                                return Html::a(
                                    '',
                                    $url,
                                    [
                                        'title' => 'редактировать',
                                        'aria-label' => 'редактировать',
                                        'data-pjax' => 0,
                                        'class' => 'fa fa-gear fa-font-18',

                                    ]
                                );
                            },
                        ],
                    ],
//                    'notice_id',
                    [
                        'attribute' => 'notice_id',
                        'value' => 'notice.name',
                        'filter' => \yii\helpers\ArrayHelper::map(common\models\EmailNotice::find()->all() , 'id', 'name'),
//                        'filterOptions' => ['style' => 'padding-left:0px;']
                    ], 
                                    
//                    'template_file',
                    [
                        'attribute' => 'template_file',
                        'content' => function( $model, $key, $index, $column ) {
                                $showLink = Html::a('Посмотреть', '#showTemplate', ['data-id' => $key,  'onclick' => 'showTemplateContent(event)']);
                                $contentSpan = Html::tag('span',$model->template_file , ['style' => 'margin-right: 7px']);
                                return $contentSpan . $showLink;
                        },
                    ],
                    'name',
    //                'data_fields',
//                    'is_default',

                    [
                        'class' => 'yii\grid\DataColumn',
                        'attribute' => 'is_default',
                        'content' => function ($model, $key, $index, $column) {
                            $checkbox = Html::checkbox('', (Boolean)$model->is_default, [
                                'id' => 'isdefault_' . $model->id,
                                'value' => $model->is_default,
                                'disabled' => true,
                                //'onclick' => 'enabledClicked(event, "Publisher")',
                            ]);
                            $label = Html::label('', 'isdefault_' . $model->id, ['class' => 'radio-check-label'], '');
                            //$ajaxLoader = Html::tag('div', '', ['class' => 'ajax-load']);
                            $content = $checkbox . $label;
                            $result = Html::tag('div', $content, ['class' => 'radio-check-wrap']);

                            return $result;
                        },
                        'contentOptions' => ['style' => 'vertical-align: middle;'],
                        'filter' => ['1' => 'Да', '0' => 'Нет'],
                        'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
                    ],
                                
                                
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'vertical-align: middle;'],
                        // 'template' => '{update}',
                        'template' => '{delete}',
                        // 'controller' => '',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a(
                                    '' ,
                                    Url::to(['email-notice-template/delete','id' => $key]),
                                    [
                                        'title' => 'Удалить шаблон',
                                        'aria-label' => 'Удалить',
                                        'data-confirm'=>'Вы уверены, что хотите удалить этот шаблон?',
                                        'data-method'=> 'post',
                                        'data-pjax' => '#email-notices-template-list',
                                        'class' => 'fa fa-trash fa-font-18',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
    <?php Pjax::end(); ?>
</div>
<?php 
    Yii::$app->request->getIsSecureConnection();
?>
<script type="text/javascript">
    function showTemplateContent(event){
        if ( typeof event == 'undefined' ) {
            event = window.event;
        }
        
        event.preventDefault();
        $.ajax({
            url : '<?php echo( Url::to(['email-notice-template/get-template-html'], Yii::$app->request->getIsSecureConnection() ) ); ?>' + '/' + $(event.target).data('id'),
            type: 'get',
            dataType : 'json',
        }).done(function(response){
            if (response.status == 'OK') {
                $('#email-template-modal-wrapper').html(response.html);
                var modal = $('#email-template-modal-wrapper #email-notice-template-modal');
                if (modal.length > 0) {
                    modal.on('shown.bs.modal', function (event) {
                        setIframeSizes();
                    })
                    modal.modal('show');
                }
            } else {
                if ( response.hasOwnProperty('errMessage') ) {
                    alert( response.errMessage );
                }
            }
        })
        .fail(function(a,b,c){
            alert('что-то пошло не так. См. консоль');
            console,log(a);
            console,log(b);
            console,log(c);
        });
        return false;
    }
    function setIframeSizes( w, h ){
        w = !w ? 700 : w;
        h = !h ? false : h;
        
        var iframe = $('#popupTemplateIframe');
        iframe.width(w + 'px');
        if ( h == false ) {
            var iframeBody = iframe.contents().find('body');
            h = iframeBody.height() + 20;
        }
        iframe.height(h + 'px');
    }
</script>
    
