<?php

namespace frontend\modules\guarded\model;

use common\models\Publisher;
use common\models\PublisherPropertyGroup;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Yii;

class User extends \common\models\User implements \yii\web\IdentityInterface
{

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        return self::findOne(['User_ID' => $id]);
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        $token = self::createTestJwt(6);

        $tokenPayload = self::verifyToken($token);
        if (empty($tokenPayload)) {
            return NULL;
        }
        $uId = $tokenPayload['uId'] ?? NULL;
        if (empty($uId)) {
            return NULL;
        }
        return self::findOne(['User_ID' => intval($uId)]);
    }

    /**
     * @return array | NULL
     * @param string $token
     */
    private static function verifyToken(string $token)
    {
        $parts = explode('.', $token);
        $publicPayload = JWT::urlsafeB64Decode($parts[1] ?? '');
        $publicPayload = json_decode($publicPayload, true);
        if (empty($publicPayload)) {
            return NULL;
        }
        $publisherId = intval($publicPayload['pId'] ?? 0);
        $secretString = Publisher::getJwtSecretString($publisherId);

        if (empty($secretString)) {
            return NULL;
        }

        $thisModule = Yii::$app->getModule('guarded');
//        $secretString = $thisModule->params['jwtSecretString'];
        $jwtAlg = $thisModule->params['jwtAlg'];
        $key = new Key($secretString, $jwtAlg);

        try {
            $tokenPayload = JWT::decode($token, $key);
            return \yii\helpers\ArrayHelper::toArray($tokenPayload);
        } catch (\Exception $e) {
            return NULL;
        }
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return intval($this->User_ID);
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public static function createTestJwt(int $userId):string
    {
        $user = self::findOne(['User_ID' => $userId]);

        if (empty($user)) {
            return '';
        }

        $jwtPayload = [
            'uId' => $user->User_ID,
            'uLogin' => $user->Login,
            'uRoles' => $user->rolesIds,
            'pId' => $user->publisher->id,
            'iat' => time()
        ];

        $thisModule = Yii::$app->getModule('guarded');
//        $secret = $thisModule->params['jwtSecretString'];
        $secret = Publisher::getJwtSecretString($user->publisher->id);
        $jwtAlg = $thisModule->params['jwtAlg'];

        $token = JWT::encode($jwtPayload, $secret, $jwtAlg);

        return $token;
    }
}