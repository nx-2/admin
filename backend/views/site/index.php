<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/* @var $this yii\web\View */

$this->title = Yii::$app->params['defaultTitle'];
?>
<div class="site-index" style='position: relative'>
    <img src="/images/default_img1.jpg" style='width: 100%;'>
    <div style="position: absolute;top: 0px;left: 0px;width: 100%;padding: 30px 50px;text-align: center;">
        <h2>
            Облачный сервис редакционной подписки
        </h2>
        <p style='font-size: 18px;line-height: 30px;text-align: left;'>
            ПО «Димео Подписка», предназначенное для издателей периодических печатных и цифровых изданий, помогает внедрять онлайн-сервисы редакционной подписки и обслуживания подписчиков. Включает «Панель издателя», позволяющую управлять общей информацией об издателе, его сотрудниках, работающих с сервисом, изданиях, их отдельных выпусках, ценах, акциях и т. д., а также создавать виджеты с информацией об издании и условиях подписки для внедрения на сайт издания, и «Панель заказов», включающую интерфейсы типовых АРМ руководителя, менеджера по подписке, логиста и бухгалтера и позволяющую управлять заказами, оплатами, отгрузками и подписчиками. Обеспечивает снижение расходов на привлечение новых заказов; прямую работу с подписчиками; режим самообслуживания для подписчиков; возможность гибкой корректировки заказа; удержание подписчиков; повышение качества доставки.
        </p>
    </div>
<!-- 

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
 -->
</div>
