<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\controllers;

use backend\models\ActionItems;
use backend\models\ActionSearch;
use backend\models\PromoCodesSearch;
use Yii;
use backend\models\Action;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\SiteController;
use \common\models\Edition;

/**
 * ActionController implements the CRUD actions for Action model.
 */
class ActionController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => yii\helpers\ArrayHelper::merge(
                parent::behaviors()['access'],
                [
                    // 'class' => AccessControl::className(),
                    'rules' => [
                        //залогиненым издателям разрешить кое что только
                        [
                            'actions' => ['update','index','create','generated','createitems','delete','delete-items','months'],
                            'allow' => true,
//                            'roles' => ['@'],
                            'roles' => ['subscribe_manager'],
//                            'matchCallback' => function ($rule, $action) {
//                                $allowedRoles = ['publisher','subscribe_manager','admin'];
//                                $allowed = Yii::$app->user->identity->isRole($allowedRoles);
//
//                                $a = Yii::$app->getUser()->can('manageActions');
//
//                                return (boolean)$allowed;
//                            },
                        ],
                    ],
                ]
            ),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
        return $behaviors;
    }


    public function actionMonths(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $journal_id = Yii::$app->request->getQueryParam('journal_id', false);

        if (!$journal_id)
            return false;

        $journal_id = intval($journal_id);
        $edition = Edition::findOne(['Message_ID' => $journal_id]);

        return yii\helpers\ArrayHelper::map($edition->editionPeriods, 'period', 'period');

//        if(!$journal=\common\models\NxPrice::find()->where(['publisher_id'=> Yii::$app->user->identity->publisher_id])->one())
//            return false;
//
//        $journal->mag_id=$journal_id;
//
//        if(!isset($journal->nxPriceItemsOne->edition))
//            return false;
//
//        $journal=$journal->nxPriceItemsOne->edition;
//
//        return  yii\helpers\ArrayHelper::map($journal->editionPeriods, 'period', 'period');
    }
    /**
     * Lists all Action models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new \backend\models\ActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Action model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Action model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new \common\models\Action();
        $model->scenario = 'create';

        $modelItems=new \common\models\ActionItems();

        $model->action_code = $model->generateRandomString();
        $model->date_start=date('d-m-Y');
        $model->date_end=date('d-m-Y');
        $model->publisher_id=Yii::$app->user->identity->publisher_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
             return $this->render('create', [
                'model' => $model,
                'modelItems' => $modelItems,
            ]);
        }
    }

    /**
     * Updates an existing Action model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $searchModel = new \backend\models\PromoCodesSearch();
        $dataProviderPromoCodes = $searchModel->search(Yii::$app->request->queryParams,$id);

        $model = $this->findModel($id);
        $modelItems=new \common\models\ActionItems();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelItems' => $modelItems,
                'searchModel' => $searchModel,
                'dataProviderPromoCodes'=>$dataProviderPromoCodes
            ]);
        }
    }


    /**
     * Deletes an existing Action model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Delete items action
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeleteItems($id)
    {
//        $this->findModel($id, '\common\models\ActionItems')->delete();
        \common\models\ActionItems::findOne([ 'id' => $id])->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * save items action
     * @return \yii\web\Response
     */
    public function actionCreateitems()
    {

        $model = new \common\models\ActionItems();
        if ($model->load(Yii::$app->request->post())) {
            if (is_array($model->type)) {
               $model->setItems($model);
            }else{
                $model->save();
            }

        }

        return $this->redirect(['update', 'id' => $model->action_id]);


    }

    /**
     * @return string
     */
    public function actionGenerated()
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $actionId = Yii::$app->request->getQueryParam('id', false);
        $count = Yii::$app->request->getQueryParam('count', 0);


        if (empty($actionId))
            return false;

        $model = new \common\models\Action();
        $string = $model->generateRandomString(6, intval($count), $actionId);
        if (is_array($string))
            $string = implode(' '.PHP_EOL, $string);

        return ['string' => $string];


    }

    /**
     * Finds the Action model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Action the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        $searchModel = new ActionSearch();
        $dataProvider = $searchModel->search(['ActionSearch'=>['id'=>$id]]);

        if (!Yii::$app->user->identity->isRole(\common\models\User::ADMIN_ROLE) && ($dataProvider->getCount() == 0))
            throw new NotFoundHttpException('Извините, но вам сюда нельзя! Недостаточно прав.');

        if (($model = \common\models\Action::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}
