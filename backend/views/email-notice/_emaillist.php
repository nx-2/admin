<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<div>
    <div id='email-message-modal-wrapper' >
        <div id='email-message-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="email-message-label">
            <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
                <div class="modal-content" style='display:inline-block;text-align:initial'>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="email-message-label">Ваш шаблон</h4>
                        <div><input type='text' placeholder='ширина...' style='width: 70px;padding: 5px;border: 1px solid rgba(0,0,0,0.1);'></div>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'email-messages-list', 'enablePushState' => true]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//                'id',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{look}',
                // 'controller' => '',
                'buttons' => [
                    'look' => function ($url, $model, $key) {
                        $url = Url::to(['email-notice/email-log','id' => $key]);
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'Посмотреть письмо',
                                'aria-label' => 'Просмотр',
                                'data-pjax' => 0,
                                'class' => 'fa fa-envelope fa-font-18',
                                'data-id' => $key,
                                'onclick' => 'loadEmail(event)',
                            ]
                        );
                    },
                ],
            ],
            [
                'attribute' => 'success',
                'content' => function ($model, $key, $index, $column) {
                    return $model->success == 1 ? 'Отправлено' : 'НЕ отправлено';
                },
            ],
            'from',
            'to',
            'subject',
            [
                'attribute' => 'time',
                'content' => function ($model, $key, $index, $column) {
                    return date('d.m.y H:i', strtotime($model->time)) ;
                }
            ],
            'type',
            'publisher_id',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<script>
    function loadEmail(event) {
        if ( !event )
            event = window.event;

        var me = $(event.target);
        var id = me.data('id');
        $.ajax({
            'url' : '/email-notice/email-log/' + id,
            'dataType' : 'json',
            'type' : 'get',
        })
        .done( function ( response ) {
            if (response.status == 'OK') {
                var modalWrapper = $('#email-message-modal-wrapper');
                modalWrapper.html(response.html);
                var modal = $('#email-message-modal-wrapper #email-message-modal');
                if (modal.length > 0) {
                    modal.on('shown.bs.modal', function (event) {
                        setIframeSizes();
                    });
                    modal.modal('show');
                }
            } else {
                alert(response.message);
            }
        })
        .fail( function( a,b,c ) {
            console.log(a,b,c);
            alert('Что-то пошло не так. См.Консоль');

        } );
        event.preventDefault();
        return false;
    }
    function setIframeSizes( w, h ){
        w = !w ? 700 : w;
        h = !h ? false : h;

        var iframe = $('#popupMessageIframe');
        iframe.width(w + 'px');
        if ( h == false ) {
            var iframeBody = iframe.contents().find('body');
            h = iframeBody.height() + 20;
        }
        iframe.height(h + 'px');
    }
    function resendImmediately(id, recipient) {
        $.ajax({
            'url' : '/email-notice/resend-logged-email/' + id + '/' + recipient,
            'dataType' : 'json',
            'type' : 'get'
        }).done(function(response){
            if (response.status == 'OK')
                alert('Отправлено.');
            else
                alert(response.message);
        })
        .fail(function(a,b,c){
            console.log(a,b,c);
            alert('что-то пошло не так. См. консоль');
        });
    }


</script>

