<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use common\models\CommonModel;
/**
 * This is the model class for table "nx_person".
 *
 * @property integer $id
 * @property string $company_id
 * @property string $position
 * @property string $position_type_id
 * @property string $speciality
 * @property string $phone
 * @property string $telcode
 * @property string $address_id
 * @property string $email
 * @property string $channel_id
 * @property string $source_id
 * @property string $name
 * @property string $f
 * @property string $i
 * @property string $o
 * @property string $gender
 * @property integer $checked
 * @property integer $enabled
 * @property string $comment
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $label
 * @property integer $xpress_id
 * @property string $xpress_type
 */
class NxPerson extends \common\models\WatchableModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'position_type_id', 'address_id', 'channel_id', 'source_id', 'checked', 'enabled', 'create_user_id', 'last_user_id', 'xpress_id'], 'integer'],
            [['gender', 'xpress_type'], 'string'],
            [['f','i','name','email', 'phone'], 'required'],
            ['email', 'email'],
            [['created', 'last_updated','xpress_id'], 'safe'],
            [['position', 'name'], 'string', 'max' => 200],
            [['speciality'], 'string', 'max' => 60],
            [['phone'], 'string', 'max' => 100],
            [['telcode'], 'string', 'max' => 15],
            [['email', 'comment', 'label'], 'string', 'max' => 255],
            [['f', 'i', 'o'], 'string', 'max' => 30],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxAddress::className(), 'targetAttribute' => ['address_id' => 'id'],'message' => 'Указанный адрес отсутствует в справочнике'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id'],'message' => 'Указанная компания отсутствует в справочнике'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>                 'ID',
            'company_id' =>         'Компания',
            'position' =>           'Должность',
            'position_type_id' =>   'Тип должности',
            'speciality' =>         'Специальность',
            'phone' =>              'Телефон',
            'telcode' =>            'Телефонный код',
            'address_id' =>         'Адрес',
            'email' =>              'Email',
            'channel_id' =>         'Channel ID',
            'source_id' =>          'Source ID',
            'name' =>               'Полное имя',
            'f' =>                  'Фамилия',
            'i' =>                  'Имя',
            'o' =>                  'Отчество',
            'gender' =>             'Пол',
            'checked' =>            'Checked',
            'enabled' =>            'Активен',
            'comment' =>            'Комментарий',
            'created' =>            'Создан',
            'last_updated' =>       'Посл. обновл.',
            'create_user_id' =>     'Создан польз.',
            'last_user_id' =>       'Обновл. польз',
            'label' =>              'Метка',
            'xpress_id' =>          'Xpress ID',
            'xpress_type' =>        'Xpress Type',
            'company'     =>        'Компания',
        ];
    }

    public function getAddress()
    {
        return $this->hasOne(NxAddress::className(), ['id' => 'address_id']);//->inverseOf('companies');
    }
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);//->inverseOf('companies');
    }
    public function getOrders()
    {
        return $this->hasMany(NxOrder::className(), ['person_id' => 'id']);//->inverseOf('companies');
    }
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['contact_person_id' => 'id']);//->inverseOf('companies');
    }
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['contact_person_id' => 'id']);//->inverseOf('companies');
    }
    public function buildFullNameString()
    {
//        if ($this->id === null)
//            return '';
        
        
        $fullName = [];
        
        if (!empty($this->f)) 
            $fullName [] = $this->f;
        
        if (!empty($this->i)) 
            $fullName [] = $this->i;
        
        if (!empty($this->o)) 
            $fullName [] = $this->o;
        
        if (!empty($this->company)) 
            $fullName [] = '(' . $this->company->name . ')';
        
        if (count($fullName) == 0)
            $fullName [] = $this->name;
        
        $fullName = implode(' ', $fullName);
        
        if (!empty($this->email)) 
//            $fullName  .= ', Email: ' . $this->email;
            $fullName  .= ', ' . $this->email;

        if (!empty($this->phone)) 
            $fullName  .= ', Тел: ' . $this->phone;
        
        return $fullName;
        
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);//->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);//->inverseOf('company');
    }


    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->created = date('Y-m-d H:i:s');
            $this->create_user_id = Yii::$app->user->isGuest ? null : Yii::$app->user->identity->User_ID;
        } else {
            $this->created = date('Y-m-d H:i:s', strtotime($this->created));
        }

//        if ( $this->created == '0000-00-00 00:00:00' )
//            $this->created = '2000-01-01 00:00:00';


        $this->last_user_id = Yii::$app->user->isGuest ? null : Yii::$app->user->identity->User_ID;
        $this->last_updated = date('Y-m-d H:i:s');
        return true;

    }
    public function afterFind()
    {
        if ( $this->created == '0000-00-00 00:00:00' )
            $this->created = '2000-01-01 00:00:00';

        $this->created = date('d-m-Y H:i:s',strtotime($this->created));
        $this->last_updated = date('d-m-Y H:i:s',strtotime($this->last_updated));
        parent::afterFind();
    }


}
