<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PublisherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Html;


        $this->title = 'Издания';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="active"><a href="/edition" data-toggle_="tab" aria-expanded="true">Список</a></li>
        <li ><a href="/edition/create" data-toggle_="tab" aria-expanded="false">Новое издание</a></li>

        <li class="pull-left header">
            <i class="fa fa-th"></i>
            <h1 style='display:inline-block;'>
                <?= Html::encode($this->title); ?>
            </h1>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="active" style='overflow: hidden;'>
            <div class='col-md-12 col-sm-12 col-xs-12'>
                <?=$this->render('_list', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
            </div>
        </div>
    </div>
</div>
