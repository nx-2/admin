<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use common\custom_components\event_handlers\OrderEventHandler;
use Yii;
use common\models\CommonModel;
use \common\models\behaviors\ARattrChangedToValue;
/**
 * This is the model class for table "nx_order".
 *
 * @property string $id
 * @property integer $shop_id
 * @property integer $channel_id
 * @property integer $price_id
 * @property string $price
 * @property double $discount
 * @property string $discount_reason
 * @property string $payment_type_id
 * @property string $payment_date
 * @property string $payment_docno
 * @property string $payment_agent_id
 * @property string $payment_sum
 * @property string $comment
 * @property string $reason
 * @property string $key
 * @property string $source_id
 * @property integer $type
 * @property integer $enabled
 * @property integer $manager_id
 * @property string $http_referer
 * @property string $label
 * @property string $epay_result
 * @property string $hash
 * @property integer $order_state_id
 * @property integer $action_id
 * @property string $action_code
 * @property string $email
 * @property string $extra_emails
 * @property integer $address_id
 * @property integer $map_address_id
 * @property integer $person_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $m131_id
 * @property integer $old_order_id
 * @property integer $subscriber_type_id
 * @property integer $new_state
 * @property string $created
 * @property string $last_updated
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property integer $xpress_id

 * @property \common\models\NxOrderItem $items
 * @property \common\models\NxPerson $peron
 * @property \common\models\Company $company
 * @property \common\models\Payment $payment
 * @property \common\models\Transactions $transaction
 * @property \common\models\Publisher $publisher
 *
 */
class NxOrder extends \common\models\WatchableModel
{
    const SUBSCRIBER_PERSON = 1;
    const SUBSCRIBER_COMPANY = 2;

    const HAS_PAID_EVENT_NAME = 'orderHasPaid';
    const NEW_ORDER_EVENT_NAME = 'newOrderCreated';

    const BECAME_ACTIVE_EVENT_NAME = 'hasBecomeActive';

    public function init() {
        parent::init();
        $this->on(self::NEW_ORDER_EVENT_NAME, ['common\custom_components\event_handlers\ARattrChangedHandler', 'newOrderCreated']);
        $this->on(self::HAS_PAID_EVENT_NAME, ['common\custom_components\event_handlers\ARattrChangedHandler', 'orderPaid']);
        $this->on(self::BECAME_ACTIVE_EVENT_NAME, [OrderEventHandler::class, 'onBecomeActive']);
    }

    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'channel_id', 'price_id', 'payment_type_id', 'payment_agent_id', 'source_id', 'type', 'enabled', 'manager_id', 'order_state_id', 'action_id', 'address_id', 'map_address_id', 'person_id', 'company_id', 'user_id', 'm131_id', 'old_order_id', 'subscriber_type_id', 'new_state', 'create_user_id', 'last_user_id', 'xpress_id'], 'integer'],
            [['price', 'discount', 'payment_sum'], 'number'],
            [['payment_date', 'created', 'last_updated'], 'safe'],
//            [['label', 'hash', 'created', 'create_user_id', 'last_user_id'], 'required'],
            [['label', 'hash', 'created'], 'required'],
            [['comment'], 'string'],
            [['discount_reason', 'http_referer', 'label', 'hash', 'action_code', 'extra_emails'], 'string', 'max' => 255],
            [['payment_docno', 'epay_result'], 'string', 'max' => 100],
            [['reason'], 'string', 'max' => 120],
            [['key'], 'string', 'max' => 60],
            [['email'], 'string', 'max' => 50],
        ];
    }

    public function behaviors()
    {
        $myBehaviors = [
            [
                'class' => ARattrChangedToValue::class,
                'options' => [
                    [
                        'attributes' => [
                            'order_state_id' => 3,
                        ],
                        'eventName' => self::HAS_PAID_EVENT_NAME,
                    ]
                ],
            ],
        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'Номер заказа',
            'shop_id'           => 'Тип заказа',
            'channel_id'        => 'Channel ID',
            'price_id'          => 'Прайс',
            'price'             => 'Сумма заказа',
            'discount'          => 'Скидка',
            'discount_reason'   => 'Основания для скидки',
            'payment_type_id'   => 'Способ оплаты',
            'payment_date'      => 'Дата оплаты',
            'payment_docno'     => 'Номер платежного документа',
            'payment_agent_id'  => 'Payment Agent ID',
            'payment_sum'       => 'Сумма платежа',
            'comment'           => 'Комментарий',
            'reason'            => 'Основание',
            'key'               => 'Ключ',
            'source_id'         => 'Source ID',
            'type'              => 'Тип',
            'enabled'           => 'Активен',
            'manager_id'        => 'Manager ID',
            'http_referer'      => 'Http Referer',
            'label'             => 'Метка',
            'epay_result'       => 'Epay Result',
            'hash'              => 'Хеш код',
            'order_state_id'    => 'Статус',
            'action_id'         => 'Акция',
            'action_code'       => 'Промо код',
            'email'             => 'Email',
            'extra_emails'      => 'Дополнительные email',
            'address_id'        => 'Адрес',
            'map_address_id'    => 'Адрес доставки',
            'person_id'         => 'Получатель',
            'company_id'        => 'Компания',
            'user_id'           => 'Пользователь',
            'm131_id'           => 'M131 ID',
            'old_order_id'      => 'Old Order ID',
            'subscriber_type_id' => 'Тип подписчика',
            'new_state'         => 'New State',
            'created'           => 'Создан',
            'last_updated'      => 'Последнее обновление',
            'create_user_id'    => 'Создан пользователем',
            'last_user_id'      => 'Обновлен пользователем',
            'xpress_id'         => 'Xpress ID',
        ];
    }


    public static function getOrder($id)
    {
        if (!$id)
            return false;

        return self::find()->where(['hash' => $id])->one();
    }

    /**
     * @return string
     */
    public static  function getOrderID($id)
    {
        if (!$id)
            return false;

        return self::find()->where(['id' => $id])->one();
    }

    public function getAddress()
    {
        return $this->hasOne(\common\models\NxAddress::class, ['id' => 'address_id']);
    }
    public function getMapaddress()
    {
        return $this->hasOne(\common\models\NxAddress::class, ['id' => 'map_address_id']);
    }
    public function getPerson()
    {
        return $this->hasOne(\common\models\NxPerson::class,['id' => 'person_id']);
    }
    public function getPayment()
    {
        return $this->hasOne(\common\models\Payment::class,['order_id' => 'id']);
    }
    public function getCompany()
    {
        return $this->hasOne(\common\models\Company::class,['id' => 'company_id']);
    }
    public function getPrice_()
    {
        return $this->hasOne(\common\models\NxPrice::class, ['id' => 'price_id']);
    }

    public function getPublisher()
    {
        return $this->hasOne(\common\models\Publisher::class, ['id' => 'publisher_id'])->via('price_');
    }

    public function getTransaction()
    {
        return $this->hasOne(\common\models\Transactions::class, ['transactionid' => 'id']);
    }
    

    public function getItems()
    {
        return $this->hasMany(\common\models\NxOrderItem::class, ['order_id' => 'id']);
    }
    public function getContent()
    {
        if (null === $this)
            return null;
        
        $itemsTemp = Yii::$app->db->createCommand('SELECT oi.item_id FROM nxnew.nx_order_item oi WHERE oi.order_id = ' . $this->id)->queryAll();
        $itemIds = [];
        foreach( $itemsTemp as $i ) {
            $itemIds [] = $i['item_id'];
        }
                
        if ($this->shop_id == 1) {
            //подписки
            $query =  Yii::createObject( \yii\db\ActiveQuery::class, [\common\models\NxSubscription::class]);
            return $query
                    ->alias('s')
                    ->where('s.id IN (' . implode(',', $itemIds) . ')')
                    ->all();
        } else if ($this->shop_id == 2) {
            //продажи
            $query =  Yii::createObject( \yii\db\ActiveQuery::class, [\common\models\Issue::class]);
            return $query
                    ->alias('i')
                    ->where('i.Message_ID IN (' . implode(',', $itemIds) . ')')
                    ->all();
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::class, ['User_ID' => 'last_user_id']);//->inverseOf('company');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['User_ID' => 'create_user_id']);//->inverseOf('company');
    }
    /**
     *
    */
    public function toString()
    {
        if ($this === null)
            return '';

        $str = 'Заказ Номер '  . $this->id . '. ';
        $type = $this->shop_id;
        foreach ( $this->items as $item ) {
            $itemStr = $item->toString();
            $str .= $itemStr ."\n";
        }
        return $str;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // оплатили заказ или сделали безоплатным
        $hasPaid = array_key_exists('order_state_id', $changedAttributes) && $this->order_state_id == 3;
        $isFree = array_key_exists('payment_type_id', $changedAttributes) && $this->payment_type_id == 0;
        if ($hasPaid || $isFree) {
            $this->trigger(self::BECAME_ACTIVE_EVENT_NAME);
        }
    }
    public function isActive(): bool
    {
        return  $this->order_state_id == NxOrderState::ORDER_STATE_PAID || $this->payment_type_id == PaymentType::PAYMENT_TYPE_FREE;
    }

}
