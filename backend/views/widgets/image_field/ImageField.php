<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
namespace backend\views\widgets\image_field;

use yii\base\Widget;
use yii\helpers\Html;

class ImageField extends Widget
{
    public $model;
    
    public $imageFieldName = 'avatar';

    // public function init()
    // {
    //     parent::init();
    // }
    /** 
    Виджет  для отображения всего, что касается поля модели, отвечающего за изображение
    **/
    // Ожидается что к модели подключено поведение ImageUploadBehavior, что обеспечивает наличие у нее методов getImagePath, getResizedImages использующихся во view
    // 

    public function run()
    {
        if ($this->model === null)
            return '';
        // $this->model->refresh();
        $data = [
            'imageFieldName'        => $this->imageFieldName,
            'model'                 => $this->model,
        ];
        return $this->render('view', $data );
    }

}
