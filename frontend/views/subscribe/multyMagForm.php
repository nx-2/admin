<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
    use yii\helpers\Html;
    $formId = Yii::$app->params['multyMagFormId'];// 'mrof-ebircsbus123';
    $formWrapperId = Yii::$app->params['multyMagFormWrapperId'];

/**
 * @var
 * @var int $width
 * @var bool $showCovers
 * @var int $publisherId
 * @var \common\models\Edition[] $editions
 * @var \common\models\Publisher $publisher
 * @var \common\models\DeliveryType[] $deliveryTypes
 * @var \common\models\NxZone[] $deliveryZones
 *
 */

?>
<style>
#<?=$formWrapperId?>{
	width: <?=$width;?>;
	max-width: 100%;
	margin: 0px auto;
	font-family: sans-serif;
	font-size: 14px;
	color: #333;
	overflow: hidden;
}
#<?=$formWrapperId?> label{
	cursor: pointer;
}
#<?=$formWrapperId?> ul {
	list-style: none;
	display: block;
	padding: 0px;
	margin: 0px;
	box-sizing: border-box;
}
#<?=$formWrapperId?> div {
	box-sizing: border-box;
}
#<?=$formWrapperId?> strong,
#<?=$formWrapperId?> b {
	font-weight: bold;
}
#<?=$formWrapperId?> .bold{
	font-weight: bold;
}
#<?=$formWrapperId?> .not-bold{
	font-weight: normal;
}
#<?=$formWrapperId?> .large-text{
	font-size: 143%;
}
#<?=$formWrapperId?> .middle-text{
	font-size: 129%;
}
#<?=$formWrapperId?> .small-text{
	font-size: 86%;
}
#<?=$formWrapperId?> .extra-small-text{
	font-size: 72%;
}
#<?=$formWrapperId?> .red-colored{
	color: red;
}
#<?=$formWrapperId?> .gray-colored{
	color: #afafaf;
}

#<?=$formWrapperId?> .transition-all{
	-webkit-transition: all .7s ease;
    -moz-transition: all .7s ease;
    -o-transition: all .7s ease;
    transition: all .7s ease;
}
#<?=$formWrapperId?> .transition-background{
	-webkit-transition: background .7s ease;
    -moz-transition: background .7s ease;
    -o-transition: background .7s ease;
    transition: background .7s ease;
}

#<?=$formWrapperId?> .transition-color{
	-webkit-transition: color .7s ease;
    -moz-transition: color .7s ease;
    -o-transition: color .7s ease;
    transition: color .7s ease;
}


#<?=$formWrapperId?> .flex-wrapper{
	display: -webkit-flex;
    display: -ms-flexbox;
    display: -ms-flex;
    display: flex;
    -webkit-justify-content: space-between;
    -ms-justify-content: space-between;
    justify-content: space-between;
}
#<?=$formWrapperId?> .flex-wrapper.flex-justify-center {
    -webkit-justify-content: center;
    -ms-justify-content: center;
    justify-content: center;	
}
#<?=$formWrapperId?> .flex-wrapper.flex-multy-rows {
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
}
#<?=$formWrapperId?> .flex-wrapper.flex-single-row {
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
}
#<?=$formWrapperId?> .flex-wrapper.flex-dir-row {
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
}
#<?=$formWrapperId?> .flex-wrapper.flex-dir-column {
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
}
#<?=$formWrapperId?> #mags-covers-block {
	overflow: hidden;
	background: rgba(0,0,0,0.05);
    padding: 10px;	
	/*display: block;*/
}
#<?=$formWrapperId?> #mags-covers-block ul {
	list-style: none;
    padding: 0px;
    max-width: 100%;
}
#<?=$formWrapperId?> #mags-covers-block ul li {
	margin-right: 15px;
	margin-bottom: 10px;
}
#<?=$formWrapperId?> #mags-covers-block ul li img {
	max-width: 100px;
}
#<?=$formWrapperId?> #delivery-options-block {
	padding: 10px;
    border: 1px solid rgba(0,0,0,0.1);
    border-radius: 2px;
    box-shadow: 1px 1px 1px rgba(0,0,0,0.1);
}

#<?=$formWrapperId?> #delivery-options-block > div {
	flex: 1 1 150px; 
}
#<?=$formWrapperId?> #delivery-options-block > div:first-child {
	/*padding-right: 10px;*/
    border-right: 1px dashed rgba(0,0,0,0.1);	
}
#<?=$formWrapperId?> #delivery-options-block > div:last-child {
	padding-left: 20px;
}

#<?=$formWrapperId?> .radio-check-wrap {
	display: inline-block;
	margin-right: 5px;
}

#<?=$formWrapperId?> .radio-check-wrap input[type="radio"],
#<?=$formWrapperId?> .radio-check-wrap input[type="checkbox"] {
	/*visibility: hidden;*/
	display: none;
}

#<?=$formWrapperId?> input[type="radio"] + label,
#<?=$formWrapperId?> input[type="checkbox"] + label {
	position: relative;
	padding-left: 20px;
	display: inline-block;
	height: 20px;
	line-height: 20px;
	/*color: #800000;*/
    font-weight: bold;
    font-size: 86%;	
}
#<?=$formWrapperId?> input[type="radio"] + label:before,
#<?=$formWrapperId?> input[type="checkbox"] + label:before {
	position: absolute;
	top: 0px;
	left: 0px;
	width: 20px;
	height: 20px;
	content: '';
	background-image:url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTBweCIgaGVpZ2h0PSI1MHB4IiB2aWV3Qm94PSIwIDAgNTAgNTAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUwIDUwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPjxnPjxwb2x5Z29uIHBvaW50cz0iMzQuNTY0LDQwLjA3IDUuNzYyLDQwLjA3IDUuNzYyLDExLjI3NiAzNC41NjQsMTEuMjc2IDM0LjU2NCwxMi43ODggNDAuMzI2LDguMDQgNDAuMzI2LDUuNTE2IDAsNS41MTYgMCw0NS44MzEgICAgIDQwLjMyNiw0NS44MzEgNDAuMzI2LDguMDQgMzQuNTY0LDEwLjc2NSIgZmlsbD0nIzA2MTE2Yic+PC9wb2x5Z29uPjwvZz48L2c+Cjwvc3ZnPg==");
	background-size: cover;
}

#<?=$formWrapperId?> input[type="radio"]:checked + label:before,
#<?=$formWrapperId?> input[type="checkbox"]:checked + label:before{
	background-image:url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJjaGVja2JveC1jaGVja2VkIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUwcHgiIGhlaWdodD0iNTBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MCA1MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxnPjxwb2x5Z29uIHBvaW50cz0iMzQuNTY0LDQwLjA3IDUuNzYyLDQwLjA3IDUuNzYyLDExLjI3NiAzNC41NjQsMTEuMjc2IDM0LjU2NCwxMi43ODggNDAuMzI2LDguMDQgNDAuMzI2LDUuNTE2IDAsNS41MTYgMCw0NS44MzEgICAgIDQwLjMyNiw0NS44MzEgNDAuMzI2LDIwLjg1NCAzNC41NjQsMjcuNzY1IiBmaWxsPScjMDYxMTZiJz48L3BvbHlnb24+PHBvbHlnb24gcG9pbnRzPSIxMy4yNTUsMTcuMTM1IDExLjAzMSwxOS41NiAyNS4yNDUsMzUuOTQzIDUwLDYuMjQ4IDQ4LjA0OSw0LjE2OSAyNS4yNDUsMjIuOTMyIiBmaWxsPScjZmYwMDAwJz48L3BvbHlnb24+PC9nPjwvZz48L3N2Zz4=");
}

#<?=$formWrapperId?> #mags-list-block {

}
#<?=$formWrapperId?> #mags-list-block ul {
	/*padding: 0px 5px;*/
}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item {
	display: block;
    padding-left: 35px;
    line-height: 35px;
    margin-bottom: 2px;
    background: rgba(0,0,0,0.05);
    font-size: 115%;
    cursor: pointer;
    position: relative;
}

#<?=$formWrapperId?> #mags-list-block .avalable-mag-item.is-selected{
	/*padding-left: 40px;*/
	position: relative;
	background: rgba(0,0,0,0.3)!important;
	color: white!important;
}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item.is-selected:before{
	content: '';
	position:absolute;
	width: 25px;
	height: 25px;
	top: 5px;
	left: 5px;
	background-image: url("data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2NCA2NDsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDY0IDY0IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMjguMDAwMDAwLCAyNzguMDAwMDAwKSI+PHBhdGggZD0iTS0yOTYtMjIyLjZjLTEyLjksMC0yMy40LTEwLjUtMjMuNC0yMy40YzAtMTIuOSwxMC41LTIzLjQsMjMuNC0yMy40ICAgICBjMTIuOSwwLDIzLjQsMTAuNSwyMy40LDIzLjRDLTI3Mi42LTIzMy4xLTI4My4xLTIyMi42LTI5Ni0yMjIuNkwtMjk2LTIyMi42eiBNLTI5Ni0yNjYuOWMtMTEuNSwwLTIwLjksOS40LTIwLjksMjAuOSAgICAgczkuNCwyMC45LDIwLjksMjAuOXMyMC45LTkuNCwyMC45LTIwLjlTLTI4NC41LTI2Ni45LTI5Ni0yNjYuOUwtMjk2LTI2Ni45eiIgZmlsbD0nI2ZmZmZmZicvPjxwb2x5bGluZSAgZmlsbD0nI2ZmZmZmZicgcG9pbnRzPSItMjk4LjgsLTIzNS45IC0zMTAuNywtMjQ3LjkgLTMwOC45LC0yNDkuNyAtMjk4LjgsLTIzOS41IC0yODMuMSwtMjU1LjIgICAgICAtMjgxLjMsLTI1My40IC0yOTguOCwtMjM1LjkgICAgIi8+PC9nPjwvZz48L3N2Zz4=");
	background-size: cover;
}

#<?=$formWrapperId?> #mags-list-block .avalable-mag-item .plus{

}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item .plus:hover{
	box-shadow: 0px 0px 3px rgba(0,0,0,0.9);
}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item:hover {
	background: rgba(0,0,0,0.09);
    color: #05116b;
    /*box-shadow: inset 0px 0px 7px rgba(0,0,0,0.15);*/
}


#<?=$formWrapperId?> .plus {
	width: 20px;
    height: 20px;
    position: absolute;
    right: 10px;
    top: 7px;
    /*background-image: url("data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjggMTI4IiBoZWlnaHQ9IjEyOHB4IiBpZD0i0KHQu9C+0LlfMSIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIgd2lkdGg9IjEyOHB4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48cG9seWdvbiBmaWxsPSIjMDYxMTZiIiBwb2ludHM9IjEyNyw1OSA2OSw1OSA2OSwxIDU5LDEgNTksNTkgMSw1OSAxLDY5IDU5LDY5IDU5LDEyNyA2OSwxMjcgNjksNjkgMTI3LDY5ICAgIi8+PC9nPjwvc3ZnPg==");*/
    background: url("data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjggMTI4IiBoZWlnaHQ9IjEyOHB4IiBpZD0i0KHQu9C+0LlfMSIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIgd2lkdGg9IjEyOHB4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48cG9seWdvbiBmaWxsPSIjMDYxMTZiIiBwb2ludHM9IjEyNyw1OSA2OSw1OSA2OSwxIDU5LDEgNTksNTkgMSw1OSAxLDY5IDU5LDY5IDU5LDEyNyA2OSwxMjcgNjksNjkgMTI3LDY5ICAgIi8+PC9nPjwvc3ZnPg==") no-repeat center;
    background-size: 18px 18px;
    display: inline-block;
    cursor: pointer;	
}
#<?=$formWrapperId?> #selected-mags-block {
	padding-top: 10px;
}
#<?=$formWrapperId?> #selected-mags-block ul {

}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item {
	padding: 10px;
    border: 1px dashed rgba(0,0,0,0.1);
    margin-bottom: 10px;
    padding-right: 40px;
    position: relative;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content > div {
/*	margin-right: 10px;
    background: rgba(0,0,0,0.1);
    margin-bottom: 10px;
*/	
	margin: 4px;
	padding:5px;
    border: 1px solid rgba(0,0,0,0.2);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content > div .field-label {
	display: block;
    text-align: left;
    /*margin-bottom: 5px;*/
    font-weight: bold;	
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content select,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content input[type="text"] {
	display: block;
	width: 100%;
    height: 30px;
    background: #ededf7;
    border: 1px solid rgba(0,0,0,0.3);
    font-size: 79%;
    cursor: pointer;
    box-sizing: border-box;
    /*margin: 3px 5px;*/
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content input[type="text"],
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content .duration-block select {
    outline: none;
    text-align: center;
    font-size: 115%;
    color: #0a0c82;
    font-weight: bold;
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content select:hover,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content input[type="text"]:hover,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content select:focus,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content input[type="text"]:focus {
	box-shadow: 0px 0px 3px rgba(0,0,255,0.8);
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content select:active,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .selected-mag-item-content input[type="text"]:active {
	box-shadow: 0px 0px 2px rgba(0,0,255,0.8);
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .mag-info-block {
	overflow: hidden;
    flex-basis: 100%;
    /* min-width: 150px; */
    flex-grow: 600;
    flex-shrink: 600; 
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .start-date-block {
	flex-basis: 100px;
    flex-grow: 600;
    flex-shrink: 300;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .duration-block {
	flex-basis: 80px;
    flex-grow: 600;
    flex-shrink: 300;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .issues-amount-block {
	flex-basis: 70px;
    flex-grow: 600;
    flex-shrink: 300;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .type-block {
	flex-basis: 80px;
    flex-grow: 600;
    flex-shrink: 300;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .cost-block {
	flex-basis: 150px;
    flex-grow: 600;
    flex-shrink: 300;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .cost-block .with-note-under{
    position:relative;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .cost-block .with-note-under:after{
    content: attr(data-after);
    /*position: absolute;*/
    /*width: 100%;*/
    /*display: inline-block;*/
    color: #bf0606;
    /*top: 100%;*/
    /*left: 0px;*/
    text-align: left;
    font-weight: normal;
    font-size: 86%;
    text-decoration: underline;
    padding-left: 5px;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .cost-block .with-note-under.note-under-hidden:after{
    display:none;
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .remove-item-block,
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .go-to-avalabeles {
	width: 35px;
	height: 35px;
	position: absolute;
	top: 16px;
	right: 4px;
	/*background: rgba(0,0,0,0.1);*/
    background: url("data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2NCA2NDsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDY0IDY0IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4KCTxnPgoJCTxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM3OC4wMDAwMDAsIDI3OC4wMDAwMDApIj4KCQkJPHBhdGggZmlsbD0nI2QwMDUwNScgZD0iTS0zNDUuOS0yMjIuMWMtMTMuMiwwLTIzLjktMTAuNy0yMy45LTIzLjljMC0xMy4yLDEwLjctMjMuOSwyMy45LTIzLjkgICAgIGMxMy4yLDAsMjMuOSwxMC43LDIzLjksMjMuOUMtMzIyLTIzMi45LTMzMi43LTIyMi4xLTM0NS45LTIyMi4xTC0zNDUuOS0yMjIuMXogTS0zNDUuOS0yNjcuNGMtMTEuNywwLTIxLjMsOS42LTIxLjMsMjEuMyAgICAgYzAsMTEuNyw5LjYsMjEuMywyMS4zLDIxLjNzMjEuMy05LjYsMjEuMy0yMS4zQy0zMjQuNi0yNTcuOC0zMzQuMi0yNjcuNC0zNDUuOS0yNjcuNEwtMzQ1LjktMjY3LjR6IiBpZD0iRmlsbC01MiIvPgoJCQk8cG9seWxpbmUgZmlsbD0nI2QwMDUwNScgcG9pbnRzPSItMzU2LjMsLTIzMy44IC0zNTguMiwtMjM1LjcgLTMzNS42LC0yNTguMyAtMzMzLjcsLTI1Ni40IC0zNTYuMywtMjMzLjggICAgIi8+CgkJCTxwb2x5bGluZSBmaWxsPScjZDAwNTA1JyBwb2ludHM9Ii0zMzUuNiwtMjMzLjggLTM1OC4yLC0yNTYuNCAtMzU2LjMsLTI1OC4zIC0zMzMuNywtMjM1LjcgLTMzNS42LC0yMzMuOCAgICAiLz4KCQk8L2c+Cgk8L2c+Cjwvc3ZnPg==") no-repeat center;
    background-size: 40px 40px;
    cursor: pointer;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .remove-item-block:hover {
	box-shadow: 0px 0px 5px rgba(0,0,0,0.5);
}

#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .go-to-avalabeles {
    top:auto;
    bottom: 16px;
    background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCAyODQuOTI5IDI4NC45MjkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDI4NC45MjkgMjg0LjkyOTsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KCTxnPg0KCQk8cGF0aCBkPSJNMjgyLjA4MiwxOTUuMjg1TDE0OS4wMjgsNjIuMjRjLTEuOTAxLTEuOTAzLTQuMDg4LTIuODU2LTYuNTYyLTIuODU2cy00LjY2NSwwLjk1My02LjU2NywyLjg1NkwyLjg1NiwxOTUuMjg1ICAgQzAuOTUsMTk3LjE5MSwwLDE5OS4zNzgsMCwyMDEuODUzYzAsMi40NzQsMC45NTMsNC42NjQsMi44NTYsNi41NjZsMTQuMjcyLDE0LjI3MWMxLjkwMywxLjkwMyw0LjA5MywyLjg1NCw2LjU2NywyLjg1NCAgIGMyLjQ3NCwwLDQuNjY0LTAuOTUxLDYuNTY3LTIuODU0bDExMi4yMDQtMTEyLjIwMmwxMTIuMjA4LDExMi4yMDljMS45MDIsMS45MDMsNC4wOTMsMi44NDgsNi41NjMsMi44NDggICBjMi40NzgsMCw0LjY2OC0wLjk1MSw2LjU3LTIuODQ4bDE0LjI3NC0xNC4yNzdjMS45MDItMS45MDIsMi44NDctNC4wOTMsMi44NDctNi41NjYgICBDMjg0LjkyOSwxOTkuMzc4LDI4My45ODQsMTk3LjE4OCwyODIuMDgyLDE5NS4yODV6IiBmaWxsPSIjRDgwMDI3Ii8+DQoJPC9nPg0KPC9zdmc+") no-repeat center;
    background-size: 30px 30px;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .go-to-avalabeles:hover {
	box-shadow: 0px 0px 5px rgba(0,0,0,0.5);
}


#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .ajax-loading,
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item .ajax-loading {
	background: url("https://www.osp.ru/images/ajax-loader.gif") no-repeat center;
	position: absolute;
	width: 20px;
	height: 20px;
	top: 20px;
	right: 50px;
	z-index: 10;
	display: none;
}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item .ajax-loading {
	top: 7px;
	right: 35px;
}
#<?=$formWrapperId?> #mags-list-block .avalable-mag-item.loading .ajax-loading {
	display: block;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item.loading .ajax-loading {
	display: block;
}
#<?=$formWrapperId?> #selected-mags-block .selected-mag-item .mag-info-block input[type="text"] {
	width: 100%;
    border: none;
    outline: none;
    line-height: 25px;
    padding-left: 10px;
    font-size: 115%;
    /*color: #0a0c82;*/
    color: #515151;
    font-weight: bold;
    box-sizing: border-box;
    border-bottom: 1px solid rgba(0,0,0,0.1);
}
#<?=$formWrapperId?> #user-data-block {

}
#<?=$formWrapperId?> #user-data-block .user-data-row {
	flex-wrap: wrap;
}

#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field {
	padding:5px;
	flex-basis: 150px;
	flex-grow: 1;
	flex-shrink: 1;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field.basis200 {
	flex-basis: 200px;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field.basis300 {
	flex-basis: 300px;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field > div:first-child{
	justify-content: space-between;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field label {
    display: inline-block;
    font-weight: bold;
    font-size: 86%;
    margin-bottom: 3px;
    position: relative;
    padding-right: 13px;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field label.error {
	color: red;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field label.required:after {
    content: '*';
    position: absolute;
    width: 10px;
    height: 10px;
    color: red;
    top: 0px;
    right: 0px;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field .error-message {
    color: #bf0606;
    margin-bottom: 3px;
    font-size: 79%;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field .error-message ul {

}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field .error-message ul li {

}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field input[type="text"],
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field select {
	display: block;
    width: 100%;
    height: 30px;
    outline: none;
    padding-left: 10px;
    font-size: 107%;
    background: #ffffee;
    border: 1px dashed rgba(1, 33, 93, 0.2);
    /* box-shadow: 0px 0px 1px rgba(1,33,93,0.7); */
    box-sizing: border-box;
    color: #2e2e80;
	-webkit-transition: all .7s ease;
    -moz-transition: all .7s ease;
    -o-transition: all .7s ease;
    transition: all .7s ease;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field input[type="text"]:focus,
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field select:focus {
	box-shadow: 0px 0px 3px rgba(255,50,50,0.5);
}



#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field select {

}
#<?=$formWrapperId?> #user-data-block #company-data-block {
	display: none;
}

#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field #check-promo {
	display: inline-block;
    width: 160px;
    height: 30px;
    border: none;
    outline: none;
    cursor: pointer;
    background: rgba(44, 94, 249, 0.7);
    /*color: #f3f341;*/
    color: #eceaea;
    font-size: 129%;
}
#<?=$formWrapperId?> #user-data-block .user-data-row .user-data-field #check-promo:hover {
	background: rgba(44, 94, 249, 0.9);
    box-shadow: 1px 1px 5px rgba(0,0,0,0.8);
}
#<?=$formWrapperId?> #user-data-block #action-info {
	background: rgba(0,0,0,0.05);
        color: #1a3482;
        font-size:86%;
        font-weight:normal;
        padding: 10px;
        display:none;
}

#<?=$formWrapperId?> #user-agreement-block {
	padding: 10px 5px;
    background: rgba(0,0,0,0.05);
    margin: 5px 0px;
}
#<?=$formWrapperId?> #user-agreement-block a {
	text-decoration: none;
	color: #1d4fec;
}

#<?=$formWrapperId?> #total-sum-block {
	border: 1px dashed rgba(0,0,0,0.2);
	padding: 10px;
	padding-top: 0px;
}

#<?=$formWrapperId?> #total-sum-block #total-sum-text {
	flex-basis: 190px;
	/*color: #b70b0b;*/
    padding-right: 10px;
    text-align: right;
    font-size: 172%;
    margin-bottom: 10px;
    flex-grow: 3;
}
#<?=$formWrapperId?> #total-sum-block .rub-mark {
	position: relative;
	/*padding-left: 35px;*/

}
/*    
#<?=$formWrapperId?> #total-sum-block .rub-mark:before { 
	content: '';
	position: absolute;
	top: 0px;
	left: 0px;
	width: 20px;
	height: 20px;

	background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgNTExLjg4MyA1MTEuODgzIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTEuODgzIDUxMS44ODM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHBhdGggc3R5bGU9ImZpbGw6ICM5YTlkOWU7IiBkPSJNMjg3LjkzNCwwaC0wLjAzMUgxMTcuMzA5bDAsMGwwLDBjLTUuODg5LDAtMTAuNjY5LDQuNzc5LTEwLjY2OSwxMC42Njl2Mjg3LjkzNEg3NC42NDcgIGMtNS44ODksMC0xMC42NjEsNC43NjYtMTAuNjYxLDEwLjY1NHM0Ljc3MiwxMC42NywxMC42NjEsMTAuNjdoMzEuOTkzdjE4MS4yODZjMCw1Ljg5LDQuNzgsMTAuNjcsMTAuNjY5LDEwLjY3ICBjNS44OSwwLDEwLjY2Mi00Ljc4LDEwLjY2Mi0xMC42N1YzMTkuOTI2aDE2Ljg1NWgxNDMuMTA3Yzg4LjM1NCwwLDE1OS45NjQtNzEuNjI1LDE1OS45NjQtMTU5Ljk2NFMzNzYuMjg5LDAsMjg3LjkzNCwweiAgIE0zODUuOTc0LDI1Ny45ODdjLTEyLjc0NywxMi43NDctMjcuNTg4LDIyLjc0NS00NC4wODQsMjkuNzI4Yy0xNy4wOSw3LjIxNy0zNS4yNDIsMTAuODg4LTUzLjk1NiwxMC44ODhIMTQ0LjgyN2gtMTYuODU1VjIxLjMyMyAgaDE1OS45MzJoMC4yNWwwLDBjMTguNjUxLDAuMDMxLDM2LjcyNiwzLjcwMiw1My43MzcsMTAuODg4YzE2LjQ5Niw2Ljk4MiwzMS4zMzcsMTYuOTgsNDQuMDg0LDI5LjcyOCAgYzEyLjczMSwxMi43MzEsMjIuNzI5LDI3LjU1NywyOS43MTIsNDQuMDY4YzcuMjE4LDE3LjA3NCwxMC44NzIsMzUuMjI3LDEwLjg3Miw1My45NTZjMCwxOC43My0zLjY1NCwzNi44ODItMTAuODcyLDUzLjk1NiAgQzQwOC43MDMsMjMwLjQzMSwzOTguNzA2LDI0NS4yNTUsMzg1Ljk3NCwyNTcuOTg3eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDogIzlhOWQ5ZTsiIGQ9Ik0zMDkuMjczLDM2Mi41ODdINzQuNjQ3Yy01Ljg4OSwwLTEwLjY2MSw0Ljc2NS0xMC42NjEsMTAuNjU0czQuNzcyLDEwLjY3LDEwLjY2MSwxMC42N2gyMzQuNjI2ICBjNS44NzMsMCwxMC42NTQtNC43OCwxMC42NTQtMTAuNjdTMzE1LjE0NiwzNjIuNTg3LDMwOS4yNzMsMzYyLjU4N3oiLz4KPC9zdmc+") no-repeat center;
	background-size: 20px 20px;   
}
*/
#<?=$formWrapperId?> #total-sum-block #total-sum-values {
	font-size: 172%;
	font-weight: bold;
	flex-basis: 190px;
	flex-grow: 3;
	margin-bottom: 10px;
}
#<?=$formWrapperId?> #total-sum-block #total-sum-values > div {
	padding: 5px;
	box-sizing: border-box;
	margin: 0px;
	padding-bottom: 0px;
}
#<?=$formWrapperId?> #total-sum-block #total-sum-values #total-sum-old {
	position: relative;
	color:#969595;
	font-weight: normal;
	font-size: 20px;
}
#<?=$formWrapperId?> #total-sum-block #total-sum-values #total-sum-old:before {
    content: '';
    border-bottom: 2px solid rgba(255,0,0,0.5);
    position: absolute;
    width: 110%;
    height: 2px;
    /* transform: rotate(-5deg); */
    top: 50%;
    left: -3px;
}
#<?=$formWrapperId?> #buttons-block {
	flex-grow: 3;
	margin-bottom: 10px;
}
#<?=$formWrapperId?> #buttons-block #send-subscribe-form-button {
	padding: 7px 15px;
    cursor: pointer;
    border: none;
    outline: none;
    font-size: 115%;
    background: #6d8ffb;
    color: #eceaed;
}
#<?=$formWrapperId?> #buttons-block #send-subscribe-form-button:hover {
	background: rgba(44, 94, 249, 0.9);
    box-shadow: 1px 1px 5px rgba(0,0,0,0.8);
}
#<?=$formWrapperId?> #<?=$formId?> #err-messages-list-block {
    display : none;
    margin-top: 5px;
    padding: 10px;
    border: 1px dashed rgba(0,0,0,0.2);
    background: rgba(255,0,0,0.12);    
}
#<?=$formWrapperId?> #<?=$formId?> #err-messages-list-block .err-messages-list{
    list-style-type: disc;
    padding-left: 10px;
}
#<?=$formWrapperId?> #<?=$formId?> #err-messages-list-block .err-messages-list .err-message-item{
    margin-bottom: 5px;
    color: #a50707;
    font-size: 100%;
    line-height: 120%;
}
#<?=$formWrapperId?> #<?=$formId?> #subscribe-sign{
    display: inline-block;
    padding: 5px 10px;
    background: rgba(0,0,0,0.1);
    font-size: 11px;
    font-weight: bold;
    /* text-decoration: underline; */
    width: 100%;
    text-align: center;
}

.animated {
  animation-duration: 1s;
  animation-fill-mode: both;
}

.animated.flipOutX,
.animated.flipOutY,
.animated.flipOutXR{
  animation-duration: .75s;
}

@keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

.fadeIn {
  display: block!important;
  animation-name: fadeIn;
}

@keyframes fadeInDown {
  from {
    opacity: 0;
    transform: translate3d(0, -100%, 0);
  }

  to {
    opacity: 1;
    transform: none;
  }
}

.fadeInDown {
  display: block!important;
  animation-name: fadeInDown;
}

@keyframes fadeInUp {
  from {
    opacity: 0;
    transform: translate3d(0, 100%, 0);
  }

  to {
    opacity: 1;
    transform: none;
  }
}

.fadeInUp {
  display: block!important;
  animation-name: fadeInUp;
}

@keyframes fadeOut {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    display: none;
  }
}

.fadeOut {
  animation-name: fadeOut;
}

@keyframes fadeOutR {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    display: none;
  }
}

.fadeOutR {
  animation-name: fadeOutR;
}

@keyframes fadeOutDown {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    transform: translate3d(0, 100%, 0);
  }
}

.fadeOutDown {
  animation-name: fadeOutDown;
}

@keyframes fadeOutDownR {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    transform: translate3d(0, 100%, 0);
  }
}

.fadeOutDownR {
  animation-name: fadeOutDownR;
}


@keyframes fadeOutUp {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    transform: translate3d(0, -100%, 0);
  }
}

.fadeOutUp {
  animation-name: fadeOutUp;
}

@keyframes fadeOutUpR {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    transform: translate3d(0, -100%, 0);
  }
}

.fadeOutUpR {
  animation-name: fadeOutUpR;
}


@keyframes flip {
  from {
    transform: perspective(400px) rotate3d(0, 1, 0, -360deg);
    animation-timing-function: ease-out;
  }

  40% {
    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);
    animation-timing-function: ease-out;
  }

  50% {
    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);
    animation-timing-function: ease-in;
  }

  80% {
    transform: perspective(400px) scale3d(.95, .95, .95);
    animation-timing-function: ease-in;
  }

  to {
    transform: perspective(400px);
    animation-timing-function: ease-in;
  }
}

.animated.flip {
  -webkit-backface-visibility: visible;
  backface-visibility: visible;
  animation-name: flip;
}

@keyframes flipInX {
  from {
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    animation-timing-function: ease-in;
    opacity: 0;
  }

  40% {
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    animation-timing-function: ease-in;
  }

  60% {
    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
    opacity: 1;
  }

  80% {
    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
  }

  to {
    transform: perspective(400px);
  }
}

.flipInX {
  display: block!important;
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
  animation-name: flipInX;
}

@keyframes flipOutX {
  from {
    transform: perspective(400px);
  }

  30% {
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    opacity: 1;
  }

  to {
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    opacity: 0;
  }
}

.flipOutX {
  animation-name: flipOutX;
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
}

@keyframes flipOutXR {
  from {
    transform: perspective(400px);
  }

  30% {
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    opacity: 1;
  }

  to {
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    opacity: 0;
  }
}

.flipOutXR {
  animation-name: flipOutXR;
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
}


@keyframes slideInUp {
  from {
    transform: translate3d(0, 100%, 0);
    visibility: visible;
  }

  to {
    transform: translate3d(0, 0, 0);
  }
}

.slideInUp {
  display: block!important;
  animation-name: slideInUp;
}

@keyframes slideOutDown {
  from {
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    transform: translate3d(0, 100%, 0);
  }
}

.slideOutDown {
  animation-name: slideOutDown;
}

@keyframes slideOutUp {
  from {
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    transform: translate3d(0, -100%, 0);
  }
}

.slideOutUp {
  animation-name: slideOutUp;
}

@keyframes slideOutUpR {
  from {
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    transform: translate3d(0, -100%, 0);
  }
}

.slideOutUpR {
  animation-name: slideOutUpR;
}


/*@media only screen and (min-width:1020px)*/
/*@media only screen and (min-width:667px) and (max-width:1019px)*/
/*@media only screen and (min-width:320px) and (max-width:666px)*/
@media only screen and (max-width:599px) and (max-height:599px) {
	/* для экранов ДО 600px шириной*/
	#<?=$formWrapperId?> #mags-covers-block {
		display: none;
	}
}
@media only screen and (max-height:599px) {
	/* для экранов ДО 600px высотой*/
	#<?=$formWrapperId?> #mags-covers-block {
		display: none;
	}
}

    
</style>
<div id='<?= $formId ?>'>
    <input id='publisher_id' type="hidden" name='order[publisher_id]' value="<?=$publisher->id; ?>"/>
    <?php if( $showCovers): ?>
        <?php if(!empty($editions)): ?>
        <div id='mags-covers-block'>
            <ul class='flex-wrapper flex-multy-rows flex-dir-row flex-justify-center'>
            <?php foreach($editions as $edition): ?>
                <li data-id='<?=$edition->Message_ID;?>'><img src="<?=$edition->lastCoverUrl();?>"></li>
            <?php endforeach; ?>
            </ul>
        </div><!--END mags-covers-block -->
        <?php endif; ?>
    <?php endif; ?>
        
    <div id='delivery-options-block' class='flex-wrapper flex-multy-rows'>
        <div id='delivery-method-wrapper' >
            <p class='bold middle-text'>Способ доставки <br>
                <span class='extra-small-text gray-colored not-bold'>(для печатных изданий)</span>
            </p>
            <div>
                <?php $defaultDeliveryType = 1;//почта ?>
                <?php foreach($deliveryTypes as $dt): ?>
                    <div class='radio-check-wrap'>
                        <input type='radio' name='order[delivery_type_id]' value='<?=$dt->id;?>' id='qwerty_o_dtid_<?=$dt->id;?>' <?php if($dt->id == $defaultDeliveryType):?>checked='true'<?php endif; ?>></input>
                        <label for='qwerty_o_dtid_<?=$dt->id;?>'>
                            <span><?=$dt->name;?></span>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div id='delivery-zone-wrapper'>
            <p class='bold middle-text'>Территория доставки <br>
                <span class='gray-colored extra-small-text not-bold'>(для печатных изданий)</span>
            </p>
            <div>
                <?php $defaultDeliveryZone = 4; //Россия?>
                <?php foreach($deliveryZones as $zone): ?>
                    <div class='radio-check-wrap'>
                        <input type='radio' name='order[delivery_zone_id]' value='<?=$zone->id;?>' id='qwerty_o_dzid_<?=$zone->id;?>' <?php if($zone->id == $defaultDeliveryZone): ?>checked='true'<?php endif;?>></input>
                        <label for='qwerty_o_dzid_<?=$zone->id;?>'>
                            <span><?=$zone->name;?></span>
                        </label>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div><!-- END delivery-options-block -->
    <?php if(!empty($editions)): ?>
    <div id='mags-list-block'>
        <p class='middle-text bold' style='text-align: center;'>Выбор изданий:</p>
        <ul>
            <!--is-selected = classname for selected items -->
            <?php foreach($editions as $edition): ?>
            <li class='avalable-mag-item transition-all' data-id='<?=$edition->Message_ID; ?>' title='добавить издание в заказ'> 
                <span><?=$edition->HumanizedName; ?></span>
                <div class='ajax-loading'></div>
                <span class='plus transition-all'></span>
            </li>
            <?php endforeach; ?>
        </ul>
    </div> <!-- END mags-list-block -->
    <?php endif; ?>
    <div id='selected-mags-block'>
        <ul class='flex-wrapper flex-multy-rows flex-dir-column'></ul>
    </div> <!-- END selected-mags-block -->
    <div id='user-data-block'>
        <p class="middle-text bold" style='border-bottom: 1px solid rgba(0,0,0,0.1); text-align: center;padding-bottom: 10px;'>Укажите данные, необходимые для доставки:</p>
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label class='required'>Фамилия</label>
                    <div class='error-message animated'></div>
                </div>
                <!--<input type='text' class='userData' name='order[person][f]' required='true'/>-->
                <input type='text' class='userData' name='f' required='true' data-contentType = 'string'/>
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label class='required'>Имя</label>
                    <div class='error-message animated'></div>								
                </div>
                <input type='text' class='userData' name='i' required='true' data-contentType = 'string'/>
                <input type='hidden' class='userData' name='name'/>
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label class='required'>Телефон</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='phone' required='true' data-contentType = 'phone'/>
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label class='required'>Email</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='email' required='true' data-contentType = 'email'/>
            </div>
        </div>
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Страна</label>
                    <div class='error-message animated'></div>
                </div>
                <?php 
                    $countries = common\models\Country::find()
                            ->select(['value' => 'Country_id', 'label' => 'Country_Name'])
                            ->orderBy('Country_Name')
                            ->where(['checked' => 1])
                            ->asArray(true)
                            ->all();
                    $countriesItems = [];
                    foreach( $countries as $c ) {
                        $countriesItems [$c['value']] = $c['label'];
                    }
                   echo( Html::dropDownList('country_id', common\models\Country::RUSSIA , $countriesItems, ['class' => 'userData'])  );
                ?>
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Почтовый индекс</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='zipcode' data-contentType = 'integer' />
                <!--<input type='hidden' class='userData' name='country_id'/>-->
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Область (регион)</label>
                    <div class='error-message animated'></div>
                </div>
                <?php 
                $regions = Yii::$app->cache->getOrSet('regionsListForDropDownList', function() {
                    $regions = common\models\Regions::find()
                            ->select(['label' => 'RegionUser_Name'])
//                            ->orderBy(['RegionUser_Name'])
                            ->where(['enabled' => 1])
                            ->asArray(true)
                            ->all();
                    $regionsList = [' ' => 'Выбор региона'];
                    foreach ( $regions as $r ) {
                        $regionsList[$r['label']] = $r['label'];
                    }
                    return $regionsList;
                 }, 300);
                 echo( Html::dropDownList('area', null, $regions, ['class' => 'userData']) );
                ?>
                <!-- 
                <select name='order[address][area]' class='userData'>
                    <option value='165' selected='true'>Россия</option>
                    <option value='1'>Австралия</option>
                    <option value='2'>Австрия</option>
                </select>
                -->
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Район</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='region'  data-contenttype='string' />
            </div>
        </div>
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column basis200'>
                <div>
                    <label>Город</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='city' data-contenttype='string' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column basis200'>
                <div>
                    <label>Входящий нас. пункт</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData'  name='city1' data-contenttype='string' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column basis200'>
                <div>
                    <label>Улица</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='street' data-contenttype='string' />
            </div>
        </div>
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Дом</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='house' data-contenttype='string' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Корпус</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='building' data-contenttype='integer' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Строение</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='structure' data-contenttype='integer' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column'>
                <div>
                    <label>Квартира</label>
                    <div class='error-message animated'></div>
                </div>
                <input type='text' class='userData' name='apartment' data-contenttype='string' />
            </div>
        </div>
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column '>
                <div>
                    <label>Промокод</label>
                    <div class='error-message animated'></div>
                </div>
                <input id='promo_code' type='text' name='order[promo]' />
                <input id='action_id' type='hidden' name='order[action_id]' value ='0' />
                <input id='price_id' type='hidden' name='order[price_id]' value ='0' />
            </div>
            <div class='user-data-field flex-wrapper flex-dir-column '>
                <div></div>
                <!-- 
                <input class='transition-all' id='check-promo' type='button' value='Проверить код'/>
                -->
            </div>
        </div>
        <div id='action-info' class='animated'></div><!--Информация про акцию -->
        <div class='user-data-row flex-wrapper'>
            <div class='user-data-field flex-wrapper flex-dir-column' style='flex-basis: 100%;'>
                <div>
                    <label>Тип подписчика</label>
                    <div class='error-message animated'></div>
                </div>
                <div id='subscriber-type-wrapper'>
                    <div class='radio-check-wrap'>
                        <input type='radio' name='order[subscriber_type_id]' value='1' checked='true' id='qwerty_o_sti_1'/>
                        <label for='qwerty_o_sti_1'>Физ. лицо</label>
                    </div>
                    <div class='radio-check-wrap'>
                        <input type='radio' name='order[subscriber_type_id]' value='2' id='qwerty_o_sti_2' />
                        <label for='qwerty_o_sti_2'>Юр. лицо</label>
                    </div>
                </div>
            </div>
        </div>
        <div id='company-data-block' class='animated'>
            <div class='user-data-row flex-wrapper'>
                <div class='user-data-field flex-wrapper flex-dir-column basis300'>
                    <div>
                        <label class='required'>Название компании</label>
                        <div class='error-message animated'></div>
                    </div>
                    <input type='text' class='companyData' name='company_name' required='true' data-contentType='string' />
                </div>
                <div class='user-data-field flex-wrapper flex-dir-column basis300'>
                    <div>
                        <label class='required'>Юридический адрес</label>
                        <div class='error-message animated'></div>
                    </div>
                    <input type='text' class='companyData' name='company_address' required='true' data-contentType='string' />
                </div>
            </div>
            <div class='user-data-row flex-wrapper'>
                <div class='user-data-field flex-wrapper flex-dir-column basis300'>
                    <div>
                        <label class='required'>ИНН</label>
                        <div class='error-message animated'></div>
                    </div>
                    <input type='text' class='companyData' name='inn' required='true' data-contentType='integer' />
                </div>
                <div class='user-data-field flex-wrapper flex-dir-column basis300'>
                    <div>
                        <label class='required'>КПП</label>
                        <div class='error-message animated'></div>
                    </div>
                    <input type='text' class='companyData' name='kpp' required='true' data-contentType='integer' />
                </div>
            </div>
        </div> <!-- END company-data-block -->
    </div><!-- END user-data-block-->
    <div id='user-agreement-block'>
        <div class='radio-check-wrap'>
            <input type='checkbox' id = 'osp_dimeo_subscription_user_agreement' class='user-agreement' value='1' checked='true' />
            <?php echo($publisher->buildPersonalPolicyBlock()); ?>
            <!--
            <label for='osp_dimeo_subscription_user_agreement'>
                Я прочел и соглашаюсь с
                <a target="_blank" href="https://www.osp.ru/subscription/terms/">
                    условиями пользовательского соглашения
                </a> и даю согласие на
                <a href="https://www.osp.ru/personalpolicy/">
                    обработку моих персональных данных
                </a>
            </label>
            -->
        </div>
    </div><!-- END  user-agreement-block -->
    <div id='total-sum-block'>
        <div class='flex-wrapper flex-dir-row' style='justify-content: left;flex-wrap: wrap;'>
            <div class='large-text bold' id='total-sum-text'>
                <div style='display: table;height: 100%;width: 100%;'>
                    <div style='    display: table-cell;vertical-align: bottom;text-align: left;text-decoration: underline;'>
                        Сумма заказа:
                    </div>
                </div>
            </div>
            <div class='flex-wrapper flex-dir-column' id='total-sum-values'>
                <div class='animated' style='display:none'>
                    <span id='total-sum-old'></span>
                    <!-- <span class='rub-mark'></span> -->
                </div>
                <div>
                    <span id='total-sum'>0</span>
                    <span class='rub-mark'>руб.</span>
                </div>
            </div>
            <div id='buttons-block'>
                <div style="display: table;width: 100%;height: 100%;">
                    <div style="display: table-cell;vertical-align: bottom;text-align: left;">
                        <button type='button' id='send-subscribe-form-button' class='transition-all'>Оформить заказ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='err-messages-list-block' class='animated'>
        <ul class='err-messages-list'></ul>
    </div>
    <div><span id='subscribe-sign'>Димео подписка</span></div>
</div>


