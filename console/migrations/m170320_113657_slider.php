<?php

use yii\db\Migration;

class m170320_113657_slider extends Migration
{
    public function up()
    {
        $mysqlCode = '
            CREATE TABLE IF NOT EXISTS `Sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL,
    `w` int(11) NOT NULL,
      `h` int(11) NOT NULL,
  `key` varchar(245) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

        ';
        $this->execute($mysqlCode);
    }

    public function down()
    {
        echo "m170320_113657_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
