<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
namespace backend\views\widgets\ace_code_editor;

use yii\base\Widget;
use yii\helpers\Html;

class AceCodeEditor extends Widget
{
//    public $model;
    
    public $content = '';
    public $codeWrapperOptions = [];
//    public $lastUpdatedFieldName = 'last_updated';
//    public $createUserFieldName = 'create_user_id';
//    public $lastUserFieldName = 'last_user_id';

    // public function init()
    // {
    //     parent::init();
    // }
    
    // Ожидается наличие у модели методов доступа к связанной модели User с именами getCreateUser (доступ $model->createUser) и getLastUser() ($model->lastUser)
    // 

    public function run()
    {
        $data = [
            'content' => $this->content,
            'codeWrapperOptions' => $this->codeWrapperOptions, 
        ];
        return $this->render('view', $data );
    }

}