<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    // require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
    // require(__DIR__ . '/params-local.php')
);

if (file_exists( __DIR__ . '/../../common/config/params-local.php' )) {
    $params = array_merge(
        $params,
        require(__DIR__ . '/../../common/config/params-local.php')
    );
}

if (file_exists( __DIR__ . '/params-local.php' )) {
    $params = array_merge(
        $params,
        require(__DIR__ . '/params-local.php')
    );
}


return [
    'id' => 'admin',
    'name' => '', //"Издательство"
    'basePath' => dirname(__DIR__),
    // uncomment and edit this section if you need alternative mapping controller to classFiles
    // 'controllerMap' => [
    //     'controllerId' => 'path\to\Controller\class\fileController',
    //     'anotherControllerId' => [
    //         'class' => 'path\to\Controller\class\fileController',
    //         'enableCsrfValidation' => false,
    //     ],
    // ],
    'controllerNamespace' => 'backend\controllers',
    // 'aliases' => [
    //     '@name1' => 'path/to/path1',
    //     '@name2' => 'path/to/path2',    
    // ],
    'bootstrap' => ['log','queue'],
//    'bootstrap' => ['log'],

    //remove comment marks if you need all incoming requests should be catched by some action
    // 'catchAll' => [
    //     'offline/notice',
    //     'param1' => 'value1',
    //     'param2' => 'value2',
    // ],
    'language' => 'ru',
    'defaultRoute' => 'site/index',
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['172.18.0.29', '172.18.0.1']
//            'generators' => [
//                'email-templates' => [
//                    'class' => \ymaker\email\templates\gii\Generator::class,
//                    'templates' => [
//                        'default' => '@vendor/yiimaker/yii2-email-templates/src/gii/default',
//                    ],
//                ],
//            ],
        ],
        'migration_utility' => [
                'class' => 'c006\utility\migration\Module',
        ],
        'adm' => [
            'class' => 'backend\modules\admin\Module',
        ],
    ],
    'components' => [
//        'queue' => [
//            'class' => \yii\queue\db\Queue::class,
//            'db' => 'db', // Компонент подключения к БД или его конфиг
//            'tableName' => '{{%queue}}', // Имя таблицы
//            'channel' => 'default', // Выбранный для очереди канал
//            'mutex' => \yii\mutex\MysqlMutex::class, // Мьютекс для синхронизации запросов
//        ],
        'request' => [
            'cookieValidationKey' => '49yZZvUwiMajinduDht6N2FOJuQ07ddN',
            'csrfParam' => '_csrf-backend',
            'ipHeaders' => ['X-Forwarded-For','X-Real-Ip'],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_publisher_admin_user', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'publisher_admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'class' => 'common\custom_components\err_handlers\MyErrorHandler', //Кастомный класс 
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
//            'baseUrl' => 'http://example.com/',
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                'company/getinfo/<id:\d+>' => 'company/getinfo',
//                'person/getinfo/<id:\d+>' => 'person/getinfo',
                '<controller:company|person|edition>/<action:getinfo>/<id:\d+>' => '<controller>/<action>',
                '<controller:download>/issuefile/<filehash:.+>' => '<controller>/issue-file',
                '<controller:issue>/<action:copy>/<type:to-next-month|to-next-year>/<id:\d+>' => '<controller>/<action>',
                '<controller:issue>/<action:[\w|_|\-]+>/<id:\d+>' => '<controller>/<action>',

                '<module:\w+>/<controller:company|person>/<action:getinfo>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:email-notice-template>/<action:get-template-html>/<templateId:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:email-notice-template>/<action:get-template>/<templateId:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:email-notice-template>/<action:test-template>/<noticeId:\d+>' => '<module>/<controller>/<action>',
                
                '<controller:email-notice>/<action:get-template-html>/<templateId:\d+>' => '<controller>/<action>',
                '<controller:email-notice>/<action:get-template>/<templateId:\d+>' => '<controller>/<action>',
                '<controller:email-notice>/<action:email-log|get-email-message>/<emailId:\d+>' => '<controller>/<action>',
                '<controller:email-notice>/<action:resend-logged-email>/<emailId:\d+>/<recipient:currentUser|same>' => '<controller>/<action>',
            ],
        ],
        /*
        */
    ],
    'params' => $params,
];
