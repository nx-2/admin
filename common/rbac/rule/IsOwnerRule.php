<?php

namespace common\rbac\rule;

use common\models\User;

class IsOwnerRule extends \yii\rbac\Rule
{
    public $name = 'isOwner';

    /**
     * @inheritDoc
     */
    public function execute($user, $item, $params = []): bool
    {
        $model = $params['model'] ?? false;

        if (!$model) {
            return true;
        }
        if ($model instanceof User) {
            return  intval($model->User_ID) === intval($user);
        }
        if (property_exists($model, 'create_user_id')) {
            return intval($user) === intval($model->create_user_id);
        }
        return false;
    }
}