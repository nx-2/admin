<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace console\controllers;

use common\models\events\CustomIssueEvent;
use common\models\Issue;
use common\services\PdfLogService;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class IssueController extends Controller
{
    public function actionSendNotShippedPdf($daysBeforeNow = 7)
    {
        $notShippedIssueIds = Issue::getLastNotShippedPdfIssuesIds($daysBeforeNow);
        $issues = Issue::findAll(['Message_ID' => $notShippedIssueIds]);
        $event = new CustomIssueEvent();
        foreach ($issues as $issue) {
            //TODO: Сделать проверку существования загруженного PDF

//            $pe = $issue->magazine->ownerid;
//            $publisherId = $pe->publisher_id;
            $publisherId = $issue->magazine->ownerid->publisher_id;
            $issue->attachBehavior('uploadFile', [
                'class' => '\yiidreamteam\upload\FileUploadBehavior',
                'attribute' => 'issue_file',
                'filePath' => Yii::$app->params['issueFiles']['path'] . $publisherId . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
                'fileUrl' => '/download/issuefile/' . $publisherId . '/[[pk]]' . '_' . '[[filename]].[[extension]]',
            ]);


            if ($issue->isPdfLoaded()) {
                $event->issue = $issue;
                $issue->trigger($issue::FOUND_NOT_SHIPPED_PDF, $event);
                $this->stdout($issue->issue_title . ' of "' . $issue->magazine->HumanizedName . '" will be send to all concerned.' . "\n", Console::FG_CYAN, Console::BOLD);
            } else {
                $this->stdout($issue->getUploadedFilePath('issue_file') . ' was not found' . "\n", Console::FG_CYAN, Console::BOLD);
            }
        }

        $this->stdout('DONE' . "\n", Console::FG_CYAN, Console::BOLD);

        return ExitCode::OK;
    }

    public function actionGetUrlForPdf($orderId, $issueId)
    {
        $issue = Issue::findOne(['Message_ID' => intval($issueId)]);
        if (empty($issue)) {
            return '';
        }
        $this->stdout($issue->buildDownloadUrl($orderId) . "\n", Console::FG_PURPLE);
        return ExitCode::OK;
    }

    public function actionCreatePdfLogsForAllIssues()
    {
        $pdfLogService = new PdfLogService();
        $queryString = "SELECT i.Message_ID AS id FROM Issue i WHERE i.status_id = 2 AND i.issue_file <> '' AND i.issue_file IS NOT NULL";
        $issuesIds = Yii::$app->getDb()->createCommand($queryString)->queryAll();
        $processedCount = 0;
        foreach ($issuesIds as $issueId) {
            $pdfLogService->updateLogsForIssue(intval($issueId['id']));
            $processedCount++;
        }
        $this->stdout('Processed ' . $processedCount . ' issues.' . "\n", Console::FG_PURPLE);
        return ExitCode::OK;
    }
}


