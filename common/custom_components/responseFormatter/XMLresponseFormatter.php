<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\custom_components\responseFormatter;

use DOMDocument;
use DOMElement;
use DOMText;
use DOMException;

/**
 * Description of XMPresponseFormatter
 *
 * @author andrey
 */
class XMLresponseFormatter extends \yii\web\XmlResponseFormatter {
    public $rootTagAttributes = [];
    
    
    public function format($response) {
        $charset = $this->encoding === null ? $response->charset : $this->encoding;
        if (stripos($this->contentType, 'charset') === false) {
            $this->contentType .= '; charset=' . $charset;
        }
        $response->getHeaders()->set('Content-Type', $this->contentType);
        if ($response->data !== null) {
            $dom = new DOMDocument($this->version, $charset);
            if (!empty($this->rootTag)) {
                $root = new DOMElement($this->rootTag);
                $dom->appendChild($root);
                foreach($this->rootTagAttributes as $attrName => $attrValue) {
                    $root->setAttribute($attrName, $attrValue);
                }
                $this->buildXml($root, $response->data);
            } else {
                $this->buildXml($dom, $response->data);
            }
            $response->content = $dom->saveXML();
        }
    }
}
