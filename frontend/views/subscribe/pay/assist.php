<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
    <img src='http://pay.osp.ru/img/logo/visa-mc.png'><br>
    <span class='db'>Visa, MasterCard</span>

<?php

$params     = "&InvId=".$transaction_id."&key=".md5($transaction_id.":".$paySystems->shop_name.":key");
?>
<form action="https://payments222.paysecure.ru/pay/order.cfm" method="post">
    <input type="hidden" name="system" value="assist">
    <input type="hidden" name="Merchant_ID" value="<?=$paySystems->shop_name?>">
    <input type="hidden" name="OrderNumber" value="<?=$transaction_id?>">
    <input type="hidden" name="OrderAmount" value="<?=$model->price?>">
    <input type="hidden" name="Delay" value="0">
    <input type="hidden" name="Language" value="RU">
    <input type="hidden" name="CardPayment" value="1">
    <input type="hidden" name="URL_RETURN_OK" value="http://pay.osp.ru/result.php?from=assist&page=success<?=$params?>">
    <input type="hidden" name="URL_RETURN_NO" value="http://pay.osp.ru/result.php?from=assist&page=success<?=$params?>">
    <input type="hidden" name="OrderCurrency" value="RUR">
    <input type="hidden" name="OrderComment" value="описание">
    <input type="hidden" name="FirstName" value="<?=$person->name?>">
    <input type="hidden" name="LastName" value="">
    <input type="hidden" name="Email" value="<?=$person->email?>">
    <input type="hidden" name="OrderMaxPoints" value="200">
    <input type="hidden" name="TestMode" value="1">
    <input type="submit">
</form>
