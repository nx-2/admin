<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\queue_jobs;

use common\custom_components\CustomHelpers;
use common\models\DeliveryType;
use common\models\MagazineSubscribesQueue;
use common\models\OrderItemRelease;
use common\models\Shipment;
use common\models\ShipmentChanel;
use common\models\ShipmentItem;
use common\services\PdfLogService;
use Exception;
use Yii;
use yii\db\Expression;

class SendPdfIssueJob extends EmailNotificationJob
{
    public function execute($queue)
    {
        try {

            $this->convertIdsToEntities();

            $orderId = isset($this->data['order_id']) ? $this->data['order_id'] : null;
            $issueId = isset($this->data['issue_id']) ? $this->data['issue_id'] : null;
            $orderItemRelease = $this->getOrderItemRelease($orderId, $issueId);

            CustomHelpers::configureMailerComponent($this->publisherId);

            $messageBody = Yii::$app->mailer->render($this->templateName, $this->data, 'layouts/html'); //Изврат т.к. объект сообщения, кот. возвр. compose НЕ содержит возможностей получить из него HTML
            $message = Yii::$app->mailer->compose($this->templateName, $this->data);
            $message->setFrom($this->fromEmail)
                ->setTo($this->toEmail)
                ->setSubject($this->subject);
            if ($message->send()) {
                $transaction = Yii::$app->getDb()->beginTransaction();

                $shipment = $this->createShipment();
                if (empty($shipment)) {
                    $transaction->rollBack();
                    return;
                }

                $this->data['shipment_id'] = $shipment->id;

                $shipmentItem = $this->createShipmentItem($shipment, intval($orderItemRelease->id));
                if (!$shipmentItem->save()) {
                    $transaction->rollBack();
                    return;
                }

                $this->data['shipment_item_id'] = $shipmentItem->id;

                $orderItemRelease->shipment_id = $shipment->id;
                $orderItemRelease->ship_count = 1;
                if (!$orderItemRelease->save()) {
                    $transaction->rollBack();
                    return;
                }
                $pdfSendLogItem = $this->getPdfLogItem($orderId, $issueId);
                $pdfSendLogItem->updateAttributes(['send' => 1, 'sendTime' => date('Y-m-d H:i:s')]);

                $transaction->commit();

                $this->logEmail($message, $messageBody, true, 'Отправка PDF выпуска ' . $issueId);

            } else {
                $this->logEmail($message, $messageBody, false, 'Сбой отправки PDF выпуска ' . $issueId);
                //TODO: обновить orderItemRelease соответствующий
                $orderItemRelease->updateAttributes(['shipment_id' => null, 'ship_count' => 0]);
            }


        } catch (Exception $ex) {
            $this->logEmail($ex, false, false, 'Exception при попытке отправить Email');
            print_r($ex);
            $orderItemRelease->updateAttributes(['shipment_id' => null, 'ship_count' => 0]);
        }
        return;
    }

    private function getPdfLogItem(int $orderId, int $issueId): MagazineSubscribesQueue
    {
        $queryStr = <<<SQL
                    SELECT msq.*
                    FROM MagazineSubscribes ms
                    LEFT JOIN MagazineSubscribesQueue msq ON ms.id = msq.subscribeID
                    WHERE  ms.issueID = $issueId AND msq.subscriptionID = $orderId
SQL;
        $pdfSendLog = MagazineSubscribesQueue::findBySql($queryStr)->one();
        if (empty($pdfSendLog)) {
            $pdfLogService = new PdfLogService();
            $pdfLogService->updateLogsForOrder($orderId);
            $pdfSendLog = MagazineSubscribesQueue::findBySql($queryStr)->one();
        }
        return $pdfSendLog;
    }

    private function getOrderItemRelease($orderId = null, $issueId = null)
    {
        if (empty($orderId) || empty($issueId))
            return null;

        $queryString = "SELECT oir.*
                        FROM nx_order_item_release oir
                        LEFT JOIN nx_order_item oi ON oi.id=oir.item_id
                        LEFT JOIN nx_order o ON o.id = oi.order_id
                        LEFT JOIN Issue i ON i.Message_ID = oir.release_id
                        WHERE i.Message_ID = :issue_id
                        AND oi.delivery_type_id = :pdf_delivery_id
                        AND o.id = :order_id";
        return OrderItemRelease::findBySql($queryString, [':issue_id' => $issueId, ':pdf_delivery_id' => DeliveryType::PDF_TYPE, ':order_id' => $orderId])->one();
    }
    /**
     * @return Shipment | NULL
     */
    private function createShipment()
    {
        $shipment = $this->getShipment();
        if (empty($shipment)) {
            $shipment = new Shipment();
            $currentUTC = gmdate("Y-m-d H:i:s");
            $shipment->channel_id = ShipmentChanel::CHANNEL_PDF;
            $shipment->date = $currentUTC;
            $shipment->created = $currentUTC;
            if (!$shipment->save()) {
                return NULL;
            }
        }
        return $shipment;
    }

    private function createShipmentItem(Shipment $shipment, int $orderItemReleaseId): ShipmentItem
    {
        $shipmentItem = new ShipmentItem();
        $shipmentItem->shipment_id = $shipment->id;
        $shipmentItem->item_type = 1;
        $shipmentItem->item_id = $orderItemReleaseId;
        return $shipmentItem;
    }
    /**
     * @return Shipment | NULL
     */
    private function getShipment()
    {
        $from = gmdate("Y-m-d 00:00:00");
        $to = gmdate("Y-m-d 23:59:59");

        $query = Shipment::find()
            ->where(['BETWEEN', 'date', $from, $to])
            ->andWhere(['channel_id' => ShipmentChanel::CHANNEL_PDF])
            ->orderBy('date DESC');

//        $sqlStr = $query->createCommand()->getRawSql();
        return $query->one();
    }
}