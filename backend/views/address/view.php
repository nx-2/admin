<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NxAddress */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Nx Addresses', 'url' => ['address/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nx-address-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'country_id',
            'area',
            'region',
            'city',
            'city1',
            'zipcode',
            'address',
            'street',
            'house',
            'building',
            'structure',
            'apartment',
            'phone',
            'fax',
            'comment:ntext',
            'telcode',
            'post_box',
            'is_onclaim',
            'is_enveloped',
            'last_updated',
            'label',
            'xpress_id',
        ],
    ]) ?>

</div>
