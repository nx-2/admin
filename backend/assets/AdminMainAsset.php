<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AdminMainAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets-data/AdminMain';
    // public $basePath = '@webroot';
    // public $baseUrl = '@web';
    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
        'dist/css/AdminLTE.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/_all.css',
        'plugins/iCheck/minimal/_all.css',
        // 'plugins/morris/morris.css',
//        'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
       // 'plugins/daterangepicker/daterangepicker.css',
//        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
       // 'plugins/datepicker/datepicker3.css',
        'css/main.css',
        // 'plugins/select2/select2.min.css'
    ];
    public $js = [
        // 'plugins/jQuery/jquery-2.2.3.min.js',
//        'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js',
        'bootstrap/js/bootstrap.min.js',
        'bootstrap/js/bootbox.min.js',
        'plugins/bootstrap-confirmation/bootstrap-confirmation.min.js',

//        'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
//        'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
//        'plugins/knob/jquery.knob.js',

       // 'plugins/daterangepicker/daterangepicker.js',
        //'plugins/datepicker/bootstrap-datepicker.js',
        // 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',

//        'plugins/slimScroll/jquery.slimscroll.min.js',
//        'plugins/fastclick/fastclick.js',
        'dist/js/app.min.js',

//        'dist/js/demo.js',
        'js/main.js',

        'plugins/input-mask/jquery.inputmask.js',
        'plugins/input-mask/jquery.inputmask.date.extensions.js',
        'plugins/input-mask/jquery.inputmask.extensions.js',

        'https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
