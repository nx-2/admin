<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
 
use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AceCodeEditorAsset;
 ?>

<div class='ace-code-editor-wrapper' style='height:0px;'>
    <div class='codeEditorOptions' >
        <ul >
            <li><span class='lbl'>Шрифт</span><input type='text' value='12' class='ace-editor-font-size' onchange='aceCodeEditorOptionChange(event)'><span class='lbl'>px</span></li>
            <li><span class='lbl'>Линии отступов</span><input type='checkbox' checked='true' class='ace-editor-show-indent-lines' onchange='aceCodeEditorOptionChange(event)' /></li>
            <li style='max-width: 570px;'>
                <span class='lbl'>Цветовая схема:</span>
                <div style='display: inline-block; line-height: 20px;vertical-align: top;max-width: 450px;height: 100%;white-space: normal;'>
                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/solarized_light' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Solarized Light
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/chrome' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Chrome
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/dawn' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Dawn
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/clouds_midnight' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Clouds Midnight
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/monokai' class='ace-editor-color-scheme' checked />
                    <span class='lbl item'>
                        Monokai
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/solarized_dark' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Solarized Dark
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/idle_fingers' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        idle Fingers
                    </span>

                    <input type='radio' name='ace-code-editor-color-theme' onchange='aceCodeEditorOptionChange(event)' value='ace/theme/pastel_on_dark' class='ace-editor-color-scheme'/>
                    <span class='lbl item'>
                        Pastel on dark
                    </span>
                </div>
            </li>
            <li><span class='lbl'>На весь экран?</span><input type='checkbox' class='ace-editor-full-size' onchange='aceCodeEditorOptionChange(event)' /></li>
        </ul>
    </div>
    <?php 
        $codeWrapperOptions ['class'] = isset($codeWrapperOptions ['class']) ? $codeWrapperOptions ['class'] . ' ace-code-editor' : 'ace-code-editor';
        echo(Html::tag('div', htmlspecialchars($content), $codeWrapperOptions));
    ?>
</div>

<?php 
    AceCodeEditorAsset::register($this);
?>

