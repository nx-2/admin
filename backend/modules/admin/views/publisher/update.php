<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Publisher */

$this->title = 'Update Publisher: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Publishers', 'url' => ['publisher/list']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['publisher/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="publisher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
