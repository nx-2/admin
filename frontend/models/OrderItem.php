<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\models;
use Yii;
/**
 *
 * @property integer $edition_id
 * @property integer $index
 * @property float $discount
 * @property integer $duration
 * @property boolean $is_pdf
 * @property integer $issuesCount
 * @property float $price
 * @property string $start_date
 * @property float $sum
 */

class OrderItem extends \yii\base\Model
{
    public $edition_id;
    public $index;
    public $discount;
    public $duration;
    public $is_pdf;
    public $issuesCount;
    public $price;
    public $start_date;
    public $sum;
    
    public function rules() {
        return [
            [['edition_id', 'start_date', 'duration','discount', 'price'], 'required'],
            [['edition_id', 'index','duration','issuesCount'],'integer'],
            [['discount', 'sum','price'],'number'],
            [['start_date'],'date', 'format' => 'php:Y-m-d'],
            [['is_pdf'],'boolean'],
        ];
    }
    public function attributeLabels() {
        return [
            'edition_id' => 'ID издателя',
            'index' => 'Номер в заказе',
            'discount' => 'скидка в %',
            'duration' => 'период подписки',
            'is_pdf' => 'ПДФ',
            'issuesCount' => 'Колич. номеров',
            'price' => 'цена номера',
            'start_date' => 'начало подписки',
            'sum' => 'Сумма',
        ];
    }
    
    
}
