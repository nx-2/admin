<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace frontend\controllers;
use Yii;
use \yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\models\AdvertsPlaces;



class AdvertsController extends ActiveController
{
    public $modelClass = 'frontend\models\AdvertsPlaces';

    public function actionIndex()
    {
        $query = AdvertsPlaces::find();
        $query->joinWith(['magazine']);
        $query->joinWith(['code']);

		return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    public function actionForMagPlace()
    {
    	$magId = Yii::$app->request->getQueryParam('mag_id', false);
    	$placeId = Yii::$app->request->getQueryParam('id', false);

        $query = AdvertsPlaces::find()
        	->where(['ads_places.magazine_id' => $magId, 'ads_places.id' => $placeId, 'ads_places.enabled' => 1] )
        	// ->joinWith(['magazine'])
        	->joinWith(['code']);
        
        $result = $query->one();

        if(empty($result)) {
            throw new NotFoundHttpException("Object not found. Adverts place with magID=$magId and placeID=$placeId does not exist.");
        } else {
            if ($result->code->enabled != 1) {
                $result->code->head_code_text = '';
                $result->code->code_text = '';
            }
            return $result;
        }
    }
    public function actionForMag()
    {
        $magId = Yii::$app->request->getQueryParam('mag_id', false);

        $query = AdvertsPlaces::find()
            ->where(['ads_places.magazine_id' => $magId])
            // ->joinWith(['magazine'])
            ->joinWith(['code']);
        
        $result = $query->all();

        if(empty($result))
            throw new NotFoundHttpException("Objects not found. Adverts places for magID=$magId does not exist.");
        else 
            return $result;
    }
    public function actions()
    {
        $a = parent::actions();
        unset($a['index']);
        return $a;
    }

}
