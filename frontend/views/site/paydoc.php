<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
?>
<script language="javascript" type="text/javascript">
    (function ($) {
        var hash='<?=$_GET['hash'];?>';
        $.ajax({
            dataType: 'json',
            url: '/subscribe/payDoc/'+hash+'?_format=json',
            success: function (jsondata) {
                $('#form').html(jsondata);
            }
        });

    })(jQuery);
</script>


<div id="form"></div>