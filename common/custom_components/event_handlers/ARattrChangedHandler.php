<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\custom_components\event_handlers;

use common\models\NxOrder;
use common\models\NxRole;
use common\models\Publisher;
use common\models\User;
use common\services\RbacService;
use Yii;
use \common\queue_jobs\AfterYaKassaApiSucceededJob;
use yii\base\ErrorException;
use yii\base\ExitException;
use yii\web\Application;
use yii\queue\Queue;


class ARattrChangedHandler extends \yii\base\BaseObject
{

    protected static $data = [];

    public static $eventsMap = [
//       имя События      => email_notice->key_string
        'userDataChanged' => 'userDataChanged',
        'roles_Changed' => 'userRolesChanged',
        'passwordChanged' => 'userPasswordChanged',
        'newUserCreated' => 'newUserCreated',
        'orderHasPaid' => 'orderHasPaid',
    ];


    public static function userRolesChanged($event)
    {
        if (!($event->sender instanceof \common\models\User) || !($event instanceof \common\models\events\ARrelatedModelChangedEvent))
            return;

        //Здесь на вход мы получаем екземпляр события \common\models\events\ARrelatedModelChangedEvent
        //внутри которого хранится имя связи (relationName) и массив моделей, представляющих собой СТАРЫЙ (до сохранения) набор
        //связанных записей $oldRelatedModels
        //по имени связи мы можем получить новый набор как $event->sender->{$event->relationName}
        // и далее формировать текст письма на основе имеющейся информации. Следует иметь в виду, что $event->relationName
        // это НЕ имя атрибута модели, поэтому получить attributeLabel по нему может оказаться невозможным

        /** @var User $user */
        /** @var Publisher $publisher */
        $user = $event->sender;
        $publisher = $user->publisher;

        $rbacService = new RbacService();
        $rbacService->updateUserRoles($user);

        $noticeKeyString = isset(self::$eventsMap[$event->name]) ? self::$eventsMap[$event->name] : $event->name;
        $emailNoticeType = \common\models\EmailNotice::findOne(['key_string' => $noticeKeyString]);
        if ($emailNoticeType == null) {
//            throw new yii\base\ErrorException('Email notice type <' . $noticeKeyString . '> not found');
            return;
        }
        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();
        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }

        if (empty($emailNoticeTemplate)) {
            return;
        }

        $newRolesNames = [];

        foreach ($event->sender->{$event->relationName} as $newRole) {
            /** @var NxRole $newRole */
            $newRolesNames [] = $newRole->name;
        }
        $newRolesNames = implode(',', $newRolesNames);
        $oldRolesNames = [];
        foreach ($event->oldRelatedModels as $oldRole) {
            $oldRolesNames [] = $oldRole->name;
        }
        $oldRolesNames = implode(',', $oldRolesNames);
        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
//                'user' => $user,
                'user_id' => $user->User_ID,
                'oldRoles' => $oldRolesNames,
                'newRoles' => $newRolesNames,
//                'publisher' => $publisher,
                'publisher_id' => $publisher->id,
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $user->Email,
        ];

        self::sendEmail($emailJobData, intval($publisher->id));
        return;
    }

    public static function newOrderCreated($event = false)
    {
        // $event = instance of \common\models\events\ARsingleAttrChangedEvent
        // доступные атрибуты события $model (Впрочем $event->sender даст ровно тоже самое)

        if (!$event)
            throw new yii\base\ErrorException('event object required!!');

        if (!$event->model || empty($event->model) || null == $event->model)
            throw new yii\base\ErrorException('an empty object was supplied or object was not supplied at all, while an instatnce of NxOrder object is expected.');

        if (!($event->model instanceof \common\models\NxOrder))
            throw new yii\base\ErrorException('Order model object is expected, but ' . get_class($event->model) . ' was supplied');

        $order = $event->model;
        $publisher = $order->publisher;

        $emailNoticeType = self::getNoticeTypeOnEventName($event->name);

        if (empty($emailNoticeType)) {
            return false;
        }

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();
        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }
        if (empty($emailNoticeTemplate)) {
            $message = 'Email notice template for <' . $emailNoticeType->name . '>  and publisher <' . $publisher->id . '> does not exist';
            Yii::$app->session->addFlash('danger', $message);
            return false;
//            throw new yii\base\ErrorException('Email notice template for <' . $emailNoticeType->name . '>  and publisher <' . $publisher->id . '> does not exist');
        }

        $toEmail = ($order->email != '') ? $order->email : ($order->person->email != '') ? $order->person->email : false;

        if (!$toEmail)
            return false;

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
//                'order' => $order->id, //сами объекты передавать в очередь не будем, только их ИД. Пускай консольное приложение, обслуживающее очередь их пересоздаст
                'order_id' => $order->id, //сами объекты передавать в очередь не будем, только их ИД. Пускай консольное приложение, обслуживающее очередь их пересоздаст
                'publisher_id' => $publisher->id, //TODO рассмотреть возможность восстановления AR объектов иp их ИД в классе EmailNotificationJob а НЕ в шаблоне письма, как сейчас
//                'publisher' => $publisher->id, //TODO рассмотреть возможность восстановления AR объектов иp их ИД в классе EmailNotificationJob а НЕ в шаблоне письма, как сейчас
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $toEmail,
        ];
        self::sendEmail($emailJobData, intval($publisher->id));

    }

    /**
     * обработчик события модели NxOrder "orderHasPaid". Это событие происходит после того, как все необходимые статусы всех моделей
     * уже изменены. Сам заказ тоже получил статус "оплачен". Обработчик должен только уведомить email-ом клиента.
     */
    public static function orderPaid($event = false)
    {
        // $event = instance of         \common\models\events\ARattrChangedToValueEvent
        // доступные атрибуты события  = $attrsUnderWatch (Список наблюдаемых атрибутов. В данном случае будет ['order_state_id'])

        if (!$event)
            throw new yii\base\ErrorException('event object required!!');
        /** @var NxOrder $order */
        $order = $event->sender;
        if (!($order instanceof \common\models\NxOrder))
            throw new yii\base\ErrorException('Order model object is expected, but ' . get_class($order) . ' was supplied');

        $publisher = $order->publisher;

        $emailNoticeType = self::getNoticeTypeOnEventName($event->name);

        if (empty($emailNoticeType)) {
            return false;
        }

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();
        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }
        if (empty($emailNoticeTemplate)) {
            $message = 'Шаблон письма для <' . $emailNoticeType->name . '>  и издателя <' . $publisher->name . '> не найден. Оповещение про оплату заказа не отправлено';
            Yii::$app->session->addFlash('danger', $message);
            return false;
        }
        $toEmail = ($order->email != '') ? $order->email : ($order->person->email != '') ? $order->person->email : false;

        if (!$toEmail) {
            Yii::$app->session->addFlash('danger', 'Заказ ' .$order->id . ' Оплачен. Но Email не найден ни в заказе ни в данных о физическом лице. Оповещение про оплату не отправлено');
            return false;
        }

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $paymentId = !empty($order->payment) ? $order->payment->id : NULL;

        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
                'order_id' => $order->id, //сами объекты передавать в очередь не будем, только их ИД. Пускай консольное приложение, обслуживающее очередь их пересоздаст
                'publisher_id' => $publisher->id, //TODO рассмотреть возможность восстановления AR объектов из их ИД в классе EmailNotificationJob а НЕ в шаблоне письма, как сейчас
//                'order' => $order->id, //сами объекты передавать в очередь не будем, только их ИД. Пускай консольное приложение, обслуживающее очередь их пересоздаст
//                'publisher' => $publisher->id, //TODO рассмотреть возможность восстановления AR объектов из их ИД в классе EmailNotificationJob а НЕ в шаблоне письма, как сейчас
                'payment_id' => $paymentId,
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $toEmail,
        ];
        self::sendEmail($emailJobData, intval($publisher->id));
    }

    /**
     * Обработчик события "yaApiPaymentObtained" модели ya_kassa_api_payment
     * это событие должно генериться в момент, когда аттрибут lastStatus модели принимает значение succeeded
     * Обработчик должен позаботиться о том, чтобы все связанные с заказом модели и сам заказ тоже получили корректные статусы,
     * оповещающие, что заказ оплачен, а также создать при необходимости новую запись в nx_payment
     */
    public static function yaApiPaymentObtained($event)
    {
        $yaKassaPaymentModel = $event->sender;
        if (empty($yaKassaPaymentModel) || !$yaKassaPaymentModel instanceof \common\models\YaKassaApiPayment)
            throw new yii\base\ErrorException('instance of YaKassaApiPayment model is required!!');

        $job = new AfterYaKassaApiSucceededJob($yaKassaPaymentModel->id);
        $queue = \Yii::$app->get('queue');
        $queue->push($job);
//        $job->execute($queue);
    }

    /**
     * @throws ErrorException
     * @throws ExitException
     */
    public static function newUserCreated($event = false)
    {
        // $event = instance of \common\models\events\ARsingleAttrChangedEvent
        // доступные атрибуты события $model (Впрочем $event->sender даст ровео тоже самое)

        if (!$event){
//            throw new yii\base\ErrorException('event object required!!');
            throw new ErrorException('event object required!!');
        }

        if (!$event->model || empty($event->model)) {
//            throw new ErrorException('an empty object was supplied or object was not supplied at all, while an instatnce of User object is expected.');
            return;
        }

        if (!($event->model instanceof \common\models\User)) {
//            throw new ErrorException('User model object is expected, but ' . get_class($event->model) . ' was supplied');
            return;
        }

        /** @var User $user */
        $user = $event->model;
        $publisher = $user->publisher;

        $rbacService = new RbacService();
        $rbacService->updateUserRoles($user);

        $emailNoticeType = self::getNoticeTypeOnEventName($event->name);

        if (empty($emailNoticeType)) {
            return;
        }

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();

        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }

        if (!empty($emailNoticeTemplate)) {

            $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
            $emailJobData = [
                'templateName' => $emailNoticeTemplate->template_file,
                'data' => [
//                    'user' => $user,
//                    'publisher' => $publisher,
                    'user_id' => $user->User_ID,
                    'publisher_id' => $publisher->id,
                ],
                'subject' => $emailNoticeType->email_subject,
                'fromEmail' => $senderAddr,
                'toEmail' => $user->Email,
            ];
            self::sendEmail($emailJobData, $publisher->id);
        } else {
            $messageStr = 'Шаблон Email-оповещения для ' . $emailNoticeType->name . ' и издателя ' . $publisher->name . ' не найден. Оповещение не отправлено. См. раздел "Email оповещения" админ панели';
            Yii::$app->session->addFlash('danger', $messageStr);
        }
    }

    /**
     * @param bool $eventName
     * @return bool|\common\models\EmailNotice|null
     */
    private static function getNoticeTypeOnEventName($eventName = false)
    {
        if (!$eventName)
            return false;

        $noticeKeyString = isset(self::$eventsMap[$eventName]) ? self::$eventsMap[$eventName] : $eventName;
        $emailNoticeType = \common\models\EmailNotice::findOne(['key_string' => $noticeKeyString]);
        if (empty($emailNoticeType)) {
            $messageStr = 'Событию  ' . $eventName . ' не присвоен тип Email-оповещения. Обратитесь к администратору';
            Yii::$app->session->addFlash('danger', $messageStr);
        }
        return $emailNoticeType;
    }

    public static function userPasswordChanged($event)
    {
        // $event = instance of \common\models\events\ARsingleAttrChangedEvent
        // доступные атрибуты события $attrName, $oldValue, $newValue

        if (!($event->sender instanceof \common\models\User))
            return false;

        $user = $event->sender;
        $publisher = $user->publisher;
        $emailNoticeType = self::getNoticeTypeOnEventName($event->name);

        if (empty($emailNoticeType)) {
            return false;
        }

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();

        if(empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }

        if ($emailNoticeTemplate == null) {
            $messageStr = 'Шаблон Email-оповещения для ' . $emailNoticeType->name . ' и издателя ' . $publisher->name . ' не найден. Оповещение про смену пароля не отправлено. См. раздел "Email оповещения" админ панели';
            Yii::$app->session->addFlash('danger', $messageStr);
            return false;
//            throw new yii\base\ErrorException('Email notice template for <' . $emailNoticeType->name . '>  and publisher <' . $publisher->id . '> does not exist');
        }

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
                'user' => $user,
                'publisher' => $user->publisher,
                'newPassword' => $event->newValue,
                'oldPassword' => $event->oldValue,
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $user->Email,
        ];
        self::sendEmail($emailJobData, intval($publisher->id));
    }

    public static function userDataChanged($event)
    {
        if (!($event->sender instanceof \common\models\User))
            return;

        $user = $event->sender;
        $publisher = $user->publisher;

        $emailNoticeType = self::getNoticeTypeOnEventName($event->name);

        $emailNoticeTemplate = $emailNoticeType->getPublisherTemplate($publisher->id)->one();

        if (empty($emailNoticeTemplate)) {
            $emailNoticeTemplate = $emailNoticeType->defaultTemplate;
        }

        if ($emailNoticeTemplate == null) {
            $messageStr = 'Шаблон Email-оповещения для ' . $emailNoticeType->name . ' и издателя ' . $publisher->name . ' не найден. Оповещение про изменения данных пользователя не отправлено. См. раздел "Email оповещения" админ панели';
            Yii::$app->session->addFlash('danger', $messageStr);
            return false;
//            throw new yii\base\ErrorException('Email notice template for <' . $emailNoticeType->name . '>  and publisher <' . $publisher->id . '> does not exist');
        }

        $senderAddr = !empty($publisher->emailSenderAddress) ? $publisher->emailSenderAddress->value : Yii::$app->params['noAnswerEmail'];
        $emailJobData = [
            'templateName' => $emailNoticeTemplate->template_file,
            'data' => [
                'user' => $user,
                'publisher' => $user->publisher,
                'attrsList' => $event->attrsList,
                'changedAttrs' => $event->changedAttrsList,
                'oldValues' => $event->oldValues,
            ],
            'subject' => $emailNoticeType->email_subject,
            'fromEmail' => $senderAddr,
            'toEmail' => $user->Email,
        ];
        self::sendEmail($emailJobData, intval($publisher->id));
    }


    protected static function send($data = false)
    {
        $data = !$data ? static::$data : $data;
        \Yii::$app->queue->push(new \common\models\NoticeJob($data));

    }

    protected static function getUser($user_id = 0)
    {
        if (!$user = \common\models\User::find()->where(['User_ID' => $user_id])->one())
            throw  new \Exception('User not found for send mail');

        return $user;

    }

    private static function sendEmail($data, $publisherId)
    {
        $job = new \common\queue_jobs\EmailNotificationJob($publisherId, $data);
        if ($job->validate()) {
            /** @var Queue $queue */
            $queue = \Yii::$app->queue;
            $jobId = $queue->push($job);

            $flashIsAcceptable = Yii::$app instanceof \yii\web\Application && !empty(Yii::$app->getSession());
//            $job->execute($queue);
            if (!empty($jobId)) {
                $messageStr = 'Email-оповещение будет отправлено на адрес: ' . $data['toEmail'];
                $messageType = 'success';
            } else {
                $messageStr = self::class .  ': Что-то пошло не так при отправке письма. Email-оповещение НЕ будет отправлено.';
                $messageType = 'danger';
            }

            if ($flashIsAcceptable) {
                Yii::$app->session->addFlash($messageType, $messageStr);
            }
            if (empty($jobId)) {
                throw new \yii\base\ExitException($messageStr);
            }

//            $job->execute($queue);
        } else {
            $errAttrs = array_keys($job->getErrors());
            $errMessage = 'incorrect data was passed to EmailNotificationJob model. There are errors in attributes: ' . implode(',', $errAttrs);
//            Yii::$app->session->addFlash('danger', $errMessage);
            throw new \yii\base\ExitException($errMessage);
        }
    }
}

