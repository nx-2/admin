<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "regions".
 *
 * @property integer $RegionUser_ID
 * @property string $RegionUser_Name
 * @property integer $RegionUser_Priority
 * @property string $Value
 * @property integer $enabled
 */
class Regions extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RegionUser_Priority', 'enabled'], 'integer'],
            [['Value'], 'string'],
            [['RegionUser_Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RegionUser_ID' => 'ID',
            'RegionUser_Name' => 'Название',
            'RegionUser_Priority' => 'Код',
            'Value' => 'Значение',
            'enabled' => 'Вкл.Выкл',
        ];
    }
}
