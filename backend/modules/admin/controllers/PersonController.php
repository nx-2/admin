<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\controllers;
use Yii;
use common\models\NxPerson;
// use backend\models\ComanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\admin\controllers\SiteController;
use common\custom_components\CustomHelpers;
use backend\modules\admin\models\NxPersonSearch;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use backend\controllers\behaviors\Autocomplete;
use common\models\NxAddress;
use yii\helpers\Url;

class PersonController extends SiteController
{
    public function behaviors()
    {
        return [
            // 'access' => parent::behaviors()['access'],
 /*           'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                        if(Yii::$app->user->isGuest) {
                            $this->redirect('/site/login');
                        } else {
                            throw new \yii\web\ForbiddenHttpException('Извините, но вам сюда нельзя!');
                        }
                },
                'rules' => [
                    // незалогиненым (гостям) разрешить только страницу входа и страницу ошибки
                    [
                        'controllers' => ['site'],
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','autocomplete','validate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [//залогиненым 
                        'actions' => ['getinfo', 'validate', 'autocomplete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    //Издателям можно смотреть и обновлять конкретную запись (свою)
                    // [
                    //     'controllers' => ['publisher'],
                    //     'allow' => true,
                    //     'actions' => ['view','update'],
                    //     'matchCallback' => function ($rule, $action) {
                    //         $allowedRoles = ['admin', 'publisher'];
                    //         $allowed = Yii::$app->user->identity->isRole($allowedRoles);
                    //         return (boolean)$allowed;
                    //     },
                    // ],
                    //Админам можно все
                    [
                        'controllers' => ['publisher'],
                        'allow' => true,
                        // 'actions' => ['list','create','delete'],
                        'matchCallback' => function ($rule, $action) {
                            $allowedRoles = ['admin'];
                            $allowed = Yii::$app->user->identity->isRole($allowedRoles);
                            return (boolean)$allowed;
                        },
                    ],
                ],
            ], */
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'autocomplete' => [
                'class' => Autocomplete::className(),
                'modelName' => 'NxPerson',
            ]
        ];
    }

    /**
     * Returns data for autocomplete widget.
     * @return json
     */
    public function actionAutocomplete()
    {
        return $this->autocompleteRequest(); //обращение к методу поведения  autocomplete
    }
    /**
     * returns json wrapped html with full person info
     * @param integer $id
     * @return mixed
     */
    public function actionGetinfo($id)
    {
        // get data from api
        $url = '/person/' . intval($id);
        $response = CustomHelpers::requestApi($url, 'get', [], true);
        $toReturn = ['success' => false];
        if ($response['isOk']) {
            $labels = [
                'company' => [
                    'org_form' => 'Организационная форма',
                    'shortname' => 'Короткое название',
                    'wrongnames' => 'wrongNames',
                    'description' => 'Описание',
                    'email' => 'Email',
                    'url' => 'Сайт',
                    'inn' => 'ИНН',
                    'okonh' => 'ОКОНХ',
                    'bank' => 'Банк',
                    'pay_account' => 'Расчетный счет',
                    'corr_account' => 'Корр. счет',
                    'kpp' => 'КПП',
                    'bik' => 'БИК',
                ],
                'address' => [
                    'area' => 'Область',
                    'region' => 'Район',
                    'city' => 'Город',
                    'city1' => 'Город1',
                    'zipcode' => 'Почтовый индекс',
                    'post_box' => 'Абонентский ящик',
                    'address' => 'Адрес',
                    'street' => 'Улица',
                    'house' => 'Дом',
                    'building' => 'Строение',
                    'structure' => 'Корпус',
                    'apartment' => 'Квартира',
                    'telcode' => 'Телефонный код',
                    'phone' => 'Телефон',
                    'fax' => 'Факс',
                    'comment' => 'Комментарий',
                ],
                'general' => [
                    'name' => 'Полное имя',
                    'f' => 'Фамилия',
                    'i' => 'Имя',
                    'o' => 'Отчество',
                    'gender' => 'Пол',
                    'position' => 'Должность',
                    'speciality' => 'Специальность',
                    'email' => 'Email',
                    'telcode' => 'Телефонный код',
                    'phone' => 'Телефон',
                    'address' => 'Адрес',
                    'comment' => 'Комментарий',
                ],
            ];
            $toReturn['success'] = true;
            $toReturn['html'] = $this->renderPartial('info', [
                                    'data' => json_decode($response['content'], true),
                                    'labels' => $labels,
            ]);
        } else {
            $toReturn['message'] = 'api answered with http code ' . $response['headers']['http-code'];
        }
        
        Yii::$app->response->format = 'json';
        return $toReturn;
    }
    /** 
    * Lists all NxPerson models. 
    * @return mixed 
    */
    public function actionIndex() 
    { 
        $searchModel = new NxPersonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [ 'searchModel' => $searchModel, 'dataProvider' => $dataProvider, ]);
    }

    /** 
    * Displays a single NxPerson model. 
    * @param integer $id 
    * @return mixed 
    */
    public function actionView($id) 
    { 
        return $this->render('view', [ 'model' => $this->findModel($id), ]);
    }

    /** 
    * Creates a new NxPerson model. 
    * If creation is successful, the browser will be redirected to the 'view' page. 
    * @return mixed 
    */
    public function actionCreate() { 
        $model = new NxPerson();

        switch (Yii::$app->request->isAjax) {
            case true:
                return $this->ajaxEdit($model);
                break;
            case false:
                return $this->edit($model);
                break;
        }
    }
    private function edit($model)
    {
        $addressModel = new NxAddress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $showMessage = ['type' => 'success', 'text' => 'Успешно сохранено'];
            return $this->render('update', ['model' => $model, 'showMessage' => $showMessage, 'addressModel' => $addressModel]);
        } else {
            $viewName = $model->isNewRecord ? 'create' : 'update';
            return $this->render($viewName, [
                'model' => $model,
                'addressModel' => $addressModel,
            ]);
        }
    }
    private function ajaxEdit($model)
    {
        $addressModel = new NxAddress();

        Yii::$app->response->format = 'json';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $showMessage = ['type' => 'success', 'text' => 'Успешно сохранено'];
        } 
        
        $data = ['model' => $model, 'submitAjax' => true, 'addressModel' => $addressModel];
        
        if (isset($showMessage))
            $data['showMessage'] = $showMessage;

        return \yii\helpers\Json::encode([
                    'html'      => $this->renderAjax('_form', $data), 
                    'callback'  => 'afterPersonSubmitted',
                    'model'     => ['errors' => $model->getErrors(), 'attributes' => $model->getAttributes()]
                ]);

    }
    /** 
    * Updates an existing NxPerson model. 
    * If update is successful, the browser will be redirected to the 'view' page. 
    * @param integer $id 
    * @return mixed 
    */
    public function actionUpdate($id) 
    { 
        $model = $this->findModel($id);
        switch (Yii::$app->request->isAjax) {
            case true:
                return $this->ajaxEdit($model);
                break;
            case false:
                return $this->edit($model);
                break;
        }
    }
    
    /** 
    * Deletes an existing NxPerson model. 
    * If deletion is successful, the browser will be redirected to the 'index' page. 
    * @param integer $id 
    * @return mixed 
    */
    public function actionDelete($id) 
    { 
        $this->findModel($id)->delete();
            return $this->redirect(['index']);
    }
    
    /** 
    * Finds the NxPerson model based on its primary key value. 
    * If the model is not found, a 404 HTTP exception will be thrown. 
    * @param integer $id 
    * @return NxPerson the loaded model 
    * @throws NotFoundHttpException if the model cannot be found 
    */

    public function actionValidate()
    {
        $model = new NxPerson();
        if ($model->load(Yii::$app->request->post()) &&  Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }
    }
    protected function findModel($id) 
    { 
        if (($model = NxPerson::findOne($id)) !== null) { 
            return $model;
        } else { 
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
