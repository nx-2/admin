<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_codes_old`.
 */
class m170125_121825_create_promo_codes_old_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = $this->db->schema->getTableNames();

        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('Promo_codes_old', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%Promo_codes_old}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'action_id' => 'int(11) NOT NULL',
                    'code' => 'varchar(45) NOT NULL',
                ], $tableOptions_mysql);
            }
        }

        $this->createIndex(
            'action_id',
            'Promo_codes_old',
            'action_id'
        );

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('promo_codes_old_ibfk_1','{{%Promo_codes_old}}', 'action_id', 'osp.Action', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->dropTable('Promo_codes_old');
        $this->execute('SET foreign_key_checks = 1;');

    }
}
