<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\custom_components\pay;

use common\models\NxOrderItem;
use common\models\Publisher;
use Yii;
use \common\models\PaySystemStatus;
use \common\models\Payment;
use \common\models\PaymentType;
use \common\models\NxOrder;
use \common\models\NxPerson;
use \common\models\RelationSystems;
use \common\models\NxOrderState;
use \common\models\PaySystems;
use \YandexCheckout\Client;
use YooKassa\Client AS yooClient;
use \yii\helpers\Url;
use \common\models\YaKassaApiPayment;
use \common\custom_components\pay\clients\YaKassaApiCustomCurlClient;
use YooKassa\Model\PaymentInterface;

/**
 * Description of YandexPay
 *
 * @author andreyw
 *
 * @property yooClient $yandexApiClient
 * @property  integer | boolean $orderId
 * @property  NxOrder | null  $order

 * @property  integer | boolean $publisherId
 * @property  Publisher | null  $publisher
 *
 * @property YaKassaApiPayment | null $paymentModel

 * @property  array $paySystemOptions

 * @property array $errors
 * @property boolean $hasErrors
 * @property \YooKassa\Model\PaymentInterface $payment
 *
 *
 */
class YandexApiPay implements interfaces\paySystem {

    CONST PAY_SYSTEM_ID = PaySystems::SYSTEM_YANDEX_KASSA_API;
    CONST LOG_CATEGORY = 'APIyandexKassaInteraction';
    CONST FIELD_NAME_FOR_SHOP_ID = 'param_1';
    CONST FIELD_NAME_FOR_SECRET_KEY = 'param_3';

    CONST PAYMENT_STATUS_PENDING                = 'pending';
    CONST PAYMENT_STATUS_WAITING_FOR_CAPTURE    = 'waiting_for_capture';
    CONST PAYMENT_STATUS_CANCELED               = 'canceled';
    CONST PAYMENT_STATUS_SUCCEEDED              = 'succeeded';
    CONST PAYMENT_STATUS_DROPPED                = 'dropped';

    // Если в течение этого количества минут от момента создания объекта платежа он все еще не оплачен, то забыть о нем.
    CONST PENDING_STATUS_AVAILABLE_FOR          = 120;


    private $orderId = false;
    private $order = null;

    private $publisherId = false;
    private $publisher = null;

    private $yandexApiClient = null;

    private $paySystemOptions = null;

    public $errors = [];
    public $hasErrors = false;

    private $payment = null;
    private $paymentModel = null;

    private $ya_notification_ips = ['185.71.77.2','185.71.77.3','185.71.77.4','185.71.77.5','185.71.76.2','185.71.76.3','185.71.76.4']; // с этих адресов могут приходить уведомления от яндекс кассы

    public function __construct( $orderId = false, $publisherId = false )
    {
        if ($orderId && is_integer($orderId))
            $this->orderId = $orderId;

        if ($publisherId && is_integer($publisherId))
            $this->publisherId = $publisherId;

        $this->init($orderId, $publisherId);

//        if ( $this->orderId && $this->publisherId ) {
//            $this->init();
//        }

    }

    public function init($orderId = false, $publisherId = false)
    {
        $result = false;

        if ($orderId && ( !$this->orderId || $this->orderId != intval($orderId)) ) {
            $this->orderId = intval($orderId);
        }

        if ($publisherId && ( !$this->publisherId || $this->publisherId != intval($publisherId) )) {
            $this->publisherId = intval($publisherId);
        }

        if ( !$this->orderId || !$this->publisherId ) {
            $this->errors[] = 'orderId or(and) publisherId was not supplied.';
            $this->hasErrors = true;
            return $result;
        }

        $order = NxOrder::findOne(['id' => $this->orderId]);
        $publisher = Publisher::findOne(['id' => $this->publisherId]);

        if ( null === $order || null === $publisher ) {
            $this->errors[] = 'requested order or(and) publisher was not found.';
            $this->hasErrors = true;
            return $result;
        }

        if ($order->publisher->id != $this->publisherId) {
            $this->errors[] = 'order #' . $order->id . ' does not belong to publisher #' . $this->publisherId;
            $this->hasErrors = true;
            return $result;
        }

        $this->order = $order;
        $this->publisher = $publisher;
        $systemId = $order->transaction->system;
//        $paySystemOptions =  $this->publisher->getPaySystems()->where(['system_id' => self::PAY_SYSTEM_ID])->asArray(true)->one();
        $paySystemOptions =  $this->publisher->getPaySystems()->where(['system_id' => intval($systemId)])->asArray(true)->one();

        if (null === $paySystemOptions) {
            $this->hasErrors = true;
            $this->errors[] = 'publisher #' . $this->publisherId . ' has no such payment system as YandexKassa(API) at all';
            return $result;
        }

        $this->paySystemOptions = $paySystemOptions;

        $customCurlClient = new YaKassaApiCustomCurlClient();
//        $this->yandexApiClient = new Client($customCurlClient);
        $this->yandexApiClient = new yooClient($customCurlClient);

        $shopId = $this->paySystemOptions[self::FIELD_NAME_FOR_SHOP_ID];
        $secretKey = $this->paySystemOptions[self::FIELD_NAME_FOR_SECRET_KEY];

        $this->yandexApiClient->setAuth($shopId, $secretKey);
        // заказ и издатель найдены, проверены, соответствуют друг другу, данные для подключения к api яндекс кассы найдены, клиент для общения с апи создан,
        // данные для аутентификации указаны. Но корректны ли данные для аутентификации неизвестно. Не нашел подходящего метода из коробки
//        $info = $this->yandexApiClient->me();

        $result = true;

//        $test = $this->yandexApiClient->capturePayment(['amount' => ['value' => '640.00', 'currency' => 'RUB']],'223b47ee-000f-5000-a000-1f0370affc36');
//        echo('<pre>');
//        print_r($test);
//        echo('</pre>');

//        $test = $this->yandexApiClient->getPaymentInfo('223b47ee-000f-5000-a000-1f0370affc36');
//        echo('<pre>');
//        print_r($test);
//        echo('</pre>');
//
//        die('STOP');

        return $result;

    }
    /**
     * @param string $paymentId Ид объекта платежа на стороне АПИ Яндекс кассы
     * @return PaymentInterface | null
     */
    public function getInfoApiYaKassa($paymentId = false)
    {
        $className = \common\models\YaKassaApiPayment::class;
        if (!$paymentId && null !== $this->paymentModel && $this->paymentModel instanceof $className ) {
            $paymentId = $this->paymentModel->ya_payment_id;
        }
        if ( !$paymentId )
            return null;

        try {
            $payment = $this->yandexApiClient->getPaymentInfo($paymentId);
            $this->setPayment($payment);
            return $payment;
        } catch (\Exception $exception) {
//            if ($exception instanceof \YandexCheckout\Common\Exceptions\ApiException) {
            if ($exception instanceof \YooKassa\Common\Exceptions\ApiException) {
                $exData = [
                    'type' => $exception->type,
                    'body' => json_decode($exception->getResponseBody(), true),
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString(),
                ];
            } else {
                $exData = $exception;
            }
            $this->errors[] = $exception->getMessage();
            $this->hasErrors = true;
            $this->log([
                ['comment' => 'Запрос данных о платеже', 'paymentId' => $paymentId],
                ['comment' => 'Ошибка при ответе Api YooKassa', 'incomingData' => $exData],
            ]);
            return NULL; // "аварийное" ничегонеделание
        }
    }
    /**
     * @param $payment \YooKassa\Model\PaymentInterface
     * @return void
     */
    public function setPayment($payment = null)
    {
        if (null !== $payment)
            $this->payment = $payment;
    }
    /**
     * @return  \YooKassa\Model\PaymentInterface
     */
    public function getPayment()
    {
        return $this->payment;
    }
    /**
     * @param YaKassaApiPayment $paymentModel
     * @return void
     */
    public function setPaymentModel( $paymentModel )
    {
        $this->paymentModel = $paymentModel;
    }
    /**
     * @return YaKassaApiPayment | null
     */
    public function getPaymentModel()
    {
        return $this->paymentModel;
    }

    /**
     * @return NxOrder
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * @return Publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }
    /**
     * см. документацию яндекс касса https://kassa.yandex.ru/docs/checkout-api/?php#platezhi
     */
    public function createApiYaKassaPayment()
    {
        //Проверить существование объекта платежа для этого заказа
        $paymentModel = YaKassaApiPayment::findOne(['order_id' => $this->orderId]);
        if ( null !== $paymentModel ) {
            $payment = $this->getInfoApiYaKassa($paymentModel->ya_payment_id);
            if (!$payment) {
                $errText = 'Some error occurs while fetching YooKassa payment object from API YooKassa. ID:' . $paymentModel->ya_payment_id;
                $this->errors[] = $errText;
                $this->hasErrors = true;
                $this->log([
                    ['error' => $errText]
                ]);
                return NULL;
            }

            if ($payment->status == self::PAYMENT_STATUS_SUCCEEDED || $payment->status == self::PAYMENT_STATUS_WAITING_FOR_CAPTURE) {
                $this->hasErrors = true;
                $this->errors[] = 'This Order is already paid';
            }
            $paymentModel->ya_payment_id = $payment->id;
            $paymentModel->last_status = $payment->status;
//                $paymentModel->last_idempotence_key = $idempotenceKey;
            $paymentModel->order_id = $this->orderId;
            $paymentModel->publisher_id = $this->publisherId;
            $paymentModel->save(false);

            return $payment;
        } else {
            $paymentModel = new YaKassaApiPayment();
        }
        // Создать новый объект платежа
        $ip = $this->getIp();
        $paymentData = [
            'amount' => [
                'value' =>  $this->order->price,
                'currency' => 'RUB',
            ],
            'confirmation' => [
                'type' => 'redirect',
                //URL, на который вернется пользователь после подтверждения или отмены платежа на веб-странице.
                'return_url' => Yii::$app->params['homeHost'] . '/site/pay-notify?' .  http_build_query(['orderId' => $this->orderId, 'publisherId' => $this->publisherId]),
            ],
            'description' => $this->order->toString(),
            'client_ip' => $ip,
            'metadata' => [
                'orderId' => $this->orderId,
                'publisherId' => $this->publisherId,
            ],
        ];

        if ( boolval($this->paySystemOptions['send_data_for_FZ54']) ) {
            // Если издатель желает и настроил в личном кабинете яндекс кассы отправку данных чека по 54-ФЗ
            $paymentData['receipt'] = $this->buildReceiptArr();
            $paymentData['capture'] = false;
        } else {
            $paymentData['capture'] = true; //Подтверждать этот платеж не нужно будет. Он, в случае успешной оплаты, сразу получит статус "succeded" минуя статус "waiting_for_capture"
        }
        $idempotenceKey = md5($this->order->id . '_' . time());
        try {
            $payment = $this->yandexApiClient->createPayment($paymentData, $idempotenceKey);
            $this->setPayment($payment);

            $rawResponse = $this->yandexApiClient->getApiClient()->getLastResponse();

            $paymentModel->ya_payment_id = $payment->id;
            $paymentModel->last_status = $payment->status;
            $paymentModel->last_idempotence_key = $idempotenceKey;
            $paymentModel->order_id = $this->orderId;
            $paymentModel->publisher_id = $this->publisherId;
            $paymentModel->save(false);

            $this->log([
                ['comment' => 'Запрос на создание платежа', 'outcomingData' => $paymentData],
                ['comment' => 'Ответ Api YooKassa', 'incomingData' =>   json_decode($rawResponse->getBody(),true)],
            ]);
            $this->setPaymentModel($paymentModel);
            return $payment;

        } catch (\Exception $exception) {
            if ($exception instanceof \YandexCheckout\Common\Exceptions\ApiException) {
                $exData = [
                    'type' => $exception->type,
                    'body' => json_decode($exception->getResponseBody(), true),
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString(),
                ];
            } else {
                $exData = $exception;
            }
            $this->errors[] = $exception->getMessage();
            $this->hasErrors = true;
            $this->log([
                ['comment' => 'Запрос на создание платежа', 'outcomingData' => $paymentData],
                ['comment' => 'Ошибка от Api YooKassa', 'incomingData' =>   $exData]
            ]);
            return json_decode($exception->getResponseBody());
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
    public function cancelOrder() {
    }

    public function checkOrder() {
        return [
            'test' => [
                'key1' => 'value1',
                'key2' => 'value2',
                'key3' => 'value3',
            ],
            'message' => 'Sorry.. Correct response will be implemented ASAP',
        ];
    }
    /**
     * сюда передаются уведомления яндекс кассы об изменениях статуса платежа
     * см. документацию https://kassa.yandex.ru/docs/guides/#uwedomleniq
     * тому, что прислано в теле запроса (там по идее объект платежа) НЕ доверяем (вдруг это поддельное уведомление).
     * Нам нужен только ИД платежа. По известным ИД издателя (для аутентификации) и ИД платежа запросим текущий объект платежа у яндекс кассы и с ним будем работать
     * Для статуса платежа "waiting_for_capture" - отправить подтверждение платежа, обновить статус платежа в ya_kassa_api_payment
     * Для статуса платежа "succeeded" - создать запись в nx_payment, обновить статус платежа в ya_kassa_api_payment, обновить статус транзакции и статус заказа
     * @return boolean
     */
    public function paymentNotify() {
        // получить ИД платежа
        $paymentId = false;
        if ($this->payment !== null && isset($this->payment->id)) {
            $paymentId = $this->payment->id;
        } else {
            $yaPaymentModel = YaKassaApiPayment::findOne(['publisher_id' => $this->publisherId, 'order_id' => $this->orderId]);
            if (null !== $yaPaymentModel)
                $paymentId = $yaPaymentModel->ya_payment_id;
        }
        if (!$paymentId)
            return false; // "аварийное" ничегонеделание


        $payment = $this->getInfoApiYaKassa($paymentId);
        if (!$payment)
            return false;

        if (!isset($yaPaymentModel))
            $yaPaymentModel = YaKassaApiPayment::findOne(['ya_payment_id' => $paymentId, 'order_id' => $this->orderId, 'publisher_id' => $this->publisherId]);

        $currentPaymentStatus = $payment->getStatus();
        if ($currentPaymentStatus == self::PAYMENT_STATUS_WAITING_FOR_CAPTURE) {
            // см. https://kassa.yandex.ru/docs/checkout-api/#informaciq-o-platezhe
            // Сумму брать из заказа. При необходимости указывать данные для отправки чека по 54-ФЗ

//            $yaPaymentModel->updateAttributes(['last_status' => self::PAYMENT_STATUS_WAITING_FOR_CAPTURE]);
            $yaPaymentModel->last_status = self::PAYMENT_STATUS_WAITING_FOR_CAPTURE;
            $yaPaymentModel->update(false);
            $this->setPaymentModel($yaPaymentModel);

            $captureRequestData = [
                'amount' => [
                    'value' => $this->order->price,
                    'currency' => 'RUB',
                ],
            ];
            if ( boolval($this->paySystemOptions['send_data_for_FZ54']) ) {
                // Если издатель желает и настроил в личном кабинете яндекс кассы отправку данных чека по 54-ФЗ
                $captureRequestData ['receipt'] = $this->buildReceiptArr();
            }

            $capturedResponse = $this->capturePayment($captureRequestData, $payment->id);// подтверждаем платеж
            if (!$capturedResponse) {
                return false;
            }

            if ($capturedResponse->status == self::PAYMENT_STATUS_SUCCEEDED) {
                $this->payment->status = self::PAYMENT_STATUS_SUCCEEDED;
                return $this->afterSuccessPayment();
            }
        } elseif ($currentPaymentStatus == self::PAYMENT_STATUS_SUCCEEDED) {
            $this->setPaymentModel($yaPaymentModel);
            return $this->afterSuccessPayment();
        } elseif ($currentPaymentStatus == self::PAYMENT_STATUS_PENDING) {
            if ($payment->created_at instanceof \DateTime) {
                $createdUTC = $payment->created_at->getTimestamp();
            } elseif (is_string($payment->created_at)) {
                $createdUTC = strtotime($payment->created_at);
            }
            $minutesPast = round((time() - $createdUTC) / 60, 0);
            if ($minutesPast > self::PENDING_STATUS_AVAILABLE_FOR) {
                //пометить как "брошеный" чтобы он больше не попадал в поле зрения никогда
                $yaPaymentModel->last_status = self::PAYMENT_STATUS_DROPPED;
                $yaPaymentModel->update(false);
                $logData = [
                    'comment' => 'После создания платежа ' . $payment->id . ' для заказа ' . $this->orderId . ' прошло более чем ' . self::PENDING_STATUS_AVAILABLE_FOR . ' минут. Он помечен как ' . self::PAYMENT_STATUS_DROPPED,
                ];
                $this->log($logData);
            }
            return true;
        } elseif ($currentPaymentStatus == self::PAYMENT_STATUS_CANCELED) {
            $yaPaymentModel->last_status = self::PAYMENT_STATUS_CANCELED;
            $yaPaymentModel->update(false);

            // Проставить статусы отмены заказу и транзакции, а платеж (если он уже есть) вообще удалить
            $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_CANCELED_BY_PAYMENT_SYSTEM],false);
            $tUpdated = $this->order->transaction->update(false);
            $this->order->setAttributes(
                [
                    'order_state_id' => NxOrderState::ORDER_STATE_CANCELED,
                    'payment_date' => null,
                    'payment_sum' => 0,
                    'epay_result' => '',
                ],false
            );
            $this->order->update(true);

            if (null !== $this->order->payment){
                $this->order->payment->delete();
            }


            $rawResponse = $this->yandexApiClient->getApiClient()->getLastResponse();

            $logData = [
                'comment' => 'Платеж "' . $payment->id . '" по заказу № ' . $yaPaymentModel->order_id . ' был отменен. Средства, если они были списаны, будут возвращены плательщику. ' ,
                ['comment' => 'Ответ Api YooKassa ', 'incomingData' => json_decode($rawResponse->getBody(),true)],
            ];
            $this->log($logData);
        }
    }
/**
 * @param array $requestData
 * @param  string $paymentId
 * @return \YooKassa\Request\Payments\Payment\CreateCaptureResponse | boolean
 */
    public function capturePayment($requestData, $paymentId)
    {
        try {
            $payment = $this->yandexApiClient->capturePayment($requestData, $paymentId);

            $rawResponse = $this->yandexApiClient->getApiClient()->getLastResponse();

            $logData = [
                ['comment' => 'Запрос на подтверждение платежа', 'outcomingData' => $requestData, 'orderId' => $this->orderId, 'publisherId' => $this->publisherId],
                ['comment' => 'Ответ Api YooKassa ', 'incomingData' => json_decode($rawResponse->getBody(),true)],
            ];
            $this->log($logData);
            return $payment;
        } catch (\Exception $exception) {
            if ($exception instanceof \YandexCheckout\Common\Exceptions\ApiException) {
                $exData = [
                    'type' => $exception->type,
                    'body' => json_decode($exception->getResponseBody(), true),
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString(),
                ];
            } else {
                $exData = $exception;
            }
            $this->errors[] = $exception->getMessage();
            $this->hasErrors = true;
            $this->log([
                ['comment' => 'Запрос на подтверждение платежа', 'outcomingData' => $requestData, 'orderId' => $this->orderId, 'publisherId' => $this->publisherId],
                ['comment' => 'Ошибка от Api YooKassa', 'incomingData' =>   $exData]
            ]);
            return false;
        }
    }
    public function log($data)
    {
        // TODO: Implement log() method.
//        $logObj = [
//            ['comment' => 'Запрос от Yandex кассы','incomingData' => $this->incomingData,],
//            ['comment' => 'Ответ на запрос Yandex кассы','outcomingData' => $answer,],
//            'post' => $_POST,
//            'server' => $_SERVER,
//        ];
//        Yii::info($logObj, 'yandexInteraction');
        $logger = Yii::getLogger();
        $logger->log($data,\yii\log\Logger::LEVEL_INFO, self::LOG_CATEGORY);
        return;

    }
    private function getIp()
    {
        $ip = 'unknown';
        $ipAddrKeys = ['HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR'];
        foreach ( $ipAddrKeys as $k ) {
            if (isset($_SERVER[$k]) && !empty($_SERVER[$k])) {
                $ip = $_SERVER[$k];
                break;
            }
        }
        return $ip;
    }

    /**
     * 1. Проверить ситуацию, когда мы имеем дело с уже оплаченым заказом, для которого все статусы уже обновлены и установлены ранее.
     * просто по какой-то причине этот объект платежа обрабатывается не впервые.
     * 2. Создать новый платеж в nx_payment
     * 3. Обновить статус платежа в ya_kassa_api_payment
     * 4. Обновить статус транзакции
     * 5. Обновить статус заказа
     * @return boolean
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    private function afterSuccessPayment()
    {

        $pm = $this->getPaymentModel();
        if (null === $pm) {
            $paymentModel = YaKassaApiPayment::findOne(['order_id' => $this->orderId, 'publisher_id' => $this->publisherId]);
            $this->setPaymentModel($paymentModel);
            $pm = $paymentModel;
            if ($pm === null) {
                $this->hasErrors = true;
                $this->errors[] = 'payment model not found';
                return false;
            }
        }
        $payment = $this->getPayment();
        if (null === $payment) {
            $payment = $this->getInfoApiYaKassa($pm->ya_payment_id);
            if (!$payment) {
                $this->hasErrors = true;
                $this->errors[] = 'Error while fetching YooKassa payment object ID = ' . $pm->ya_payment_id;
                return false;
            }
        }

        if ($payment->id != $pm->ya_payment_id) {
            $this->hasErrors = true;
            $this->errors[] = 'paymentId discrepancy';
            return false;
        }

        if ($payment->status != self::PAYMENT_STATUS_SUCCEEDED) {
            $this->hasErrors = true;
            $this->errors[] = 'Trying to update order with not paid payment';
            return false;
        }

        if ($pm->last_status != self::PAYMENT_STATUS_SUCCEEDED) {
            $pm->last_status = self::PAYMENT_STATUS_SUCCEEDED;
            $pm->update(false);
            // Все остальное см. в \common\queue_jobs\AfterYaKassaApiSucceededJob
        }

        //TODO: Весь нижеследующий функцилнал хорошо бы перенести в обработчик события модели YaKassaApiPayment
        // нужно привязать к этой модели поведение, отслеживающее изменение атрибута last_status в значение "succeeded"
        // пусть это поведение генерит соотвтетствующее событие, обработчик которого либо пусть сразу делает всю ниженаписанную работу, либо пусть добавит в очередь задачу
        // выполняющую всю эту работу

//        $isAlreadyPaid =  $this->order->order_state_id == NxOrderState::ORDER_STATE_PAID //статус заказа уже "оплачен"
//                            && $this->order->payment != null //платеж уже существует
//                            &&  (
//                                    $this->order->transaction->sys_status == PaySystemStatus::STATUS_PAID_IMMEDIATELY
//                                    ||
//                                    $this->order->transaction->sys_status == PaySystemStatus::STATUS_PAID_ON_REQUEST_TO_PAYMENT_SYSTEM
//                                ) //статус транзакции уже 'Оплачен'
//                            && $this->order->payment->sum == $payment->amount->value; //Сумма оплаты правильная
//
//        if ($isAlreadyPaid)
//            return true;
//
//        $transaction = Yii::$app->db->beginTransaction();
//        $d = !(null == $payment->captured_at) ?  $payment->captured_at : $payment->created_at;
//        $this->order->setAttributes(
//            [
//                'order_state_id' => NxOrderState::ORDER_STATE_PAID, // оплачен
////                'payment_date' => date('Y-m-d'),
//                'payment_date' => $d->format('Y-m-d'),
//                'payment_type_id' =>  PaymentType::PAYMENT_TYPE_ONLINE,
//                'payment_agent_id' => 0,
//                'payment_sum' => $payment->amount->value,
//                'epay_result' => 'approved',
//            ],false
//        );
//
//        $oUpdated =  $this->order->update(true);
//        //оплачено, сегодня, электронным платежем, агент не указан, сумма
//        // $oUpdated хранит affectedRowsNumber (т.е. 0 или 1)
//
////        $tUpdated = $this->order->transaction->updateAttributes(['sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY]); //оплачено
//        $this->order->transaction->setAttributes(['system' => self::PAY_SYSTEM_ID, 'sys_status' => PaySystemStatus::STATUS_PAID_IMMEDIATELY],false);
//        $tUpdated = $this->order->transaction->update(true);
//        // $tUpdated хранит affectedRowsNumber (0 или 1)
//
//        $paymentData = [
//            'order_id' => $this->order->id,
//            'transaction_id' => $this->order->transaction->id,
////            'sum' => $this->incomingData['orderSumAmount'],
//            'sum' => $payment->amount->value,
////            'date' => $this->answer['performedDatetime'],
//            'date' => $d->format('Y-m-d'),
//            'year' => $d->format('Y'),
//            'payment_agent_id' => 0,
////            'purpose' => '',
////            'comment' => '',
////            'status' => Payment::STATUS_NOT_SENT_TO_1C_YET,
//            'status' => Payment::STATUS_SENT_TO_1C_SUCCESSFULL, //это ВРЕМЕННО!!! Строка выше - правильная.
//            'result' => serialize($payment),
//            'request_data' => serialize($payment),
//            'label' => 'transaction_payment',
//        ];
//
//        $publisherId = false;
//        try {
//            $publisherId = $this->publisher->id;
//        } catch (\Exception $e) {
//            $publisherId = false;
//        }
//
//        if ( $publisherId )
//            $paymentData['publisher_id'] = $publisherId;
//
//        if ($this->order->payment != null) {
//            $this->order->payment->setAttributes($paymentData, false);
//            $pUpdated = $this->order->payment->update(true);
//            // $pUpdated хранит affectedRowsNumber т.е. 0 или 1
//        } else {
//            $nxPayment = new Payment();
//            $nxPayment->setAttributes($paymentData);
//            $pUpdated = $nxPayment->insert(true); // insert возвращает boolean
//        }
//
//        if (boolval($oUpdated) && boolval($tUpdated) && boolval($pUpdated)) {
//            $transaction->commit();
//        } else {
//            $oErr = print_r($this->order->getErrors(),true);
//            $tErr = print_r($this->order->transaction->getErrors(), true);
//            $pErr = isset($nxPayment) ? print_r($nxPayment->getErrors(), true) : print_r($this->order->payment->getErrors(), true);
//            $logMessage = 'API Yandex касса сообщила про успешную оплату заказа №' . $this->order->id
//                . ', но при обновлении заказа, транзакции, платежа что-то пошло не так. Статусы НЕ БЫЛИ обновлены ' . $oErr . ', ' . $tErr . ', ' . $pErr;
//            $this->log($logMessage);
//            $transaction->rollBack();
//            $this->hasErrors = true;
//            $this->errors[] = $oErr . ', '. $tErr . ', '. $pErr;
//            return false;
//        }

        return true;
    }

    private function buildReceiptArr(){
        $result = [
            'items' => [],
        ];
        foreach ($this->order->items as $orderItem) {

            if ($orderItem->delivery_type_id == \common\models\DeliveryType::PDF_TYPE) {
                $vat = $this->publisher->pdfVat;
            } else {
                $vat = $this->publisher->paperVat;
            }

            $price = Yii::$app->formatter->asDecimal($orderItem->price - ($orderItem->price * $orderItem->discount / 100),2);
            $price = str_replace(',', '.', $price);
            $item = [
                'description' => $orderItem->toString(),
                'quantity' => $orderItem->quantity,
                'amount' => [
                    'value' => $price,
                    'currency' => 'RUB',
                ],
                'vat_code' => $vat->yandex_kassa_id,
            ];
            $result['items'][] = $item;
        }
        if (!empty($this->order->email)) {
            $result['email'] = $this->order->email;
        } elseif ( $this->order->peron !== null ) {
            $result['phone'] = $this->order->peron->phone;
        }
        return $result;
    }
}
