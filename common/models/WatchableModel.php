<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\CommonModel;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

class WatchableModel extends CommonModel
{
    // Родительский класс для моделей, у которых есть такие атрибуты: created, create_user_id, last_updated, last_user_id
    // для подключения timestampBehavior и BlameableBehavior в одном месте 
    // Таких моделей нашлось 17. А именно:
    // 'mapi_role','nx_chart','nx_comment','nx_company','nx_file_storage','nx_order','nx_order_item','nx_payment','nx_person',
    // 'nx_price','nx_price_item','nx_role','nx_shipment','nx_subscription','nx_tag','publisher', user

    public function behaviors()
    {

        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user_id',
                'updatedByAttribute' => 'last_user_id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                // 'updatedAtAttribute' => false, //это поле будет обновляться средствами БД (CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) 
                
                // метки времени в БД нужно хранить только в UTC. Никаких "местных" времен в БД не должно быть. Функция БД CURRENT_TIMESTAMP возвращает метку времени 
                // в соответствии с тем, что указано в настройках БД. Если @@global.time_zone = system а на сервере установлено московское время, то и CURRENT_TIMESTAMP 
                // будет отдавать московское время, а не UTC как нам нужно. Поэтому прежде чем пользоваться CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP нужно 
                // в настройках mysql (my.cnf или какой там сейчас конфигурационный) указать default_time_zone='+00:00'
                // пока этого не сделано поля last_updated будем устанавливать средствами ПХП
                'updatedAtAttribute' => 'last_updated',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'last_updated',
                ],
                // 'value' => new \yii\db\Expression('NOW()'),
                // 'value' => date('Y-m-d H:i:s', time()),
//                'value' => gmdate("Y-m-d\TH:i:s\Z"),// current UTC timestamp
                'value' => gmdate("Y-m-d H:i:s"),// current UTC timestamp
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (parent::afterSave($insert, $changedAttributes)) {
            $this->refresh();
            return true;
        } else {
            return false;
        }
    }
}
