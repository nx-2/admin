<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "issue_download_file".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $issue_id
 * @property string $file_hash
 * @property string $expire
 * @property string $last_ip
 * @property string $last_user_agent
 * @property string $created
 * @property integer $downloaded_times
 * @property integer $master_link
 * *
 * @property Issue $issue
 * @property NxOrder $order
 */
class IssueDownloadFile extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issue_download_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['issue_id', 'file_hash'], 'required'],
            [['order_id', 'issue_id', 'downloaded_times', 'master_link'], 'integer', 'skipOnEmpty' => true],
            ['master_link', 'default', 'value' => 0],
            [['expire', 'created'], 'safe'],
            [['file_hash'], 'string', 'max' => 100],
            [['last_ip'], 'string', 'max' => 45],
            [['last_user_agent'], 'string', 'max' => 255],
            [['file_hash'], 'unique'],
            [['issue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Issue::className(), 'targetAttribute' => ['issue_id' => 'Message_ID']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrder::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'issue_id' => 'Issue ID',
            'file_hash' => 'File Hash',
            'expire' => 'Expire',
            'last_ip' => 'Last Ip',
            'last_user_agent' => 'Last User Agent',
            'created' => 'Created',
            'downloaded_times' => 'Downloaded Times',
        ];
    }
    /**
     * @param integer $orderId
     * @param integer $issueId
     * @return array ['success' => true|false, ['link' => string | 'exception' => string | 'errors' => array]]
     **/
    public static function createLink($orderId = false, $issueId = false)
    {
        if (!$orderId || !$issueId)
            return false;

        $issue = \common\models\Issue::findOne($issueId);

        if ($issue == null)
            return false;

        $days = intval($issue->pdf_link_life_time);
        if ($issue->pdf_link_life_time != null && $days > 0) {
            $add = ' +' . $days . ' days';
//            $currentUTC = gmdate("Y-m-d\TH:i:s\Z");
            $currentUTC = gmdate("Y-m-d H:i:s");
            $expired = date('Y-m-d H:i:s', strtotime($currentUTC . $add));
        } else {
            $expired = null;
        }

        $link = \common\models\IssueDownloadFile::findOne(['order_id' => $orderId, 'issue_id' => $issueId, 'master_link' => 0]);

        if (empty($link))
            $link = new \common\models\IssueDownloadFile();

        $link->expire = $expired;
        $link->last_ip = isset(Yii::$app->request->userIP) ?  Yii::$app->request->userIP: '0.0.0.0';
        $link->last_user_agent = isset(Yii::$app->request->userAgent) ? Yii::$app->request->userAgent : 'Unknown';

        if ($link->isNewRecord) {
            $link->order_id = $orderId;
            $link->issue_id = $issueId;
            $link->file_hash = self::generateHash($orderId, $issueId);

//            $link->created = gmdate("Y-m-d\TH:i:s\Z");
            $link->created = gmdate("Y-m-d H:i:s");
            $link->downloaded_times = 0;
        }

        if ($link->validate()) {
            try {
                $link->save(false);
                return ['success' => true, 'link' => $link->toArray()];
            } catch (Exception $e) {
                return ['success' => false, 'exception' => $e];
            }
        } else {
            return ['success' => false, 'errors' => $link->getErrors()];
        }
    }
    private static function generateHash($orderId = false, $issueId = false)
    {
        if (!$orderId || !$issueId)
            return false;

        return md5($orderId . '&' . $issueId);
    }
    private static function generateMasterHash($issueId = false, $publisherId = false)
    {
        if (!$issueId || !$publisherId)
            return false;

        return md5($publisherId . '&' . $issueId . '&' . time());
    }

    public static function createMasterLink($issueId = false)
    {
        if (!$issueId)
            return null;

        $link = new \common\models\IssueDownloadFile();

        $issue = \common\models\Issue::findOne($issueId);

        if ($issue == null)
            return false;

        $publisherId = $issue->magazine->publisher->id;
        $order = \common\models\NxOrder::findOne(['order_state_id' => '3']); //любой, первый попавшийся оплаченный заказ. Не имеет знаения. Лишь бы существующий
        if (empty($order) || $order->id == null) {
            $orderId = NULL;
//            return ['success' => false, 'message' => 'no paid order found'];
        } else {
            $orderId = $order->id;
        }

        $link->order_id = $orderId;
        $link->issue_id = $issueId;
        $link->file_hash = self::generateMasterHash($issueId, $publisherId);
        $link->expire = null;
        $link->last_ip = Yii::$app->request->userIP;
        $link->last_user_agent = Yii::$app->request->userAgent;
//        $link->created = gmdate("Y-m-d\TH:i:s\Z");
        $link->created = gmdate("Y-m-d H:i:s");
        $link->downloaded_times = 0;
        $link->master_link = 1;

        if ($link->validate()) {
            try {
                $link->save(false);
                return ['success' => true, 'link' =>  $link->toArray()];
            } catch (\Exception $e) {
                return ['success' => false, 'exception' => $e];
            }
        } else {
            return ['success' => false, 'errors' => $link->getErrors()];
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssue()
    {
        return $this->hasOne(Issue::className(), ['Message_ID' => 'issue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(NxOrder::className(), ['id' => 'order_id']);
    }
}
