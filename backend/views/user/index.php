<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый пользователь', ['user/create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'users-list', 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            // ['class' => 'yii\grid\SerialColumn'],

            'User_ID',
            // 'Password',
            // 'Checked',

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'Checked',
                'content' => function ($model, $key, $index, $column) {
                    $checkbox = Html::checkbox('', (Boolean)$model->Checked, [
                        'id' => 'checked_' . $model->id,
                        'value' => $model->Checked,
                        // 'disabled' => true,
                        'onclick' => 'enabledClicked(event, "User","Checked")',
                    ]);
                    $label = Html::label('', 'checked_' . $model->id, ['class' => 'radio-check-label'], '');
                    $ajaxLoader = Html::tag('div', '', ['class' => 'ajax-load']);
                    $content = $ajaxLoader . $checkbox . $label;
                    $result = Html::tag('div', $content, ['class' => 'radio-check-wrap']);

                    return $result;
                },
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                'filter' => ['1' => 'вкл', '0' => 'выкл'],
                'filterInputOptions' => ['style' => 'width:100%;height:100%', 'class' => 'form-control'],
            ],


            // 'Language',
            // 'Created',
            // 'LastUpdated',
            'Email:email',
            'Login',
            // 'FullName',
            'LastName',
            'FirstName',
            'MidName',
            // 'Confirmed',
            // 'RegistrationCode',
            // 'Auth_Hash:ntext',
            // 'company',
            // 'job',
            'phone',
            // 'country',
            // 'city',
            'address:ntext',
            // 'street',
            // 'house',
            // 'building',
            // 'flat',
            // 'url:url',
            // 'company_profile',
            // 'sex',
            // 'birthday',
            // 'phone_prefix',
            // 'fax_prefix',
            // 'fax',
            // 'region',
            // 'district',
            // 'index_num',
            // 'open_email:email',
            // 'avatar',

            [
                'attribute' => 'avatar',
                'content' => function ($model, $key, $index, $column) {
                    if (empty($model->avatar)) {
                        return Html::tag('span', '--', ['style' => 'text-align:center;display:inline-block;width:100%']);
                    } else {
                        $img = Html::img($model->avatar, ['width' => '70']);
                        return Html::tag('span', $img, ['style' => 'display: inline-block;width: 100%;text-align: center;']);
                    }
                },
                'filter' => false,
            ],

            // 'UserType',
            // 'Text:ntext',
            // 'LastLoginDate',
            [
                'attribute' => 'publisher',
                'value' => 'publisher.name',
            ],
            // 'rolesIds',
            [
                'attribute' => 'roles',
                'content' => function($model, $key, $index, $column){
                    $res = '';
                    foreach($model->roles as $r) {
                        $res .= '<small class="label pull-right bg-maroon" style="margin:0px 5px 5px 0px">' . $r['name'] . '</small>';
                    }
                    return ($res);
                },
                'filter' => false,
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{update}',
                // 'controller' => '',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = Url::to(['user/update', 'id' => $key]);
                        return Html::a(
                            '',
                            $url,
                            [
                                'title' => 'редактировать пользователя',
                                'aria-label' => 'редактировать',
                                'data-pjax' => 0,
                                'class' => 'fa fa-edit fa-font-18',
                            ]
                        );
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'vertical-align: middle;'],
                // 'template' => '{update}',
                'template' => '{delete}',
                // 'controller' => '',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a(
                            '',
                            Url::to(['user/delete', 'id' => $key]),
                            [
                                'title' => 'Удалить пользователя',
                                'aria-label' => 'Удалить',
                                'data-confirm' => 'Вы уверены, что хотите удалить этого пользователя?',
                                'data-method' => 'post',
                                'data-pjax' => '#users-list',
                                'class' => 'fa fa-trash fa-font-18',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
