<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "nx_address".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $area
 * @property string $region
 * @property string $city
 * @property string $city1
 * @property string $zipcode
 * @property string $address
 * @property string $street
 * @property string $house
 * @property string $building
 * @property string $structure
 * @property string $apartment
 * @property string $phone
 * @property string $fax
 * @property string $comment
 * @property string $telcode
 * @property string $post_box
 * @property integer $is_onclaim
 * @property integer $is_enveloped
 * @property string $last_updated
 * @property string $label
 * @property integer $xpress_id
 *
 * @property ClassificatorCountry $country
 * @property NxCompany[] $nxCompanies
 * @property NxCompany[] $nxCompanies0
 * @property NxOrder[] $nxOrders
 * @property NxOrder[] $nxOrders0
 * @property NxPerson[] $nxPeople
 */
class NxAddress extends CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_address';
    }

    /**
     * @return string
     */
    public static function getAddress($id)
    {
        $data = self::find()->where(['id' => $id])->one();
        return $data;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'address', 'phone', 'street', 'house'], 'required'],
            [['id', 'country_id', 'is_onclaim', 'is_enveloped', 'xpress_id'], 'integer'],
            [['comment'], 'string'],
            [['address'], 'unique', 'on' => 'create', 'message' => 'точно такой адрес уже существует'],
            [['last_updated'], 'safe'],
            [['area', 'region', 'city', 'city1', 'address', 'street', 'phone'], 'string', 'max' => 200],
            [['zipcode'], 'string', 'max' => 10],
            [['house', 'building', 'structure', 'apartment'], 'string', 'max' => 20],
            [['fax'], 'string', 'max' => 30],
            [['telcode'], 'string', 'max' => 15],
            [['post_box'], 'string', 'max' => 50],
            [['label'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'Country_ID'], 'message' => ' Указанная страна отсутствует в справочнике'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Страна',
            'area' => 'Область',
            'region' => 'Район',
            'city' => 'Город(насел.пункт)',
            'city1' => 'City1',
            'zipcode' => 'Почтовый индекс',
            'post_box' => 'Абон. ящик',
            'street' => 'Улица',
            'house' => 'Дом',
            'building' => 'Корпус',
            'structure' => 'Строение',
            'apartment' => 'Квартира',
            'address' => 'Полный адрес',
            'telcode' => 'Телефонный код',
            'phone' => 'Телефон',
            'fax' => 'Факс',
            'comment' => 'Комментарий',
            'label' => 'Метка',
            'is_onclaim' => 'Is Onclaim',
            'is_enveloped' => 'Is Enveloped',
            'last_updated' => 'Посл. обновл.',
            'xpress_id' => 'Xpress ID',
            'country' => 'Страна'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['Country_ID' => 'country_id']);//->inverseOf('companies');
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['legal_address_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanies1()
    {
        return $this->hasMany(NxCompany::className(), ['address_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(NxOrder::className(), ['address_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrders1()
    {
        return $this->hasMany(NxOrder::className(), ['map_address_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(NxPerson::className(), ['address_id' => 'id']);
    }

    public function buildAddressString()
    {
//        if ($this->id == null)
//            return '';

        $fullAddress = [];
        if ($this->country_id !== null)
            $fullAddress [] = $this->country->Country_Name;

        if (!empty($this->area))
            $fullAddress [] = $this->area;

        if (!empty($this->region))
            $fullAddress [] = $this->region;

        if (!empty($this->city))
            $fullAddress [] = 'г.' . $this->city;

        if (!empty($this->zipcode))
            $fullAddress [] = $this->zipcode;

        if (!empty($this->street)) {
            $streetVariants = ['ул.', '.пр', 'проезд', 'переулок', 'пер.', 'пр.', 'просп.', 'проспект', 'бул.', 'бульвар', 'блвр.', 'шоссе'];
            $street = trim($this->street);
            $suffix = array_reduce($streetVariants, function ($res, $variant) use($street) {
                if (str_starts_with($street, $variant)) {
                    $res = $variant;
                }
                return $res;
            }, '');
            if (!empty($suffix)) {
                $fullAddress [] = $street;
            } else {
                $fullAddress [] = 'ул.' . $street;
            }
        }

        if (!empty($this->house))
            $fullAddress [] = 'д.' . $this->house;

        if (!empty($this->building))
            $fullAddress [] = 'корп.' . $this->building;

        if (!empty($this->structure))
            $fullAddress [] = 'стр.' . $this->structure;

        if (!empty($this->apartment))
            $fullAddress [] = 'кв.' . $this->apartment;

        $fullAddress = implode(', ', $fullAddress);
        $fullAddress = strlen($fullAddress) > strlen($this->address) ? $fullAddress : $this->address;

        return $fullAddress;

    }

    public function buildPhoneString()
    {
        $phoneString = [];
        if (!empty($this->telcode)) {
            $telCode = $this->telcode;
            preg_replace("/[\s|\(\)]/i", '',$telCode);
            if (strpos($telCode, '+7') !== false) {
                $telCode = str_replace('+7', '', $telCode);
                $phoneString [] = '+7(' . $telCode . ') ';
            } else {
                $phoneString [] = '(' . $telCode . ') ';
            }
        }

        if (!empty($this->phone))
            $phoneString [] = $this->phone;

        return implode('', $phoneString);
    }
}
