<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $data Array */
?>
<?php //echo($data['name']);?>

<div id='edition-info-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edition-info-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="edition-info-label"></h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#edition-info-general" data-toggle="tab" aria-expanded="false">Общее</a></li>

                        <?php if(isset($data['editionPassport']) && !empty($data['editionPassport'])): ?>
                            <li><a href="#edition-info-passport" data-toggle="tab" aria-expanded="true">Паспорт издания</a></li>
                        <?php endif;?>

                        <?php if(isset($data['editionPeriods']) && !empty($data['editionPeriods'])): ?>
                            <li><a href="#edition-info-periods" data-toggle="tab" aria-expanded="true">Периоды подписки</a></li>
                        <?php endif;?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="edition-info-general">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['edition'] as $name => $label):?>
                                                <?php if(!empty($data[$name]) && !is_array($data[$name])):?>
                                                    <tr>
                                                        <th><?=$label; ?></th>
                                                        <td><?=$data[$name];?></td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($data['editionPassport'])): ?>
                        <div class="tab-pane" id="edition-info-passport">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['passport'] as $name => $label):?>
                                                <?php if(!empty($data['editionPassport'][$name])):?>
                                                    <tr>
                                                        <th>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['editionPassport'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($data['editionPeriods'])): ?>
                        <div class="tab-pane" id="edition-info-periods">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <tr>
                                                <th>Период (мес.)</th>
                                                <th>По умолчанию</th>
                                            </tr>
                                            <?php //foreach($labels['periods'] as $name => $label):?>
                                            <?php foreach($data['editionPeriods'] as $period):?>
                                                    <tr>
                                                        <td style='width:100px;'>
                                                            <?=$period['period'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo($period['default'] == 1 ? 'Да' : 'Нет');?>
                                                        </td>
                                                    </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($data['legalAddress'])): ?>
                        <div class="tab-pane" id="edition-info-legal-address">
                            <div class="box">
                                <div class='box-body no-padding'>
                                    <table class='table table-striped table-condensed'>
                                        <tbody>
                                            <?php foreach($labels['address'] as $name => $label):?>
                                                <?php if(!empty($data['legalAddress'][$name])):?>
                                                    <tr>
                                                        <th style='width:100px;'>
                                                            <?=$label; ?>
                                                        </th>
                                                        <td>
                                                            <?=$data['legalAddress'][$name];?>
                                                        </td>
                                                    </tr>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
