<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "Edition_passport".
 *
 * @property integer $id
 * @property string $founderRu
 * @property string $founderEn
 * @property string $yearOfFoundation
 * @property string $distributionRu
 * @property string $distributionEn
 * @property string $auditoryRu
 * @property string $auditoryEn
 * @property string $licenseRu
 * @property string $licenseEn
 * @property string $addressRu
 * @property string $addressEn
 * @property string $phone
 * @property string $email
 * @property string $site
 * @property integer $edition_id
 * @property string $commentEn
 * @property string $commentRu
 * @property string $issn
 * @property string $ageRestriction
 *
 * @property Edition $edition
 */
class EditionPassport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Edition_passport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['yearOfFoundation'], 'safe'],
            [['edition_id'], 'required'],
            [['edition_id'], 'integer'],
            [['founderRu', 'founderEn', 'distributionRu', 'distributionEn', 'auditoryRu', 'auditoryEn', 'licenseRu', 'licenseEn', 'addressRu', 'addressEn', 'phone', 'email', 'site', 'commentEn', 'commentRu', 'issn', 'ageRestriction'], 'string', 'max' => 255],
            [['edition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edition::className(), 'targetAttribute' => ['edition_id' => 'Message_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ИД',
            'founderRu'         => 'Учредитель (Ru)',
            'founderEn'         => 'Учредитель (En)',
            'yearOfFoundation'  => 'Год основания',
            'distributionRu'    => 'Распространение (Ru)',
            'distributionEn'    => 'Распространение (En)',
            'auditoryRu'        => 'Аудитория (Ru)',
            'auditoryEn'        => 'Аудитория (En)',
            'licenseRu'         => 'Лицензия (Ru)',
            'licenseEn'         => 'Лицензия (En)',
            'addressRu'         => 'Адрес (Ru)',
            'addressEn'         => 'Адрес (En)',
            'phone'             => 'Телефон',
            'email'             => 'Email',
            'site'              => 'Адрес сайта',
            'edition_id'        => 'Издание',
            'commentEn'         => 'Комментарий (En)',
            'commentRu'         => 'Комментарий (Ru)',
            'issn'              => 'Issn',
            'ageRestriction'    => 'Возрастные ограничения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdition()
    {
        return $this->hasOne(Edition::className(), ['Message_ID' => 'edition_id']);
    }
}
