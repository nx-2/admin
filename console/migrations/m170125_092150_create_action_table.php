<?php

use yii\db\Migration;

/**
 * Handles the creation of table `action`.
 */
class m170125_092150_create_action_table extends Migration
{




    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = $this->db->schema->getTableNames();

        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('Action', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%Action}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'date_start' => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
                    'date_end' => 'timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
                    'action_code' => 'VARCHAR(255) NULL',
                    'name' => 'varchar(245) NOT NULL',
                    'desc' => 'text NOT NULL',
                ], $tableOptions_mysql);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('Action');
    }
}
