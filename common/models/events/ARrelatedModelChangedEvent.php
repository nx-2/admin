<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models\events;

/**
 * Description of ARattrChangedEvent
 * Событие, которое должно генерироваться в случаях изменения набора подчиненных записей в связанной таблице
 * со связями типа многие ко многим или один ко многим.
 * Имеется в виду НЕ изменения самих связанных записей, а изменения набора подчиненных записей
 * Например: для записи "Зарубежье" были доступны способы доставки "Почта" и "Курьер", а стало только "Почта"
 * @author andrey
 */
class ARrelatedModelChangedEvent extends \yii\base\Event {
    public $relationName; //Name of relation.
    public $oldRelatedModels;
//    public $newRelatedModels;
}
