<?php

use yii\db\Migration;

class m170320_113805_slider_item extends Migration
{
    public function up()
    {
        $mysqlCode = '
         CREATE TABLE IF NOT EXISTS `Sliders_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
   `original` varchar(345) NOT NULL,
      `resizedFiles` varchar(345) NOT NULL,
  `text` varchar(345) NOT NULL,
  `url` varchar(345) NOT NULL,
  `target` int(11) NOT NULL,
    `slider_id` int(11) NOT NULL,
      `status` int(11) NOT NULL,
      `code` text NOT NULL,
      `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

        ';
        $this->execute($mysqlCode);



        $this->createIndex(
            'slider_id',
            'Sliders_item',
            'slider_id'
        );

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('sliders_item_ibfk_1','{{%Sliders_item}}', 'slider_id', 'osp.Sliders', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        echo "m170320_113805_slider_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
