<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "email_notice_template".
 *
 * @property integer $id
 * @property integer $notice_id
 * @property string $template_file
 * @property string $name
 * @property string $data_fields
 * @property integer $is_default
 *
 * @property EmailNotice $notice
 * @property PublisherNoticeTemplate[] $publisherNoticeTemplates
 * @property Publisher[] $publishers
 */
class EmailNoticeTemplate extends \common\models\CommonModel
{

    public $template_body = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_notice_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id', 'template_file', 'name'], 'required'],
            [['notice_id'], 'integer'],
            ['is_default','boolean'],
            ['template_body', 'safe'],
            [['template_file', 'data_fields','name'], 'string', 'max' => 255],
            [['notice_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmailNotice::className(), 'targetAttribute' => ['notice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notice_id' => 'Тип уведомления',
            'template_file' => 'Шаблон',
            'data_fields' => 'Данные для шаблона',
            'is_default' => 'Использовать по умолчанию',
            'name' => 'Имя шаблона',
            'template_body' => 'Шаблон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotice()
    {
        return $this->hasOne(EmailNotice::className(), ['id' => 'notice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisherNoticeTemplates()
    {
        return $this->hasMany(PublisherNoticeTemplate::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['id' => 'publisher_id'])->viaTable('publisher_notice_template', ['template_id' => 'id']);
    }
    public function buildTemplateFile()
    {
        $viewPath = Yii::$app->components['mailer']['viewPath'] . '/';
        $id = empty($this->id) ? 'temp' : $this->id;
        $result = $viewPath .  $this->notice->key_string . '/' . $id;
        $this->template_file = $result;
        return $result;
    }
    public function createTemplateFile( $fileAlias = false )
    {
        if ( !$fileAlias ) 
            $fileAlias = $this->template_file;
        
        try {
            $fileName = Yii::getAlias( $this->template_file, true ) . '.php';
        } catch (Exception $ex) {
            $this->template_file = '@common/mail/' . $this->notice->key_string . '/' . $this->id;
            $fileName = Yii::getAlias( $this->template_file) . '.php';
        }
        $filePath = dirname($fileName);
        
        if ( !dir($filePath) ) 
            mkdir ($filePath, 0755, true);
        
        file_put_contents($fileName, '');
        
        return $fileName;
    }
    public function getTemplate_body()
    {
        if (null !== $this->template_body)
            return $this->template_body;
        
        try {
            $file = Yii::getAlias( $this->template_file, true ) . '.php';
        } catch (Exception $ex) {
            $file = $this->createTemplateFile();
        }
        
        if ( !file_exists($file) )
            $file = $this->createTemplateFile();
        
        $this->template_body = file_get_contents($file);
        return $this->template_file;
    }
    private function saveTemplateBody() 
    {
        try {
            $file = Yii::getAlias( $this->template_file, true ) . '.php';
        } catch (Exception $ex) {
            $file = $this->createTemplateFile();
        }
        file_put_contents($file, $this->template_body);
    }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if ( $insert ) {
            $this->buildTemplateFile();
            Yii::$app->db->createCommand()->update( $this->tableName(), ['template_file' => $this->template_file], 'id = ' . $this->id )->execute();
        }
        if ( $this->is_default == 1) {
            $condition = 'notice_id = ' . $this->notice_id . ' AND id <> ' . $this->id;
            Yii::$app->db->createCommand()->update( $this->tableName(), [ 'is_default' => 0 ], $condition )->execute();
        }
        if ( array_key_exists('notice_id', $changedAttributes) ) {
            // старый файл удалить
            $fp = Yii::getAlias($this->template_file) . '.php';
            if (file_exists($fp))
                unlink ($fp);
            $this->buildTemplateFile();
            Yii::$app->db->createCommand()->update( $this->tableName(), ['template_file' => $this->template_file], 'id = ' . $this->id )->execute();
            
        }
        $this->saveTemplateBody();
            
    }
    public function afterFind() 
    {
        parent::afterFind();
        $this->getTemplate_body();
    }
    public function afterDelete() {
        parent::afterDelete();
        
        $fp = Yii::getAlias($this->template_file) . '.php';
        if (file_exists($fp))
            unlink ($fp);
        
    }
}
