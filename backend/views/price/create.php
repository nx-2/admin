<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NxPrice */

$this->title = 'Создать новый прайс';
$this->params['breadcrumbs'][] = ['label' => 'Nx Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nx-price-create">

    <h1><?= Html::encode($this->title) ?></h1>
<?php 

?>
    <?= $this->render('_form', [
        'model' => $model,
        'priceItems' => $priceItems,        
    ]) ?>

</div>
