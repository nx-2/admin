<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;
use \common\models\CommonModel;

/**
 * This is the model class for table "nx_order_state".
 *
 * @property integer $id
 * @property string $name
 *
 * @property NxOrder[] $nxOrders
 */
class NxOrderState extends CommonModel
{
    const ORDER_STATE_CREATING = 1;
    const ORDER_STATE_READY = 2;
    const ORDER_STATE_PAID = 3;
    const ORDER_STATE_CANCELED = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_order_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrders()
    {
        return $this->hasMany(NxOrder::className(), ['order_state_id' => 'id']);
    }
}
