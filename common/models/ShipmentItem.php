<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_shipment_item".
 *
 * @property integer $id
 * @property integer $shipment_id
 * @property integer $item_type
 * @property integer $item_id
 * @property string $cancel_date
 * @property integer $cancel_user_id
 * @property string $last_updated
 *
 * @property Shipment $shipment
 * @property OrderItemRelease $item
 */
class ShipmentItem extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_shipment_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment_id', 'item_type', 'item_id'], 'required'],
            [['shipment_id', 'item_type', 'item_id', 'cancel_user_id'], 'integer'],
            [['cancel_date', 'last_updated'], 'safe'],
            [['shipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shipment::className(), 'targetAttribute' => ['shipment_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderItemRelease::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shipment_id' => 'Shipment ID',
            'item_type' => 'Item Type',
            'item_id' => 'Item ID',
            'cancel_date' => 'Cancel Date',
            'cancel_user_id' => 'Cancel User ID',
            'last_updated' => 'Last Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipment()
    {
        return $this->hasOne(Shipment::className(), ['id' => 'shipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(OrderItemRelease::className(), ['id' => 'item_id']);
    }
}
