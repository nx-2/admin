<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "tax_system".
 *
 * @property int $id
 * @property string $name
 * @property string $daescription
 * @property int $yandex_kassa_id 1	Общая система налогообложения 2	Упрощенная (УСН, доходы) 3	Упрощенная (УСН, доходы минус расходы) 4	Единый налог на вмененный доход (ЕНВД) 5	Единый сельскохозяйственный налог (ЕСН) 6	Патентная система налогообложения
 *
 * @property Publisher[] $publishers
 */
class TaxSystem extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['yandex_kassa_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['daescription'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'daescription' => 'Daescription',
            'yandex_kassa_id' => 'Yandex Kassa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishers()
    {
        return $this->hasMany(Publisher::className(), ['tax_system_id' => 'id']);
    }
}
