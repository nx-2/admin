<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;

use Yii;

/**
 * This is the model class for table "nx_order_item_release".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $release_id
 * @property integer $shipment_id
 * @property integer $ship_count
 * @property integer $is_doc_wait
 * @property string $doc_result
 * @property string $last_request
 * @property string $last_request_date
 *
 * @property NxOrderItem $item
 * @property Issue $release
 * @property Shipment $shipment
 * @property ShipmentItem[] $nxShipmentItems
 */
class OrderItemRelease extends \common\models\CommonModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_order_item_release';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'release_id', 'shipment_id', 'ship_count', 'is_doc_wait'], 'integer'],
            [['last_request'], 'string'],
            [['last_request_date'], 'safe'],
            [['doc_result'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => NxOrderItem::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['release_id'], 'exist', 'skipOnError' => true, 'targetClass' => Issue::className(), 'targetAttribute' => ['release_id' => 'Message_ID']],
            [['shipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shipment::className(), 'targetAttribute' => ['shipment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'release_id' => 'ИД выпуска',
            'shipment_id' => 'ИД доставки',
            'ship_count' => 'Количество',
            'is_doc_wait' => 'Is Doc Wait',
            'doc_result' => 'Doc Result',
            'last_request' => 'Last Request',
            'last_request_date' => 'Last Request Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(NxOrderItem::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssue()
    {
        return $this->hasOne(Issue::className(), ['Message_ID' => 'release_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipment()
    {
        return $this->hasOne(Shipment::className(), ['id' => 'shipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentItems()
    {
        return $this->hasMany(ShipmentItem::className(), ['item_id' => 'id']);
    }
}
