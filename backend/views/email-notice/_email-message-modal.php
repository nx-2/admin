<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div id='email-message-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="email-message-label">
    <div class="modal-dialog" role="document" style='width:auto;text-align:center;max-width:80%'>
        <div class="modal-content" style='display:inline-block;text-align:initial'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div>
                    <input type='text' placeholder='ширина...' onchange='setIframeSizes(this.value)' style='width: 70px;padding: 5px;border: 1px solid rgba(0,0,0,0.1);'>
                </div>
            </div>
            <div class="modal-body">
                <?php //echo(Yii::$app->params['homeHost']);?>
                <iframe src='/email-notice/get-email-message/<?php echo($emailId); ?>' style='border:none;outline: none' id='popupMessageIframe'>
                    <?php // echo($content); ?>
                </iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-maroon btn-flat pull-left"  onclick='resendImmediately(<?php echo($emailId); ?>, "currentUser")'>Отправить себе</button>
                <button type="button" class="btn bg-purple btn-flat pull-left"  onclick='resendImmediately(<?php echo($emailId); ?>, "same")'>Повторно отправить</button>
                <button type="button" class="btn bg-navy btn-flat" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
