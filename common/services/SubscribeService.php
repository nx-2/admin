<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\services;
use common\models\NxOrder;
use Yii;

/**
 * Description of SubscribeService
 *
 * @author andrey
 */
class SubscribeService {
    
    public function getOrderItemDiscount ( $itemData = [], $actionId = false ) {
        if ( empty($itemData) || !$actionId ) 
            return 0;
        
        $actionId = intval($actionId);
        
        if ( $actionId < 1) 
            return 0;
       
        $params = [
            ':aId' => $actionId,
            ':magId' => isset($itemData['edition_id']) ?  intval($itemData['edition_id']) : 0,
            ':month' => isset($itemData['duration']) ?  intval($itemData['duration']) : 0,
            ':type' => (Boolean) $itemData['is_pdf'] ?  'pdf' : 'paper',
        ];
        $result = Yii::$app->cache->get($params);
        if ( $result !== false )
            return $result;
        
        $queryString = "SELECT ai.discount "
                . "FROM Action a LEFT JOIN Action_items ai ON ai.action_id = a.id "
                . "WHERE a.id = :aId AND ai.mag_id = :magId AND ai.months = :month AND ai.type = :type";
        
        $discount = Yii::$app->db->createCommand($queryString)->bindValues($params)->queryOne();
        $discount = empty($discount) ? 0 : $discount['discount'];
        Yii::$app->cache->set( $params, $discount, 60 );
        return $discount;
    }
    public function calculateSumOnData ( $data = [] ) 
    {
        // Получаем массив с данными для заказа, в котором есть все необходимое, чтобы расчитать полную стоимость заказа
        // Отдаем тот же массив, но дополненный данными про акцию, скидки, цены, суммы по каждой позиции и общие суммы (с учетом скидки и без нее)
        
        $subscriberTypeId = isset($data['subscriber_type_id']) ? intval($data['subscriber_type_id']) : false;
        $publisherId = isset($data['publisher_id']) ? intval($data['publisher_id']) : false;
        $deliveryZoneId = isset($data['delivery_zone_id']) ? intval($data['delivery_zone_id']) : false;
        $deliveryTypeId = isset( $data['delivery_type_id'] ) ? intval($data['delivery_type_id']) : false;
        $countryId = isset($data['country_id']) ? intval($data['country_id']) :  false;
        $promoCode = (isset($data['promo_code']) && !empty($data['promo_code'])) ? $data['promo_code'] : false;
        $userAgreement = isset($data['user_agreement']) ? (Boolean) $data['user_agreement'] : false;
        $data['totalSum'] = 0;
        $data['oldTotalSum'] = 0;
        $items = isset($data['items']) ?  $data['items'] : false;
        
        $actionId = false;
        
        if ( $promoCode ) {
            $date = date('Y-m-d');
            $parameters = [':promoCode' => $promoCode, ':date' => date('Y-m-d')];
            $queryString = "SELECT a.id FROM Action a LEFT JOIN Promo_codes pc ON pc.action_id = a.id WHERE pc.code = :promoCode AND a.date_start <= :date AND a.date_end >= :date";
            $actionId = Yii::$app->db->createCommand($queryString)->bindValues($parameters)->queryOne();

            if ( !empty($actionId) ) {
                $actionId = $actionId['id'];
                $action = \common\models\Action::getActionById( $actionId );
                $data['action'] = $action;
            }
        } 
        
        if ( isset($data['items']) && !empty($data['items']) ) {
            foreach ( $data['items'] as $index => &$item ) {
                $isPdf = (Boolean) $item['is_pdf'];
                $editionId = intval($item['edition_id']);
                $startDate = $item['start_date'];
                $monthes = intval($item['duration']);
                $params = [
                    'publisherId' => $publisherId,
                    'subscriberTypeId' => $subscriberTypeId,
                    'editionId' => $editionId,
                    'deliveryTypeId' => $deliveryTypeId,
                    'countryId' => $countryId,
                ];
                if ( $isPdf == true ) {
                    $params ['deliveryTypeId'] = \common\models\DeliveryType::PDF_TYPE;
                    $params ['countryId'] = \common\models\Country::RUSSIA;
                }
                $price = \common\models\Edition::getPrice( $params );
                if ($price['price_id'] === NULL) {
                    $item['price_id'] = NULL;
                    $item['price'] = -1;
                    $item['sum'] = -1;
                    $item['issuesCount'] = 0;
                    $data['failed'] = true;
                    $data['errMessage'] = $price['message'] ?? 'Ошибка при вычислении стоимости заказа';
                    break;
                }
                $issuesCount = \common\models\Edition::getIssuesCount(['magId' => $editionId, 'startDate' => $startDate, 'monthes' => $monthes ]);

                $discount = $actionId != false ?  $this->getOrderItemDiscount( $item, $actionId ) : 0;
                
                $item['discount'] = $discount . '%';
                $discountRatio = $discount / 100;
                
                $item['price'] = round(floatval( $price['price'] ) - (floatval( $price['price'] ) * $discountRatio), 2) ;
                $item['oldPrice'] = floatval( $price['price'] ); //цена без скидки
                
                $item['issuesCount'] = intval($issuesCount['count']);

                $item['oldSum'] = $item['oldPrice'] * $item['issuesCount'];//сумма без скидки
                $item['sum'] = round(floatval( $item['oldSum'] ) - floatval( ($item['oldSum'] * $discountRatio) ), 2);
                $item['price_id'] = $price['price_id'];
                   
                $data['totalSum'] += $item['sum'];
                $data['oldTotalSum'] += $item['oldSum']; // Общая сумма без скидки
                $data['price_id'] = $price['price_id'];
            }
        }
        
        return $data;
    }

    public function addOrder( $data ) 
    {
        // написано для много журнальной формы 
        
        $pId = intval($data['publisher_id']);
        $wrapperWidth = isset($data['wrapperWidth']) ? intval($data['wrapperWidth']) : 'auto';

        
        // проверка входных данных 
        $validate = $this->validateOrderData( $data );
        
        if ( $validate['valid'] == false ) {
            return [
                'status' => 'invalidData',
                'errMessage' => 'Некорректные данные',
                'errors' => $validate['errors'],
            ];
        }
        
        // Тому, что пришло из браузера доверять не будем. Перерасчитаем все заново
        $data = $this->calculateSumOnData( $data );
        
        $data = $this->adaptDataForNxRequest( $data );
        $data ['apikey'] = md5(Yii::$app->params['nxApiKey']);
        $data ['method'] = 'subscription_add';

        $url = Yii::$app->params['nxAddress'] . '/index/api';
        
        $publisher = \common\models\Publisher::findOne(['id' => $pId]);
        
        if ( $publisher == null ) {
            return [
                'status' => 'err',
                'errMessage' => 'Указанный издатель не найден',
            ];
        }
        //  TODO!!  Сделать проверку пересекающихся подписок (Пересекающимися считать если совпадают ид издания и тип подписки и есть пересечение сроков подписки)
        
        $result = \common\custom_components\CustomHelpers::curlPostRequest($data, $url);
        $result = json_decode($result);

        if (!isset($result->success) || $result->success !== true){
            // аварийный выход. На стороне nx что-то не так
            $toReturn = [
                'status' => 'err',
                'errMessage' => 'при создании заказа произошла ошибка',
                'apiResponse' => json_encode($result),
            ];
            return $toReturn;
        } 
        
        $orderId = intval($result->result); // заказ вроде создан.  Найдем его и сгенерим к нему транзакцию
        $order = \common\models\NxOrder::findOne(['id' => $orderId]);
        
        $transaction = \common\models\Transactions::setTransaction( $order );
        if ( $order == null || $transaction == false ) {
            // аварийный выход после неудачи создать транзакцию к заказу
            $toReturn = [
                'status' => 'err',
                'errMessage' => 'при создании заказа и(или) транзакции произошла ошибка',
            ];
            return $toReturn;
        }
        if ( !$paySystems = \common\models\RelationSystems::find()->where(['publisher_id' => intval($publisher->id), 'enabled' => 1])->all() ) {
            //аварийный выход, когда у издателя не выбрано ни одного способа оплачивать свои заказы
            $toReturn = [
                'status' => 'err',
                'errMessage' => ' Заказ создан, но у указанного издателя отсутствуют доступные способы оплатить заказ',
            ];
            return $toReturn;
        }
        
        //списать промокод (если нужно)
        if ( $order->action_id != 0 && !empty($order->action_code) ) {
            \common\models\PromoCodes::repealPromoCode( $order->action_code, $order->id );
        }
        
        $orderCreatedEvent = new \common\models\events\ARcreatedEvent(['model' => $order]);
        $order->trigger(NxOrder::NEW_ORDER_EVENT_NAME, $orderCreatedEvent);

        $html = Yii::$app->controller->renderPartial('//subscribe/pay/allmethod', [
            'model' => $order,
            'paySystems' => $paySystems,
            'publisherId' => $publisher->id,
            'width' => $wrapperWidth,
        ]);

        $toReturn = [
            'status' => 'OK',
            'orderId' => $order->id,
            'transactionId' => $transaction->id,
            'html' => $html,
        ];
        return $toReturn;
    }
    
    private function validateOrderData ( $data = [] ) 
    {
        if ( empty($data) || !is_array($data) )
            return ['result' => false, 'errors' => []];
        
        $result = ['valid' => true, 'errors' => []];
        $generalErrors = [];
        
        if (!isset($data['items']) || empty($data['items']) ) {
            $result['valid'] = false;
            $generalErrors[] = ['items' => 'Не выбрано ни одного издания.'];
        } else {
            $itemModel = new \frontend\models\OrderItem();
            $itemsErrors = [];
            $items = $data['items'];
            unset($data['items']);
            foreach ( $items as $item ) {
                if ( !$itemModel->load(['item' =>  $item] ,'item') || !$itemModel->validate() ) {
                    $itemsErrors [$item['index']] = $itemModel->getErrors();
                    $result['valid'] = false;
                } 
            }
            if ( !empty($itemsErrors) ) 
                $result ['errors']['itemsErrors'] = $itemsErrors; 
        }
        
        if ( !isset($data['personData']) || empty($data['personData']) ) {
            $result['valid'] = false;
            $generalErrors[] = ['personData' => 'Отсутствуют данные о подписчике'];
        } else {
            $pd = $data['personData'];
            unset($data['personData']);
            $personDataModel = new \frontend\models\OrderPersonData();
            if ( !$personDataModel->load(['personData' => $pd],'personData') || !$personDataModel->validate() ) {
                $result['valid'] = false;
                $result['errors']['personDataErrors'] = $personDataModel->getErrors();
            }
        }
        
        if ( intval($data['subscriber_type_id']) == 2  && !isset($data['companyData']) ) {
            $result['valid'] = false;
            $result['errors'][] = ['personData' => 'Отсутствуют данные про компанию подписчика'];
        } elseif ( intval($data['subscriber_type_id']) == 2 ) {
            $cd = $data['companyData'];
            unset($data['companyData']);
            $companyDataModel = new \frontend\models\OrderCompanyData();
            if ( !$companyDataModel->load(['companyData' => $cd],'companyData') || !$companyDataModel->validate() ) {
                $result['valid'] = false;
                $result['errors']['companyDataErrors'] = $companyDataModel->getErrors();
            }
        }
        
        $orderModel = new \frontend\models\OrderData();
        if ( intval($data['action_id']) == 0 ) {
            unset($data['action_id']);
        }
        
        if ( !$orderModel->load( ['data' => $data], 'data' ) ||  !$orderModel->validate() ) {
            $result['valid'] = false;
            $result['errors']['orderDataErrors'] = $orderModel->getErrors();
        }
        
        if ( !empty($generalErrors) ) 
            $result ['errors']['generalErrors'] = $generalErrors; 
        
        return $result;
    }
    public function createSubscribe(Array $data = [])
    {
        if (empty($data))
            return false;
        $data = $this->groupSubscribeData($data);
        
        $personData = $data['person'];
        $addressData = $data['address'];
        $companyData = $data['company'];
        
        //is such address exists?
        $addressSearchConditions = [
            'country_id'    => $addressData['country_id'],
            'zipcode'       => $addressData['index'],
            'area'          => $addressData['region'],
            'region'        => $addressData['region1'],
            'city'          => $addressData['city'],
            'city1'         => $addressData['city1'],
            'street'        => $addressData['street'],
            'house'         => $addressData['home'],
            'building'      => $addressData['housing'],
            'apartment'     => $addressData['flat'],
        ];
        $address = $this->getAddress($addressSearchConditions);

        $isCompany = false;
        if (!empty($companyData['company_name']) 
                || !empty($companyData['inn_code']) 
                || !empty($companyData['kpp_code'])
            ) {
            $isCompany = true;
            $companySearchData = [
                'name' => $companyData['company_name'], 
                'inn' => $companyData['inn_code'],
                'kpp' => $companyData['kpp_code'],
                'address_id' => $address->id,
            ];
            $company = $this->getCompany($companySearchData);
        }
        
        //is such a person exists 
        $personSearchData = [
            'name' => $personData['username'], 
            'email' => $personData['email'],
            'phone' => $personData['phone'],
            'address_id' => $address->id,
            'company_id' => $company->id,
        ];
        $person = $this->getPerson($personSearchData);

        $data['person_id'] = $person->id;
        $data['company'] = $company->id;
       
        return $data;
    }
    
    private function groupSubscribeData(Array $data = [])
    {
        //Получает данные в виде простого ассоциативного массива.
        //Возвращает массив с теми же данными, кот. получил, но сгруппироваными по типам
        
        if(empty($data))
            return ['person' => [],'address' => [],'edition' => [],'company' => [],'general' => []];
        
        $categoriesMap = [
            'person' => ['username', 'email','phone'],
            'address' => ['country_id','index','region','region1','area','city','city1','street','home','housing','flat'],
            'edition' => ['subscriber_type_id','magazineID','date_start','period','promocode','delivery_type_id','discount'],
            'company' => ['company_name','legal_address','inn_code','kpp_code'],
            'general' => ['price_id','publisher_id'],
        ];
        
        $groupedData = [];
        foreach($categoriesMap as $category => $keysList){
            foreach($keysList as $key) {
                if(!isset($groupedData[$category]))
                    $groupedData[$category] = [];
                
                if (isset($data[$key]))
                    $groupedData [$category] [$key] = $data[$key];
                else $groupedData [$category] [$key] = '';
            }
        }
        return $groupedData;
    }
    
    private function getAddress(Array $data)
    {
        // получает массив с данными об адресе. Ищет по ним существующий адрес и возвращает его.
        // Интересует полное совпадение. Если не находит, то создает новый адрес
        // и возвращает его. В случае неудачной попытки создать новый адрес выбрасывает исключение

        $address = \common\models\NxAddress::findOne($data);
        
        if (null == $address) {
            $address = new \common\models\NxAddress();
            $address->attributes = $data;
            // заполним обязательные поля
            $address->address = $address->buildAddressString();
            $address->phone = !empty($personData['phone']) ? empty($personData['phone']) : '0';
            
            if (!$address->save(true)) {
                $errFields = implode(',', array_keys($address->errors));
                throw new \yii\web\BadRequestHttpException('address was not saved because of errors in ' . $errFields . ' attributes');
            } 
        } 
        return $address;
    }
    
    private function getPerson(Array $data)
    {
        // получает массив с данными о физ. лице. Ищет по ним существующего человека и возвращает его.
        // Интересует полное совпадение. Если не находит, то создает новый 
        // и возвращает его. В случае неудачной попытки создать выбрасывает исключение
        
        $person = \common\models\NxPerson::findOne($data);
        
        if (null == $person) {
            $person = new \common\models\NxPerson();
            $person->attributes = $data;
            
            $name = preg_replace('/ +/', ' ', $person->name); //удалить лишние пробелы. 
            $p = explode(' ', $name);
            $f = count($p) > 0 ? $p[0] : '';
            $i = count($p) > 1 ? $p[1] : '';
            $person->f = $f;
            $person->i = $i;
            if (!$person->save(true)) {
                $errFields = implode(',', array_keys($person->errors)); 
                throw new \yii\web\BadRequestHttpException('new person was not saved because of errors in ' . $errFields . ' fields');
            }
        }
        return $person;
    }
    private function getCompany(Array $data) {
        // получает массив с данными о компании(КПП ИНН назва). Ищет по ним существующую компанию и возвращает ее.
        // Интересует совпадение имени, ИНН и КПП. Если не находит, то создает новую 
        // и возвращает ее. В случае неудачной попытки создать выбрасывает исключение
        
        if (isset($data['address_id'])) {
            $addressId = $data['address_id'];
            unset($data['address_id']);
        } else {
            $addressId = false;
        }
        
        $company = \common\models\Company::findOne($data);
        
        if(null == $company) {
            $company = new \common\models\Company();
            $company->attributes = $data;
            $company->address_id = $addressId;
            
            if (!$company->save(true)) {
                $errFields = implode(',', array_keys($company->errors)); 
                throw new \yii\web\BadRequestHttpException('new company was not saved because of errors in ' . $errFields . ' fields');
            }
        }
        return $company;
    }
    
    private function adaptDataForNxRequest( $data = [] ) 
    {
        //Получает массив с данными для создания нового заказа, полученный из подписной формы 
        // преобразует этот массив к виду, подходящему для отправки на nx Api, где и будет создан новый заказ
        
        if ( empty($data) || !is_array($data) || !isset($data['items']) || empty($data['items']) )
            return false;

        $label = "multy_mag_subscribe_form";
        
        $items = [];
        foreach ( $data['items'] as $item ) {
            $condition = 'MagazineID = ' . $item['edition_id'] . ' AND PublicDate >= \'' . $item['start_date'] . '\'';
            
            $startItemData = \common\models\Issue::find()->select(['Number', 'year'])->where($condition)->one();

            $newItem = [
                'periodical_id' => $item['edition_id'],
                'date_start' => $item['start_date'],
                'period' => $item['duration'],
                'delivery_type_id' => $item['is_pdf'] != 1 ? $data['delivery_type_id'] : \common\models\DeliveryType::PDF_TYPE,
                'discount' => str_replace('%', '', $item['discount']),
                'rqty' => $item['issuesCount'],
                'price' => $item['oldPrice'], // цена БЕЗ учета скидки!!
                
            ];
            if ( !empty($startItemData) ) {
                $newItem['byear'] = $startItemData['year'];
                $newItem['bnum'] = $startItemData['Number'];
            }
            $items[] = $newItem;
        }
        $area = empty($data['personData']['area']) ? '' : $data['personData']['area'] . ',';
        $region = $data['personData']['region'] ==  '' ? '' : $data['personData']['region'] . ' район,';
        $zip = $data['personData']['zipcode'] == '' ? '' : $data['personData']['zipcode'] . ',';
        $city = $data['personData']['city'] == '' ? '' :  $data['personData']['city'] . ',';
        $city1 = $data['personData']['city1'] == '' ? '' : $data['personData']['city1'] . ',';
        $street = $data['personData']['street'] == '' ? '' : ' ул.' . $data['personData']['street'] . ',';
        $house = $data['personData']['house'] == '' ? '' : ' д.' . $data['personData']['house'] . ', ';
        $building = $data['personData']['building'] == '' ? '' : ' корп.' . $data['personData']['building'] . ',';
        $structure = $data['personData']['structure'] == '' ? '' : ' строение.' . $data['personData']['structure'] . ',';
        $apartment = $data['personData']['apartment'] == '' ? '' : ' кв.' . $data['personData']['apartment'];
        
        $fullAddress = $area . $region . $zip . $city . $city1 . $street . $house . $building . $structure . $apartment;
        
        
        
        $subscriptionData = [
            'subscriptionData' => [
                'order_state_id' => 2,//2 = Готов, 3 = Оплачено, 1 = Формируется 4 = Отказ
                'label' => 'multy_mag_subscribe_form',
                'payment_type_id' => 2,//тип оплаты (на данный момент эл.платеж по умолчанию)
                'email' => $data['personData']['email'],
                'subscriber_type_id' => $data['subscriber_type_id'], // 1 физ лицо 2 юр лицо,
                'user_id' => 0,
                'created' => date('Y-m-d H:i:s'),
                'create_user_id' => 0,
                'action_id' => isset($data['action']) ? $data['action']['id'] : 0,
                'action_code' => $data['promo_code'],
                'price_id'=>$data['price_id'],
            ],
            'addressData' => [
                'country_id' => $data['country_id'],
                'city' => $data['personData']['city'],
                'city1' =>  $data['personData']['city1'],
                'area' => $data['personData']['area'],
                'region' => $data['personData']['region'],
                'zipcode' => $data['personData']['zipcode'],
                'address' => $fullAddress, 
                'street' => $data['personData']['street'],
                'house' => $data['personData']['house'],
                'building' => $data['personData']['building'],
                'structure' => $data['personData']['structure'],
                'apartment' => $data['personData']['apartment'],
                'phone' => $data['personData']['phone'],
                'fax' => "",
                'label' => $label,
            ],
            'mapAddressData' => [
                'country_id' => $data['country_id'],
                'city' => $data['personData']['city'],
                'city1' =>  $data['personData']['city1'],
                'area' => $data['personData']['area'],
                'region' => $data['personData']['region'],
                'zipcode' => $data['personData']['zipcode'],
                'address' => $fullAddress, 
                'street' => $data['personData']['street'],
                'house' => $data['personData']['house'],
                'building' => $data['personData']['building'],
                'apartment' => $data['personData']['apartment'],
                'phone' => $data['personData']['phone'],
                'fax' => "",
                'label' => $label,
            ],
            'itemsData' => $items,

        ];
       
        $personData = [
            'personData' => [
                'position' => '',
                'speciality' => '',
                'phone' => $data['personData']['phone'],
                'email' => $data['personData']['email'],
                'name' => $data['personData']['f'] . ' ' . $data['personData']['i'], 
                'f' => $data['personData']['f'],
                'i' => $data['personData']['i'],
                'o' => '',
                'checked' => 1, 
                'created' => date('Y-m-d H:i:s'),
                'label' => $label,
                'comment' => '',
            ]
        ];
        if ( $data['subscriber_type_id'] == \common\models\NxOrder::SUBSCRIBER_COMPANY ) {
            $companyData = [
                'companyData' => [
                    'name' => $data['companyData']['company_name'],
                    'inn' => $data['companyData']['inn'],
                    'kpp' =>  $data['companyData']['kpp'],
                    'label' => $label,
                    'checked' => 1,
                    'created' => date('Y-m-d H:i:s'),
                ],
                'legalAddressData' => [
                    'address' => $data['companyData']['company_address'],
                    'label' => $label
                ]
            ];
        } else {
            $companyData = [];
        }
        $params = array(
            'params' => array(
                'subscriptionData' => $subscriptionData,
                'personData' => $personData,
                'companyData' => $companyData,
                'orderData' => [],
            )
        );
        return $params;
        
    }
}
