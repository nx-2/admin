<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;
use yii\helpers\Url;

/**
 * ComanySearch represents the model behind the search form about `common\models\Company`.
 */
class ComanySearch extends Company
{

    public  $address;
    public  $contactPerson;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'source_id', 'channel_id', 'address_id', 'legal_address_id', 'contact_person_id', 'xpressid', 'checked', 'enabled', 'ws1c_synced', 'create_user_id', 'last_user_id'], 'integer'],
            [['name', 'org_form', 'email', 'url', 'description', 'shortname', 'othernames', 'wrongnames', 'sorthash', 'inn', 'okonh', 'okpo', 'pay_account', 'corr_account', 'kpp', 'bik', 'bank', 'created', 'last_updated', 'label','address','contactPerson'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();
        $query->joinWith(['address']);
        $query->joinWith(['contactPerson']);



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'address',
                    'contactPerson' => [
                        'asc' => ['nx_person.name' => SORT_ASC],
                        'desc' => ['nx_person.name' => SORT_DESC],
                    ],
                    'email',
                ],
                'route' => Url::to(['company/index']),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'source_id' => $this->source_id,
            'channel_id' => $this->channel_id,
            'address_id' => $this->address_id,
            'legal_address_id' => $this->legal_address_id,
            'contact_person_id' => $this->contact_person_id,
            'xpressid' => $this->xpressid,
            'checked' => $this->checked,
            'enabled' => $this->enabled,
            'ws1c_synced' => $this->ws1c_synced,
            'created' => $this->created,
            'last_updated' => $this->last_updated,
            'create_user_id' => $this->create_user_id,
            'last_user_id' => $this->last_user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'org_form', $this->org_form])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'shortname', $this->shortname])
            ->andFilterWhere(['like', 'othernames', $this->othernames])
            ->andFilterWhere(['like', 'wrongnames', $this->wrongnames])
            ->andFilterWhere(['like', 'sorthash', $this->sorthash])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'okonh', $this->okonh])
            ->andFilterWhere(['like', 'okpo', $this->okpo])
            ->andFilterWhere(['like', 'pay_account', $this->pay_account])
            ->andFilterWhere(['like', 'corr_account', $this->corr_account])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'bik', $this->bik])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'nx_address.address', $this->address])
            ->andFilterWhere(['like', 'nx_person.name', $this->contactPerson]);

        return $dataProvider;
    }
}
