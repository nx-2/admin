<?php
/**
 * @link      https://gitlab.com/nx-2/admin
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/admin/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */
namespace common\models;
use yii\db\ActiveQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "nx_price".
 *
 * @property integer $id
 * @property integer $publisher_id
 * @property integer $item_type
 * @property integer $customer_type_id
 * @property string $name
 * @property string $date
 * @property integer $checked
 * @property integer $create_user_id
 * @property integer $last_user_id
 * @property string $created
 * @property string $last_updated
 *
 * @property NxOrder[] $nxOrders
 * @property Publisher $publisher
 * @property NxPriceItem[] $nxPriceItems
 */
class NxPrice extends \common\models\CommonModel
{

    public $mag_id=0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nx_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['publisher_id', 'item_type', 'customer_type_id', 'checked', 'create_user_id', 'last_user_id'], 'integer'],
            [['customer_type_id', 'name', 'date','publisher_id'], 'required'],
            ['item_type','default', 'value' => 1],
            ['customer_type_id', 'in','range' => [1,2]], //1=физлица 2=юрлица
            ['date','date','format' => 'php:d.m.Y'],
//            ['birthday','date', 'format' => 'php:d.m.Y', 'timestampAttribute' => 'birthday', 'timestampAttributeFormat' => 'php:Y-m-d 00:00:00'],
            [['date', 'created', 'last_updated','create_user_id', 'last_user_id'], 'safe'],
            [['create_user_id', 'last_user_id'],'default', 'value' => null],
            [['name'], 'string', 'max' => 255],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id'],'message' => 'Указанный издатель не существует'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'publisher_id'      => 'Издатель',
            'item_type'         => 'Item Type',
            'customer_type_id'  => 'Тип подписчика',
            'name'              => 'Название',
            'date'              => 'Дата начала действия',
            'checked'           => 'Активен',
            'create_user_id'    => 'Создан пользователем',
            'last_user_id'      => 'Обновлен пользователем',
            'created'           => 'Создан',
            'last_updated'      => 'Обновлен',
        ];
    }
    public function behaviors()
    {
        $myBehaviors =  [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_user_id',
                'updatedByAttribute' => 'last_user_id',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false, 
                // 'updatedAtAttribute' => 'last_updated',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created',
                    // ActiveRecord::EVENT_BEFORE_UPDATE => 'last_updated',
                ],
//                'value' => gmdate("Y-m-d\TH:i:s\Z"),// current UTC timestamp
                'value' => gmdate("Y-m-d H:i:s"),// current UTC timestamp
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'date',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'date',
                ],
                'value' => function ($event) {
                    $date = \DateTime::createFromFormat('d.m.Y', $event->sender->date);
                    return $date != false ?  $date->format('Y-m-d') : $event->sender->date;
                },
            ],
        ];
        return yii\helpers\ArrayHelper::merge(parent::behaviors(), $myBehaviors);
    }
    /** 
     * возвращает список id изданий, для которых в данном прайсе отсутствуют установленные цены.
     * Например, издателю принадлежит 10 изданий, но в данном прайсе установлены цены только для 7 из них
     * Мы хотим знать для каких НЕ установлены цены
     * **/
    public function unfilledEditions()
    {
        
        $allEditionsIds = $this->publisher->allMyActiveEditionsIds;
        
        $filledEditionsIds = [];
        
        if ($this->id != null) {
            $filledEditions = NxPriceItem::find()
                    ->select(['item_id'])
                    ->alias('pi')
                    ->joinWith('edition e')
                    ->where(['pi.price_id' => $this->id])
                    ->andWhere(['e.Checked' => '1'])
                    ->groupBy('pi.item_id')
                    ->orderBy('pi.item_id')
                    ->asArray(true)
                    ->all();
            foreach($filledEditions as $fe) {
                $filledEditionsIds[] = $fe['item_id'];
            }
        } 
        
        return array_diff($allEditionsIds, $filledEditionsIds);
    }
    public function clonePrice()
    {
        if ($this->id == null)
            return false;
        
        $priceItems = \common\models\NxPriceItem::find()
                ->where(['price_id' => $this->id])
                ->asArray(true)
                ->all();
        
        $newPrice = \common\models\CommonModel::cloneModel($this);
        $newPrice->id = null;
        $newPrice->created = null;
        $newPrice->date = date('d.m.Y');
        $newPrice->name .= '(копия)';
        
        if ($newPrice->save()) {
            $columns = ['price_id','item_id','zone_id','delivery_type_id','price'];  
            $rows = [];
            foreach($priceItems as $index=>$priceItem) {
                $row = [$newPrice->id, $priceItem['item_id'], $priceItem['zone_id'],$priceItem['delivery_type_id'], $priceItem['price']];
                $rows [] = $row;
            }

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            try {
                $insertedRows = $db->createCommand()->batchInsert('nx_price_item', $columns, $rows)->execute();
                $transaction->commit();
                return $newPrice->id;
            } catch(\Exception $e) {
                $transaction->rollBack();
                $newPrice->delete();
                return false;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                $newPrice->delete();
                return false;
            }
        } else {
            return false;
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxOrders()
    {
        return $this->hasMany(NxOrder::className(), ['price_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNxPriceItems()
    {
//        return $this->hasMany(NxPriceItem::className(), ['price_id' => 'id'])
//                ->joinWith('deliveryZone', true)
//                ->where(['nx_zone.enabled' => 1]);

// цены нужны только для активных журналов, атктивных зон доставки и для способов доставки указанных для этих активных зон
//        
        return $this->hasMany(NxPriceItem::className(), ['price_id' => 'id'])
                ->select('pi.*')
                ->alias('pi')
                ->join('LEFT JOIN', 'nx_zone_delivery zd', 'zd.delivery_type_id = pi.delivery_type_id AND zd.zone_id = pi.zone_id')
                ->join('LEFT JOIN', 'nx_zone z', 'zd.zone_id = z.id')
                ->join('LEFT JOIN', 'Edition e', 'pi.item_id = e.Message_ID')
                ->where('z.enabled = 1')
                ->andWhere('e.Checked = 1')
                ->orderBy('pi.item_id, pi.zone_id, pi.delivery_type_id');
        
    }

    public function getNxPriceItemsOne()
    {
        return $this->hasOne(NxPriceItem::className(), ['price_id' => 'id'])->where(['item_id' => $this->mag_id]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'create_user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastUser()
    {
        return $this->hasOne(User::className(), ['User_ID' => 'last_user_id']);
    }



}
